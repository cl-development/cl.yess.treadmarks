﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class NilActivityClaimRule : BaseBusinessRule
    {
        public NilActivityClaimRule() : base("NilActivityClaimsRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool nilActivityClaimsRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;

            //checking for any transaction adjustment
            var items = DataLoader.Items;
            var claimDetails = claimRepository.LoadClaimDetails(claim.ID,items);

            //If no claimdetail records
            if (claimDetails.Count() == 0)
            {
                nilActivityClaimsRule = true;
            }                

            if (nilActivityClaimsRule)
            {
                var error = new ValidationFailure("NilActivityClaims", "This claim has no transactions in it.");
                error.ErrorCode = "NilActivityClaims";
                validationResults.Errors.Add(error);
            }
        }
    }
}
