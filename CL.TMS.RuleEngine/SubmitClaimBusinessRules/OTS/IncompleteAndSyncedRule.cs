﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class IncompleteAndSyncedRule : BaseBusinessRule
    {
        public IncompleteAndSyncedRule() : base("IncompleteAndSyncedRule") {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool incompleteAndSyncedRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;

            if (claimRepository.IncompleteTransaction(claim.ID))
            {
                incompleteAndSyncedRule = true;
            }

            if (incompleteAndSyncedRule)
            {
                var error = new ValidationFailure("IncompleteAndSyncedRule", 
                    "This claim has incomplete transactions. You cannot submit your claim until the incomplete transactions are completed or voided and re-synced.");
                error.ErrorCode = "IncompleteAndSyncedRule";
                validationResults.Errors.Add(error);
            }
        }
    }
}
