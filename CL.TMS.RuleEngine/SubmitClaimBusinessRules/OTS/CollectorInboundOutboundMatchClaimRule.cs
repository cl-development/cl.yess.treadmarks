﻿using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class CollectorInboundOutboundMatchClaimRule : BaseBusinessRule
    {
        public CollectorInboundOutboundMatchClaimRule() : base("CollectorInboundOutboundMatchClaimRule") {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool inboundOutboundMatchClaimsRule = true;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;            
            //var submitClaimModel = ruleContext["submitClaimModel"] as CollectorSubmitClaimViewModel;
            var TotalInbound = ruleContext["TotalInbound"] as ItemRow;
            var TotalOutbound = ruleContext["TotalOutbound"] as ItemRow;

            var isNilActivity = (TotalInbound.IsAllFieldsZero && TotalOutbound.IsAllFieldsZero);

            if (!isNilActivity)
            {
                if (TotalInbound.PLT == TotalOutbound.PLT && TotalInbound.MT == TotalOutbound.MT
                        && TotalInbound.AGLS == TotalOutbound.AGLS && TotalInbound.IND == TotalOutbound.IND
                        && TotalInbound.SOTR == TotalOutbound.SOTR && TotalInbound.MOTR == TotalOutbound.MOTR
                        && TotalInbound.LOTR == TotalOutbound.LOTR && TotalInbound.GOTR == TotalOutbound.GOTR)
                {
                    inboundOutboundMatchClaimsRule = true;
                }
                else
                {
                    inboundOutboundMatchClaimsRule = false;
                }
            }

            if (!inboundOutboundMatchClaimsRule)
            {
                var error = new ValidationFailure("InboundOutboundMatchClaim", "The total inbound doesn't match with total outbound.");
                error.ErrorCode = "InboundOutboundMatchClaim";
                validationResults.Errors.Add(error);
            }
        }


    }
}
