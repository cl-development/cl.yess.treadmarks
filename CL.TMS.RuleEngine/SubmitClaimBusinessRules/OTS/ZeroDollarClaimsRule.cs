﻿using CL.Framework.RuleEngine;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class ZeroDollarClaimsRule : BaseBusinessRule
    {
        public ZeroDollarClaimsRule() : base("ZeroDollarClaimsRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool zeroDollarClaimsRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            var submitClaimModel = ruleContext["submitClaimModel"] as CollectorSubmitClaimViewModel;

            if(submitClaimModel.GrandTotal == 0){
                zeroDollarClaimsRule = true;
            }

            if (zeroDollarClaimsRule)
            {
                var error = new ValidationFailure("ZeroDollarClaims", "This claim has zero dollar.");
                error.ErrorCode = "ZeroDollarClaims";
                validationResults.Errors.Add(error);
            }
        }

    }    
        
}
