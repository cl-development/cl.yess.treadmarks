﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class PreviousClaimStillOpenRule : BaseBusinessRule
    {
        public PreviousClaimStillOpenRule() : base("PreviousClaimStillOpenRule") {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool previousClaimStillOpenRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            var firstOpenClaim = claimRepository.GetFirstOpenClaimByStartDate(claim.ParticipantId);

            if (firstOpenClaim != null)
            {
                if (firstOpenClaim.ID != claim.ID)
                    previousClaimStillOpenRule = true;
            }

            if (previousClaimStillOpenRule)
            {
                var error = new ValidationFailure("PreviousClaimOpen", "This claim cannot be submitted, as you have a previous claim open. You must submit in order by month and year.");
                error.ErrorCode = "PreviousClaimOpen";
                validationResults.Errors.Add(error);
            }
        }
    }
}
