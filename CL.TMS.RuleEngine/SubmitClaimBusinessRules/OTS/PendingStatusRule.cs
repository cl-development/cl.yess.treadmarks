﻿using CL.Framework.RuleEngine;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class PendingStatusRule : BaseBusinessRule
    {
        public PendingStatusRule() : base("PendingStatusRule") {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;
            //var hasPendingTransaction = claimRepository.HasPendingTransactionAndAdjust(claim.ID);
            var hasPendingTransaction = claimRepository.HasPendingTransactionAndAdjustForThisClaimPeriod(claim.ID);
            if (hasPendingTransaction)
            {
                var error = new ValidationFailure("PendingStatusRule",
                    "This claim cannot be submitted because the claim has transactions that are either incomplete or pending. In order to submit your claim ensure that Incomplete Transactions are completed or voided, Pending Transactions are completed or recalled, and Pending Transaction Adjustments are either accepted, rejected or recalled. Please review your claim.");
                error.ErrorCode = "PendingStatusRule";
                validationResults.Errors.Add(error);
            }
        }
    }
}
