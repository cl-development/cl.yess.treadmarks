﻿using CL.Framework.RuleEngine;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS
{
    public class LateClaimRule : BaseBusinessRule
    {
        public static string ValidationMessage = "The claim is late, therefore your incentive payment will be paid at $0.00.";

        public LateClaimRule() : base("LateClaimRule") { }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            bool lateClaimRule = false;
            var claim = target as Claim;
            var claimRepository = ruleContext["claimRepository"] as IClaimsRepository;

            if (claim.ClaimPeriod.SubmitEnd != null)
            {
                //Fixed OTSTM2-517
                if (claim.ClaimPeriod.SubmitEnd.Date < DateTime.Now.Date)
                    lateClaimRule = true;
            }

            if (lateClaimRule)
            {
                var error = new ValidationFailure("LateClaims", ValidationMessage);
                error.ErrorCode = "LateClaims";
                validationResults.Errors.Add(error);
            }
        }
    }
}
