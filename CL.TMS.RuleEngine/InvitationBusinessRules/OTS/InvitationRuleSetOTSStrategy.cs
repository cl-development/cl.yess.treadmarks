﻿using CL.Framework.BLL;
using CL.Framework.RuleEngine;

namespace CL.TMS.RuleEngine.InvitationBusinessRules.OTS
{
    public class InvitationRuleSetOTSStrategy:IBusinessRuleSetStrategy
    {
        public BaseRuleSet CreateBusinessRuleSet()
        {
            return new OTSInvitationRuleSet();
        }
    }
}
