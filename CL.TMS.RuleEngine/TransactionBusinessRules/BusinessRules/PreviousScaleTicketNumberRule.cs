﻿using CL.Framework.RuleEngine;
using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Transaction.BusinessRuleModels;
using CL.TMS.IRepository.Claims;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.TransactionBusinessRules.BusinessRules
{
    public class PreviousScaleTicketNumberRule: BaseBusinessRule
    {
        public PreviousScaleTicketNumberRule() : base("PreviousScaleTicketNumberRule")
        {
            TerminateOnFail = true;
        }

        public override void Execute<T>(T target, ValidationResult validationResults, IDictionary<string, object> ruleContext = null)
        {
            List<PreviousScaleTicketResult> result = null;
            
            var transactionRepository = ruleContext["transactionRepository"] as ITransactionRepository;
            var model = target as PreviousScaleTicketModel;

            if (model.TransactionTypeCode == TreadMarksConstants.SPS)
            {
                //SPS
                if (!string.IsNullOrEmpty(model.ScaleTicket))
                    result = transactionRepository.GetDuplicateScaleTicket(model.FormType, model.Outbound.VendorId, model.ScaleTicket, model.PeriodType, false, true);
            }
            else
            {
                //regular
                if (!string.IsNullOrEmpty(model.ScaleTicket))
                    result = transactionRepository.GetDuplicateScaleTicket(model.FormType, model.Inbound.VendorId, model.ScaleTicket, model.PeriodType);
            }

            if (result != null && result.Count > 0)
            {
                result.ForEach(c =>
                {
                    var error = new ValidationFailure("PreviousScaleTicketNumber", string.Format("{0} has used the scale ticket #{1} before with {2} in {3} claim Trans # {4}",
                            ((model.TransactionTypeCode == TreadMarksConstants.SPS) ? c.OutgoingVendor.Number : c.IncomingVendor.Number), c.TicketNumber, (model.TransactionTypeCode == TreadMarksConstants.SPS ? (c.IncomingVendor != null ? c.IncomingVendor.Number: string.Empty) : (c.OutgoingVendor != null ? c.OutgoingVendor.Number : string.Empty)),
                            c.PeriodShortName, c.TransactionNumber));
                    error.ErrorCode = "PreviousScaleTicketNumber";
                    validationResults.Errors.Add(error);
                });
            }
        }   
    }
}
