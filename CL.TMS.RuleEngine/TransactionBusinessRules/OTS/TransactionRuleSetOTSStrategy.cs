﻿using CL.Framework.BLL;
using CL.Framework.RuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RuleEngine.TransactionBusinessRules.OTS
{
    public class TransactionRuleSetOTSStrategy : IBusinessRuleSetStrategy
    {
        public BaseRuleSet CreateBusinessRuleSet()
        {
            return new OTSTransactionRuleSet();
        }
    }
}
