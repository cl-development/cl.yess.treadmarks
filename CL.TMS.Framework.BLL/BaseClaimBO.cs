﻿using CL.Framework.BLL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.IRepository.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Framework.BLL
{
    public abstract class BaseClaimBO:BaseBO
    {
        protected InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
            if (previousClaimSummary == null)
            {
                return claimsRepository.LoadInventoryOpeningSummary(claim.ParticipantId);
            }
            else
            {
                var inventoryOpeningSummary = new InventoryOpeningSummary();
                //Set current claim opening based on previous claim summary
                inventoryOpeningSummary.TotalEligibleOpeningOnRoad = previousClaimSummary.EligibleClosingOnRoad != null ? (decimal)previousClaimSummary.EligibleClosingOnRoad : 0;
                inventoryOpeningSummary.TotalEligibleOpeningOffRoad = previousClaimSummary.EligibleClosingOffRoad != null ? (decimal)previousClaimSummary.EligibleClosingOffRoad : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOnRoad = previousClaimSummary.IneligibleClosingOnRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoad : 0;
                inventoryOpeningSummary.TotalIneligibleOpeningOffRoad = previousClaimSummary.IneligibleClosingOffRoad != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoad : 0;
                return inventoryOpeningSummary;
            }
        }
    }
}
