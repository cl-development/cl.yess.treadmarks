﻿using CL.Framework.Common;
using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Transactions;
using SystemIO = System.IO;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Helper;

namespace CL.TMS.CollectorBLL
{
    public class CollectorRegistrationBO
    {
        private IApplicationRepository applicationRepository;
        private IItemRepository itemRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private IClaimsRepository claimsRepository;
        private IUserRepository userRepository;
        private IInvitationRepository invitationRepository;
        private IConfigurationsRepository configurationsRepository;
        private string uploadRepositoryPath;

        public CollectorRegistrationBO(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository,
            IApplicationInvitationRepository applicationInvitationRepository, IClaimsRepository claimsRepository, IUserRepository userRepository, IInvitationRepository invitationRepository,
            IConfigurationsRepository configurationsRepository)
        {
            this.applicationRepository = applicationRepository;
            this.itemRepository = itemRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.claimsRepository = claimsRepository;
            this.userRepository = userRepository;
            this.invitationRepository = invitationRepository;
            this.configurationsRepository = configurationsRepository;
            this.uploadRepositoryPath = AppSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            applicationRepository.UpdateFormObject(id, formObject, updatedBy);
        }

        public IList<BusinessActivity> GetBusinessActivities(int type)
        {
            var Activity = itemRepository.GetBusinessActivities(type);
            return Activity;
        }

        public CollectorRegistrationModel GetFormObject(int id)
        {
            var application = applicationRepository.GetSingleApplication(id);
            if (application == null)
            {
                throw new KeyNotFoundException();
            }

            CollectorRegistrationModel result = null;
            var applicationStatus = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            if (applicationStatus == ApplicationStatusEnum.Approved
                || applicationStatus == ApplicationStatusEnum.BankInformationSubmitted
                || applicationStatus == ApplicationStatusEnum.BankInformationApproved
                || applicationStatus == ApplicationStatusEnum.Completed)
            {
                var vendor = vendorRepository.GetSingleWithApplicationID(id);
                result = GetApprovedApplication(vendor.ID);
                result.ID = id;
                result.VendorID = vendor.ID;
            }
            else
            {
                string formObject = application.FormObject;
                if (formObject != null)
                {
                    result = JsonConvert.DeserializeObject<CollectorRegistrationModel>(formObject);
                }
                else
                {
                    result = new CollectorRegistrationModel(id);
                }
            }

            //OTSTM2-486
            UpdateApplicationEASRSupportingDocument(result);

            if (application.AssignToUser.HasValue)
            {
                result.AssignToUserId = application.AssignToUser.Value;
            }

            result.Status = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            result.SubmittedDate = application.SubmittedDate;
            result.ExpireDate = application.ExpireDate;
            //OTSTM2-973 TermsAndConditons
            result.TermsAndConditions.TermsAndConditionsID = application.TermsAndConditionsID;

            return result;
        }

        public CollectorRegistrationModel GetByTokenId(Guid tokenId)
        {
            var application = applicationRepository.GetApplicationByTokenId(tokenId);
            return GetFormObject(application.ID);
        }

        public CollectorRegistrationModel GetAllItemsList()
        {
            var result = new CollectorRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            var ListItemModel = new List<TireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new TireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new CollectorTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public CollectorRegistrationModel GetAllByFilter(int appId)
        {
            throw new NotImplementedException();
        }

        public CollectorRegistrationModel ModelInitialization(CollectorRegistrationModel model)
        {
            throw new NotImplementedException();

            //#region initialiazation
            //if (model.Status == 0)
            //    model.Status = ApplicationStatusEnum.Open;
            //if (model.InvalidFormFields == null)
            //    model.InvalidFormFields = new List<string>();
            //model.TireDetailsItemTypeString = new List<string>();

            //if (model.Status == 0)
            //    model.Status = ApplicationStatusEnum.Open;
            //if (model.InvalidFormFields == null)
            //    model.InvalidFormFields = new List<string>();
            //if (model.TireDetailsItemTypeString == null)
            //    model.TireDetailsItemTypeString = new List<string>();

            //if (model.BusinessLocation == null)
            //{
            //    model.BusinessLocation = new BusinessLocation()
            //    {
            //        BusinessLocationVendor = new Vendor()
            //        {
            //           // Status = model.Status,
            //           // InvalidFormFieldList = model.InvalidFormFields
            //        },
            //        BusinessMailingAddress = new Address()
            //        {
            //           // Status = model.Status,
            //           // InvalidFormFieldList = model.InvalidFormFields
            //        },
            //        BusinessLocationAddress = new Address()
            //        {
            //           // Status = model.Status,
            //           // InvalidFormFieldList = model.InvalidFormFields
            //        },
            //    };
            //}
            //if (model.ContactInformation == null)
            //{
            //    model.ContactInformation = new List<ContactInformation>()
            //    {
            //        new ContactInformation(){
            //            ContactInformationAddress = new Address()
            //            {
            //              //  Status = model.Status,
            //              //  InvalidFormFieldList = model.InvalidFormFields
            //            },
            //            ContactInformationContact = new Contact()
            //            {
            //                ValidStatus = model.Status,
            //                InvalidFormFieldList = model.InvalidFormFields
            //            }
            //        }
            //    };
            //}
            //if (model.SortYardDetails == null)
            //{
            //    model.SortYardDetails = new List<SortYardDetails>()
            //    {
            //        new SortYardDetails(){
            //            SortYardDetailsAddress = new Address()
            //            {
            //             //   Status = model.Status
            //            },
            //            //SortYardDetailsVendor = new VendorModel(),
            //            SortYardDetailsVendorStorageSiteModel = new VendorStorageSite()
            //            {
            //             //   Status = model.Status
            //            }
            //        }
            //    };
            //}
            //if (model.TireDetails == null)
            //{
            //    model.TireDetails = new TireDetails()
            //    {
            //        TireDetailsVendor = new Vendor()
            //    };

            //    if (model.TireDetails.TireDetailsItemType == null)
            //    {
            //        model.TireDetails.TireDetailsItemType = new List<Item>();
            //    }
            //}
            //if (model.CollectorDetails == null)
            //{
            //    model.CollectorDetails = new CollectorDetails()
            //    {
            //        CollectorDetailsVendor = new Vendor()
            //    };
            //}
            //if (model.BankingInformation == null)
            //{
            //    model.BankingInformation = new BankingInformation()
            //    {
            //        BankInformation = new BankInformation()
            //    };
            //}
            //if (model.SupportingDocuments == null)
            //{
            //    model.SupportingDocuments = new SupportingDocuments()
            //    {
            //        Attachment = new Attachment()
            //    };
            //}
            //if (model.TermsAndConditions == null)
            //{
            //    model.TermsAndConditions = new TermsAndConditions()
            //    {
            //        TermsAndConditionsApplication = new Application()
            //    };
            //}
            //if (model.ApplicationInvitationChecks == null)
            //{
            //    model.ApplicationInvitationChecks = new ApplicationInvitationChecks()
            //    {
            //        ApplicationInvitationCheck = new ApplicationInvitation()
            //    };
            //}
            //if (model.RegistrantActiveStatus == null)
            //{
            //    model.RegistrantActiveStatus = new RegistrantStatusChange();
            //}
            //if (model.RegistrantInActiveStatus == null)
            //{
            //    model.RegistrantInActiveStatus = new RegistrantStatusChange();
            //}
            //#endregion
            //return model;
        }

        //public ICollection<Vendor> GetVendorsByApplicationId(int applicationId)
        //{
        //    return this.applicationRepository.GetVendorsByApplicationId(applicationId);
        //}

        public Application GetSingleApplication(int appId)
        {
            return applicationRepository.GetSingleApplication(appId);
        }

        public Application GetApplicationById(int appId)
        {
            var result = applicationRepository.GetSingleApplication(appId);
            return result;
        }

        public void UpdateUserExistsinApplication(string formObjectData, int appId, string updatedBy)
        {
            Application application = new Application() { FormObject = formObjectData, ID = appId, AgreementAcceptedCheck = true };
            if (applicationRepository.GetSingleApplication(appId) != null)
            {
                applicationRepository.UpdateApplication(application);
            }
            else
            {
                //applicationRepository.AddApplication(application);
            }
        }

        public void RemoveEmptyContacts(ref IList<ContactRegistrationModel> contacts, ref IList<string> invalidFieldList)
        {
            if (contacts != null && contacts.Count() > 1)
            {
                List<ContactRegistrationModel> results = new List<ContactRegistrationModel>() { contacts[0] };
                IList<string> resultsFieldList = invalidFieldList.ToList();

                int totalContacts = contacts.Count();
                for (int i = 1; i < totalContacts; i++)
                {
                    bool remove = false;
                    if (contacts[i].ContactInformation != null)
                    {
                        var contact = contacts[i].ContactInformation;
                        remove = string.IsNullOrWhiteSpace(contact.FirstName) &&
                                  string.IsNullOrWhiteSpace(contact.LastName) &&
                                  string.IsNullOrWhiteSpace(contact.Position) &&
                                  string.IsNullOrWhiteSpace(contact.PhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Ext) &&
                                  string.IsNullOrWhiteSpace(contact.AlternatePhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Email);
                        if (remove)
                        {
                            //check if contact address has any information
                            if (!contacts[i].ContactAddressSameAsBusinessAddress)
                            {
                                if (contacts[i].ContactAddress != null)
                                {
                                    var address = contacts[i].ContactAddress;
                                    remove = !contact.ContactAddressSameAsBusiness &&
                                            string.IsNullOrWhiteSpace(address.AddressLine1) &&
                                            string.IsNullOrWhiteSpace(address.AddressLine2) &&
                                            string.IsNullOrWhiteSpace(address.City) &&
                                            string.IsNullOrWhiteSpace(address.Postal) &&
                                            string.IsNullOrWhiteSpace(address.Province) &&
                                            string.IsNullOrWhiteSpace(address.Country);
                                }
                            }
                        }
                    }
                    if (remove)
                    {
                        //remove items from invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                            }
                        }
                    }
                    else
                    {
                        results.Add(contacts[i]);
                        int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                        if (numRemoved > 0)
                        {
                            //replace all the names in invalidlist
                            foreach (string nameStr in invalidFieldList)
                            {
                                string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                                Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    resultsFieldList.Remove(nameStr);
                                    string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                    resultsFieldList.Add(newName);
                                }
                            }
                        }
                    }
                }
                contacts = results;
                invalidFieldList = resultsFieldList;
            }
        }

        public void UpdateApplication(Application application)
        {
            applicationRepository.UpdateApplication(application);
        }

        public void SetStatus(int applicationId, ApplicationStatusEnum status, string denyReasons, long assignToUser = 0)
        {
            applicationRepository.SetStatus(applicationId, status, denyReasons, assignToUser);
        }

        public ApplicationEmailModel GetApprovedCollectorApplicantInformation(int applicationId)
        {
            var application = this.applicationRepository.FindApplicationByAppID(applicationId);
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }

            var applicationCollectorApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                PrimaryContactLastName = vendorPrimaryContact.LastName,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = this.applicationInvitationRepository.GetTokenByApplicationId(applicationId),
            };

            return applicationCollectorApplicationModel;
        }

        public void SendEmailForBackToApplicant(int ApplicationID, ApplicationEmailModel emailModel)
        {
            #region
            string message = string.Empty;

            Guid invitationToken = this.applicationInvitationRepository.GetTokenByApplicationId(ApplicationID);
            emailModel.InvitationToken = invitationToken;

            string viewApplication = string.Format("{0}{1}{2}", AppSettings.Instance.GetSettingValue("Settings.DomainName"), "/Collector/Registration/index/", emailModel.InvitationToken);

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.BackToApplicantEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationBacktoApplicant");
            string subject = EmailContentHelper.GetSubjectByName("NewApplicationBacktoApplicant");

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "Collector")
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ViewApplication, viewApplication)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            bool bSendBCCEmail = AppSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            Email.Email email = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                AppSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(AppSettings.Instance.GetSettingValue("Email.useSSL")));

            //OTSTM2-132
            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, bSendBCCEmail ? AppSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("Collector application deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);
            #endregion
        }

        private void AddUsedTiresToVendorInventory(CollectorRegistrationModel collectorModel, Application app, Vendor vendor)
        {
            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.PLT))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.PLT, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);

                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.MT))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.MT, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.AGLS))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.AGLS, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.IND))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.IND, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.SOTR))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.SOTR, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.MOTR))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.MOTR, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.LOTR))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.LOTR, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }

            if (!string.IsNullOrWhiteSpace(collectorModel.TireDetails.GOTR))
            {
                int qty;
                if ((int.TryParse(collectorModel.TireDetails.GOTR, out qty)) && qty >= 0)
                {
                    var item = defaultItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID).StandardWeight;
                    AddToVendorInventory(app, vendor, qty, item, itemWeight);
                }
            }
        }

        private static void AddToVendorInventory(Application app, Vendor vendor, int qty, Item item, decimal itemWeight)
        {
            var inventory = new Inventory()
            {
                ItemId = item.ID,
                IsEligible = true,
                Qty = qty,
                Weight = itemWeight,
                UpdatedBy = app.AssignToUser.Value,
                UpdatedDate = DateTime.UtcNow,
                IsValidForClaim = false
            };
            vendor.VendorInventories.Add(inventory);
        }

        public int ApproveApplication(CollectorRegistrationModel collectorModel, int appID)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var app = this.applicationRepository.FindApplicationByAppID(appID);
            app.SigningAuthorityFirstName = collectorModel.TermsAndConditions.SigningAuthorityFirstName;
            app.SigningAuthorityLastName = collectorModel.TermsAndConditions.SigningAuthorityLastName;
            app.SigningAuthorityPosition = collectorModel.TermsAndConditions.SigningAuthorityPosition;
            app.FormCompletedByFirstName = collectorModel.TermsAndConditions.FormCompletedByFirstName;
            app.FormCompletedByLastName = collectorModel.TermsAndConditions.FormCompletedByLastName;
            app.FormCompletedByPhone = collectorModel.TermsAndConditions.FormCompletedByPhone;
            app.AgreementAcceptedByFullName = collectorModel.TermsAndConditions.AgreementAcceptedByFullName;
            app.Status = ApplicationStatusEnum.Approved.ToString();
            app.FormObject = collectorModel.FormObject;

            AddApplicationSupportingDocuments(collectorModel, app);

            if (collectorModel.CollectorDetails.BusinessActivity.Equals("generator", StringComparison.OrdinalIgnoreCase))
            {
                collectorModel.CollectorDetails.GeneratorStatus = true;
                collectorModel.CollectorDetails.GeneratorDate = DateTime.UtcNow;
            }

            var vendor = new Vendor
            {
                BusinessName = collectorModel.BusinessLocation.BusinessName,
                OperatingName = collectorModel.BusinessLocation.OperatingName,
                BusinessStartDate = collectorModel.CollectorDetails.BusinessStartDate,
                BusinessNumber = collectorModel.CollectorDetails.BusinessNumber,
                CommercialLiabHSTNumber = collectorModel.CollectorDetails.CommercialLiabHstNumber,
                IsTaxExempt = collectorModel.CollectorDetails.IsTaxExempt,
                CommercialLiabInsurerName = collectorModel.CollectorDetails.CommercialLiabInsurerName,
                CommercialLiabInsurerExpDate = collectorModel.CollectorDetails.CommercialLiabInsurerExpDate,
                HasMoreThanOneEmp = collectorModel.CollectorDetails.HasMoreThanOneEmp,
                WSIBNumber = collectorModel.CollectorDetails.WsibNumber,
                CIsGenerator = collectorModel.CollectorDetails.GeneratorStatus,
                CGeneratorDate = collectorModel.CollectorDetails.GeneratorDate,
                IsSubCollector = collectorModel.CollectorDetails.SubCollectorStatus, //OTSTM2-953
                SubCollectorDate = collectorModel.CollectorDetails.SubCollectorDate, //OTSTM2-953
                ApplicationID = app.ID,
                CreatedDate = DateTime.UtcNow,
                VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = updatedBy,
                ActiveStateChangeDate = DateTime.Now.Date,
                ActivationDate = DateTime.Now.Date,
                PrimaryBusinessActivity = collectorModel.CollectorDetails.BusinessActivity,
                BusinessActivityID = (int)BusinessActivityTypeEnum.Collector,
                IsActive = true,
                ContactInfo = string.Format("{0} {1}, {2}", collectorModel.ContactInformationList[0].ContactInformation.FirstName,
                                collectorModel.ContactInformationList[0].ContactInformation.LastName, collectorModel.ContactInformationList[0].ContactInformation.PhoneNumber),

                RegistrantStatus = ApplicationStatusEnum.Approved.ToString(),
                SigningAuthorityFirstName = collectorModel.TermsAndConditions.SigningAuthorityFirstName,
                SigningAuthorityLastName = collectorModel.TermsAndConditions.SigningAuthorityLastName,
                SigningAuthorityPosition = collectorModel.TermsAndConditions.SigningAuthorityPosition,
                FormCompletedByFirstName = collectorModel.TermsAndConditions.FormCompletedByFirstName,
                FormCompletedByLastName = collectorModel.TermsAndConditions.FormCompletedByLastName,
                FormCompletedByPhone = collectorModel.TermsAndConditions.FormCompletedByPhone,
                AgreementAcceptedByFullName = collectorModel.TermsAndConditions.AgreementAcceptedByFullName,
                AgreementAcceptedCheck = collectorModel.TermsAndConditions.AgreementAcceptedCheck,
            };

            MoveAttachmentToVendor(collectorModel, vendor);

            var vendorPrimaryAddress = new Address()
            {
                Address1 = collectorModel.BusinessLocation.BusinessAddress.AddressLine1,
                Address2 = collectorModel.BusinessLocation.BusinessAddress.AddressLine2,
                City = collectorModel.BusinessLocation.BusinessAddress.City,
                Province = collectorModel.BusinessLocation.BusinessAddress.Province,
                PostalCode = collectorModel.BusinessLocation.BusinessAddress.Postal,
                Country = (!string.IsNullOrEmpty(collectorModel.BusinessLocation.BusinessAddress.Country) ? collectorModel.BusinessLocation.BusinessAddress.Country : "Canada"),
                Phone = collectorModel.BusinessLocation.Phone,
                Email = collectorModel.BusinessLocation.Email,
                AddressType = (int)AddressTypeEnum.Business,
            };

            vendor.VendorAddresses.Add(vendorPrimaryAddress);

            if (!(collectorModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (collectorModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(collectorModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = new Address
                {
                    Address1 = collectorModel.BusinessLocation.MailingAddress.AddressLine1,
                    Address2 = collectorModel.BusinessLocation.MailingAddress.AddressLine2,
                    City = collectorModel.BusinessLocation.MailingAddress.City,
                    Province = collectorModel.BusinessLocation.MailingAddress.Province,
                    PostalCode = collectorModel.BusinessLocation.MailingAddress.Postal,
                    Country = (!string.IsNullOrEmpty(collectorModel.BusinessLocation.MailingAddress.Country) ? collectorModel.BusinessLocation.MailingAddress.Country : "Canada"),
                    Phone = collectorModel.BusinessLocation.Phone,
                    Email = collectorModel.BusinessLocation.Email,
                    AddressType = (int)AddressTypeEnum.Mail,
                };

                vendor.VendorAddresses.Add(vendorMailingAddress);
            }

            var firstVendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
            if (firstVendorAddress != null)
            {
                for (var i = 0; i < collectorModel.ContactInformationList.Count; i++)
                {
                    if (collectorModel.ContactInformationList[i].ContactInformation.FirstName != null &&
                        collectorModel.ContactInformationList[i].ContactInformation.LastName != null)
                    {
                        var contact = new Contact()
                        {
                            FirstName = collectorModel.ContactInformationList[i].ContactInformation.FirstName,
                            LastName = collectorModel.ContactInformationList[i].ContactInformation.LastName,
                            Position = collectorModel.ContactInformationList[i].ContactInformation.Position,
                            PhoneNumber =
                                collectorModel.ContactInformationList[i].ContactInformation.PhoneNumber,
                            Ext = collectorModel.ContactInformationList[i].ContactInformation.Ext,
                            AlternatePhoneNumber =
                                collectorModel.ContactInformationList[i].ContactInformation.AlternatePhoneNumber,
                            Email = collectorModel.ContactInformationList[i].ContactInformation.Email,
                            IsPrimary = i == 0,
                        };

                        if (!collectorModel.ContactInformationList[i].ContactAddressSameAsBusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(collectorModel.ContactInformationList[i].ContactAddress.AddressLine1))
                            {
                                var contactAddress = new Address()
                                {
                                    Address1 =
                                        collectorModel.ContactInformationList[i].ContactAddress.AddressLine1,
                                    Address2 =
                                        collectorModel.ContactInformationList[i].ContactAddress.AddressLine2,
                                    City = collectorModel.ContactInformationList[i].ContactAddress.City,
                                    Province = collectorModel.ContactInformationList[i].ContactAddress.Province,
                                    PostalCode = collectorModel.ContactInformationList[i].ContactAddress.Postal,
                                    Country = string.IsNullOrWhiteSpace(collectorModel.ContactInformationList[i].ContactAddress.Country) ? "Canada" : collectorModel.ContactInformationList[i].ContactAddress.Country,
                                    AddressType = (int)AddressTypeEnum.Contact,
                                };
                                contactAddress.Contacts.Add(contact);
                                vendor.VendorAddresses.Add(contactAddress);
                            }
                            else
                            {
                                firstVendorAddress.Contacts.Add(contact);
                            }
                        }
                        else
                        {
                            firstVendorAddress.Contacts.Add(contact);
                        }
                    }
                }
            }

            //vendoractivehistory
            var vendorActiveHistory = new VendorActiveHistory()
            {
                ActiveState = true,
                ActiveStateChangeDate = DateTime.Now.Date,
                VendorID = vendor.ID,
                CreateDate = DateTime.UtcNow,
                IsValid = true
            };

            var currentUser = TMSContext.TMSPrincipal.Identity;
            vendorActiveHistory.CreatedBy = currentUser.Name;

            vendor.VendorActiveHistories.Add(vendorActiveHistory);

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            foreach (var item in collectorModel.TireDetails.TireDetailsItemType)
            {
                if (item.isChecked)
                {
                    var result = defaultItems.Single(s => s.ID == item.ID);
                    if (result != null)
                    {
                        vendor.Items.Add(result);
                    }
                }
            }

            AddUsedTiresToVendorInventory(collectorModel, app, vendor);

            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                applicationRepository.UpdateApplicationEntity(app);
                vendor.Number = vendorRepository.GetVendorRegistrationNumber(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue).ToString();//OTSTM2-508
                vendorRepository.AddVendor(vendor);

                //create vendor - GroupRate mapping
                this.configurationsRepository.ApproveVendortoGroup(collectorModel.rateGroupId, collectorModel.SelectedVendorGroupId, vendor.ID, TreadMarksConstants.PickupGroup, collectorModel.approvedByUserId);

                //Add VendorAttachments
                var vendorAttachments = new List<VendorAttachment>();
                app.Attachments.ToList().ForEach(c =>
                {
                    var vendorAttachment = new VendorAttachment();
                    vendorAttachment.AttachmentId = c.ID;
                    vendorAttachment.VendorId = vendor.ID;
                    vendorAttachments.Add(vendorAttachment);
                });
                vendorRepository.AddVendorAttachments(vendorAttachments);
                vendorRepository.UpdateVendorKeywords(vendor);
                //Update applicationnote
                applicationRepository.UpdateApplicatioNoteWithVendorId(vendor.ID, app.ID);


                //Initialize Claim for approved application
                //ET-37 Stop creating claims
                //Uncommenting for YS-6
                claimsRepository.InitializeClaimForVendor(vendor.ID);

                transationScope.Complete();
            }

            return vendor.ID;
        }

        public int UpdateApproveApplication(CollectorRegistrationModel collectorModel, int vendorId)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var deleteAddresses = new List<Address>();
            var newContactAddresses = new List<ContactAddressDto>();
            var deleteContacts = new List<Contact>(); //OTSTM2-50

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                if (!(vendor.GpUpdate ?? false))
                {
                    var participantExisting = new GpParticipantUpdate(vendor);
                    var newContact = collectorModel.ContactInformationList.First();
                    var participantNew = new GpParticipantUpdate()
                    {
                        MailingAddress1 = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.AddressLine1 : collectorModel.BusinessLocation.MailingAddress.AddressLine1,
                        MailingAddress2 = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.AddressLine2 : collectorModel.BusinessLocation.MailingAddress.AddressLine2,
                        MailingCity = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.City : collectorModel.BusinessLocation.MailingAddress.City,
                        MailingCountry = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.Country : collectorModel.BusinessLocation.MailingAddress.Country,
                        MailingPostalCode = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.Postal : collectorModel.BusinessLocation.MailingAddress.Postal,
                        MailingProvince = collectorModel.BusinessLocation.MailingAddressSameAsBusiness ? collectorModel.BusinessLocation.BusinessAddress.Province : collectorModel.BusinessLocation.MailingAddress.Province,
                        PrimaryContactName = string.Format("{0} {1}", newContact.ContactInformation.FirstName, newContact.ContactInformation.LastName),
                        PrimaryContactPhone = newContact.ContactInformation.PhoneNumber ?? string.Empty,
                        IsTaxExempt = collectorModel.CollectorDetails.IsTaxExempt,
                        IsActive = vendor.IsActive
                    };
                    if (!participantExisting.Equals(participantNew))
                    {
                        vendor.GpUpdate = true;
                    }
                }
                vendor.BusinessName = collectorModel.BusinessLocation.BusinessName;
                vendor.OperatingName = collectorModel.BusinessLocation.OperatingName;
                vendor.BusinessStartDate = collectorModel.CollectorDetails.BusinessStartDate;
                vendor.BusinessNumber = collectorModel.CollectorDetails.BusinessNumber;
                vendor.CommercialLiabHSTNumber = collectorModel.CollectorDetails.CommercialLiabHstNumber;
                vendor.IsTaxExempt = collectorModel.CollectorDetails.IsTaxExempt;
                vendor.CommercialLiabInsurerName = collectorModel.CollectorDetails.CommercialLiabInsurerName;
                vendor.CommercialLiabInsurerExpDate = collectorModel.CollectorDetails.CommercialLiabInsurerExpDate;
                vendor.HasMoreThanOneEmp = collectorModel.CollectorDetails.HasMoreThanOneEmp;
                vendor.WSIBNumber = collectorModel.CollectorDetails.WsibNumber;
                //vendor.CIsGenerator = collectorModel.CollectorDetails.GeneratorStatus; //comment out for OTSTM2-953
                //vendor.CGeneratorDate = DateTime.UtcNow; //comment out for OTSTM2-953
                vendor.VendorType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue;
                vendor.LastUpdatedBy = updatedBy;
                vendor.PrimaryBusinessActivity = collectorModel.CollectorDetails.BusinessActivity;
                vendor.BusinessActivityID = (int)BusinessActivityTypeEnum.Collector;
                vendor.LastUpdatedDate = DateTime.UtcNow;
                vendor.ContactInfo = string.Format("{0} {1}, {2}", collectorModel.ContactInformationList[0].ContactInformation.FirstName,
                                collectorModel.ContactInformationList[0].ContactInformation.LastName, collectorModel.ContactInformationList[0].ContactInformation.PhoneNumber);

                //added
                vendor.SigningAuthorityFirstName = collectorModel.TermsAndConditions.SigningAuthorityFirstName;
                vendor.SigningAuthorityLastName = collectorModel.TermsAndConditions.SigningAuthorityLastName;
                vendor.SigningAuthorityPosition = collectorModel.TermsAndConditions.SigningAuthorityPosition;
                vendor.FormCompletedByFirstName = collectorModel.TermsAndConditions.FormCompletedByFirstName;
                vendor.FormCompletedByLastName = collectorModel.TermsAndConditions.FormCompletedByLastName;
                vendor.FormCompletedByPhone = collectorModel.TermsAndConditions.FormCompletedByPhone;
                vendor.AgreementAcceptedByFullName = collectorModel.TermsAndConditions.AgreementAcceptedByFullName;
                vendor.AgreementAcceptedCheck = collectorModel.TermsAndConditions.AgreementAcceptedCheck;

                //Save vendor supporting document
                UpdateVendorSupportingDocuments(collectorModel, vendor);
            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                vendorPrimaryAddress.Address1 = collectorModel.BusinessLocation.BusinessAddress.AddressLine1;
                vendorPrimaryAddress.Address2 = collectorModel.BusinessLocation.BusinessAddress.AddressLine2;
                vendorPrimaryAddress.City = collectorModel.BusinessLocation.BusinessAddress.City;
                vendorPrimaryAddress.Province = collectorModel.BusinessLocation.BusinessAddress.Province;
                vendorPrimaryAddress.PostalCode = collectorModel.BusinessLocation.BusinessAddress.Postal;
                vendorPrimaryAddress.Country = collectorModel.BusinessLocation.BusinessAddress.Country;
                vendorPrimaryAddress.Phone = collectorModel.BusinessLocation.Phone;
                vendorPrimaryAddress.Email = collectorModel.BusinessLocation.Email;
                vendorPrimaryAddress.Ext = collectorModel.BusinessLocation.Extension;
                vendorPrimaryAddress.AddressType = (int)AddressTypeEnum.Business;
            }

            if (!(collectorModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (collectorModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(collectorModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);

                if (vendorMailingAddress != null)
                {
                    vendorMailingAddress.Address1 = collectorModel.BusinessLocation.MailingAddress.AddressLine1;
                    vendorMailingAddress.Address2 = collectorModel.BusinessLocation.MailingAddress.AddressLine2;
                    vendorMailingAddress.City = collectorModel.BusinessLocation.MailingAddress.City;
                    vendorMailingAddress.Province = collectorModel.BusinessLocation.MailingAddress.Province;
                    vendorMailingAddress.PostalCode = collectorModel.BusinessLocation.MailingAddress.Postal;
                    vendorMailingAddress.Country = collectorModel.BusinessLocation.MailingAddress.Country;
                    vendorMailingAddress.Phone = collectorModel.BusinessLocation.Phone;
                    vendorMailingAddress.Email = collectorModel.BusinessLocation.Email;
                }
                else
                {
                    var mailingAddress = new Address()
                    {
                        Address1 = collectorModel.BusinessLocation.MailingAddress.AddressLine1,
                        Address2 = collectorModel.BusinessLocation.MailingAddress.AddressLine2,
                        City = collectorModel.BusinessLocation.MailingAddress.City,
                        Province = collectorModel.BusinessLocation.MailingAddress.Province,
                        PostalCode = collectorModel.BusinessLocation.MailingAddress.Postal,
                        Country = collectorModel.BusinessLocation.MailingAddress.Country,
                        Phone = collectorModel.BusinessLocation.Phone,
                        Email = collectorModel.BusinessLocation.Email,
                        AddressType = (int)AddressTypeEnum.Mail
                    };

                    vendor.VendorAddresses.Add(mailingAddress);
                }
            }

            else
            {
                //OTSTM2-476
                var currentMailingAddress = vendor.VendorAddresses.FirstOrDefault(c => c.AddressType == (int)AddressTypeEnum.Mail);

                if (currentMailingAddress != null)
                {
                    vendor.VendorAddresses.Remove(currentMailingAddress);
                }
            }

            //OTSTM2-50 remove contact
            var contactIdList = collectorModel.ContactInformationList.Select(c => c.ContactInformation.ID).ToList();
            foreach (var contactAddress in vendor.VendorAddresses.ToList())
            {
                foreach (var contact in contactAddress.Contacts.ToList())
                {
                    if (!contactIdList.Contains(contact.ID))
                    {
                        deleteContacts.Add(contact);
                    }
                }
            }

            //existing adding and updating contact
            foreach (var applicationContactInfo in collectorModel.ContactInformationList)
            {
                Contact contact = null;
                if (applicationContactInfo.isEmptyContact)
                {
                    var contactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                    contact = contactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                    contactAddress.Contacts.Remove(contact);
                    vendor.VendorAddresses.Remove(contactAddress);
                }
                else
                {
                    #region contact
                    //check if this is a new contact and address
                    if (applicationContactInfo.ContactInformation.AddressID == 0)
                    {
                        contact = new Contact() { IsPrimary = false };
                        ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        if (!applicationContactInfo.ContactInformation.ContactAddressSameAsBusiness)
                        {
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contactAddress.Contacts.Add(contact);
                            vendor.VendorAddresses.Add(contactAddress);
                        }
                        else
                        {
                            var vendorBusinessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == collectorModel.BusinessLocation.BusinessAddress.ID);
                            vendorBusinessAddress.Contacts.Add(contact);
                            vendorRepository.UpdateVendor(vendor); //OTSTM2-1037 make sure contact 1 with same as Business Address checked to be saved earlier, so that has a smaller ID value
                        }
                    }
                    else
                    {

                        var currentContactAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                        var businessAddress = vendor.VendorAddresses.FirstOrDefault(r => r.ID == collectorModel.BusinessLocation.BusinessAddress.ID);
                        var oldIsAddressSameAsBusiness = currentContactAddress.AddressType == (int)AddressTypeEnum.Business;
                        var newIsAddressSameAsBusiness = applicationContactInfo.ContactAddressSameAsBusinessAddress;
                        if (newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //no change update contact information
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                        else if (newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //changed selection to same as business
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            contact.AddressID = businessAddress.ID;
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = businessAddress
                            });
                            deleteAddresses.Add(contact.Address);
                        }
                        else if (!newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //remove from business address and add new address
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = contactAddress
                            });
                        }
                        else if (!newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //no change update address and contact information
                            ModelAdapter.UpdateAddress(ref currentContactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                    }
                    #endregion
                }
            }

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            collectorModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    if (!vendor.Items.Any(r => r.ID == s.ID))
                    {
                        var result = defaultItems.Single(p => p.ID == s.ID);
                        if (result != null)
                        {
                            vendor.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = vendor.Items.FirstOrDefault(r => r.ID == s.ID);
                    if (result != null)
                    {
                        vendor.Items.Remove(result);
                    }
                }
            });

            SaveCancelVendorInventory(collectorModel, vendor);

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //OTSTM2-239 check generator drop down
                GeneratorCheck(vendor);
                vendorRepository.RemoveAddresses(deleteAddresses);
                vendorRepository.RemoveContacts(deleteContacts); //OTSTM2-50
                vendorRepository.UpdateVendorContacts(newContactAddresses, vendor.ID);
                vendorRepository.UpdateVendor(vendor);
                //vendorRepository.UpdateVendorContacts(newContactAddresses, vendor.ID);
                vendorRepository.UpdateVendorKeywords(vendor);
                transationScope.Complete();
            }
            return vendor.ID;
        }

        //OTSTM2-239 check generator drop down
        public void GeneratorCheck(Vendor vendor)
        {
            var invitation = invitationRepository.FindInvitationByVendorId(vendor.ID);

            if (invitation != null)
            {
                var uSer = userRepository.FindUserByUserName(invitation.UserName);

                if (!string.Equals(vendor.PrimaryBusinessActivity, "Generator") && (vendor.BankInformations == null || vendor.BankInformations.Count == 0))
                {
                    //update invitation and user for redirect URL
                    invitation.RedirectUrl = string.Format(@"/System/FinancialIntegration/BankingInformation/{0}", vendor.ID);
                    invitationRepository.UpdateRedirectUrl(invitation);

                    if (uSer != null)
                    {
                        //update user
                        uSer.RedirectUserAfterLogin = true;
                        uSer.RedirectUserAfterLoginUrl = invitation.RedirectUrl;
                        userRepository.UpdateUser(uSer);
                    }
                }
                else
                {
                    invitation.RedirectUrl = null;
                    invitationRepository.UpdateRedirectUrl(invitation);

                    if (uSer != null)
                    {
                        //update user
                        uSer.RedirectUserAfterLogin = null;
                        uSer.RedirectUserAfterLoginUrl = null;
                        userRepository.UpdateUser(uSer);
                    }
                }
            }
        }

        public CollectorRegistrationModel GetApprovedApplication(int vendorId)
        {
            CollectorRegistrationModel collectorModel = new CollectorRegistrationModel();

            var vendor = vendorRepository.FindVendorByID(vendorId);
            if (vendor != null)
            {
                collectorModel.Status = vendor.RegistrantStatus == null ? ApplicationStatusEnum.Approved :
                    (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), vendor.RegistrantStatus);
                collectorModel.ActiveInactive.ApprovedDate = vendor.CreatedDate;

                if (vendor.ApplicationID.HasValue)
                {
                    var app = this.applicationRepository.FindApplicationByAppID(vendor.ApplicationID.Value);
                    collectorModel.ID = app.ID;
                    collectorModel.UserIPAddress = app.UserIP;
                    collectorModel.SubmittedDate = app.SubmittedDate;
                    //OTSTM2-973
                    collectorModel.TermsAndConditions.TermsAndConditionsID = app.TermsAndConditionsID;
                }

                collectorModel.BusinessLocation.BusinessName = vendor.BusinessName;
                collectorModel.BusinessLocation.OperatingName = vendor.OperatingName;
                collectorModel.CollectorDetails.BusinessStartDate = vendor.BusinessStartDate;
                collectorModel.CollectorDetails.BusinessNumber = vendor.BusinessNumber;
                collectorModel.CollectorDetails.CommercialLiabHstNumber = vendor.CommercialLiabHSTNumber;
                collectorModel.CollectorDetails.IsTaxExempt = vendor.IsTaxExempt;
                collectorModel.CollectorDetails.CommercialLiabInsurerName = vendor.CommercialLiabInsurerName;
                collectorModel.CollectorDetails.CommercialLiabInsurerExpDate = vendor.CommercialLiabInsurerExpDate;
                collectorModel.CollectorDetails.HasMoreThanOneEmp = vendor.HasMoreThanOneEmp;
                collectorModel.CollectorDetails.WsibNumber = vendor.WSIBNumber;
                collectorModel.CollectorDetails.GeneratorStatus = (vendor.CIsGenerator.HasValue ? vendor.CIsGenerator.Value : false);
                collectorModel.CollectorDetails.GeneratorDate = vendor.CGeneratorDate;
                collectorModel.CollectorDetails.SubCollectorStatus = (vendor.IsSubCollector.HasValue ? vendor.IsSubCollector.Value : false); //OTSTM2-953
                collectorModel.CollectorDetails.SubCollectorDate = (vendor.SubCollectorDate.HasValue ? vendor.SubCollectorDate.Value : (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue))); //OTSTM2-953
                collectorModel.CollectorDetails.BusinessActivity = vendor.PrimaryBusinessActivity;
                collectorModel.RegistrationNumber = Convert.ToInt32(vendor.Number);

                collectorModel.TermsAndConditions.SigningAuthorityFirstName = vendor.SigningAuthorityFirstName;
                collectorModel.TermsAndConditions.SigningAuthorityLastName = vendor.SigningAuthorityLastName;
                collectorModel.TermsAndConditions.SigningAuthorityPosition = vendor.SigningAuthorityPosition;
                collectorModel.TermsAndConditions.FormCompletedByFirstName = vendor.FormCompletedByFirstName;
                collectorModel.TermsAndConditions.FormCompletedByLastName = vendor.FormCompletedByLastName;
                collectorModel.TermsAndConditions.FormCompletedByPhone = vendor.FormCompletedByPhone;
                collectorModel.TermsAndConditions.AgreementAcceptedByFullName = vendor.AgreementAcceptedByFullName;
                collectorModel.TermsAndConditions.AgreementAcceptedCheck = vendor.AgreementAcceptedCheck ?? true;
                collectorModel.CreatedDate = (null == vendor.ConfirmationMailedDate) ? (vendor.CreatedDate) : ((vendor.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (vendor.ActivationDate ?? DateTime.MinValue) : (vendor.ConfirmationMailedDate ?? DateTime.MinValue));
            }

            var vendorPrimaryAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                collectorModel.BusinessLocation.BusinessAddress.AddressLine1 = vendorPrimaryAddress.Address1;
                collectorModel.BusinessLocation.BusinessAddress.AddressLine2 = vendorPrimaryAddress.Address2;
                collectorModel.BusinessLocation.BusinessAddress.City = vendorPrimaryAddress.City;
                collectorModel.BusinessLocation.BusinessAddress.Province = vendorPrimaryAddress.Province;
                collectorModel.BusinessLocation.BusinessAddress.Postal = vendorPrimaryAddress.PostalCode;
                collectorModel.BusinessLocation.BusinessAddress.Country = vendorPrimaryAddress.Country;
                collectorModel.BusinessLocation.Phone = vendorPrimaryAddress.Phone;
                collectorModel.BusinessLocation.Email = vendorPrimaryAddress.Email;
                collectorModel.BusinessLocation.Extension = vendorPrimaryAddress.Ext;
                collectorModel.BusinessLocation.BusinessAddress.ID = vendorPrimaryAddress.ID;
            }

            var vendorMailingAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
            if (vendorMailingAddress != null)
            {
                collectorModel.BusinessLocation.MailingAddress.AddressLine1 = vendorMailingAddress.Address1;
                collectorModel.BusinessLocation.MailingAddress.AddressLine2 = vendorMailingAddress.Address2;
                collectorModel.BusinessLocation.MailingAddress.City = vendorMailingAddress.City;
                collectorModel.BusinessLocation.MailingAddress.Province = vendorMailingAddress.Province;
                collectorModel.BusinessLocation.MailingAddress.Postal = vendorMailingAddress.PostalCode;
                collectorModel.BusinessLocation.MailingAddress.Country = vendorMailingAddress.Country;
                collectorModel.BusinessLocation.MailingAddress.ID = vendorMailingAddress.ID;
                collectorModel.BusinessLocation.MailingAddressSameAsBusiness = false;
            }
            else
            {
                collectorModel.BusinessLocation.MailingAddressSameAsBusiness = true;
            }

            foreach (var contactAddress in vendor.VendorAddresses)
            {
                foreach (var contact in contactAddress.Contacts)
                {
                    ContactRegistrationModel contactRegistrationModel = new ContactRegistrationModel()
                    {
                        ContactInformation = new ContactModel()
                        {
                            ID = contact.ID,
                            AddressID = contact.AddressID,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Position = contact.Position,
                            PhoneNumber = contact.PhoneNumber,
                            Ext = contact.Ext,
                            AlternatePhoneNumber = contact.AlternatePhoneNumber,
                            Email = contact.Email,
                            IsPrimary = (contact.IsPrimary.HasValue ? contact.IsPrimary.Value : false)
                        },

                        ContactAddress = new ContactAddressModel()
                        {
                            AddressLine1 = contact.Address.Address1,
                            AddressLine2 = contact.Address.Address2,
                            City = contact.Address.City,
                            Province = contact.Address.Province,
                            Postal = contact.Address.PostalCode,
                            Country = contact.Address.Country,
                        },
                        ContactAddressSameAsBusinessAddress = (collectorModel.BusinessLocation.BusinessAddress.ID == contact.AddressID)
                    };

                    collectorModel.ContactInformationList.Add(contactRegistrationModel);
                }
            }

            collectorModel.ContactInformationList = collectorModel.ContactInformationList.OrderBy(c => c.ContactInformation.ID).ToList();

            collectorModel.TireDetails = new CollectorTireDetailsRegistrationModel();
            collectorModel.TireDetails.TireDetailsItemType = new List<TireDetailsItemTypeModel>();

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            defaultItems.ToList().ForEach(s =>
            {
                bool isChecked = false;

                if (vendor.Items.Any(r => r.ID == s.ID))
                {
                    isChecked = true;
                }

                TireDetailsItemTypeModel tireDetailsItemTypeModel = new TireDetailsItemTypeModel()
                {
                    ID = s.ID,
                    Description = s.Description,
                    Name = s.Name,
                    ShortName = s.ShortName,
                    isChecked = isChecked,
                    IsActive = s.IsActive,
                    ItemTypeID = s.ItemType,
                };

                collectorModel.TireDetails.TireDetailsItemType.Add(tireDetailsItemTypeModel);
            });

            var result = vendor.VendorInventories.Join(defaultItems, a => a.ItemId, b => b.ID, (a, b) => a).ToList();

            foreach (var usedTiresInStorage in result)
            {
                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.PLT)
                {
                    collectorModel.TireDetails.PLT = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.MT)
                {
                    collectorModel.TireDetails.MT = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.AGLS)
                {
                    collectorModel.TireDetails.AGLS = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.IND)
                {
                    collectorModel.TireDetails.IND = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.SOTR)
                {
                    collectorModel.TireDetails.SOTR = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.MOTR)
                {
                    collectorModel.TireDetails.MOTR = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.LOTR)
                {
                    collectorModel.TireDetails.LOTR = usedTiresInStorage.Qty.ToString();
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.GOTR)
                {
                    collectorModel.TireDetails.GOTR = usedTiresInStorage.Qty.ToString();
                }
            }

            collectorModel.SupportingDocuments = new SupportingDocumentsRegistrationModel();
            foreach (var supportingDocument in vendor.VendorSupportingDocuments)
            {
                SupportingDocumentTypeEnum supportingDocumentType;
                if (Enum.TryParse(supportingDocument.TypeID.ToString(), out supportingDocumentType))
                {
                    switch (supportingDocumentType)
                    {
                        case SupportingDocumentTypeEnum.MBL:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionMBL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CLI:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionCLI = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.HST:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionHST = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.WSIB:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionWSIB = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CHQ:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionCHQ = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.EASR_ECA:
                            collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA = supportingDocument.Option;
                            break;
                        default:
                            break;
                    }
                }
            }

            var bankingInformation = vendor.BankInformations.FirstOrDefault();

            if (bankingInformation != null)
            {
                collectorModel.BankingInformation.BankName = bankingInformation.BankName;
                collectorModel.BankingInformation.TransitNumber = bankingInformation.TransitNumber;
                collectorModel.BankingInformation.BankNumber = bankingInformation.BankNumber;
                collectorModel.BankingInformation.AccountNumber = bankingInformation.AccountNumber;
                collectorModel.BankingInformation.EmailPaymentTransferNotification = bankingInformation.EmailPaymentTransferNotification;
                collectorModel.BankingInformation.NotifyToPrimaryContactEmail = bankingInformation.NotifyToPrimaryContactEmail;
            }

            return collectorModel;
        }

        private void AddApplicationSupportingDocuments(CollectorRegistrationModel collectorModel, Application app)
        {
            var applicationSupportingDocumentMBL = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentMBL);

            var applicationSupportingDocumentCLI = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCLI);

            var applicationSupportingDocumentHST = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentHST);

            var applicationSupportingDocumentWSIB = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentWSIB);

            var applicationSupportingDocumentCHQ = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCHQ);

            //OTSTM2-486
            var applicationSupportingDocumentEASR_ECA = new ApplicationSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA,
                TypeID = (int)SupportingDocumentTypeEnum.EASR_ECA
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentEASR_ECA);
        }

        private void MoveAttachmentToVendor(CollectorRegistrationModel collectorModel, Vendor vendor)
        {
            //Moving supporting document options
            var vendorSupportingDocumentMBL = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionMBL,
                TypeID = (int)SupportingDocumentTypeEnum.MBL
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentMBL);

            var vendorSupportingDocumentCLI = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCLI,
                TypeID = (int)SupportingDocumentTypeEnum.CLI
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCLI);

            var vendorSupportingDocumentHST = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionHST,
                TypeID = (int)SupportingDocumentTypeEnum.HST
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentHST);

            var vendorSupportingDocumentWSIB = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionWSIB,
                TypeID = (int)SupportingDocumentTypeEnum.WSIB
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentWSIB);

            var vendorSupportingDocumentCHQ = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentCHQ);

            //OTSTM2-486
            var vendorSupportingDocumentEASR_ECA = new VendorSupportingDocument()
            {
                Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA,
                TypeID = (int)SupportingDocumentTypeEnum.EASR_ECA
            };
            vendor.VendorSupportingDocuments.Add(vendorSupportingDocumentEASR_ECA);
        }

        private static void UpdateVendorSupportingDocuments(CollectorRegistrationModel collectorModel, Vendor vendor)
        {
            foreach (var supportDocOption in vendor.VendorSupportingDocuments)
            {
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.MBL)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionMBL;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CLI)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCLI;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.HST)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionHST;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.WSIB)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionWSIB;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CHQ)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionCHQ;
                }
                if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.EASR_ECA)
                {
                    supportDocOption.Option = collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA;
                }
            }
        }

        private void SaveCancelVendorInventory(CollectorRegistrationModel collectorModel, Vendor vendor)
        {
            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Collector)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            var allVendorInventories = vendor.VendorInventories.Join(defaultItems, a => a.ItemId, b => b.ID, (a, b) => a).ToList();

            foreach (var usedTiresInStorage in allVendorInventories)
            {
                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.PLT)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.PLT) ? Convert.ToInt32(collectorModel.TireDetails.PLT) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.MT)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.MT) ? Convert.ToInt32(collectorModel.TireDetails.MT) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.AGLS)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.AGLS) ? Convert.ToInt32(collectorModel.TireDetails.AGLS) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.IND)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.IND) ? Convert.ToInt32(collectorModel.TireDetails.IND) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.SOTR)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.SOTR) ? Convert.ToInt32(collectorModel.TireDetails.SOTR) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.MOTR)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.MOTR) ? Convert.ToInt32(collectorModel.TireDetails.MOTR) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.LOTR)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.LOTR) ? Convert.ToInt32(collectorModel.TireDetails.LOTR) : 0);
                }

                if (usedTiresInStorage.Item.ShortName == TreadMarksConstants.GOTR)
                {
                    usedTiresInStorage.Qty = (!string.IsNullOrEmpty(collectorModel.TireDetails.GOTR) ? Convert.ToInt32(collectorModel.TireDetails.GOTR) : 0);
                }
            }
        }

        //OTSTM2-486
        private void UpdateApplicationEASRSupportingDocument(CollectorRegistrationModel collectorModel)
        {
            var application = applicationRepository.GetSingleApplication(collectorModel.ID);

            //existing collector
            if (application.FormObject != null && collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA == null)
            {
                collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA = "optionmail";
            }

            //new collector
            if (application.FormObject == null)
            {
                collectorModel.SupportingDocuments.SupportingDocumentsOptionEASR_ECA = "optionupload";
            }
        }
    }
}