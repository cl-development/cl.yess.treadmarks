﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.SystemBLL;
using CL.TMS.SystemServices;
using CL.TMS.Common.Enum;
using CL.TMS.RegistrantBLL;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.TSFSteward;

namespace CL.TMS.SystemServices
{
    public class ApplicationService : IApplicationService
    {
        private readonly ApplicationBO applicationBO;
        private RegistrantBO registrantBO;
        private IApplicationRepository applicationRepository;

        public ApplicationService(IApplicationRepository applicationRepository, IVendorRepository vendorRepository, IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationInvitationRepository applicationInvitationRepository, ITSFRemittanceRepository tsfRemittanceRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository, IClaimsRepository claimsRepository, IMessageRepository messageRepository)
        {
            this.applicationBO = new ApplicationBO(applicationRepository, vendorRepository, applicationInvitationRepository);
            this.applicationRepository = applicationRepository;
            this.registrantBO = new RegistrantBO(vendorRepository, assetRepository, appUserRepository, applicationRepository, tsfRemittanceRepository, applicationInvitationRepository, gpRepository, settingRepository, claimsRepository, messageRepository);
        }

        public Application GetByApplicationID(int ID)
        {
            try
            {
                return applicationBO.GetApplicationByID(ID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public Application AddApplication(Application application)
        {
            applicationBO.AddApplication(application);
            return application;
        }

        public Application GetSingleApplication(int id)
        {
            try
            {
                return applicationBO.GetSingleApplication(id);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0)
        {
            try
            {
                this.registrantBO.SetStatus(applicationId, applicationStatus, denyReasons, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            try
            {
                return this.applicationBO.GetApprovedApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public IEnumerable<ProcessorListModel> GetProcessorList()
        {
            try
            {
                return applicationBO.GetProcessorList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateApplicatioExpireDate(int applicationId, int days)
        {
            try
            {
                applicationBO.UpdateApplicatioExpireDate(applicationId, days);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
    }
}
