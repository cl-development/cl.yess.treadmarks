﻿using CL.TMS.ServiceContracts.SystemServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.SystemBLL;
using CL.TMS.IRepository.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.IRepository.Claims;
using CL.TMS.DataContracts.ViewModel.Announcement;

namespace CL.TMS.SystemServices
{
    public class MessageService : IMessageService
    {
        private readonly MessageBO messageBO;
        public MessageService(IMessageRepository messageRepository, IClaimsRepository claimRepository)
        {
            messageBO = new MessageBO(messageRepository, claimRepository);
        }

        #region Notification
        public PaginationDTO<NotificationViewModel, int> LoadAllNotifications(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string userName)
        {
            try
            {
                return messageBO.LoadAllNotifications(pageIndex, pageSize, searchText, orderBy, sortDirection, userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<Notification> LoadUnreadNotifications(string userName)
        {
            try
            {
                return messageBO.LoadUnreadNotifications(userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddNotification(Notification notification)
        {
            try
            {
                messageBO.AddNotification(notification);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateNotificationStatus(int notificationId, string newStatus)
        {
            try
            {
                messageBO.UpdateNotificationStatus(notificationId, newStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void MarkAllAsRead(string userName)
        {
            try
            {
                messageBO.MarkAllAsRead(userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public int TotalUnreadNotification(string userName)
        {
            try
            {
                return messageBO.TotalUnreadNotification(userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return 0;
        }

        public int TotalNotification(string userName)
        {
            try
            {
                return messageBO.TotalNotification(userName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return 0;
        }

        public NotificationRedirectionViewModel RedirectToClaimSummaryPage(int notificationId)
        {
            try
            {
                return messageBO.RedirectToClaimSummaryPage(notificationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<AllNotificationsExportModel> GetAllNotificationsExport(string userName, string searchText, string sortColumn, string sortDirection)
        {
            try
            {
                return messageBO.GetAllNotificationsExport(userName, searchText, sortColumn, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region Activity
        public void AddActivity(Activity activity)
        {
            try
            {
                messageBO.AddActivity(activity);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            try
            {
                return messageBO.LoadAllActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllCommonActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectId, string ActivityArea, int ActivityType)
        {
            try
            {
                return messageBO.LoadAllCommonActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, objectId, ActivityArea, ActivityType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<ActivityViewModel, int> LoadRecoverableMaterialActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectID)
        {
            try
            {
                return messageBO.LoadRecoverableMaterialActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, objectID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public PaginationDTO<ActivityViewModel, int> LoadRateGroupsActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int activityType)
        {
            try
            {
                return messageBO.LoadRateGroupsActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, activityType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<AllActivitiesExportModel> GetAllActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection)
        {
            try
            {
                return messageBO.GetAllActivitiesExport(cid, searchText, sortColumn, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<AllActivitiesExportModel> GetAllCommonActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection, string activityArea, int activityType)
        {
            try
            {
                return messageBO.GetAllCommonActivitiesExport(cid, searchText, sortColumn, sortDirection, activityArea, activityType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public List<AllActivitiesExportModel> GetProductCompositionActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            try
            {
                return messageBO.GetProductCompositionActivitiesExport(searchText, sortColumn, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public int TotalActivity(int claimid)
        {
            try
            {
                return messageBO.TotalActivity(claimid);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return 0;
        }

        //OTSTM2-745
        public PaginationDTO<ActivityViewModel, int> LoadAllGPActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return messageBO.LoadAllGPActivities(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        //OTSTM2-745
        public List<AllActivitiesExportModel> GetAllGPActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            try
            {
                return messageBO.GetAllGPActivitiesExport(searchText, sortColumn, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion

        #region // Announcement
        public void AddAnnouncement(AnnouncementVM vm)
        {
            try
            {                
                messageBO.AddAnnouncement(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void UpdateAnnouncement(AnnouncementVM vm)
        {
            try
            {
                messageBO.UpdateAnnouncement(vm);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
        public void RemoveAnnouncement(int announcementID)
        {
            try
            {
                messageBO.RemoveAnnouncement(announcementID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public AnnouncementVM LoadAnnouncementByID(int announcementID)
        {
            try
            {
                return messageBO.LoadAnnouncementByID(announcementID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }
        public PaginationDTO<AnnouncementListVM, int> LoadAnnouncementList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return messageBO.LoadAnnouncementList(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        public IEnumerable<AnnouncementVM> LoadDashboardAnnouncements()
        {
            try
            {
                return messageBO.LoadDashboardAnnouncements();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion
    }
}
