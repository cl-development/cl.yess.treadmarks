﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Common
{
    /// <summary>
    /// An Enum helper to convert a string to enum
    /// </summary>
    public static class EnumHelper
    {
        #region Public Methods
        /// <summary>
        /// Convert a string to a enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Convert a string to a enum with default value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T ToEnum<T>(this string value, T defaultValue) where T : struct
        {
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            T result;
            return Enum.TryParse(value, true, out result) ? result : defaultValue;
        }

        /// <summary>
        /// Get enum description
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString().Trim());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var value = (attributes.Length > 0) ? ((DescriptionAttribute)attributes[0]).Description : null;

            return value; 
        }
        /// <summary>
        /// Get enum from description
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
        }
        #endregion
    }
}
