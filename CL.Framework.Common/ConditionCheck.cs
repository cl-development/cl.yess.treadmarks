﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Common
{
    /// <summary>
    /// An utility class used to write better reading code when checking preconditions on method arguments.
    /// </summary>
    public static class ConditionCheck
    {
        #region Fields
        public static readonly string OutOfBounds = "{0} was {1}. The value must be less than {2} and greater than {3}.";
        public static readonly string RequiredField = "A value must be specified for {0}.";

        /// <summary>
        /// String that indicates that a list does not have the right number of elements.
        /// </summary>
        private const string SizeSame = "{0} contains {1} elements but must contain {2}.";
        #endregion

        #region Public Methods
        /// <summary>
        /// Throws an <see cref="ArgumentOutOfRangeException"/> if the value is out of bounds. />
        /// </summary>
        /// <param name="value">The int to check</param>
        /// <param name="name">The name to put in the exception if out of bounds</param>
        /// <param name="maximum">The upper bounds</param>
        /// <param name="minimum">The lower bounds. Defaults to zero.</param>
        /// <returns></returns>
        public static int Bounds(int value, string name, int maximum, int minimum = 0)
        {
            if ((value < minimum) || (value > maximum))
                throw new ArgumentOutOfRangeException(string.Format(OutOfBounds, name, value, maximum, minimum));
            return value;
        }

        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> if the object is null.
        /// </summary>
        /// <param name="value">The object to check</param>
        /// <param name="name">The name to put in the exception if the object is null</param>
        public static T Null<T>(T value, string name) where T : class
        {
            if (value == null)
                throw new ArgumentNullException(name);
            return value;
        }      
        

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the given value is null or 
        /// empty after removing leading and trailing whitespace.
        /// </summary>
        /// <param name="value">The string that must have content</param>
        /// <param name="name">The name of the field</param>
        /// <returns>Returns the given value, if it is acceptable</returns>
        public static string NullOrEmpty(string value, string name)
        {
            if (String.IsNullOrWhiteSpace(value))
                throw new ArgumentException(string.Format(RequiredField, name));
            return value;
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the given value is empty
        /// </summary>
        /// <param name="value">The guid that must have content</param>
        /// <param name="name">The name of the field</param>
        /// <returns>Returns the given value, if it is acceptable</returns>
        public static Guid NullOrEmpty(Guid value, string name)
        {
            if (value == Guid.Empty)
                throw new ArgumentException(string.Format(RequiredField, name));
            return value;
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the two passed values are not equivalent (.Equals)
        /// </summary>
        /// <param name="value0">The first value</param>
        /// <param name="value1">The second value</param>
        /// <param name="name0">The name of the first value</param>
        /// <param name="name1">The name of the second value</param>
        /// <returns>Returns the given value, if it is acceptable</returns>
        public static bool ValueEquals<T>(T value0, T value1, string name0, string name1)
        {
            if (!value0.Equals(value1))
            {
                throw new ArgumentException(string.Format("{0} has value {1} and must be equal to {2} with value {3}.", name0, value0, name1, value1));
            }
            return true;
        }

        /// <summary>
        /// Throws an <see cref="ArgumentOutOfRangeException"/> if the list count does not match the
        /// specified count.
        /// </summary>
        /// <typeparam name="T">object type</typeparam>
        /// <param name="list">The list</param>
        /// <param name="name">The name to put in the exception if list count does not match count</param>
        /// <param name="count">The count</param>
        /// <returns>list count</returns>
        public static int SameSize<T>(IEnumerable<T> list, string name, int count)
        {
            if (list.Count() != count)
                throw new ArgumentOutOfRangeException(string.Format(SizeSame, name, list.Count(), count));
            return list.Count();
        }
        #endregion
    }
}
