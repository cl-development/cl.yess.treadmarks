﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMTireDetailsItemTypeModel
    {
        public int ID { get; set; }
        public int ItemTypeID { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TaxCode { get; set; }
        public int? DefaultUOMID { get; set; }
        public bool? IsActive { get; set; }
        public bool isChecked { get; set; }
    }
}
