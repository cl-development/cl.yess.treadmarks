﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Claims;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMPaymentViewModel
    {
        public RPMPaymentViewModel()
        {
            HSTBase = 0.13m;
        }
        public decimal SPS { get; set; }
        public decimal PaymentAdjustment { get; set; }

        public bool IsTaxApplicable { get; set; }
        public decimal HSTBase { get; set; } //set default value=13
        public decimal HST { get; set; }

        public decimal GroundTotal
        {
            get
            {
                return UsingOldRounding ? Math.Round(SPS+HST + PaymentAdjustment, 2) : Math.Round(SPS+HST + PaymentAdjustment, 2, MidpointRounding.AwayFromZero);
            }
        }

        public DateTime? PaymentDueDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public DateTime? MailedDate { get; set; }
        public string BatchNumber { get; set; }
        public bool? isStaff { get; set; }
        public bool UsingOldRounding { get; set; }

    }
}
