﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.RPM
{
    public class RPMSortYardDetailsRegistrationModel : RegistrationModelBase
    {
        public RPMSortYardDetailsRegistrationModel() 
        {
            this.SortYardAddress = new SortYardAddressModel();
        }
        public SortYardAddressModel SortYardAddress { get; set; }
        public string MaxStorageCapacity { get; set; }
        public string CertificateOfApprovalNumber { get; set; }
        public int StorageSiteType { get; set; }

    }
}
