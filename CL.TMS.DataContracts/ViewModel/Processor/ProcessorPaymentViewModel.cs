﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Processor
{
    public class ProcessorPaymentViewModel
    {
        //OTSTM2-1294
        public ProcessorPaymentViewModel()
        {
            HSTBase = 0.13m;
        }
        public decimal PTR { get; set; }
        public decimal SPS { get; set; }
        public decimal PITOutbound { get; set; }
        public decimal DOR { get; set; }
        public decimal PaymentAdjustment { get; set; }   
        //public decimal GroundTotal
        //{
        //    get
        //    {
        //        return UsingOldRounding ? Math.Round(PTR + SPS + PITOutbound + DOR + PaymentAdjustment, 2) : Math.Round(PTR + SPS + PITOutbound + DOR + PaymentAdjustment, 2, MidpointRounding.AwayFromZero);
        //    }
        //}

        //OTSTM2-1294
        public bool IsTaxApplicable { get; set; }
        public decimal HSTBase { get; set; } //set default value=13
        public decimal HST { get; set; }

        public decimal GroundTotal
        {
            get
            {

                return UsingOldRounding ? Math.Round(PTR + SPS + PITOutbound + DOR + PaymentAdjustment + HST, 2) :
                    Math.Round(PTR + SPS + PITOutbound + DOR + PaymentAdjustment + HST, 2, MidpointRounding.AwayFromZero);
            }
        }


        public DateTime? PaymentDueDate { get; set; }
        public string ChequeNumber { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public DateTime? MailedDate { get; set; }
        public string BatchNumber { get; set; }
        public bool? isStaff { get; set; }
        public bool UsingOldRounding { get; set; }
    }
}
