﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Processor
{
    public class ProcessorTireDetailsRegistrationModel : RegistrationModelBase
    {
        public IList<ProcessorTireDetailsItemTypeModel> TireDetailsItemType { get; set; }  
        public int RegistrantSubTypeID { get; set; }        
        public IList<ProcessorTireDetailsItemTypeModel> ProductsProduceType { get; set; }
        public string OtherRegistrantSubType { get; set; }
        public string OtherProcessProduct {get ; set;}
    }
}
