﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class InternalNoteViewModel
    {
        public DateTime AddedOn { get; set; }
        public string Note { get; set; }
        public string AddedBy { get; set; }
    }
}
