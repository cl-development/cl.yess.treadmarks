﻿using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class TransactionFileUploadServiceModel
    {
        public string TransactionID { get; set; }        
        public HttpPostedFileBase File { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
        public bool IsBankingRelated { get; set; }

        public AttachmentModel SaveUploadedFilesAndCreateAttachmentModels(string uploadRepositoryPath, string createdBy)
        {
            AttachmentModel attachment = null;

            try
            {
                if (this.File != null)
                {
                    string uploadAbsolutePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath(uploadRepositoryPath, this.FileUploadSectionName, this.TransactionID);

                    string uniqueFileName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(File.FileName));
                    CL.TMS.Common.Helper.FileUploadHandler.SaveUploadFile(File, uploadAbsolutePath, uniqueFileName, true);

                    attachment = new AttachmentModel()
                    {
                        FileType = File.ContentType,
                        FileName = File.FileName,
                        Description = string.Empty,
                        Size = File.ContentLength,
                        UniqueName = uniqueFileName,
                        IsValid = true,
                        CreatedBy = createdBy,
                        CreatedDate = DateTime.UtcNow,
                        FilePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadFileRelativePath(this.FileUploadSectionName, this.TransactionID, uniqueFileName),
                        IsBankingRelated = this.IsBankingRelated,
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return attachment;
        }

        public static void FillTransactionFileUploadModelFromAttachments(TransactionFileUploadModel fileUploadServiceModel, AttachmentModel attachment, string fileUploadPreviewDomainName)
        {
            fileUploadServiceModel.AttachmentId = attachment.ID;
            fileUploadServiceModel.Name = attachment.FileName;
            fileUploadServiceModel.Type = attachment.FileType;
            fileUploadServiceModel.Size = attachment.Size;
            fileUploadServiceModel.Url = Path.Combine(fileUploadPreviewDomainName, attachment.FilePath);
            fileUploadServiceModel.Description = attachment.Description;
        }       
    }
}
