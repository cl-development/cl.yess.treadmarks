﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class ProductFileUploadServiceModel
    {
        public string ProductID { get; set; }
        public HttpPostedFileBase File { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
        public bool IsBankingRelated { get; set; }

        public AttachmentModel SaveUploadedFilesAndCreateAttachmentModels(string uploadRepositoryPath, string createdBy)
        {
            AttachmentModel attachment = null;

            try
            {
                if (this.File != null)
                {
                    string uploadAbsolutePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath_Product(uploadRepositoryPath, this.FileUploadSectionName, this.ProductID);
                    string uniqueFileName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(File.FileName));
                    CL.TMS.Common.Helper.FileUploadHandler.SaveUploadFile(File, uploadAbsolutePath, uniqueFileName, true, string.Empty, TreadMarksConstants.allowedFileSize, true);
                    attachment = new AttachmentModel()
                    {
                        FileType = File.ContentType,
                        FileName = File.FileName,
                        Description = string.Empty,
                        Size = File.ContentLength,
                        UniqueName = uniqueFileName,
                        IsValid = true,
                        CreatedBy = createdBy,
                        CreatedDate = DateTime.UtcNow,
                        FilePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadFileRelativePath_Product(this.FileUploadSectionName, this.ProductID, uniqueFileName),
                        IsBankingRelated = this.IsBankingRelated,
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return attachment;
        }

        public static void FillProductionFileUploadModelFromAttachments(RcProductFileUploadModel fileUploadServiceModel, AttachmentModel attachment, string fileUploadPreviewDomainName)
        {
            fileUploadServiceModel.AttachmentId = attachment.ID;
            fileUploadServiceModel.Name = attachment.FileName;
            fileUploadServiceModel.Type = attachment.FileType;
            fileUploadServiceModel.Size = attachment.Size;
            fileUploadServiceModel.Url = Path.Combine(fileUploadPreviewDomainName, attachment.FilePath);
            fileUploadServiceModel.Description = attachment.Description;
        }       
    }
}
