﻿using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Text.RegularExpressions;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class LogoFileUploadServiceModel
    {
        //public string LogoID { get; set; }
        public HttpPostedFileBase File { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
        public bool IsBankingRelated { get; set; }

        public AttachmentModel SaveUploadedFilesAndCreateAttachmentModels(string uploadRepositoryPath, string createdBy)
        {
            AttachmentModel attachment = null;

            try
            {
                if (this.File != null)
                {              
                    string uploadAbsolutePath = Path.Combine(uploadRepositoryPath, this.FileUploadSectionName.ToString());
                    string uniqueFileName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(File.FileName));
                    CL.TMS.Common.Helper.FileUploadHandler.SaveUploadFile(File, uploadAbsolutePath, uniqueFileName, true, string.Empty, TreadMarksConstants.allowedFileSize, true);
                    attachment = new AttachmentModel()
                    {
                        FileType = File.ContentType,
                        FileName = File.FileName,
                        Description = string.Empty,
                        Size = File.ContentLength,
                        UniqueName = uniqueFileName,
                        IsValid = true,
                        CreatedBy = createdBy,
                        CreatedDate = DateTime.UtcNow,
                        FilePath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadFileRelativePath_Logo(this.FileUploadSectionName, uniqueFileName),
                        IsBankingRelated = this.IsBankingRelated,                      
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;             
            }

            return attachment;
        }  
        public static void FillLogoFileUploadModelFromAttachments(CompanyLogoFileUploadModel fileUploadServiceModel, AttachmentModel attachment, string fileUploadPreviewDomainName)
        {
            fileUploadServiceModel.AttachmentId = attachment.ID;
            fileUploadServiceModel.Name = attachment.FileName;
            fileUploadServiceModel.Type = attachment.FileType;
            fileUploadServiceModel.Size = attachment.Size;

            //for QA/UAT/Prod
            fileUploadServiceModel.Url = Path.Combine(fileUploadPreviewDomainName, attachment.FilePath);

            //for localhost
            //var array = attachment.FilePath.Split('\\');
            //fileUploadServiceModel.Url = Path.Combine(fileUploadPreviewDomainName, array[1]);

            fileUploadServiceModel.Description = attachment.Description;
        }       
    }
}
