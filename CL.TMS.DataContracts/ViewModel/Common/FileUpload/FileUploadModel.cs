﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Common.FileUpload
{
    public class FileUploadModel
    {
        public string TransactionID { get; set; }
        public bool Disable { get; set; }
        public string AddFilesURL { get; set; }
        public string GetFilesURL { get; set; }
        public string DeleteFileURL { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowSelection { get; set; }
        public bool AllowDownload { get; set; }
        public int CountOfUploadedDocuments { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
    }
}