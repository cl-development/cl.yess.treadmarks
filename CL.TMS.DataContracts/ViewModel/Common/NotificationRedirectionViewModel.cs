﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class NotificationRedirectionViewModel : BaseDTO<int>
    {
        public int ClaimId { get; set; }
        public string RegistrationNumber { get; set; }
    } 
}
