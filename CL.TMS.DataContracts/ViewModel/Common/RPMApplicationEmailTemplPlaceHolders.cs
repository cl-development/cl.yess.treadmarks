﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public static class RPMApplicationEmailTemplPlaceHolders
    {
        public static string Date { get { return "@Date"; } }
        public static string RegistrationNumber { get { return "@RegistrationNumber"; } }
        public static string BusinessName { get { return "@BusinessName"; } }
        public static string BusinessAddress1 { get { return "@BusinessAddress1"; } }
        public static string BusinessCity { get { return "@BusinessCity"; } }
        public static string BusinessProvinceState { get { return "@BusinessProvinceState"; } }
        public static string BusinessPostalCode { get { return "@BusinessPostalCode"; } }
        public static string BusinessCountry { get { return "@BusinessCountry"; } }
        public static string BusinessLegalName { get { return "@BusinessLegalName"; } }
        public static string AcceptInvitationUrl { get { return "@AcceptInvitationUrl"; } }
        public static string InvitationExpirationDateTime { get { return "@InvitationExpirationDateTime"; } }
        public static string CompanyLogo { get { return "@CompanyLogo"; } }
        //public static string TwitterLogo { get { return "@TwitterLogo"; } }
        public static string TwitterURL { get { return "@TwitterURL"; } }
        public static string FacebookURL { get { return "@FacebookURL"; } }
        public static string WebsiteURL { get { return "@WebsiteURL"; } }
        public static string ApplicationType { get { return "@ApplicationType"; } }
        public static string ApplicationLogo { get { return "@ApplicationLogo"; } }
        public static string ValidationErrorMessages { get { return "@ValidationErrorMessages"; } }
        public static string ViewApplication { get { return "@ViewApplication"; } }

        public static string NewDatetimeNowYear { get { return "@NewDatetimeNowYear"; } } //OTSTM2-609
    }
}
