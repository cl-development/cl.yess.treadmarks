﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common.Enum;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class SupportingDocumentsRegistrationModel : RegistrationModelBase
    {
        public int ID { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Description { get; set; }
        public long Size { get; set; }
        public string UniqueName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public string ValidationMessage { get; set; }
        public string RequiredDocuments { get; set; }
        public string SupportingDocumentsOptionMBL { get; set; }
        public string SupportingDocumentsOptionCLI { get; set; }
        public string SupportingDocumentsOptionCVOR { get; set; }
        public string SupportingDocumentsOptionPRL { get; set; }
        public string SupportingDocumentsOptionHST { get; set; }
        public string SupportingDocumentsOptionEASR_ECA { get; set; } //OTSTM2-486
        public string SupportingDocumentsCertificateforApproval { get; set; }
        public string SupportingDocumentsProcessorRelationshipLetter { get; set; }        
        public string SupportingDocumentsOptionWSIB { get; set; }
        public string SupportingDocumentsOptionCHQ { get; set; }
        public string SupportingDocumentsOptionCFP { get; set; }
        public string negativeAdj { get; set; }
        public int CountOfUploadedDocuments { get; set; }
        public string ParticipantType { get; set; }
        public FileUploadSectionName FileUploadSectionName { get; set; }
    }
}
