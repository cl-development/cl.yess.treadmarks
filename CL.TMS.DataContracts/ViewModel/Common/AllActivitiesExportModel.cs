﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class AllActivitiesExportModel
    {
        public int ID { get; set; }
        [Display(Name = "Date/Time")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Initiator")]
        public string Initiator { get; set; }
        [Display(Name = "Activity")]
        public string Message { get; set; }
    }
}
