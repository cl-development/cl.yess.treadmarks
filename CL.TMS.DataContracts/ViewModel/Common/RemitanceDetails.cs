﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class RemitanceDetails
    {
        public bool? SRemitsPerYear { get; set; }
        //public bool? SAudit { get; set; } //OTSTM2-1110 comment out
        public bool? SMOE { get; set; }
        //public DateTime? SAuditSwitchDate { get; set; } //OTSTM2-1110 comment out
        public DateTime? SRemitsPerYearSwitchDate { get; set; }
        public DateTime? SMOESwitchDate { get; set; }
    }
}
