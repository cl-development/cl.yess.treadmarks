﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class RemittanceEmailModel
    {
        public string BusinessLegalName { get; set; }
        public string Email { get; set; }
        public string Period { get; set; }
    }
}