﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class VendorReference
    {
        public int VendorId { get; set; }
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }

        public bool IsVendor { get; set; }

        public int VendorType { get; set; }

        public bool IsActive { get; set; }

        public DateTime? ActiveStatusChangeDate { get; set; }
        public DateTime? ActivationDate { get; set; }
        public Address Address { get; set; }
        public Contact PrimaryContact { get; set; }

        public string ActiveDateString
        {
            get
            {
                if (ActiveStatusChangeDate == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ((DateTime)ActiveStatusChangeDate).ToString("yyyy-MM-dd");
                }
            }
        }

        public string RedirectUrl { get; set; }
        public int? ApplicationID { get; set; }
        public bool? CIsGenerator { get; set; }
        public bool? IsSubCollector { get; set; } //OTSTM2-953        
        public string OperatingName { get; set; }
        public string PrimaryBusinessActivity { get; set; }
        public DateTime? CGeneratorDate { get; set; }

        public string Status { get; set; }
        public string GroupName { get; set; }
        public DateTime? CreatedDate { get; set; }

        public List<Address> VendorAddresses { get; set; }
        public ICollection<BankInformation> BankInformations { get; set; }

        public bool IsThresholdSet { get; set; }
        public int ThresholdDays { get; set; }
        public int DaysSinceLastServiced { get; set; }

    }
}
