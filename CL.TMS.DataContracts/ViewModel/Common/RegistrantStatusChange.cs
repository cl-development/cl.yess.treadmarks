﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class RegistrantStatusChange
    {           
        public DateTime RegistrantStatusChangeEffectiveDate { get; set; }
        
        public string RegistrantStatusChangeReason { get; set; }
        
        public string NewRegistrationNumber { get; set; }
        
        public string OtherReason { get; set; }
        
        public string ApprovedDate { get; set; }
        
        public ActivityStatusEnum RegistrantStatusChangeActivityStatus { get; set; }
        
        public string VendorID { get; set; }
        
        public string OtherStatus { get; set; }

        public string PropValues { get; set; }

    }
}
