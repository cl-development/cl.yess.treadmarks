﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class PageSecurityViewModel
    {
        public bool PageReadonlyForParticipant { get; set; }
        public bool PageReadonlyForStaff { get; set; }
        public bool PageReadonly { get; set; }
        
        public bool SubmitDisable { get; set; }

        public bool ApproveDisable { get; set; }

        //OTSTM2-882
        public bool StaffParticipantUserPageReadonly { get; set; }
        public bool AdminUserPageReadonly { get; set; }        
    }
}
