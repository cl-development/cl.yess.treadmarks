﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public static class RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders
    {
        public static string Date { get { return "@Date"; } }
        public static string ProductName { get { return "@ProductName"; } }
        public static string RetailerName { get { return "@RetailerName"; } }

        public static string Product { get { return "@Product"; } }
        public static string Retailer { get { return "@Retailer"; } }

        public static string SiteUrl { get { return "@siteUrl"; } }
        public static string NewDatetimeNowYear { get { return "@NewDatetimeNowYear"; } } //OTSTM2-609
        

    }
}
