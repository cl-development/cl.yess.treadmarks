﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class ActivityViewModel : BaseDTO<int>
    {
        public DateTime CreatedDate { get; set; }
        public string Message { get; set; }
        public string Initiator { get; set; }
        //public int ClaimId { get; set; }
    } 
}
