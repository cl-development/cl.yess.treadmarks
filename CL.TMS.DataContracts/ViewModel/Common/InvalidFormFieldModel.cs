﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    //ToDo: We need a good name for type "InvalidFormFieldModel"
    public abstract class InvalidFormFieldModel : BaseDTO<int>
    {
        private IList<string> invalidFormFieldList;
        public IList<string> InvalidFormFieldList
        {
            set
            {
                invalidFormFieldList = value;
            }
            get
            {
                if (invalidFormFieldList == null)
                {
                    invalidFormFieldList = new List<string>();
                }
                return invalidFormFieldList;
            }
        }
        [NotMapped]
        public ApplicationStatusEnum ValidStatus { get; set; }

    }
}
