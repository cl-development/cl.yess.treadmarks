﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Common
{
    public class TransactionItemListDetailViewModel
    {
        public int ItemID { get; set; }
        public int? Quantity { get; set; }
        public string ShortName { get; set; }
        public decimal AverageWeight { get; set; }
        public decimal ActualWeight { get; set; }
    }
}
