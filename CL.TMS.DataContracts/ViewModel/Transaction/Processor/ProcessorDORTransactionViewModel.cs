﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;

using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class ProcessorDORTransactionViewModel : ProcessorTransactionViewModel
    {
        public int? RegNumber { get; set; }
        public TransactionScaleTicketViewModel ScaleTicket { get; set; }
        public string InvoiceNumber { get; set; }
        public ProcessorDORProductViewModel ProductDetail { get; set; }
        public string SoldTo { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public List<TypeDefinition> MaterialTypeList { get; set; }
        public List<TypeDefinition> DispositionReasonList { get; set; }
        public List<int> WeightTypeList { get; set; }
        public List<string> CountryList { get; set; }
        public ProcessorRateDetailViewModel RateDetail { get; set; }

        public ProcessorDORTransactionViewModel() : base()
        {            
        }   

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.DOR;
            FormDate = new TransactionFormDateViewModel();            
            TireTypeList = new List<TransactionTireTypeViewModel>();            
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Scale Ticket", DocumentFormat = 3, Required = true },
                new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = "Invoice", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();
            ProductDetail = new ProcessorDORProductViewModel() { 
                MaterialTypeId = 1,
                OffRoadScaleWeight = new ProcessorScaleWeightViewModel() { WeightType = TreadMarksConstants.Ton }, 
                OnRoadScaleWeight = new ProcessorScaleWeightViewModel() { WeightType = TreadMarksConstants.Ton } 
            };
            ScaleTicket = new TransactionScaleTicketViewModel() { WeightType = TreadMarksConstants.Ton, WeightTypeList = new List<int> { TreadMarksConstants.Lb, TreadMarksConstants.Kg, TreadMarksConstants.Ton } };
            SoldTo = "registered";
            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "Canada" };

            CountryList = new List<string>();
            RateDetail = new ProcessorRateDetailViewModel();
        }
    }
}
