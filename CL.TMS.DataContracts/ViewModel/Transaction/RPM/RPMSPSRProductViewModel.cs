﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class RPMSPSRProductViewModel : TransactionProductBaseViewModel
    {
        public RPMSPSRProductViewModel()
        {
            UnitType = TMS.Common.TreadMarksConstants.Ton;
        }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public Decimal? Percentage { get; set; }
        public Decimal? TotalTDPWeight { get; set; }
        public int UnitType { get; set; }
        public decimal? RetailPrice { get; set; }
    }
}
