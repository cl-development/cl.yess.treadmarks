﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class PreviousScaleTicketModel
    {        
        public string FormType { get; set; }
        public VendorReference Inbound { get; set; }
        public VendorReference Outbound { get; set; }
        public string ScaleTicket { get; set; }
        public int? PeriodType { get; set; }
        public string TransactionTypeCode { get; set; }
    }
}
