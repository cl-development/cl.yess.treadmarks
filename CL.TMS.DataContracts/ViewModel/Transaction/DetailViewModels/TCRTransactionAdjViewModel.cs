﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TCRTransactionAdjViewModel : TransactionAdjViewModel
    {        
        public bool GenerateTires { get; set; }
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }        
    }
}
