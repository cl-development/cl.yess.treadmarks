﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class UCRTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public string PaymentEligible { get; set; } 
        public TransactionViewParticipantViewModel OutboundParticipant { get; set; }
        public UCRTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();
        }
    }
}
