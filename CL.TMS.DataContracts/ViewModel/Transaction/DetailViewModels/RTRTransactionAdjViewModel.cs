﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class RTRTransactionAdjViewModel : TransactionAdjViewModel
    {
        public List<TransactionTireTypeViewModel> TireTypeList { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }
        public List<string> CountryList { get; set; }
    }
}
