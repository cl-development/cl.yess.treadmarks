﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TCRTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public bool GenerateTires { get; set; }

        public TCRTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();
        }
    }
}
