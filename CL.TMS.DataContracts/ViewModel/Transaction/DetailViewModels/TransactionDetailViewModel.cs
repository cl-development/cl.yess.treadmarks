﻿using CL.TMS.DataContracts.ViewModel.Transaction.DetailViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionDetailViewModel : ITransactionDetailViewModel
    {
        public bool IsIncomplete { get; set; }

        public int Id { get; set; }
        public Guid TransactionId { get; set; }
        public string Status { get; set; }
        public string CurrentMode { get; set; }
        public bool AdjustmentAllowedInClaim { get; set; }
        public bool AdjustmentReviewAllowed { get; set; }
        public bool AdjustmentRecallAllowed { get; set; }
        public bool AdjustmentRejectAllowed { get; set; }
        public bool ActiveAdjustmentExits { get; set; }
        public bool PendingAdjustmentExits { get; set; }
        public bool DisplayOriginal { get; set; }
        public string TypeCode { get; set; }
        public int Direction { get; set; }
        public int? IncomingId { get; set; }
        public int? OutgoingId { get; set; }
        public string DisplayTypeCode { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime? TransactionEndDate { get; set; }
        public DateTime TransactionCreatedDate { get; set; }
        public long FriendlyNumber { get; set; }
        public bool IsMobile { get; set; }
        public DateTime? MobileSyncDate { get; set; }
        public string DeviceName { get; set; }
        public string Badge { get; set; }
        public decimal? RetailPrice { get; set; }
        public TransactionViewParticipantViewModel InboundParticipant { get; set; }
        public TransactionViewParticipantViewModel OutboundParticipant { get; set; }
        public List<string> CommentList { get; set; }
        public List<TransactionViewPhotoViewModel> PhotoList { get; set; }
        public TransactionViewSignatureViewModel Signature { get; set; }
        public TransactionManageInternalNoteViewModel InternalNote { get; set; }
        public TransactionAdjStatusViewModel AdjustmentDetail { get; set; }

        public ITransactionAdjViewModel AdjModel { get; set; }
        public ITransactionAdjustableDetailViewModel Original { get; set; }
        public ITransactionAdjustableDetailViewModel Adjusted { get; set; }

        public string AdjustmentNote { get; set; }

        public TransactionDetailViewModel()
        {
            CommentList = new List<string>();
            PhotoList = new List<TransactionViewPhotoViewModel>();
            InternalNote = new TransactionManageInternalNoteViewModel();
            AdjustmentDetail = null;
            //OTSTM2-660
            TransactionReviewToolBar = null;
            MobileTransactionGPS = new List<MobileTransactionGPS>();
        }
        public string OtherClaimStatus { get; set; }

        //OTSTM2-660
        public TransactionReviewToolBar TransactionReviewToolBar { get; set; }
        public string TransactionProcessingStatus { get; set; }
        public List<MobileTransactionGPS> MobileTransactionGPS { get; set; }
    }
}
