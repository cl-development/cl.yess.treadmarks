﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class SPSRTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public string InvoiceNumber { get; set; }        
        public TransactionViewProductViewModel ProductDetail { get; set; }
        public TransactionViewParticipantViewModel InboundParticipant { get; set; }

        public SPSRTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = null;
        }
    }
}
