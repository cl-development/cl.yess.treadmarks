﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class STCTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public string EventNumber { get; set; }
        public TransactionViewParticipantViewModel OutboundParticipant { get; set; }
        public STCTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();
        }
    }
}
