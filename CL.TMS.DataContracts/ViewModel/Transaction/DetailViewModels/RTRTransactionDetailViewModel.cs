﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class RTRTransactionDetailViewModel : TransactionDetailViewModel
    {
        public TransactionTireMarketDetailViewModel TireMarketInfo { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
