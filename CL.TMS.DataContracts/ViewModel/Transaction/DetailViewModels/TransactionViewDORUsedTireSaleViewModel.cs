﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewDORUsedTireSaleViewModel
    {
        public List<TransactionViewTireTypeViewModel> TireTypeList { get; set; }         
        public ProcessorCalculationSummaryViewModel CalculationSummary { get; set; }
    }
}
