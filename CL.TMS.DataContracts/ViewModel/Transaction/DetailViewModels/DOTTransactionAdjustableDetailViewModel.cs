﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class DOTTransactionAdjustableDetailViewModel : TransactionAdjustableDetailViewModel
    {
        public bool GenerateTires { get; set; }
        public TransactionViewScaleTicketDetailViewModel ScaleTicketDetail { get; set; }

        public DOTTransactionAdjustableDetailViewModel()
        {
            SupportingDocumentDetail = new TransactionSupportingDocumentDetailViewModel();
            TireTypeList = new List<TransactionViewTireTypeViewModel>();

            ScaleTicketDetail = new TransactionViewScaleTicketDetailViewModel();
        }
    }
}
