﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionViewScaleTicketDetailViewModel
    {
        public bool SingleTicketType { get; set; }
        public int WeightType { get; set; }
        public string Unit { get; set; }
        public List<TransactionViewScaleTicketViewModel> Tickets { get; set; }
        public List<string> TicketPhotos { get; set; }
        public Decimal? NetWeight { get; set; }
        public Decimal? Variance { get; set; }
        public TransactionViewScaleTicketDetailViewModel()
        {            
            TicketPhotos = new List<string>();
            Tickets = new List<TransactionViewScaleTicketViewModel>();
        }
    }

    public class TransactionViewScaleTicketViewModel
    {
        public int TicketType { get; set; }
        public string TicketNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public Decimal? InboundWeight { get; set; }
        public Decimal? OutboundWeight { get; set; }
    }
}
