﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionAdjStatusViewModel 
    {
        public string StartedBy { get; set; }
        public DateTime AdjustedOn { get; set; }
        public string Status { get; set; }
    }
}
