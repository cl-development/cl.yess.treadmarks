﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionFormDateViewModel
    {
        public DateTime? On { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }

        public List<TransactionDisabledDateRangeViewModel> DisabledDateRangeList;

        public DateTime FocusDate { get; set; }
    }

    public class TransactionDisabledDateRangeViewModel
    {        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
