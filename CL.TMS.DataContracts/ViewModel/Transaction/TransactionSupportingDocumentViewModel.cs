﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class TransactionSupportingDocumentViewModel
    {
        public int UniqueId { get; set; }
        public string DocumentName { get; set; }
        public int DocumentFormat { get; set; }
        public bool Required { get; set; }
        public int? OriginalDocumentFormat { get; set; }
    }
}
