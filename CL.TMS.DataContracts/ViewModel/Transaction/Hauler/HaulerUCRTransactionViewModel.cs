﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.Validation.Transaction;
using FluentValidation.Attributes;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class HaulerUCRTransactionViewModel : HaulerTransactionViewModel
    {
        public int? CollectorNumber { get; set; }
        public string PaymentEligible { get; set; }
        public string PaymentEligibleOther { get; set; }
        public string PickedUpFrom { get; set; }
        public TransactionUnregisteredCompanyViewModel CompanyInfo { get; set; }

        public HaulerUCRTransactionViewModel() : base()
        {
        }

        public override void initialize()
        {
            TransactionId = Guid.NewGuid();
            FormType = TreadMarksConstants.UCR;
            FormDate = new TransactionFormDateViewModel();
            TireTypeList = new List<TransactionTireTypeViewModel>();
            SupportingDocumentList = new List<TransactionSupportingDocumentViewModel>() {
                new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = "Paper Form", DocumentFormat = 3, Required = true }
            };
            UploadDocumentList = new List<TransactionFileUploadModel>();

            PaymentEligible = "0";//Please select a reason type...
            PickedUpFrom = "unregistered";
            CompanyInfo = new TransactionUnregisteredCompanyViewModel() { Country = "Canada" };
        }
    }
}
