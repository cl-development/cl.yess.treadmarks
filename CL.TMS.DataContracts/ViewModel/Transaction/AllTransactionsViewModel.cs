﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Transaction
{
    public class AllTransactionsViewModel : BaseDTO<int>, IBaseTransactionListVM
    {
        public AllTransactionsViewModel()
        {
            this.TransactionListSecurity = new TransactionListSecurityViewModel();
        }
        public string TransactionId { get; set; }
        public string Status { get; set; }
        public string iPadForm { get; set; }
        public string TransNum { get; set; }
        public string Type { get; set;}
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime? SyncAdd { get; set; }
        public string IncRegNum { get; set; }
        public string OutRegNum { get; set; }
        public string BadgeUser { get; set; }
        public string BadgeUserID { get; set; }
        public string Actions { get; set; }
        public long CreatedUserId { get; set; }
        public int CreatedVendorId { get; set; }
        public bool MobileFormat { get; set; }
        public string ClaimReviewStatus { get; set; }
        public int ClaimId { get; set; }
        public TransactionListSecurityViewModel TransactionListSecurity { get; set; }
        //public List<TransactionItem> TransactionItems { get; set; }
        public List<TransactionItemListDetailViewModel> TireCountList { get; set; } 
    }
}
