﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimWorkflowViewModel
    {
        public bool RepresentativeView { get; set; }
        public bool LeadView { get; set; }
        public bool SupervisorView { get; set; }
        public bool SupervisorWithoutApproverView { get; set; }
        public bool Approver1View { get; set; }
        public bool approver1WithoutApprover2View { get; set; }
        public bool Approver2View { get; set; }

        public bool IsApprover2BtnVisible { get; set; }
        public bool IsApprover1BtnVisible { get; set; }
        public bool IsApproverBtnVisible { get; set; }

        public bool IsRepresentativeBtnDisable { get; set; }
        public bool IsLeadBtnDisable { get; set; }
        public bool IsSupervisorBtnDisable { get; set; }
        public bool IsApprover1BtnDisable { get; set; }
        public bool IsApprover2BtnDisable { get; set; }
        public bool IsApproverBtnDisable { get; set; }

        public ClaimWorkflowStatus ClaimWorkflowStatus { get; set; }

        public string SendBackInfo { get; set; }
        public decimal? ClaimsAmountTotal { get; set; }
    }
}
