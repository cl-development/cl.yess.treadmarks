﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public abstract class ClaimModelBase
    {
        public ClaimModelBase()
        {
            ClaimSummarySecurity = new ClaimSummarySecurityViewModel();
        }
        public ClaimStatus Status { get; set; }
        public int ClaimId { get; set; }
        public int VendorId { get; set; }
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }
        public string ClaimPeriodName { get; set; }
        public Period ClaimPeriod { get; set; }
        public ClaimSummarySecurityViewModel ClaimSummarySecurity;


    }
}
