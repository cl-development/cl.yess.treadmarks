﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClawbackViewModel
    {
        public decimal MooseCreekOnRoadWeight { get; set; }
        public decimal MooseCreakOnRoadRate { get; set; }
        public decimal MooseCreakOnRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(MooseCreekOnRoadWeight * MooseCreakOnRoadRate, 2) : Math.Round(MooseCreekOnRoadWeight * MooseCreakOnRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal MooseCreekOffRoadWeight { get; set; }
        public decimal MooseCreakOffRoadRate { get; set; }
        public decimal MooseCreakOffRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(MooseCreekOffRoadWeight * MooseCreakOffRoadRate, 2) : Math.Round(MooseCreekOffRoadWeight * MooseCreakOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal MooseCreakTotal
        {
            get
            {
                return MooseCreakOnRoadTI + MooseCreakOffRoadTI;
            }
        }

        public decimal GTAOnRoadWeight { get; set; }
        public decimal GTAOnRoadRate { get; set; }
        public decimal GTAOnRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(GTAOnRoadWeight * GTAOnRoadRate, 2) : Math.Round(GTAOnRoadWeight * GTAOnRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal GTAOffRoadWeight { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal GTAOffRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(GTAOffRoadWeight * GTAOffRoadRate, 2) : Math.Round(GTAOffRoadWeight * GTAOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal GTATotal
        {
            get
            {
                return GTAOnRoadTI + GTAOffRoadTI;
            }
        }

        public decimal WTCOnRoadWeight { get; set; }
        public decimal WTCOnRoadRate { get; set; }
        public decimal WTCOnRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(WTCOnRoadWeight * WTCOnRoadRate, 2) : Math.Round(WTCOnRoadWeight * WTCOnRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal WTCOffRoadWeight { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal WTCOffRoadTI
        {
            get
            {
                return Math.Round(WTCOffRoadWeight * WTCOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal WTCTotal
        {
            get
            {
                return WTCOnRoadTI + WTCOffRoadTI;
            }
        }

        public decimal SturgeonFallsOnRoadWeight { get; set; }
        public decimal SturgeonFallsOnRoadRate { get; set; }
        public decimal SturgeonFallsOnRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(SturgeonFallsOnRoadWeight * SturgeonFallsOnRoadRate, 2) : Math.Round(SturgeonFallsOnRoadWeight * SturgeonFallsOnRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal SturgeonFallsOffRoadWeight { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
        public decimal SturgeonFallsOffRoadTI
        {
            get
            {
                return UsingOldRounding ? Math.Round(SturgeonFallsOffRoadWeight * SturgeonFallsOffRoadRate, 2) : Math.Round(SturgeonFallsOffRoadWeight * SturgeonFallsOffRoadRate, 2, MidpointRounding.AwayFromZero);
            }
        }

        public decimal SturgeonFallsTotal
        {
            get
            {
                return SturgeonFallsOnRoadTI + SturgeonFallsOffRoadTI;
            }
        }

        public decimal OnRoadTotalTI
        {
            get
            {
                return MooseCreakOnRoadTI + GTAOnRoadTI + WTCOnRoadTI + SturgeonFallsOnRoadTI;
            }
        }

        public decimal OffRoadTotalTI
        {
            get
            {
                return MooseCreakOffRoadTI + GTAOffRoadTI + WTCOffRoadTI + SturgeonFallsOffRoadTI;
            }
        }

        public decimal TotalTI
        {
            get
            {
                return OnRoadTotalTI + OffRoadTotalTI;
            }
        }

        public bool UsingOldRounding { get; set; }
    }
}
