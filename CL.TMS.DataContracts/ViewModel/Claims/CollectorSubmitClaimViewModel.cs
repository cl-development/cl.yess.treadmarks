﻿using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class CollectorSubmitClaimViewModel : SubmitClaimModelBase
    {
        public List<TireOriginItemRow> InboundList { get; set; }
        public ItemRow TotalInbound { get; set; }
        public ItemRow TotalOutbound { get; set; }
        public ItemRow TotalReuse { get; set; }
        public ItemRow TCR { get; set; }
        public ItemRow DOT { get; set; }

        //OTSTM2-588
        //public ItemRow EligibleAdjustments { get; set; }
        //public ItemRow IneligibleAdjustments { get; set; }
        public ItemRow TotalEligible { get; set; }
        public ItemRow TotalIneligible { get; set; }
        public decimal GrandTotal
        {
            get;
            set;
        }
    }
}
