﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimSummarySecurityViewModel:PageSecurityViewModel
    {
        public bool SubmitButtonDisabled { get; set; }
    }
}
