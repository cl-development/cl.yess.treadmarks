﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimCommonModel
    {
        public ClaimCommonModel()
        {
            ClaimSummarySecurity = new ClaimSummarySecurityViewModel();
            ClaimCommonHeader = new ClaimsCommonHeaderViewModel();
        }
        public ClaimStatus Status { get; set; }
        public int ClaimId { get; set; }
        public int VendorId { get; set; }
        public string RegistrationNumber { get; set; }
        public string BusinessName { get; set; }
        public string ClaimPeriodName { get; set; }
        public string UnderReview { get; set; }
        public Period ClaimPeriod { get; set; }
        
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }

        public DateTime ClaimLastCalculationTime { get; set; }

        public ClaimSummarySecurityViewModel ClaimSummarySecurity;

        public ClaimsCommonHeaderViewModel ClaimCommonHeader;
    }
}
