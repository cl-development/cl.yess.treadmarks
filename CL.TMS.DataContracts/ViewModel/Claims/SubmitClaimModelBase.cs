﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public abstract class SubmitClaimModelBase
    {
        public SubmitClaimModelBase()
        {
            Warnings = new List<string>();
            Errors = new List<string>();
        }
        public int claimId { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Warnings { get; set; }
    }
}
