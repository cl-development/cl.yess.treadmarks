﻿using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class ClaimViewModel:BaseDTO<int>
    {
        public string Status { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? SubmittedDate { get; set; }

        public DateTime? ReviewDueDate { get; set; }

        public DateTime? PaymentDue
        {
            get
            {
                //return ChequeDueDate + OnHoldDays;
                return ChequeDueDate; //OTSTM2-194
            }
        }
        public DateTime? ChequeDueDate { get; set; }
        public DateTime? ClaimOnholdDate { get; set; }
        public TimeSpan OnHoldDays
        {
            get
            {
                if (null == ClaimOnholdDate)
                {
                    return TimeSpan.Zero;
                }
                DateTime parseResult = DateTime.MinValue;
                DateTime.TryParse(this.ClaimOnholdDate.ToString(), out parseResult);
                return DateTime.UtcNow.Date - parseResult.Date;
            }
        }

        public string EftNumber { get; set; }

        public DateTime? PaymentDate { get; set; }

        public DateTime? AssignedDate { get; set; }

        public string SubmittedBy { get; set; }

        public ClaimType ClaimType { get; set; }

        public int PeriodId { get; set; }
        public string PeriodName { get; set; }

        public int VendorId { get; set; }

        public bool ClaimOnhold { get; set; }
        public bool AuditOnhold { get; set; }
        public DateTime? AuditOnholdDate { get; set; }
        public DateTime? ReviewDate {
            get
            {
                int days = 0;
                if (this.ClaimOnhold)
                {
                    days += DateTime.UtcNow.Date.Subtract(((DateTime)this.ClaimOnholdDate).Date).Days;                    
                }
                if (this.AuditOnhold)
                {
                    days += DateTime.UtcNow.Date.Subtract(((DateTime)this.AuditOnholdDate).Date).Days;                    
                }
                
                return this.ReviewDueDate.HasValue ? TimeZoneInfo.ConvertTimeFromUtc(this.ReviewDueDate.Value.AddDays(days), TimeZoneInfo.Local) : this.ReviewDueDate;
            }
        }
        

        public string AssignedTo { get; set; }

        public DateTime PeriodStartDate { get; set; }

        public bool IsClaimPost { get; set; }  //OTSTM2-484

    }
}
