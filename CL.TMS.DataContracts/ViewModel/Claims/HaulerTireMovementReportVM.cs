﻿using CL.TMS.DataContracts.ViewModel.Reporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class HaulerTireMovementReportVM: HaulerTireMovementReport
    {
        public DateTime StartDate { get; set; }
        public int ClaimId { get; set; }
    }
}
