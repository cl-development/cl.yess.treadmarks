﻿using CL.TMS.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Claims
{
    public class InventoryOpeningSummary
    {
        public decimal TotalEligibleOpeningOnRoad { get; set; }
        public decimal TotalEligibleOpeningOffRoad { get; set; }
        public decimal TotalIneligibleOpeningOnRoad { get; set; }
        public decimal TotalIneligibleOpeningOffRoad { get; set; }
        public decimal TotalOpening { get; set; }
        public decimal TotalClosing { get; set; }

        public decimal TotalEligibleOpeningOnRoadEstimated { get; set; }
        public decimal TotalEligibleOpeningOffRoadEstimated { get; set; }
        public decimal TotalIneligibleOpeningOnRoadEstimated { get; set; }
        public decimal TotalIneligibleOpeningOffRoadEstimated { get; set; }
        public decimal TotalOpeningEstimated { get; set; }
        public decimal TotalClosingEstimated { get; set; }


    }
}
