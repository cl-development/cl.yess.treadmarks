﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Roles
{
    public class ApplicationsWorkflowViewModel
    {
        public ApplicationsWorkflowViewModel(string accountName)
        {
            AccountName = accountName;
        }
        public int Id { get; set; }
        public string AccountName { get; set; }
        public bool Routing { get; set; }
        public bool ActionGear { get; set; }

    }
}
