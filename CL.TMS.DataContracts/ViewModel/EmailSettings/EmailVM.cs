﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.DataContracts.ViewModel.EmailSettings
{
    public class EmailVM
    {
        public EmailVM()
        { }

        public int ID { get; set; }
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }

        public string HeaderSection { get; set; }

        public string Body { get; set; }

        public string ButtonSection { get; set; }

        public string ButtonLabel { get; set; }

        public bool IsEnabled { get; set; }

        public int SortOrder { get; set; }
        
        public long? CreatedByID { get; set; }

        public long? ModifiedByID { get; set; }
    }
}
