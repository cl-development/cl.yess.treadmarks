﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;

namespace CL.TMS.DataContracts.ViewModel.CompanyBranding
{
    public class TermAndConditionVM
    {
        public TermAndConditionVM()
        {
            notes = new List<InternalNoteViewModel>();
        }
        public int termConditionID { get; set; }       
        public int applicationTypeID { get; set; }     
        public string applicationName { get; set; }
        public string content { get; set; }
        public DateTime effectiveStartDate { get; set; }
        public string note { get; set; }
        public List<InternalNoteViewModel> notes { get; set; }
    }
}
