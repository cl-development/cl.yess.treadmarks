﻿using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Metric
{
    public class TopTenStewardsVM
    {
        public string Steward { get; set; }
        public int StewardSubTypeID { get; set;}   
        public decimal Amount { get; set; }
        public decimal TSFPercentage { get; set; }
        public decimal ChangePercentage { get; set; }
    }
    public class TopTenStewardChartsVM
    {
        public int Year { get; set; }
        public decimal Total { get; set; }
    }
}
