﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Metric
{
    public class MetricVM 
    {        
        public IEnumerable<YearToDateVM> ytdVMs { get; set; }
        public IEnumerable<TSFStatusVM> tsfStatusVMs { get; set; }
        public IEnumerable<TSFStatusTimeClassVM> tsfStatusTimeClassVMs { get; set; }
        public IEnumerable<PaymentMethodVM> paymentMethodVMs { get; set; }
        public IEnumerable<TopTenStewardsVM> topTenTableVMs { get; set; }
        public IEnumerable<TopTenStewardChartsVM> topTenChartVMs { get; set; }
    }
}
