﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class HaulerTireCollectionReport
    {
        [Display(Name = "Claim Period")]
        public string ClaimPeriod { get; set; }
        [Display(Name = "Hauler Registration Number")]
        public string HaulerRegistrationNumber { get; set; }
        [Display(Name = "Hauler Registration Name")]
        public string HaulerRegistrationName { get; set; }
        [Display(Name = "Hauler Status")]
        public string HaulerStatus { get; set; }//Active /Inactive
        [Display(Name = "Claim State")]
        public string ClaimState { get; set; }//(Open, Submitted, Under Review, Approved)
        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }//(TCR, DOT, STC, HIT, UCR,)
        [Display(Name = "Transaction Date")]
        public DateTime TransactionDate { get; set; }//transaction start date
        [Display(Name = "Transaction #")]
        public long TransactionNbr { get; set; }
        [Display(Name = "Transaction status")]
        public string TransactionStatus { get; set; }// (underreview, approved, invalidated)
        [Display(Name = "PLT (Collected)")]
        public int PLT { get; set; }
        [Display(Name = "MT (Collected)")]
        public int MT { get; set; }
        [Display(Name = "AGLS (Collected)")]
        public int AGLS { get; set; }
        [Display(Name = "IND (Collected)")]
        public int IND { get; set; }
        [Display(Name = "SOTR (Collected)")]
        public int SOTR { get; set; }
        [Display(Name = "MOTR (Collected)")]
        public int MOTR { get; set; }
        [Display(Name = "LOTR (Collected)")]
        public int LOTR { get; set; }
        [Display(Name = "GOTR (Collected)")]
        public int GOTR { get; set; }
        [Display(Name = "PLT Weight")]
        public Nullable<decimal> PLTWeight { get; set; }
        [Display(Name = "MT Weight")]
        public Nullable<decimal> MTWeight { get; set; }
        [Display(Name = "AGLS Weight")]
        public Nullable<decimal> AGLSWeight { get; set; }
        [Display(Name = "IND Weight")]
        public Nullable<decimal> INDWeight { get; set; }
        [Display(Name = "SOTR Weight")]
        public Nullable<decimal> SOTRWeight { get; set; }
        [Display(Name = "MOTR Weight")]
        public Nullable<decimal> MOTRWeight { get; set; }
        [Display(Name = "LOTR Weight")]
        public Nullable<decimal> LOTRWeight { get; set; }
        [Display(Name = "GOTR Weight")]
        public Nullable<decimal> GOTRWeight { get; set; }
    }
}
