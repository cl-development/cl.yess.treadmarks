﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class HaulerTireMovementReport
    {
        [Display(Name = "Reporting Period")]
        public string ReportingPeriod { get; set; }
        [Display(Name = "Hauler Registration Number")]
        public string HaulerRegistrationNumber { get; set; }
        [Display(Name = "Hauler Status")]
        public string HaulerStatus { get; set; }
        [Display(Name = "Submission Number")]
        public string SubmissionNumber { get; set; }
        [Display(Name = "Record State")]
        public string RecordState { get; set; }
        [Display(Name = "Total T [Collected]")]
        public int? TotalT_Collected { get; set; }
 
        [Display(Name = "PLT [Collected]")]
        public int? PLT_Collected { get; set; }
        [Display(Name = "MT [Collected]")]
        public int? MT_Collected { get; set; }
        [Display(Name = "AG/LS [Collected]")]
        public int? AGLS_Collected { get; set; }
        [Display(Name = "IND [Collected]")]
        public int? IND_Collected { get; set; }
        [Display(Name = "SOTR [Collected]")]
        public int? SOTR_Collected { get; set; }
        [Display(Name = "MOTR [Collected]")]
        public int? MOTR_Collected { get; set; }
        //public int? MOTR_CollectedTest { get; set; }
        [Display(Name = "LOTR [Collected]")]
        public int? LOTR_Collected { get; set; }
        [Display(Name = "GOTR [Collected]")]
        public int? GOTR_Collected { get; set; }
        [Display(Name = "N Total T [Delivered]")]
        public int? NTotalT_Delivered { get; set; }
        [Display(Name = "N PLT [Delivered]")]
        public int? NPLT_Delivered { get; set; }
        [Display(Name = "N MT [Delivered]")]
        public int? NMT_Delivered { get; set; }
        [Display(Name = "N AG/LS [Delivered]")]
        public int? NAGLS_Delivered { get; set; }
        [Display(Name = "N IND [Delivered]")]
        public int? NIND_Delivered { get; set; }
        [Display(Name = "N SOTR [Delivered]")]
        public int? NSOTR_Delivered { get; set; }
        [Display(Name = "N MOTR [Delivered]")]
        public int? NMOTR_Delivered { get; set; }
        [Display(Name = "N LOTR [Delivered]")]
        public int? NLOTR_Delivered { get; set; }
        [Display(Name = "N GOTR [Delivered]")]
        public int? NGOTR_Delivered { get; set; }
        [Display(Name = "Total T [Adjustments]")]
        public int? TotalT_Adjustments { get; set; }
        [Display(Name = "PLT [Adjustments]")]
        public int? PLT_Adjustments { get; set; }
        [Display(Name = "MT [Adjustments]")]
        public int? MT_Adjustments { get; set; }
        [Display(Name = "AG/LS [Adjustments]")]
        public int? AGLS_Adjustments { get; set; }
        [Display(Name = "IND [Adjustments]")]
        public int? IND_Adjustments { get; set; }
        [Display(Name = "SOTR [Adjustments]")]
        public int? SOTR_Adjustments { get; set; }
        [Display(Name = "MOTR [Adjustments]")]
        public int? MOTR_Adjustments { get; set; }
        [Display(Name = "LOTR [Adjustments]")]
        public int? LOTR_Adjustments { get; set; }
        [Display(Name = "GOTR [Adjustments]")]
        public int? GOTR_Adjustments { get; set; }
    }
}
