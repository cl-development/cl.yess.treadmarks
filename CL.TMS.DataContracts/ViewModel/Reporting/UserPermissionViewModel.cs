﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class UserPermissionViewModel
    {
        public string Account { get; set; }
        public string Menu { get; set; }
        public string Screen { get; set; }
        public string Panel { get; set; }
        public string Workflow { get; set; }
        public bool ActionGear { get; set; }
        public int DisplayOrder { get; set; }
        public int MaxPermissionLevel { get; set; }
        public int ResourceLevel { get; set; }

        public bool IsNoAccess
        {
            get
            {
                return MaxPermissionLevel == 0;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return MaxPermissionLevel == 1;
            }
        }
        public bool IsEditSave
        {
            get
            {
                return MaxPermissionLevel ==3;
            }
        }
        public bool IsCustom
        {
            get
            {
                return MaxPermissionLevel == 2;
            }
        }

    }
}
