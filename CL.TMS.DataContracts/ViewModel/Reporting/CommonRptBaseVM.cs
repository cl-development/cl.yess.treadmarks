﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CommonRptBaseVM : BaseDTO<int>
    {
        public string BusinessName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ProvinceState { get; set; }
        public string PostalZip { get; set; }
        public string RegistrantNumber { get; set; }
        public string ReportType { get; set; }
        public List<string> ReportTypes = new List<string>(new string[] { "csv File", "Crystal Report (RPT)", "Acrobat Format (PDF)" });
    }
}
