﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresFSAProcessing
    {
        public string FromPostalCode { get; set; }
        public string ShortName { get; set; }
        public int RegionId { get; set; }
        public int Quantity { get; set; }
        public int transactionID { get; set; } //introduce this ID to avoid duplicated records in query result
    }

}
