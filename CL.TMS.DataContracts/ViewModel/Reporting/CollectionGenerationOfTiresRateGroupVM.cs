﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class CollectionGenerationOfTiresRateGroupVM
    {
        public string RateGroupName { get; set; }
        public int RateGroupId { get; set; }
        public int RateTransactionId { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public List<CollectionGenerationOfTiresVendorGroupVM> VendorGroups { get; set; }
    }
}
