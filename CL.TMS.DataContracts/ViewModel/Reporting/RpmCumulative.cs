﻿using CL.TMS.Common.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class RpmCumulative : RpmCumulativeExtended
    {
        [Display(Name = "registration_number")]
        public string RegNumber { get; set; }

        [Display(Name = "active_status")]
        public string ActiveStatus { get; set; }

        [Display(Name = "claim_period")]
        public string ClaimPeriodName { get; set; }

        [Display(Name = "claim_status")]
        public string ClaimStatus { get; set; }

        [Display(Name = "date_submitted")]
        public DateTime? ClaimSubmissionDate { get; set; }

        [Display(Name = "date_new_claim_started")]
        public DateTime? ClaimStartedDate { get; set; }

        [Display(Name = "last_date_updated")]
        public DateTime? LastDateUpdated { get; set; }

        [Display(Name = "Subtotal")] //no include tax
        public decimal? Subtotal { get; set; }

        [Display(Name = "HST")]
        public decimal? HST  { get;set;}

        [Display(Name = "amount_payable($)")] 
        public decimal? AmountPayable  { get;set; }

        [Display(Name = "opening_inventory(t)")]
        public decimal? OpeningInventory
        {
            get
            {
                return Math.Round(DataConversion.ConvertKgToTon((double)OpeningInventoryVal), 4, MidpointRounding.AwayFromZero);
            }
        }

        [Display(Name = "closing_inventory(t)")]
        public decimal? ClosingInventory
        {
            get
            {
                return Math.Round(DataConversion.ConvertKgToTon((double)ClosingInventoryVal), 4, MidpointRounding.AwayFromZero);
            }
        }

        [Display(Name = "assigned_to_name")]
        public string AssignedToName { get; set; }

        [Display(Name = "submitted_by")]
        public string SubmittedBy { get; set; }
    }

    public class RpmCumulativeExtended
    {
        //not used
        public decimal? OpeningInventoryVal
        {
            get;
            set;
        }
        public decimal? ClosingInventoryVal
        {
            get;
            set;
        }

        public bool? IsTaxApplicable { get; set; }
    }
}
