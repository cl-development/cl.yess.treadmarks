﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class RegistrantWeeklyReport
    {
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }
        [Display(Name = "Business Operating Name")]
        public string BusinessOperatingName { get; set; }
        public string DisplayName { get; set; }
        public string RegistrantTypeDesc { get; set; }
        public string BusinessDescription { get; set; }
        public string PrimaryContactName { get; set; }
        public string PrimaryContactEmail { get; set; }
        public string PrimaryContactPosition { get; set; }
        public string PrimaryContactAddress1 { get; set; }
        public string PrimaryContactAddress2 { get; set; }
        public string PrimaryContactAddress3 { get; set; }
        public string PrimaryContactCity { get; set; }
        public string PrimaryContactProvince { get; set; }
        public string PrimaryContactPostalCode { get; set; }
        public string PrimaryContactCountry { get; set; }
        public string PrimaryContactPhone { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingAddress3 { get; set; }
        public string MailingCity { get; set; }
        public string MailingProvince { get; set; }
        public string MailingPostalCode { get; set; }
        public string MailingCountry { get; set; }
        public string LocEmail { get; set; }
        public string LocAddress1 { get; set; }
        public string LocAddress2 { get; set; }
        public string LocAddress3 { get; set; }
        public string LocCity { get; set; }
        public string LocProvince { get; set; }
        public string LocPostalCode { get; set; }
        public string LocCountry { get; set; }
        public string LocPhone { get; set; }
        public string LTT { get; set; }
        public string MTT { get; set; }
        public string LST { get; set; }
        public string SLT { get; set; }
        public string SOTR { get; set; }
        public string MOTR { get; set; }
        public string LOTR { get; set; }
        public string GOTR { get; set; }
        public string RegNumber { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ActivateDate { get; set; }
        public DateTime? ConfirmationMailDate { get; set; }
        public string RegStatus { get; set; }
        public DateTime? EffectiveInactiveDate { get; set; }
        public string Reason { get; set; }
        public string NewRegNumber { get; set; }
    }
}
