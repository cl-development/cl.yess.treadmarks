﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class ProcessorDispositionOfResidual
    {
        [Display(Name = "Reporting Period")]
        public DateTime ReportingPeriod { get; set;}
        [Display(Name = "Processor Reg #")]
        public string ProcessorRegNumber { get; set;}
        [Display(Name = "Processor Legal Name")]
        public string ProcessorsLegalName { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Display(Name = "Record Status")]
        public string RecordStatus { get; set;}
        [Display(Name = "Date of Disposition")]
        public DateTime DateOfDisposition { get; set; }
        [Display(Name = "Name of Disposition Site")]
        public string NameOfDispositionSite { get; set;}
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Disposition Reason")]
        public string DispositionReason { get; set;}
        [Display(Name = "Material Type")]
        public string MaterialType { get; set;}
        [Display(Name = "Scale Ticket Invoice Number")]
        public string ScaleTicketInvoiceNumber { get; set; }
        [Display(Name = "Scale Weight (Tonnes)")]
        public decimal? ScaleWeightTonnes { get; set; }
        [Display(Name = "Total Estimated Weight")]
        public decimal? TotalEstimatedWeight { get; set; }
        [Display(Name = "Payment Adjustment")]
        public decimal? PaymentAdjustment { get; set;}
        [Display(Name = "PLT")]
        public int? PLT { get; set; }
        [Display(Name = "MT")]
        public int? MT { get; set; }
        [Display(Name = "AGLS")]
        public int? AGLS { get; set; }
        [Display(Name = "IND")]
        public int? IND { get; set; }
        [Display(Name = "SOTR")]
        public int? SOTR { get; set; }
        [Display(Name = "MOTR")]
        public int? MOTR { get; set; }
        [Display(Name = "LOTR")]
        public int? LOTR { get; set; }
        [Display(Name = "GOTR")]
        public int? GOTR { get; set; }
    }
}
