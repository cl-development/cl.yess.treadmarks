﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Reporting
{
    public class WebListingProcessorReport
    {
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        [Display(Name = "Province/State")]
        public string Province { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        public string PLT { get; set; }
        public string MT { get; set; }
        [Display(Name = "AG/LS")]
        public string AGLS { get; set; }
        public string IND { get; set; }
        public string SOTR { get; set; }
        public string MOTR { get; set; }
        public string LOTR { get; set; }
        public string GOTR { get; set; }
        [Display(Name = "Registration Number")]
        public string RegistrationNumber { get; set; }
    }
}
