﻿using System;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.DomainEntities;
using System.Web.Mvc;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class StewardDetailsRegistrationModel : RegistrationModelBase
    {
        public DateTime? BusinessStartDate { get; set; }
        public DateTime? GeneratorDate { get; set; }
        public string BusinessNumber { get; set; }
        public string CommercialLiabHstNumber { get; set; }
        public bool IsTaxExempt { get; set; }
        public string BusinessActivity { get; set; }
        public string CommercialLiabInsurerName { get; set; }
        public DateTime? CommercialLiabInsurerExpDate { get; set; }
        public string SBusinessDesc { get; set; }
     
        public bool? HasFeeToALLVendor { get; set; }

        public int RegistrantSubTypeID { get; set; }

        public IList<SelectListItem> BusinessActivites { get; set; }
        public bool? HasMoreThanOneEmp { get; set; }

        public string WsibNumber { get; set; }
    }
}
