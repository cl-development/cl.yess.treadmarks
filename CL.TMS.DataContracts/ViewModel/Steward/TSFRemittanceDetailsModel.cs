﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Web.Mvc;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
   public class TSFRemittanceDetailsModel
    {
      /// <summary>
      /// 
      /// </summary>

        public int ID { get; set; }
        public int TSFClaimID { get; set; }
        public int ItemID { get; set; }
        public int TireSupplied { get; set; }
        public int NegativeAdjustment { get; set; }
        public int CountSupplied { get; set; }
        public double TSFRate { get; set; }
        public double TSFDue { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public int AdjustmentReasonType { get; set; }
        public IList<SelectListItem> TireList { get; set; }
        public IList<SelectListItem> AdjustmentReasonTypeList { get; set; }

    }
}
