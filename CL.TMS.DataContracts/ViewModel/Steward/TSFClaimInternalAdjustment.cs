﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFClaimInternalAdjustment:BaseDTO<int>
    {
        public int InternalAdjustmentId { get; set; }
        public DateTime AdjustmentDate { get; set; }

        public string AdjustmentType
        {
            get { return EnumHelper.GetEnumDescription(TSFInternalAdjustmentType); }
        }
        public string AdjustmentBy { get; set; }

        public TSFInternalAdjustmentType TSFInternalAdjustmentType { get; set; }

        public string Note { get; set; }

        public string NotesAllText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.TSFClaimInternalAdjustmentNotes)
                    foreach (TSFClaimInternalAdjustmentNote item in this.TSFClaimInternalAdjustmentNotes)
                    {
                        if (null != item)
                        {
                            //sSum += ("- " + item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                            sSum += (item.Note.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        [JsonIgnore]
        public List<TSFClaimInternalAdjustmentNote> TSFClaimInternalAdjustmentNotes { get; set; }
    }
}
