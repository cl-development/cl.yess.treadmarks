﻿using CL.TMS.DataContracts.ViewModel.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class TSFClaimAdjustmentModalResult
    {
        public TSFClaimAdjustmentModalResult()
        {
            SelectedItem = new TSFClaimAdjustmentType();
        }
        public TSFClaimAdjustmentType SelectedItem { get; set; }

        public decimal Amount { get; set; }
        public string PaymentType { get; set; }

        public bool IsAdd { get; set; }

        public int AdjustmentId { get; set; }

        public int UnitType { get; set; }

        public string InternalNote { get; set; }

        public UserReference AdjustUser { get; set; }
    }
}
