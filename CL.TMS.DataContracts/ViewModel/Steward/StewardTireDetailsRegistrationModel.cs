﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;

namespace CL.TMS.DataContracts.ViewModel.Steward
{
    public class StewardTireDetailsRegistrationModel : RegistrationModelBase
    {
        public IList<TireDetailsItemTypeModel> TireDetailsItemType { get; set; }

        public int PLT { get; set; }
        public int MT { get; set; }
        public int AGLS { get; set; }
        public int IND { get; set; }
        public int SOTR { get; set; }
        public int MOTR { get; set; }
        public int LOTR { get; set; }
        public int GOTR { get; set; }
        public int OTR { get; set; }
        
        
            

    }
}
