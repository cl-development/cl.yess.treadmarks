﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.Registration;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorRegistrationModel : BaseDTO<long>, IBusinessLocation, IContactInformationListModel, ITermsAndConditions, ITireDetails
    {
        public CollectorRegistrationModel()
        {
            InitializeModel(0);
        }

        public CollectorRegistrationModel(int applicationId)
        {
            InitializeModel(applicationId);
        }

        private void InitializeModel(int applicationId)
        {
            this.ID = applicationId;
            InvalidFormFields = new List<string>();
            this.BusinessLocation = new BusinessRegistrationModel();
            this.ContactInformationList = new List<ContactRegistrationModel>();
            this.ContactInformation = new List<ContactInformation>();
            //this.TireDetails = new TireDetailsRegistrationModel();
            this.CollectorDetails = new CollectorDetailsRegistrationModel();
            this.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.TermsAndConditions = new TermsAndConditionsRegistrationModel();
            TireDetailsItemTypeString = new List<string>();
            this.ApplicationInvitationChecks = new ApplicationInvitationChecks();
            this.BankingInformation = new BankingInformationRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
            this.RegistrantActiveStatus = new RegistrantStatusChange();
            this.RegistrantInActiveStatus = new RegistrantStatusChange();

            ActiveInactive = new ActiveInactiveViewModel();
        }

        public int ID { get; set; }
        public int RegistrationNumber { get; set; }
        public int VendorID { get; set; }
        public ActivityStatusEnum ActivityStatus { get; set; }
        public bool IsInternal { get; set; }
        public string FormObject { get; set; }
        public string GroupName { get; set; }

        #region Panel attributes

        public BusinessRegistrationModel BusinessLocation { get; set; }

        public IList<ContactRegistrationModel> ContactInformationList { get; set; }

        public CollectorTireDetailsRegistrationModel TireDetails { get; set; }

        public CollectorDetailsRegistrationModel CollectorDetails { get; set; }

        public SupportingDocumentsRegistrationModel SupportingDocuments { get; set; }

        public TermsAndConditionsRegistrationModel TermsAndConditions { get; set; }
        public RemitanceDetails RemitanceDetail { get; set; }

        #endregion Panel attributes

        public int ServerDateTimeOffset { get; set; }
        public ApplicationInvitationChecks ApplicationInvitationChecks { get; set; }
        public IList<string> InvalidFormFields { get; set; }
        public BankingInformationRegistrationModel BankingInformation { get; set; }
        public List<ContactInformation> ContactInformation { get; set; }
        public ApplicationStatusEnum Status { get; set; }
        public RegistrantStatusChange RegistrantActiveStatus { get; set; }
        public RegistrantStatusChange RegistrantInActiveStatus { get; set; }
        public List<BusinessActivity> BusinessActivities { get; set; }
        public List<string> TireDetailsItemTypeString { get; set; }
        public string UserIPAddress { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public ActiveInactiveViewModel ActiveInactive { get; set; }
        public string GetClientStatus()
        {
            string status = "Open";
            switch (this.Status)
            {
                case ApplicationStatusEnum.Open:
                case ApplicationStatusEnum.BackToApplicant:
                    status = "Open";
                    break;

                case ApplicationStatusEnum.Submitted:
                    status = "Submitted";
                    break;

                case ApplicationStatusEnum.Assigned:
                case ApplicationStatusEnum.ReAssignToRep:
                case ApplicationStatusEnum.OffHold:
                case ApplicationStatusEnum.OnHold:
                    status = "Under Review";
                    break;

                case ApplicationStatusEnum.Approved:
                case ApplicationStatusEnum.Resend:
                case ApplicationStatusEnum.BankInformationSubmitted:
                case ApplicationStatusEnum.BankInformationApproved:
                    status = "Approved";
                    break;

                case ApplicationStatusEnum.Denied:
                    status = "Rejected";
                    break;

                case ApplicationStatusEnum.Completed:
                    status = "Completed";
                    break;
            }

            return status;
        }
        //Added
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        public DateTime CreatedDate { get; set; }
        public long? AssignToUserId { get; set; }

        public int SelectedVendorGroupId { get; set; }
        public int rateGroupId { get; set; }
        public long approvedByUserId { get; set; }
    }

    public class CollectorDetails
    {
        public Vendor CollectorDetailsVendor { get; set; }
    }

    public class BusinessLocation
    {
        public Vendor BusinessLocationVendor { get; set; }
        public Address BusinessLocationAddress { get; set; }
        public bool MailAddressSameAsBusinessAddress { get; set; }
        public Address BusinessMailingAddress { get; set; }
    }

    public class ContactInformation
    {
        public Address ContactInformationAddress { get; set; }
        public Contact ContactInformationContact { get; set; }
        public bool ContactAddressSameAsBusinessAddress { get; set; }
    }

    public class SortYardDetails
    {
        public VendorStorageSite SortYardDetailsVendorStorageSiteModel { get; set; }
        public Address SortYardDetailsAddress { get; set; }
    }

    public class TireDetails
    {
        public List<Item> TireDetailsItemType { get; set; }
        public Vendor TireDetailsVendor { get; set; }
    }

    public class HaulerDetails
    {
        public Vendor HaulerDetailsVendor { get; set; }
    }

    public class BankingInformation
    {
        public BankInformation BankInformation { get; set; }
    }

    public class SupportingDocuments
    {
        public Attachment Attachment { get; set; }
    }

    public class TermsAndConditions
    {
        public Application TermsAndConditionsApplication { get; set; }
    }

    public class ApplicationInvitationChecks
    {
        public ApplicationInvitation ApplicationInvitationCheck { get; set; }
    }
}