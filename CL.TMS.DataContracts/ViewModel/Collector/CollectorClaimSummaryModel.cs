﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Collector
{
    public class CollectorClaimSummaryModel
    {
        #region Constructor
        public CollectorClaimSummaryModel()
        {
            this.InboundList = new List<TireOriginItemRow>();
            this.SupportingDocument = new SupportingDocumentModelBase();
            this.TCR = new ItemRow();
            this.DOT = new ItemRow();
            //this.EligibleAdjustments = new ItemRow();
            //this.IneligibleAdjustments = new ItemRow();
            this.TotalEligible = new ItemRow();
            this.TotalIneligible = new ItemRow();
            this.TotalOutbound = new ItemRow();
            TotalInbound = new ItemRow();
            ReuseTire = new ReuseTiresItemRow();
            CollectorPayment = new CollectorPaymentViewModel();
            ClaimCommonModel = new ClaimCommonModel();

            //OTSTM2-588
            this.TotalAdjustmentEligible = new ItemRow();
            this.TotalAdjustmentIneligible = new ItemRow();
            this.TotalAdjustments = new ItemRow();
            this.CollectorAllowancePaymentDetail = new CollectorAllowancePaymentDetailVM();
        }
        #endregion

        #region Common
        public ClaimCommonModel ClaimCommonModel { get; set; }
        #endregion

        #region Inbound
        public List<TireOriginItemRow> InboundList { get; set; }
        public ItemRow TotalInbound { get; set; }
        #endregion

        #region Outbound
        public ItemRow TCR { get; set; }
        public ItemRow DOT { get; set; }

        //OTSTM2-588
        //public ItemRow EligibleAdjustments { get; set; }
        //public ItemRow IneligibleAdjustments { get; set; }

        public ItemRow TotalEligible { get; set; }
        public ItemRow TotalIneligible { get; set; }
        public ItemRow TotalOutbound { get; set; }

        #endregion

        //OTSTM2-588
        #region Adjustment
        public ItemRow TotalAdjustmentEligible { get; set; }
        public ItemRow TotalAdjustmentIneligible { get; set; }
        public ItemRow TotalAdjustments { get; set; }
        #endregion

        #region Reuse Tires
        public ReuseTiresItemRow ReuseTire { get; set; }
        #endregion

        #region Support Documents        
        public List<ClaimSupportingDocVM> ClaimSupportingDocVMList { get; set; }
        public SupportingDocumentModelBase SupportingDocument { get; set; }


        #endregion

        #region Payment and Adjustment
        public CollectorPaymentViewModel CollectorPayment { get; set; }
        #endregion

        #region Staff Specific
        public ClaimStatusViewModel ClaimStatus { get; set; }
        public ClaimWorkflowViewModel ClaimWorkflow { get; set; }
        #endregion

        public CollectorAllowancePaymentDetailVM CollectorAllowancePaymentDetail { get; set; }
    }
}
