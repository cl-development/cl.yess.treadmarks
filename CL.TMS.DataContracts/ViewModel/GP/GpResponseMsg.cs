﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpResponseMsg : ResponseMsgBase
    {
        public GpResponseMsg()
        {
            BatchIDs = new List<int>();            
        }
        public List<int> BatchIDs { get; set; }
    }
}
