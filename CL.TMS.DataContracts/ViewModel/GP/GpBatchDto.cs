﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpBatchDto
    {
        public int ID { get; set; }
        public int GpiBatchCount { get; set; }
        public string GpiTypeID { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string GpiStatusId { get; set; }
        public string Message { get; set; }
    }
}
