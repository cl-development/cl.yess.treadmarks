﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Common.Enum;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpParticipantDto
    {
        public GpParticipantDto()
        {
            
        }
        public bool IsVendor { get; set; }
        public string RegistrationNumber { get; set; }
        public int RegistrantTypeId { get; set; }
        public bool GpUpdate { get; set; }
        public bool InBatch { get; set; }
        public bool IsActive { get; set; }
        public bool IsTaxExempt { get; set; }
        public string RegistrantTypeGP
        {
            get { return GetRegistrantTypeGP(this.RegistrantTypeId); }
        }
        public string BusinessName { get; set; }
        public Address Address { get; set; }
        public Contact Contact { get; set; }
        public int ObjectID { get; set; }

        public List<Address> Addresses { get; set; }

        public BankInformation BankInformation { get; set; }

        private string GetRegistrantTypeGP(int registrantTypeId)
        {
            switch (registrantTypeId)
            {
                case 1:
                    return "STEWARD";
                    break;
                case 2:
                    return "COLLECTOR";
                    break;
                case 3:
                    return "HAULERS";
                    break;
                case 4:
                    return "PROCESSOR";
                    break;
                case 5:
                    return "RPM";
                    break;
                default:
                    return string.Empty;
                    break;
            }
        }
    }
}
