﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GPListViewModel : BaseDTO<int>
    {        
        public int ID { get; set; }
        public int BatchNum { get; set; }
        public int Count { get; set; }
        public DateTime Modified { get; set; }        
        public string Status { get; set; }
        public string GpiStatusId { get; set; }
        public string Message { get; set; }
        public string Actions { get; set; }
    }
}
