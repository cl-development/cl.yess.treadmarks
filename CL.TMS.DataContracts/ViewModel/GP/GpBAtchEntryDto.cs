﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GP
{
    public class GpBatchEntryDto
    {
        public int ID { get; set; }
        public string GpiTxnNumber { get; set; }
        public string GpiStatusID { get; set; }
        public string Message { get; set; }
    }
}
