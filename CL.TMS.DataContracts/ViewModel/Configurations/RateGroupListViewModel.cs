﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class RateGroupListViewModel : BaseDTO<int>
    {
        public string Name { get; set; }
        //public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateModified { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }     
        public string Category { get; set; }
        public bool IsAssociatedWithRate { get; set; }
        public string AllAssociatedGroupsText
        {
            get
            {
                string sSum = string.Empty;
                if (null != this.AssociatedGroupList)
                    foreach (string item in this.AssociatedGroupList)
                    {
                        if (null != item)
                        {
                            sSum += (item.Replace("\"", "&quot;").Replace("<", "&#060;").Replace(">", "&#062;") + " <hr>");
                        }
                    }
                return sSum;
            }
        }
        [JsonIgnore]
        public List<string> AssociatedGroupList { get; set; }
    }
}
