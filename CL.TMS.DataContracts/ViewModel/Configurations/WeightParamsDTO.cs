﻿using System;
using System.Collections.Generic;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class WeightParamsDTO
    {
        public WeightParamsDTO() {         
        }
        public long userId { get; set; }
        public string categoryName { get; set; } 
               
        //Recovery Estimated Weights 
        public RecoveryEstimatedWeightsParam REW { get; set; }

    }
   
    public class RecoveryEstimatedWeightsParam
    {
        public int claimType { get; set; }
        public string userName { get; set; }
        public int PLT { get; set; }
        public int MT { get; set; }
        public int AGLS { get; set; }
        public int IND { get; set; }
        public int SOTR { get; set; }
        public int MOTR { get; set; }
        public int LOTR { get; set; }
        public int GOTR { get; set; }
        public int[] items { get { return new int[] { PLT, MT, AGLS, IND, SOTR, MOTR, LOTR, GOTR }; } }
    }
}
