﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class DOTPremiumRateVM //PaymentType == 2
    {
        public decimal N1OffRoadRate { get; set; }
        public decimal N2OffRoadRate { get; set; }
        public decimal N3OffRoadRate { get; set; }
        public decimal N4OffRoadRate { get; set; }

        public decimal MooseCreekOffRoadRate { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
    }
}
