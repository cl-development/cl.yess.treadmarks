﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class EstimatedWeightsRateVM
    {
        public decimal PLT { get; set; }
        public decimal MT { get; set; }
        public decimal AGLS { get; set; }
        public decimal IND { get; set; }
        public decimal SOTR { get; set; }
        public decimal MOTR { get; set; }
        public decimal LOTR { get; set; }
        public decimal GOTR { get; set; }
    }
}
