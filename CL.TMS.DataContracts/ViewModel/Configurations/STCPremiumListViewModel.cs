﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class STCPremiumListViewModel : BaseDTO<int>
    {
        public string EventNumber { get; set; }
        public string Description { get; set; }
        public decimal? OnRoadRate { get; set; }
        public decimal? OffRoadRate { get; set; }
        public int TransactionID { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public string StartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public string EndDate { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}
