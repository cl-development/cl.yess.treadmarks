﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class NorthernPremiumRateVM //PaymentType == 1
    {
        #region N1 Row
        public decimal N1SturgeonFallRateOnRoad { get; set; }
        public decimal N1SturgeonFallRateOffRoad { get; set; }
        public decimal N1SouthRateOnRoad { get; set; }
        public decimal N1SouthRateOffRoad { get; set; }
        #endregion

        #region N2 Row
        public decimal N2SturgeonFallRateOnRoad { get; set; }
        public decimal N2SturgeonFallRateOffRoad { get; set; }
        public decimal N2SouthRateOnRoad { get; set; }
        public decimal N2SouthRateOffRoad { get; set; }
        #endregion


        #region N3 Row
        public decimal N3SturgeonFallRateOnRoad { get; set; }
        public decimal N3SturgeonFallRateOffRoad { get; set; }
        public decimal N3SouthRateOnRoad { get; set; }
        public decimal N3SouthRateOffRoad { get; set; }
        #endregion

        #region N4 Row
        public decimal N4SturgeonFallRateOnRoad { get; set; }
        public decimal N4SturgeonFallRateOffRoad { get; set; }
        public decimal N4SouthRateOnRoad { get; set; }
        public decimal N4SouthRateOffRoad { get; set; }
        #endregion
    }
}
