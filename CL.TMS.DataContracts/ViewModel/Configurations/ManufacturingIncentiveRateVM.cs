﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class ManufacturingIncentiveRateVM 
    {
        public decimal Calendared { get; set; }
        public decimal Extruded { get; set; }
        public decimal Molded { get; set; }
    }
}
