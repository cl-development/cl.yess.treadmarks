﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Configurations
{
    public class IneligibleInventoryPaymentRateVM //PaymentType == 3
    {
        public decimal MooseCreakOnRoadRate { get; set; }
        public decimal MooseCreakOffRoadRate { get; set; }
        public decimal GTAOnRoadRate { get; set; }
        public decimal GTAOffRoadRate { get; set; }
        public decimal WTCOnRoadRate { get; set; }
        public decimal WTCOffRoadRate { get; set; }
        public decimal SturgeonFallsOnRoadRate { get; set; }
        public decimal SturgeonFallsOffRoadRate { get; set; }
    }
}
