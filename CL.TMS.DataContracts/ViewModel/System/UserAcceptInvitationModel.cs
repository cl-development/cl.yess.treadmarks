﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Validation;
using CL.TMS.Framework.DTO;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.System
{
     [Validator(typeof(UserAcceptInvitationModelValidator))]
    public class UserAcceptInvitationModel
     {
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public Guid InviteGUID { get; set; }

        public string UserName { get; set; }

        public bool IsEmailed { get; set; }

        public DateTime InviteDate { get; set; }
        public DateTime ExpireDate { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

    }
}
