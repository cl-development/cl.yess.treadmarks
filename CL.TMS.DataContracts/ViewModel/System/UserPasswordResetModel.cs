﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Validation;
using CL.TMS.Framework.DTO;
using CL.TMS.Resources;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.System
{
     [Validator(typeof(UserPasswordResetModelValidator))]
    public class UserPasswordResetModel:BaseDTO<long>
    {
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName", ResourceType = typeof(UIResource))]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(UIResource))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(UIResource))]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }

        public string Ops { get; set; }

    }
}
