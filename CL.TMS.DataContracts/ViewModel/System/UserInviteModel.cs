﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class UserInviteModel : BaseDTO<long>
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Status { get; set; }

        public string InviteEmail { get; set; }

        public bool Inactive { get; set; }
        public bool IsLocled { get; set; }

        public DateTime InvitedDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime InviteSendDate
        {
            get; set; // ExpireDate.AddDays(-1 * TMS.Common.TreadMarksConstants.InviteExpirationDays); }//hard code value Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays"))
        }
        public string UserEmail { get; set; }
        public string RedirectUrl { get; set; }

        public string RowversionString
        {
            get
            {
                if (Rowversion == null)
                    return string.Empty;
                return Convert.ToBase64String(Rowversion);
            }
        }


        public int VendorId { get; set; }

        public string DisplayStatus { get; set; }

        public bool IsVendor { get; set; }

        public long UserId { get; set; }
    }
}
