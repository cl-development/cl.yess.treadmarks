﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CL.TMS.DataContracts.ViewModel.System
{
    public class RecoverableMaterialsVM : BaseDTO<int>
    {
        public string MaterialName { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }
        public string MaterialDescription { get; set; }
        public string Color { get; set; }
    }

}