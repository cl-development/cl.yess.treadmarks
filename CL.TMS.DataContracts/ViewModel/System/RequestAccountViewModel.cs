﻿using CL.TMS.DataContracts.Validation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [Validator(typeof(RequestAccountValidator))]
    public class RequestAccountViewModel
    {
        public string PrimaryEmail { get; set; }
        public string RegistrationNumber { get; set; }

        public string Message { get; set; }

        public bool IsReadOnly { get; set; }
    }
}
