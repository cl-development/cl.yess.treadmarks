﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Validation;
using FluentValidation.Attributes;

namespace CL.TMS.DataContracts.ViewModel.System
{
    [Validator(typeof(EditUserModelValidor))]
    public class EditUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public bool IsRead { get; set; }

        public bool IsWrite { get; set; }

        public bool IsSubmit { get; set; }

        public bool IsParticipantAdmin { get; set; }

        public string RowversionString { get; set; }
        public string Status { get; set; }
    }
}
