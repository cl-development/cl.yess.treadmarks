﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class TermsAndConditionsRegistrationModel : RegistrationModelBase
    {

        public string ParticipantType { get; set; }
        public int? TermsAndConditionsID { get; set; }
        public string TermAndConditionContent { get; set; }
        public string SigningAuthorityFirstName { get; set; }
        public string SigningAuthorityLastName { get; set; }
        public string SigningAuthorityPosition { get; set; }
        public string FormCompletedByFirstName { get; set; }
        public string FormCompletedByLastName { get; set; }
        public string FormCompletedByPhone { get; set; }
        public string AgreementAcceptedByFullName { get; set; }
        public bool? AgreementAcceptedCheck { get; set; }
        public string UserIP { get; set; }


        public Nullable<DateTime> SubmittedDate { get; set; }
    }
}
