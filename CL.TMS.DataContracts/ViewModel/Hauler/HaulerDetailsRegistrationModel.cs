﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.DataContracts.ViewModel.Hauler
{
    public class HaulerDetailsRegistrationModel : RegistrationModelBase
    {
        public DateTime? BusinessStartDate { get; set; }
        public string BusinessNumber { get; set; }
        public string CommercialLiabHstNumber { get; set; }
        public string CommercialLiabInsurerName { get; set; }
        public string FranchiseName { get; set; }
        public string OperatingName { get; set; }
        public bool IsTaxExempt { get; set; }
        public DateTime? CommercialLiabInsurerExpDate { get; set; }
        public bool? HIsGvwr{ get; set; }
        public string CvorNumber { get; set; }
        public DateTime? CvorExpiryDate { get; set; }
        public bool? HasMoreThanOneEmp { get; set; }
        public string WsibNumber { get; set; }

    }
}
