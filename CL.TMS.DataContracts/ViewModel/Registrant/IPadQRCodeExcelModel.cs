﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class IPadQRCodeExcelModel
    {
        public List<IPadFleetExcelModel> IPadFleetList { get; set; }
        public List<QRCodeExcelModel> QRCodeList { get; set; }
    }
}