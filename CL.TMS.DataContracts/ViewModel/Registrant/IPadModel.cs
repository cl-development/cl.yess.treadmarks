﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class IPadModel
    {
        public int ID { get; set; }
        public string Number { get; set; }
        public string AssignedToName { get; set; }
        public string AssignedToNumber { get; set; }
        public bool IsAssigned { get; set; }
        public bool Activated { get; set; }
    }
}
