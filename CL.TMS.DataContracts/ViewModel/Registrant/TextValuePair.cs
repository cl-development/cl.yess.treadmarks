﻿using System;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class TextValuePair
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}