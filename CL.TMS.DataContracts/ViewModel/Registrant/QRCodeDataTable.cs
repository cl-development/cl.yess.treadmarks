﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class QRCodeDataTable : DataGrid<QRCodeRow>
    {
    }

    public class QRCodeRow : Row
    {
        public int ID_l { get; set; }
        public int UID_l { get; set; }
        public string QRCodeNumber_l { get; set; }
        public string BusinessNumber_l { get; set; }
        public string BusinessName_l { get; set; }
        public string BusinessPrimaryAddress_l { get; set; }
        public string RegName_l { get; set; }
        public bool IsActivated_l { get; set; }
        public int ID_r { get; set; }
        public int UID_r { get; set; }
        public string QRCodeNumber_r { get; set; }
        public string BusinessNumber_r { get; set; }
        public string BusinessName_r { get; set; }
        public string BusinessPrimaryAddress_r { get; set; }
        public bool IsActivated_r { get; set; }
    }

}