﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.DataContracts.ViewModel.Registrant
{
    public class IPadQRCodeModelAll
    {
        public IPadFleetGrid IPadFleetList { get; set; }
       // public Grid QRCodes { get; set; }

        public static List<IPadFleetRow> IPadFleetListSample()
        {

            var list = new List<IPadFleetRow>();
            list.AddRange(new IPadFleetRow[] { 
                new IPadFleetRow() { IPadID = "OTSIPAD120", AssignedTo = "3000180", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD121", AssignedTo = "Unassigned", IsAssigned = false, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD122", AssignedTo = "3000181", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD123", AssignedTo = "3000182", IsAssigned = true, IsActivated = false },
                new IPadFleetRow() { IPadID = "OTSIPAD124", AssignedTo = "3000183", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD125", AssignedTo = "Unassigned", IsAssigned = false, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD126", AssignedTo = "3000184", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD127", AssignedTo = "3000185", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD128", AssignedTo = "3000186", IsAssigned = true, IsActivated = false },
                new IPadFleetRow() { IPadID = "OTSIPAD129", AssignedTo = "3000187", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD130", AssignedTo = "Unassigned", IsAssigned = false, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD131", AssignedTo = "3000188", IsAssigned = true, IsActivated = true },
                new IPadFleetRow() { IPadID = "OTSIPAD132", AssignedTo = "3000189", IsAssigned = true, IsActivated = true }
            });
            
            return list;
        }

        public class IPadFleetRow : Row
        {
            public string IPadID { get; set; }
            public string AssignedTo { get; set; }
            public bool IsAssigned { get; set; }
            public bool IsActivated { get; set; }
        }

        public class IPadFleetGrid : DataGrid<IPadFleetRow>
        {
            public int draw { get; set; }
            public int recordsFiltered { get; set; }
            public int recordsTotal { get; set; }
        }

    }
}