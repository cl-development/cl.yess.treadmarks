﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection.Export
{
    public class StaffRetailerExportModel
    {
        //datatable view props
        //public string RetailerId { get; set; }
        [Display(Name = "Date Added")]
        public DateTime? ApprovedDate { get; set; }
        public string Status { get; set; }
        [Display(Name = "Retailer Name")]
        public string RetailerName { get; set; }
        [Display(Name = "Added By")]
        public string AddedBy { get; set; }

        //form view props
        [Display(Name = "Chain Online")]
        public bool ChainOnline { get; set; }
        public string Url { get; set; }
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        public string Country { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Internal Note")]
        public string InternalNote { get; set; }
    }
}
