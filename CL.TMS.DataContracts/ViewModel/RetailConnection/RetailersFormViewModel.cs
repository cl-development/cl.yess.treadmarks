﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.RetailConnection
{
    public class RetailersFormViewModel
    {
        public RetailersFormViewModel()
        {
            RetailerNotes = new List<RetailerNoteViewModel>();
            CountryList = new List<string>();
        }
        public int Id { get; set; }
        public string RetailerName { get; set; }
        public bool ChainOnline { get; set; }
        public string Url { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public int? VendorId { get; set; }
        public string InternalNote { get; set; }
        public List<RetailerNoteViewModel> RetailerNotes { get; set; }
        public List<string> CountryList { get; set; }
    }
}
