﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class DOTPaymentViewModel
    {
        public decimal TotalOffRoadPremium { get; set; }
        public decimal TotalOffRoadWeight { get; set; }
        public decimal TotalPremium { get; set; }

        public List<DOTPaymentItem> RowDatas { get; set; }
        public int RowSpan { get; set; }
    }

    public class DOTPaymentItem
    {
        public int PickupGroupId { get; set; }
        public string PickupGroupName { get; set; }
        public int DeliveryGroupId { get; set; }
        public string DeliveryGroupName { get; set; }
        public decimal OffRoadRate { get; set; }

        public decimal OffRoadWeight { get; set; }

        public decimal OffRoadPremium { get; set; }

        public int RowType { get; set; }
    }
}
