﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class ProcessorTIViewModel
    {
        public ProcessorTIViewModel() {
            ProcessorTIItems = new List<ProcessorTIItem>();
        }
        public List<ProcessorTIItem> ProcessorTIItems { get; set; }
    }

    public class ProcessorTIItem
    {
        public int DeliveryGroupId { get; set; }
        public string DeliveryGroupName { get; set; }
        public decimal OnRoadRate { get; set; }
        public decimal OffRoadRate { get; set; }
    }
}
