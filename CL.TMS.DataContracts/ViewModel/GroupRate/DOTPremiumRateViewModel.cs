﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class DOTPremiumRateViewModel
    {
        public DOTPremiumRateViewModel() {
            DOTPremiumItems = new List<DOTPremiumItem>();
        }
        public List<DOTPremiumItem> DOTPremiumItems { get; set; }
    }

    public class DOTPremiumItem
    {
        public int PickupGroupId { get; set; }
        public string PickupGroupName { get; set; }
        public int DeliveryGroupId { get; set; }
        public string DeliveryGroupName { get; set; }
        public decimal OffRoadRate { get; set; }
    }
}
