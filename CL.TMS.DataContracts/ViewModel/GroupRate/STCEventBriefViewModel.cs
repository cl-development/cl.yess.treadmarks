﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class STCEventBriefViewModel
    {
        public int TransactionId { get; set; }
        public long STCNumber { get; set; }
        public string EventNumber { get; set; }
    }
}
