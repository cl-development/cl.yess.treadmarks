﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.ViewModel.GroupRate
{
    public class TransportationPremiumViewModel
    {
        public TransportationPremiumViewModel() {
            TransportationPremiumItems = new List<TransportationPremiumItem>();
        }
        public List<TransportationPremiumItem> TransportationPremiumItems { get; set; }
    }

    public class TransportationPremiumItem
    {
        public int PickupGroupId { get; set; }
        public string PickupGroupName { get; set; }
        public int DeliveryGroupId { get; set; }
        public string DeliveryGroupName { get; set; }
        public decimal OnRoadRate { get; set; }
        public decimal OffRoadRate { get; set; }
    }
}
