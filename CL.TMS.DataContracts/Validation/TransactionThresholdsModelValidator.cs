﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using FluentValidation;

namespace CL.TMS.DataContracts.Validation
{
    public class TransactionThresholdsModelValidator : AbstractValidator<TransactionThresholdsVM>
    {
        public TransactionThresholdsModelValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
                default:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
            }
        }

        private void BuildOTSValidationRules()
        {            
            RuleFor(c => c.Threshold_IndividualTireCountPLT).NotNull().When(c => c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null && c.Threshold_IndividualTireCountIND == null 
            && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountMT).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountAGLS == null && c.Threshold_IndividualTireCountIND == null
            && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountAGLS).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountIND == null
            && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountIND).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null
            && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountSOTR).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null
            && c.Threshold_IndividualTireCountIND == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountMOTR).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null
            && c.Threshold_IndividualTireCountIND == null && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountLOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountLOTR).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null
            && c.Threshold_IndividualTireCountIND == null && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountGOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_IndividualTireCountGOTR).NotNull().When(c => c.Threshold_IndividualTireCountPLT == null && c.Threshold_IndividualTireCountMT == null && c.Threshold_IndividualTireCountAGLS == null
            && c.Threshold_IndividualTireCountIND == null && c.Threshold_IndividualTireCountSOTR == null && c.Threshold_IndividualTireCountMOTR == null && c.Threshold_IndividualTireCountLOTR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountDOR).NotNull().When(c => c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null && c.Threshold_TotalTireCountPTR == null
            && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountDOT).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountHIT == null && c.Threshold_TotalTireCountPTR == null
            && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountHIT).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountPTR == null
            && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountPTR).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null
            && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountRTR).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null
            && c.Threshold_TotalTireCountPTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountSTC).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null
            && c.Threshold_TotalTireCountPTR == null && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountTCR == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountTCR).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null
            && c.Threshold_TotalTireCountPTR == null && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountUCR == null).WithMessage(MessageResource.RequiredFieldMissing);

            RuleFor(c => c.Threshold_TotalTireCountUCR).NotNull().When(c => c.Threshold_TotalTireCountDOR == null && c.Threshold_TotalTireCountDOT == null && c.Threshold_TotalTireCountHIT == null
            && c.Threshold_TotalTireCountPTR == null && c.Threshold_TotalTireCountRTR == null && c.Threshold_TotalTireCountSTC == null && c.Threshold_TotalTireCountTCR == null).WithMessage(MessageResource.RequiredFieldMissing);
        }
    }
}
