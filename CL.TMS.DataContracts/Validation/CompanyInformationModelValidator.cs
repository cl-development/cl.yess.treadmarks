﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using FluentValidation;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;

namespace CL.TMS.DataContracts.Validation
{
    public class CompanyInformationModelValidator : AbstractValidator<CompanyInformationVM>
    {
        public CompanyInformationModelValidator()
        {
            var stewardType = RuleSetHelper.GetStewardType();
            switch (stewardType)
            {
                case StewardType.OTS:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
                default:
                    {
                        BuildOTSValidationRules();
                        break;
                    }
            }
        }

        private void BuildOTSValidationRules()
        {
            RuleFor(c => c.CompanyName).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.AddressLine1).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.City).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.Province).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.PostalCode).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.PostalCode).Matches(@"^[0-9]{5}(-[0-9]{4})?$").When(c => c.Country == "United States").WithMessage(MessageResource.ValidationMsgInvalidPostalCode);
            RuleFor(c => c.PostalCode).Matches(@"^[ABCEGHJ-NPRSTVXY][0-9][ABCEGHJ-NPRSTV-Z][ ]?[0-9][ABCEGHJ-NPRSTV-Z][0-9]$").When(c => c.Country == "Canada").WithMessage(MessageResource.ValidationMsgInvalidPostalCode);
            RuleFor(c => c.PostalCode).Matches(@"a^").When(c => c.Country == null).WithMessage(MessageResource.ValidationMsgInvalidPostalCode);
            RuleFor(c => c.Country).NotNull().WithMessage(MessageResource.RequiredFieldMissing);
            RuleFor(c => c.PhoneNumber).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$").WithMessage(MessageResource.ValidationMsgInvalidPhone);
            RuleFor(c => c.Fax).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$").WithMessage(MessageResource.ValidationMsgInvalidPhone);
            RuleFor(c => c.Email).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$").WithMessage(MessageResource.ValidationMsgInvalidEmail);
            RuleFor(c => c.WebSite).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$").WithMessage(MessageResource.InvalidInvitationUrl);
            RuleFor(c => c.Facebook).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$").WithMessage(MessageResource.InvalidInvitationUrl);
            RuleFor(c => c.Twitter).NotNull().WithMessage(MessageResource.RequiredFieldMissing)
                .Matches(@"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$").WithMessage(MessageResource.InvalidInvitationUrl);          
        }
    }
}
