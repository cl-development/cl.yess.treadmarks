﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Invitation")]
    public class Invitation : BaseDTO<long>
    {
        public Invitation()
        {
            InvitationRoleIds = new List<int>();
            InvitationRoles = new List<InvitationRole>();
            InvitationClaimsWorkflows = new List<InvitationClaimsWorkflow>(); //new added
            InvitationApplicationsWorkflows = new List<InvitationApplicationsWorkflow>(); //new added
        }
        [Display(Name = "Invite GUID")]
        public Guid InviteGUID { get; set; }

        [Display(Name = "Email Address")]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Emailed")]
        public bool IsEmailed { get; set; }

        [Display(Name = "Invited On")]
        public DateTime InviteDate { get; set; }

        [Display(Name = "Invite Expires On")]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "Invite Send Date")]
        public DateTime InviteSendDate { get; set; }

        [Display(Name = "Sent By")]
        public string CreatedBy { get; set; }

        public string Status { get; set; }
        public string UserIP { get; set; }

        [NotMapped]
        public string SiteUrl { get; set; }
        public string RedirectUrl { get; set; }

        public int? VendorId { get; set; }

        public int? CustomerId { get; set; }

        public int? ApplicationId { get; set; }

        public DateTime? UpdateDate { get; set; }
        public virtual ICollection<InvitationRole> InvitationRoles { get; set; }
        public virtual ICollection<InvitationClaimsWorkflow> InvitationClaimsWorkflows { get; set; } //new added
        public virtual ICollection<InvitationApplicationsWorkflow> InvitationApplicationsWorkflows { get; set; } //new added

        [NotMapped]
        public List<int> InvitationRoleIds { get; set; }   

    }


}
