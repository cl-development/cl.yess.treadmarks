﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsRmTransDistribution")]
    public class RrOtsRmTransDistribution
    {
        [Key, Column(Order = 2)]
        public short RMDTYPAL { get; set; }
        [Key, Column(Order = 0)]    
        public string DOCNUMBR { get; set; }    
        public string CUSTNMBR { get; set; }
        [Key, Column(Order = 1)]    
        public int SEQNUMBR { get; set; }    
        public int DISTTYPE { get; set; }   
        public string DistRef { get; set; }    
        public string ACTNUMST { get; set; }    
        public decimal DEBITAMT { get; set; }    
        public decimal CRDTAMNT { get; set; }    
        public string USERDEF1 { get; set; }    
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 3)]    
        public string INTERID { get; set; }    
        public int? INTSTATUS { get; set; }    
        public DateTime? INTDATE { get; set; }    
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int DEX_ROW_ID { get; set; }
   
    
    }

}
