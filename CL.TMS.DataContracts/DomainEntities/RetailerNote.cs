﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RetailerNote")]
    public class RetailerNote:BaseDTO<int>
    {
        public int RetailerId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User AddedBy { get; set; }
        [ForeignKey("RetailerId")]
        public virtual Retailer Retailer { get; set; }
    }
}
