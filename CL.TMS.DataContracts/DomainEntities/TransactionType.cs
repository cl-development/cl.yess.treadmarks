﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
using System.ComponentModel.DataAnnotations;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionType")]
    public class TransactionType
    {
        public int ID { get; set; }
        [Key]
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Inbound { get; set; }
        public string Outbound { get; set; }
        public int SortIndex { get; set; }
        public bool IsFinancialEligible { get; set; }
        
    }
}
