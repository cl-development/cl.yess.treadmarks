﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Product")]
    public class Product:BaseDTO<int>
    {
        public Product()
        {
            RetailerProducts = new List<RetailerProduct>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BrandName { get; set; }
        public string Status { get; set; }
        public int? AttachmentId { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int VendorId { get; set; }
        public int CategoryId { get; set; }
        public long AddedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [ForeignKey("AttachmentId")]
        public virtual Attachment ProductAttachment { get; set; }
        [ForeignKey("CategoryId")]
        public virtual ProductCategory ProductCategory { get; set; }
        [ForeignKey("AddedBy")]
        public virtual User AddedUser { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor ProductVendor { get; set; }

        public virtual List<RetailerProduct> RetailerProducts { get; set; }

    }
}
