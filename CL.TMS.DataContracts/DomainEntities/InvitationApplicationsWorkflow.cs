﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("InvitationApplicationsWorkflow")]
    public class InvitationApplicationsWorkflow 
    {
        public int Id { get; set; }
        public long InvitationId { get; set; }
        public string AccountName { get; set; }
        public bool Routing { get; set; }
        public bool ActionGear { get; set; }
        [ForeignKey("InvitationId")]
        public virtual Invitation Invitation { get; set; }

    }
}
