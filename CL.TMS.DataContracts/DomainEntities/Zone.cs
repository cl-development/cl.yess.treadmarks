﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Zone")]
    public class Zone : BaseDTO<int>
    {
        public int ZoneType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string GreaterZone { get; set; }
    }
}
