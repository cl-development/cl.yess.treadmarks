using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimAdjustmentDetail")]
    public class ClaimAdjustmentDetail : BaseDTO<int>
    {
        public int ClaimAdjustmentID { get; set; }
        public int OriginalClaimID { get; set; }
        public decimal PaymentAdjustment { get; set; }
        public int PaymentType { get; set; }
    
        public virtual ICollection<ClaimDetailAdjustment> ClaimDetailAdjustments { get; set; }
        public virtual ICollection<ClaimPaymentAdjustment> ClaimPaymentAdjustments { get; set; }
        public virtual ICollection<ClaimSummaryAdjustment> ClaimSummaryAdjustments { get; set; }
    }
}
