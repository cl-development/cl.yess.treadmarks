﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RegionDetail")]
    public class RegionDetail : BaseDTO<int>
    {
        public string FSA { get; set; }
        public string City { get; set; }
        public int RegionId { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
    }
}
