﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimSupportingDocument")]
    public class ClaimSupportingDocument : BaseDTO<int>
    {
        [ForeignKey("Claim")]
        public int ClaimId { get; set; }
        public string Category { get; set; }
        public string TypeCode { get; set; }
        public int DefinitionValue { get; set; }
        public string SelectedOption { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
