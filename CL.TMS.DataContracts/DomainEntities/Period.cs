﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Period")]
    public class Period:BaseDTO<int>
    {
        public int PeriodType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FiscalYear { get; set; }
        public bool EnableSubmit { get; set; }
        public bool ShowPeriod { get; set; }
        public bool? IsAdjustment { get; set; }
        public int? OriginalPeriodId { get; set; }
        public bool? IsSpecialEvent { get; set; }
        public DateTime? ShowStart { get; set; }
        public DateTime SubmitStart { get; set; }
        public DateTime SubmitEnd { get; set; }
        public string ShortName { get; set; }
        public string PeriodDesc { get; set; }
    }
}
