﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionAttachment")]
    public class TransactionAttachment : BaseDTO<int>
    {        
        public int AttachmentId { get; set; } 
        public int TransactionId { get; set; }            
        public string Description { get; set; }
        public int? TransactionAdjustmentId { get; set; }
        [ForeignKey("AttachmentId")]
        public virtual Attachment Attachemnt { get; set; }
    }
}
