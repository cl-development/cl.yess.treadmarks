using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;
namespace CL.TMS.DataContracts.DomainEntities
{

    [Table("ClaimDetailAdjustment")]
    public class ClaimDetailAdjustment : BaseDTO<int>
    {
        public int ClaimAdjustmentDetailId { get; set; }
        public int TransactionId { get; set; }
        public bool Direction { get; set; }
        public string TransactionType { get; set; }
        public bool IsEligible { get; set; }
        public int UnitTye { get; set; }
    }
}
