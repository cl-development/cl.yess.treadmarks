﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{  
    
    [Table("TSFClaimDetail")]
    public class TSFClaimDetail : BaseDTO<int>
    {
        //public int ID { get; set; }
        public int TSFClaimID { get; set; }
        public int ItemID { get; set; }
        public int TireSupplied { get; set; }
        public int NegativeAdjustment { get; set; }
        public int CountSupplied { get; set; }
        public decimal TSFRate { get; set; }
        public string OtherDesc { get; set; }        
        public decimal TSFDue { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public int AdjustmentReasonType { get; set; }
        public DateTime? NegativeAdjDate { get; set; }
        public int CreditItemID { get; set; }
        public int CreditNegativeAdj { get; set; }
        public decimal CreditTSFRate { get; set; }
        public decimal CreditTSFDue { get; set; }
        public DateTime? TSFRateDate { get; set; } //add for 2x
        public virtual Item Item { get; set; }
        public virtual TSFClaim TSFClaim { get; set; } 

    }
}
