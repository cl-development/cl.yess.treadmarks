﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Transaction")]
    public partial class Transaction : BaseDTO<int>
    {
        public Guid TransactionId { get; set; }
        public bool MobileFormat { get; set; }
        public string TransactionTypeCode { get; set; }
        public long FriendlyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime? TransactionEndDate { get; set; }
        public int? OutgoingGpsLogId { get; set; }
        public int? IncomingGpsLogId { get; set; }
        public string Status { get; set; }
        public string OutgoingSignatureName { get; set; }
        public string IncomingSignatureName { get; set; }
        public long? CreatedUserId { get; set; }
        public string ProcessingStatus { get; set; }

        [ForeignKey("CreatedUserId")]
        public virtual User CreatedUser { get; set; }

        public int? CreatedVendorId { get; set; }

        [ForeignKey("CreatedVendorId")]
        public virtual Vendor CreatedVendor { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? SyncDate { get; set; }
        public int IncomingSignaturePhotoId { get; set; }
        public int OutgoingSignaturePhotoId { get; set; }
        public string ResponseComment { get; set; }
        public string FromPostalCode { get; set; }
        public string ToPostalCode { get; set; }
        public string DeviceName { get; set; }
        public int? ZoneId { get; set; }
        public int? IncomingId { get; set; }
        public int? IncomingAddressId { get; set; }
        public int? OutgoingId { get; set; }
        public int? OutgoingAddressId { get; set; }
        public bool IsGenerateTires { get; set; }
        public bool IsEligible { get; set; }
        public decimal? OnRoadWeight { get; set; }
        public decimal? OffRoadWeight { get; set; }
        public decimal? EstimatedOnRoadWeight { get; set; }
        public decimal? EstimatedOffRoadWeight { get; set; } 
        public string MarketLocation { get; set; }
        public int? TireMarketType { get; set; }
        public string BillofLoading { get; set; }
        public string InvoiceNumber { get; set; }
        public string EventNumber { get; set; }        
        public string Badge { get; set; }
        public int? PaymentEligible { get; set; }
        public string PaymentEligibleOther { get; set; }
        public int? MaterialType { get; set; }
        public int? UnitType { get; set; }
        public string PTRNumber { get; set; }
        public int? DispositionReason { get; set; }
        public string DeviceId { get; set; }

        public bool IsAddedByStaff { get; set; }
        public bool IsSingle { get; set; }

        public int? CompanyInfoId { get; set; }
        public decimal? RetailPrice { get; set; }
        public int? TransactionAdjustmentID { get; set; }

        [ForeignKey("CompanyInfoId")]
        public virtual CompanyInfo CompanyInfo { get; set; }

        public int? VendorInfoId { get; set; }
        [ForeignKey("VendorInfoId")]
        public virtual Vendor VendorInfo { get; set; }

        [ForeignKey("TransactionTypeCode")]
        public virtual TransactionType TransactionType { get; set; }

        [ForeignKey("IncomingId")]
        public virtual Vendor IncomingVendor { get; set; }

        [ForeignKey("OutgoingId")]
        public virtual Vendor OutgoingVendor { get; set; }

        [ForeignKey("IncomingGpsLogId")]
        public virtual GpsLog IncomingGpsLog { get; set; }

        [ForeignKey("OutgoingGpsLogId")]
        public virtual GpsLog OutgoingGpsLog { get; set; }

        //[ForeignKey("TransactionAdjustmentID")]
        //public virtual TransactionAdjustment ApprovedTransactionAdjustments { get; set; }

        public virtual List<TransactionItem> TransactionItems { get; set; }
        public virtual List<TransactionPhoto> TransactionPhotos { get; set; }        
        public virtual List<TransactionAttachment> TransactionAttachments { get; set; }
        public virtual List<TransactionNote> TransactionNotes { get; set; }
        public virtual List<ScaleTicket> ScaleTickets { get; set; }
        public virtual List<TransactionSupportingDocument> TransactionSupportingDocuments { get; set; }
        public virtual List<TransactionComment> TransactionComments { get; set; }
        public virtual List<TransactionAdjustment> TransactionAdjustments { get; set; }


        public Transaction()
        {
            TransactionItems = new List<TransactionItem>();
            TransactionPhotos = new List<TransactionPhoto>();
            TransactionAttachments = new List<TransactionAttachment>();
            TransactionNotes = new List<TransactionNote>();
            ScaleTickets = new List<ScaleTicket>();
            TransactionSupportingDocuments = new List<TransactionSupportingDocument>();
            TransactionComments = new List<TransactionComment>();
            TransactionAdjustments = new List<TransactionAdjustment>();
        }
    }
}
