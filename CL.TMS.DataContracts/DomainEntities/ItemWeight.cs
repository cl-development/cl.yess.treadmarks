﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ItemWeight")]
    public class ItemWeight : BaseDTO<int>
    {
        public int ItemID { get; set; }
        public Decimal StandardWeight { get; set; }
        public string WeightDescription { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        //public int? RateTransactionID { get; set; }
        public Decimal? SetRangeMin { get; set; }
        public Decimal? SetRangeMax { get; set; }
        public int? WeightTransactionID { get; set; }

        //[ForeignKey("RateTransactionID")]
        //public virtual RateTransaction RateTransaction { get; set; }

        [ForeignKey("ItemID")]
        public virtual Item item { get; set; }        

        [ForeignKey("WeightTransactionID")]
        public virtual WeightTransaction WeightTransaction { get; set; }
    }
}
