﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ProductCategory")]
    public class ProductCategory:BaseDTO<int>
    {
        public ProductCategory()
        {
            CategoryNotes = new List<CategoryNote>();
            Products = new List<Product>();
        }
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual List<CategoryNote> CategoryNotes { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}
