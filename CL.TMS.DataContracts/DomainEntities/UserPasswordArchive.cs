﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;


namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("UserPasswordHistory")]
    public class UserPasswordArchive : BaseDTO<int>
    {
        public long UserID { get; set; }
        public string Password { get; set; }
        public Nullable<DateTime> LastLoginDate { get; set; }
        public User User { get; set; }
    }
}
