﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("rptClaimInventoryDetail")]
    public class ClaimInventoryDetail : BaseDTO<int>
    {
        public ClaimInventoryDetail()
        {
            TransactionId = 0;
            Quantity = 0;
            ActualWeight = 0;
            AverageWeight = 0;
        }
        [ForeignKey("Claim")]
        public int ClaimId { get; set; }
        public int TransactionId { get; set; }
        public bool Direction { get; set; }
        public string TransactionProcessingStatus { get; set; }
        public int ItemId { get; set; }
        public int ItemType { get; set; }
        public string ItemShortName { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public decimal ActualWeight { get; set; }
        public decimal AverageWeight { get; set; }
        public int Quantity { get; set; }
        public int UnitType { get; set; }
        public string TransactionTypeCode { get; set; }
        public int ClaimPeriodId { get; set; }
        public string PeriodName { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public int VendorId { get; set; }
        public int VendorType { get; set; }
        public string BusinessName { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// From Transaction Item table, which is only applicable for certain type of transaction such as RPMSPS
        /// </summary>
        public string ItemDescription { get; set; }
        /// <summary>
        /// From Transaction Item table, which is only applicable for certain type of transaction such as RPMSPS
        /// </summary>
        public string ItemCode { get; set; }

        public decimal? TransactionItemRate { get; set; }

        public virtual Claim Claim { get; set; }
    }
}
