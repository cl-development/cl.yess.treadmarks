﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RateGroupRate")] 
    public class RateGroupRate
    {  
        [Key]
        public int Id { get; set; }
        public int? CollectorGroupId { get; set; }
        public int? ProcessorGroupId { get; set; }
        public int PaymentType { get; set; }
        public decimal Rate { get; set; }
        public int ItemType { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public int RateTransactionId { get; set; }
        [ForeignKey("RateTransactionId")]
        public virtual RateTransaction RateTransaction { get; set; }
    }
}
