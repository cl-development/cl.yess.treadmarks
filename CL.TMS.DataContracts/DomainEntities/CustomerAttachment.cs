﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("CustomerAttachment")]
    public class CustomerAttachment : BaseDTO<int>
    {
        public int CustomerId { get; set; }
        public int AttachmentId { get; set; }
        [ForeignKey("AttachmentId")]
        public virtual Attachment Attachment { get; set; }

    }
}
