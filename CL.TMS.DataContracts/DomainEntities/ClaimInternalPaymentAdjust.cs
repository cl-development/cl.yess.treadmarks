﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("ClaimInternalPaymentAdjust")]
    public class ClaimInternalPaymentAdjust : BaseDTO<int>
    {
        public ClaimInternalPaymentAdjust()
        {
            ClaimPaymentAdjustmentNotes = new List<ClaimInternalAdjustmentNote>();
        }
        public int ClaimId { get; set; }
        public int PaymentType { get; set; }
        public decimal AdjustmentAmount { get; set; }

        [ForeignKey("AdjustUser")]
        public long AdjustBy { get; set; }
        public DateTime AdjustDate { get; set; }
        public virtual User AdjustUser { get; set; }
        //OTSTM2-668
        public virtual ICollection<ClaimInternalAdjustmentNote> ClaimPaymentAdjustmentNotes { get; set; }
    }
}
