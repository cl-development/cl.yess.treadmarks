﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("TransactionPhoto")]
    public class TransactionPhoto : BaseDTO<int>
    {
        public int TransactionId { get; set; }
        public int PhotoId { get; set; }
        public int? TransactionAdjustmentId { get; set; }
        [ForeignKey("PhotoId")]
        public virtual Photo Photo { get; set; }
        [ForeignKey("TransactionId")]
        public virtual Transaction Transaction { get; set; }
        [ForeignKey("TransactionAdjustmentId")]
        public virtual TransactionAdjustment TransactionAdjustment { get; set; }
    }
}
