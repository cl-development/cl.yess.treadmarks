﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CL.TMS.Framework.DTO;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("Asset")]
    public class Asset : BaseDTO<int>
    {
        public string AssetTag { get; set; }
        public Nullable<int> VendorID { get; set; }
        public int AssetTypeUID { get; set; }
        public int AssetType { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public virtual Vendor Vendor { get; set; }

     //   public virtual AssetType AssetType { get; set; }
    }
}
