﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RrOtsRmTransaction")]
    public class RrOtsRmTransaction
    {
        [Key, Column(Order = 2)]
        public short RMDTYPAL { get; set; }    
        public string RMDNUMWK { get; set; }
        [Key, Column(Order = 0)]    
        public string DOCNUMBR { get; set; }    
        public string DOCDESCR { get; set; }    
        public DateTime? DOCDATE { get; set; }    
        public string BACHNUMB { get; set; }    
        public string CUSTNMBR { get; set; }    
        public string ADRSCODE { get; set; }    
        public string CSTPONBR { get; set; }    
        public string TAXSCHID { get; set; }    
        public decimal SLSAMNT { get; set; }    
        public decimal? TAXAMNT { get; set; }    
        public decimal DOCAMNT { get; set; }    
        public string PYMTRMID { get; set; }    
        public DateTime? DUEDATE { get; set; }    
        public string USERDEF1 { get; set; }    
        public string USERDEF2 { get; set; }
        [Key, Column(Order = 1)]    
        public string INTERID { get; set; }    
        public int? INTSTATUS { get; set; }    
        public DateTime? INTDATE { get; set; }    
        public string ERRORCODE { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int DEX_ROW_ID { get; set; }    
        [Display(Name = "fibatchentry_id")] //OTSTM2-1124
        public long gpibatchentry_id { get; set; }
      
    }
}
