﻿using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.DomainEntities
{
    [Table("RateTransaction")]
    public class RateTransaction : BaseDTO<int>
    {
        public DateTime CreatedDate { get; set; }
        public long CreatedByID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByID { get; set; }
        public int Category { get; set; }

        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public bool? IsSpecificRate { get; set; }
        public int? PickupRateGroupId { get; set; }
        public int? DeliveryRateGroupId { get; set; }

        [ForeignKey("CreatedByID")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("ModifiedByID")]
        public virtual User ModifiedByUser { get; set; }

        public virtual List<RateTransactionNote> RateTransactionNotes { get; set; }

        public virtual List<Rate> Rates { get; set; }
        public virtual List<VendorRate> PIRateMappings { get; set; }

        public virtual List<RateGroupRate> RateGroupRates { get; set; }
    }
}
