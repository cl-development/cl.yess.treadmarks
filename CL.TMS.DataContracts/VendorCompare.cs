﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts
{
    public class VendorCompare : IEqualityComparer<Vendor>
    {
        public bool Equals(Vendor x, Vendor y)
        {
            return (x.ID == y.ID);
        }

        public int GetHashCode(Vendor obj)
        {
            return 0;
        }
    }
}
