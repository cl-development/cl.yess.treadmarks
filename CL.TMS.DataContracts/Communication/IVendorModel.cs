﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace CL.TMS.DataContracts.Communication
{
    public interface IVendorModel
    {
        int ID { get; set; }
        int VendorTypeID { get; set; }
        string Number { get; set; }
        string BusinessName { get; set; }
        string FranchiseName { get; set; }
        string OperatingName { get; set; }
        string BusinessNumber { get; set; }
        string TerritoryCode { get; set; }
        string GlobalDimension1Code { get; set; }
        string GlobalDimension2Code { get; set; }
        Nullable<int> CreditLimit { get; set; }
        string TelexAnswerBack { get; set; }
        string TaxRegistrationNo { get; set; }
        string TaxRegistrationNo2 { get; set; }
        string HomePage { get; set; }
        string BusinessGroupCode { get; set; }
        string CustomerGroupCode { get; set; }
        string DepartmentCode { get; set; }
        string ProjectCode { get; set; }
        string PurchaserCode { get; set; }
        string SalesCampaignCode { get; set; }
        string SalesPersonCode { get; set; }
        string AuthSigComplete { get; set; }
        Nullable<DateTime> ReceivedDate { get; set; }
        Nullable<DateTime> GracePeriodStartDate { get; set; }
        Nullable<DateTime> GracePeriodEndDate { get; set; }
        string CommercialLiabInsurerName { get; set; }
        Nullable<DateTime> CommercialLiabInsurerExpDate { get; set; }
        string CommercialLiabHSTNumber { get; set; }
        string WorkerHealthSafetyCertNo { get; set; }
        string PermitsCertDescription { get; set; }
        Nullable<bool> AmendedAgreement { get; set; }
        Nullable<DateTime> AmendedDate { get; set; }
        bool IsTaxExempt { get; set; }
        string UpdateToFinancialSoftware { get; set; }
        string PrimaryBusinessActivity { get; set; }
        Nullable<int> MailingAddressSameAsBusiness { get; set; }
        int BusinessActivityID { get; set; }
        string GSTNumber { get; set; }
        string AuthorizedSigComplete { get; set; }
        string LocationEmailAddress { get; set; }
        Nullable<DateTime> DateReceived { get; set; }
        Nullable<DateTime> ActivationDate { get; set; }
        Nullable<DateTime> ConfirmationMailedDate { get; set; }
        Nullable<int> PreferredCommunication { get; set; }
        int RegistrantTypeID { get; set; }
        int RegistrantSubTypeID { get; set; }
        string LastUpdatedBy { get; set; }
        Nullable<DateTime> LastUpdatedDate { get; set; }
        Nullable<int> CheckoutNum { get; set; }
        DateTime CreatedDate { get; set; }
        Nullable<int> InBatch { get; set; }
        bool IsActive { get; set; }
        Nullable<DateTime> ActiveStateChangeDate { get; set; }
        string FacilityDescription { get; set; }
        string TaxClass { get; set; }
        Nullable<int> PrimaryBusinessAddrStorageCap { get; set; }
        Nullable<int> NumberOfStorageSites { get; set; }
        Nullable<int> MaxStorageSitesCapacity { get; set; }
        string StorageSiteID { get; set; }
        string CertificateOfApprovalNum { get; set; }
        Nullable<DateTime> CompanyYearEndDate { get; set; }
        string ProcProductsProduced { get; set; }
        string ProcProductsProducedDesc { get; set; }
        string HPrimaryBusAddrSortYards { get; set; }
        string HNumberOfSortYards { get; set; }
        Nullable<int> HSortYardsStorageCap { get; set; }
        Nullable<int> HSortYardID { get; set; }
        Nullable<DateTime> HYardCountDate { get; set; }
        Nullable<DateTime> HYardCountDate2 { get; set; }
        Nullable<int> HPrimaryBusAddrStorageCap { get; set; }
        string CPointsAdditional { get; set; }
        string CPointsType { get; set; }
        string CPointsGarage { get; set; }
        string CPointsUsedVehicle { get; set; }
        string CPointsOther { get; set; }
        string CPointsOtherDesc { get; set; }
        string HasLocalInventory { get; set; }
        string CIsGenerator { get; set; }
        string EndUseMarket { get; set; }
        string EndUseMarketDesc { get; set; }

    }
}
