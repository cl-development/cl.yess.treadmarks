﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.Communication
{
    public interface IAppUserModel
    {
        int ID { get; set; }
        int VendorID { get; set; }
        int VendorAppUserID { get; set; }
        string Number { get; set; }
        string Name { get; set; }
        string EMail { get; set; }
        int AccessTypeID { get; set; }
        DateTime CreatedOn { get; set; }
        Nullable<DateTime> ModifiedOn { get; set; }
        string Metadata { get; set; }
        Nullable<DateTime> LastAccessOn { get; set; }
        Nullable<DateTime> SyncOn { get; set; }
        bool Active { get; set; }
    }
}
