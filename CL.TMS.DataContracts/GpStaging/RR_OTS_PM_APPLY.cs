﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataContracts.GpStaging
{
    public class RR_OTS_PM_APPLY
    {
        [Key, Column(Order = 0)]
        public string TRX_NUMBER { get; set; }
        [Key, Column(Order = 1)]
        public string CHQ_EFT_NUMBER { get; set; }
        public decimal DOCAMNT { get; set; }
        public DateTime DOCDATE { get; set; }
        [Key, Column(Order = 2)]
        public string INTERID { get; set; }
        public int INTSTATUS { get; set; }
        public DateTime INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        public bool Voided { get; set; }
    }
}
