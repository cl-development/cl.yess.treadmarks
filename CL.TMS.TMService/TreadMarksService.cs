﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.Repository.Claims;
using CL.TMS.Repository.Registrant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CL.TMS.TMService
{
    public partial class TreadMarksService : ServiceBase
    {
        private Timer _timer;
        public TreadMarksService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(60 * 60 * 1000); //every hour to run job
            _timer.Enabled = true;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Open Claim Service
            //Check if it is 0:00am to 1:00am
            var now = DateTime.Now;
            if ((now.Day == 1) && IsTimeOfDayBetween(now, new TimeSpan(0, 0, 0), new TimeSpan(1, 0, 0)))
            {
                OpenClaimsForActiveVendors();
            } 

            //Steward auto switch service
            //Check if it is 2:00am to 3:00am of Jan 1st in the future year
            var date = ConfigurationManager.AppSettings["AutoSwitchDateKey"];
            var autoSwitchDate = DateTime.Parse(date);
            if ((now.Day == autoSwitchDate.Day) && (now.Month == autoSwitchDate.Month) && IsTimeOfDayBetween(now, new TimeSpan(2, 0, 0), new TimeSpan(3, 0, 0)))
            {
                AutoSwitchForAllCustomers();
            }
        }

        private void AutoSwitchForAllCustomers()
        {
            LogManager.LogInfo("Start AutoSwitchForAllCustomers ...");
            var changeBy = (int)AppSettings.Instance.SystemUser.ID;
            var vendorRepository = new VendorRepository();
            try
            {
                vendorRepository.AutoSwitchForAllCustomers(changeBy);
            }
            catch (Exception ex)
            {
                LogManager.LogExceptionWithMessage("Failed to auto switch for all customers", ex);
            }
            LogManager.LogInfo("Done of AutoSwitchForAllCustomers ...");
        }


        private void OpenClaimsForActiveVendors()
        {
            LogManager.LogInfo("Start opening claims for all active vendors ...");
            try
            {
                var claimsRepository = new ClaimsRepository();
                claimsRepository.InitializeClaimForActiveVendors();
            }
            catch (Exception ex)
            {
                LogManager.LogExceptionWithMessage("Failed to open claim for all active vendors", ex);
            }
            LogManager.LogInfo("Done of opening claims for all active vendors ...");
        }

        private bool IsTimeOfDayBetween(DateTime time, TimeSpan startTime, TimeSpan endTime)
        {
            if (endTime == startTime)
            {
                return true;
            }
            else if (endTime < startTime)
            {
                return time.TimeOfDay < endTime ||
                    time.TimeOfDay > startTime;
            }
            else
            {
                return time.TimeOfDay > startTime &&
                    time.TimeOfDay < endTime;
            }

        }
        protected override void OnStop()
        {
            _timer.Enabled = false;
            _timer.Stop();
        }
    }
}
