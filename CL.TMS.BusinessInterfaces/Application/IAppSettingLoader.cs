﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.BusinessInterfaces.Application
{
    public interface IAppSettingLoader
    {
        List<Setting> LoadAllSettings();
    }
}
