﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;

namespace CL.TMS.BusinessInterfaces.Application
{
    public interface IInvitationBO
    {
        void AddInvitation(Invitation invitation);
    }
}
