﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CL.TMS.Framework.DAL
{
    public class GpStagingBaseContext<TContext> : DbContext where TContext : DbContext
    {
        static GpStagingBaseContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        protected GpStagingBaseContext() : base("GP")
        {
        }

    }
}
