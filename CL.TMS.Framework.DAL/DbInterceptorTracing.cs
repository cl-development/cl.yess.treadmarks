﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.Logging;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// DB interceptor
    /// </summary>
    public class DbInterceptorTracing : DbCommandInterceptor
    {
        #region Fields
        private readonly Stopwatch stopwatch = new Stopwatch();
        #endregion 

        #region Public Methods
        /// <summary>
        /// Add tracing log before scalar command execting
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                base.ScalarExecuting(command, interceptionContext);
                stopwatch.Restart();
            }
        }

        /// <summary>
        /// Add tracing log after scaler command execting
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            //Do DB Audit Log, exclude calculation related table because there is a lot of records will be added ClaimInventory, Inventory, ClaimPayment, ClaimPaymentDetail, RptClaimInventoryDetail
            var dbCommandText = command.CommandText.ToLower();
            if ((dbCommandText.Contains("insert")) || (dbCommandText.Contains("update")) || (dbCommandText.Contains("delete")) &&
                (!((dbCommandText.Contains("claiminventory")) || (dbCommandText.Contains("inventory")) || (dbCommandText.Contains("claimpayment")) ||
                (dbCommandText.Contains("claimpaymentdetail")) || (dbCommandText.Contains("rptclaiminventorydetail")))))
            {
                var logArgs = new List<LogArg>();
                for (int i = 0; i < command.Parameters.Count; i++)
                {
                    logArgs.Add(new LogArg { Name = command.Parameters[i].ParameterName, Value = command.Parameters[i].Value });
                }
                LogManager.LogDBAuditLog(command.CommandText, logArgs);
            }

            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                stopwatch.Stop();
                var message = string.Format("SchoolInterceptor.ScalarExecuted:{0} ms", stopwatch.Elapsed.Milliseconds);
                var logArgs = new List<LogArg>();
                logArgs.Add(new LogArg { Name = "CommandText", Value = command.CommandText });
                for (int i = 0; i < command.Parameters.Count; i++)
                {
                    logArgs.Add(new LogArg { Name = command.Parameters[i].ParameterName, Value = command.Parameters[i].Value });
                }
                LogManager.LogDBDebug(message, logArgs);

                base.ScalarExecuted(command, interceptionContext);
            }
        }

        /// <summary>
        /// Add tracing log before non query command executing
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                base.NonQueryExecuting(command, interceptionContext);
                stopwatch.Restart();
            }
        }

        /// <summary>
        /// Add tracing log after non query command executing
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            //Do DB Audit Log
            var dbCommandText = command.CommandText.ToLower();
            if ((dbCommandText.Contains("insert")) || (dbCommandText.Contains("update")) || (dbCommandText.Contains("delete")) &&
                (!((dbCommandText.Contains("claiminventory")) || (dbCommandText.Contains("inventory")) || (dbCommandText.Contains("claimpayment")) ||
                (dbCommandText.Contains("claimpaymentdetail")) || (dbCommandText.Contains("rptclaiminventorydetail")))))
            {
                var logArgs = new List<LogArg>();
                for (int i = 0; i < command.Parameters.Count; i++)
                {
                    logArgs.Add(new LogArg { Name = command.Parameters[i].ParameterName, Value = command.Parameters[i].Value });
                }
                LogManager.LogDBAuditLog(command.CommandText, logArgs);
            }

            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                stopwatch.Stop();
                var message = string.Format("SchoolInterceptor.NonQueryExecuted:{0} ms", stopwatch.Elapsed.Milliseconds);
                var logArgs = new List<LogArg>();
                logArgs.Add(new LogArg { Name = "CommandText", Value = command.CommandText });
                for (int i = 0; i < command.Parameters.Count; i++)
                {
                    logArgs.Add(new LogArg { Name = command.Parameters[i].ParameterName, Value = command.Parameters[i].Value });
                }
                LogManager.LogDBDebug(message, logArgs);

                base.NonQueryExecuted(command, interceptionContext);
            }
        }

        /// <summary>
        /// Add tracing log before data reader executing
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                base.ReaderExecuting(command, interceptionContext);
                stopwatch.Restart();
            }
        }

        /// <summary>
        /// Add tracing log after data reader executing
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            if ((LogManager.IsTracingEnabled) && (!command.CommandText.Contains("SystemLog")))
            {
                stopwatch.Stop();
                var message = string.Format("SchoolInterceptor.ReaderExecuted:{0} ms", stopwatch.Elapsed.Milliseconds);
                var logArgs = new List<LogArg>();
                logArgs.Add(new LogArg { Name = "CommandText", Value = command.CommandText });
                for (int i = 0; i < command.Parameters.Count; i++)
                {
                    logArgs.Add(new LogArg { Name = command.Parameters[i].ParameterName, Value = command.Parameters[i].Value });
                }
                LogManager.LogDBDebug(message, logArgs);

                base.ReaderExecuted(command, interceptionContext);
            }
        }
        #endregion 
    }
}
