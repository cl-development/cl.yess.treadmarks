﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Framework.DTO;

namespace CL.TMS.Framework.DAL
{
    /// <summary>
    /// Context helper class
    /// </summary>
    public static class DbContextHelper
    {
        /// <summary>
        /// Only use with short lived contexts 
        /// </summary>
        /// <param name="context"></param>
        public static void ApplyStateChanges(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelper.ConvertState(stateInfo.State);
            }
        }
    }
}
