﻿using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.RetailConnectionService;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Email;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Export;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.RPM;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RPMBLL
{
    public class RetailConnectionBO
    {
        private readonly IRetailConnectionRepository retailConnectionRepository;

        public RetailConnectionBO(IRetailConnectionRepository retailConnectionRepository)
        {
            this.retailConnectionRepository = retailConnectionRepository;
        }

        public PaginationDTO<ParticipantProductsViewModel, int> LoadParticipantProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                return retailConnectionRepository.LoadParticipantProducts(pageIndex, pageSize, searchText, orderBy, sortDirection, SecurityContextHelper.CurrentVendor.VendorId);
            }
            else
            {
                throw new Exception("Current vendor is empty");
            }
        }

        public void EditProduct(ProductsFormViewModel productViewModel)
        {
            //retailConnectionRepository.UpdateRetailerProduct(productViewModel.RetailerProducts, productViewModel.Id, SecurityContextHelper.CurrentUser.Id);
            retailConnectionRepository.UpdateRetailerProduct(productViewModel, productViewModel.Id, SecurityContextHelper.CurrentUser.Id);
        }

        public void EditStaffProduct(ProductsFormViewModel productViewModel)
        {
            retailConnectionRepository.UpdateStaffProduct(productViewModel, SecurityContextHelper.CurrentUser.Id);
        }
        public ProductsFormViewModel GetProduct(int productId)
        {
            var result = new ProductsFormViewModel();
            var product = retailConnectionRepository.GetProduct(productId);
            var productRetailers = retailConnectionRepository.GetProductRetailers(productId);
            result.Id = product.ID;
            result.CategoryId = product.CategoryId;
            result.CategoryName = product.ProductCategory.Name;
            result.ProductName = product.Name;
            result.BrandName = product.BrandName;
            result.Description = product.Description;
            result.VendorId = product.VendorId;


            if ((product.AttachmentId != null) && (product.ProductAttachment != null))
            {
                result.UploadDocument.AttachmentId = product.AttachmentId;
                result.UploadDocument.ProductId = product.ProductAttachment.UniqueName;
                result.UploadDocument.Type = product.ProductAttachment.FileType;
                result.UploadDocument.Size = product.ProductAttachment.Size;
                //var fileUploadPath = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.FileUploadRepositoryPath").Value;
                //string fileFullPath = Path.Combine(fileUploadPath, product.ProductAttachment.FilePath);
                var fileUploadPath = AppSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(AppSettings.Instance.GetSettingValue("Settings.FileUploadPreviewDomainName"), product.ProductAttachment.FilePath);
                result.UploadDocument.Url = fileFullPath;
                //result.UploadDocument.Url = ConfigurationManager.AppSettings["Protocol"] + fileFullPath;
                result.UploadDocument.Name = product.ProductAttachment.FileName;
                result.UploadDocument.Description = product.ProductAttachment.Description;

                string[] pid = product.ProductAttachment.FilePath.Split(Path.DirectorySeparatorChar);
                result.ProductId = new Guid(pid[1]);
            }

            result.RetailerProducts = productRetailers;
            return result;
        }

        public void AddProduct(ProductsFormViewModel productFormViewModel)
        {
            var product = new Product();
            product.Name = productFormViewModel.ProductName;
            product.Description = productFormViewModel.Description;
            product.Status = TreadMarksConstants.UnderReviewStatus;
            product.AttachmentId = productFormViewModel.UploadDocument.AttachmentId;
            product.VendorId = productFormViewModel.VendorId;
            product.AddedBy = SecurityContextHelper.CurrentUser.Id;
            product.CategoryId = productFormViewModel.CategoryId;
            product.CreatedDate = DateTime.UtcNow;

            if (string.IsNullOrEmpty(productFormViewModel.BrandName))
            {
                product.BrandName = SecurityContextHelper.CurrentVendor != null ? SecurityContextHelper.CurrentVendor.BusinessName : string.Empty;
            }

            retailConnectionRepository.AddProduct(product);

            if (productFormViewModel.RetailerProducts.Count > 0)
            {
                var retailerProducts = new List<RetailerProduct>();
                var userId = SecurityContextHelper.CurrentUser.Id;
                productFormViewModel.RetailerProducts.ForEach(c =>
                {
                    var retailerProduct = new RetailerProduct();
                    retailerProduct.RetailerId = c.RetailerId;
                    retailerProduct.ProductId = product.ID;
                    retailerProduct.ProductUrl = c.ProductUrl;
                    retailerProduct.CreatedDate = DateTime.UtcNow;
                    retailerProduct.AddedBy = userId;
                    retailerProducts.Add(retailerProduct);
                });
                retailConnectionRepository.AddRetailerProducts(retailerProducts);
            }
        }

        public void AddCategory(StaffCategoryFormViewModel categoryFormViewModel)
        {
            var category = new ProductCategory();
            category.Name = categoryFormViewModel.CategoryName;
            category.Status = TreadMarksConstants.ActiveStatus;
            category.CreatedDate = DateTime.UtcNow;
            if (!string.IsNullOrEmpty(categoryFormViewModel.InternalNote))
            {
                var note = new CategoryNote();
                note.UserId = categoryFormViewModel.UserId;
                note.Note = categoryFormViewModel.InternalNote;
                note.CreatedDate = DateTime.UtcNow;
                note.ProductCategory = category;
                category.CategoryNotes.Add(note);
            }
            retailConnectionRepository.AddCategory(category);
        }

        public List<StaffCategoryViewModel> LoadProductCategory()
        {
            return retailConnectionRepository.LoadProductCategory();
        }

        public PaginationDTO<RetailerViewModel, int> LoadParticipantRetailer(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            return retailConnectionRepository.LoadParticipantRetailer(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
        }

        public List<RetailerReference> LoadParticipantRetailer()
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendorId = SecurityContextHelper.CurrentVendor.VendorId;
                return retailConnectionRepository.LoadParticipantRetailer(vendorId);
            }
            else
            {
                return retailConnectionRepository.LoadAllActiveRetailer();
            }
        }

        public PaginationDTO<StaffCategoryViewModel, int> LoadProductCategory(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return retailConnectionRepository.LoadProductCategory(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public PaginationDTO<StaffProductViewModel, int> LoadStaffProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            int vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
                vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            return retailConnectionRepository.LoadStaffProducts(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
        }

        public PaginationDTO<RetailerViewModel, int> LoadStaffRetailers(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            int vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
                vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            return retailConnectionRepository.LoadStaffRetailers(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
        }

        public List<InternalNoteViewModel> LoadRetailerInternalNotes(int retailerId)
        {
            var internalNoteList = retailConnectionRepository.LoadRetailerInternalNotes(retailerId).OrderByDescending(s => s.CreatedDate);

            var list = new List<InternalNoteViewModel>();
            foreach (var internalNote in internalNoteList)
            {
                var user = internalNote.AddedBy;
                list.Add(new InternalNoteViewModel() { AddedOn = internalNote.CreatedDate, AddedBy = user.FirstName + " " + user.LastName, Note = internalNote.Note });
            }

            return list;
        }

        public InternalNoteViewModel AddRetailerNote(int retailerId, string note)
        {
            var retailerNote = new RetailerNote
            {
                RetailerId = Convert.ToInt32(retailerId),
                Note = note,
                CreatedDate = DateTime.UtcNow,
                UserId = SecurityContextHelper.CurrentUser.Id
            };

            retailConnectionRepository.AddRetailerNote(retailerNote);

            return new InternalNoteViewModel() { Note = retailerNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = retailerNote.CreatedDate };
        }

        public bool CheckRetailerName(string retailerName)
        {
            return retailConnectionRepository.CheckRetailerName(retailerName);
        }

        public void AddRetailer(RetailersFormViewModel retailerForm, bool isAddedByParticipant)
        {
            var retailer = new Retailer();
            retailer.Name = retailerForm.RetailerName;
            retailer.Url = string.Empty;
            if (!string.IsNullOrEmpty(retailerForm.Url))
            {
                retailer.Url = retailerForm.Url;
            }
            retailer.Status = TreadMarksConstants.ActiveStatus;
            retailer.IsOnline = retailerForm.ChainOnline;
            retailer.IsAddedByParticipant = isAddedByParticipant;
            retailer.CreatedDate = DateTime.UtcNow;
            retailer.AddedBy = SecurityContextHelper.CurrentUser.Id;
            if (isAddedByParticipant)
            {
                retailer.VendorId = SecurityContextHelper.CurrentVendor.VendorId;
                retailer.Status = TreadMarksConstants.UnderReviewStatus;
            }
            else
            {
                retailer.Status = TreadMarksConstants.ActiveStatus;
                retailer.ApprovedDate = retailer.CreatedDate;
            }

            if (!retailer.IsOnline)
            {
                var address = new Address();
                address.Address1 = retailerForm.AddressLine1 ?? string.Empty;
                address.Address2 = retailerForm.AddressLine2 ?? string.Empty;
                address.AddressType = 1;
                address.City = retailerForm.City ?? string.Empty;
                address.Province = retailerForm.Province ?? string.Empty;
                address.PostalCode = retailerForm.PostalCode;
                address.Country = retailerForm.Country;
                address.Phone = retailerForm.PhoneNumber;
                retailer.RetailerAddress = address;
            }
            if (!string.IsNullOrWhiteSpace(retailerForm.InternalNote))
            {
                var retailerNote = new RetailerNote();
                retailerNote.Note = retailerForm.InternalNote;
                retailerNote.CreatedDate = DateTime.UtcNow;
                retailerNote.UserId = SecurityContextHelper.CurrentUser.Id;
                retailerNote.Retailer = retailer;
                retailer.RetailerNotes.Add(retailerNote);
            }
            retailConnectionRepository.AddRetailer(retailer);
        }

        public ProductRetailerApproveEmailModel GetEmailInforByProductId(int productId)
        {
            return retailConnectionRepository.GetEmailInforByProductId(productId);
        }
        public ProductRetailerApproveEmailModel GetEmailInforByRetailerId(int retailerId)
        {
            return retailConnectionRepository.GetEmailInforByRetailerId(retailerId);
        }

        public void ChangeProductStatus(int productId, string fromStatus, string toStatus)
        {
            retailConnectionRepository.ChangeProductStatus(productId, fromStatus, toStatus);
        }

        public void ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus)
        {
            retailConnectionRepository.ChangeCategoryStatus(categoryId, fromStatus, toStatus);
        }

        public void ChangeRetailerStatus(int retailerId, string fromStatus, string toStatus)
        {
            retailConnectionRepository.ChangeRetailerStatus(retailerId, fromStatus, toStatus);
        }

        public RetailersFormViewModel LoadRetailerById(int retailerId)
        {
            return retailConnectionRepository.LoadRetailerById(retailerId);
        }

        public void AddRetailerNote(RetailerNoteViewModel retailerNoteViewModel)
        {
            var retailerNote = new RetailerNote();
            retailerNote.UserId = SecurityContextHelper.CurrentUser.Id;
            retailerNote.CreatedDate = DateTime.UtcNow;
            retailerNote.Note = retailerNoteViewModel.Note;
            retailerNote.RetailerId = retailerNoteViewModel.RetailerId;
            retailConnectionRepository.AddRetailerNote(retailerNote);
        }

        public void EditRetailer(RetailersFormViewModel retailerFormViewModel)
        {
            retailConnectionRepository.EditRetailer(retailerFormViewModel);
        }

        #region Export
        public List<ParticipantRetailerExportModel> GetParticipantRetailerExport()
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;

            return retailConnectionRepository.GetParticipantRetailerExport(vendorId);
        }

        public List<StaffRetailerExportModel> GetStaffRetailerExport()
        {
            var vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            }
            return retailConnectionRepository.GetStaffRetailerExport(vendorId);
        }

        public List<ParticipantProductExportModel> GetParticipantProductExport()
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;

            return retailConnectionRepository.GetParticipantProductExport(vendorId);
        }

        public List<StaffProductExportModel> GetStaffProductExport()
        {
            var vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            }
            return retailConnectionRepository.GetStaffProductExport(vendorId);
        }

        public List<ProductCategoryExportModel> GetProductCategoryExport()
        {
            return retailConnectionRepository.GetProductCategoryExport();
        }

        public StaffCategoryFormViewModel LoadCategoryById(int categoryId)
        {
            return retailConnectionRepository.LoadCategoryById(categoryId);
        }

        public List<CollectorModel> LoadActiveCollectors()
        {
            return retailConnectionRepository.LoadActiveCollectors();
        }

        public List<CategoryModel> LoadActiveCategory()
        {
            return retailConnectionRepository.LoadActiveCategory();
        }

        public List<ProductListModel> LoadActiveProductList(int categoryId)
        {
            var result = retailConnectionRepository.LoadActiveProductList(categoryId);
            result.ForEach(r =>
            {
                if (string.IsNullOrEmpty(r.ImageUrl))
                {
                    r.ImageUrl = Path.Combine(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.DomainName").Value, "Images\\blank-imagesm.png").Replace("\\", "/");
                }
                else
                {
                    var imageUrls = r.ImageUrl.Split('.');
                    var thumbnailImageUrl = string.Format("{0}sm.{1}", imageUrls[0], imageUrls[1]);
                    r.ImageUrl = Path.Combine(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.FileUploadPreviewDomainName").Value, thumbnailImageUrl).Replace("\\", "/");
                }
            });
            return result;
        }

        public ProductModel LoadActiveProductDetails(int productId)
        {
            var result = new ProductModel();
            var product = retailConnectionRepository.LoadProductDetails(productId);
            var retailers = retailConnectionRepository.LoadProductRetailers(productId);

            if (product.Status == TreadMarksConstants.ActiveStatus)
            {
                result.Id = product.ID;
                result.CategoryId = product.CategoryId;
                result.Title = product.Name;
                result.Brand = product.BrandName;
                result.ImageUrl = (product.ProductAttachment == null) ? Path.Combine(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.DomainName").Value, "Images\\blank-image.png").Replace("\\", "/") : Path.Combine(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.FileUploadPreviewDomainName").Value, product.ProductAttachment.FilePath).Replace("\\", "/");
                result.ProductInformation = product.Description;
                result.Retailers = retailers;

                return result;
            }
            return null;
        }
        #endregion
    }
}