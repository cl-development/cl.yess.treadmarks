﻿using CL.TMS.Common.Extension;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Claims;
using CL.TMS.Security.Authorization;
using CL.TMS.Common;
using CL.Framework.Common;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.Framework.BLL;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Configuration;
using CL.TMS.Security;

namespace CL.TMS.RPMBLL
{
    public class RPMClaimBO : BaseBO
    {
        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        public RPMClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;
        }

        public RPMClaimSummaryModel LoadRPMClaimSummary(int vendorId, int claimId)
        {
            var rpmClaimSummary = new RPMClaimSummaryModel();

            //Load status
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            rpmClaimSummary.ClaimCommonModel.Status = status;

            //Load ClaimPeriod
            rpmClaimSummary.ClaimCommonModel.ClaimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (rpmClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;

            //Load Inventory Opening data
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);
            rpmClaimSummary.TotalOpening = DataConversion.ConvertKgToTon((double)inventoryOpeningResult.TotalOpening);

            //Load Inbound and Outbound Transactions
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);
            PopulateInboundOutbound(rpmClaimSummary, claimDetails);

            //Load Inventory Adjustment
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateInventoryAdjustment(rpmClaimSummary, inventoryAdjustments);

            //Load sort yard capacity
            rpmClaimSummary.TotalCapacity = DataConversion.ConvertKgToTon((double)claimsRepository.LoadVendorSortYardCapacity(vendorId));

            //Load payment and adjustment
            //OTSTM2-1139 No talk back needed
            //UpdateGpBatchForClaim(claimId);

            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    rpmClaimSummary.RPMPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(rpmClaimSummary.RPMPayment, claimPaymentSummay, claimPayments, claimPaymentAdjustments);
            rpmClaimSummary.RPMPayment.PaymentDueDate = claim.ChequeDueDate;

            // set tax flag
            rpmClaimSummary.RPMPayment.IsTaxApplicable = claim.IsTaxApplicable ?? false;
            // calculate HST
            rpmClaimSummary.RPMPayment.HST = 0;
            if (rpmClaimSummary.RPMPayment.IsTaxApplicable)
            {
                var temp = rpmClaimSummary.RPMPayment.SPS + rpmClaimSummary.RPMPayment.PaymentAdjustment;
                //if (temp > 0) OTSTM2-1296 HST needs calculate for negative too.
                //{
                rpmClaimSummary.RPMPayment.HST = rpmClaimSummary.RPMPayment.UsingOldRounding ? Math.Round(temp * rpmClaimSummary.RPMPayment.HSTBase, 2)
                    : Math.Round(temp * rpmClaimSummary.RPMPayment.HSTBase, 2, MidpointRounding.AwayFromZero);
                //}
            }

            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                LoadStaffClaimSummary(rpmClaimSummary, claimId, vendorId, rpmClaimSummary.ClaimCommonModel.ClaimPeriod);

                if (status == ClaimStatus.Open)
                {
                    rpmClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    rpmClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}", claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }
                if (status == ClaimStatus.Approved)
                {
                    rpmClaimSummary.ClaimStatus.UnderReview = "Approved";
                }
                rpmClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499

                rpmClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                rpmClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                rpmClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            }

            ApplyUserSecurity(status, rpmClaimSummary);

            return rpmClaimSummary;
        }

        //OTSTM2-1139 No talk back needed
        //private void UpdateGpBatchForClaim(int claimId)
        //{
        //    var needPullBatch = claimsRepository.IsClaimPosted(claimId);
        //    if (needPullBatch)
        //    {
        //        var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);
        //        if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
        //        {
        //            var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
        //            var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
        //            if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
        //            {
        //                claimsRepository.UpdateClaimPaymentSummary(gpPaymentSummary, claimId);
        //            }
        //        }
        //    }
        //}
        private void ApplyUserSecurity(ClaimStatus status, RPMClaimSummaryModel rpmClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    rpmClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    rpmClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsRPMClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        rpmClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        private void LoadStaffClaimSummary(RPMClaimSummaryModel rpmClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            rpmClaimSummary.ClaimStatus = claimStatus;
            rpmClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy) ? string.Empty : string.Format("Under Review by {0}", claimStatus.ReviewedBy);

            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        private void PopulatPaymentAdjustments(RPMPaymentViewModel rpmPayment, ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            rpmPayment.SPS = claimPayments.Sum(c => rpmPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            rpmPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
            rpmPayment.isStaff = SecurityContextHelper.IsStaff();

            if (claimPaymentSummay != null)
            {
                rpmPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                rpmPayment.PaidDate = claimPaymentSummay.PaidDate;
                rpmPayment.MailedDate = claimPaymentSummay.MailDate;
                rpmPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                rpmPayment.BatchNumber = claimPaymentSummay.BatchNumber;               
            }
        }

        private void PopulateInventoryAdjustment(RPMClaimSummaryModel rpmClaimSummary, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            var query = inventoryAdjustments.GroupBy(c => new { c.Direction }).Select(c => new
            {
                Name = c.Key,
                Adjustmment = c.Sum(i => i.AdjustmentWeightOnroad + i.AdjustmentWeightOffroad)
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 1)
                {
                    rpmClaimSummary.AdjustIn = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }
                if (c.Name.Direction == 2)
                {
                    rpmClaimSummary.AdjustOut = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

                if (c.Name.Direction == 0)
                {
                    rpmClaimSummary.OverallAdjustments = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

            });
        }

        private void PopulateInboundOutbound(RPMClaimSummaryModel rpmClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            var query = claimDetails
              .GroupBy(c => new { c.TransactionType, c.Direction })
              .Select(c => new
              {
                  Name = c.Key,
                  ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero))
              });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && c.Name.Direction)
                {
                    rpmClaimSummary.SIR = c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPSR && !c.Name.Direction)
                {
                    rpmClaimSummary.SPS = c.ActualWeight;
                }
            });
        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = currentClaimSummary.TotalOpeningEstimated != null ? (decimal)currentClaimSummary.TotalOpeningEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = currentClaimSummary.TotalOpening != null ? (decimal)currentClaimSummary.TotalOpening : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                }
            }
            return inventoryOpeningSummary;
        }

        #region submit claim rules
        public RPMSubmitClaimViewModel RPMClaimSubmitBusinessRule(RPMSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>{
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel}
            };

            var updatedSubmitClaimForError = RPMClaimSubmitErrors(claim, ruleContext, submitClaimModel);
            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = RPMClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;
        }

        private RPMSubmitClaimViewModel RPMClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, RPMSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            this.BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);

            return submitClaimModel;

        }

        private void ExecuteBusinessRuleForErrors(RPMSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }

        private RPMSubmitClaimViewModel RPMClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, RPMSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new NilActivityClaimRule());
            this.BusinessRuleSet.AddBusinessRule(new NegativeClosingInventoryRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }

        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();

            // STEP 1 GET CLAIMS THAT ARE APPROVED AND NOT IN A BATCH	
            var claimType = (int)ClaimType.RPM;
            var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, true);
            var allClaimIds = allClaims.Select(c => c.ID).ToList();
            //Remove claim if it is banking status is not approved
            var claims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

            if (!claims.Any())
            {
                msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
                return msg;
            }

            //STEP 2 INSERT ENTRY INTO GpiBatch Table
            var gpiBatch = new GpiBatch()
            {
                GpiTypeID = GpHelper.GetGpiTypeID(GpManager.RPM),
                GpiBatchCount = claims.Count(),
                GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                CreatedDate = DateTime.UtcNow,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
            };

            var minPostDate = gpRepository.GetAllGpFiscalYears()
                .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                .Select(d => d.MinimumDocDate).FirstOrDefault();
            var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
            var claimIds = claims.Select(c => c.ID).ToList();

            ////SPS - outbound is Processor
            //var transactionTypes = new List<string> { "SPS" };

            //Now the claim ids are not going to match after Tony's change so moving it inside the method
            //var claimDetails = claimsRepository.LoadClaimDetailsForRPMGP(claimIds, transactionTypes);
            //var gpiAccounts = DataLoader.GPIAccounts;
            var fiAccounts = DataLoader.FIAccounts; //OTSTM2-1124

            //Update database
            using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                //1. GP Batch
                gpRepository.AddGpBatch(gpiBatch);
                msg.ID = gpiBatch.ID;

                //2. GP Batch entries
                var gpiBatchEntries = AddBatchEntries(claims, gpiBatch.ID);
                gpRepository.AddGpBatchEntries(gpiBatchEntries);

                //3. GP Transactions and Transaction Distributions
                var rrOtsPmTransaction = AddGPTransactions(claims, gpiBatch.ID, gpiBatchEntries, minPostDate, INTERID, fiAccounts);
                gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransaction.Keys.ToList());
                var rrOtsTransDistributions = new List<RrOtsPmTransDistribution>();
                rrOtsPmTransaction.Values.ToList().ForEach(c =>
                {
                    rrOtsTransDistributions.AddRange(c);
                });
                gpRepository.AddGpRrOtsPmTransDistributions(rrOtsTransDistributions);

                //4. Update claim
                claimsRepository.UpdateClaimsForGPBatch(claimIds, gpiBatch.ID.ToString());

                transactionScope.Complete();

            }
            return msg;
        }

        private List<GpiBatchEntry> AddBatchEntries(List<Claim> claims, int gpiBatchId)
        {
            return claims.Select(c => new GpiBatchEntry
            {
                GpiBatchID = gpiBatchId,
                //GpiTxnNumber = string.Format("{0}-{1}-{2}", c.Participant.Number, c.ClaimPeriod.ID, c.ID),
                GpiTxnNumber = string.Format("{0}-{1}-{2:yyyy MM dd}", c.ID.ToString(), c.Participant.Number, c.ClaimPeriod.StartDate),
                GpiBatchEntryDataKey = c.ID.ToString(),
                GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                CreatedDate = DateTime.UtcNow,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.UtcNow,
                ClaimId = c.ID
            }).ToList();
        }

        private Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>> AddGPTransactions(List<Claim> claims, int gpiBatchId, List<GpiBatchEntry> gpiBatchEntries, DateTime minPostDate, string INTERID, List<FIAccount> fiAccounts)
        {
            //Step 4 Add RM_Transaction
            var result = new Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>>();
            var rrOtsPmTransaction = new List<RrOtsPmTransaction>();

            var claimIds = claims.Select(c => c.ID).ToList();
            var items = DataLoader.Items;

            claims.ForEach(c =>
            {
                var rpmPaymentViewModel = LoadRPMPaymentForGP(c.ID);
                //DOCType=1
                //var prchAmount1 = Math.Round(rpmPaymentViewModel.SPS + rpmPaymentViewModel.PaymentAdjustment, 2, MidpointRounding.AwayFromZero);

                //OTSTM2-17
                var prchAmount1 = Math.Round(rpmPaymentViewModel.SPS + (rpmPaymentViewModel.PaymentAdjustment > 0 ? rpmPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                var prchTax1 = !c.IsTaxApplicable.HasValue || !c.IsTaxApplicable.Value  ? 0: Math.Round((rpmPaymentViewModel.SPS + (rpmPaymentViewModel.PaymentAdjustment > 0 ? rpmPaymentViewModel.PaymentAdjustment : 0)) * rpmPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var prchAmount5 = Math.Round(rpmPaymentViewModel.PaymentAdjustment < 0 ? rpmPaymentViewModel.PaymentAdjustment : 0, 2, MidpointRounding.AwayFromZero);
                var prchTax5 = !c.IsTaxApplicable.HasValue || !c.IsTaxApplicable.Value ? 0 : Math.Round(rpmPaymentViewModel.PaymentAdjustment < 0 ? rpmPaymentViewModel.PaymentAdjustment * rpmPaymentViewModel.HSTBase : 0, 2, MidpointRounding.AwayFromZero);

                var distributedAmount = Math.Round(rpmPaymentViewModel.SPS, 2, MidpointRounding.AwayFromZero);
                var distributedTax = !c.IsTaxApplicable.HasValue || !c.IsTaxApplicable.Value ? 0 : Math.Round(rpmPaymentViewModel.SPS * rpmPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);
                var distributedPaymentAdjustment = Math.Round(rpmPaymentViewModel.PaymentAdjustment > 0 ? rpmPaymentViewModel.PaymentAdjustment : 0, 2, MidpointRounding.AwayFromZero);
                var distributedPaymentAdjustmentTax = !c.IsTaxApplicable.HasValue || !c.IsTaxApplicable.Value ? 0 : Math.Round(rpmPaymentViewModel.PaymentAdjustment > 0 ? rpmPaymentViewModel.PaymentAdjustment * rpmPaymentViewModel.HSTBase : 0, 2, MidpointRounding.AwayFromZero);
                //var distributedAmount = Math.Round(rpmPaymentViewModel.SPS + (rpmPaymentViewModel.PaymentAdjustment < 0 ? rpmPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                //var distributedPaymentAdjustment = rpmPaymentViewModel.PaymentAdjustment > 0 ? rpmPaymentViewModel.PaymentAdjustment : 0;
                var gpiBatchEntryId = gpiBatchEntries.Single(q => q.ClaimId == c.ID).ID;
                int dstSeqNum = 1;

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                var typeOneTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    DOCTYPE = 1,
                    DOCAMNT = prchAmount1 + prchTax1,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = "P-EXEMPT",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount1,

                    TAXAMNT = prchTax1,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };

                if (prchAmount1 != 0)  //OTSTM2-17
                {
                    //Add transaction distribution
                    var currentClaimDetails = claimsRepository.LoadClaimDetailsForRPMGP(new List<int> { c.ID }, new List<string> { "SPS" }, items);
                    var typeOneDistrubutions = AddTypeOneDistributions(gpiBatchId, INTERID, distributedAmount, distributedTax, distributedPaymentAdjustment, distributedPaymentAdjustmentTax, typeOneTransaction, fiAccounts, currentClaimDetails, c.ClaimPeriod.StartDate, ref dstSeqNum); //OTSTM2-1124
                    result.Add(typeOneTransaction, typeOneDistrubutions);
                }

                //OTSTM2-17  DOCType=5            
                var typeFiveTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    DOCTYPE = 5,
                    DOCAMNT = prchAmount5 + prchTax5,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = "P-EXEMPT",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount5,

                    TAXAMNT = prchTax5,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };
                if (prchAmount5 != 0)
                {
                    //Add transaction distribution
                    var typeFiveDistrubutions = AddTypeFiveDistributions(gpiBatchId, INTERID, prchAmount5, prchTax5, typeFiveTransaction, fiAccounts, ref dstSeqNum); //OTSTM2-1124
                    result.Add(typeFiveTransaction, typeFiveDistrubutions);
                }
            });
            return result;
        }

        //OTSTM2-17
        private List<RrOtsPmTransDistribution> AddTypeFiveDistributions(int gpiBatchId, string INTERID, decimal prchAmount5, decimal prchTax5, RrOtsPmTransaction typeFiveTransaction, List<FIAccount> fiAccounts, ref int dstSeqNum)
        {
            var typeFiveDistrubutions = new List<RrOtsPmTransDistribution>();          
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Manufacturer Payable" && a.AdminFICategoryID == 5); //OTSTM2-1124

            var miAdjustment = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            
            var miAdjustmentHST = fiAccounts.FirstOrDefault(a => a.AccountLabel == "Internal Payment Adjustments" && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);


            if (prchAmount5 != 0)
            {
                prchAmount5 = Math.Abs(prchAmount5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = prchAmount5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)miAdjustment.distribution_type_id;
                rowTIAdjustment.ACTNUMST = miAdjustment.AccountNumber; //OTSTM2-1124
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchAmount5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            if (prchTax5 != 0)
            {
                prchTax5 = Math.Abs(prchTax5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber;
                rowAccountPayrable.DEBITAMT = prchTax5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)miAdjustmentHST.distribution_type_id;
                rowTIAdjustment.ACTNUMST = miAdjustmentHST.AccountNumber;
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchTax5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            return typeFiveDistrubutions;
        }

        private List<RrOtsPmTransDistribution> AddTypeOneDistributions(int gpiBatchId, string INTERID, decimal distributedAmount, decimal distributedAmountTax, decimal distributedPaymentAdjustment, decimal distributedPaymentAdjustmentTax, RrOtsPmTransaction typeOneTransaction, List<FIAccount> fiAccounts, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, ref int dstSeqNum)
        {
            var typeOneDistrubutions = new List<RrOtsPmTransDistribution>();
            //var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "M");
            //var PLTAccount = gpiAccounts.Single(a => a.account_number == "4201-10-50-40");
            //var MTAccount = gpiAccounts.Single(a => a.account_number == "4203-20-50-40");
            //var AGLSAccount = gpiAccounts.Single(a => a.account_number == "4206-30-50-40");
            //var INDAccount = gpiAccounts.Single(a => a.account_number == "4208-40-50-40");
            //var SOTRAccount = gpiAccounts.Single(a => a.account_number == "4211-50-50-40");
            //var MOTRAccount = gpiAccounts.Single(a => a.account_number == "4213-60-50-40");
            //var LOTRAccount = gpiAccounts.Single(a => a.account_number == "4216-70-50-40");
            //var GOTRAccount = gpiAccounts.Single(a => a.account_number == "4218-80-50-40");
            //var miAdjustment = gpiAccounts.Single(a => a.account_number == "4210-90-50-40");

            //OTSTM2-1124
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Manufacturer Payable" && a.AdminFICategoryID == 5);
            var PLTAccount = fiAccounts.FirstOrDefault(a =>  a.Name == "PLT"   && a.AdminFICategoryID == 5 && a.distribution_type_id==6);
            var MTAccount = fiAccounts.FirstOrDefault(a =>   a.Name == "MT"    && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var AGLSAccount = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS" && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var INDAccount = fiAccounts.FirstOrDefault(a =>  a.Name == "IND"   && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var SOTRAccount = fiAccounts.FirstOrDefault(a => a.Name == "SOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var MOTRAccount = fiAccounts.FirstOrDefault(a => a.Name == "MOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var LOTRAccount = fiAccounts.FirstOrDefault(a => a.Name == "LOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);
            var GOTRAccount = fiAccounts.FirstOrDefault(a => a.Name == "GOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 6);

            //Tax for all these accounts
            var PLTHSTAccount = fiAccounts.FirstOrDefault(a =>  a.AccountLabel == "PLT"   && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var MTHSTAccount = fiAccounts.FirstOrDefault(a =>   a.AccountLabel == "MT"    && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var AGLSHSTAccount = fiAccounts.FirstOrDefault(a => a.AccountLabel == "AG/LS" && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var INDHSTAccount = fiAccounts.FirstOrDefault(a =>  a.AccountLabel == "IND"   && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var SOTRHSTAccount = fiAccounts.FirstOrDefault(a => a.AccountLabel == "SOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var MOTRHSTAccount = fiAccounts.FirstOrDefault(a => a.AccountLabel == "MOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var LOTRHSTAccount = fiAccounts.FirstOrDefault(a => a.AccountLabel == "LOTR"  && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);
            var GOTRHSTAccount = fiAccounts.FirstOrDefault(a => a.AccountLabel == "GOTR" && a.AdminFICategoryID == 5 && a.distribution_type_id == 10);


            var miAdjustment = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 5 && a.distribution_type_id == 6); ;
            var miAdjustmentHST = fiAccounts.FirstOrDefault(a => a.AccountLabel == "Internal Payment Adjustments" && a.AdminFICategoryID == 5 && a.distribution_type_id == 10); ;

            var gpTireItem = FindPercentageByTireType(currentClaimDetails, claimStartDate);
            var subTotal = distributedAmount + distributedAmountTax+ distributedPaymentAdjustment + distributedPaymentAdjustmentTax;
            if (subTotal  != 0)
            {
                var rowAccountPayrable = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = 0;
                rowAccountPayrable.CRDTAMNT = subTotal;
                typeOneDistrubutions.Add(rowAccountPayrable);
            }

            if (distributedAmount != 0)
            {
                var amountDistributions = new List<RrOtsPmTransDistribution>();

                if (gpTireItem.TotalWeight != 0)
                {
                    //Add distribution records
                    if (gpTireItem.PLTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)PLTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = PLTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.AGLSPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)AGLSAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = AGLSAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.INDPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)INDAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = INDAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.SOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)SOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = SOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)LOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = LOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                    if (gpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)GOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = GOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(distributedAmount * gpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        amountDistributions.Add(rowDistribution);
                    }
                }
                else
                {
                    var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowDistribution.DISTTYPE = (int)PLTAccount.distribution_type_id;
                    rowDistribution.ACTNUMST = PLTAccount.AccountNumber; //OTSTM2-1124
                    rowDistribution.DEBITAMT = distributedAmount;
                    rowDistribution.CRDTAMNT = 0;
                    amountDistributions.Add(rowDistribution);
                }

                //Fixed cent difference caused by rounding
                var totalAmount = amountDistributions.Sum(c => c.DEBITAMT);
                if (totalAmount != distributedAmount)
                {
                    var difference = (totalAmount - distributedAmount);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < amountDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            amountDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }

                typeOneDistrubutions.AddRange(amountDistributions);
                // tax distribution
                if (distributedAmountTax != 0)
                {
                    var taxDistributions = new List<RrOtsPmTransDistribution>();
                    //Add distribution records
                    if (gpTireItem.TotalWeight != 0)
                    {
                        if (gpTireItem.PLTPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)PLTHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = PLTHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.MTPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)MTHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = MTHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.AGLSPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)AGLSHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = AGLSHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.INDPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)INDHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = INDHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.SOTRPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)SOTRHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = SOTRHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.MOTRPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)MOTRHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = MOTRHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.LOTRPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)LOTRHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = LOTRHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                        if (gpTireItem.GOTRPercentage > 0)
                        {
                            var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                            rowDistribution.DISTTYPE = (int)GOTRHSTAccount.distribution_type_id;
                            rowDistribution.ACTNUMST = GOTRHSTAccount.AccountNumber;
                            rowDistribution.DEBITAMT = Math.Round(distributedAmountTax * gpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                            rowDistribution.CRDTAMNT = 0;
                            taxDistributions.Add(rowDistribution);
                        }
                    } else
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)PLTHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = PLTHSTAccount.AccountNumber;
                        rowDistribution.DEBITAMT = Math.Round(distributedAmountTax, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        taxDistributions.Add(rowDistribution);
                    }
                    //Fixed cent difference caused by rounding
                    var totalTaxAmount = taxDistributions.Sum(c => c.DEBITAMT);
                    if (totalTaxAmount != distributedAmountTax)
                    {
                        var difference = (totalTaxAmount - distributedAmountTax);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < taxDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                taxDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                    typeOneDistrubutions.AddRange(taxDistributions);
                }
            }

            if (distributedPaymentAdjustment > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)miAdjustment.distribution_type_id;
                rowDistribution.ACTNUMST = miAdjustment.AccountNumber;
                rowDistribution.DEBITAMT = Math.Round(distributedPaymentAdjustment, 2, MidpointRounding.AwayFromZero);
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }
            if (distributedPaymentAdjustmentTax > 0)
            {
                var taxDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                taxDistribution.DISTTYPE = (int)miAdjustmentHST.distribution_type_id;
                taxDistribution.ACTNUMST = miAdjustmentHST.AccountNumber;
                taxDistribution.DEBITAMT = Math.Round(distributedPaymentAdjustmentTax, 2, MidpointRounding.AwayFromZero);
                taxDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(taxDistribution);
            }
            return typeOneDistrubutions;
        }

        private RrOtsPmTransDistribution GetTransDistribution(RrOtsPmTransaction gpTransaction, int gpiBatchId, string INTERID, int dstSeqNum)
        {
            var result = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = gpTransaction.VCHNUMWK,
                DOCTYPE = gpTransaction.DOCTYPE,
                VENDORID = gpTransaction.VENDORID,
                DSTSQNUM = dstSeqNum,
                DistRef = null,
                USERDEF1 = gpiBatchId.ToString(),
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null
            };
            return result;
        }

        private GpTireItem FindPercentageByTireType(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }

        private RPMPaymentViewModel LoadRPMPaymentForGP(int claimId)
        {
            var rpmPayment = new RPMPaymentViewModel();

            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);

            rpmPayment.SPS = claimPayments.Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            rpmPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
            

            return rpmPayment;
        }
        #endregion

    }
}