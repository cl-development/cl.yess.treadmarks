﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.UI.Common.Mobile
{
    public static class MobileMethods
    {

        public static string GetImagePathForMobile(string fullFileName)
        {
            Uri url = new Uri(ConfigurationManager.AppSettings["CentralHostName"]);
            Uri urlFinal = new Uri(url, string.Format("/images{0}", fullFileName.Replace("\\", "/")));
            return urlFinal.ToString();
        }

        //from central database conventions.....
        //outgoing if filename is like xxxx-1.png
        public static bool isOutgoingRegistrant(string fullfilePath)
        {
            string fileName = System.IO.Path.GetFileNameWithoutExtension(fullfilePath);
            string isOutgoingRegistrant = fileName.Substring(fileName.Length - 1, 1);
            return isOutgoingRegistrant == "1";
        }

    }
}
