﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Collections;
using System.Reflection;
using OfficeOpenXml.Table;
using System.ComponentModel.DataAnnotations;

namespace CL.TMS.UI.Common.Helper
{
    public static class ExcelFileHandler
    {
        public static void Export(ExcelPackage excelPackage)
        {
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.BinaryWrite(excelPackage.GetAsByteArray());
            HttpContext.Current.Response.AddHeader("content-disposition",
                      "attachment;filename=results.xlsx");
            HttpContext.Current.Response.ContentType = "application/excel";
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public static byte[] ExportListWithBytes<T>(List<T> data, string heading)
        {
            using (var package = new ExcelPackage())
            {
                List<int> dateColumn = new List<int>();
                ExcelWorksheet ws = package.Workbook.Worksheets.Add(heading);

                var t = typeof(T);
                var classProps = t.GetProperties();

                for (int i = 0; i < classProps.Count(); i++)
                {
                    ws.Cells[1, i + 1].Value = classProps[i].Name;

                    if (classProps[i].PropertyType == typeof(DateTime?))
                    {
                        dateColumn.Add(i + 1);
                    }
                }

                if (data.Count() > 0)
                {
                    ws.Cells["A2"].LoadFromCollection(data);
                }

                using (ExcelRange rng = ws.Cells["A1:BZ1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                foreach (int dateColVal in dateColumn)
                {
                    ws.Column(dateColVal).Style.Numberformat.Format = null;
                    ws.Column(dateColVal).Style.Numberformat.Format = "yyyy-mm-dd";
                    ws.Column(dateColVal).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                }

                var memoryStream = package.GetAsByteArray();

                return memoryStream;
            }
        }

        /// <summary>
        /// Exports the list with package.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <param name="heading">The heading.</param>
        /// <param name="package">The package.</param>
        /// <param name="timeCols">The time cols.</param>
        /// <param name="flags">The flags - to avoid derived attributes being exported.</param>
        public static void ExportListWithPackage<T>(IEnumerable<T> data, string heading, ExcelPackage package, List<string> timeCols = null, BindingFlags flags = System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly)
        {
            var dateColumn = new List<int>();
            var timeColumn = new List<int>();
            var formattedColumns = new Dictionary<int, string>();
            bool bIncludeColHeader = false;

            ExcelWorksheet ws = package.Workbook.Worksheets.Add(heading);

            var t = typeof(T);
            var classProps = t.GetProperties(flags);
            for (int i = 0; i < classProps.Count(); i++)
            {
                var prop = classProps[i];

                //Added code to show display names if they are there
                var dispName = prop.GetCustomAttributes(typeof(DisplayAttribute), false).Cast<DisplayAttribute>().FirstOrDefault();                
                ws.Cells[1, i + 1].Value = dispName != null ? dispName.Name : prop.Name;
                
                var att = (DisplayFormatAttribute)prop.GetCustomAttributes(typeof(DisplayFormatAttribute), true).FirstOrDefault();
                if (att != null)
                {
                    formattedColumns.Add(i + 1, att.DataFormatString);
                }
                if (timeCols != null)
                {
                    if (timeCols.Contains(prop.Name))
                    {
                        timeColumn.Add(i + 1);
                        continue;
                    }
                }
                if (prop.PropertyType == typeof(DateTime?) || prop.PropertyType == typeof(DateTime))
                {
                    dateColumn.Add(i + 1);
                }
            }

            if (data.Count() > 0)
            {
                ws.Cells["A2"].LoadFromCollection(data, bIncludeColHeader, TableStyles.None, flags, null);
            }

            using (ExcelRange rng = ws.Cells[1, 1, 1, classProps.Count()])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            foreach (var formattedColumn in formattedColumns) {
                ws.Column(formattedColumn.Key).Style.Numberformat.Format = null;
                if (string.Equals(formattedColumn.Value, "{0:0.00}")) {
                    ws.Column(formattedColumn.Key).Style.Numberformat.Format = "0.00";
                    ws.Column(formattedColumn.Key).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                }
                else if (string.Equals(formattedColumn.Value, "{0:C}"))
                {
                    ws.Column(formattedColumn.Key).Style.Numberformat.Format = "$0.00";
                    ws.Column(formattedColumn.Key).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                }        
            }

            foreach (int dateColVal in dateColumn)
            {
                ws.Column(dateColVal).Style.Numberformat.Format = null;
                ws.Column(dateColVal).Style.Numberformat.Format = "mm/dd/yyyy";
                ws.Column(dateColVal).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            }

            foreach (int dateColVal in timeColumn)
            {
                ws.Column(dateColVal).Style.Numberformat.Format = null;
                ws.Column(dateColVal).Style.Numberformat.Format = "mm/dd/yyyy h:mm:ss";
                ws.Column(dateColVal).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            }
        }
    }
}
