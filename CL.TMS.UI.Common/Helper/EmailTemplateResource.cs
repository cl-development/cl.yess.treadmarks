﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.UI.Common.Helper
{
    public static class EmailTemplateResource
    {
        public const string EmailTemplateLocation = @"Templates\PasswordResetTemplate.html";
        public const string RethinkTireImage = @"Images\RethinkTires.jpg";
        //public const string FacebookImage = @"Images\FacebookLogo.png";
        //public const string TwitterImage = @"Images\TwitterLogo.png";
        public const string RethinkTireID = "RethinkTires";
        public const string FollowOnTwitterImage = @"Images\follow_us_on_twitter.jpg";
        public const string FollowOnTwitterID = "follow_us_on_twitter";
        public const string TreadMarkLogoImage = @"Images\TreadMarksLogo.jpg";
        public const string TreadMarkLogoID = "@ApplicationLogo";
        public const string OTSEmailLogoImage = @"Images\OTS_Email_Logo.jpg";
        public const string OTSEmailLogoID = "@CompanyLogo";
        public const string siteURL = "@siteUrl";
        public const string datetimeNowYear = "@datetimeNowYear"; //OTSTM2-609
        public const string ClaimPeriod = "@ClaimPeriod";
    }
}
