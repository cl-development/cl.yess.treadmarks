﻿using CL.TMS.DataContracts.ViewModel.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.UI.Common
{
    public class ClaimBaseController : BaseController
    {
        protected void PopulateTransationUrlForStatusPanel(ClaimStatusViewModel claimStatus, string areaName, int claimId)
        {
            claimStatus.TotalTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.TotalTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.PaperTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.PaperTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.MobileTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.MobileTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.UnreviewedTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.UnreviewedTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.AdjustedTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.AdjustedTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.ApprovedTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.ApprovedTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.ReviewedTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.ReviewedTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.InvalidatedTransactionDetailsIn.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
            claimStatus.InvalidatedTransactionDetailsOut.ForEach(c =>
            {
                c.NavigationUrl = Url.Action("Index", "Transaction", new { area = areaName, id = claimId, type = c.TransactionType, direction = (c.Direction) ? 1 : 2 });
            });
        }
    }
}
