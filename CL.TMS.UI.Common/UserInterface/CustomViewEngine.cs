﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CL.TMS.UI.Common.UserInterface
{
    /// <summary>
    /// A custom view engine
    /// </summary>
    public class CustomViewEngine : RazorViewEngine
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CustomViewEngine()
        {
            base.PartialViewLocationFormats = new string[] {
                    "~/Views/Shared/ApplicationPartialViews/{0}.cshtml",
                    "~/Views/Shared/ApplicationPartialViews/EditorTemplates/{0}.cshtml",
                    "~/Views/Shared/Claims/{0}.cshtml",
                    "~/Views/Shared/Claims/Hauler/{0}.cshtml",
                    "~/Views/Shared/Claims/Collector/{0}.cshtml",
                    "~/Views/Shared/Claims/RPM/{0}.cshtml",
                    "~/Views/Shared/Claims/Processor/{0}.cshtml",
                    "~/Views/Shared/Claims/Common/{0}.cshtml",
                    "~/Views/Shared/Transaction/{0}.cshtml",
                    "~/Views/Shared/Transaction/TransactionViewTemplate/{0}.cshtml",
                    "~/Views/Shared/Transaction/Hauler/{0}.cshtml",
                    "~/Views/Shared/Transaction/Collector/{0}.cshtml",
                    "~/Views/Shared/Transaction/Processor/{0}.cshtml",
                    "~/Views/Shared/Transaction/RPM/{0}.cshtml",
                    "~/Views/Shared/Fileupload/{0}.cshtml",
                    "~/Views/Shared/FinancialIntegration/{0}.cshtml",
                    "~/Views/Shared/RetailConnection/{0}.cshtml",
                    "~/Views/Shared/GlobalSearch/{0}.cshtml",
                    "~/Views/Shared/Notification/{0}.cshtml",
                    "~/Views/Shared/AdminMenu/{0}.cshtml"
            }.Union(base.PartialViewLocationFormats).ToArray<string>();
        }
        /// <summary>
        /// Create a partial view
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="partialPath"></param>
        /// <returns></returns>
        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.CreatePartialView(controllerContext, partialPath.Replace("%1", nameSpace));
        }

        /// <summary>
        /// Create a view
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="viewPath"></param>
        /// <param name="masterPath"></param>
        /// <returns></returns>
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.CreateView(controllerContext, viewPath.Replace("%1", nameSpace), masterPath.Replace("%1", nameSpace));
        }

        /// <summary>
        /// Check if the file is existing
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            var nameSpace = controllerContext.Controller.GetType().Namespace;
            return base.FileExists(controllerContext, virtualPath.Replace("%1", nameSpace));
        }

    }
}
