﻿using CL.TMS.Common;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CL.TMS.UI.Common.Security
{
    /// <summary>
    /// Claims authorization attribute
    /// </summary>
    public class ClaimsAuthorizationAttribute : AuthorizeAttribute
    {
        private readonly string Resource;

        /// <summary>
        /// Claims Authorization using Resource Name and Permission
        /// </summary>
        /// <param name="resource">Resource Name</param>
        /// <param name="permission">Permission</param>
        public ClaimsAuthorizationAttribute(string resource)
        {
            this.Resource = resource;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) { throw new ArgumentNullException("filterContext"); }
            var result = ResourceActionAuthorizationHandler.CheckUserSecurity(Resource);
            if (result == SecurityResultType.NoAccess)
            {
                HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                if (result > SecurityResultType.ReadOnly)
                {
                    filterContext.Controller.ViewBag.IsEditable = true;
                }
                else
                {
                    filterContext.Controller.ViewBag.IsEditable = false;
                }
                base.OnAuthorization(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                                  new RouteValueDictionary
                                  {
                                       { "action", "UnauthorizedPage" },
                                       { "controller", "Common" },
                                       {  "area","System" }
                                  });
        }

    }
}
