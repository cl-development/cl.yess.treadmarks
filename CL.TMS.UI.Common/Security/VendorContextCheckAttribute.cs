﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.SystemServices;
using Unity.Attributes;
using System.Diagnostics;
using System.Web;
using System.IO;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using CL.TMS.ServiceContracts.StewardServices;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.UI.Common.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class VendorContextCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //RouteData routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(new HttpContext(((HttpRequest)(System.Web.HttpContext.Current.Request)), new HttpResponse(new StringWriter()))));
            //RouteData routeData3 = RouteTable.Routes.GetRouteData(filterContext.HttpContext);
            //var routeController = (string)routeData3.Values["controller"];
            //var routeAction = (string)routeData3.Values["action"];
            var routeData = filterContext.RouteData;
            var area = GetAreaName(routeData);
            var controller = filterContext.RouteData.Values["Controller"];
            var action = filterContext.RouteData.Values["Action"];
            //            var val = filterContext.ActionParameters["uid"];


            //List action parameter
            //List all scenarios base on area
            //1. Collector 2....
            //Controller....


            switch (area)
            {
                case "Collector":
                    DoCollectorActionFilter(filterContext);
                    break;
            }

            //RouteValueDictionary values = routeData.Values;
            //string controller = string.Empty, action = string.Empty, id = string.Empty;
            //int claimId = 0, vendorId = 0;
            //string area = routeData.DataTokens["area"] != null ? routeData.DataTokens["area"].ToString() : string.Empty;
            //HttpContext.Current.Request.InputStream.Position = 0;
            //string result = new System.IO.StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();//[HttpPost] doesn't have query string
            //string regNumber = string.Empty;



            //try
            //{
            //    foreach (System.Collections.Generic.KeyValuePair<string, object> key in values)
            //    {
            //        if (key.Key == "controller")
            //        {
            //            controller = key.Value.ToString();
            //        }
            //        if (key.Key == "action")
            //        {
            //            action = key.Value.ToString();
            //        }
            //        if (key.Key == "id")
            //        {
            //            id = key.Value.ToString();
            //        }
            //        Debug.WriteLine("key: " + key.Key + " value: " + key.Value);
            //    }
            //    Debug.WriteLine("area: " + area);

            //    JToken jObj;// = new JObject();
            //    Dictionary<string, string> keyValuePairs = null;// = new Dictionary<string, string>();
            //    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            //    object routes_list = null;

            //    if (((System.Web.HttpRequestWrapper)filterContext.HttpContext.Request).HttpMethod == "POST")
            //    {
            //        if (result.StartsWith("draw=1"))//Angular DataTables
            //        {
            //            //
            //        }
            //        try
            //        {
            //            jObj = JContainer.Parse(result); // If this throws exception then it is not a valid Json. i.e "{\"claimId\":\"139292\"}"
            //        }
            //        catch (Exception ex)
            //        {
            //            //throw;
            //        }

            //        try
            //        {
            //            keyValuePairs = result.Split('&').Select(value => value.Split('=')).ToDictionary(pair => pair[0], pair => pair[1]);//i.e "claimId=139292&transactionId=210399"
            //        }
            //        catch (Exception ex)
            //        {
            //            //throw;
            //        }
            //        try
            //        {
            //            routes_list = json_serializer.DeserializeObject(result);//"{\"id\":\"210399\",\"vendorType\":\"RPM\",\"adjRequested\":true,\"claimId\":\"139292\",\"adjustmentId\":null}"
            //        }
            //        catch (Exception ex)
            //        {
            //            //throw;
            //        }
            //    }

            //    if ((((System.Web.HttpRequestWrapper)filterContext.HttpContext.Request).HttpMethod == "POST") && (routes_list != null))
            //    {
            //        Type valueType = routes_list.GetType();
            //        if (valueType.IsGenericType)
            //        {
            //            Type baseType = valueType.GetGenericTypeDefinition();
            //            if (baseType == typeof(Dictionary<,>))//
            //            {
            //                foreach (KeyValuePair<string, object> entry in ((IEnumerable<KeyValuePair<string, object>>)routes_list))
            //                {
            //                    if (controller == "Transaction" && action == "initializePaperForm")
            //                    {
            //                        if (entry.Key.ToLower() == "id")
            //                        {
            //                            int.TryParse(entry.Value.ToString(), out claimId);
            //                        }
            //                    }
            //                    //if (area == "System" && controller == "Transactions" && action == "GetTransactionDetailData")
            //                    {
            //                        if (entry.Key.ToLower() == "claimid")
            //                        {
            //                            int.TryParse(entry.Value.ToString(), out claimId);
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }

            //    if ((((System.Web.HttpRequestWrapper)filterContext.HttpContext.Request).HttpMethod == "POST") && (keyValuePairs != null))
            //    {
            //        foreach (KeyValuePair<string, string> entry in (keyValuePairs))
            //        {
            //            if (((entry.Key.ToLower() == "claimid") || (entry.Key.ToLower() == "tsfclaimid")) && (claimId == 0))
            //            {
            //                int.TryParse(entry.Value.ToString(), out claimId);
            //            }
            //            if ((entry.Key.ToLower() == "vid") || (entry.Key.ToLower() == "vendorid"))
            //            {
            //                int.TryParse(entry.Value.ToString(), out vendorId);
            //            }
            //            if ((area.ToLower() == "steward" && controller.ToLower() == "tsfremittances") && (claimId == 0))
            //            {
            //                int.TryParse(entry.Value.ToString(), out claimId);
            //            }
            //            if (entry.Key == "draw" && entry.Value == "1")//datatable draw request
            //            {
            //                break;
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return;
            //}

            //if (filterContext.HttpContext.Request.QueryString.HasKeys())
            //{
            //    foreach (String key in filterContext.HttpContext.Request.QueryString.AllKeys)
            //    {
            //        if (!(key.StartsWith("columns") || key.StartsWith("order") || key.StartsWith("search") || key.StartsWith("_") || key.StartsWith("draw") || key.StartsWith("length") || key.StartsWith("start")
            //            || key.StartsWith("type") || key.StartsWith("direction") || key.StartsWith("DataType") || key.StartsWith("DataSubType") || key.StartsWith("direction")))
            //        {
            //            Debug.WriteLine(key.ToString() + " - " + filterContext.HttpContext.Request.QueryString[key]);
            //        }
            //    }
            //    if (filterContext.HttpContext.Request.QueryString["claimId"] != null)
            //    {
            //        int.TryParse(filterContext.HttpContext.Request.QueryString["claimId"].ToString(), out claimId);
            //    }

            //    if (filterContext.HttpContext.Request.QueryString["tsfClaimId"] != null)
            //    {
            //        int.TryParse(filterContext.HttpContext.Request.QueryString["tsfClaimId"].ToString(), out claimId);
            //    }

            //    if (filterContext.HttpContext.Request.QueryString["vId"] != null)
            //    {
            //        int.TryParse(filterContext.HttpContext.Request.QueryString["vId"].ToString(), out vendorId);
            //    }

            //    if (filterContext.HttpContext.Request.QueryString["regNumber"] != null)
            //    {
            //        regNumber = filterContext.HttpContext.Request.QueryString["regNumber"].ToString();
            //    }
            //}

            //if (controller == "Transaction" && action == "Index" && (!string.IsNullOrWhiteSpace(id) || (result.Length > 0)))//this id is ClaimId
            //{
            //    int.TryParse(id, out claimId);
            //    if (result.Length > 0)
            //    {
            //        //routes_list
            //    }
            //}

            //if (controller == "TSFRemittances" && action == "StaffIndex" && (!string.IsNullOrWhiteSpace(id) || (result.Length > 0)))//this id is ClaimId
            //{
            //    int.TryParse(id, out claimId);
            //    if (result.Length > 0)
            //    {
            //        //routes_list
            //    }
            //}

            //if (!SecurityContextHelper.IsStaff())
            //{
            //    //Participant User
            //    if (SecurityContextHelper.CurrentVendor != null)
            //    {
            //        if (!string.IsNullOrWhiteSpace(SecurityContextHelper.CurrentVendor.RedirectUrl))
            //        {
            //            filterContext.Result = new RedirectResult(SecurityContextHelper.CurrentVendor.RedirectUrl);
            //        }
            //    }
            //    else
            //    {
            //        filterContext.Result = new RedirectResult("~/System/Common/EmptyPage");
            //    }
            //}
            //else
            //{
            //    if (controller.ToLower() == "claims" && action.ToLower() == "claimsummary" && claimId > 0 && vendorId > 0)//trigger from global search, allow switch vendor context
            //    {
            //        return;
            //    }
            //}

            //var claimService = DependencyResolver.Current.GetService<IClaimService>();
            //var remittanceService = DependencyResolver.Current.GetService<ITSFRemittanceService>();
            //VendorReference vendorReference = null;
            //if (claimId > 0)
            //{
            //    if (controller.ToLower() == "tsfremittances" || regNumber.StartsWith("100"))
            //    {//customer
            //        vendorReference = remittanceService.GetVendorReference(claimId);
            //    }
            //    else
            //    {//vendor
            //        vendorReference = claimService.GetVendorReference(claimId);
            //    }
            //}


            //if (vendorReference != null && SecurityContextHelper.CurrentVendor != null && (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || SecurityContextHelper.CurrentVendor.VendorType != vendorReference.VendorType))//vendor and customer are load from different table
            //{
            //    if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
            //    {
            //        //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            //        filterContext.Result = new RedirectResult("~/System/Common/EmptyPage?navigationAction=loadTopMenu");
            //    }
            //    else
            //    {
            //        //return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
            //        filterContext.Result = new RedirectResult("~/System/Common/UnauthorizedPage");
            //    }
            //}
        }

        private void DoCollectorActionFilter(ActionExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }

        private string GetAreaName(RouteBase route)
        {
            var area = route as IRouteWithArea;
            if (area != null)
            {
                return area.Area;
            }
            var route2 = route as Route;
            if ((route2 != null) && (route2.DataTokens != null))
            {
                return (route2.DataTokens["area"] as string);
            }
            return null;
        }


        private string GetAreaName(RouteData routeData)
        {
            object obj2;
            if (routeData.DataTokens.TryGetValue("area", out obj2))
            {
                return (obj2 as string);
            }
            return GetAreaName(routeData.Route);
        }
    }
}
