﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Resources;
using System.Web.Mvc;

namespace CL.TMS.UI.Common.ExceptionHelper
{
    public class HandleClientException
    {
        public static void AddValidationErrors(ModelStateDictionary modelState, CLValidaitonException ex)
        {
            foreach (var validationError in ex.ValidationDetails.Errors)
            {
                if (validationError.FormattedMessagePlaceholderValues != null)
                {
                    var message = string.Format(MessageResource.ResourceManager.GetString(validationError.ErrorCode),
                        validationError.FormattedMessagePlaceholderValues.Values.ToArray());
                    modelState.AddModelError(validationError.ErrorCode, message);
                }
                else
                {
                    modelState.AddModelError(validationError.ErrorCode, MessageResource.ResourceManager.GetString(validationError.ErrorCode));
                }
            }
        }

        public static void AddDbConcurrencyErrors(ModelStateDictionary modelState, CLDbConcurrencyException ex)
        {
            modelState.AddModelError(ex.Message, MessageResource.ResourceManager.GetString(ex.Message));
        }
    }
}
