﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CL.TMS.UI.Common
{
    /// <summary>
    /// Base Controller
    /// </summary>
    [Authorize]
    public class BaseController : Controller
    {
        /// <summary>
        /// Begin execute core
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            var cultureCookie = Request.Cookies["culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                        Request.UserLanguages[0] :  
                        null;
            
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); 

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            //Parse TimeZoneInfo, ans store it in ViewBag
            ViewBag.TimeZoneInfo =TimeZoneInfo.Local; //Get local timezoninfor as default timezoneinfo
            var timeZoneCookie = Request.Cookies["_timeZoneInfo"];
            if (timeZoneCookie != null)
            {
                var clientTimeZoneString = timeZoneCookie.Value;
                var startIndex = clientTimeZoneString.IndexOf("(", StringComparison.OrdinalIgnoreCase);
                var endIndex = clientTimeZoneString.IndexOf(")", StringComparison.OrdinalIgnoreCase);
                var timeZoneInfoName = clientTimeZoneString.Substring(startIndex + 1, endIndex - startIndex - 1);
                var id = string.Empty;
                foreach (var zone in TimeZoneInfo.GetSystemTimeZones())
                {
                    if ((zone.DaylightName == timeZoneInfoName) || (zone.StandardName == timeZoneInfoName) || (zone.DisplayName == timeZoneInfoName))
                    {
                        id = zone.Id;
                        break;
                    }
                }
                if (!string.IsNullOrWhiteSpace(id))
                {
                    ViewBag.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(id);
                }             
            }

            ViewBag.ngApp = "DefaultApp";
            ViewBag.versionid = ConfigurationManager.AppSettings["AppVersion"];
            ViewBag.GoogleAPIKey= ConfigurationManager.AppSettings["GoogleAPIKey"];
            return base.BeginExecuteCore(callback, state);
        }

        protected DateTime ConvertTimeFromUtc(DateTime dt)
        {
            var destinationTimeZone = ViewBag.TimeZoneInfo as TimeZoneInfo;
            if (destinationTimeZone==null)
            {
                destinationTimeZone = TimeZoneInfo.Local;
            }

            return TimeZoneInfo.ConvertTimeFromUtc(dt, destinationTimeZone);
        }

        protected DateTime? ConvertTimeFromUtc(DateTime? dt)
        {
            if (null == dt)
            {
                return null;
            }
            DateTime parseResult = DateTime.MinValue;
            DateTime.TryParse(dt.ToString(), out parseResult);
            var destinationTimeZone = ViewBag.TimeZoneInfo as TimeZoneInfo;
            if (destinationTimeZone == null)
            {
                destinationTimeZone = TimeZoneInfo.Local;
            }

            return TimeZoneInfo.ConvertTimeFromUtc(parseResult, destinationTimeZone);
        }

        protected DateTime ConvertTimeToUtc(DateTime dt)
        {
            var sourceTimeZone = ViewBag.TimeZoneInfo as TimeZoneInfo;
            if (sourceTimeZone == null)
            {
                sourceTimeZone = TimeZoneInfo.Local;
            }

            return TimeZoneInfo.ConvertTimeToUtc(dt, sourceTimeZone);
        }
    }
}
