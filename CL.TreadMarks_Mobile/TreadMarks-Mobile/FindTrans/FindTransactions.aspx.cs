﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TreadMarks_Mobile
{
    public partial class FindTransactions : System.Web.UI.Page
    {
        //default sort
        string Sort_Direction = "syncDate desc";
       
        protected void Page_Load(object sender, EventArgs e)
        {                
            if (!IsPostBack)
            {
                dpSyncDate.SelectedDate = DateTime.Now;
                BindSyncStatusList();                           
                PopulateGrid();                
            }
        }

        private void PopulateGrid()
        {
            ViewState["SortExpr"] = Sort_Direction;
            DataView transactions = Getdata();
            gdTransactions.DataSource = transactions;
            gdTransactions.DataBind();
        }

        private DataView Getdata()
        {
            string filter = GetFilter();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString))
            {
                DataSet dsTrans = new DataSet();
                string sqlCommand = @"SELECT friendlyId, t.createdDate transactionStartDate, transactionDate, t.syncDate, responseComments, deviceName, 
                                                        tt.shortNameKey _type, tsst.nameKey sync_status, tst.nameKey _status, incomingRegistrationNumber, 
                                                        outgoingRegistrationNumber, incomingUserId, versionBuild, createdUserId, outgoingUserId,                                                          
                                                        CAST(t.transactionSyncStatusTypeId AS nvarchar) AS transactionSyncStatusTypeId, 
                                                        t.transactionStatusTypeId,  t.transactionTypeId, 
                                                        transactionId FROM [Transaction] t
                                                        inner join TransactionType tt on t.TransactionTypeid=tt.transactionTypeId
                                                        inner join TransactionStatusType tst on t.transactionStatusTypeId=tst.transactionStatusTypeId
                                                        inner join TransactionSyncStatusType tsst on t.transactionSyncStatusTypeId=tsst.transactionSyncStatusTypeId
                                                        where 1=1 " + filter;

                SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);
                da.Fill(dsTrans, "Transactions");
                DataView dvTrans = dsTrans.Tables["Transactions"].DefaultView;
                dvTrans.Sort = ViewState["SortExpr"].ToString();
                lblTotal.Visible = true;
                lblTotal.Text = "Total rows: " + dvTrans.Count.ToString();
                cmdExport.Enabled = dvTrans.Count > 0;
                return dvTrans;
            }
        }

        private string GetFilter()
        {
            string filter = "";
            string syncStatuses = Check_Clicked();
            if (syncStatuses != string.Empty)
                filter = String.Format(" and t.transactionSyncStatusTypeId IN ({0})", syncStatuses);

            if (txtDevice.Text != string.Empty)
                filter += String.Format(" and deviceName='{0}' ", txtDevice.Text);

            if (txtFriendlyId.Text != string.Empty)
                filter += String.Format(" and friendlyId ='{0}' ", txtFriendlyId.Text);

            if (txtIncomingReg.Text != string.Empty)
                filter += String.Format(" and (incomingRegistrationNumber ='{0}' or outgoingRegistrationNumber = '{1}') ", txtIncomingReg.Text, txtIncomingReg.Text);

            if (dpTDate.SelectedDate != DateTime.MinValue)
                filter += String.Format(" and CAST(t.transactiondate AS DATE) = CAST('{0}' AS DATE) ", dpTDate.SelectedDate.ToString("yyyy-MM-dd"));

            if (dpSyncDate.SelectedDate != DateTime.MinValue)
                filter += String.Format(" and CAST(t.syncDate AS DATE) = CAST('{0}' AS DATE) ", dpSyncDate.SelectedDate.ToString("yyyy-MM-dd"));
            
            return filter;
        }

        protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdTransactions.PageIndex = e.NewPageIndex;
            DataView dvEmployee = Getdata();
            gdTransactions.DataSource = dvEmployee;
            gdTransactions.DataBind();
        }

        protected void Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] SortOrder = ViewState["SortExpr"].ToString().Split(' ');
            if (SortOrder[0] == e.SortExpression)
            {
                if (SortOrder[1] == "ASC")
                {
                    ViewState["SortExpr"] = e.SortExpression + " " + "DESC";
                }
                else
                {
                    ViewState["SortExpr"] = e.SortExpression + " " + "ASC";
                }
            }
            else
            {
                ViewState["SortExpr"] = e.SortExpression + " " + "ASC";
            }
            gdTransactions.DataSource = Getdata();
            gdTransactions.DataBind();
        }

        protected void cmdFind_Click(object sender, EventArgs e)
        {
            PopulateGrid();
        }

        string Check_Clicked()
        {
            // Iterate through the Items collection of the CheckBoxList 
            List<string> strSyncStatus = new List<string>();
            for (int i = 0; i < cblSyncStatus.Items.Count; i++)
            {
                if (cblSyncStatus.Items[i].Selected)
                {
                    strSyncStatus.Add(cblSyncStatus.Items[i].Value);
                }
            }
            return String.Join(",", strSyncStatus);
        }

        void BindSyncStatusList()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TreadMarks-MobileConnectionString"].ConnectionString))
            {
                DataSet dsSync = new DataSet();
                string sqlCommand = @"SELECT DISTINCT [transactionSyncStatusTypeId], [nameKey], [internalDescription], [userMessage] FROM [TransactionSyncStatusType]";

                SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);
                da.Fill(dsSync, "SyncStatus");

                cblSyncStatus.DataSource = dsSync;
                cblSyncStatus.DataTextField="nameKey";
                cblSyncStatus.DataValueField = "transactionSyncStatusTypeId";
                cblSyncStatus.DataBind();
                //cblSyncStatus.SelectedValue = "6";
            }
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            // set the resulting file attachment name to the name of the report...
            string fileName = String.Format("Transactions-{0}", DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]"));

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Get the header row text form the sortable columns
            LinkButton headerLink = new LinkButton();
            string headerText = string.Empty;

            for (int k = 0; k < gdTransactions.HeaderRow.Cells.Count; k++)
            {
                //add separator
                headerLink = gdTransactions.HeaderRow.Cells[k].Controls[0] as LinkButton;
                headerText = headerLink.Text;
                sb.Append(headerText + ",");
            }
            //append new line
            sb.Append("\r\n");

            DataTable dt = Getdata().Table;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < gdTransactions.HeaderRow.Cells.Count; k++)
                {
                    //have to replace new lines and \n .. there are so many of them in OTS data
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", "").Replace(System.Environment.NewLine, "").Replace("\n", "") + ",");
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}