﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrantMapper.aspx.cs" Inherits="TreadMarks_Mobile.RegistrantMapper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mobile Badge App User ID Mapping</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Mobile Badge App User ID Mapping
            </h2>
            <br />

            <asp:TextBox ID="txtSearch" runat="server" placeholder="Registration #"  />&nbsp;            
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" formnovalidate="formnovalidate" />
            <br /><br />

            <table class="table">
                <tr class="header">
                    <th>Registration Number</th>
                    <th>UserId</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Access Type Id</th>
                    <th>Action</th>
                </tr>

                <asp:Repeater ID="rptRegistrantMapping" runat="server" OnItemCommand="rptRegistrantMapping_OnItemCommand" 
                    OnItemDataBound="rptRegistrantMapping_OnItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <FooterTemplate>
                        <tr>
                            <td colspan="6">
                                <center>
                                 <asp:Label ID="lblEmptyMsg" runat="server" Text="Registrant does not exist" />                              
                               </center>

                            </td>
                        </tr>
                    </FooterTemplate>
                    <ItemTemplate>
                        <tr class='row <%# Container.ItemIndex % 2 == 0 ? string.Empty : "row-alt" %>'>
                            <td><%#Eval("registrationNumber") %></td>
                            <td>
                                <%#Eval("userId") %>
                            </td>
                            <td><%#Eval("Name") %></td>
                            <td><%#Eval("Email") %></td>
                            <td>                                
                                <%# Eval("accessTypeId") %>
                            </td>
                            <td>
                                <a href="javascript:void(0);" class="btn-edit">Edit</a>
                                <a href="javascript:void(0);" onclick="window.open('/qrcode.aspx?reg=<%#Eval("registrationNumber") %>&uid=<%#Eval("userId") %>','','width=400,height=500');" class="btn-qrcode" >QR Code</a>
                            </td>
                        </tr>
                        <tr class="row-edit <%# Container.ItemIndex % 2 == 0 ? string.Empty : "row-alt" %>">
                            <td>
                                <asp:TextBox ID="txtRegistrationNumber" runat="server" Enabled="false" Text='<%#Eval("registrationNumber") %>' CssClass="validateNumber" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtUserId" runat="server" Enabled="false" Text='<%#Eval("userId") %>' CssClass="validateNumber" />

                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Text='<%#Eval("Name") %>' />

                            </td>
                            <td>
                                <asp:TextBox ID="TxtEmail" runat="server" Text='<%#Eval("Email") %>' />

                            </td>
                            <td>
                                <asp:TextBox ID="txtAccessTypeId" runat="server" Text='<%#Eval("accessTypeId") %>' CssClass="validateNumber" />

                            </td>
                            <td>
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn-save" Text="Save" CommandName="Edit" />
                                <a href="javascript:void(0);" class="btn-cancel">Cancel</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                   
                </asp:Repeater>

                <tr class="row">
                    <td>
                        <asp:TextBox ID="txtRegAdd" runat="server" required="required"  pattern="[0-9]{7}" title="Registration # must be 7 digits" placeholder="Registration #"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserIdAdd" runat="server" required="required" CssClass="validateNumber" placeholder="User Id"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNameAdd" runat="server" placeholder="Name"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailAdd" runat="server" placeholder="Email" ></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAccessTypeIdAdd" runat="server" required="required" CssClass="validateNumber" placeholder="Access Type Id"></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnAdd" Style="display: none" runat="server" Text="Add" CausesValidation="true" OnClick="btnAdd_OnClick" ValidationGroup="vgAdd" />
                        <input type="submit" id="btnAdd" name="btnAdd" value="Insert" />
                    </td>
                </tr>
            </table>
        </div>

        <asp:Panel ID="pnlPager" runat="server" Visible="false">

            <div class="pager-panel">
                <asp:LinkButton ID="lbtnFirst" runat="server" CausesValidation="false" OnClick="lbtnFirst_Click">First</asp:LinkButton>

            </div>
            <div class="pager-panel">
                <asp:LinkButton ID="lbtnPrevious" runat="server" CausesValidation="false" OnClick="lbtnPrevious_Click">Previous</asp:LinkButton>
            </div>

            <div class="pager-panel">
            <asp:DataList ID="dlPaging" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPaging_ItemCommand"
                OnItemDataBound="dlPaging_ItemDataBound">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CssClass="pager"
                        CommandName="Paging" Text='<%# Eval("PageText") %>'></asp:LinkButton>&nbsp;
                </ItemTemplate>
            </asp:DataList>
            </div>

            <div class="pager-panel">
            <asp:LinkButton ID="lbtnNext" runat="server" CausesValidation="false"
                OnClick="lbtnNext_Click">Next</asp:LinkButton>
            </div>
            <div class="pager-panel">
                <asp:LinkButton ID="lbtnLast" runat="server" CausesValidation="false" OnClick="lbtnLast_Click">Last</asp:LinkButton>
            </div>

            <asp:Label ID="lblPageInfo" Visible="false" runat="server"></asp:Label>
        </asp:Panel>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript">
            $(function () {

                $('.validateNumber').keydown(function (e) {
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                        return;
                    }
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                $('.btn-edit').click(function () {
                    var index = $(".btn-edit").index(this);
                    $('tr.row').show();
                    $('tr.row').eq(index).hide();
                    $('tr.row-edit').hide();
                    $('tr.row-edit').eq(index).fadeIn();
                });

                $('.btn-cancel').click(function () {
                    var index = $(".btn-cancel").index(this);
                    $('tr.row-edit').eq(index).hide();
                    $('tr.row').eq(index).fadeIn();
                });
            });

        </script>


        <!--
<script type="text/javascript" src="http://typecast.com/project_css/VKgxHNGLCX/103243db0122c6.js"></script>
-->
        <!--
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
-->
<link href='css/style.css' rel='stylesheet' type='text/css'>

    </form>
</body>
</html>
