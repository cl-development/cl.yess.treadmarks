﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace TreadMarks_Mobile
{
    public sealed class Helper
    {
        #region Constructor

        private Helper() { }

        #endregion

        #region Conversion Helpers

        /// <summary>
        /// C# to convert a unixTimeStamp to a datetime. 
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Double.Parse(Regex.Match(unixTimeStamp, @"\d+").Value) / 1000).ToUniversalTime();
            return dtDateTime;
        }

        #endregion
    }
}
