﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qrcode.aspx.cs" Inherits="TreadMarks_Mobile.qrcode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>QR Code</title>
    <style type="text/css" media="screen">
    	body { margin: 0; }
    </style>
    <style type="text/css" media="print">
        .DoNotPrint {
            display:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 0px;">
        <asp:Image ID="imgOutput" runat="server" style="float: left; margin-right: 100px;" />
        <div style="clear: left;"></div>
    </div>
    <div class="DoNotPrint" style="width:100%;text-align:center;">
        <a href="#" onclick='window.print();return false;'>Print</a>
        | 
        <a href="#" onclick="window.close();">Close</a>

        test
    </div>
    </form>
</body>
</html>
