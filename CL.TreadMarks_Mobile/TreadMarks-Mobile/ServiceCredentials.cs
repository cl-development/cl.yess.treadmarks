﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Runtime.Serialization;

namespace TreadMarks_Mobile
{
    /// <summary>
    /// Web service requestor credentials validation.
    /// Passed credentials via the web service call are validated against username/password pair specified in Web.config file.
    /// </summary>
    [DataContract]
    public class ServiceCredentials
    {
        [DataMember]  
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }

        public bool Validate()
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                return false;

            if (ConfigurationManager.AppSettings["serviceUserName"] != username || ConfigurationManager.AppSettings["servicePassword"] != password)
                return false;
          
            return true;
        }
    }
}
