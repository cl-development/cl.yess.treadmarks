namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionType_RegistrantTypeModel : ITransactionType_RegistrantTypeModel
    {
        public TransactionType_RegistrantTypeModel()
        { }
        [DataMember(Name = "transactionTypeRegistrantTypeId")]
        public long TransactionTypeRegistrantTypeId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "registrantTypeId")]
        public long RegistrantTypeId { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
    }
}
