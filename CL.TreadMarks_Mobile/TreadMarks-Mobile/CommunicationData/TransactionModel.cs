namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionModel : ITransactionModel
    {
        public TransactionModel()
        {
            this.Photos = new HashSet<IPhotoModel>();
            this.Transaction_TireTypes = new HashSet<ITransaction_TireTypeModel>();
        }

        [DataMember(Name = "transactionId")]
        public System.Guid TransactionID { get; set; }
        [DataMember(Name = "friendlyId")]
        public long FriendlyId { get; set; }
        [DataMember(Name = "transactionDate")]
        public System.DateTime TransactionDate { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
        [DataMember(Name = "outgoingUserId")]
        public long OutgoingUserId { get; set; }
        [DataMember(Name = "outgoingGpsLogId")]
        public Guid OutgoingGpsLogId { get; set; }
        [DataMember(Name = "incomingUserId")]
        public long IncomingUserId { get; set; }
        [DataMember(Name = "incomingGpsLogId")]
        public Guid? IncomingGpsLogId { get; set; }
        [DataMember(Name = "registrationNumber")]
        public decimal RegistrationNumber { get; set; }
        [DataMember(Name = "transactionStatusTypeId")]
        public long TransactionStatusTypeId { get; set; }
        [DataMember(Name = "transactionSyncStatusTypeId")]
        public long TransactionSyncStatusTypeId { get; set; }
        [DataMember(Name = "trailerNumber")]
        public string TrailerNumber { get; set; }
        [DataMember(Name = "trailerLocationId")]
        public Nullable<long> TrailerLocationId { get; set; }
        [DataMember(Name = "outgoingSignaturePhotoId")]
        public Nullable<Guid> OutgoingSignaturePhotoId { get; set; }
        [DataMember(Name = "outgoingSignatureName")]
        public string OutgoingSignatureName { get; set; }
        [DataMember(Name = "incomingSignaturePhotoId")]
        public Nullable<Guid> IncomingSignaturePhotoId { get; set; }
        [DataMember(Name = "incomingSignatureName")]
        public string IncomingSignatureName { get; set; }
        [DataMember(Name = "createdUserId")]
        public long CreatedUserId { get; set; }
        [DataMember(Name = "createdDate")]
        public System.DateTime CreatedDate { get; set; }
        [DataMember(Name = "modifiedUserId")]
        public Nullable<long> ModifiedUserId { get; set; }
        [DataMember(Name = "modifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        public virtual ICollection<IPhotoModel> Photos { get; set; }
        public virtual ICollection<ITransaction_TireTypeModel> Transaction_TireTypes { get; set; }
        public virtual ICollection<ITransaction_EligibilityModel> Transaction_EligibilityList { get; set; }
        public ILocationModel TrailerLocation { get; set; }
        public virtual ICollection<ICommentModel> Comments { get; set; }
        public ITransaction_EligibilityModel Transaction_Eligibility { get; set; }
        public string ResponseComments { get; set; }
        [DataMember(Name = "postalCode1")]
        public string PostalCode1 { get; set; }
        [DataMember(Name = "postalCode2")]
        public string PostalCode2 { get; set; }

        [DataMember(Name = "outgoingRegistrationNumber")]
        public decimal OutgoingRegistrationNumber { get; set; }

        [DataMember(Name = "deviceName")]
        public string DeviceName { get; set; }

        [DataMember(Name = "versionBuild")]
        public string VersionBuild { get; set; }
        
        [DataMember(Name = "deviceId")]
        public string DeviceId { get; set; }
        public ISTCAuthorizationModel STCAuthorization { get; set; }
        public IAuthorizationModel Authorization { get; set; }
        public ILocationModel STCLocation { get; set; }
        public IScaleTicketModel InboundScaleTicket { get; set; }
        public IScaleTicketModel OutboundScaleTicket { get; set; }
        public IScaleTicketModel ScaleTicketBoth { get; set; }
        //Dennis is sending registrant and location in companyInfo
        [DataMember(Name = "companyInfo")]
        public RegistrantModel CompanyInfo { get; set; }
        public ICompanyModel Company { get; set; }
    
    }
}
