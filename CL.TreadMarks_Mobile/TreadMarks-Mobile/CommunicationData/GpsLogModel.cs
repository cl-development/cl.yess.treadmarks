namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class GpsLogModel : IGpsLogModel
    {
        public GpsLogModel()
        { }
        [DataMember(Name = "gpsLogId")]
        public System.Guid GpsLogId { get; set; }
        [DataMember(Name = "latitude")]
        public decimal Latitude { get; set; }
        [DataMember(Name = "longitude")]
        public decimal Longitude { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "timestamp")]
        public System.DateTime TimeStamp { get; set; }
    }
}
