namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionTypeModel : ITransactionTypeModel
    {
        public TransactionTypeModel()
        {            
        }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "shortNameKey")]
        public string ShortNameKey { get; set; }
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
        [DataMember(Name = "sortIndex")]
        public int SortIndex { get; set; }
        [DataMember(Name = "outgoingSignatureKey")]
        public string OutgoingSignatureKey { get; set; }
        [DataMember(Name = "incomingSignatureKey")]
        public string IncomingSignatureKey { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "tireCountMessageKey")]
        public string TireCountMessageKey { get; set; }
    }
}
