namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class PhotoTypeModel : IPhotoTypeModel
    {
        public PhotoTypeModel()
        { }
        [DataMember(Name = "photoTypeId")]
        public int PhotoTypeId { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
