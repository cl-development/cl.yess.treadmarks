﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Runtime.Serialization;

namespace TreadMarks_Mobile
{
    /// <summary>
    /// Web service requestor credentials validation.
    /// Passed credentials via the web service call are validated against username/password pair specified in Web.config file.
    /// </summary>
    [DataContract]
    public class IncomingDateModel
    {
        [DataMember(Name = "lastUpdated")]       
        public DateTime lastUpdated { get; set; }        
    }
}
