namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]    
    public partial class STCAuthorizationModel : ISTCAuthorizationModel
    {
        public STCAuthorizationModel()
        {
            Authorization = AuthorizationModel;
            Location = LocationModel;
        }

        [DataMember(Name = "authorization")]
        public AuthorizationModel AuthorizationModel { get; set; }

        [DataMember(Name = "location")]
        public LocationModel LocationModel { get; set; }

        public IAuthorizationModel Authorization { get {return AuthorizationModel;} set{} }
        public ILocationModel Location { get {return LocationModel;} set{} }
    }
}
