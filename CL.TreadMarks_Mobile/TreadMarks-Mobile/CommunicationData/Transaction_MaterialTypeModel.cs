namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class Transaction_MaterialTypeModel : ITransaction_MaterialTypeModel
    {
        public Transaction_MaterialTypeModel()
        { }
        [DataMember(Name = "transactionMaterialTypeId")]
        public System.Guid TransactionMaterialTypeId { get; set; }
        [DataMember(Name = "materialTypeId")]
        public long MaterialTypeId { get; set; }
        [DataMember(Name = "transactionId")]
        public System.Guid TransactionId { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
