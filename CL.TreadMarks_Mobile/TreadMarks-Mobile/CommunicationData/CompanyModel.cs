namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class CompanyModel :ICompanyModel
    {
        public CompanyModel()
        {            
        }

        public int CompanyId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public Guid TransactionId { get; set; }        
    
        
    }
}
