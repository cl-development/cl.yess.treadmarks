﻿namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class TireTypeModel : ITireTypeModel
    {
        public TireTypeModel()
        {
            this.Transaction_TireType = new HashSet<ITransaction_TireTypeModel>();
            this.TransactionType_TireType = new HashSet<ITransactionType_TireTypeModel>();
        }

        [DataMember(Name = "tireTypeId")]
        public long TireTypeId { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "sortIndex")]
        public long SortIndex { get; set; }
        [DataMember(Name = "estimatedWeight")]
        public decimal EstimatedWeight { get; set; }
        [DataMember(Name = "effectiveStartDate")]
        public System.DateTime EffectiveStartDate { get; set; }
        [DataMember(Name = "effectiveEndDate")]
        public System.DateTime EffectiveEndDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        [DataMember(Name = "shortNameKey")]
        public string ShortNameKey { get; set; }
    
        public virtual ICollection<ITransaction_TireTypeModel> Transaction_TireType { get; set; }
        public virtual ICollection<ITransactionType_TireTypeModel> TransactionType_TireType { get; set; }

    }
}
