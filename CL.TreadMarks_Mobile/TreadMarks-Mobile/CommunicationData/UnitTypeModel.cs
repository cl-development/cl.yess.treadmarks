namespace TreadMarks_Mobile.CommunicationData
{
    using TreadMarksContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class UnitTypeModel : IUnitTypeModel
    {
        public UnitTypeModel()
        { }
        [DataMember(Name = "unitTypeId")]
        public long UnitTypeId { get; set; }
        [DataMember(Name = "kgMultiplier")]
        public double KgMultiplier { get; set; }
        [DataMember(Name = "nameKey")]
        public string NameKey { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
