﻿using System;
using System.Diagnostics;
using otscommon.ExtensionMethods;

public static class Logger
{
    //System.ServiceModel will catch server error messages as well e.g. unexpected incoming message format
    private static TraceSource ts = new TraceSource("Logger");      
    public static void Error(string message, string module)
    {
        WriteEntry(message, TraceEventType.Error, module);
    }

    public static void Error(Exception ex, string module)
    {
        WriteEntry(ex.GetAllMessages() + ex.StackTrace.Substring(0, 100), TraceEventType.Error, module);
    }

    public static void Warning(string message, string module)
    {
        WriteEntry(message, TraceEventType.Warning, module);
    }

    public static void Info(string message, string module)
    {
        WriteEntry(message, TraceEventType.Information, module);
    }

    private static void WriteEntry(string message, TraceEventType type, string module)
    {
        ts.TraceEvent(type,  (int)type, string.Format("{0},{1},{2}",
                              DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                              module,
                              message));
        ts.Flush(); 
        ts.Close();        
    }
}