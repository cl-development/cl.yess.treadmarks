﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using otscommon.ExtensionMethods;
using System.Web;

namespace TreadMarks_Mobile
{
    public class GlobalExceptionHandler : IErrorHandler
    {
        #region IErrorHandler Members

        public bool HandleError(Exception ex)
        {
            return true;
        }

        public void ProvideFault(Exception ex, MessageVersion version,
                                 ref Message msg)
        {
            var newEx = new FaultException(
                string.Format("WS Global Exception triggered by {0}",
                              ex.TargetSite.Name));
            
            MessageFault msgFault = newEx.CreateMessageFault();
            msg = Message.CreateMessage(version, msgFault, newEx.Action);
            Logger.Error(msg.ToString() + " Exception message: " + ex.GetAllMessages(), "Global Exception");            
        }

        #endregion
    }
}