﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Xsl;

namespace TreadMarks_Mobile.HelpPage
{
    public class HelpPageGenerator
    {        
        /// <summary>
        /// This XmlDocument based attribute contains the 
        /// XML documentation for the project.
        /// </summary>
        /// <permission cref="System.Security.PermissionSet">private</permission>
        private XmlDocument m_xdSourceXML = new XmlDocument();

        /// <value>
        /// The SourceXML property contains the XML that will be used in the
        /// transformations by the member functions for this class.
        /// </value>
        /// <permission cref="System.Security.PermissionSet">public</permission>
        public string SourceXML
        {
            get
            {
                return m_xdSourceXML.InnerXml;
            }
            set
            {
                m_xdSourceXML.LoadXml(SourceXML);
            }
        }

        public void LoadXMLFromFile(string strFilePath)
        {
            m_xdSourceXML.Load(strFilePath);
        }

        public string HelpAPIListHTMLPage()
        {
            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageAPI.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                    new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            xslTransformFile.Transform(m_xdSourceXML, null, tw);

            return tw.ToString();
        }

        public string HelpMemberListHtmlPage(string strType)
        {
            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageMemberList.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                    new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            // the strType parameter is passed into the stylesheet
            XsltArgumentList arglist = new XsltArgumentList();
            arglist.AddParam("WhichType", "", strType);

            xslTransformFile.Transform(m_xdSourceXML, arglist, tw);

            return tw.ToString();
        }

        public string HelpMemberHTMLHelp(string strMember)
        {
            // A small kludge to get around an encoding problem.
            // #ctor gets striped as it looks like a bookmark.
            // If we get a member that ends in '.', we append the constructor 
            // value
            if (strMember.EndsWith(".") == true)
            {
                strMember += "#ctor";
            }

            // Load the corresponding XSLT
            XslTransform xslTransformFile = new XslTransform();
            // NOTE:if the file referenced is in a different location, you may need to
            // change the following line.
            xslTransformFile.Load(ConfigurationManager.AppSettings["HelpPageAbsoluteBasePath"] + "HelpPage\\HelpPageMember.xsl");

            // create the output repository for the transform
            System.Text.StringBuilder sbTransformed =
                new System.Text.StringBuilder();
            System.IO.TextWriter tw =
                (System.IO.TextWriter)new System.IO.StringWriter(sbTransformed);

            // the strType and strMember parameters is passed into the stylesheet
            XsltArgumentList arglist = new XsltArgumentList();
            arglist.AddParam("WhichMember", "", strMember);

            xslTransformFile.Transform(m_xdSourceXML, arglist, tw);

            return tw.ToString();
        }
    }
}