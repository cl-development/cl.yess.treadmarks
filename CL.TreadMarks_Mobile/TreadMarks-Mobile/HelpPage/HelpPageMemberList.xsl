﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" />
  <xsl:param name="WhichType" />
  <xsl:template match="doc">
    <xsl:apply-templates select="./assembly/name" />
    <H2>
      Web Services
    </H2>
    <xsl:apply-templates select="./members/member[starts-with(@name,concat('F:',$WhichType))]" mode="fields"/>
    <xsl:apply-templates select="./members/member[starts-with(@name,concat('P:',$WhichType))]" mode="properties"/>
    <xsl:apply-templates select="./members/member[starts-with(@name,concat('M:',$WhichType))]" mode="methods"/>
    <xsl:apply-templates select="./members/member[starts-with(@name,concat('E:',$WhichType))]" mode="events"/>
    <xsl:apply-templates select="./members/member[starts-with(@name,concat('!:',$WhichType))]" mode="others"/>
  </xsl:template>
  <xsl:template match="name">
    <TABLE width="100%" border="0" cellspacing="0" cellpadding="10" bgcolor="#7aa83e">
      <TR>
        <TD align="center">
          <H1>
            
            <xsl:value-of select="substring-after($WhichType,'.')" />
            <xsl:text>.svc</xsl:text>
            
          </H1>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>
  <xsl:template match="member" mode="fields">
    <xsl:if test="position() = 1">
      <P></P>
      <H3>Fields</H3>
    </xsl:if>
    <DL>
      <DT>
        <A>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=<xsl:value-of select="substring-after(@name,':')" />
          </xsl:attribute>
          
          <B>
            <xsl:value-of select="substring-after(@name,concat($WhichType,'.'))" />
          </B>
        </A>
      </DT>
      <DD>
        <xsl:apply-templates select="remarks | summary | value" />
      </DD>
    </DL>
    <P></P>
  </xsl:template>
  <xsl:template match="member" mode="properties">
    <xsl:if test="position() = 1">
      <P></P>
      <H3>Properties</H3>
    </xsl:if>
    <DL>
      <DT>
        <A>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=<xsl:value-of select="substring-after(@name,':')" />
          </xsl:attribute>
          <B>
            <xsl:value-of select="substring-after(@name,concat($WhichType,'.'))" />
          </B>
        </A>
      </DT>
      <DD>
        <xsl:apply-templates select="remarks | summary | value" />
      </DD>
    </DL>
    <P></P>
  </xsl:template>
  <xsl:template match="member" mode="methods">
    <xsl:if test="position() = 1">
      <P></P>
    </xsl:if>
    <DL>
      <DT>
        <A>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=<xsl:value-of select="substring-after(@name,':')" />
          </xsl:attribute>
          <B>
            <!--<xsl:value-of select="substring-after(@name,concat($WhichType,'.'))" />-->
            <xsl:value-of select="substring-before(substring-after(@name,concat($WhichType,'.')),'(')" />
            <xsl:text>(</xsl:text>
            <xsl:for-each select="param">
              <xsl:value-of select="@name"/>
              <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
              </xsl:if>
            </xsl:for-each>
            <xsl:text>)</xsl:text>
          </B>
        </A>
      </DT>
      <DD>
        <xsl:apply-templates select="remarks | summary" />
      </DD>
    </DL>
    <P></P>
  </xsl:template>
  <xsl:template match="member" mode="events">
    <xsl:if test="position() = 1">
      <P></P>
      <H3>Events</H3>
    </xsl:if>
    <DL>
      <DT>
        <A>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=<xsl:value-of select="substring-after(@name,':')" />
          </xsl:attribute>
          <B>
            <xsl:value-of select="substring-after(@name,concat($WhichType,'.'))" />
          </B>
        </A>
      </DT>
      <DD>
        <xsl:apply-templates select="remarks | summary" />
      </DD>
    </DL>
    <P></P>
  </xsl:template>
  <xsl:template match="member" mode="others">
    <xsl:if test="position() = 1">
      <P></P>
      <H3>Others</H3>
    </xsl:if>
    <DL>
      <DT>
        <A>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=<xsl:value-of select="substring-after(@name,':')" />
          </xsl:attribute>
          <B>
            <xsl:value-of select="substring-after(@name,concat($WhichType,'.'))" />
          </B>
        </A>
      </DT>
      <DD>
        <xsl:apply-templates select="remarks | summary" />
      </DD>
    </DL>
    <P></P>
  </xsl:template>
  <xsl:template match="remarks | summary | value">
    <xsl:apply-templates />
  </xsl:template>
  <xsl:template match="c">
    <CODE>
      <xsl:apply-templates />
    </CODE>
  </xsl:template>
  <xsl:template match="para">
    <P>
      <xsl:apply-templates />
    </P>
  </xsl:template>
  <xsl:template match="paramref">
    <I>
      <xsl:apply-templates />
    </I>
  </xsl:template>
  <xsl:template match="see">
    <A>
      <xsl:variable name="MemberString" select="substring-after(@cref,':') " />
      <xsl:variable name="TypeString" select="concat(concat( substring-before($MemberString,'.'),  '.'), substring-before(substring-after($MemberString,'.'),'.'))" />
      <xsl:choose>
        <xsl:when test="starts-with(@cref,'T:')">
          <xsl:attribute name="href">
            HelpPageAPI.aspx?Type=
            <xsl:value-of select="$TypeString" />
          </xsl:attribute>
          <xsl:value-of select="substring-after(substring-after($MemberString,'.'),'.')" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="href">
            HelpPageMember.aspx?Member=
            <xsl:value-of select="$MemberString" />
          </xsl:attribute>
          <xsl:value-of select="substring-after(substring-after($MemberString,'.'),'.')" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates />
    </A>
  </xsl:template>
  <xsl:template match="list[@type='table']">
    <TABLE border="1">
      <xsl:apply-templates mode="table" />
    </TABLE>
  </xsl:template>
  <xsl:template match="list[@type='bullet']">
    <UL>
      <xsl:apply-templates mode="list" />
    </UL>
  </xsl:template>
  <xsl:template match="list[@type='number']">
    <OL>
      <xsl:apply-templates mode="list" />
    </OL>
  </xsl:template>
  <xsl:template match="listheader" mode="table">
    <THEAD>
      <xsl:apply-templates mode="table" />
    </THEAD>
  </xsl:template>
  <xsl:template match="listheader" mode="list">
    <LI>
      <B>
        <xsl:apply-templates mode="list" />
      </B>
    </LI>
  </xsl:template>
  <xsl:template match="item" mode="table">
    <TR>
      <xsl:choose>
        <xsl:when test="count(child::*) = 0">
          <xsl:choose>
            <xsl:when test="name(parent::*)='listheader'">
              <TH>
                <xsl:apply-templates mode="table" />
              </TH>
            </xsl:when>
            <xsl:otherwise>
              <TD>
                <xsl:apply-templates mode="table" />
              </TD>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="table" />
        </xsl:otherwise>
      </xsl:choose>
    </TR>
  </xsl:template>
  <xsl:template match="item" mode="list">
    <LI>
      <xsl:apply-templates mode="list" />
    </LI>
  </xsl:template>
  <xsl:template match="term | description" mode="table">
    <xsl:choose>
      <xsl:when test="name(parent::*)='listheader'">
        <TH>
          <xsl:apply-templates mode="table" />
        </TH>
      </xsl:when>
      <xsl:when test="name(parent::*)='item'">
        <TD>
          <xsl:apply-templates mode="table" />
        </TD>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="term" mode="list">
    <xsl:apply-templates mode="list" />
    <xsl:text> - </xsl:text>
  </xsl:template>
  <xsl:template match="description" mode="list">
    <xsl:apply-templates mode="description" />
  </xsl:template>
  <xsl:template match="code">
    <pre>
      <xsl:apply-templates />
    </pre>
  </xsl:template>
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
