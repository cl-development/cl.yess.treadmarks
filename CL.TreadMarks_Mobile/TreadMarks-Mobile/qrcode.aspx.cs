﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using TreadMarks_Mobile.IO.Compression;
using TreadMarks_Mobile.Security.Cryptography;

using ThoughtWorks.QRCode.Codec;

using ZXing;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;


namespace TreadMarks_Mobile
{
    public partial class qrcode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                bool print = Convert.ToBoolean(Request.QueryString["print"] ?? "false");
                uint Id = Convert.ToUInt32(Request.QueryString["id"]);
                if (print)
                {
                    //this.imgOutput.ImageUrl = string.Format("qrcode.aspx?id={0}&width={1}", Id, this.Request.QueryString["width"]);
                    Response.Write("Not Yet Implemented");
                }
                else
                {

                    //Registrant reg = RegistrantGW.RetrieveRegistrant(entity.RegistrationNumber);
                    string reg = (Request.QueryString["reg"] ?? string.Empty).ToString();
                    string uid = (Request.QueryString["uid"] ?? string.Empty).ToString();

                    var data = new
                    {
                        r = reg,
                        w = uid
                    };

                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    string qrText = jsonSerializer.Serialize(data);
                    byte[] compressedBytes = TreadMarks_Mobile.IO.Compression.CompressionUtitity.Compress(qrText);
                    byte[] encryptedBytes = EncryptionUtility.Encrypt(compressedBytes);
                    string encryptedString = Encoding.GetEncoding("iso-8859-1").GetString(encryptedBytes);

                    BarcodeWriter writer = new BarcodeWriter();
                    writer.Format = BarcodeFormat.QR_CODE;

                    int width;
                    if (int.TryParse(this.Request.QueryString["width"], out width))
                    {
                        writer.Options.Width = width;
                    }
                    else
                    {
                        writer.Options.Width = 400;
                    }
                    writer.Options.Height = writer.Options.Width;

                    using (Bitmap output = writer.Write(encryptedString))
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            output.Save(stream, ImageFormat.Png);
                            this.Response.ContentType = "image/png";
                            stream.WriteTo(this.Response.OutputStream);
                            this.Response.End();
                        }
                    }

                }            
            }

        }
    }
}