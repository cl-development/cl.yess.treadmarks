﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// DocumentType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(DocumentTypeModel))]
    public class DocumentTypeResponse
    {
        #region DocumentTypeResponse Members
        public void GetDocumentTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<DocumentType> docTypeList = pullStaticDataMgr.GetDocumentTypeList(lastUpdated);
                if (docTypeList != null)
                    DocumentTypeList = (from documentType in docTypeList
                                        select new DocumentTypeModel()
                                        {
                                            DocumentTypeId = documentType.documentTypeId,
                                            NameKey = documentType.nameKey,
                                            SortIndex = documentType.sortIndex,
                                            SyncDate = documentType.syncDate
                                        }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<DocumentTypeModel> responseContent;
        public IEnumerable<DocumentTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<DocumentTypeModel>();
                if (DocumentTypeList.Count > 0)
                    this.responseContent = DocumentTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<DocumentTypeModel> DocumentTypeList { get; set; }
                
        #endregion
    }
}