﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(UserModel))]
    public class UserResponse : IUserResponse
    {
        #region TransactionResponse Members

        //public ITransactionService TransactionService { get; set; }
        
        public void SyncUser(IUserModel userInfo)
        {
            try
            {
                UserMgr UserMgr = new UserMgr();
                this.syncDate = UserMgr.Save(userInfo);
                this.responseMessages.Add("User saved successfully.");
                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("User not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }


        public void GetUserList(DateTime lastUpdated)
        {
            try
            {
                UserMgr userMgr = new UserMgr();
                IEnumerable<User> uList = userMgr.GetList(lastUpdated);
                if (uList != null)
                    UserList = (from user in uList
                                select new UserModel()
                                {
                                    UserId = user.userId,
                                    RegistrationNumber = user.registrationNumber,
                                    AccessTypeId = user.accessTypeId,
                                    CreatedDate = user.createdDate,
                                    Email = user.email,
                                    LastAccessDate = user.lastAccessDate,
                                    Metadata = user.metadata,
                                    ModifiedDate = user.modifiedDate,
                                    Name = user.name,
                                    SyncDate = user.syncDate
                                }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<UserModel> responseContent;
        public IEnumerable<UserModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<UserModel>();
                if (UserList.Count > 0)
                    this.responseContent = UserList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public UserModel SingleEntity { get; set; }

        private DateTime? syncDate;
        [DataMember]
        public DateTime? SyncDate
        {
            get { return this.syncDate; }
            set { this.syncDate = value; }
        }

        private DateTime? appSyncDate;
        [DataMember]
        public DateTime? AppSyncDate
        {
            get { return this.appSyncDate; }
            set { this.appSyncDate = value; }
        }
     
        [DataMember]
        public List<UserModel> UserList { get; set; }
                
        #endregion
    }
}