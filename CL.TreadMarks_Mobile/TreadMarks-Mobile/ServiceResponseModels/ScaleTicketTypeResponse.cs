﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// ScaleTicketType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(ScaleTicketTypeModel))]
    public class ScaleTicketTypeResponse
    {
        #region ScaleTicketTypeResponse Members
        public void GetScaleTicketTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<ScaleTicketType> sttList = pullStaticDataMgr.GetScaleTicketTypeList(lastUpdated);
                if (sttList != null)
                    ScaleTicketTypeList = (from scaleTicketType in sttList
                                           select new ScaleTicketTypeModel()
                                           {
                                               ScaleTicketTypeId = scaleTicketType.scaleTicketTypeId,
                                               NameKey = scaleTicketType.nameKey,
                                               SortIndex = scaleTicketType.sortIndex,
                                               SyncDate = scaleTicketType.syncDate

                                           }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<ScaleTicketTypeModel> responseContent;
        public IEnumerable<ScaleTicketTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<ScaleTicketTypeModel>();
                if (ScaleTicketTypeList.Count > 0)
                    this.responseContent = ScaleTicketTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<ScaleTicketTypeModel> ScaleTicketTypeList { get; set; }
                
        #endregion
    }
}