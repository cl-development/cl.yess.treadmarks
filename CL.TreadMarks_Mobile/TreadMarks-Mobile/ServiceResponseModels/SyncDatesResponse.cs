﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarksContracts;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;
using TreadMarks_Mobile.CommunicationData;
using System.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// Transaction response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SyncDatesModel))]
    public class SyncDatesResponse 
    {
        #region SyncDatesResponse Methods        
        public void GetSyncDatesList()
        {
            try
            {
                PullStaticDataMgr syncDatesMgr = new PullStaticDataMgr();
                this.SyncDatesList = syncDatesMgr.GetSyncDatesList();
                this.responseStatus = MobileWSResponseStatus.OK;
                this.responseMessages.Add("list sent successfully");
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("SyncDates not saved!");
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }

        #endregion

        #region SyncDatesResponse Variables
     
        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public ICollection<SyncDatesModel> SyncDatesList { get; set; }
                
        #endregion
    }
}