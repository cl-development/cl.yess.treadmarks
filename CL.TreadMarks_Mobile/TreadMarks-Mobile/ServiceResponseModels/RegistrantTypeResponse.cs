﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using TreadMarks_Mobile.CommunicationData;
using System.Collections.Generic;
using otscommon.ExtensionMethods;
using otscommon.Enums;
using System.IO;
using System.Web;
using System.Configuration;
using TreadMarksContracts.CommunicationSrv;
using TreadMarks_Mobile.Data;

namespace TreadMarks_Mobile.ResponseMessages
{
    /// <summary>
    /// RegistrantType response message class.
    /// </summary>
    [DataContract]
    [KnownType(typeof(RegistrantTypeModel))]
    public class RegistrantTypeResponse
    {
        #region RegistrantTypeResponse Members
        public void GetRegistrantTypeList(DateTime lastUpdated)
        {
            try
            {
                PullStaticDataMgr pullStaticDataMgr = new PullStaticDataMgr();
                IEnumerable<RegistrantType> rtList = pullStaticDataMgr.GetRegistrantTypeList(lastUpdated);
                if (rtList != null)
                    RegistrantTypeList = (from registrantType in rtList
                                          select new RegistrantTypeModel()
                                          {
                                              RegistrantTypeID = registrantType.registrantTypeId,
                                              CreatedDate = registrantType.createdDate,
                                              DescriptionKey = registrantType.descriptionKey,
                                              FileName = registrantType.fileName,
                                              ModifiedDate = registrantType.modifiedDate,
                                              ShortDescriptionKey = registrantType.shortDescriptionKey,
                                              SyncDate = registrantType.syncDate
                                          }).ToList();

                this.responseStatus = MobileWSResponseStatus.OK;
            }

            catch (ArgumentException aEx)
            {
                this.responseMessages.Add(aEx.Message);
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
            catch (Exception ex)
            {
                //Send all underlying error messages 
                this.ResponseMessages.Add("Error: " + ex.GetAllMessages());
                this.responseStatus = MobileWSResponseStatus.ERROR;
            }
        }
        
        private IEnumerable<RegistrantTypeModel> responseContent;
        public IEnumerable<RegistrantTypeModel> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<RegistrantTypeModel>();
                if (RegistrantTypeList.Count > 0)
                    this.responseContent = RegistrantTypeList;

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private MobileWSResponseStatus responseStatus = MobileWSResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (MobileWSResponseStatus)Enum.Parse(typeof(MobileWSResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }
      
        [DataMember]
        public List<RegistrantTypeModel> RegistrantTypeList { get; set; }
                
        #endregion
    }
}