﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTS_MobileToTM.Interfaces
{
    interface IServiceCredentials : IHaulerServiceCredentials, IRegistrantServiceCredentials
    {

    }
}
