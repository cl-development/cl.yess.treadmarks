﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionModelExtended : ITransactionModelExtended
    {

        private ITransactionModel parent { get; set; }
        public TransactionModelExtended()
            : base()
        {

        }
        public TransactionModelExtended(ITransactionModel transaction)
        {
            parent = transaction;

            foreach (PropertyInfo prop in parent.GetType().GetProperties())
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(parent, null), null);
            this.Phone = string.Empty;
            this.PreparedBy = string.Empty;
            this.OpeningInventoryCredit = 0;
            this.OpeningInventoryWeight = 0;
            this.OtsComments = string.Empty;
            this.UID = string.Empty;
            this.Revised = false;   
        }

        [DataMember(Name = "transactionId")]
        public System.Guid TransactionID { get; set; }
        [DataMember(Name = "friendlyId")]
        public long FriendlyId { get; set; }
        [DataMember(Name = "transactionDate")]
        public System.DateTime TransactionDate { get; set; }
        [DataMember(Name = "transactionTypeId")]
        public long TransactionTypeId { get; set; }
        [DataMember(Name = "outgoingUserId")]
        public long OutgoingUserId { get; set; }
        [DataMember(Name = "outgoingGpsLogId")]
        public Guid OutgoingGpsLogId { get; set; }
        [DataMember(Name = "incomingUserId")]
        public long IncomingUserId { get; set; }
        [DataMember(Name = "incomingGpsLogId")]
        public Guid? IncomingGpsLogId { get; set; }
        [DataMember(Name = "registrationNumber")]
        public decimal RegistrationNumber { get; set; }
        [DataMember(Name = "transactionStatusTypeId")]
        public long TransactionStatusTypeId { get; set; }
        [DataMember(Name = "transactionSyncStatusTypeId")]
        public long TransactionSyncStatusTypeId { get; set; }
        [DataMember(Name = "trailerNumber")]
        public string TrailerNumber { get; set; }
        [DataMember(Name = "trailerLocationId")]
        public Nullable<long> TrailerLocationId { get; set; }
        [DataMember(Name = "outgoingSignaturePhotoId")]
        public Nullable<Guid> OutgoingSignaturePhotoId { get; set; }
        [DataMember(Name = "outgoingSignatureName")]
        public string OutgoingSignatureName { get; set; }
        [DataMember(Name = "incomingSignaturePhotoId")]
        public Nullable<Guid> IncomingSignaturePhotoId { get; set; }
        [DataMember(Name = "incomingSignatureName")]
        public string IncomingSignatureName { get; set; }
        [DataMember(Name = "createdUserId")]
        public long CreatedUserId { get; set; }
        [DataMember(Name = "createdDate")]
        public System.DateTime CreatedDate { get; set; }
        [DataMember(Name = "modifiedUserId")]
        public Nullable<long> ModifiedUserId { get; set; }
        [DataMember(Name = "modifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember(Name = "syncDate")]
        public Nullable<System.DateTime> SyncDate { get; set; }
        public virtual ICollection<IPhotoModel> Photos { get; set; }
        public virtual ICollection<ITransaction_TireTypeModel> Transaction_TireTypes { get; set; }
        public virtual ICollection<ITransaction_EligibilityModel> Transaction_EligibilityList { get; set; }
        public ILocationModel TrailerLocation { get; set; }
        public virtual ICollection<ICommentModel> Comments { get; set; }
        public ITransaction_EligibilityModel Transaction_Eligibility { get; set; }
        public string ResponseComments { get; set; }
        [DataMember(Name = "postalCode1")]
        public string PostalCode1 { get; set; }
        [DataMember(Name = "postalCode2")]
        public string PostalCode2 { get; set; }

        [DataMember(Name = "outgoingRegistrationNumber")]
        public decimal OutgoingRegistrationNumber { get; set; }
        [DataMember(Name = "Phone")]
        public System.String Phone { get; set; }

        [DataMember(Name = "PreparedBy")]
        public System.String PreparedBy { get; set; }

        [DataMember(Name = "OpeningInventoryWeight")]
        public Decimal? OpeningInventoryWeight { get; set; }

        [DataMember(Name = "OpeningInventoryCredit")]
        public Decimal? OpeningInventoryCredit { get; set; }

        [DataMember(Name = "deviceName")]
        public string DeviceName { get; set; }

        [DataMember(Name = "Revised")]
        public bool? Revised { get; set; }

        [DataMember(Name = "OtsComments")]
        public String OtsComments { get; set; }

        [DataMember(Name = "versionBuild")]
        public string VersionBuild { get; set; }
        
        [DataMember(Name = "deviceId")]
        public string DeviceId { get; set; }

        [DataMember(Name = "UID")]
        public String UID { get; set; }

        public ISTCAuthorizationModel STCAuthorization { get; set; }
        public IAuthorizationModel Authorization { get; set; }
        public ILocationModel STCLocation { get; set; }
        public IScaleTicketModel InboundScaleTicket { get; set; }
        public IScaleTicketModel OutboundScaleTicket { get; set; }
        public IScaleTicketModel ScaleTicketBoth { get; set; }
        [DataMember(Name = "companyInfo")]
        public IRegistrantModel CompanyInfo { get; set; }
        public ICompanyModel Company { get; set; }
    }
}
