﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_MobileToTM.ServiceResponseModels;
using TreadMarks_MobileToTM.CommunicationSrv;

namespace TreadMarks_MobileToTM
{
    class HelperFunctions
    {

        public static BooleanResponse CastToBooleanResponse(IResponseMessage<object> response)
        {
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = Convert.ToBoolean(response.SingleEntity);
            return result;
        }

        public static DateTime GetFirstDayOfMonth(DateTime date)
        {
            return  new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime GetLastDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1);
        }

    }
}
