﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_MobileToTM.CommunicationSrv;
using TreadMarksContracts;
//using otscontracts.Common;

namespace TreadMarks_MobileToTM.ServiceResponseModels
{
    public class BooleanResponse : IResponseMessage<Object>
    {

        #region IResponseMessage<bool> Members

        private IEnumerable<Object> responseContent = null;
        [DataMember]
        public IEnumerable<Object> ResponseContent
        {
            get
            {
                if (this.responseContent != null)
                    return this.responseContent;

                this.responseContent = new List<Object>();
                if (SingleEntity != null)
                    ((List<Object>)this.responseContent).Add(SingleEntity);

                return this.responseContent;
            }
            set { this.responseContent = value; }
        }

        private WebServiceResponseStatus responseStatus = WebServiceResponseStatus.NONE;
        [DataMember]
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (WebServiceResponseStatus)Enum.Parse(typeof(WebServiceResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
        [DataMember]
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        [DataMember]
        public object SingleEntity { get; set; }

        #endregion
    }
}
