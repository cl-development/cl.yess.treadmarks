﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarksContracts;
using TreadMarks_MobileToTM.CommunicationSrv;

namespace TreadMarks_MobileToTM
{
    public interface ICommunicator
    {
        IEnumerable<AppUserModel> GetUpdatedAppUsersList(DateTime lastUpdated);
        IEnumerable<VendorModel> GetUpdatedVendorsList(DateTime lastUpdated);    
      
    }
}
