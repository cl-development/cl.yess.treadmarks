﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_MobileToTM.CommunicationSrv;
using WS = TreadMarks_MobileToTM.PullService;
using TreadMarks_MobileToTM.ServiceResponseModels;
using TreadMarksContracts;
using System.Net.Http;
using Newtonsoft.Json;

namespace TreadMarks_MobileToTM.Controllers
{
    public class PullServiceController : IPullServiceController
    {
        #region Properties

        public string BaseAddress { get; set; }

        #endregion

        #region Constructors

        public PullServiceController(string baseAddress)
        {
            BaseAddress = baseAddress + "api/v1/";
        }

        #endregion

        #region Methods      
    

        public IEnumerable<VendorModel> GetUpdatedVendorsList(DateTime lastUpdated, string token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);  
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                var response = client.GetAsync(string.Format("Pull/GetAllUpdatedVendors?dateInfo={0}", lastUpdated.ToString())).Result;
                
                //resp.EnsureSuccessStatusCode();    // Throw if not a success code. 
                //if (response.IsSuccessStatusCode)

                var message = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<VendorResponse>(message);
                return result.Data;
            }
        }

        public IEnumerable<AppUserModel> GetUpdatedAppUsersList(DateTime lastUpdated, string token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                var response = client.GetAsync(string.Format("Pull/GetAllUpdatedAppUsers?dateInfo={0}", lastUpdated.ToString())).Result;
                var message = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<AppUserResponse>(message);
                return result.Data;
            }
        }
            //{
        //    WS.GetAllUpdatedAppUsersResponse response = client.GetAllUpdatedAppUsers(new WS.GetAllUpdatedAppUsersRequest() { dateInfo = lastUpdated, credentials = cred.PullServiceCredentials });
            
        //    IEnumerable<IAppUserModel> appUserList = new List<IAppUserModel>();
        //    if (((WS.AppUserResponse)response.GetAllUpdatedAppUsersResult).UpdatedAppUserList != null)
        //        appUserList = (from appUser in ((WS.AppUserResponse)response.GetAllUpdatedAppUsersResult).UpdatedAppUserList
        //                   select new AppUserModel()
        //                   {                              
        //                       ID = appUser.ID,
        //                       AccessTypeID = appUser.AccessTypeID,
        //                       Active = appUser.Active,
        //                       CreatedOn = appUser.CreatedOn,
        //                       EMail = appUser.EMail,
        //                       LastAccessOn = appUser.LastAccessOn,
        //                       Metadata = appUser.Metadata,
        //                       ModifiedOn = appUser.ModifiedOn,
        //                       Name = appUser.Name,
        //                       Number = appUser.Number,
        //                       SyncOn = appUser.SyncOn,
        //                       VendorAppUserID = appUser.VendorAppUserID,
        //                       VendorID = appUser.VendorID
        //                   }).ToList();

        //    return appUserList;
        //}

        //public IEnumerable<IVendorModel> GetUpdatedVendorsList(DateTime lastUpdated, string token)
        //{
        //    WS.GetAllUpdatedVendorsResponse response = client.GetAllUpdatedVendors(new WS.GetAllUpdatedVendorsRequest() { dateInfo = lastUpdated, credentials = cred.PullServiceCredentials });

        //    IEnumerable<IVendorModel> vendorList = new List<IVendorModel>();
        //    if (((WS.VendorResponse)response.GetAllUpdatedVendorsResult).UpdatedVendorList != null)
        //        vendorList = (from Vendor in ((WS.VendorResponse)response.GetAllUpdatedVendorsResult).UpdatedVendorList
        //                   select new VendorModel()
        //                   {
        //                       ID = Vendor.ID,
        //                       Number = Vendor.Number,
        //                       BusinessNumber = Vendor.BusinessNumber,
        //                       BusinessName = Vendor.BusinessName,
        //                       ActivationDate = Vendor.ActivationDate,
        //                       ActiveStateChangeDate = Vendor.ActiveStateChangeDate,
        //                       IsActive = Vendor.IsActive,
        //                       LastUpdatedDate = Vendor.LastUpdatedDate,
        //                       VendorTypeID = Vendor.VendorTypeID,
        //                       CreatedDate = Vendor.CreatedDate,
        //                       LocationAddress = new AddressModel()
        //                       {
        //                           ID = Vendor.LocationAddress.ID,
        //                           AddressTypeID = Vendor.LocationAddress.AddressTypeID,
        //                           Address1 = Vendor.LocationAddress.Address1,
        //                           Address2 = Vendor.LocationAddress.Address2,
        //                           Address3 = Vendor.LocationAddress.Address3,
        //                           City = Vendor.LocationAddress.City,
        //                           Province = Vendor.LocationAddress.Province,
        //                           PostalCode = Vendor.LocationAddress.PostalCode,
        //                           Country = Vendor.LocationAddress.Country,
        //                           Phone = Vendor.LocationAddress.Phone ?? "",
        //                           Fax = Vendor.LocationAddress.Fax,
        //                           GPSCoordinate1 = Vendor.LocationAddress.GPSCoordinate1,
        //                           GPSCoordinate2 = Vendor.LocationAddress.GPSCoordinate2
        //                       }
                              
        //                   }).ToList();

        //    return vendorList;
        //}

        #endregion
    }
}
