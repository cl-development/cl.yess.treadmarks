﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using WS = OTS_MobileToTM.CollectorServiceReference;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;

namespace OTS_MobileToTM.Controllers
{
    public class CollectorSummaryController : ICollectorSummaryController
    {
        #region Properties

        private WS.CollectorCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }

        #endregion

        #region Constructor

        public CollectorSummaryController()
        { 
            client = new WS.CollectorCommunicationClient();
            cred = new ServiceCredentials();
            cred.CollectorServiceCredentials.UserName = string.Empty;
            cred.CollectorServiceCredentials.Password = string.Empty;
        }

        public CollectorSummaryController(string username, string password)
        {
            client = new WS.CollectorCommunicationClient();
            cred = new ServiceCredentials();
            cred.CollectorServiceCredentials.UserName = username;
            cred.CollectorServiceCredentials.Password = password;
        }

        #endregion

        #region Methods

        public IResponseMessage<object> Insert(ITransactionModelExtended transaction)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {

                WS.CollectorSummaryModel summary = new WS.CollectorSummaryModel();
                summary.ClaimPeriod = 0; //if 0 claim period is taken based on ClaimPeriodStart
                summary.ClaimPeriodStart = HelperFunctions.GetFirstDayOfMonth(transaction.TransactionDate);               
                summary.RegistrationNumber = Convert.ToString(transaction.OutgoingRegistrationNumber);
                summary.CollectorName = string.Empty; //populated from registration number in TM
                summary.UID = transaction.UID;
                summary.UserEmail = transaction.UID;
                summary.Submit = null;
                summary.WebSubmissionDate = null;
                summary.TotalPay = summary.TotalPayPreTax = null;
                summary.CreatedBy = null;
                summary.TotalTax = 0;
                summary.PreAdjustmentTotal = null;
                summary.PenaltyAmount = null;
                WS.CollectorSummaryResponse response = (WS.CollectorSummaryResponse)client.SaveClaim(summary, transaction.PreparedBy, cred.CollectorServiceCredentials);
                result.ResponseContent = response.ResponseContent;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.NewClaimID;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt16(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = -1;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="claimStartDate">the begin date where claim period can fall</param>
        /// <param name="claimEndDate">the end date where claim period can fall</param>
        /// <param name="registrationNumber"></param>
        /// <returns></returns>
        public IResponseMessage<object> GetCollectorClaimsByFilter(DateTime? claimStartDate, DateTime? claimEndDate, string registrationNumber)
        {
            WS.CollectorSummaryResponse response = (WS.CollectorSummaryResponse)client.GetClaimsByFilter(GetSettings(claimStartDate, claimEndDate, registrationNumber), cred.CollectorServiceCredentials);
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = Convert.ToBoolean(response.SingleEntity);
            return result;        
        }

        public IResponseMessage<object> GetCollectorClaimsByFilter(ITransactionModel transaction)
        {
            WS.CollectorSummaryResponse response = (WS.CollectorSummaryResponse)client.GetClaimsByFilter(GetSettings(HelperFunctions.GetFirstDayOfMonth(transaction.TransactionDate), HelperFunctions.GetLastDayOfMonth(transaction.TransactionDate), Convert.ToString(transaction.OutgoingRegistrationNumber)), cred.CollectorServiceCredentials);
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = Convert.ToBoolean(response.SingleEntity);
            return result;
        }

        private WS.GridSettings GetSettings(DateTime? createdDate, DateTime? endDate, string registrationNumber)
        {
            WS.GridSettings settings = new WS.GridSettings();
            settings.PageIndex = 0;
            settings.PageSize = -1;



            System.Collections.Generic.List<WS.Rule> rules = new List<WS.Rule>();
            WS.Rule rule = new WS.Rule();
            if (createdDate != null)
            {
                rule = new WS.Rule();
                rule.field = "claimPeriodStart";
                rule.op = "cn";
                rule.data = createdDate.GetValueOrDefault().ToString("yyyy-MM-01");
                rules.Add(rule);
            }

            if (endDate != null)
            {
                rule = new WS.Rule();
                rule.field = "claimPeriodEnd";
                rule.op = "cn";
                rule.data = endDate.GetValueOrDefault().ToString("yyyy-MM-01"); //createdDate.AddMonths(1).ToString("yyyy-MM-01");
                rules.Add(rule);
            }
/*
            rule = new WS.Rule();
            rule.field = "uid";
            rule.op = "cn";
            rule.data = null;
            rules.Add(rule);
*/
/*            
            rule = new WS.Rule();
            rule.field = "submit";
            rule.op = "cn";
            rule.data = "n";
            rules.Add(rule);
*/
            rule = new WS.Rule();
            rule.field = "RegistrationNumber";
            rule.op = "cn";
            rule.data = registrationNumber;
            rules.Add(rule);

            WS.Filter filter = new WS.Filter();
            filter.rules = rules.ToArray();
            settings.Where = filter;
            return settings;
        }

        public void Close()
        {
            client.Close();
        }



        #endregion

    }
}
