﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.CollectorServiceReference;


namespace OTS_MobileToTM.Controllers
{
    interface ICollectorDetailController : IController
    {

        IResponseMessage<object> Insert(ITransactionModelExtended transaction, int SummaryID);

        void Close();

    }
}
