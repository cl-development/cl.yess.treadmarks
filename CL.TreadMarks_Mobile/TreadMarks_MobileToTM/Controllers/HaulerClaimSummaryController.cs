﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using WS = OTS_MobileToTM.HaulerServiceReference;
using OTS_MobileToTM.ServiceResponseModels;
using otscontracts;

namespace OTS_MobileToTM.Controllers
{
    public class HaulerClaimSummaryController : IHaulerClaimSummaryController
    {
        #region Properties

        private WS.HaulerCommunicationClient client;
        public OTS_MobileToTM.ServiceCredentials cred { get; set; }

        #endregion

        #region Constructor

        public HaulerClaimSummaryController()
        { 
            client = new WS.HaulerCommunicationClient();
            cred = new ServiceCredentials();
            cred.HaulerServiceCredentials.UserName = string.Empty;
            cred.HaulerServiceCredentials.Password = string.Empty;
        }

        public HaulerClaimSummaryController(string username, string password)
        {
            client = new WS.HaulerCommunicationClient();
            cred = new ServiceCredentials();// WS.ServiceCredentials();
            cred.HaulerServiceCredentials.UserName = username;
            cred.HaulerServiceCredentials.Password = password;
        }

        #endregion

        #region Methods

        /// <summary>
        /// gets claims for that transaction month
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="recordStatus"></param>
        /// <returns></returns>
        public IResponseMessage<object> GetHaulerClaimsByFilter(ITransactionModel transaction, string recordStatus = "I")
        {
            WS.HaulerClaimSummaryResponse response = (WS.HaulerClaimSummaryResponse)client.GetClaimsByFilterExtended(GetSettings(transaction.TransactionDate, transaction.TransactionDate.AddMonths(1), Convert.ToString(transaction.RegistrationNumber), recordStatus), cred.HaulerServiceCredentials);            
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = Convert.ToBoolean(response.SingleEntity);
            return result;
        }

        public IResponseMessage<object> GetHaulerClaimsByFilter(DateTime? claimStartDate, DateTime? claimEndDate,  string registrationNumber, string recordStatus = "I")
        {
            WS.HaulerClaimSummaryResponse response = (WS.HaulerClaimSummaryResponse)client.GetClaimsByFilterExtended(GetSettings(claimStartDate, claimEndDate, registrationNumber, recordStatus), cred.HaulerServiceCredentials);
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = Convert.ToBoolean(response.SingleEntity);
            return result;
        }

        public IResponseMessage<object> GetHaulerClaimMostRecent(string registrationNumber, DateTime? claimStartDate, DateTime? claimEndDate)
        {
            WS.HaulerClaimSummaryResponse response = (WS.HaulerClaimSummaryResponse)client.GetClaimMostRecent(registrationNumber, claimStartDate, claimEndDate, cred.HaulerServiceCredentials);
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = response.SingleEntity;
            return result;        
        }

        public IResponseMessage<object> GetHaulerClaimByID(int haulerSummaryId)
        {
            WS.HaulerClaimSummaryResponse response = (WS.HaulerClaimSummaryResponse)client.GetClaimSummary(haulerSummaryId, cred.HaulerServiceCredentials);
            BooleanResponse result = new BooleanResponse();
            result.ResponseContent = response.ResponseContent;
            result.ResponseMessages = response.ResponseMessages.ToList();
            result.ResponseStatus = response.ResponseStatus;
            result.SingleEntity = response.SingleEntity;
            return result;
        }

        public IResponseMessage<object> Insert(ITransactionModelExtended model)
        {
            BooleanResponse result = new BooleanResponse();
            try
            {
                WS.HaulerClaimSummaryModel summary = new WS.HaulerClaimSummaryModel();
                summary.ClaimPeriodStart = HelperFunctions.GetFirstDayOfMonth(model.TransactionDate);
                summary.ClaimPeriodEnd = HelperFunctions.GetLastDayOfMonth(model.TransactionDate);
                summary.HaulerClaimSummaryID = 0;
                summary.InBatch = 0;
                summary.IsTaxExempted = false; //determined in onlinecommunicator 
                summary.IsUnderTaxApplicableDate = false; //determined in onlinecommunicator 
                summary.OpeningInventoryCredit = model.OpeningInventoryCredit; 
                summary.OpeningInventoryWeight = model.OpeningInventoryWeight; 
                summary.Phone = model.Phone;
                summary.PreparedBy = model.PreparedBy;
                summary.RecordState = null;
                summary.RegistrationNumber = Convert.ToDecimal(model.RegistrationNumber);
                summary.SubmissionNumber = 0;
                WS.HaulerClaimSummaryResponse response = (WS.HaulerClaimSummaryResponse)client.SaveClaim(summary, model.PreparedBy, cred.HaulerServiceCredentials);
                result.ResponseContent = response.ResponseContent;
                result.ResponseMessages = response.ResponseMessages.ToList();
                result.ResponseStatus = response.ResponseStatus;
                result.SingleEntity = response.NewClaimID;
            }
            catch (Exception e)
            {
                result.ResponseContent = null;
                result.ResponseStatus = Convert.ToInt16(WebServiceResponseStatus.ERROR);
                result.ResponseMessages = new List<string>() { string.Format("Exception: {0}", e.Message) };
                result.SingleEntity = -1;
            }
            return result;
        }

        public void Close()
        {
            client.Close();        
        }

        private WS.GridSettings GetSettings(DateTime? createdDate, DateTime? endDate, string registrationNumber, string recordStatus)
        {
            WS.GridSettings settings = new WS.GridSettings();
            settings.PageIndex = 0;
            settings.PageSize = -1;



            System.Collections.Generic.List<WS.Rule> rules = new List<WS.Rule>();
            WS.Rule rule = new WS.Rule();
            if (createdDate != null)
            {
                rule = new WS.Rule();
                rule.field = "claimPeriodStart";
                rule.op = "cn";
                rule.data = createdDate.GetValueOrDefault().ToString("yyyy-MM-01");
                rules.Add(rule);            
            }

            if (endDate != null)
            {
                rule = new WS.Rule();
                rule.field = "claimPeriodEnd";
                rule.op = "cn";
                rule.data = endDate.GetValueOrDefault().ToString("yyyy-MM-01"); //createdDate.AddMonths(1).ToString("yyyy-MM-01");
                rules.Add(rule);            
            }

            rule = new WS.Rule();
            rule.field = "RegistrationNumber";
            rule.op = "cn";
            rule.data = registrationNumber;
            rules.Add(rule);

            rule = new WS.Rule();
            rule.field = "recordState";
            rule.op = "cn";
            rule.data = recordStatus;
            rules.Add(rule);

            WS.Filter filter = new WS.Filter();
            filter.rules = rules.ToArray();
            settings.Where = filter;
            return settings;
        }

        #endregion

    }
}
