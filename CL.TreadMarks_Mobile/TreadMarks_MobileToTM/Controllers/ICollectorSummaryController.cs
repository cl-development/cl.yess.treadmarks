﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.CollectorServiceReference;


namespace OTS_MobileToTM.Controllers
{
    interface ICollectorSummaryController : IController
    {

        IResponseMessage<object> Insert(ITransactionModelExtended transaction);

        IResponseMessage<object> GetCollectorClaimsByFilter(DateTime? claimStartDate, DateTime? claimEndDate, string registrationNumber);

        IResponseMessage<object> GetCollectorClaimsByFilter(ITransactionModel transaction);
        void Close();

    }
}
