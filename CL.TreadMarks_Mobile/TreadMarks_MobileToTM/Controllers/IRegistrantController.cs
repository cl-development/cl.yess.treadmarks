﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.RegistrantServiceReference;

namespace OTS_MobileToTM.Controllers
{
    interface IRegistrantController : IController
    {
        IResponseMessage<object> GetRegistrantByRegNumber(ITransactionModel transaction);
        IEnumerable<IRegistrantModelExtended> GetUpdatedRegistrantList(DateTime lastUpdated);
    }
}
