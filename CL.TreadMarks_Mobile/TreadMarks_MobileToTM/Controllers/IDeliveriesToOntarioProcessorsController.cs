﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileToTM.CommunicationSrv;
using otscontracts;
using WS = OTS_MobileToTM.HaulerServiceReference;

namespace OTS_MobileToTM.Controllers
{
    interface IDeliveriesToOntarioProcessorsController : IController
    {
        IResponseMessage<object> Insert(ITransactionModelExtended transaction, int haulerSummaryId);
        IResponseMessage<object> Delete(int tiresReceivedSpecialTireCollectionEventsId);
        void Close();


    }
}
