﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace PullDataFromTM
{
	abstract class BaseProgram
	{
		/// <summary>
		/// Use System.Diagnostics.Process to see if this process is already running on this machine.
		/// </summary>
		bool IsAlreadyRunning
		{
			get
			{
				Process currentProcess = Process.GetCurrentProcess();

				return Process.GetProcessesByName( currentProcess.ProcessName ).
					Count( i => i.Id != currentProcess.Id ) > 0;
			}
		}

		protected BaseProgram ( string[] args )
		{
			this.Initialize();
		}

		/// <summary>
		/// Start the process
		/// </summary>
		protected void Initialize ()
		{
			if ( this.IsAlreadyRunning )
			{
				/// End -- only one processs can run at any given time
				Environment.Exit( 0 );
			}
		}
		protected static void LogError ( Exception ex, bool writeToError )
		{
			LogError( string.Concat( ex.Message, "\r\n", ex.StackTrace ), writeToError );
		}
		static void LogError ( string message, bool writeToError )
		{
			string errorLogFolder = ConfigurationManager.AppSettings["ErrorLogFolder"];

			if ( errorLogFolder == null )
			{
				throw new Exception( "'ErrorLogFolder' is not defined in the configuration file." );
			}

			if ( errorLogFolder.EndsWith( "\\" ) == false )
			{
				errorLogFolder += "\\";
			}

			DateTime timestamp = DateTime.Now;

			string path = string.Format( "{0}{1:yyyy-MM-dd}.log",
				errorLogFolder,
				timestamp );

			if ( Directory.Exists( errorLogFolder ) == false )
			{
				Directory.CreateDirectory( errorLogFolder );
			}

			using ( StreamWriter writer = new StreamWriter( path, true ) )
			{
				writer.WriteLine( string.Format( "[{0:yyyy-MM-dd hh:mm:ss tt}] {1}",
					timestamp,
					message ) );
			}

			Console.Error.WriteLine( message );
		}
		protected static int Main ( BaseProgram program )
		{
			try
			{
				program.Run();
			}
			catch ( Exception ex )
			{
				LogError( ex, true );

				return -1;
			}

			return 0;
		}
		protected abstract void Run ();
	}
}
