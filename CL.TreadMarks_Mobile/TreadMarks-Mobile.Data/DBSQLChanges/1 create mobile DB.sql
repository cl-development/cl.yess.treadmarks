CREATE DATABASE "TreadMarks-Mobile";
GO

USE "TreadMarks-Mobile";
GO

CREATE TABLE [dbo].[Authorization](
	[authorizationId] [uniqueidentifier] NOT NULL,
	[authorizationNumber] [varchar](50) NULL,
	[expiryDate] [datetime] NULL,
	[syncDate] [datetime] NULL,
	[userId] [bigint] NOT NULL,
	[transactionId] [uniqueidentifier] NULL,
	[transactionTypeId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[authorizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[commentId] [uniqueidentifier] NOT NULL,
	[createdDate] [datetime] NOT NULL,
	[syncDate] [datetime] NULL,
	[text] [ntext] NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
	[userId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[commentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Document]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[documentId] [bigint] NOT NULL,
	[date] [datetime] NOT NULL,
	[documentName] [varchar](64) NOT NULL,
	[documentNumber] [varchar](64) NOT NULL,
	[documentTypeOther] [varchar](64) NULL,
	[sortIndex] [bigint] NOT NULL,
	[documentTypeId] [bigint] NOT NULL,
	[photoId] [uniqueidentifier] NOT NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[documentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentType](
	[documentTypeId] [bigint] NOT NULL,
	[nameKey] [nvarchar](64) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[documentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Eligibility]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Eligibility](
	[eligibilityId] [bigint] NOT NULL,
	[nameKey] [varchar](255) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
	[transactionTypeId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[eligibilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GpsLog]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GpsLog](
	[gpsLogId] [uniqueidentifier] NOT NULL,
	[latitude] [decimal](9, 6) NOT NULL,
	[longitude] [decimal](9, 6) NOT NULL,
	[syncDate] [datetime] NULL,
	[timeStamp] [datetime] NULL,
 CONSTRAINT [PK__GpsLog__D09D27C0015E79D3] PRIMARY KEY CLUSTERED 
(
	[gpsLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Location]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[locationId] [uniqueidentifier] NOT NULL,
	[address1] [varchar](100) NULL,
	[address2] [varchar](100) NULL,
	[address3] [varchar](100) NULL,
	[city] [varchar](64) NULL,
	[country] [varchar](64) NULL,
	[fax] [varchar](48) NULL,
	[latitude] [decimal](9, 6) NULL,
	[longitude] [decimal](9, 6) NULL,
	[name] [varchar](64) NULL,
	[phone] [varchar](48) NOT NULL,
	[postalCode] [varchar](10) NULL,
	[province] [varchar](64) NULL,
	[syncDate] [datetime] NULL,
	[isUserGenerated] [bit] NULL,
 CONSTRAINT [PK__Location__30646B6E0C57AFB5] PRIMARY KEY CLUSTERED 
(
	[locationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[messageId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[syncDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[messageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Module]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Module](
	[moduleId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[moduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Photo]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Photo](
	[photoId] [uniqueidentifier] NOT NULL,
	[comments] [varchar](512) NULL,
	[createdDate] [datetime] NOT NULL,
	[date] [datetime] NULL,
	[fileName] [text] NOT NULL,
	[latitude] [decimal](9, 6) NULL,
	[longitude] [decimal](9, 6) NULL,
	[syncDate] [datetime] NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
	[photoTypeId] [int] NOT NULL,
	[uniqueFileName] [text] NULL,
 CONSTRAINT [PK__Photo__547C322DEE5DA462] PRIMARY KEY CLUSTERED 
(
	[photoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhotoPreSelectComment]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhotoPreSelectComment](
	[photoPreSelectCommentId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[photoPreSelectCommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhotoType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoType](
	[photoTypeId] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[photoTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Registrant]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Registrant](
	[registrationNumber] [decimal](18, 0) NOT NULL,
	[businessName] [varchar](65) NOT NULL,
	[createdDate] [datetime] NULL,
	[metadata] [varchar](256) NULL,
	[modifiedDate] [datetime] NULL,
	[syncDate] [datetime] NULL,
	[locationId] [uniqueidentifier] NULL,
	[registrantTypeId] [bigint] NOT NULL,
	[activationDate] [datetime] NULL,
	[activeStateChangeDate] [datetime] NULL,
	[lastUpdatedDate] [datetime] NULL,
	[isActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[registrationNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistrantType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrantType](
	[registrantTypeId] [bigint] NOT NULL,
	[createdDate] [datetime] NULL,
	[descriptionKey] [varchar](256) NOT NULL,
	[fileName] [varchar](50) NOT NULL,
	[modifiedDate] [datetime] NULL,
	[shortDescriptionKey] [varchar](50) NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[registrantTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScaleTicket]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScaleTicket](
	[scaleTicketId] [uniqueidentifier] NOT NULL,
	[date] [datetime] NOT NULL,
	[inboundWeight] [decimal](13, 4) NULL,
	[outboundWeight] [decimal](13, 4) NULL,
	[ticketNumber] [varchar](100) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
	[photoId] [uniqueidentifier] NOT NULL,
	[scaleTicketTypeId] [bigint] NOT NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
	[unitTypeId] [bigint] NOT NULL,
 CONSTRAINT [PK__ScaleTic__C46D2E17634C102F] PRIMARY KEY CLUSTERED 
(
	[scaleTicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScaleTicketType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScaleTicketType](
	[scaleTicketTypeId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[scaleTicketTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STCAuthorization]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STCAuthorization](
	[authorizationId] [uniqueidentifier] NOT NULL,
	[locationId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[authorizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TireType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TireType](
	[tireTypeId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[estimatedWeight] [decimal](8, 2) NOT NULL,
	[effectiveStartDate] [datetime] NOT NULL,
	[effectiveEndDate] [datetime] NOT NULL,
	[syncDate] [datetime] NULL,
	[shortNameKey] [varchar](64) NULL,
 CONSTRAINT [PK__TireType__3B087C7BF0417E70] PRIMARY KEY CLUSTERED 
(
	[tireTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction](
	[transactionId] [uniqueidentifier] NOT NULL,
	[friendlyId] [bigint] NOT NULL,
	[transactionDate] [datetime] NOT NULL,
	[transactionTypeId] [bigint] NOT NULL,
	[outgoingUserId] [bigint] NOT NULL,
	[outgoingGpsLogId] [uniqueidentifier] NOT NULL,
	[incomingUserId] [bigint] NOT NULL,
	[incomingGpsLogId] [uniqueidentifier] NULL,
	[incomingRegistrationNumber] [decimal](18, 0) NOT NULL,
	[transactionStatusTypeId] [bigint] NOT NULL,
	[transactionSyncStatusTypeId] [bigint] NOT NULL,
	[trailerNumber] [varchar](64) NULL,
	[trailerLocationId] [bigint] NULL,
	[outgoingSignatureName] [varchar](256) NULL,
	[incomingSignatureName] [varchar](256) NULL,
	[createdUserId] [bigint] NOT NULL,
	[createdDate] [datetime] NOT NULL,
	[modifiedUserId] [bigint] NULL,
	[modifiedDate] [datetime] NULL,
	[syncDate] [datetime] NULL,
	[incomingSignaturePhotoId] [uniqueidentifier] NULL,
	[outgoingSignaturePhotoId] [uniqueidentifier] NULL,
	[responseComments] [ntext] NULL,
	[postalCode1] [char](3) NULL,
	[postalCode2] [char](3) NULL,
	[deviceName] [varchar](255) NULL,
	[outgoingRegistrationNumber] [decimal](18, 0) NOT NULL,
	[versionBuild] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction_Eligibility]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction_Eligibility](
	[transactionEligibilityId] [uniqueidentifier] NOT NULL,
	[syncDate] [datetime] NULL,
	[value] [bit] NOT NULL,
	[eligibilityId] [bigint] NOT NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionEligibilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction_TireType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction_TireType](
	[transactionTireTypeId] [uniqueidentifier] NOT NULL,
	[syncDate] [datetime] NULL,
	[quantity] [bigint] NOT NULL,
	[tireTypeId] [bigint] NOT NULL,
	[transactionId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionTireTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionStatusType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionStatusType](
	[transactionStatusTypeId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[fileName] [varchar](256) NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionStatusTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionSyncStatusType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionSyncStatusType](
	[transactionSyncStatusTypeId] [bigint] NOT NULL,
	[nameKey] [varchar](255) NOT NULL,
	[fileName] [varchar](255) NOT NULL,
	[syncDate] [datetime] NULL,
	[internalDescription] [varchar](255) NULL,
	[userMessage] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionSyncStatusTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionType](
	[transactionTypeId] [bigint] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[shortNameKey] [varchar](64) NOT NULL,
	[fileName] [varchar](255) NOT NULL,
	[sortIndex] [int] NOT NULL,
	[outgoingSignatureKey] [varchar](255) NOT NULL,
	[incomingSignatureKey] [varchar](255) NOT NULL,
	[syncDate] [datetime] NULL,
	[tireCountMessageKey] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionType_Module]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType_Module](
	[transactionTypeModuleId] [bigint] NOT NULL,
	[step] [bigint] NOT NULL,
	[sortIndex] [bigint] NOT NULL,
	[moduleId] [bigint] NOT NULL,
	[transactionTypeId] [bigint] NOT NULL,
	[isRequired] [bit] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionTypeModuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionType_RegistrantType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType_RegistrantType](
	[transactionTypeRegistrantTypeId] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
	[registrantTypeId] [bigint] NOT NULL,
	[transactionTypeId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionTypeRegistrantTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionType_TireType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType_TireType](
	[transactionTypeTireTypeId] [bigint] NOT NULL,
	[TireTypeId] [bigint] NOT NULL,
	[transactionTypeId] [bigint] NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[transactionTypeTireTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UnitType]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UnitType](
	[unitTypeId] [bigint] NOT NULL,
	[kgMultiplier] [float] NOT NULL,
	[nameKey] [varchar](64) NOT NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[unitTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[userId] [bigint] NOT NULL,
	[name] [varchar](64) NOT NULL,
	[email] [varchar](256) NOT NULL,
	[accessTypeId] [bigint] NOT NULL,
	[registrationNumber] [decimal](18, 0) NOT NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[metadata] [varchar](256) NULL,
	[lastAccessDate] [datetime] NULL,
	[syncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[registrationNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].['user-2014-06-30']    Script Date: 11/24/2015 4:02:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].['user-2014-06-30'](
	[userId] [float] NULL,
	[name] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[accessTypeId] [float] NULL,
	[registrationNumber] [float] NULL,
	[createdDate] [nvarchar](255) NULL,
	[modifiedDate] [nvarchar](255) NULL,
	[metadata] [nvarchar](255) NULL,
	[lastAccessDate] [nvarchar](255) NULL,
	[syncDate] [datetime] NULL
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [TreadMarks-Mobile] SET  READ_WRITE 
GO
