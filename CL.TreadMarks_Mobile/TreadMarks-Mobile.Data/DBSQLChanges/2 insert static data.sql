USE [TreadMarks-Mobile]
GO
INSERT [dbo].[DocumentType] ([documentTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (1, N'Invoice', 0, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[DocumentType] ([documentTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (2, N'Bill_of_Lading', 10, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[DocumentType] ([documentTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (3, N'Other', 20, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[Eligibility] ([eligibilityId], [nameKey], [sortIndex], [syncDate], [transactionTypeId]) VALUES (1, N'These_tires_are_eligible_for_payment_of_the_Collector_Allowance_By_OTS', 10, CAST(0x0000A33200000000 AS DateTime), 1)
GO
INSERT [dbo].[Eligibility] ([eligibilityId], [nameKey], [sortIndex], [syncDate], [transactionTypeId]) VALUES (2, N'These_tires_are_Generated_and_therefore_not_eligible_for_Payment', 20, CAST(0x0000A33200000000 AS DateTime), 1)
GO
INSERT [dbo].[Eligibility] ([eligibilityId], [nameKey], [sortIndex], [syncDate], [transactionTypeId]) VALUES (3, N'Charged_a_disposal_fee_From', 30, CAST(0x0000A33200000000 AS DateTime), 6)
GO
INSERT [dbo].[Eligibility] ([eligibilityId], [nameKey], [sortIndex], [syncDate], [transactionTypeId]) VALUES (4, N'Generated prior to Sept. 1st, 2009', 40, CAST(0x0000A33200000000 AS DateTime), 6)
GO
INSERT [dbo].[Eligibility] ([eligibilityId], [nameKey], [sortIndex], [syncDate], [transactionTypeId]) VALUES (5, N'Picked up from a non-registered collector', 50, CAST(0x0000A33200000000 AS DateTime), 6)
GO
INSERT [dbo].[PhotoPreSelectComment] ([photoPreSelectCommentId], [nameKey], [sortIndex], [syncDate]) VALUES (1, N'Dirty Tires Noted', 10, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoPreSelectComment] ([photoPreSelectCommentId], [nameKey], [sortIndex], [syncDate]) VALUES (2, N'Snowy Tires Noted', 20, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoPreSelectComment] ([photoPreSelectCommentId], [nameKey], [sortIndex], [syncDate]) VALUES (3, N'Rims Indicated', 30, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoPreSelectComment] ([photoPreSelectCommentId], [nameKey], [sortIndex], [syncDate]) VALUES (4, N'Fuel Surplus Noted ', 40, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoType] ([photoTypeId], [name], [syncDate]) VALUES (1, N'Photo', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoType] ([photoTypeId], [name], [syncDate]) VALUES (2, N'Signature', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoType] ([photoTypeId], [name], [syncDate]) VALUES (3, N'Scale Ticket', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[PhotoType] ([photoTypeId], [name], [syncDate]) VALUES (4, N'Document', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[RegistrantType] ([registrantTypeId], [createdDate], [descriptionKey], [fileName], [modifiedDate], [shortDescriptionKey], [syncDate]) VALUES (1, CAST(0x0000A33200000000 AS DateTime), N'Collector', N'icon-collector.png', CAST(0x0000A33200000000 AS DateTime), N'Collector', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[RegistrantType] ([registrantTypeId], [createdDate], [descriptionKey], [fileName], [modifiedDate], [shortDescriptionKey], [syncDate]) VALUES (2, CAST(0x0000A33200000000 AS DateTime), N'Hauler', N'icon-hauler.png', CAST(0x0000A33200000000 AS DateTime), N'Hauler', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[RegistrantType] ([registrantTypeId], [createdDate], [descriptionKey], [fileName], [modifiedDate], [shortDescriptionKey], [syncDate]) VALUES (3, CAST(0x0000A33200000000 AS DateTime), N'Steward', N'icon-steward.png', CAST(0x0000A33200000000 AS DateTime), N'Steward', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[RegistrantType] ([registrantTypeId], [createdDate], [descriptionKey], [fileName], [modifiedDate], [shortDescriptionKey], [syncDate]) VALUES (4, CAST(0x0000A33200000000 AS DateTime), N'Recycled Product Manufacturer', N'icon-rpm.png', CAST(0x0000A33200000000 AS DateTime), N'RPM', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[RegistrantType] ([registrantTypeId], [createdDate], [descriptionKey], [fileName], [modifiedDate], [shortDescriptionKey], [syncDate]) VALUES (5, CAST(0x0000A33200000000 AS DateTime), N'Processor', N'icon-processor.png', CAST(0x0000A33200000000 AS DateTime), N'Processor', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[ScaleTicketType] ([scaleTicketTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (1, N'Inbound', 0, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[ScaleTicketType] ([scaleTicketTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (2, N'Outbound', 10, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[ScaleTicketType] ([scaleTicketTypeId], [nameKey], [sortIndex], [syncDate]) VALUES (3, N'Both', 20, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (1, N'Passenger and Light Truck Tires ', 0, CAST(10.20 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'PLT')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (2, N'Medium Truck Tires', 10, CAST(50.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'MT')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (3, N'Agricultural Drive  & Logger Skidder', 20, CAST(60.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'AGLS')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (4, N'Small and Large Industrial Tires', 30, CAST(40.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'IND')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (5, N'Small OTR Tires', 40, CAST(120.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'SOTR')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (6, N'Medium OTR Tires', 50, CAST(580.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'MOTR')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (7, N'Large OTR Tires', 60, CAST(740.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), N'LOTR')
GO
INSERT [dbo].[TireType] ([tireTypeId], [nameKey], [sortIndex], [estimatedWeight], [effectiveStartDate], [effectiveEndDate], [syncDate], [shortNameKey]) VALUES (8, N'Giant OTR Tires', 70, CAST(1560.00 AS Decimal(8, 2)), CAST(0x00009B8400000000 AS DateTime), CAST(0x0000A34E00000000 AS DateTime), CAST(0x0000A34500000000 AS DateTime), N'GOTR')
GO
INSERT [dbo].[TransactionStatusType] ([transactionStatusTypeId], [nameKey], [fileName], [syncDate]) VALUES (1, N'Error in Transaction', N'cal-pop-flag-red.png', CAST(0x0000A38600B12790 AS DateTime))
GO
INSERT [dbo].[TransactionStatusType] ([transactionStatusTypeId], [nameKey], [fileName], [syncDate]) VALUES (2, N'Incomplete Transaction', N'cal-pop-flag-yellow.png', CAST(0x0000A38600B12790 AS DateTime))
GO
INSERT [dbo].[TransactionStatusType] ([transactionStatusTypeId], [nameKey], [fileName], [syncDate]) VALUES (3, N'Complete Transaction', N'cal-pop-flag-green.png', CAST(0x0000A38600B28720 AS DateTime))
GO
INSERT [dbo].[TransactionStatusType] ([transactionStatusTypeId], [nameKey], [fileName], [syncDate]) VALUES (4, N'Deleted Transaction', N'cal-pop-flag-black.png', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (1, N'Device_Not_Attempted_Repository', N'list-dot-yellow.png', CAST(0x0000A33200000000 AS DateTime), N'Mobile transaction not synced to repository as yet', N'Not Synced')
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (2, N'Device_Sync_Repository', N'list-dot-green.png', CAST(0x0000A33200000000 AS DateTime), N'Mobile transaction synced to repository', N'Synced')
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (3, N'Device_Sync_Fail_Repository', N'list-dot-red.png', CAST(0x0000A33200000000 AS DateTime), N'Mobile transaction failed to sync to repository', N'Error during Sync')
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (4, N'Repository_Sent_To_TM', N'list-dot-yellow.png', CAST(0x0000A33200000000 AS DateTime), N'A completed mobile transaction saved successfully to repository and sent to TM for submission (waiting for response from TM which is sent to device)', N'Not Synced')
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (5, N'Repository_Sync_TM', N'list-dot-green.png', CAST(0x0000A33200000000 AS DateTime), N'A completed mobile transaction successfully submitted to TM', N'Synced')
GO
INSERT [dbo].[TransactionSyncStatusType] ([transactionSyncStatusTypeId], [nameKey], [fileName], [syncDate], [internalDescription], [userMessage]) VALUES (6, N'Repository_Sync_Fail_TM', N'list-dot-red.png', CAST(0x0000A33200000000 AS DateTime), N'A completed mobile transaction failed to sync to TM (see responseComments field for error)', N'Error during Sync')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (1, N'Tire_Collection_Receipt', N'TCR', N'tcr.png', 10, N'I_certify_that_these_used_tires_were_accumulated_in_Ontario_after_Aug_31_2009', N'I_certify_picking_up_the_quantities_of_tires_noted_above', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_collecting_for_each_category')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (2, N'Dedicated_Off-the-Road_Tire_Collection', N'DOT', N'dot.png', 20, N'I_certify_that_these_used_tires_were_accumulated_in_Ontario_after_Aug_31_2009', N'I_certify_picking_up_the_quantities_of_tires_and_the_total_weight_noted_above', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_collecting_for_each_category.')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (3, N'Hauler_Inventory_Transfer', N'HIT', N'hit.png', 30, N'I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector_or_through_an_eligible_OTS_Special_Collection_Event_and_were_collected_after_August_31_2009', N'I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector,_an_unregistered_collection_site_or_an_eligible_OTS_Special_Collection_Event_after_August_31,_2009.', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_transferring_for_each_category')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (4, N'Reuse_Tire_Reporting', N'RTR', N'rtr.png', 40, N'TBD', N'TBD', CAST(0x0000A33200000000 AS DateTime), N'TBD')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (5, N'Special_Tire_Collection', N'STC', N'stc.png', 50, N'I_certify_that_these_used_tires_were_accumulated_in_Ontario', N'I_certify_picking_up_the_quantities_of_tires_noted_above', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_collecting_for_each_category.')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (6, N'Unregistered_Collection_Receipt', N'UCR', N'ucr.png', 60, N'TBD', N'I_certify_picking_up_the_quantities_of_tires_noted_above.', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_delivering_for_each_category.')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (7, N'Processor_Tire_Receipt', N'PTR', N'ptt.png', 70, N'I_acknowledge_receipt_of_the_tires_above_for_processing_/_recycling_on_the_delivery_date_indicated.', N'I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector,_an_unregistered_collection_site_or_an_eligible_OTS_Special_Collection_Event_after_August_31,_2009.', CAST(0x0000A33200000000 AS DateTime), N'Enter_the_number_of_tires_you_are_delivering_for_each_category.')
GO
INSERT [dbo].[TransactionType] ([transactionTypeId], [nameKey], [shortNameKey], [fileName], [sortIndex], [outgoingSignatureKey], [incomingSignatureKey], [syncDate], [tireCountMessageKey]) VALUES (8, N'Processor_Inventory_Transfer', N'PIT', N'pit.png', 80, N'TBD', N'TBD', CAST(0x0000A33200000000 AS DateTime), N'TBD')
GO
INSERT [dbo].[TransactionType_RegistrantType] ([transactionTypeRegistrantTypeId], [syncDate], [registrantTypeId], [transactionTypeId]) VALUES (1, CAST(0x0000A39501098458 AS DateTime), 2, 1)
GO
INSERT [dbo].[TransactionType_RegistrantType] ([transactionTypeRegistrantTypeId], [syncDate], [registrantTypeId], [transactionTypeId]) VALUES (2, CAST(0x0000A39501098458 AS DateTime), 2, 5)
GO
INSERT [dbo].[TransactionType_RegistrantType] ([transactionTypeRegistrantTypeId], [syncDate], [registrantTypeId], [transactionTypeId]) VALUES (3, CAST(0x0000A39501098458 AS DateTime), 2, 7)
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (1, 1, 1, CAST(0x0000A35300000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (2, 2, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (3, 3, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (4, 4, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (5, 5, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (6, 6, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (7, 7, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (8, 8, 1, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (14, 6, 2, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (15, 7, 2, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (16, 8, 2, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (17, 1, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (18, 2, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (19, 3, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (20, 4, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (21, 5, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (22, 6, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (23, 7, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (24, 8, 3, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (25, 1, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (26, 2, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (27, 3, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (28, 4, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (29, 5, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (30, 6, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (31, 7, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (32, 8, 5, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (33, 1, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (34, 2, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (35, 3, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (36, 4, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (37, 5, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (38, 6, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (39, 7, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[TransactionType_TireType] ([transactionTypeTireTypeId], [TireTypeId], [transactionTypeId], [syncDate]) VALUES (40, 8, 7, CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[UnitType] ([unitTypeId], [kgMultiplier], [nameKey], [syncDate]) VALUES (1, 0.453592002391815, N'lbs', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[UnitType] ([unitTypeId], [kgMultiplier], [nameKey], [syncDate]) VALUES (2, 1, N'kgs', CAST(0x0000A33200000000 AS DateTime))
GO
INSERT [dbo].[UnitType] ([unitTypeId], [kgMultiplier], [nameKey], [syncDate]) VALUES (3, 1000, N'tonnes', CAST(0x0000A33200000000 AS DateTime))
GO
