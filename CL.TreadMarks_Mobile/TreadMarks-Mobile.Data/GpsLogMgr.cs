﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class GpsLogMgr
    {
        private TM_Entities dbContext;
        public GpsLogMgr()
        {
            dbContext = new TM_Entities();
        }
        public GpsLog GetById(Guid id)
        {
            var query = this.dbContext.GpsLogs.Where(t => t.gpsLogId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public DateTime? Save(IGpsLogModel[] gpsLogInfoList)
        {
            try
            {
                DateTime syncDate = DateTime.Now;
                foreach (IGpsLogModel gpsLogInfo in gpsLogInfoList)
                {
                    GpsLog entity = GetById(gpsLogInfo.GpsLogId);
                    if (entity == null)
                    {
                        entity = new GpsLog();
                        this.dbContext.GpsLogs.Add((GpsLog)entity);
                    }
                    entity.gpsLogId = gpsLogInfo.GpsLogId;
                    entity.latitude = gpsLogInfo.Latitude;
                    entity.longitude = gpsLogInfo.Longitude;
                    entity.syncDate = syncDate;
                    entity.timeStamp = gpsLogInfo.TimeStamp.ToLocalTime();
                    this.dbContext.SaveChanges();
                }
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
