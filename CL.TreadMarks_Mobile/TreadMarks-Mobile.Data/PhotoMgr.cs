﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class PhotoMgr
    {
        private TM_Entities dbContext;
        public PhotoMgr()
        {
            dbContext = new TM_Entities();
        }
        public Photo GetById(Guid id)
        {
            var query = this.dbContext.Photos.Where(t => t.photoId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public List<Photo> GetAllByTransactionId(Guid transactionId)
        {
            return this.dbContext.Photos.Where(t => t.transactionId == transactionId).ToList();
        }

        public DateTime? Save(IPhotoModel photoInfo)
        {
            try
            {
                DateTime syncDate;
                Photo entity = GetById(photoInfo.PhotoID);
                syncDate = DateTime.Now;
                if (entity == null)
                {
                    entity = new Photo();
                    this.dbContext.Photos.Add((Photo)entity);
                }
                entity.photoId = photoInfo.PhotoID;
                entity.createdDate = photoInfo.CreatedDate.ToLocalTime();
                entity.syncDate = syncDate;
                entity.latitude = photoInfo.Latitude;
                entity.longitude = photoInfo.Longitude;
                entity.comments = photoInfo.Comments;
                entity.date = photoInfo.Date.HasValue ? (DateTime?)photoInfo.Date.Value.ToLocalTime() : null;
                entity.fileName = photoInfo.FileName;
                entity.photoTypeId = photoInfo.PhotoTypeId;
                entity.transactionId = photoInfo.TransactionID;
                entity.uniqueFileName = photoInfo.UniqueFileName;

                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
