﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarksContracts;
using System.Data.SqlClient;

namespace TreadMarks_Mobile.Data
{
    public class PullStaticDataMgr
    {
        private TM_Entities dbContext;
        public PullStaticDataMgr()
        {
            dbContext = new TM_Entities();
        }
        public IEnumerable<DocumentType> GetDocumentTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.DocumentTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<Eligibility> GetEligibilityList(DateTime appSyncDate)
        {
            var query = this.dbContext.Eligibilities.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<Message> GetMessageList(DateTime appSyncDate)
        {
            var query = this.dbContext.Messages.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<PhotoPreSelectComment> GetPhotoPreSelectCommentList(DateTime appSyncDate)
        {
            var query = this.dbContext.PhotoPreSelectComments.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<PhotoType> GetPhotoTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.PhotoTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }
        public IEnumerable<RegistrantType> GetRegistrantTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.RegistrantTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<ScaleTicketType> GetScaleTicketTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.ScaleTicketTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TireType> GetTireTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TireTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionStatusType> GetTransactionStatusTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionStatusTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionType> GetTransactionTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionSyncStatusType> GetTransactionSyncStatusTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionSyncStatusTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionType_RegistrantType> GetTransactionType_RegistrantTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionType_RegistrantType.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionType_TireType> GetTransactionType_TireTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionType_TireType.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<UnitType> GetUnitTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.UnitTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<MaterialType> GetMaterialTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.MaterialTypes.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public IEnumerable<TransactionType_MaterialType> GetTransactionType_MaterialTypeList(DateTime appSyncDate)
        {
            var query = this.dbContext.TransactionType_MaterialType.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }
      

        public ICollection<SyncDatesModel> GetSyncDatesList()
        {
            ICollection<SyncDatesModel> SyncDatesList = new List<SyncDatesModel>();
            using (SqlConnection sConnection = (SqlConnection)this.dbContext.Database.Connection)
            {
                sConnection.Open();
                string query = @"select 'DocumentType' entityName, max(syncDate) lastSyncDate from DocumentType
                                union ALL
                                select 'Eligibility', max(syncDate) lastSyncDate from Eligibility
                                union ALL
                                select 'Message', max(syncDate) lastSyncDate from Message
                                union ALL
                                select 'Module', max(syncDate) lastSyncDate from Module
                                union ALL
                                select 'PhotoPreselectComment', max(syncDate) lastSyncDate from PhotoPreSelectComment
                                union ALL
                                select 'PhotoType', max(syncDate) lastSyncDate from PhotoType
                                union ALL
                                select 'Registrant', max(syncDate) lastSyncDate from Registrant
                                union ALL
                                select 'RegistrantType', max(syncDate) lastSyncDate from RegistrantType
                                union ALL
                                select 'ScaleTicketType', max(syncDate) lastSyncDate from ScaleTicketType
                                union ALL
                                select 'TireType', max(syncDate) lastSyncDate from TireType
                                union ALL
                                select 'TransactionStatusType', max(syncDate) lastSyncDate from TransactionStatusType
                                union ALL
                                select 'TransactionSyncStatusType', max(syncDate) lastSyncDate from TransactionSyncStatusType
                                union ALL
                                select 'TransactionType', max(syncDate) lastSyncDate from TransactionType
                                union ALL
                                select 'Location', max(syncDate) lastSyncDate from Location where isUserGenerated=0 
                                union ALL
                                select 'TransactionType_Module', max(syncDate) lastSyncDate from TransactionType_Module
                                union ALL
                                select 'TransactionType_RegistrantType', max(syncDate) lastSyncDate from TransactionType_RegistrantType
                                union ALL
                                select 'TransactionType_TireType', max(syncDate) lastSyncDate from TransactionType_TireType
                                union ALL
                                select 'UnitType', max(syncDate) lastSyncDate from UnitType
                                union ALL
                                select 'User', max(syncDate) lastSyncDate from [dbo].[User]
                                union ALL
                                select 'MaterialType', max(syncDate) lastSyncDate from [dbo].[MaterialType]
								union ALL
                                select 'TransactionType_MaterialType', max(syncDate) lastSyncDate from [dbo].[TransactionType_MaterialType]";
                using (SqlCommand cmd = new SqlCommand(query, sConnection))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr == null)
                        {
                            throw new NullReferenceException("No SyncDates Available.");
                        }
                        while (rdr.Read())
                        {
                            SyncDatesModel syncDateEntity = new SyncDatesModel();
                            syncDateEntity.entityName = rdr[0].ToString();
                            if(rdr[1]!=DBNull.Value)
                                syncDateEntity.lastUpdated = DateTime.SpecifyKind(Convert.ToDateTime(rdr[1]), DateTimeKind.Local);

                            SyncDatesList.Add(syncDateEntity);
                        }
                    }
                }
                return SyncDatesList;
            }     
        }

    }
}
