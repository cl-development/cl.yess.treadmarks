﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreadMarks_Mobile.Data;
using TreadMarksContracts;

namespace TreadMarks_Mobile.Data
{
    public class RegistrantMgr
    {
        private TM_Entities dbContext;
        public RegistrantMgr()
        {
            dbContext = new TM_Entities();
        }
        public Registrant GetById(decimal registrationNumber)
        {
            var query = this.dbContext.Registrants.Where(t => t.registrationNumber == registrationNumber);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }
        public IEnumerable<Registrant> GetList(DateTime appSyncDate)
        {
            var query = this.dbContext.Registrants.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public DateTime GetMaxLastUpdatedDate()
        {
            return this.dbContext.Registrants.Max(t => t.lastUpdatedDate) != null ? this.dbContext.Registrants.Max(t => t.lastUpdatedDate).Value : new DateTime(2000, 01, 01);
        }

        public IRegistrantModelExtended SaveRegistrantWithLocation(IRegistrantModelExtended regInfo)
        {
            try
            {
                DateTime syncDate = DateTime.Now;
                
                Registrant entity = GetById(regInfo.RegistrationNumber);
                //If not already existing create one
                if (entity == null)
                {
                    
                    entity = new Registrant();               
                    this.dbContext.Registrants.Add((Registrant)entity);

                    //need to clean this code for location
                    //create location ... made changes to this line - OTSJ-984
                    regInfo.LocationId = new Guid(regInfo.RegistrationNumber.ToString().PadLeft(32, '0')); ;

                    //new entity but location already there
                    Location loc = this.dbContext.Locations.Where(l => l.locationId == regInfo.LocationId).FirstOrDefault();
                    if (loc != null)
                    {
                        loc.address1 = regInfo.LocationAddress1;
                        loc.address2 = regInfo.LocationAddress2;
                        loc.address3 = regInfo.LocationAddress3;
                        loc.city = regInfo.City;
                        loc.country = regInfo.Country;
                        loc.fax = regInfo.LocationFax;
                        loc.phone = regInfo.LocationPhone;
                        loc.postalCode = regInfo.PostalCode;
                        loc.province = regInfo.Province;
                        loc.syncDate = syncDate;
                        loc.isUserGenerated = false;
                    }
                    else
                    {
                        this.dbContext.Locations.Add(new Location
                        {
                            address1 = regInfo.LocationAddress1,
                            address2 = regInfo.LocationAddress2,
                            address3 = regInfo.LocationAddress3,
                            city = regInfo.City,
                            country = regInfo.Country,
                            fax = regInfo.LocationFax,
                            locationId = (Guid)regInfo.LocationId,
                            phone = regInfo.LocationPhone,
                            postalCode = regInfo.PostalCode,
                            province = regInfo.Province,
                            syncDate = syncDate,
                            isUserGenerated = false
                        });
                    }
                }
                else
                {
                    regInfo.LocationId = entity.locationId;
                    
                    //Update existing location associated with existing entity               
                    Location loc = this.dbContext.Locations.Where(l => l.locationId == regInfo.LocationId).FirstOrDefault();
                    loc.address1 = regInfo.LocationAddress1;
                    loc.address2 = regInfo.LocationAddress2;
                    loc.address3 = regInfo.LocationAddress3;
                    loc.city = regInfo.City;
                    loc.country = regInfo.Country;
                    loc.fax = regInfo.LocationFax;
                    loc.phone = regInfo.LocationPhone;
                    loc.postalCode = regInfo.PostalCode;
                    loc.province = regInfo.Province;
                    loc.syncDate = syncDate;
                    loc.isUserGenerated = false;
                }
                
                entity.businessName = regInfo.BusinessName;

                if (regInfo.CreatedDate != null && regInfo.CreatedDate != DateTime.MinValue)
                    entity.createdDate = regInfo.CreatedDate;
                
                entity.locationId = regInfo.LocationId;
                //entity.metadata = regInfo.Metadata;

                if (regInfo.ModifiedDate != null)
                    entity.modifiedDate = regInfo.ModifiedDate;

                entity.registrantTypeId = ConvertToRegistrantType(regInfo.RegistrantTypeId);
                entity.registrationNumber = regInfo.RegistrationNumber;

                if (regInfo.ActivationDate != null && regInfo.ActivationDate != DateTime.MinValue)
                    entity.activationDate = regInfo.ActivationDate;

                entity.isActive = regInfo.IsActive;

                if (regInfo.ActiveStateChangeDate != null && regInfo.ActiveStateChangeDate != DateTime.MinValue)
                    entity.activeStateChangeDate = regInfo.ActiveStateChangeDate;

                if (regInfo.LastUpdatedDate != null)
                    entity.lastUpdatedDate = regInfo.LastUpdatedDate;                

                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();
                regInfo.SyncDate = entity.syncDate;
                return regInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public enum RegistrantTypeCentral : byte
        {
            Collector = 1,
            Hauler = 2,
            Steward = 3,
            RPM = 4,
            Processor = 5,
        }   
        
        public enum VendorTypeNewTM : byte
        {
            Collector = 2,
            Hauler = 3,            
            RPM = 5,
            Processor = 4,
        }

        long ConvertToRegistrantType(long vendorType)
        {
            switch (vendorType)
            {
                case (int)VendorTypeNewTM.Collector:
                    return (int)RegistrantTypeCentral.Collector;
                case (int)VendorTypeNewTM.Hauler:
                    return (int)RegistrantTypeCentral.Hauler;
                case (int)VendorTypeNewTM.Processor:
                    return (int)RegistrantTypeCentral.Processor;
                case (int)VendorTypeNewTM.RPM:
                    return (int)RegistrantTypeCentral.RPM;
            }
            return 0;
        }
        
        public void SaveRegistrantWithLocation(VendorModel vendorInfo)
        {
            try
            {
                DateTime syncDate = DateTime.Now;

                //Alphanumeric handling not done on device so need this stupid code
                Registrant entity = GetById(Convert.ToDecimal(vendorInfo.Number));
                //If not already existing create one
                if (entity == null)
                {

                    entity = new Registrant();
                    this.dbContext.Registrants.Add((Registrant)entity);

                    //need to clean this code for location
                    //create location ... made changes to this line - OTSJ-984
                    entity.locationId = new Guid(vendorInfo.Number.PadLeft(32, '0')); ;

                    //new entity but location already there
                    Location loc = this.dbContext.Locations.Where(l => l.locationId == entity.locationId).FirstOrDefault();
                    if (loc != null)
                    {
                        loc.address1 = vendorInfo.LocationAddress.Address1;
                        loc.address2 = vendorInfo.LocationAddress.Address2;
                        loc.address3 = vendorInfo.LocationAddress.Address3;
                        loc.city = vendorInfo.LocationAddress.City;
                        loc.country = vendorInfo.LocationAddress.Country;
                        loc.fax = vendorInfo.LocationAddress.Fax;
                        loc.phone = vendorInfo.LocationAddress.Phone;
                        loc.postalCode = vendorInfo.LocationAddress.PostalCode;
                        loc.province = vendorInfo.LocationAddress.Province;
                        loc.syncDate = syncDate;
                        loc.isUserGenerated = false;
                    }
                    else
                    {
                        this.dbContext.Locations.Add(new Location
                        {
                            address1 = vendorInfo.LocationAddress.Address1,
                            address2 = vendorInfo.LocationAddress.Address2,
                            address3 = vendorInfo.LocationAddress.Address3,
                            city = vendorInfo.LocationAddress.City,
                            country = vendorInfo.LocationAddress.Country,
                            fax = vendorInfo.LocationAddress.Fax,
                            locationId = entity.locationId.Value,
                            phone = vendorInfo.LocationAddress.Phone,
                            postalCode = vendorInfo.LocationAddress.PostalCode,
                            province = vendorInfo.LocationAddress.Province,
                            syncDate = syncDate,
                            isUserGenerated = false
                        });
                    }
                }
                else
                {
                   
                    //Update existing location associated with existing entity               
                    Location loc = this.dbContext.Locations.Where(l => l.locationId == entity.locationId).FirstOrDefault();
                    loc.address1 = vendorInfo.LocationAddress.Address1;
                    loc.address2 = vendorInfo.LocationAddress.Address2;
                    loc.address3 = vendorInfo.LocationAddress.Address3;
                    loc.city = vendorInfo.LocationAddress.City;
                    loc.country = vendorInfo.LocationAddress.Country;
                    loc.fax = vendorInfo.LocationAddress.Fax;
                    loc.phone = vendorInfo.LocationAddress.Phone;
                    loc.postalCode = vendorInfo.LocationAddress.PostalCode;
                    loc.province = vendorInfo.LocationAddress.Province;
                    loc.syncDate = syncDate;
                    loc.isUserGenerated = false;
                }

                entity.locationId = new Guid(vendorInfo.Number.ToString().PadLeft(32, '0')); 
                entity.businessName = vendorInfo.BusinessName;

                if (vendorInfo.CreatedDate != null && vendorInfo.CreatedDate != DateTime.MinValue)
                    entity.createdDate = vendorInfo.CreatedDate;

                //Discussed with Aditya (Mobile team lead) to store alphanumeric number in metadata 2014-04-24
                //Device is not programmed yet to handle metadata so commenting it 2015-0427
                //entity.metadata = vendorInfo.Number;

                entity.modifiedDate = vendorInfo.LastUpdatedDate;

                //entity.registrantTypeId = vendorInfo.VendorTypeID;
                entity.registrantTypeId = ConvertToRegistrantType(vendorInfo.VendorTypeID);
                
                
                //entity.registrationNumber = Decimal.Parse(vendorInfo.Number);
                //Alphanumeric handling not done on device so need this stupid code                
                entity.registrationNumber = Convert.ToDecimal(vendorInfo.Number);

                if (vendorInfo.ActivationDate != null && vendorInfo.ActivationDate != DateTime.MinValue)
                    entity.activationDate = vendorInfo.ActivationDate;

                entity.isActive = vendorInfo.IsActive;

                if (vendorInfo.ActiveStateChangeDate != null && vendorInfo.ActiveStateChangeDate != DateTime.MinValue)
                    entity.activeStateChangeDate = vendorInfo.ActiveStateChangeDate;

                if (vendorInfo.LastUpdatedDate != null)
                    entity.lastUpdatedDate = vendorInfo.LastUpdatedDate;

                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        public DateTime? Save(IRegistrantModel registrantInfo)
        {
            try
            {
                DateTime syncDate = DateTime.Now;
                Registrant entity = GetById(registrantInfo.RegistrationNumber);
                if (entity == null)
                {
                    entity = new Registrant();
                    this.dbContext.Registrants.Add((Registrant)entity);
                }
                entity.businessName = registrantInfo.BusinessName;
                entity.createdDate = registrantInfo.CreatedDate;
                entity.locationId = registrantInfo.LocationId;
                entity.metadata = registrantInfo.Metadata;
                entity.modifiedDate = registrantInfo.ModifiedDate;
                entity.registrantTypeId = registrantInfo.RegistrantTypeId;
                entity.registrationNumber = registrantInfo.RegistrationNumber;
                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
