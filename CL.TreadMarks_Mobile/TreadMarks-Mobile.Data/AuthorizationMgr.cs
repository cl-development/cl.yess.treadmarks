﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTS_MobileProto.Data;
using otscontracts;

namespace OTS_MobileProto.Data
{
    public class AuthorizationMgr
    {
        private OTS_MobileEntities dbContext;
        public AuthorizationMgr()
        {
            dbContext = new OTS_MobileEntities();
        }
        public Authorization GetById(Guid id)
        {
            var query = this.dbContext.Authorizations.Where(t => t.authorizationId == id);
            if (query.Count() == 0)
                return null;
            else
                return query.First();
        }

        public IEnumerable<Authorization> GetList(DateTime appSyncDate)
        {
            var query = this.dbContext.Authorizations.Where(t => t.syncDate > appSyncDate);
            if (query.Count() == 0)
                return null;
            else
                return query.AsEnumerable();
        }

        public DateTime? Save(ISTCAuthorizationModel authorizationInfo)
        {
            try
            {
                DateTime syncDate = DateTime.UtcNow;
                Authorization entity = GetById(authorizationInfo.Authorization.authorizationId);
                if (entity == null)
                {
                    entity = new Authorization();
                    this.dbContext.Authorizations.Add((Authorization)entity);
                }
                entity.authorizationId = authorizationInfo.Authorization.authorizationId;
                entity.authorizationNumber = authorizationInfo.Authorization.authorizationNumber;
                entity.expiryDate = authorizationInfo.Authorization.expiryDate;
                entity.transactionId = authorizationInfo.Authorization.transactionId;
                entity.transactionTypeId = authorizationInfo.Authorization.transactionTypeId;
                entity.userId = authorizationInfo.Authorization.userId;

                LocationMgr locMgr = new LocationMgr();
                Location existingLoc = locMgr.GetById(authorizationInfo.Location.LocationId);

                //Update location in location table
                if (existingLoc != null)
                    locMgr.Save(authorizationInfo.Location);
                //Add location to location table and STCAuthorization table
                else
                    entity.Locations.Add(new Location
                        {
                            address1 = authorizationInfo.Location.Address1,
                            address2 = authorizationInfo.Location.Address2,
                            address3 = authorizationInfo.Location.Address3,
                            city = authorizationInfo.Location.City,
                            country = authorizationInfo.Location.Country,
                            fax = authorizationInfo.Location.Fax,
                            latitude = authorizationInfo.Location.Latitude,
                            locationId = authorizationInfo.Location.LocationId,
                            longitude = authorizationInfo.Location.Longitude,
                            name = authorizationInfo.Location.Name,
                            phone = authorizationInfo.Location.Phone,
                            postalCode = authorizationInfo.Location.PostalCode,
                            province = authorizationInfo.Location.Province,
                            syncDate = syncDate
                        });
                entity.syncDate = syncDate;
                this.dbContext.SaveChanges();
                return syncDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
