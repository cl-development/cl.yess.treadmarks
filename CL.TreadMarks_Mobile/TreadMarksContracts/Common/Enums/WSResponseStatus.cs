﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts.Common.Enums
{
    /// <summary>
    /// Enumeration which defines possible Web Service Response Status values and their labels
    /// </summary>
    public enum WSResponseStatus
    {
        NONE = 0,
        OK = 1,
        ERROR = 2,
        WARNING = 3
    }
}
