﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts.Common.Enums
{
    /// <summary>
    /// Enumeration which defines possible Web Service Response Status values and their labels
    /// </summary>
    public enum SyncStatuses
    {
        [Description("Device_Not_Attempted_Repository")]
        Device_Not_Attempted_Repository = 1,

        [Description("Device_Sync_Repository")]
        Device_Sync_Repository = 2,

        [Description("Device_Sync_Fail_Repository")]
        Device_Sync_Fail_Repository = 3,

        [Description("Repository_Sent_To_TM")]
        Repository_Sent_To_TM = 4,

        [Description("Repository_Sync_TM")]
        Repository_Sync_TM = 5,

        [Description("Repository_Sync_Fail_TM")]
        Repository_Sync_Fail_TM = 6,  

    }
}
