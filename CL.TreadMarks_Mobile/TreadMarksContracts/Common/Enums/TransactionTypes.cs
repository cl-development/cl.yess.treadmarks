﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreadMarksContracts.Common.Enums
{
    /// <summary>
    /// Enumeration which defines possible Transaction Type values and their labels
    /// </summary>
    public enum TransactionTypes
    {
        [Description("TCR")]
        TCR = 1,

        [Description("DOT")]
        DOT = 2,

        [Description("HIT")]
        HIT = 3,

        [Description("RTR")]
        RTR = 4,

        [Description("STC")]
        STC = 5,

        [Description("UCR")]
        UCR = 6,

        [Description("PTR")]
        PTR = 7,

        [Description("PIT")]
        PIT = 8,
    }    
}
