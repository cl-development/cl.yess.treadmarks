namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionType_MaterialTypeModel
    {
        long TransactionTypeMaterialTypeId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        long MaterialTypeId { get; set; }
        long TransactionTypeId { get; set; }
    }
}
