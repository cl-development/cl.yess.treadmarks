namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public class AddressModel
    {
        public int ID { get; set; }
        public int AddressTypeID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<decimal> GPSCoordinate1 { get; set; }
        public Nullable<decimal> GPSCoordinate2 { get; set; }
    }
}
