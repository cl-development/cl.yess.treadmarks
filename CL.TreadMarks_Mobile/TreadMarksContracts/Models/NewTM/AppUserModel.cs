namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public class AppUserModel
    {
        public int ID { get; set; }
        public int VendorID { get; set; }
        public int VendorAppUserID { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string EMail { get; set; }
        public int AccessTypeID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string Metadata { get; set; }
        public Nullable<DateTime> LastAccessOn { get; set; }
        public Nullable<DateTime> SyncOn { get; set; }
        public bool Active { get; set; }
    }
}
