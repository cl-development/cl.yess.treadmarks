namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class EligibilityModel
    {
        public EligibilityModel()
        {}
        public long EligibilityId { get; set; }
        public string NameKey { get; set; }
        public long SortIndex { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public long TransactionTypeId { get; set; }
    }
}
