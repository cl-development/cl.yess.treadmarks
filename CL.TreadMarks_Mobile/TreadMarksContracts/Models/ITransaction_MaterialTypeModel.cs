namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    
    public interface ITransaction_MaterialTypeModel
    {
        System.Guid TransactionMaterialTypeId { get; set; }
        long MaterialTypeId { get; set; }
        System.Guid TransactionId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}

