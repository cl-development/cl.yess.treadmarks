namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TransactionType_RegistrantTypeModel
    {
        public TransactionType_RegistrantTypeModel()
        { }
        public long TransactionTypeRegistrantTypeId { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public long RegistrantTypeId { get; set; }
        public long TransactionTypeId { get; set; }
    }
}
