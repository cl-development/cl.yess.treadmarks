﻿namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransaction_EligibilityModel
    {
        System.Guid TransactionEligibilityId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        bool Value { get; set; }
        long EligibilityId { get; set; }
        System.Guid TransactionId { get; set; }

    }
}
