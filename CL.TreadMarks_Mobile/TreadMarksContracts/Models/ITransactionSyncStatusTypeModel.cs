namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionSyncStatusTypeModel
    {
        long TransactionSyncStatusTypeId { get; set; }
        string NameKey { get; set; }
        string FileName { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        string internalDescription { get; set; }
        string userMessage { get; set; }
    }
}
