namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class ScaleTicketTypeModel
    {
        public ScaleTicketTypeModel()
        { }
        public long ScaleTicketTypeId { get; set; }
        public string NameKey { get; set; }
        public long SortIndex { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
