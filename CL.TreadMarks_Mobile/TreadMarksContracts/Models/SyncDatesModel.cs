﻿namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class SyncDatesModel 
    {
        public SyncDatesModel()
        { }
        [DataMember(Name = "entityName")]
        public string entityName { get; set; }
        [DataMember(Name = "lastUpdated")]
        public System.DateTime? lastUpdated { get; set; }        

    }
}
