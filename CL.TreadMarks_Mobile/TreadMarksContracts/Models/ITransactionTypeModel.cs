namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionTypeModel 
    {
        long TransactionTypeId { get; set; }
        string NameKey { get; set; }
        string ShortNameKey { get; set; }
        string FileName { get; set; }
        int SortIndex { get; set; }
        string OutgoingSignatureKey { get; set; }
        string IncomingSignatureKey { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        string TireCountMessageKey { get; set; }
    }
}
