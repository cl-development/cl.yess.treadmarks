namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
   
    public interface IPhotoTypeModel
    {
        int PhotoTypeId { get; set; }
        string Name { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
