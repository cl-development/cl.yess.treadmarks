﻿using System;
using System.Collections.Generic;

namespace TreadMarksContracts.Models
{
    public interface IModel
    {
        bool Validate();
        IEnumerable<string> GetErrors();
    }
}
