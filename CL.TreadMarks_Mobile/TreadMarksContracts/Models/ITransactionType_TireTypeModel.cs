﻿namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface ITransactionType_TireTypeModel
    {
        long TransactionTypeTireTypeId { get; set; }
        long TireTypeId { get; set; }
        long TransactionTypeId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }

    }
}
