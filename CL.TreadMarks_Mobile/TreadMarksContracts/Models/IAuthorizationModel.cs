namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
   
    public interface IAuthorizationModel
    {        
        Guid authorizationId { get; set; }
        string authorizationNumber { get; set; }
        Nullable<System.DateTime> expiryDate { get; set; }
        Nullable<System.DateTime> syncDate { get; set; }
        long userId { get; set; }
        Nullable<System.Guid> transactionId { get; set; }
        Nullable<long> transactionTypeId { get; set; }    
        ICollection<ILocationModel> Locations { get; set; }
    }
}
