namespace otscontracts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]  
    public partial class ModuleModel
    {
        public ModuleModel()
        {}    
        public long ModuleId { get; set; }
        public string NameKey { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}
