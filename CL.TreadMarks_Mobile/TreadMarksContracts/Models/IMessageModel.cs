namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    public interface IMessageModel
    {
        long MessageId { get; set; }
        string NameKey { get; set; }
        System.DateTime SyncDate { get; set; }
    }
}
