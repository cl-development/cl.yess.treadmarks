namespace TreadMarksContracts
{
    using System;
    using System.Collections.Generic;
    
    public interface ITransaction_TireTypeModel
    {
        System.Guid TransactionTireTypeId { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
        long Quantity { get; set; }
        long TireTypeId { get; set; }
        System.Guid TransactionId { get; set; }
    }
}

