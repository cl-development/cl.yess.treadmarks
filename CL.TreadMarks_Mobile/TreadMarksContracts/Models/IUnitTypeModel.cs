namespace TreadMarksContracts
{
    using System;
   
    public interface IUnitTypeModel
    {
        long UnitTypeId { get; set; }
        double KgMultiplier { get; set; }
        string NameKey { get; set; }
        Nullable<System.DateTime> SyncDate { get; set; }
    }
}
