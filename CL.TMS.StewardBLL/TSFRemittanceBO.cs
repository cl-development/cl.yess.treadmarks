﻿using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.IRepository.System;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using CL.TMS.IRepository.Registrant;
using CL.TMS.Common.Enum;
using CL.TMS.Security.Authorization;
using CL.TMS.Common;
using CL.Framework.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Common.Helper;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.StewardBLL
{
    public class TSFRemittanceBO
    {
        private ITSFRemittanceRepository remittanceRepository;
        private IVendorRepository vendorRepository;
        private readonly IGpRepository gpRepository;
        private IMessageRepository messageRepository; //OTSTM2-551
        private readonly IUserRepository userRepository;  //OTSTM2-551
        private IEventAggregator eventAggregator; //OTSTM2-553

        public TSFRemittanceBO(IVendorRepository vendorRepository, ITSFRemittanceRepository remittanceRepository, IGpRepository gpRepository, IMessageRepository messageRepository, IUserRepository userRepository, IEventAggregator eventAggregator)
        {
            this.remittanceRepository = remittanceRepository;
            this.vendorRepository = vendorRepository;
            this.gpRepository = gpRepository;
            this.messageRepository = messageRepository; //OTSTM2-551
            this.userRepository = userRepository; //OTSTM2-551
            this.eventAggregator = eventAggregator; //OTSTM2-553
        }

        public PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceBrief(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var result = remittanceRepository.LoadAllTSFRemittance(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
            return result;
        }

        public PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceBriefByCustomer(string RegistertionNo, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var result = remittanceRepository.LoadAllTSFRemittanceByCustomer(RegistertionNo, pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
            return result;
        }

        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod)
        {
            return remittanceRepository.GetRateList(reportingPeriod);
        }

        //Add for 2X
        public IReadOnlyList<PeriodListModel> GetTSFRateDateList(DateTime PeriodDate)
        {
            var currentYear = PeriodDate.Year;
            var periodListModes = new List<PeriodListModel>();
            if (PeriodDate.Month == 6)
            {
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Jan " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Jan " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Feb " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Feb " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Mar " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Mar " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Apr " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Apr " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "May " + currentYear,
                    TSFRateDate = Convert.ToDateTime("May " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Jun " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Jun " + currentYear)
                });
            }
            else if (PeriodDate.Month == 12)
            {
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Jul " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Jul " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Aug " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Aug " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Sep " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Sep " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Oct " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Oct " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Nov " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Nov " + currentYear)
                });
                periodListModes.Add(new PeriodListModel
                {
                    ShortName = "Dec " + currentYear,
                    TSFRateDate = Convert.ToDateTime("Dec " + currentYear)
                });
            }
            return periodListModes;
        }

        public int AddTSFClaim(TSFRemittanceModel model, ref bool isCreated)
        {
            return remittanceRepository.AddTSFClaim(model, ref isCreated);
        }

        public TSFRemittanceModel GetTSFRemittanceById(int id, int customerID)
        {
            var customer = vendorRepository.FindCustomerByID(customerID);
            var remittanceModel = remittanceRepository.GetTSFRemittanceByCustomerId(customer);
            remittanceModel = remittanceRepository.PopulateTSFModelByTSFClaimId(id, remittanceModel);

            var currentActiveHistory = vendorRepository.LoadCurrenActiveHistoryForCustomer(customer.ID);
            remittanceModel.ActivityStatus = currentActiveHistory.ActiveState ? ActivityStatusEnum.Active : ActivityStatusEnum.InActive;
            return remittanceModel;
        }
        public TSFRemittanceModel GetTSFRemittanceByClaimId(int id)
        {
            var tsfClaim = remittanceRepository.GetTSFClaimById(id);
            if (tsfClaim != null)
            {
                var customer = vendorRepository.FindCustomerByID(tsfClaim.CustomerID);
                var status = tsfClaim.RecordState;// EnumHelper.GetValueFromDescription<ClaimStatus>(tsfClaim.RecordState);

                var remittanceModel = remittanceRepository.GetTSFRemittanceByCustomerId(customer);
                remittanceModel = remittanceRepository.PopulateTSFModelByTSFClaimId(id, remittanceModel);
                remittanceModel.AssignToUser = tsfClaim.AssignToUser;

                var currentActiveHistory = vendorRepository.LoadCurrenActiveHistoryForCustomer(customer.ID);
                remittanceModel.ActivityStatus = currentActiveHistory.ActiveState ? ActivityStatusEnum.Active : ActivityStatusEnum.InActive;
                remittanceModel.BatchNumber = gpRepository.GetBatchIdByTSFClaimId(id);

                //OTSTM2-1360
                var stewardHstNumber = AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.StewardHstNumber");               
                remittanceModel.HstAccountNumber = stewardHstNumber != null ? stewardHstNumber.Value : string.Empty;
                var tsfTaxApplicablePeriod= AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFTaxApplicablePeriod");
                remittanceModel.IsHstAccountNumberShowable = tsfTaxApplicablePeriod !=null && tsfClaim.Period.StartDate >= Convert.ToDateTime(tsfTaxApplicablePeriod.Value);

                return remittanceModel;
            }
            else
            {
                return null;
            }
        }
        public TSFRemittanceBriefModel GetFirstSubmitRemittance()
        {
            return remittanceRepository.GetFirstSubmitRemittance();
        }

        private void ApplyStaffUserSecurity(ClaimStatus status, TSFRemittanceModel remittanceClaimSummary)
        {
            remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
            remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
            remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonly = true;
            remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
            remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.ApproveDisable = true;

            if (SecurityContextHelper.IsStaff())
            {
                //TODO: SecurityContextHelper.IsTSFClerk is removed, please use the new method
                if (true) //SecurityContextHelper.IsTSFClerk()) //only TSFClerk can edit remittance
                {
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = false;
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonly = false;
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = false;
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.ApproveDisable = false;
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = SecurityContextHelper.HasEditable;
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        remittanceClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        public void AddRemittanceDetail(TSFRemittanceItemModel model)
        {

            remittanceRepository.AddRemittanceDetail(model);
        }

        public void UpdatePenaltiesManually(int claimID, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false)
        {

            remittanceRepository.UpdatePenaltiesManually(claimID, penaltiesManually, modifiedBy, IsPenaltyOverride);
        }
        public void UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true)
        {

            remittanceRepository.UpdateHST(tsfClaimId, modifiedBy, IsTaxApplicable);
        }

        public void UpdateSupportingDocumentValue(int id, string docOptionValue, string modifiedBy)
        {

            remittanceRepository.UpdateSupportingDocumentValue(id, docOptionValue, modifiedBy);
        }

        public GpResponseMsg UpdatePaymentType(int id, string paymentType, string modifiedBy)
        {
            var msg = new GpResponseMsg();
            var claim = remittanceRepository.GetTSFClaimById(id);
            if (claim != null)
            {
                claim.PaymentType = paymentType;
                claim.ModifiedUser = modifiedBy;
                claim.ModifiedDate = DateTime.Now;
                if ((claim.CurrencyType == CurrencyType.USD.ToString()) && (paymentType == "C"))
                {
                    claim.Account = "1020-00-00-00";
                }
                else
                {
                    claim.Account = null;
                }
                msg.ID = claim.ID;
                remittanceRepository.UpdateTsfClaim(claim);
            }
            else
            {
                msg.Errors.Add("Claim does not exist");
                return msg;
            }
            return msg;
        }

        public GpResponseMsg UpdateCurrencyType(int id, string currencyType, string modifiedBy)
        {
            var msg = new GpResponseMsg();
            var claim = remittanceRepository.GetTSFClaimById(id);
            if (claim != null)
            {
                claim.CurrencyType = currencyType;
                claim.ModifiedUser = modifiedBy;
                claim.ModifiedDate = DateTime.Now;
                if ((claim.PaymentType == "C") && (currencyType == CurrencyType.USD.ToString()))
                {
                    claim.Account = "1020-00-00-00";
                }
                else
                {
                    claim.Account = null;
                }
                msg.ID = claim.ID;
                remittanceRepository.UpdateTsfClaim(claim);
            }
            else
            {
                msg.Errors.Add("Claim does not exist");
                return msg;
            }
            return msg;
        }


        public IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod, int ItemID)
        {
            return remittanceRepository.GetRateList(reportingPeriod, ItemID);
        }

        //OTSTM2-567
        public RateListModel GetRate(DateTime reportingPeriod, int ItemID)
        {
            return remittanceRepository.GetRate(reportingPeriod, ItemID);
        }

        public void DeleteRemittanceDetail(int claimId, int detailId)
        {
            remittanceRepository.DeleteRemittanceDetail(claimId, detailId);
        }
        public TSFRemittanceItemModel GetRemittanceDetail(int claimId, int detailId)
        {
            return remittanceRepository.GetRemittanceDetail(claimId, detailId);
        }

        public void SetRemittanceStatus(int ClaimId, string remittanceStatus, string updatedBy, long assignToUser, bool isAssigningToMe)
        {
            var assignedToUserReference = new UserReference();
            if (assignToUser == -1) //for Un-Assign only
            {
                var claim = remittanceRepository.GetTSFClaimById(ClaimId);
                var assignedToUserId = (long)claim.AssignToUser;
                var assignedToUser = userRepository.FindUserByUserId(assignedToUserId);
                assignedToUserReference.Id = assignedToUser.ID;
                assignedToUserReference.Email = assignedToUser.Email;
                assignedToUserReference.FirstName = assignedToUser.FirstName;
                assignedToUserReference.LastName = assignedToUser.LastName;
                assignedToUserReference.UserName = assignedToUser.UserName;
            }
            var activityModel = remittanceRepository.SetRemittanceStatus(ClaimId, remittanceStatus, updatedBy, assignToUser, isAssigningToMe);
            if (activityModel.IsApproved || activityModel.IsAssigned || activityModel.IsAssignToMe || activityModel.IsBackToParticipant || activityModel.IsReassigned || activityModel.IsSubmitted || activityModel.IsUnassigned)
            {
                if (assignToUser == 0) //for Submit/Back-To-Participant/Approve
                {
                    AddActivity(ClaimId, updatedBy, null, activityModel);
                }
                else if (assignToUser == -1) //for Un-Assign
                {
                    AddActivity(ClaimId, updatedBy, assignedToUserReference, activityModel);
                }
                else //for Assign/Re-Assign/Assign-To-Me
                {
                    var assignee = userRepository.FindUserByUserId(assignToUser);
                    var assigneeUser = new UserReference
                    {
                        Id = assignee.ID,
                        Email = assignee.Email,
                        FirstName = assignee.FirstName,
                        LastName = assignee.LastName,
                        UserName = assignee.UserName
                    };
                    AddActivity(ClaimId, updatedBy, assigneeUser, activityModel);
                }

            }
        }

        private void AddActivity(int claimId, string updatedBy, UserReference assigneeUser, StewardActivityHandlingModel activityModel)
        {
            var assignerUser = SecurityContextHelper.CurrentUser;

            var activity = new Activity();
            activity.Initiator = assignerUser.UserName;
            activity.InitiatorName = assignerUser.FullName;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? assigneeUser.FirstName + " " + assigneeUser.LastName : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.TsfClaimActivity;
            activity.ActivityArea = TreadMarksConstants.StewardType;
            activity.ObjectId = claimId;
            if (activityModel.IsApproved)
            {
                activity.Message = string.Format("{0} <strong>approved</strong> this remittance.", activity.InitiatorName);
            }
            else if (activityModel.IsSubmitted)
            {
                activity.Message = string.Format("Participant <strong>submitted</strong> the remittance.");
            }
            else if (activityModel.IsBackToParticipant)
            {
                activity.Message = string.Format("{0} sent this remittance <strong>back</strong> to the <strong>Participant</strong>.", activity.InitiatorName);
            }
            else if (activityModel.IsAssignToMe)
            {
                activity.Message = string.Format("Remittance <strong>assigned</strong> to <strong>{0}</strong>.", activity.AssigneeName);
            }
            else if (activityModel.IsAssigned || activityModel.IsReassigned)
            {
                activity.Message = string.Format("{0} <strong>assigned</strong> this remittance to <strong>{1}</strong>.", activity.InitiatorName, activity.AssigneeName);
            }
            else if (activityModel.IsUnassigned)
            {
                activity.Message = string.Format("{0} <strong>unassigned</strong> this remittance from <strong>{1}</strong>.", activity.InitiatorName, activity.AssigneeName);
            }

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(claimId);
        }

        public decimal? SetRemittanceStatus(int ClaimId, string remittanceStatus, string updatedBy, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignToUser)
        {
            return remittanceRepository.SetRemittanceStatus(ClaimId, remittanceStatus, updatedBy, submitted, received, deposit, referenceNo, amount, PaymentType, assignToUser);
        }

        public void SetRemittanceStatusAuto(int ClaimId, string remittanceStatus, string updatedBy, string amount, long assignToUser)
        {
            remittanceRepository.SetRemittanceStatusAuto(ClaimId, remittanceStatus, updatedBy, amount, assignToUser);
        }
        public List<TSFRemittanceBriefModel> LoadRemittanceApplications(string searchText, string sortcolumn, string sortdirection, string regNo, Dictionary<string, string> columnSearchText)
        {
            return remittanceRepository.LoadRemittanceApplications(searchText, sortcolumn, sortdirection, regNo, columnSearchText);
        }

        public void AddTSFRemittanceNote(TSFRemittanceNote applicationNote)
        {
            remittanceRepository.AddTSFRemittanceNote(applicationNote);
        }

        public TSFClaim GetLastApprovedTSFRemittance(int customerID, int claimID)
        {
            return remittanceRepository.GetLastApprovedTSFRemittance(customerID, claimID);
        }

        public IList GetTSFClaimPeriodForBreadCrumb(int customerId)
        {
            return remittanceRepository.GetTSFClaimPeriodForBreadCrumb(customerId);
        }
        public IEnumerable GetTSFClaimPeriods(int periodID)
        {
            return remittanceRepository.GetTSFClaimPeriods(periodID);
        }
        public bool IsAnyPastPenalty(int customerID, int claimID)
        {
            return remittanceRepository.IsAnyPastPenalty(customerID, claimID);
        }

        public VendorReference GetVendorReference(int tsfClaimId)
        {
            return remittanceRepository.GetVendorReference(tsfClaimId);
        }

        //OTSTM2-21
        public Customer GetCustomerByCustomerId(int customerId)
        {
            return vendorRepository.FindCustomerByID(customerId);
        }

        //OTSTM2-485
        #region internal adjustment
        public PaginationDTO<TSFClaimInternalAdjustment, int> LoadTSFClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false)
        {
            return this.remittanceRepository.LoadTSFClaimInternalAdjustments(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId, isStaff);//LoadClaimInternalAdjustments
        }

        public bool AddInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            if (modalResult.SelectedItem.Id == 1)
            {
                AddInternalPaymentAdjustment(claimId, modalResult);
                return true;
            }
            return false;
        }

        private void AddInternalPaymentAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            if (modalResult.Amount != 0)
            {
                if (string.IsNullOrWhiteSpace(modalResult.PaymentType))
                    modalResult.PaymentType = "Overall";
                var tsfclaimInternalPaymentAdjustment = new TSFClaimInternalPaymentAdjustment();
                tsfclaimInternalPaymentAdjustment.TSFClaimId = claimId;
                tsfclaimInternalPaymentAdjustment.AdjustBy = SecurityContextHelper.CurrentUser.Id;
                tsfclaimInternalPaymentAdjustment.AdjustDate = DateTime.UtcNow;
                tsfclaimInternalPaymentAdjustment.AdjustmentAmount = modalResult.Amount;
                tsfclaimInternalPaymentAdjustment.PaymentType = (int)(modalResult.PaymentType.ToEnum<TSFInternalAdjustmentPaymentType>());
                tsfclaimInternalPaymentAdjustment.Status = TSFInternalAdjustmentStatus.Open.ToString();

                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    remittanceRepository.AddInternalPaymentAdjustment(tsfclaimInternalPaymentAdjustment);

                    remittanceRepository.AddInternalAdjustmentNote(new TSFClaimInternalAdjustmentNote()
                    {
                        TSFInternalAdjustmentId = tsfclaimInternalPaymentAdjustment.ID,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });

                    transationScope.Complete();
                }
            }
        }

        public void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            remittanceRepository.RemoveInternalAdjustment(claimId, adjustmentType, internalAdjustmentId);
        }

        public TSFClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId)
        {
            var result = new TSFClaimAdjustmentModalResult();
            if (internalAdjustType == (int)(TSFInternalAdjustmentType.Payment))
            {
                var claimInternalPaymentAdjust = remittanceRepository.GetClaimInternalPaymentAdjust(internalAdjustId);
                result.SelectedItem.Id = 1;
                result.SelectedItem.Name = "Payment";
                result.PaymentType = ((TSFInternalAdjustmentPaymentType)claimInternalPaymentAdjust.PaymentType).ToString();
                result.Amount = Math.Round(claimInternalPaymentAdjust.AdjustmentAmount, 2, MidpointRounding.AwayFromZero);
                result.AdjustmentId = claimInternalPaymentAdjust.ID;
                result.IsAdd = false;
                result.AdjustUser = new UserReference() { FirstName = claimInternalPaymentAdjust.AdjustUser.FirstName, LastName = claimInternalPaymentAdjust.AdjustUser.LastName };
            }
            return result;
        }

        public void EditInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult)
        {
            if (modalResult.SelectedItem.Id == 1)
            {
                using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    remittanceRepository.UpdateInternalPaymentAdjust(modalResult.AdjustmentId, modalResult, SecurityContextHelper.CurrentUser.Id);

                    remittanceRepository.AddInternalAdjustmentNote(new TSFClaimInternalAdjustmentNote()
                    {
                        TSFInternalAdjustmentId = modalResult.AdjustmentId,
                        Note = modalResult.InternalNote,
                        CreatedDate = DateTime.UtcNow,
                        UserId = SecurityContextHelper.CurrentUser.Id
                    });

                    transationScope.Complete();
                }
            }
        }

        public List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int ClaimInternalPaymentAdjustId)
        {
            List<TSFClaimInternalAdjustmentNote> internalNoteList = null;

            internalNoteList = remittanceRepository.LoadClaimInternalNotesForPayment(ClaimInternalPaymentAdjustId).OrderByDescending(s => s.CreatedDate).ToList();

            var list = new List<InternalNoteViewModel>();
            foreach (var internalNote in internalNoteList)
            {
                var user = internalNote.User;
                list.Add(new InternalNoteViewModel() { AddedOn = internalNote.CreatedDate, AddedBy = user.FirstName + " " + user.LastName, Note = internalNote.Note });
            }

            return list;
        }

        public InternalNoteViewModel AddNotesHandlerInternalAdjustment(int ClaimInternalPaymentAdjustId, string notes)
        {
            var note = new TSFClaimInternalAdjustmentNote()
            {
                TSFInternalAdjustmentId = ClaimInternalPaymentAdjustId,
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserId = SecurityContextHelper.CurrentUser.Id
            };

            remittanceRepository.AddInternalAdjustmentNote(note);

            return new InternalNoteViewModel() { Note = note.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = note.CreatedDate };
        }

        public List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse)
        {
            return remittanceRepository.ExportToExcelInternalAdjustmentNotes(ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);
        }


        #endregion


        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();
            var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID") ?? "TEST1";

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                var allClaims = remittanceRepository.GetAllTsfClaimsAsQueryable();
                var claims = allClaims.Where(c => !(c.InBatch ?? false) && c.PaymentType != null &&
                                                  ((c.TotalRemittancePayable - c.Penalties - c.Interest) > 0 || (c.Credit ?? 0) > 0 || (c.AdjustmentTotal ?? 0) != 0) &&
                                                  string.Compare(c.RecordState, "Approved", StringComparison.OrdinalIgnoreCase) == 0).ToList();
                var minPostDate =
                            gpRepository.GetAllGpFiscalYears()
                                .Single(f => f.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= f.EffectiveEndDate.Date)
                                .MinimumDocDate;
                var paymentTypes = claims.Select(p => p.PaymentType).Distinct();
                // STEP 1 ITERATE THROUGH tsf_claim_summary WHERE in_batch = 0 AND (total_remittance_payable - penalties - interest) > 0 AND record_state = 'F';
                if (!paymentTypes.Any())
                {
                    msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
                }
                else
                {
                    if (paymentTypes.Contains("C"))
                    {
                        var countC = claims.Count(c => c.PaymentType == "C");
                        //STEP 2 INSERT INTO gpibatch
                        var gpiBatch = CreateGpiBatch(countC);
                        gpRepository.AddGpBatch(gpiBatch);
                        msg.BatchIDs.Add(gpiBatch.ID);
                        // STEP 3 INSERT INTO gpibatch_entry
                        var claimsWithPaymentTypeC = claims.Where(c => c.PaymentType == "C").ToList();
                        CreateGpiBatchEntries(claimsWithPaymentTypeC, gpiBatch);
                        // STEP 4 INSERT INTO rr_ots_rm_cash_receipt                    
                        InsertRrOtsRmCashReceipt(gpiBatch, INTERID, minPostDate);
                        // STEP 5 INSERT INTO rr_ots_rm_transactions
                        InsertRrOtsRmTransaction(gpiBatch, INTERID, minPostDate);
                        // STEP 6 INSERT INTO rr_ots_rm_apply	
                        InsertRrOtsRmCashApply(gpiBatch, INTERID, minPostDate);
                        // STEP 7 ITERATE THROUGH tmp_rr_ots_rm_trans_distributions
                        var rrOtsRmTransDistributionsDto = gpRepository.GetRrOtsRmTransDistributionsForRemittance(gpiBatch.ID);
                        // STEP 8 INSERT INTO rr_ots_rm_trans_distributions
                        InsertRrOtsRmTransDistribution(rrOtsRmTransDistributionsDto, INTERID, gpiBatch, true);
                    }
                    if (paymentTypes.Contains("E") || paymentTypes.Contains("O"))
                    {
                        var countEO = claims.Count(c => c.PaymentType == "E" || c.PaymentType == "O");
                        //STEP 2 INSERT INTO gpibatch
                        var gpiBatch = CreateGpiBatch(countEO);
                        gpRepository.AddGpBatch(gpiBatch);
                        msg.BatchIDs.Add(gpiBatch.ID);
                        // STEP 3 INSERT INTO gpibatch_entry
                        var claimsWithPaymentTypeEO = claims.Where(c => c.PaymentType == "E" || c.PaymentType == "O").ToList();
                        CreateGpiBatchEntries(claimsWithPaymentTypeEO, gpiBatch);
                        // STEP 4 INSERT INTO rr_ots_rm_transactions
                        InsertRrOtsRmTransaction(gpiBatch, INTERID, minPostDate);
                        // STEP 5 ITERATE THROUGH tmp_rr_ots_rm_trans_distributions
                        var rrOtsRmTransDistributionsDto = gpRepository.GetRrOtsRmTransDistributionsForRemittance(gpiBatch.ID);
                        // STEP 6 INSERT INTO rr_ots_rm_trans_distributions
                        InsertRrOtsRmTransDistribution(rrOtsRmTransDistributionsDto, INTERID, gpiBatch, false);
                    }
                    // STEP 9/7 UPDATE tsf_claim_summary
                    foreach (var tsfClaim in claims)
                    {
                        tsfClaim.InBatch = true;
                        remittanceRepository.UpdateTsfClaim(tsfClaim);
                    }
                }
                transationScope.Complete();
            }
            return msg;
        }

        private GpiBatch CreateGpiBatch(int count)
        {
            var gpiBatch = new GpiBatch()
            {
                GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Steward),
                GpiBatchCount = count,
                GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                CreatedDate = DateTime.UtcNow,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
            };
            return gpiBatch;
        }

        private void CreateGpiBatchEntries(List<TSFClaim> claims, GpiBatch gpiBatch)
        {
            var gpiBatchEntries = new List<GpiBatchEntry>();

            foreach (var claim in claims)
            {
                var gpiBatchEntry = new GpiBatchEntry()
                {
                    GpiBatchID = gpiBatch.ID,
                    GpiTxnNumber =
                        string.Format("{0}-{1:yyyy-MM-dd}-{2}", claim.Customer.RegistrationNumber,
                            claim.Period.EndDate, claim.ID),
                    //CONCAT(CAST(registration_number AS CHAR), '-', DATE_FORMAT(reporting_period_end, '%Y-%m-%d' ),'-',CAST(tsf_claim_summary_id AS CHAR)),
                    GpiBatchEntryDataKey = claim.ID.ToString(),
                    GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                };
                gpiBatchEntries.Add(gpiBatchEntry);
            }
            gpRepository.AddGpBatchEntries(gpiBatchEntries);
        }

        private void InsertRrOtsRmCashReceipt(GpiBatch gpiBatch, string INTERID, DateTime minPostDate)
        {
            var cashReceipts = new List<RrOtsRmCashReceipt>();
            var tmpCashReceipts = gpRepository.GetTsfClaimsCheckBookByGpBatchId(gpiBatch.ID);
            var firstOrDefault = gpRepository.GetAllGpCheckBooks().FirstOrDefault(c => c.is_default == 1);
            if (firstOrDefault != null)
            {
                var checkBookNameDefault = firstOrDefault.name;
                foreach (var cashReceipt in tmpCashReceipts)
                {
                    var tmpClaim = remittanceRepository.GetTSFClaimById(cashReceipt.ClaimId);
                    var row = new RrOtsRmCashReceipt()
                    {
                        BACHNUMB = gpiBatch.ID.ToString(),
                        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", tmpClaim.Customer.RegistrationNumber, tmpClaim.Period.EndDate, "1"),
                        CUSTNMBR = tmpClaim.Customer.RegistrationNumber,
                        RMDTYPAL = 9,
                        DOCDATE = ((tmpClaim.DepositDate ?? tmpClaim.ReceiptDate) == null || (tmpClaim.DepositDate ?? tmpClaim.ReceiptDate) < minPostDate)
                                ? minPostDate
                                : (tmpClaim.DepositDate ?? tmpClaim.ReceiptDate),
                        ORTRXAMT = tmpClaim.PaymentAmount.Value,
                        CSHRCTYP = 0,
                        CHEKNMBR = tmpClaim.ChequeReferenceNumber ?? "0",
                        CHEKBKID = "",
                        //Not required for successor
                        /*string.IsNullOrEmpty(tmpClaim.Account)
                                ? checkBookNameDefault
                                : cashReceipt.GpCheckBookName,*/
                        //(CASE WHEN S.account IS NULL THEN (SELECT name FROM gp_cheque_book g WHERE is_default = 1) ELSE gcb.name END) AS CHEKBKID,
                        CRCARDID = null,
                        CURNCYID = cashReceipt.CurrencyType ?? "CAD",//approved TSFClaim must have currency type after Dec. release
                        TRXDSCRN =
                            string.Format("{0}-{1:MM/yy}-{2}", tmpClaim.Customer.RegistrationNumber,
                                tmpClaim.Period.EndDate, "1"),
                        GLPOSTDT = ((tmpClaim.DepositDate ?? tmpClaim.ReceiptDate) == null || (tmpClaim.DepositDate ?? tmpClaim.ReceiptDate) < minPostDate)
                                ? minPostDate
                                : (tmpClaim.DepositDate ?? tmpClaim.ReceiptDate),
                        USERDEF1 = gpiBatch.ID.ToString(),
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = null,
                        gpibatchentry_id = cashReceipt.GpiBatchEntry.ID,
                    };
                    cashReceipts.Add(row);
                }
                gpRepository.AddGpRrOtsRmCashReceipts(cashReceipts);
            }
        }

        private void InsertRrOtsRmTransaction(GpiBatch gpiBatch, string INTERID, DateTime minPostDate)
        {
            var rrOtsRmTransactions = new List<RrOtsRmTransaction>();

            var tmpRrOtsRmTransactions = gpRepository.GetTsfClaimsRegistrantByGpBatchId(gpiBatch.ID);
            foreach (var transaction in tmpRrOtsRmTransactions)
            {
                var actions = new List<string>();

                if ((transaction.TotalRemittancePayable + transaction.Penalties + transaction.Interest) > 0 || transaction.InternalPaymentAdjustment > 0)
                {
                    actions.Add("DEBIT");
                }

                if (transaction.Credit > 0 || transaction.InternalPaymentAdjustment < 0)
                {
                    actions.Add("CREDIT");
                }


                foreach (var action in actions)
                {
                    var suffix = string.Equals(action, "DEBIT") ? "1" : "7";
                    var row = new RrOtsRmTransaction()
                    {
                        RMDTYPAL = Int16.Parse(suffix),
                        RMDNUMWK = transaction.ClaimId.ToString(),
                        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", transaction.RegistrationNumber, transaction.Period.EndDate, suffix),
                        DOCDESCR = string.Format("{0}-{1:MM/yy}-{2}", transaction.RegistrationNumber, transaction.Period.EndDate, suffix),
                        DOCDATE = (((transaction.DepositDate ?? transaction.ReceiptDate)) == null || ((transaction.DepositDate ?? transaction.ReceiptDate) < minPostDate))
                                ? minPostDate
                                : (transaction.DepositDate ?? transaction.ReceiptDate),
                        BACHNUMB = transaction.GpiBatch.ID.ToString(),
                        CUSTNMBR = transaction.RegistrationNumber,
                        ADRSCODE = "PRIMARY",
                        CSTPONBR = null,
                        TAXSCHID = (transaction.ApplicableTaxesHst) == 0 && (transaction.IsTaxExempt ?? false)
                                ? "S-EXEMPT"
                                : "S-HST",
                        SLSAMNT = string.Equals(action, "DEBIT") ? (transaction.TotalRemittancePayable + transaction.Penalties + transaction.Interest + (transaction.InternalPaymentAdjustment > 0 ? transaction.InternalPaymentAdjustment : 0)) + transaction.ApplicableTaxesHst :
                                  (Math.Abs(transaction.Credit) + Math.Abs(transaction.InternalPaymentAdjustment < 0 ? transaction.InternalPaymentAdjustment : 0)),
                        TAXAMNT = 0, //transaction.ApplicableTaxesHst, //OTSTM2-495 keep it 0 as per Alessandra Canizares's instruction
                        DOCAMNT = string.Equals(action, "DEBIT") ? transaction.TotalTsfDue + (transaction.InternalPaymentAdjustment > 0 ? transaction.InternalPaymentAdjustment : 0) :
                                  (Math.Abs(transaction.Credit) + Math.Abs(transaction.InternalPaymentAdjustment < 0 ? transaction.InternalPaymentAdjustment : 0)),
                        PYMTRMID = "Net 30 days",
                        DUEDATE = null,
                        USERDEF1 = transaction.GpiBatch.ID.ToString(),
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = "",
                        gpibatchentry_id = transaction.GpiBatchEntry.ID
                    };
                    rrOtsRmTransactions.Add(row);
                }
            }
            gpRepository.AddGpRrOtsRmTransactions(rrOtsRmTransactions.OrderBy(i => i.gpibatchentry_id).ThenBy(j => j.DOCNUMBR).ToList());
        }

        private void InsertRrOtsRmCashApply(GpiBatch gpiBatch, string INTERID, DateTime minPostDate)
        {
            var rrOtsRmApplies = new List<RrOtsRmApply>();
            var tmpCashReceipts = gpRepository.GetTsfClaimsCheckBookByGpBatchId(gpiBatch.ID);
            var tmpRrOtsRmApply = tmpCashReceipts;
            foreach (var rmApply in tmpRrOtsRmApply)
            {
                var row = new RrOtsRmApply()
                {
                    APTODCNM = string.Format("{0}-{1:MM/yy}-{2}", rmApply.RegistrationNumber, rmApply.Period.EndDate, "1"),
                    APFRDCNM = string.Format("{0}-{1:MM/yy}-{2}", rmApply.RegistrationNumber, rmApply.Period.EndDate, "1"),
                    APPTOAMT = rmApply.PaymentAmount,
                    APFRDCTY = 9,
                    APTODCTY = 1,
                    APPLYDATE = ((rmApply.DepositDate ?? rmApply.ReceiptDate) == null || (rmApply.DepositDate ?? rmApply.ReceiptDate) < minPostDate)
                            ? minPostDate
                            : (rmApply.DepositDate ?? rmApply.ReceiptDate),
                    GLPOSTDT = ((rmApply.DepositDate ?? rmApply.ReceiptDate) == null || (rmApply.DepositDate ?? rmApply.ReceiptDate) < minPostDate)
                            ? minPostDate
                            : (rmApply.DepositDate ?? rmApply.ReceiptDate),
                    USERDEF1 = rmApply.GpiBatch.ID.ToString(),
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //DEX_ROW_ID = null
                };
                rrOtsRmApplies.Add(row);
            }
            gpRepository.AddGpRrOtsRmApplies(rrOtsRmApplies);
        }

        private void InsertRrOtsRmTransDistribution(List<GpRrOtsRmTransDistributionsDto> rrOtsRmTransDistributionsDto, string INTERID, GpiBatch gpiBatch, bool isPaymentTypeC)
        {
            var rrOtsRmTransDistributions = new List<RrOtsRmTransDistribution>();
            // STEP 8 INSERT INTO rr_ots_rm_trans_distributions
            var curDocNumbr = string.Empty;
            var seqNumbr = 0;
            foreach (var rrOtsRmTransDistribution in rrOtsRmTransDistributionsDto)
            {
                if (isPaymentTypeC)
                {
                    if (string.Equals(rrOtsRmTransDistribution.PAYMENTTYPE, "C", StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.Equals(curDocNumbr, rrOtsRmTransDistribution.DOCNUMBR, StringComparison.OrdinalIgnoreCase))
                        {
                            seqNumbr += 1;
                        }
                        else
                        {
                            curDocNumbr = rrOtsRmTransDistribution.DOCNUMBR;
                            seqNumbr = 1;
                        }
                    }
                }
                else
                {
                    if (string.Equals(rrOtsRmTransDistribution.PAYMENTTYPE, "E", StringComparison.OrdinalIgnoreCase) || string.Equals(rrOtsRmTransDistribution.PAYMENTTYPE, "O", StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.Equals(curDocNumbr, rrOtsRmTransDistribution.DOCNUMBR, StringComparison.OrdinalIgnoreCase))
                        {
                            seqNumbr += 1;
                        }
                        else
                        {
                            curDocNumbr = rrOtsRmTransDistribution.DOCNUMBR;
                            seqNumbr = 1;
                        }
                    }
                }

                var row = new RrOtsRmTransDistribution()
                {
                    RMDTYPAL = rrOtsRmTransDistribution.RMDTYPAL ?? 1,
                    DOCNUMBR = rrOtsRmTransDistribution.DOCNUMBR,
                    CUSTNMBR = rrOtsRmTransDistribution.CUSTNMBR,
                    SEQNUMBR = seqNumbr,
                    DISTTYPE = rrOtsRmTransDistribution.DISTTYPE,
                    DistRef = null,
                    ACTNUMST = rrOtsRmTransDistribution.ACTNUMST,
                    DEBITAMT = Math.Round(rrOtsRmTransDistribution.DEBITAMT, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = Math.Round(rrOtsRmTransDistribution.CRDTAMNT, 2, MidpointRounding.AwayFromZero),
                    USERDEF1 = gpiBatch.ID.ToString(),
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL AS DEX_ROW_ID;     
                };
                rrOtsRmTransDistributions.Add(row);
            }
            gpRepository.AddRrOtsRmTransDistributions(rrOtsRmTransDistributions);
        }

        public bool HasData(int TSFClaimId)
        {
            return remittanceRepository.HasData(TSFClaimId);
        }

        public bool RemoveTSFClaim(int tsfClaimId)
        {
            return remittanceRepository.RemoveTSFClaim(tsfClaimId);
        }
    }
}
