﻿using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Transactions;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.GP;
using SystemIO = System.IO;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.Framework.Logging;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.UI.Common.Helper;

namespace CL.TMS.StewardBLL
{
    public class StewardRegistrationBO
    {
        private IApplicationRepository applicationRepository;
        private IItemRepository itemRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;
        private string uploadRepositoryPath;

        public StewardRegistrationBO(IApplicationRepository applicationRepository, IItemRepository itemRepository, IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository)
        {
            this.applicationRepository = applicationRepository;
            this.itemRepository = itemRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
            this.uploadRepositoryPath = AppSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            applicationRepository.UpdateFormObject(id, formObject, updatedBy);
        }

        public StewardRegistrationModel GetFormObject(int id)
        {
            var application = applicationRepository.GetSingleApplication(id);
            if (application == null)
            {
                throw new KeyNotFoundException();
            }

            StewardRegistrationModel result = null;
            var applicationStatus = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            if (applicationStatus == ApplicationStatusEnum.Approved
                || applicationStatus == ApplicationStatusEnum.BankInformationSubmitted
                || applicationStatus == ApplicationStatusEnum.BankInformationApproved
                || applicationStatus == ApplicationStatusEnum.Completed)
            {
                var customer = vendorRepository.GetSingleCustomeWithApplicatioId(id);
                result = GetApprovedApplication(customer.ID);
                result.ID = id;
                result.VendorID = customer.ID;
            }
            else
            {
                string formObject = application.FormObject;
                if (formObject != null)
                {
                    result = JsonConvert.DeserializeObject<StewardRegistrationModel>(formObject);
                }
                else
                {
                    result = new StewardRegistrationModel(id);
                }
            }
            if (application.AssignToUser.HasValue)
            {
                result.AssignToUserId = application.AssignToUser.Value;
            }

            result.Status = (application.Status).ToEnum<ApplicationStatusEnum>(ApplicationStatusEnum.Open);
            result.SubmittedDate = application.SubmittedDate;
            result.ExpireDate = application.ExpireDate;

            //OTSTM2-973 TermsAndConditons
            result.TermsAndConditions.TermsAndConditionsID = application.TermsAndConditionsID;

            return result;
        }

        public StewardRegistrationModel GetByTokenId(Guid tokenId)
        {
            var application = applicationRepository.GetApplicationByTokenId(tokenId);
            return GetFormObject(application.ID);
        }

        public IList<Item> GetAllItems()
        {
            return itemRepository.GetAllItems();
        }

        public StewardRegistrationModel GetAllItemsList()
        {
            var result = new StewardRegistrationModel();
            var allTireItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Steward)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();

            var ListItemModel = new List<TireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new TireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new StewardTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public IList<BusinessActivity> GetBusinessActivities(int type)
        {
            var Activity = itemRepository.GetBusinessActivities(type);
            return Activity;
        }

        public StewardRegistrationModel GetAllItemsList(int participantType)
        {
            var result = new StewardRegistrationModel();
            var allTireItems = itemRepository.GetAllItems(participantType);
            var ListItemModel = new List<TireDetailsItemTypeModel>();
            foreach (var items in allTireItems)
            {
                ListItemModel.Add(new TireDetailsItemTypeModel() { ID = items.ID, Name = items.Name, ShortName = items.ShortName, isChecked = false });
            }
            result.TireDetails = new StewardTireDetailsRegistrationModel()
            {
                TireDetailsItemType = ListItemModel
            };
            return result;
        }

        public StewardRegistrationModel GetAllByFilter(int appId)
        {
            throw new NotImplementedException();
        }

        public StewardRegistrationModel ModelInitialization(StewardRegistrationModel model)
        {
            throw new NotImplementedException();
        }

        public void SetRemitanceStatus(int vendorId, int ID, string PropName, int userId)
        {
            vendorRepository.SetRemitanceStatus(vendorId, ID, PropName, userId);
        }

        public Application GetSingleApplication(int appId)
        {
            return applicationRepository.GetSingleApplication(appId);
        }

        public Application GetApplicationById(int appId)
        {
            var result = applicationRepository.GetSingleApplication(appId);
            return result;
        }

        public void UpdateUserExistsinApplication(string formObjectData, int appId, string updatedBy)
        {
            Application application = new Application() { FormObject = formObjectData, ID = appId, AgreementAcceptedCheck = true };
            if (applicationRepository.GetSingleApplication(appId) != null)
            {
                applicationRepository.UpdateApplication(application);
            }
        }

        public void RemoveEmptyContacts(ref IList<ContactRegistrationModel> contacts, ref IList<string> invalidFieldList)
        {
            if (contacts != null && contacts.Count() > 1)
            {
                List<ContactRegistrationModel> results = new List<ContactRegistrationModel>() { contacts[0] };
                IList<string> resultsFieldList = invalidFieldList.ToList();

                int totalContacts = contacts.Count();
                for (int i = 1; i < totalContacts; i++)
                {
                    bool remove = false;
                    if (contacts[i].ContactInformation != null)
                    {
                        var contact = contacts[i].ContactInformation;
                        remove = string.IsNullOrWhiteSpace(contact.FirstName) &&
                                  string.IsNullOrWhiteSpace(contact.LastName) &&
                                  string.IsNullOrWhiteSpace(contact.Position) &&
                                  string.IsNullOrWhiteSpace(contact.PhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Ext) &&
                                  string.IsNullOrWhiteSpace(contact.AlternatePhoneNumber) &&
                                  string.IsNullOrWhiteSpace(contact.Email);
                        if (remove)
                        {
                            //check if contact address has any information
                            if (!contacts[i].ContactAddressSameAsBusinessAddress)
                            {
                                if (contacts[i].ContactAddress != null)
                                {
                                    var address = contacts[i].ContactAddress;
                                    remove = !contact.ContactAddressSameAsBusiness &&
                                            string.IsNullOrWhiteSpace(address.AddressLine1) &&
                                            string.IsNullOrWhiteSpace(address.AddressLine2) &&
                                            string.IsNullOrWhiteSpace(address.City) &&
                                            string.IsNullOrWhiteSpace(address.Postal) &&
                                            string.IsNullOrWhiteSpace(address.Province) &&
                                            string.IsNullOrWhiteSpace(address.Country);
                                }
                            }
                        }
                    }
                    if (remove)
                    {
                        //remove items from invalidlist
                        foreach (string nameStr in invalidFieldList)
                        {
                            string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                            Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                resultsFieldList.Remove(nameStr);
                            }
                        }
                    }
                    else
                    {
                        results.Add(contacts[i]);
                        int numRemoved = i - (results.Count() - 1);//-1 because of primary contact that is added
                        if (numRemoved > 0)
                        {
                            //replace all the names in invalidlist
                            foreach (string nameStr in invalidFieldList)
                            {
                                string regex = string.Format(@"^(ContactInformation\[{0}\]).*$", i.ToString());
                                Match match = Regex.Match(nameStr, regex, RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    resultsFieldList.Remove(nameStr);
                                    string newName = nameStr.Replace((i).ToString(), (results.Count() - 1).ToString());
                                    resultsFieldList.Add(newName);
                                }
                            }
                        }
                    }
                }
                contacts = results;
                invalidFieldList = resultsFieldList;
            }
        }

        public void UpdateApplication(Application application)
        {
            applicationRepository.UpdateApplication(application);
        }

        public void SetStatus(int applicationId, ApplicationStatusEnum status, string denyReasons, long assignToUser = 0)
        {
            applicationRepository.SetStatus(applicationId, status, denyReasons, assignToUser);
        }

        public ApplicationEmailModel GetApprovedStewardApplicantInformation(int applicationId)
        {
            var application = this.applicationRepository.FindApplicationByAppID(applicationId);
            var customer = vendorRepository.GetSingleCustomeWithApplicatioId(applicationId);
            var customerAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == 1);

            Contact customerPrimaryContact = null;

            foreach (var address in customer.CustomerAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        customerPrimaryContact = contact;
                        break;
                    }
                }

                if (customerPrimaryContact != null)
                    break;
            }

            if (customerPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + customer.BusinessName);
            }

            var applicationHaulerApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = customerPrimaryContact.FirstName,
                PrimaryContactLastName = customerPrimaryContact.LastName,
                RegistrationNumber = customer.RegistrationNumber,
                BusinessName = customer.BusinessName,
                BusinessLegalName = customer.BusinessName,
                Address1 = customerAddress.Address1,
                City = customerAddress.City,
                ProvinceState = customerAddress.Province,
                PostalCode = customerAddress.PostalCode,
                Country = customerAddress.Country,
                Email = customerPrimaryContact.Email,
                ApplicationToken = this.applicationInvitationRepository.GetTokenByApplicationId(applicationId),
            };

            return applicationHaulerApplicationModel;
        }
      
        public int ApproveApplication(StewardRegistrationModel StewardModel, int appID)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;

            var app = this.applicationRepository.FindApplicationByAppID(appID);
            app.SigningAuthorityFirstName = StewardModel.TermsAndConditions.SigningAuthorityFirstName;
            app.SigningAuthorityLastName = StewardModel.TermsAndConditions.SigningAuthorityLastName;
            app.SigningAuthorityPosition = StewardModel.TermsAndConditions.SigningAuthorityPosition;
            app.FormCompletedByFirstName = StewardModel.TermsAndConditions.FormCompletedByFirstName;
            app.FormCompletedByLastName = StewardModel.TermsAndConditions.FormCompletedByLastName;
            app.FormCompletedByPhone = StewardModel.TermsAndConditions.FormCompletedByPhone;
            app.AgreementAcceptedByFullName = StewardModel.TermsAndConditions.AgreementAcceptedByFullName;
            app.TermsAndConditionsID = StewardModel.TermsAndConditions.TermsAndConditionsID;
            app.Status = ApplicationStatusEnum.Approved.ToString();
            app.FormObject = app.FormObject;

            var applicationSupportingDocumentCHQ = new ApplicationSupportingDocument()
            {
                Option = StewardModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            app.ApplicationSupportingDocuments.Add(applicationSupportingDocumentCHQ);

            var customer = new Customer
            {
                BusinessName = StewardModel.BusinessLocation.BusinessName,
                OperatingName = StewardModel.BusinessLocation.OperatingName,
                BusinessStartDate = StewardModel.StewardDetails.BusinessStartDate,
                BusinessNumber = StewardModel.StewardDetails.BusinessNumber,
                CommercialLiabHSTNumber = StewardModel.StewardDetails.CommercialLiabHstNumber,
                IsTaxExempt = StewardModel.StewardDetails.IsTaxExempt,
                CommercialLiabInsurerName = StewardModel.StewardDetails.CommercialLiabInsurerName,
                CommercialLiabInsurerExpDate = StewardModel.StewardDetails.CommercialLiabInsurerExpDate,
                HasMoreThanOneEmp = StewardModel.StewardDetails.HasMoreThanOneEmp,
                WSIBNumber = StewardModel.StewardDetails.WsibNumber,
                PayOTSFee = StewardModel.StewardDetails.HasFeeToALLVendor.HasValue ? StewardModel.StewardDetails.HasFeeToALLVendor.Value : false,
                ApplicationID = app.ID,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedBy = updatedBy,
                LastUpdatedDate = DateTime.UtcNow,
                RegistrantSubTypeID = StewardModel.StewardDetails.RegistrantSubTypeID,
                BusinessDesc = StewardModel.StewardDetails.SBusinessDesc,
                MOESwitchDate = DateTime.UtcNow,//StewardModel.SMOESwitchDate,
                //OTSTM2-1110 comment out
                //Audit = Convert.ToBoolean(SAudit.Off),
                //AuditSwitchDate = DateTime.UtcNow,//StewardModel.SAuditSwitchDate,
                RemitsPerYear = Convert.ToBoolean(SRemitsPerYear.SRemitsPerYear12x),
                RemitsPerYearSwitchDate = DateTime.UtcNow,//StewardModel.SRemitsPerYearSwitchDate,
                MOE = Convert.ToBoolean(SMOE.Off),
                ActiveStateChangeDate = DateTime.Now.Date,
                ActivationDate = DateTime.Now.Date,
                IsActive = true,
                ContactInfo = string.Format("{0} {1}, {2}", StewardModel.ContactInformationList[0].ContactInformation.FirstName,
                                StewardModel.ContactInformationList[0].ContactInformation.LastName, StewardModel.ContactInformationList[0].ContactInformation.PhoneNumber),

                FranchiseName = string.Empty,
                UpdateToFinancialSoftware = updatedBy,
                PrimaryBusinessActivity = StewardModel.StewardDetails.BusinessActivity,
                BusinessActivityID = (int)BusinessActivityTypeEnum.Steaward,
                CustomerType = 1,

                RegistrantStatus = ApplicationStatusEnum.Approved.ToString(),
                SigningAuthorityFirstName = StewardModel.TermsAndConditions.SigningAuthorityFirstName,
                SigningAuthorityLastName = StewardModel.TermsAndConditions.SigningAuthorityLastName,
                SigningAuthorityPosition = StewardModel.TermsAndConditions.SigningAuthorityPosition,
                FormCompletedByFirstName = StewardModel.TermsAndConditions.FormCompletedByFirstName,
                FormCompletedByLastName = StewardModel.TermsAndConditions.FormCompletedByLastName,
                FormCompletedByPhone = StewardModel.TermsAndConditions.FormCompletedByPhone,
                AgreementAcceptedByFullName = StewardModel.TermsAndConditions.AgreementAcceptedByFullName,
                AgreementAcceptedCheck = StewardModel.TermsAndConditions.AgreementAcceptedCheck,
            };

            var customerSupportingDocumentCHQ = new CustomerSupportingDocument()
            {
                Option = StewardModel.SupportingDocuments.SupportingDocumentsOptionCHQ,
                TypeID = (int)SupportingDocumentTypeEnum.CHQ
            };
            customer.CustomerSupportingDocuments.Add(customerSupportingDocumentCHQ);


            var customerPrimaryAddress = new Address()
            {
                Address1 = StewardModel.BusinessLocation.BusinessAddress.AddressLine1,
                Address2 = StewardModel.BusinessLocation.BusinessAddress.AddressLine2,
                City = StewardModel.BusinessLocation.BusinessAddress.City,
                Province = StewardModel.BusinessLocation.BusinessAddress.Province,
                PostalCode = StewardModel.BusinessLocation.BusinessAddress.Postal,
                Country = StewardModel.BusinessLocation.BusinessAddress.Country,
                Phone = StewardModel.BusinessLocation.Phone,
                Email = StewardModel.BusinessLocation.Email,
                Ext = StewardModel.BusinessLocation.Extension,
                AddressType = (int)AddressTypeEnum.Business,
            };

            customer.CustomerAddresses.Add(customerPrimaryAddress);

            if (!(StewardModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                (StewardModel.BusinessLocation.MailingAddress != null) &&
                (!string.IsNullOrWhiteSpace(StewardModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var customerMailingAddress = new Address
                {
                    Address1 = StewardModel.BusinessLocation.MailingAddress.AddressLine1,
                    Address2 = StewardModel.BusinessLocation.MailingAddress.AddressLine2,
                    City = StewardModel.BusinessLocation.MailingAddress.City,
                    Province = StewardModel.BusinessLocation.MailingAddress.Province,
                    PostalCode = StewardModel.BusinessLocation.MailingAddress.Postal,
                    Country = StewardModel.BusinessLocation.MailingAddress.Country,
                    Phone = StewardModel.BusinessLocation.Phone,
                    Email = StewardModel.BusinessLocation.Email,
                    AddressType = (int)AddressTypeEnum.Mail,
                };

                customer.CustomerAddresses.Add(customerMailingAddress);
            }

            var firstVendorAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == 1);
            if (firstVendorAddress != null)
            {
                for (var i = 0; i < StewardModel.ContactInformationList.Count; i++)
                {
                    if (StewardModel.ContactInformationList[i].ContactInformation.FirstName != null &&
                        StewardModel.ContactInformationList[i].ContactInformation.LastName != null)
                    {
                        var contact = new Contact()
                        {
                            FirstName = StewardModel.ContactInformationList[i].ContactInformation.FirstName,
                            LastName = StewardModel.ContactInformationList[i].ContactInformation.LastName,
                            Position = StewardModel.ContactInformationList[i].ContactInformation.Position,
                            PhoneNumber =
                                StewardModel.ContactInformationList[i].ContactInformation.PhoneNumber,
                            Ext = StewardModel.ContactInformationList[i].ContactInformation.Ext,
                            AlternatePhoneNumber =
                                StewardModel.ContactInformationList[i].ContactInformation.AlternatePhoneNumber,
                            Email = StewardModel.ContactInformationList[i].ContactInformation.Email,
                            IsPrimary = i == 0,
                        };

                        if (!StewardModel.ContactInformationList[i].ContactAddressSameAsBusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(StewardModel.ContactInformationList[i].ContactAddress.AddressLine1))
                            {
                                var contactAddress = new Address()
                                {
                                    Address1 =
                                        StewardModel.ContactInformationList[i].ContactAddress.AddressLine1,
                                    Address2 =
                                        StewardModel.ContactInformationList[i].ContactAddress.AddressLine2,
                                    City = StewardModel.ContactInformationList[i].ContactAddress.City,
                                    Province = StewardModel.ContactInformationList[i].ContactAddress.Province,
                                    PostalCode = StewardModel.ContactInformationList[i].ContactAddress.Postal,
                                    Country = string.IsNullOrWhiteSpace(StewardModel.ContactInformationList[i].ContactAddress.Country) ? "Canada" : StewardModel.ContactInformationList[i].ContactAddress.Country,
                                    AddressType = (int)AddressTypeEnum.Contact,
                                };
                                contactAddress.Contacts.Add(contact);
                                customer.CustomerAddresses.Add(contactAddress);
                            }
                            else
                            {
                                firstVendorAddress.Contacts.Add(contact);
                            }
                        }
                        else
                        {
                            firstVendorAddress.Contacts.Add(contact);
                        }
                    }
                }

            }

            //customeractivehistory
            var customerActiveHistory = new CustomerActiveHistory()
            {
                ActiveState = true,
                ActiveStateChangeDate = DateTime.Now.Date,
                CustomerID = customer.ID,
                CreateDate = DateTime.UtcNow,
                IsValid = true
            };

            var currentUser = TMSContext.TMSPrincipal.Identity;
            customerActiveHistory.CreatedBy = currentUser.Name;

            customer.CustomerActiveHistories.Add(customerActiveHistory);

            //OTSTM2-21
            var setting12x = new CustomerSettingHistory()
            {
                CustomerId = customer.ID,
                ColumnName = "RemitsPerYear",
                OldValue = "12x",
                NewValue = "12x",
                ChangeBy = (int)AppSettings.Instance.SystemUser.ID,
                ChangeDate = customer.CreatedDate
            };
            customer.CustomerSettingHistoryList.Add(setting12x);

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Steward)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            StewardModel.TireDetails.TireDetailsItemType.ToList().ForEach(s =>
            {
                if (s.isChecked)
                {
                    var result = defaultItems.Single(p => p.ID == s.ID);
                    if (result != null)
                    {
                        customer.Items.Add(result);
                    }
                }
            });

            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                applicationRepository.UpdateApplicationEntity(app);
                customer.RegistrationNumber = vendorRepository.GetCustomerRegistrationNumber().ToString();         //OTSTM2-508
                vendorRepository.AddCustomer(customer);

                //Add CustomerAttachments
                var customerAttachments = new List<CustomerAttachment>();
                app.Attachments.ToList().ForEach(c =>
                {
                    var customerAttachment = new CustomerAttachment();
                    customerAttachment.AttachmentId = c.ID;
                    customerAttachment.CustomerId = customer.ID;
                    customerAttachments.Add(customerAttachment);
                });
                vendorRepository.UpdateCustomerKeywords(customer);
                vendorRepository.AddCustomerAttachments(customerAttachments);

                //Update applicationnote
                applicationRepository.UpdateApplicatioNoteWithCustomerId(customer.ID, app.ID);

                transationScope.Complete();
            }

            return customer.ID;
        }

        public int UpdateApproveApplication(StewardRegistrationModel stewardModel, int customerId)
        {
            var user = TMSContext.TMSPrincipal.Identity;
            var updatedBy = user.Name;
            var deleteContacts = new List<Contact>(); //OTSTM2-50
            var deleteAddresses = new List<Address>();
            var newContactAddresses = new List<ContactAddressDto>();

            var customer = vendorRepository.GetSingleCustomerByID(customerId);
            if (customer != null)
            {
                if (!(customer.GpUpdate ?? false))
                {
                    var participantExisting = new GpParticipantUpdate(customer);
                    var newContact = stewardModel.ContactInformationList.FirstOrDefault();
                    var participantNew = new GpParticipantUpdate()
                    {
                        MailingAddress1 = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.AddressLine1 : stewardModel.BusinessLocation.MailingAddress.AddressLine1,
                        MailingAddress2 = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.AddressLine2 : stewardModel.BusinessLocation.MailingAddress.AddressLine2,
                        MailingCity = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.City : stewardModel.BusinessLocation.MailingAddress.City,
                        MailingCountry = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.Country : stewardModel.BusinessLocation.MailingAddress.Country,
                        MailingPostalCode = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.Postal : stewardModel.BusinessLocation.MailingAddress.Postal,
                        MailingProvince = stewardModel.BusinessLocation.MailingAddressSameAsBusiness ? stewardModel.BusinessLocation.BusinessAddress.Province : stewardModel.BusinessLocation.MailingAddress.Province,
                        PrimaryContactName = string.Format("{0} {1}", newContact.ContactInformation.FirstName, newContact.ContactInformation.LastName),
                        PrimaryContactPhone = newContact.ContactInformation.PhoneNumber ?? string.Empty,
                        IsTaxExempt = stewardModel.StewardDetails.IsTaxExempt,
                        IsActive = customer.IsActive
                    };
                    if (!participantExisting.Equals(participantNew))
                    {
                        customer.GpUpdate = true;
                    }
                }
                customer.BusinessName = stewardModel.BusinessLocation.BusinessName;
                customer.OperatingName = stewardModel.BusinessLocation.OperatingName;
                customer.BusinessStartDate = stewardModel.StewardDetails.BusinessStartDate;
                customer.BusinessNumber = stewardModel.StewardDetails.BusinessNumber;
                customer.CommercialLiabHSTNumber = stewardModel.StewardDetails.CommercialLiabHstNumber;
                customer.IsTaxExempt = stewardModel.StewardDetails.IsTaxExempt;
                customer.CommercialLiabInsurerName = stewardModel.StewardDetails.CommercialLiabInsurerName;
                customer.CommercialLiabInsurerExpDate = stewardModel.StewardDetails.CommercialLiabInsurerExpDate;
                customer.HasMoreThanOneEmp = stewardModel.StewardDetails.HasMoreThanOneEmp;
                customer.WSIBNumber = stewardModel.StewardDetails.WsibNumber;
                customer.PayOTSFee = stewardModel.StewardDetails.HasFeeToALLVendor.HasValue ? stewardModel.StewardDetails.HasFeeToALLVendor.Value : false;
                customer.LastUpdatedBy = updatedBy;
                customer.RegistrantSubTypeID = stewardModel.StewardDetails.RegistrantSubTypeID;
                customer.BusinessDesc = stewardModel.StewardDetails.SBusinessDesc;

                //separate calls exist for these
                customer.FranchiseName = stewardModel.BusinessLocation.BusinessName;
                customer.UpdateToFinancialSoftware = updatedBy;
                customer.LastUpdatedDate = DateTime.UtcNow;
                customer.PrimaryBusinessActivity = stewardModel.StewardDetails.BusinessActivity;
                customer.BusinessActivityID = (int)BusinessActivityTypeEnum.Steaward;
                customer.CustomerType = 1;

                customer.ContactInfo = string.Format("{0} {1}, {2}", stewardModel.ContactInformationList[0].ContactInformation.FirstName,
                                stewardModel.ContactInformationList[0].ContactInformation.LastName, stewardModel.ContactInformationList[0].ContactInformation.PhoneNumber);

                customer.SigningAuthorityFirstName = stewardModel.TermsAndConditions.SigningAuthorityFirstName;
                customer.SigningAuthorityLastName = stewardModel.TermsAndConditions.SigningAuthorityLastName;
                customer.SigningAuthorityPosition = stewardModel.TermsAndConditions.SigningAuthorityPosition;
                customer.FormCompletedByFirstName = stewardModel.TermsAndConditions.FormCompletedByFirstName;
                customer.FormCompletedByLastName = stewardModel.TermsAndConditions.FormCompletedByLastName;
                customer.FormCompletedByPhone = stewardModel.TermsAndConditions.FormCompletedByPhone;
                customer.AgreementAcceptedByFullName = stewardModel.TermsAndConditions.AgreementAcceptedByFullName;
                customer.AgreementAcceptedCheck = stewardModel.TermsAndConditions.AgreementAcceptedCheck;

                foreach (var supportDocOption in customer.CustomerSupportingDocuments)
                {
                    if (supportDocOption.TypeID == (int)SupportingDocumentTypeEnum.CHQ)
                    {
                        supportDocOption.Option = stewardModel.SupportingDocuments.SupportingDocumentsOptionCHQ;
                    }
                }
            };

            var customerAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (customerAddress != null)
            {
                customerAddress.Address1 = stewardModel.BusinessLocation.BusinessAddress.AddressLine1;
                customerAddress.Address2 = stewardModel.BusinessLocation.BusinessAddress.AddressLine2;
                customerAddress.City = stewardModel.BusinessLocation.BusinessAddress.City;
                customerAddress.Province = stewardModel.BusinessLocation.BusinessAddress.Province;
                customerAddress.PostalCode = stewardModel.BusinessLocation.BusinessAddress.Postal;
                customerAddress.Country = stewardModel.BusinessLocation.BusinessAddress.Country;
                customerAddress.Phone = stewardModel.BusinessLocation.Phone;
                customerAddress.Email = stewardModel.BusinessLocation.Email;
                customerAddress.AddressType = (int)AddressTypeEnum.Business;
                customerAddress.Ext = stewardModel.BusinessLocation.Extension;
            }

            if (!(stewardModel.BusinessLocation.MailingAddressSameAsBusiness) &&
                    (stewardModel.BusinessLocation.MailingAddress != null) &&
                    (!string.IsNullOrWhiteSpace(stewardModel.BusinessLocation.MailingAddress.AddressLine1)))
            {
                var vendorMailingAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);

                if (vendorMailingAddress != null)
                {
                    vendorMailingAddress.Address1 = stewardModel.BusinessLocation.MailingAddress.AddressLine1;
                    vendorMailingAddress.Address2 = stewardModel.BusinessLocation.MailingAddress.AddressLine2;
                    vendorMailingAddress.City = stewardModel.BusinessLocation.MailingAddress.City;
                    vendorMailingAddress.Province = stewardModel.BusinessLocation.MailingAddress.Province;
                    vendorMailingAddress.PostalCode = stewardModel.BusinessLocation.MailingAddress.Postal;
                    vendorMailingAddress.Country = stewardModel.BusinessLocation.MailingAddress.Country;
                    vendorMailingAddress.Phone = stewardModel.BusinessLocation.Phone;
                    vendorMailingAddress.Email = stewardModel.BusinessLocation.Email;
                }
                else
                {
                    var mailingAddress = new Address()
                    {
                        Address1 = stewardModel.BusinessLocation.MailingAddress.AddressLine1,
                        Address2 = stewardModel.BusinessLocation.MailingAddress.AddressLine2,
                        City = stewardModel.BusinessLocation.MailingAddress.City,
                        Province = stewardModel.BusinessLocation.MailingAddress.Province,
                        PostalCode = stewardModel.BusinessLocation.MailingAddress.Postal,
                        Country = stewardModel.BusinessLocation.MailingAddress.Country,
                        Phone = stewardModel.BusinessLocation.Phone,
                        Email = stewardModel.BusinessLocation.Email,
                        AddressType = (int)AddressTypeEnum.Mail
                    };

                    customer.CustomerAddresses.Add(mailingAddress);
                }
            }

            else
            {
                //OTSTM2-476
                var currentMailingAddress = customer.CustomerAddresses.FirstOrDefault(c => c.AddressType == (int)AddressTypeEnum.Mail);

                if (currentMailingAddress != null)
                {
                    customer.CustomerAddresses.Remove(currentMailingAddress);
                }
            }

            //OTSTM2-50 remove contact
            var contactIdList = stewardModel.ContactInformationList.Select(c => c.ContactInformation.ID).ToList();
            foreach (var contactAddress in customer.CustomerAddresses.ToList())
            {
                foreach (var contact in contactAddress.Contacts.ToList())
                {
                    if (!contactIdList.Contains(contact.ID))
                    {
                        deleteContacts.Add(contact);
                    }
                }
            }

            foreach (var applicationContactInfo in stewardModel.ContactInformationList)
            {
                Contact contact = null;
                if (applicationContactInfo.isEmptyContact)
                {
                    var contactAddress = customer.CustomerAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                    contact = contactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                    contactAddress.Contacts.Remove(contact);
                    customer.CustomerAddresses.Remove(contactAddress);
                }
                else
                {
                    #region contact
                    //check if this is a new contact and address
                    if (applicationContactInfo.ContactInformation.AddressID == 0)
                    {
                        contact = new Contact() { IsPrimary = false };
                        ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        if (!applicationContactInfo.ContactInformation.ContactAddressSameAsBusiness)
                        {
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contactAddress.Contacts.Add(contact);
                            customer.CustomerAddresses.Add(contactAddress);
                        }
                        else
                        {
                            var customerBusinessAddress = customer.CustomerAddresses.FirstOrDefault(r => r.ID == stewardModel.BusinessLocation.BusinessAddress.ID);
                            customerBusinessAddress.Contacts.Add(contact);
                            vendorRepository.UpdateCustomer(customer); //OTSTM2-1037 make sure contact 1 with same as Business Address checked to be saved earlier, so that has a smaller ID value
                        }
                    }
                    else
                    {

                        var currentContactAddress = customer.CustomerAddresses.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.AddressID);
                        var businessAddress = customer.CustomerAddresses.FirstOrDefault(r => r.ID == stewardModel.BusinessLocation.BusinessAddress.ID);
                        var oldIsAddressSameAsBusiness = currentContactAddress.AddressType == (int)AddressTypeEnum.Business;
                        var newIsAddressSameAsBusiness = applicationContactInfo.ContactAddressSameAsBusinessAddress;
                        if (newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //no change update contact information
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                        else if (newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //changed selection to same as business
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            contact.AddressID = businessAddress.ID;
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = businessAddress
                            });
                            deleteAddresses.Add(contact.Address);
                        }
                        else if (!newIsAddressSameAsBusiness && oldIsAddressSameAsBusiness)
                        {
                            //remove from business address and add new address
                            var contactAddress = new Address();
                            ModelAdapter.UpdateAddress(ref contactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                            newContactAddresses.Add(new ContactAddressDto()
                            {
                                Contact = contact,
                                Address = contactAddress
                            });
                        }
                        else if (!newIsAddressSameAsBusiness && !oldIsAddressSameAsBusiness)
                        {
                            //no change update address and contact information
                            ModelAdapter.UpdateAddress(ref currentContactAddress, applicationContactInfo);
                            contact = currentContactAddress.Contacts.FirstOrDefault(r => r.ID == applicationContactInfo.ContactInformation.ID);
                            ModelAdapter.UpdateContact(ref contact, applicationContactInfo);
                        }
                    }
                    #endregion
                }
            }

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Steward)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            foreach (var item in stewardModel.TireDetails.TireDetailsItemType)
            {
                if (item.isChecked)
                {
                    if (!customer.Items.Any(r => r.ID == item.ID))
                    {
                        var result = defaultItems.Single(s => s.ID == item.ID);
                        if (result != null)
                        {
                            customer.Items.Add(result);
                        }
                    }
                }
                else
                {
                    var result = customer.Items.FirstOrDefault(r => r.ID == item.ID);
                    if (result != null)
                    {
                        customer.Items.Remove(result);
                    }
                }
            }
            //update the related tables via transaction
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                vendorRepository.RemoveAddresses(deleteAddresses);
                vendorRepository.RemoveContacts(deleteContacts); //OTSTM2-50
                vendorRepository.UpdateCustomerContacts(newContactAddresses, customer.ID);
                vendorRepository.UpdateCustomer(customer);
                vendorRepository.UpdateCustomerKeywords(customer);
                transationScope.Complete();
            }

            return customer.ID;
        }

        //Send Back To application to Steward
        public void SendEmailForBackToApplicantSteward(int ApplicationID, ApplicationEmailModel emailModel)
        {
            #region

            string message = string.Empty;

            Guid invitationToken = this.applicationInvitationRepository.GetTokenByApplicationId(ApplicationID);
            emailModel.InvitationToken = invitationToken;

            string viewApplication = string.Format("{0}{1}{2}", AppSettings.Instance.GetSettingValue("Settings.DomainName"), "/Steward/Registration/index/", emailModel.InvitationToken);

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Email.BackToApplicantEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationBacktoApplicant");
            string subject = EmailContentHelper.GetSubjectByName("NewApplicationBacktoApplicant");

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ApplicationType, "Steward")
                .Replace(HaulerApplicationEmailTemplPlaceHolders.ViewApplication, viewApplication)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, emailModel.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            bool bSendBCCEmail = AppSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

            Email.Email email = new Email.Email(Convert.ToInt32(AppSettings.Instance.GetSettingValue("Email.smtpPort")),
                AppSettings.Instance.GetSettingValue("Email.smtpServer"),
                AppSettings.Instance.GetSettingValue("Email.smtpUserName"),
                AppSettings.Instance.GetSettingValue("Email.smtpPassword"),
                AppSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                AppSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                AppSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                AppSettings.Instance.GetSettingValue("Company.Email"),
                Boolean.Parse(AppSettings.Instance.GetSettingValue("Email.useSSL")));

            //OTSTM2-132 BCC value update
            email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailModel.Email, null, bSendBCCEmail ? AppSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

            message = string.Format("Steward application deny email was successfully sent to {0} at email {1}", emailModel.BusinessLegalName, emailModel.Email);

            #endregion
        }

        public StewardRegistrationModel GetApprovedApplication(int customerId)
        {
            StewardRegistrationModel stewardModel = new StewardRegistrationModel();

            var customer = vendorRepository.GetSingleCustomerByID(customerId);
            if (customer != null)
            {
                stewardModel.Status = customer.RegistrantStatus == null ? ApplicationStatusEnum.Approved :
                    (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), customer.RegistrantStatus);

                stewardModel.ActiveInactive.ApprovedDate = customer.CreatedDate;
                //stewardModel.RemitanceDetail.SAudit = customer.Audit; //OTSTM2-1110 comment out
                stewardModel.RemitanceDetail.SRemitsPerYear = customer.RemitsPerYear;
                stewardModel.RemitanceDetail.SMOE = customer.MOE;
                //stewardModel.RemitanceDetail.SAuditSwitchDate = customer.AuditSwitchDate; //OTSTM2-1110 comment out
                stewardModel.RemitanceDetail.SRemitsPerYearSwitchDate = customer.RemitsPerYearSwitchDate;
                stewardModel.RemitanceDetail.SMOESwitchDate = customer.MOESwitchDate;

                if (customer.ApplicationID.HasValue)
                {
                    var app = this.applicationRepository.FindApplicationByAppID(customer.ApplicationID.Value);
                    stewardModel.ID = app.ID;
                    stewardModel.UserIPAddress = app.UserIP;
                    stewardModel.SubmittedDate = app.SubmittedDate;
                    //OTSTM2-973
                    stewardModel.TermsAndConditions.TermsAndConditionsID = app.TermsAndConditionsID;
                }

                stewardModel.BusinessLocation.BusinessName = customer.BusinessName;
                stewardModel.BusinessLocation.OperatingName = customer.OperatingName;
                stewardModel.StewardDetails.BusinessStartDate = customer.BusinessStartDate;
                stewardModel.StewardDetails.BusinessNumber = customer.BusinessNumber;
                stewardModel.StewardDetails.CommercialLiabHstNumber = customer.CommercialLiabHSTNumber;
                stewardModel.StewardDetails.IsTaxExempt = customer.IsTaxExempt;
                stewardModel.StewardDetails.CommercialLiabInsurerName = customer.CommercialLiabInsurerName;
                stewardModel.StewardDetails.CommercialLiabInsurerExpDate = customer.CommercialLiabInsurerExpDate;
                stewardModel.StewardDetails.HasMoreThanOneEmp = customer.HasMoreThanOneEmp;
                stewardModel.StewardDetails.WsibNumber = customer.WSIBNumber;
                stewardModel.RegistrationNumber = Convert.ToInt32(customer.RegistrationNumber);
                stewardModel.StewardDetails.RegistrantSubTypeID = customer.RegistrantSubTypeID;
                stewardModel.StewardDetails.SBusinessDesc = customer.BusinessDesc;
                stewardModel.SMOESwitchDate = customer.MOESwitchDate;
                stewardModel.StewardDetails.HasFeeToALLVendor = customer.PayOTSFee.HasValue ? customer.PayOTSFee.Value : false;
                //OTSTM2-1110 comment out
                //stewardModel.SAudits = (customer.Audit.HasValue && customer.Audit.Value ? SAudit.On : SAudit.Off);
                //stewardModel.SAuditSwitchDate = customer.AuditSwitchDate;
                stewardModel.SRemitsPerYears = (customer.RemitsPerYear.HasValue && customer.RemitsPerYear.Value ? SRemitsPerYear.SRemitsPerYear2x : SRemitsPerYear.SRemitsPerYear12x);
                stewardModel.SRemitsPerYearSwitchDate = customer.RemitsPerYearSwitchDate;
                stewardModel.SMOES = (customer.MOE.HasValue && customer.MOE.Value ? SMOE.On : SMOE.Off);
                stewardModel.StewardDetails.BusinessActivity = customer.PrimaryBusinessActivity.Trim();

                stewardModel.TermsAndConditions.SigningAuthorityFirstName = customer.SigningAuthorityFirstName;
                stewardModel.TermsAndConditions.SigningAuthorityLastName = customer.SigningAuthorityLastName;
                stewardModel.TermsAndConditions.SigningAuthorityPosition = customer.SigningAuthorityPosition;
                stewardModel.TermsAndConditions.FormCompletedByFirstName = customer.FormCompletedByFirstName;
                stewardModel.TermsAndConditions.FormCompletedByLastName = customer.FormCompletedByLastName;
                stewardModel.TermsAndConditions.FormCompletedByPhone = customer.FormCompletedByPhone;
                stewardModel.TermsAndConditions.AgreementAcceptedByFullName = customer.AgreementAcceptedByFullName;
                stewardModel.TermsAndConditions.AgreementAcceptedCheck = customer.AgreementAcceptedCheck ?? true;
                stewardModel.CreatedDate = (null == customer.ConfirmationMailedDate) ? (customer.CreatedDate) : ((customer.ConfirmationMailedDate < (new DateTime(2009, 7, 1))) ? (customer.ActivationDate ?? DateTime.MinValue) : (customer.ConfirmationMailedDate ?? DateTime.MinValue));

            }

            var vendorPrimaryAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Business);
            if (vendorPrimaryAddress != null)
            {
                stewardModel.BusinessLocation.BusinessAddress.AddressLine1 = vendorPrimaryAddress.Address1;
                stewardModel.BusinessLocation.BusinessAddress.AddressLine2 = vendorPrimaryAddress.Address2;
                stewardModel.BusinessLocation.BusinessAddress.City = vendorPrimaryAddress.City;
                stewardModel.BusinessLocation.BusinessAddress.Province = vendorPrimaryAddress.Province;
                stewardModel.BusinessLocation.BusinessAddress.Postal = vendorPrimaryAddress.PostalCode;
                stewardModel.BusinessLocation.BusinessAddress.Country = vendorPrimaryAddress.Country;
                stewardModel.BusinessLocation.Phone = vendorPrimaryAddress.Phone;
                stewardModel.BusinessLocation.Email = vendorPrimaryAddress.Email;
                stewardModel.BusinessLocation.Extension = vendorPrimaryAddress.Ext;
                stewardModel.BusinessLocation.BusinessAddress.ID = vendorPrimaryAddress.ID;
            }

            var vendorMailingAddress = customer.CustomerAddresses.FirstOrDefault(r => r.AddressType == (int)AddressTypeEnum.Mail);
            if (vendorMailingAddress != null)
            {
                stewardModel.BusinessLocation.MailingAddress.AddressLine1 = vendorMailingAddress.Address1;
                stewardModel.BusinessLocation.MailingAddress.AddressLine2 = vendorMailingAddress.Address2;
                stewardModel.BusinessLocation.MailingAddress.City = vendorMailingAddress.City;
                stewardModel.BusinessLocation.MailingAddress.Province = vendorMailingAddress.Province;
                stewardModel.BusinessLocation.MailingAddress.Postal = vendorMailingAddress.PostalCode;
                stewardModel.BusinessLocation.MailingAddress.Country = vendorMailingAddress.Country;
                stewardModel.BusinessLocation.MailingAddress.ID = vendorMailingAddress.ID;
                stewardModel.BusinessLocation.MailingAddressSameAsBusiness = false;
            }
            else
            {
                stewardModel.BusinessLocation.MailingAddressSameAsBusiness = true;
            }

            foreach (var contactAddress in customer.CustomerAddresses)
            {
                foreach (var contact in contactAddress.Contacts)
                {
                    ContactRegistrationModel contactRegistrationModel = new ContactRegistrationModel()
                    {
                        ContactInformation = new ContactModel()
                        {
                            ID = contact.ID,
                            AddressID = contact.AddressID,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            Position = contact.Position,
                            PhoneNumber = contact.PhoneNumber,
                            Ext = contact.Ext,
                            AlternatePhoneNumber = contact.AlternatePhoneNumber,
                            Email = contact.Email,
                            IsPrimary = (contact.IsPrimary.HasValue ? contact.IsPrimary.Value : false)
                        },

                        ContactAddress = new ContactAddressModel()
                        {
                            AddressLine1 = contact.Address.Address1,
                            AddressLine2 = contact.Address.Address2,
                            City = contact.Address.City,
                            Province = contact.Address.Province,
                            Postal = contact.Address.PostalCode,
                            Country = contact.Address.Country,
                        },
                        ContactAddressSameAsBusinessAddress = (stewardModel.BusinessLocation.BusinessAddress.ID == contact.AddressID)
                    };

                    stewardModel.ContactInformationList.Add(contactRegistrationModel);
                }
            }

            stewardModel.ContactInformationList = stewardModel.ContactInformationList.OrderBy(c => c.ContactInformation.ID).ToList();

            stewardModel.TireDetails = new StewardTireDetailsRegistrationModel();
            stewardModel.TireDetails.TireDetailsItemType = new List<TireDetailsItemTypeModel>();

            var defaultItems = vendorRepository.GetDefaultItemsItemParticipantType(ItemParticipantTypeEnum.Steward)
                .Where(s => s.ItemCategory == (int)ItemCategoryEnum.TireType).ToList();
            foreach (var defaultItem in defaultItems)
            {
                bool isChecked = false;
                if (customer.Items.Any(r => r.ID == defaultItem.ID))
                {
                    isChecked = true;
                }
                TireDetailsItemTypeModel tireDetailsItemTypeModel = new TireDetailsItemTypeModel()
                {
                    ID = defaultItem.ID,
                    Description = defaultItem.Description,
                    Name = defaultItem.Name,
                    ShortName = defaultItem.ShortName,
                    isChecked = isChecked,
                    IsActive = defaultItem.IsActive,
                    ItemTypeID = defaultItem.ItemType,
                };

                stewardModel.TireDetails.TireDetailsItemType.Add(tireDetailsItemTypeModel);
            }

            stewardModel.SupportingDocuments = new SupportingDocumentsRegistrationModel();
            foreach (var supportingDocument in customer.CustomerSupportingDocuments)
            {
                SupportingDocumentTypeEnum supportingDocumentType;
                if (Enum.TryParse(supportingDocument.TypeID.ToString(), out supportingDocumentType))
                {
                    switch (supportingDocumentType)
                    {
                        case SupportingDocumentTypeEnum.MBL:
                            stewardModel.SupportingDocuments.SupportingDocumentsOptionMBL = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CLI:
                            stewardModel.SupportingDocuments.SupportingDocumentsOptionCLI = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.HST:
                            stewardModel.SupportingDocuments.SupportingDocumentsOptionHST = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.WSIB:
                            stewardModel.SupportingDocuments.SupportingDocumentsOptionWSIB = supportingDocument.Option;
                            break;
                        case SupportingDocumentTypeEnum.CHQ:
                            stewardModel.SupportingDocuments.SupportingDocumentsOptionCHQ = supportingDocument.Option;
                            break;
                        default:
                            break;
                    }
                }
            }

            return stewardModel;
        }
    }
}