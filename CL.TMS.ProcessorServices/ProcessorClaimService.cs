﻿using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.ProcessorBLL;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.IRepository.Claims;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.ProcessorServices
{
    public class ProcessorClaimService : IProcessorClaimService
    {
        private ProcessorClaimBO processorClaimBO;

        public ProcessorClaimService(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            processorClaimBO = new ProcessorClaimBO(claimsRepository, transactionRepository, gpRepository, settingRepository);
        }
     
        public ProcessorClaimSummaryModel LoadProcessorClaimSummary(int vendorId, int claimId)
        {
            try
            {
                return processorClaimBO.LoadProcessorClaimSummary(vendorId, claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProcessorSubmitClaimViewModel CheckClaimSubmitBusinessRule(ProcessorSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                return processorClaimBO.CheckClaimSubmitBusinessRule(submitClaimModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public RateDetailsVM LoadPIRatesByTransactionID(int rateTransactionID)
        {
            try
            {
                return processorClaimBO.LoadPIRatesByTransactionID(rateTransactionID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public GpResponseMsg CreateBatch()
        {
            return processorClaimBO.CreateBatch();
        }

    }
}
