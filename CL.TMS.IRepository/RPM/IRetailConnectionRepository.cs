﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.RetailConnectionService;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Email;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Export;
using CL.TMS.Framework.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.IRepository.RPM
{
    public interface IRetailConnectionRepository
    {
        void AddProductAttachment(AttachmentModel attachmentModel);
        PaginationDTO<ParticipantProductsViewModel, int> LoadParticipantProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);
        PaginationDTO<StaffCategoryViewModel, int> LoadProductCategory(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        PaginationDTO<StaffProductViewModel, int> LoadStaffProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);
        PaginationDTO<RetailerViewModel, int> LoadStaffRetailers(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);
        AttachmentModel GetProductAttachment(int attachmentId);
        void RemoveTemporaryProductAttachment(int attachmentId);

        List<StaffCategoryViewModel> LoadProductCategory();
        void AddProduct(Product product);

        void AddCategory(ProductCategory category);
        void AddCategoryNote(CategoryNote note);

        int GetRetailerIdByRetailerName(string retailerName);
        bool CheckRetailerName(string retailerName);
        void AddRetailer(Retailer retailer);
        void AddRetailerNote(RetailerNote note);

        int GetCategoryIdByCategoryName(string categoryName);

        PaginationDTO<RetailerViewModel, int> LoadParticipantRetailer(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId);
        List<RetailerReference> LoadParticipantRetailer(int vendorId);

        List<RetailerReference> LoadAllActiveRetailer();

        List<RetailerNote> LoadRetailerInternalNotes(int retailerId);

        void AddRetailerProducts(List<RetailerProduct> retailerProducts);

        void UpdateRetailerProduct(ProductsFormViewModel productViewModel, int productId, long userId);

        Product GetProduct(int productId);

        List<RetailerProductFormViewModel> GetProductRetailers(int productId);

        void UpdateStaffProduct(ProductsFormViewModel productViewModel, long userId);

        void ChangeProductStatus(int productId, string fromStatus, string toStatus);

        ProductRetailerApproveEmailModel GetEmailInforByProductId(int productId);
        ProductRetailerApproveEmailModel GetEmailInforByRetailerId(int retailerId);

        void ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus);

        void ChangeRetailerStatus(int retailerId, string fromStatus, string toStatus);

        RetailersFormViewModel LoadRetailerById(int retailerId);

        void EditRetailer(RetailersFormViewModel retailerFormViewModel);

        List<ParticipantRetailerExportModel> GetParticipantRetailerExport(int vendorId);

        List<StaffRetailerExportModel> GetStaffRetailerExport(int vendorId);

        List<ParticipantProductExportModel> GetParticipantProductExport(int vendorId);

        List<StaffProductExportModel> GetStaffProductExport(int vendorId);

        List<ProductCategoryExportModel> GetProductCategoryExport();

        StaffCategoryFormViewModel LoadCategoryById(int categoryId);

        List<CategoryModel> LoadActiveCategory();

        List<CollectorModel> LoadActiveCollectors();

        List<ProductListModel> LoadActiveProductList(int categoryId);

        Product LoadProductDetails(int productId);

        List<RetailerModel> LoadProductRetailers(int productId);
    }
}
