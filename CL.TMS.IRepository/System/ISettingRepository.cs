﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.IRepository.System
{
    public interface ISettingRepository
    {
        IQueryable<Setting> GetAll();
        string GetSettingValue(string key);
        void AddSetting(Setting setting);
        void EditSetting(Setting setting);
        void DeleteSetting(Setting setting);

        List<TypeDefinition> LoadDefinitions();
        List<Role> LoadUserRoles();

        List<ParticipantType> LoadAllParticipantTypes();

        List<Item> LoadAllItems();

        List<Rate> LoadRates(DateTime date);

        List<ItemWeight> LoadRecoveryItemWeights(DateTime date);

        List<ItemWeight> LoadSupplyItemWeights(); //OTSTM2-1042

        List<TransactionType> LoadTransactionTypes();

        List<PostalCode> LoadPostalCodes();

        List<Zone> LoadZones();

        void AddSystemActivity(SystemActivity systemActivity);

        List<SystemActivity> LoadSystemActivity(string category, int activityId);

        User GetSystemUser();

        List<GpiAccount> LoadGPIAccounts();

        List<Item> LoadAllItemsForReportingService();

        List<RolePermissionViewModel> GetDefaultRolePermissions();

        List<AppResource> GetAppResources();
        List<AppPermission> GetAppPermissions();

        List<SpecificRateModel> LoadSpecificRates();
    }
}