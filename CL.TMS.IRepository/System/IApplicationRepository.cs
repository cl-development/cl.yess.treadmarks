﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;

namespace CL.TMS.IRepository.System
{
    public interface IApplicationRepository
    {
        void UpdateFormObject(int id, string formObject, string updatedBy);
        void UpdateApplication(Application application);
        void UpdateApplicationEntity(Application application);
        void UpdateApplicationGraph(Application application);
        string GetFormObject(int id);
        Application GetSingleApplication(int id);
        Application GetApplicationByTokenId(Guid tokenId);

        Application AddApplication(Application app);
        void AddApplicationAttachments(int applicationId, IEnumerable<AttachmentModel> attachments);
        IEnumerable<AttachmentModel> GetApplicationAttachments(int applicationId, bool bBankInfo = false);
        AttachmentModel GetApplicationAttachment(int applicationId, string fileUniqueName);
        void RemoveApplicationAttachment(int applicationId, string fileUniqueName);
        void UpdateAttachmentDescription(string fileUniqueName, string description);
        void SetStatus(int id, ApplicationStatusEnum status, string denyReasons, long assignToUser = 0);
        Application FindApplicationByAppID(int id);

        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection);

        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForVendor(int vendorId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);

        PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForCustomer(int customerId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        void AddApplicationNote(ApplicationNote applicationNote);

        List<ApplicationNoteViewMode> LoadApplicationNotesForExport(int applicationId, string searchText, string sortcolumn, string sortdirection);

        List<ApplicationNoteViewMode> LoadVendorNotesForExport(int vendorId, string searchText, string sortcolumn, string sortdirection);

        List<ApplicationNoteViewMode> LoadCustomerNotesForExport(int customerId, string searchText, string sortcolumn, string sortdirection);

        void UpdateApplicatioNoteWithVendorId(int vendorId, int applicationId);
        void UpdateApplicatioNoteWithCustomerId(int customerId, int applicationId);

        void UpdateApplicatioExpireDate(int applicationId, int days);
    }
}
