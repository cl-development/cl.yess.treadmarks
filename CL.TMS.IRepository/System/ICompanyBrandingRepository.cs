﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections.Generic;

namespace CL.TMS.IRepository.System
{
    public interface ICompanyBrandingRepository
    {   
        TermsAndConditions GetTermConditionByID(int applicationTypeID);
        string GetTermConditionContentByID(int? termCondtionID, int applicationTypeID = 0);
        int GetCurrentTermConditionID(int applicationTypeID);
        void SaveTermCondition(TermAndConditionVM vm, long userID);
        bool RemoveTermCondition(int id);
        List<InternalNoteViewModel> LoadNoteByID(int transactionID);
        TermsAndConditionsNote AddNote(TermsAndConditionsNote note);
        List<InternalNoteViewModel> ExportNotesToExcel(int transactionID, bool sortReverse, string sortcolumn, string searchText);
        CompanyInformationVM LoadCompanyInformation();
        void UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath);
        string LoadCompanyLogoUrl();      
    }
}