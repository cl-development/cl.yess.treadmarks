﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;

namespace CL.TMS.IRepository.System
{
    public interface IGpRepository
    {
        void AddGpBatch(GpiBatch gpiBatch);
        void AddGpRrOtsCustomers(List<RrOtsRmCustomer> customers);
        void AddGpRrOtsCustomer(RrOtsRmCustomer customer);
        void AddGpRrOtsVendors(List<RrOtsPmVendor> vendors);
        void AddGpRrOtsRmCashReceipts(List<RrOtsRmCashReceipt> cashReceipts);
        void AddGpRrOtsRmTransactions(List<RrOtsRmTransaction> rrOtsRmTransactions);
        void AddGpRrOtsPmTransactions(List<RrOtsPmTransaction> rrOtsPmTransactions);
        void AddGpRrOtsPmTransDistributions(List<RrOtsPmTransDistribution> rrOtsPmTransDistributions);
        void AddRrOtsPmTransDistribution(RrOtsPmTransDistribution rrOtsPmTransDistribution);
        void AddGpRrOtsRmApplies(List<RrOtsRmApply> rrOtsRmApplies);
        void AddRrOtsRmTransDistributions(List<RrOtsRmTransDistribution> rrOtsRmTransDistributions);
        void AddRrOtsPmTransDistributions(List<RrOtsPmTransDistribution> rrOtsPmTransDistributions);
        void AddGpRrOtsVendor(RrOtsPmVendor vendor);
        void AddGpBatchEntry(GpiBatchEntry gpiBatchEntry);
        void AddGpBatchEntries(List<GpiBatchEntry> gpiBatchEntries);
        List<GpFiscalYear> GetAllGpFiscalYears();
        List<GpClaimDto> GetClaimsByGpBatchId(int gpiBatchId);
        List<GpTsfClaimDto> GetTsfClaimsCheckBookByGpBatchId(int gpiBatchId);
        List<GpTsfClaimDto> GetTsfClaimsRegistrantByGpBatchId(int gpiBatchId);
        IQueryable<GpiBatch> GetAllGpBatches();
        GpiAccount GetP2GpiAccountByAccountNumber(string accountNumber);
        IQueryable<BankInformation> GetAllBankingInformation();
        GpiBatch GetGpBatch(int gpiBatchId);
        IQueryable<GpChequeBook> GetAllGpCheckBooks();
        GpiBatchEntry GetGpBatchEntry(int gpiBatchEntryId);
        List<GpiBatchEntry> GetGpBatchEntriesByBatchId(int gpiBatchId);
        IQueryable<GpiBatch> GetGpBatchesByFilter(string gpiTypeId);
        IQueryable<RrOtsRmCustomer> GetAllRrOtsRmCustomers();
        IQueryable<RrOtsPmVendor> GetAllRrOtsPmVendors();
        IQueryable<RrOtsPmTransaction> GetAllRrOtsPmTransactions();
        //IQueryable<RrOtsPmTransDistribution> GetAllRrOtsPmTransDistributions();
        IQueryable<GpiAccount> GetAllGpiAccounts();
        List<RrOtsRmCustomer> GetRrOtsRmCustomersByBatchId(int gpiBatchId);
        List<RrOtsPmVendor> GetRrOtsPmVendorsByBatchId(int gpiBatchId);
        List<RrOtsRmApply> GetRrOtsRmApplyByBatchId(int gpiBatchId);
        List<RrOtsRmCashReceipt> GetRrOtsRmCashReceiptByBatchId(int gpiBatchId);
        List<RrOtsRmTransDistribution> GetRrOtsRmTransDistributionByBatchId(int gpiBatchId);
        List<RrOtsRmTransaction> GetRrOtsRmTransactionByBatchId(int gpiBatchId);
        List<RrOtsPmTransaction> GetRrOtsPmTransactionByBatchId(int gpiBatchId);
        List<RrOtsPmTransDistribution> GetRrOtsPmTransDistributionByBatchId(int gpiBatchId); 
        void UpdateGpBatch(GpiBatch gpBatch);
        void UpdateGpBatchEntry(GpiBatchEntry gpiBatchEntry);
        void UpdateGpBatchEntries(List<GpiBatchEntry> gpiBatchEntries);
        void UpdateRrOtsRmCustomer(RrOtsRmCustomer customer);
        void UpdateRrOtsPmVendor(RrOtsPmVendor vendor);
        void UpdateRrOtsPmTransaction(string batchId);
        void DeleteGpEntry(GpiBatchEntry gpiBatchEntry);

        //changes cause of UI
        PaginationDTO<GPListViewModel, int> GetAllGpBatchesPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string type, Dictionary<string, string> GpiStatus);
        PaginationDTO<GPModalListViewModal, int> GetGpTransactionsByBatchIdPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int batchId, Dictionary<string, string> GpiStatus);
        List<GPListViewModel> GetAllGpBatchesList(string searchText, string orderBy, string sortDirection, string type, Dictionary<string, string> GpiStatus);
        List<GpRrOtsRmTransDistributionsDto> GetRrOtsRmTransDistributionsForRemittance(int gpiBatchId);
        string GetTransactionNumber(int claimId);
        int GetBatchIdByTSFClaimId(int claimId);        
        void UpdateGpBatchEntries(List<int> gpiBatchEntryIds, string status, string currentUser, string message);
       
        //Admin Financial Integration
        IQueryable<FIAccount> GetAdminFinancialIntegration();
        bool FIUpdateData(List<FinancialIntegrationVM> vm, long? userID);
    }
}
