﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.IRepository.System
{
    public interface IItemRepository
    {
        IList<Item> GetAllItems();
        IList<Item> GetAllItems(int PaticipantType);
        IList<Item> GetAllProduct(int ItemCategory);
        IList<BusinessActivity> GetBusinessActivities(int Type);
    }
}
