﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.RetailConnectionService;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Email;
using CL.TMS.DataContracts.ViewModel.RetailConnection.Export;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ExceptionHandling;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.RPM;
using CL.TMS.RPMBLL;
using CL.TMS.ServiceContracts.RPMServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.RPMServices
{
    public class RetailConnectionService : IRetailConnectionService
    {
        private RetailConnectionBO retailConnectionBO;

        public RetailConnectionService(IRetailConnectionRepository retailConnectionRepository)
        {
            retailConnectionBO = new RetailConnectionBO(retailConnectionRepository);
        }
        public PaginationDTO<ParticipantProductsViewModel, int> LoadParticipantProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return retailConnectionBO.LoadParticipantProducts(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<RetailerViewModel, int> LoadParticipantRetailer(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return retailConnectionBO.LoadParticipantRetailer(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }
        public void AddProduct(ProductsFormViewModel productFormViewModel)
        {
            try
            {
                retailConnectionBO.AddProduct(productFormViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddCategory(StaffCategoryFormViewModel categoryFormViewModel)
        {
            try
            {
                retailConnectionBO.AddCategory(categoryFormViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public bool CheckRetailerName(string retailerName)
        {
            try
            {
                return retailConnectionBO.CheckRetailerName(retailerName);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public void AddRetailer(RetailersFormViewModel retailersFormViewModel, bool isAddedByParticipant)
        {
            try
            {
                retailConnectionBO.AddRetailer(retailersFormViewModel, isAddedByParticipant);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public List<StaffCategoryViewModel> LoadProductCategory()
        {
            try
            {
                return retailConnectionBO.LoadProductCategory();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<RetailerReference> LoadParticipantRetailer()
        {
            try
            {
                return retailConnectionBO.LoadParticipantRetailer();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public PaginationDTO<StaffCategoryViewModel, int> LoadProductCategory(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return retailConnectionBO.LoadProductCategory(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<StaffProductViewModel, int> LoadStaffProducts(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return retailConnectionBO.LoadStaffProducts(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public PaginationDTO<RetailerViewModel, int> LoadStaffRetailers(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            try
            {
                return retailConnectionBO.LoadStaffRetailers(pageIndex, pageSize, searchText, orderBy, sortDirection);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<InternalNoteViewModel> LoadRetailerInternalNotes(int retailerId)
        {
            try
            {
                return retailConnectionBO.LoadRetailerInternalNotes(retailerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public InternalNoteViewModel AddRetailerNote(int retailerId, string note)
        {
            try
            {
                return retailConnectionBO.AddRetailerNote(retailerId, note);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProductsFormViewModel GetProduct(int productId)
        {
            try
            {
                return retailConnectionBO.GetProduct(productId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;

        }

        public void EditProduct(ProductsFormViewModel productViewModel)
        {
            try
            {
                retailConnectionBO.EditProduct(productViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void EditStaffProduct(ProductsFormViewModel productViewModel)
        {
            try
            {
                retailConnectionBO.EditStaffProduct(productViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ChangeProductStatus(int productId, string fromStatus, string toStatus)
        {
            try
            {
                retailConnectionBO.ChangeProductStatus(productId, fromStatus, toStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public ProductRetailerApproveEmailModel GetEmailInforByProductId(int productId)
        {
            try
            {
                return retailConnectionBO.GetEmailInforByProductId(productId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProductRetailerApproveEmailModel GetEmailInforByRetailerId(int retailerId)
        {
            try
            {
                return retailConnectionBO.GetEmailInforByRetailerId(retailerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus)
        {
            try
            {
                retailConnectionBO.ChangeCategoryStatus(categoryId, fromStatus, toStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void ChangeRetailerStatus(int retailerId, string fromStatus, string toStatus)
        {
            try
            {
                retailConnectionBO.ChangeRetailerStatus(retailerId, fromStatus, toStatus);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public RetailersFormViewModel LoadRetailerById(int retailerId)
        {
            try
            {
                return retailConnectionBO.LoadRetailerById(retailerId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void AddRetailerNote(RetailerNoteViewModel retailerNoteViewModel)
        {
            try
            {
                retailConnectionBO.AddRetailerNote(retailerNoteViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void EditRetailer(RetailersFormViewModel retailerFormViewModel)
        {
            try
            {
                retailConnectionBO.EditRetailer(retailerFormViewModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #region Export
        public List<ParticipantRetailerExportModel> GetParticipantRetailerExport()
        {
            try
            {
                return retailConnectionBO.GetParticipantRetailerExport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<StaffRetailerExportModel> GetStaffRetailerExport()
        {
            try
            {
                return retailConnectionBO.GetStaffRetailerExport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ParticipantProductExportModel> GetParticipantProductExport()
        {
            try
            {
                return retailConnectionBO.GetParticipantProductExport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<StaffProductExportModel> GetStaffProductExport()
        {
            try
            {
                return retailConnectionBO.GetStaffProductExport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ProductCategoryExportModel> GetProductCategoryExport()
        {
            try
            {
                return retailConnectionBO.GetProductCategoryExport();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public StaffCategoryFormViewModel LoadCategoryById(int categoryId)
        {
            try
            {
                return retailConnectionBO.LoadCategoryById(categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        #endregion

        #region Retail Connection Web Service

        public List<CollectorModel> LoadActiveCollectors()
        {
            try
            {
                return retailConnectionBO.LoadActiveCollectors();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<CategoryModel> LoadActiveCategory()
        {
            try
            {
                return retailConnectionBO.LoadActiveCategory();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public List<ProductListModel> LoadActiveProductList(int categoryId)
        {
            try
            {
                return retailConnectionBO.LoadActiveProductList(categoryId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public ProductModel LoadActiveProductDetails(int productId)
        {
            try
            {
                return retailConnectionBO.LoadActiveProductDetails(productId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }
        #endregion
    }
}
