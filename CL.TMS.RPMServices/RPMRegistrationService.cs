﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.RPM;
using CL.TMS.ExceptionHandling;
using CL.TMS.RPMBLL;
using CL.TMS.IRepository.System;
using CL.TMS.ServiceContracts.RPMServices;
using CL.TMS.SystemBLL;
using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.IRepository;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Registrant;
using CL.TMS.RegistrantBLL;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.IRepository.Claims;

namespace CL.TMS.RPMServices
{
    public class RPMRegistrationService : IRPMRegistrationService
    {
        private RPMRegistrationBO RPMRegistrationBO;
        private FileUploadBO fileUploadBO;
        private RegistrantBO registrantBO;

        public RPMRegistrationService(IApplicationRepository applicationRepository, IItemRepository itemRepository, 
            IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository,
            IAssetRepository assetRepository, IAppUserRepository appUserRepository, IApplicationInvitationRepository appInvitationRepository, ITSFRemittanceRepository tsfRemittanceRepository, IClaimsRepository claimsRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository,IMessageRepository messageRepository)
        {
            this.RPMRegistrationBO = new RPMRegistrationBO(applicationRepository, itemRepository, vendorRepository, applicationInvitationRepository, claimsRepository);
            //this.fileUploadBO = new FileUploadBO(applicationRepository);
            this.registrantBO = new RegistrantBO(vendorRepository, assetRepository, appUserRepository, applicationRepository, tsfRemittanceRepository, appInvitationRepository, gpRepository, settingRepository, claimsRepository, messageRepository);
        }

        #region ApplicationServices Methods
      

        public RPMRegistrationModel GetFormObject(int id)
        {
            RPMRegistrationModel result = new RPMRegistrationModel();
            try
            {
                result = RPMRegistrationBO.GetFormObject(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public RPMRegistrationModel GetByTokenId(Guid id)
        {
            RPMRegistrationModel result = new RPMRegistrationModel();
            try
            {
                result = RPMRegistrationBO.GetByTokenId(id);
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;            
        }

        public int ApproveApplication(RPMRegistrationModel model, int appID)
        {
            try
            {
                return this.RPMRegistrationBO.ApproveApplication(model, appID);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }

        public int UpdateApproveApplication(RPMRegistrationModel model,int appID)
        {
            try
            {
                return this.RPMRegistrationBO.UpdateApproveApplication(model,appID);
              
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return -1;
            }
        }

        public RPMRegistrationModel GetApprovedApplication(int vendorId)
        {
            try
            {
                return this.RPMRegistrationBO.GetApprovedApplication(vendorId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
                return null;
            }
        }
        #endregion

        #region IRPMRegistrationService Methods

        public void UpdateUserExistsinApplication(RPMRegistrationModel model, int appId, string updatedBy)
        {
            try
            {
                var application = RPMRegistrationBO.GetApplicationById(appId);
                if (application != null)
                {
                    IList<string> tmpInvalidFields = model.InvalidFormFields ?? new List<string>();
                    IList<ContactRegistrationModel> tmpContacts = model.ContactInformationList;
                    IList<RPMSortYardDetailsRegistrationModel> tmpSortYardDetails = model.SortYardDetails;

                    //remove empty contacts from lists
                    RPMRegistrationBO.RemoveEmptyContacts(ref tmpContacts, ref tmpInvalidFields);
                    RPMRegistrationBO.RemoveEmptySortYardDetails(ref tmpSortYardDetails, ref tmpInvalidFields);

                    //reassign model values to new tmp variables
                    model.ContactInformationList = tmpContacts as List<ContactRegistrationModel>;
                    model.SortYardDetails = tmpSortYardDetails as List<RPMSortYardDetailsRegistrationModel>;
                    model.InvalidFormFields = tmpInvalidFields ?? new List<string>();

                    //serialize object
                    string formObjectDataSerialized = JsonConvert.SerializeObject(model);
                    Application result = new Application()
                    {
                        FormObject = formObjectDataSerialized,
                        ID = appId,
                        AgreementAcceptedCheck = true,
                        UserIP = model.UserIPAddress
                    };

                    if (model.BusinessLocation != null)
                    {
                        result.Company = model.BusinessLocation.BusinessName;
                    }

                    if (model.ContactInformationList.Count > 0)
                    {
                        result.Contact = string.Format("{0} {1} {2}",
                        model.ContactInformationList[0].ContactInformation.FirstName,
                        model.ContactInformationList[0].ContactInformation.LastName,
                        model.ContactInformationList[0].ContactInformation.PhoneNumber);
                    }

                    RPMRegistrationBO.UpdateApplication(result);
                }
                else
                {
                    string formObjectData = JsonConvert.SerializeObject(model);
                    RPMRegistrationBO.UpdateFormObject(appId, formObjectData, updatedBy);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public Address GetAddressByID(int addressID)
        {
            Address addr = new Address();
            return addr;
        }        

        public RPMRegistrationModel GetAllItemsList(int ParticipantType)
        {
            RPMRegistrationModel result = new RPMRegistrationModel();
            try
            {
                result = RPMRegistrationBO.GetAllItemsList(ParticipantType);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }

        public RPMRegistrationModel GetAllItemsList()
        {
            RPMRegistrationModel result = new RPMRegistrationModel();
            try
            {
                result = RPMRegistrationBO.GetAllItemsList();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }

        public RPMRegistrationModel GetAllProduct()
        {
            RPMRegistrationModel result = new RPMRegistrationModel();
            try
            {
                result = RPMRegistrationBO.GetAllProduct();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return result;
        }
        public RPMRegistrationModel GetAllItemsWithAllChecks()
        {
            RPMRegistrationModel model = new RPMRegistrationModel();
            return model;
        }

        #endregion

        #region IActivityMessageService Methods

        public IList<ActivityMessageModel> GetActivityMessages(long? applicationId)
        {

            throw new NotImplementedException();

            //if (applicationId == null || applicationId <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = activityPageLength;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;


        }

        public IList<ActivityMessageModel> GetAllActivityMessages(long? applicationId, string searchValue)
        {
            throw new NotImplementedException();

            //if (applicationID == null || applicationID <= 0)
            //    return new List<ActivityMessageModel>();

            //ActivityShowTypeModel actShow = new ActivityShowTypeModel();
            //actShow.ActivityLevelCode = activityShowLevel;
            //actShow.Resource = activityResource;
            //actShow.EntityID = (long)applicationID;
            //actShow.EntityTable = activityEntityTable;
            //actShow.PageSize = 0;
            //actShow.PageNumber = 0;
            //ActivityShowResultType actShowResult = activityService.Show(actShow);
            //return actShowResult.Messages;
        }


        #endregion

        //public RPMRegistrationModel GetApplicantInfoByApplicationID(int applicationID)
        //{
        //    RPMRegistrationBO.CheckIfApplicationExists(applicationID);

        //    RPMRegistrationModel model = new RPMRegistrationModel();
        //    return model;
        //}

        #region WorkFlow Services

        public void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0)
        {
            try
            {
                this.registrantBO.SetStatus(applicationId, applicationStatus, denyReasons, assignToUser);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }

            #region Activity Logging
            //LogActivity(applicationId, null, String.Format("Application Status changed to {0} by {1} on {2}", applicationStatus.ToString(), updatedBy, DateTime.Now), updatedBy);
            #endregion

        }

        public ApplicationEmailModel GetApprovedRPMApplicantInformation(int applicationId)
        {
            try
            {
                return this.RPMRegistrationBO.GetApprovedRPMApplicantInformation(applicationId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }        

        public void SendEmailForBackToApplicant(int applicationID, ApplicationEmailModel emailModel)
        {
            try
            {
                this.registrantBO.SendEmailForBackToApplicantRPM(applicationID, emailModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        #endregion        
    }
}
