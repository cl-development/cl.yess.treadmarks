﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Processor;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.Registrant;
using CL.TMS.Repository.System;
using System.Configuration;
using CL.TMS.Security;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.ProcessorBLL
{
    public class ProcessorClaimBO : BaseBO
    {
        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        public ProcessorClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository,
            IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.settingRepository = settingRepository;
        }

        #region Public Methods

        public ProcessorClaimSummaryModel LoadProcessorClaimSummary(int vendorId, int claimId)
        {
            var processorClaimSummary = new ProcessorClaimSummaryModel();

            //Load status
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            processorClaimSummary.ClaimCommonModel.Status = status;
            processorClaimSummary.ClaimCommonModel.RegistrationNumber = claim.Participant.Number;

            //Load ClaimPeriod
            processorClaimSummary.ClaimCommonModel.ClaimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (processorClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
            processorClaimSummary.ClaimCommonModel.VendorId = vendorId;

            //Load Inventory Opening data
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);
            processorClaimSummary.TotalOpening =
                DataConversion.ConvertKgToTon((double)inventoryOpeningResult.TotalOpening);

            //Load Inbound and Outbound Transactions
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);

            PopulateInboundOutbound(processorClaimSummary, claimDetails);

            //Load Inventory Adjustment
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateInventoryAdjustment(processorClaimSummary, inventoryAdjustments);

            //Load sort yard capacity
            processorClaimSummary.TotalCapacity =
                DataConversion.ConvertKgToTon((double)claimsRepository.LoadVendorSortYardCapacity(vendorId));

            //Load payment and adjustment
            //OTSTM2-1139 No talk back needed
            //UpdateGpBatchForClaim(claimId);
            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    processorClaimSummary.ProcessorPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(processorClaimSummary.ProcessorPayment, claimPaymentSummay, claimPayments,
                claimPaymentAdjustments);

            processorClaimSummary.ProcessorPayment.PaymentDueDate = claim.ChequeDueDate;
            // set tax flag
            processorClaimSummary.ProcessorPayment.IsTaxApplicable = claim.IsTaxApplicable ?? false;
            // calculate HST
            processorClaimSummary.ProcessorPayment.HST = 0;
            if (processorClaimSummary.ProcessorPayment.IsTaxApplicable)
            {
                var temp = processorClaimSummary.ProcessorPayment.PTR + processorClaimSummary.ProcessorPayment.SPS + processorClaimSummary.ProcessorPayment.PITOutbound + processorClaimSummary.ProcessorPayment.DOR + processorClaimSummary.ProcessorPayment.PaymentAdjustment;
                //if (temp > 0) //OTSTM2-1296 HST needs to calculate for negative too.
                //{
                    processorClaimSummary.ProcessorPayment.HST = Math.Round(temp * processorClaimSummary.ProcessorPayment.HSTBase, 2, MidpointRounding.AwayFromZero);
                //}
            }
            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                LoadStaffClaimSummary(processorClaimSummary, claimId, vendorId,
                    processorClaimSummary.ClaimCommonModel.ClaimPeriod);

                if (status == ClaimStatus.Open)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}",
                        claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }
                if (status == ClaimStatus.Approved)
                {
                    processorClaimSummary.ClaimStatus.UnderReview = "Approved";
                }
                processorClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499

                processorClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                processorClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                processorClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            }

            ApplyUserSecurity(status, processorClaimSummary);

            //OTSTM2-1198 Check if processor specific rate defined - Processor claim summary page
            processorClaimSummary.AssociatedRateTransactionId = claimsRepository.GetAssociatedRateTrnsactionId(processorClaimSummary.ClaimCommonModel.ClaimPeriod.StartDate, processorClaimSummary.ClaimCommonModel.ClaimPeriod.EndDate, vendorId);
            processorClaimSummary.HasSpecificRate = processorClaimSummary.AssociatedRateTransactionId != 0;

            return processorClaimSummary;
        }

        private void ApplyUserSecurity(ClaimStatus status, ProcessorClaimSummaryModel processorClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    processorClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    processorClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsProcessorClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        processorClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        public ProcessorSubmitClaimViewModel CheckClaimSubmitBusinessRule(ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>
            {
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel}
            };

            var updatedSubmitClaimForError = ProcessorClaimSubmitErrors(claim, ruleContext, submitClaimModel);
            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = ProcessorClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;

        }
        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                var claimType = (int)ClaimType.Processor;
                var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
                //var gpiAccountsAll = DataLoader.GPIAccounts;             
                //var gpiAccounts = gpiAccountsAll.Where(i => i.registrant_type_ind == "P");
                var fiAccounts = DataLoader.FIAccounts; //OTSTM2-1124
                //var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, false);              
                var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, true); //OTSTM2-16
                var allClaimIds = allClaims.Select(c => c.ID).ToList();
                //Remove claim if it is banking status is not approved
                var claims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

                if (!claims.Any())
                {
                    msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
                }
                else
                {

                    // STEP 1 INSERT INTO gpibatch
                    var gpiBatch = new GpiBatch()
                    {
                        GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Processor),
                        GpiBatchCount = claims.Count,
                        GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                    };
                    gpRepository.AddGpBatch(gpiBatch);

                    msg.ID = gpiBatch.ID;
                    // STEP 2 INSERT INTO gpibatch_entry
                    var gpiBatchEntries = claims.Select(claim => new GpiBatchEntry()
                    {
                        GpiBatchID = gpiBatch.ID,
                        GpiTxnNumber = string.Format("{0}-{1:yyyy-MM-dd}-{2}", claim.Participant.Number, claim.ClaimPeriod.EndDate, claim.ID),
                        GpiBatchEntryDataKey = claim.ID.ToString(),
                        GpiStatusID = GpHelper.GetGpiStatusString(GpStatus.Extract),
                        CreatedDate = DateTime.Now,
                        CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                        LastUpdatedDate = DateTime.Now,
                        ClaimId = claim.ID
                    }).ToList();
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    var minPostDate = gpRepository.GetAllGpFiscalYears()
                        .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                        .Select(d => d.MinimumDocDate).FirstOrDefault();

                    // STEP 3 INSERT INTO rr_ots_pm_transactions
                    var rrOtsPmTransactions = this.GetTransactions(gpiBatch.ID, minPostDate, INTERID);
                    gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransactions);

                    //list of claims and batch entries
                    var claimIds = rrOtsPmTransactions.Select(i => i.ClaimId).ToList();
                    var items = DataLoader.Items;
                    var claimDetailsAll = claimsRepository.LoadClaimDetailsForGP(claimIds, new List<string> { "PTR" }, items);
                    var rrOtsPmTransDistributions = this.GetTransDistributions(rrOtsPmTransactions, claimDetailsAll, fiAccounts, claims, INTERID); //OTSTM2-1124
                    //var debits = rrOtsPmTransDistributions.Sum(i => i.DEBITAMT);
                    //var credits = rrOtsPmTransDistributions.Sum(i => i.CRDTAMNT);
                    gpRepository.AddGpRrOtsPmTransDistributions(rrOtsPmTransDistributions);

                    // STEP 8 UPDATE processor_claim_summary
                    var claimIdsForUpdate = claims.Select(c => c.ID).ToList();
                    claimsRepository.UpdateClaimsForGPBatch(claimIdsForUpdate, gpiBatch.ID.ToString());
                    transationScope.Complete();
                }
            }
            return msg;
        }

        #endregion

        public RateDetailsVM LoadPIRatesByTransactionID(int RateTransactionID)
        {
            RateDetailsVM ratesVM = new RateDetailsVM()
            {
                RateTransactionID = RateTransactionID
            };

            var param = new RateParamsDTO()
            {
                categoryName = TreadMarksConstants.ProcessingIncentiveRates
            };


            LoadPIRates(param, ratesVM);

            return ratesVM;
        }

        private void LoadPIRates(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            getProcessingIncentiveParam(param);
            ratesVM.decimalsize = 2;
            var rates = claimsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
            ratesVM.notes = claimsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
            ratesVM = PopulateProcessingIncentiveRateVM(rates, ratesVM, param.PI);
            ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
        }

        private RateDetailsVM PopulateProcessingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, ProcessingIncentiveParam param)
        {
            var SPS = result.Where(x => x.PaymentType == param.spsPaymentType);
            var PIT = result.Where(x => x.PaymentType == param.pitPaymentType);
            ratesVM.processingIncentiveRate = new ProcessingIncentiveRateVM()
            {
                #region //SPS OnRoad
                TDP1OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OnRoad).ItemRate,
                TDP1FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOnRoad).ItemRate,
                TDP2OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OnRoad).ItemRate,
                TDP2FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOnRoad).ItemRate,
                TDP3OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OnRoad).ItemRate,
                TDP3FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOnRoad).ItemRate,
                TDP4OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OnRoad).ItemRate,
                TDP5OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OnRoad).ItemRate,
                #endregion

                #region //SPS OffRoad
                TDP1OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OffRoad).ItemRate,
                TDP1FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOffRoad).ItemRate,
                TDP2OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OffRoad).ItemRate,
                TDP2FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOffRoad).ItemRate,
                TDP3OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OffRoad).ItemRate,
                TDP3FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOffRoad).ItemRate,
                TDP4OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OffRoad).ItemRate,
                TDP5OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OffRoad).ItemRate,
                #endregion

                #region //PIT
                TDP1 = PIT.FirstOrDefault(x => x.ItemID == param.tdp1).ItemRate,
                TDP2 = PIT.FirstOrDefault(x => x.ItemID == param.tdp2).ItemRate,
                TDP3 = PIT.FirstOrDefault(x => x.ItemID == param.tdp3).ItemRate,
                TDP4FF = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FF).ItemRate,
                TDP4FFNoPI = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FFNoPI).ItemRate,
                TDP5FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5FT).ItemRate,
                TDP5NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5NT).ItemRate,
                TDP6NP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6NP).ItemRate,
                TDP6FP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6FP).ItemRate,
                TDP7NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7NT).ItemRate,
                TDP7FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7FT).ItemRate,
                TransferFibreRubber = PIT.FirstOrDefault(x => x.ItemID == param.transferFibreRubber).ItemRate,
                #endregion
            };
            return ratesVM;
        }

        private void getProcessingIncentiveParam(RateParamsDTO param)
        {
            var itemParams = new
            {
                pitItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue,
                spsItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue,
                onRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OnRoad").DefinitionValue,
                offRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OffRoad").DefinitionValue,
                spsPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorSPS").DefinitionValue,//4
                pitPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorPITOutbound").DefinitionValue//5
            };

            var itemList = new
            {
                spsOnRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.onRoad),
                spsOffRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.offRoad),
                pit = DataLoader.Items.Where(i => i.ItemCategory == itemParams.pitItemCategory)
            };

            param.PI = new ProcessingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                pitItemCategory = itemParams.pitItemCategory,
                spsItemCategory = itemParams.spsItemCategory,
                onRoad = itemParams.onRoad,
                offRoad = itemParams.offRoad,
                spsPaymentType = itemParams.spsPaymentType,
                pitPaymentType = itemParams.pitPaymentType,

                spsOffRoad = itemList.spsOffRoad,
                spsOnRoad = itemList.spsOnRoad,
                pit = itemList.pit,

                #region //SPS OnRoad
                tdp1OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //SPS OffRoad
                tdp1OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //PIT
                tdp1 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP1")).ID,
                tdp2 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP2")).ID,
                tdp3 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP3")).ID,
                tdp4FF = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && !i.Description.Contains("PI")).ID,
                tdp4FFNoPI = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && i.Description.Contains("PI")).ID,
                tdp5FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5FT")).ID,
                tdp5NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5NT")).ID,
                tdp6NP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6NP")).ID,
                tdp6FP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6FP")).ID,
                tdp7NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7NT")).ID,
                tdp7FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7FT")).ID,
                transferFibreRubber = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TransferFiberRubber")).ID,
                #endregion
            };
            param.PI.items = new int[] {
                #region // SPS On-Road
                param.PI.tdp1OnRoad,
                param.PI.tdp1FeedstockOnRoad,
                param.PI.tdp2OnRoad,
                param.PI.tdp2FeedstockOnRoad,
                param.PI.tdp3OnRoad,
                param.PI.tdp3FeedstockOnRoad,
                param.PI.tdp4OnRoad,
                param.PI.tdp5OnRoad,
                #endregion

                #region //SPS OffRoad
                param.PI.tdp1OffRoad,
                param.PI.tdp1FeedstockOffRoad,
                param.PI.tdp2OffRoad,
                param.PI.tdp2FeedstockOffRoad,
                param.PI.tdp3OffRoad,
                param.PI.tdp3FeedstockOffRoad,
                param.PI.tdp4OffRoad,
                param.PI.tdp5OffRoad,
                #endregion

                #region //PIT
                param.PI.tdp1,
                param.PI.tdp2,
                param.PI.tdp3,
                param.PI.tdp4FF,
                param.PI.tdp4FFNoPI,
                param.PI.tdp5FT,
                param.PI.tdp5NT,
                param.PI.tdp6NP,
                param.PI.tdp6FP,
                param.PI.tdp7NT,
                param.PI.tdp7FT,
                param.PI.transferFibreRubber,
                #endregion
            };
        }

        #region Private Methods

        private List<RrOtsPmTransaction> GetTransactions(int gpiBatchId, DateTime minPostDate, string INTERID)
        {
            var result = new List<RrOtsPmTransaction>();
            //var tmpClaimsDto = gpRepository.GetClaimsByGpBatchId(gpiBatchId).Where(i => i.TotalClaimAmount > 0);
            var tmpClaimsDto = gpRepository.GetClaimsByGpBatchId(gpiBatchId).Where(i => i.TotalClaimAmount != 0); //OTSTM2-16
            foreach (var t in tmpClaimsDto)
            {
                //OTSTM2-16
                var processorPaymentViewModel = LoadProcessorPaymentForGP(t.ClaimId);
                var prchAmount1 = Math.Round(processorPaymentViewModel.PTR + processorPaymentViewModel.SPS + processorPaymentViewModel.PITOutbound + (processorPaymentViewModel.PaymentAdjustment > 0 ? processorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                //var distributedPaymentAdjustment = Math.Round(processorPaymentViewModel.PaymentAdjustment>0?processorPaymentViewModel.PaymentAdjustment:0, 2, MidpointRounding.AwayFromZero);
                var prchAmount5 = Math.Round(processorPaymentViewModel.DOR + (processorPaymentViewModel.PaymentAdjustment < 0 ? processorPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                //var gpTotal = prchAmount1 + prchAmount5;

                //OTSTM2-1294            
                var prchTax1 = (t.IsTaxApplicable ?? false) ? Math.Round(prchAmount1 * processorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero) : 0;
                var prchTax5 = (t.IsTaxApplicable ?? false) ? Math.Round(prchAmount5 * processorPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero) : 0;

                //Fixed 0.01 difference for HST caused by rounding
                var hstTotal = prchTax1 + prchTax5;
                if (hstTotal != processorPaymentViewModel.HST)
                {
                    var difference = (hstTotal - processorPaymentViewModel.HST);
                    //put all difference into prchTax1
                    prchTax1 -= difference;
                }

                var positiveTotal = prchAmount1 + prchTax1;
                var gpTotal = positiveTotal + prchAmount5 + prchTax5;
              
                //Fixed cent difference caused by rounding
                if (gpTotal != processorPaymentViewModel.GroundTotal)
                {
                    var difference = (gpTotal - processorPaymentViewModel.GroundTotal);
                    //put all difference in prchAmount1
                    prchAmount1 -= difference;
                }

                var submissionNumberForTypeOne = "1";
                var submissionNumberForTypeFive = "5";
                //var submissionNumber = "1";

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                if (prchAmount1 != 0) //OTSTM2-16
                {
                    var rowForTypeOne = new RrOtsPmTransaction()
                    {
                        BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                        VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        VENDORID = t.RegistrationNumber,
                        DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        DOCTYPE = 1,
                        //DOCAMNT = t.TotalClaimAmount,
                        DOCAMNT = prchAmount1 + prchTax1, //OTSTM2-1294
                        DOCDATE = t.Period.EndDate < minPostDate ? minPostDate : t.Period.EndDate,
                        PSTGDATE = null,
                        VADDCDPR = "PRIMARY",
                        PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                        TAXSCHID = "P-EXEMPT",
                        DUEDATE = null,
                        //PRCHAMNT = t.TotalClaimAmount,
                        PRCHAMNT = prchAmount1,
                        TAXAMNT = prchTax1, //OTSTM2-1294
                        PORDNMBR = null,
                        USERDEF1 = t.GpiBatchId.ToString(), //t.GpiBatch.ID.ToString(),
                        USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeOne), //OTSTM2-16
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = null,
                        GpiBatchEntryId = t.GpiBatchEntryId, //t.GpiBatchEntry.ID,
                        ClaimId = t.ClaimId
                    };
                    result.Add(rowForTypeOne);
                }

                //OTSTM2-16 -- add type 5 transaction
                if (prchAmount5 != 0)
                {
                    var rowForTypeFive = new RrOtsPmTransaction()
                    {
                        BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                        VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        VENDORID = t.RegistrationNumber,
                        DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        DOCTYPE = 5,
                        //DOCAMNT = t.TotalClaimAmount,
                        DOCAMNT = prchAmount5 + prchTax5, //OTSTM2-1294
                        DOCDATE = t.Period.EndDate < minPostDate ? minPostDate : t.Period.EndDate,
                        PSTGDATE = null,
                        VADDCDPR = "PRIMARY",
                        PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                        TAXSCHID = "P-EXEMPT",
                        DUEDATE = null,
                        //PRCHAMNT = t.TotalClaimAmount,
                        PRCHAMNT = prchAmount5,
                        TAXAMNT = prchTax5, //OTSTM2-1294
                        PORDNMBR = null,
                        USERDEF1 = t.GpiBatchId.ToString(), //t.GpiBatch.ID.ToString(),
                        USERDEF2 = string.Format("{0}-{1:MMyy}-{2}-{3}", t.RegistrationNumber, t.Period.EndDate, "1", submissionNumberForTypeFive), //OTSTM2-16
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //DEX_ROW_ID = null,
                        GpiBatchEntryId = t.GpiBatchEntryId, //t.GpiBatchEntry.ID,
                        ClaimId = t.ClaimId
                    };
                    result.Add(rowForTypeFive);
                }
            }
            return result;
        }

        //OTSTM2-16
        private ProcessorPaymentViewModel LoadProcessorPaymentForGP(int claimId)
        {
            var processorPayment = new ProcessorPaymentViewModel();
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.PTR)
                {
                    processorPayment.PTR = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.SPS)
                {
                    processorPayment.SPS = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.PITOutbound)
                {
                    processorPayment.PITOutbound = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.DOR)
                {
                    processorPayment.DOR = c.Amount;
                }
            });

            processorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);

            //OTSTM2-1294
            processorPayment.IsTaxApplicable = claimsRepository.FindClaimByClaimId(claimId).IsTaxApplicable ?? false;
            processorPayment.HST = 0;
            if (processorPayment.IsTaxApplicable)
            {
                processorPayment.HST = Math.Round((processorPayment.PTR + processorPayment.SPS + processorPayment.PITOutbound + processorPayment.DOR + processorPayment.PaymentAdjustment) * processorPayment.HSTBase, 2, MidpointRounding.AwayFromZero);
            }

            return processorPayment;

        }

        private List<RrOtsPmTransDistribution> GetTransDistributions(List<RrOtsPmTransaction> transactions, List<ClaimDetailViewModel> claimDetails, List<FIAccount> fiAccounts, List<Claim> claims, string INTERID)
        {
            var result = new List<RrOtsPmTransDistribution>();
            //var accountsPayable = gpiAccounts.Single(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "P");
            //var pltPi = gpiAccounts.Single(a => a.account_number == "4246-10-40-40");
            //OTSTM2-16 GP distribution for positive adjustment
            //var positiveAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40");

            //OTSTM2-1124
            var accountsPayable = fiAccounts.FirstOrDefault(a => a.Name == "Processor Payable" && a.AdminFICategoryID == 4);
            var pltPi = fiAccounts.FirstOrDefault(a => a.Name == "PLT" && a.AdminFICategoryID == 4);
            var pltPiHST = fiAccounts.FirstOrDefault(a => a.Name == "PLT HST" && a.AdminFICategoryID == 4); //OTSTM2-1294
            //OTSTM2-16 GP distribution for positive adjustment
            var positiveAdjAcc = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 4);
            var positiveAdjAccHST = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 4); //OTSTM2-1294

            var HSTBase = 0.13m; //OTSTM2-1294

            foreach (var transaction in transactions)
            {
                var seqNum = 1;
                //OTSTM2-16 Ti:PTR, Pi: SPS + PIT Outbound, and positive adj
                if (transaction.DOCTYPE.ToString() == "1")
                {
                    var rowCredit = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)accountsPayable.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = accountsPayable.AccountNumber, //OTSTM2-1124
                        DEBITAMT = 0,
                        CRDTAMNT = transaction.DOCAMNT,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    result.Add(rowCredit);

                    var curClaimDetails = claimDetails.Where(i => i.ClaimId == transaction.ClaimId).ToList();
                    var totalEstWeight = curClaimDetails.Sum(i => i.EstimatedOffRoad + i.EstimatedOnRoad);
                    var claim = claims.Single(i => i.ID == transaction.ClaimId);
                    var totalTiAmount = claim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));

                    //var totalPiAmount = transaction.DOCAMNT - totalTiAmount;
                    //OTSTM2-16 separate the positive adjustment from Invoice Amount for separated distribution
                    var totalPiAmount = claim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS || c.PaymentType == (int)ClaimPaymentType.PITOutbound).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                    //var positivePaymentAdj = transaction.DOCAMNT - totalTiAmount - totalPiAmount;
                    var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claim.ID);
                    var positivePaymentAdj = claimPaymentAdjustments.Sum(c => c.AdjustmentAmount) > 0 ? Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero) : 0;
                    var includePi = totalPiAmount > 0;

                    var gpTireItem = FindTirePercentage(curClaimDetails, claim.ClaimPeriod.StartDate);
                    var isDataReceived = totalEstWeight > 0;
                    var isTaxApplicable = claim.IsTaxApplicable ?? false; //OTSTM2-1294
                    var prchTax1 = transaction.TAXAMNT ?? 0.00m; ; //OTSTM2-1294 to fix rounding difference for positive HST

                    if (isDataReceived)
                    {
                        this.AddTireTypes(transaction, gpTireItem, fiAccounts, totalTiAmount, totalPiAmount, positivePaymentAdj, includePi, INTERID, ref result, ref seqNum, isTaxApplicable, prchTax1); //OTSTM2-16 one more param "positivePaymentAdj"
                    }
                    //no data received
                    else
                    {
                        var preMonthEstWeight = 0;
                        var prevClaim = claimsRepository.GetPreviousApprovedClaim((int)ClaimType.Processor, claim.ClaimPeriod.StartDate, claim.ParticipantId);
                        var prevClaimId = prevClaim != null ? prevClaim.ID : -1;
                        var items = DataLoader.Items;
                        var prevClaimDetails = claimsRepository.LoadClaimDetailsForGP(new List<int> { prevClaimId }, new List<string> { "PTR" }, items);
                        var prevTotalEstWeight = prevClaimDetails != null ? prevClaimDetails.Sum(i => i.EstimatedOnRoad + i.EstimatedOffRoad) : 0;
                        isDataReceived = prevTotalEstWeight > 0;
                        //isTaxApplicable = prevClaim.IsTaxApplicable ?? false; //OTSTM2-1294
                        if (isDataReceived)
                        {
                            var prevTotalTiAmount = prevClaim.ClaimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR).Sum(c => Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
                            //var prevTotalPiAmount = transaction.DOCAMNT - totalTiAmount;
                            var prevGpTireItem = FindTirePercentage(prevClaimDetails, prevClaim.ClaimPeriod.StartDate);
                            //OTSTM2-1124
                            this.AddTireTypes(transaction, prevGpTireItem, fiAccounts, 0, totalPiAmount, positivePaymentAdj, includePi, INTERID, ref result, ref seqNum, isTaxApplicable, prchTax1);  //OTSTM2-16 one more param "positivePaymentAdj", pass the total PiAmount and positiveAdj of current claim
                        }
                        else
                        {
                            if (totalPiAmount > 0) //OTSTM2-16 to avoid a default distribution account with zero value in both Debit and Credit
                            {
                                var rowDebitPi = new RrOtsPmTransDistribution()
                                {
                                    VCHNUMWK = transaction.VCHNUMWK,
                                    DOCTYPE = transaction.DOCTYPE,
                                    VENDORID = transaction.VENDORID,
                                    DSTSQNUM = seqNum++,
                                    DISTTYPE = (int)pltPi.distribution_type_id,
                                    DistRef = null,
                                    ACTNUMST = pltPi.AccountNumber, //OTSTM2-1124
                                    DEBITAMT = totalPiAmount,
                                    CRDTAMNT = 0,
                                    USERDEF1 = transaction.USERDEF1,
                                    USERDEF2 = null,
                                    INTERID = INTERID,
                                    INTSTATUS = null,
                                    INTDATE = null,
                                    ERRORCODE = null,
                                    //NULL DEX_ROW_ID                        
                                };
                                result.Add(rowDebitPi);

                                //OTSTM2-1294
                                if (isTaxApplicable)
                                {
                                    var rowDebitPiHST = new RrOtsPmTransDistribution()
                                    {
                                        VCHNUMWK = transaction.VCHNUMWK,
                                        DOCTYPE = transaction.DOCTYPE,
                                        VENDORID = transaction.VENDORID,
                                        DSTSQNUM = seqNum++,
                                        DISTTYPE = (int)pltPiHST.distribution_type_id,
                                        DistRef = null,
                                        ACTNUMST = pltPiHST.AccountNumber,
                                        DEBITAMT = totalPiAmount * HSTBase,
                                        CRDTAMNT = 0,
                                        USERDEF1 = transaction.USERDEF1,
                                        USERDEF2 = null,
                                        INTERID = INTERID,
                                        INTSTATUS = null,
                                        INTDATE = null,
                                        ERRORCODE = null,
                                        //NULL DEX_ROW_ID                        
                                    };
                                    result.Add(rowDebitPiHST);
                                }
                            }


                            //OTSTM2-16 separate the positive adjustment from Invoice Amount for separated distribution
                            if (positivePaymentAdj > 0)
                            {
                                var rowPositiveAdj = new RrOtsPmTransDistribution()
                                {
                                    VCHNUMWK = transaction.VCHNUMWK,
                                    DOCTYPE = transaction.DOCTYPE,
                                    VENDORID = transaction.VENDORID,
                                    DSTSQNUM = seqNum++,
                                    DISTTYPE = (int)positiveAdjAcc.distribution_type_id,
                                    DistRef = null,
                                    ACTNUMST = positiveAdjAcc.AccountNumber, //OTSTM2-1124
                                    DEBITAMT = Math.Round(positivePaymentAdj, 2, MidpointRounding.AwayFromZero),
                                    CRDTAMNT = 0,
                                    USERDEF1 = transaction.USERDEF1,
                                    USERDEF2 = null,
                                    INTERID = INTERID,
                                    INTSTATUS = null,
                                    INTDATE = null,
                                    ERRORCODE = null,
                                    //NULL DEX_ROW_ID                        
                                };
                                result.Add(rowPositiveAdj);

                                //OTSTM2-1294
                                if (isTaxApplicable)
                                {
                                    var rowPositiveAdjHST = new RrOtsPmTransDistribution()
                                    {
                                        VCHNUMWK = transaction.VCHNUMWK,
                                        DOCTYPE = transaction.DOCTYPE,
                                        VENDORID = transaction.VENDORID,
                                        DSTSQNUM = seqNum++,
                                        DISTTYPE = (int)positiveAdjAccHST.distribution_type_id,
                                        DistRef = null,
                                        ACTNUMST = positiveAdjAccHST.AccountNumber,
                                        DEBITAMT = Math.Round(positivePaymentAdj * HSTBase, 2, MidpointRounding.AwayFromZero),
                                        CRDTAMNT = 0,
                                        USERDEF1 = transaction.USERDEF1,
                                        USERDEF2 = null,
                                        INTERID = INTERID,
                                        INTSTATUS = null,
                                        INTDATE = null,
                                        ERRORCODE = null,
                                        //NULL DEX_ROW_ID                        
                                    };
                                    result.Add(rowPositiveAdjHST);
                                }
                            }
                        }
                    }

                    //Fixed cent difference caused by rounding
                    //var VCHNUMWKs = result.Select(i => i.VCHNUMWK).ToList();
                    //foreach (var vchnumwk in VCHNUMWKs)
                    //{
                    //    var totalAmountDebit = result.Where(i => i.VCHNUMWK == vchnumwk).Sum(c => c.DEBITAMT);
                    //    var totalAmountCredit = result.Where(i => i.VCHNUMWK == vchnumwk).Sum(c => c.CRDTAMNT);
                    //    if (totalAmountDebit != totalAmountCredit)
                    //    {
                    //        var difference = (totalAmountDebit - totalAmountCredit);
                    //        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    //        var debitRows = result.Where(i => i.VCHNUMWK == vchnumwk && i.DISTTYPE == 6 && i.ACTNUMST != positiveAdjAcc.AccountNumber && i.ACTNUMST != positiveAdjAccHST.AccountNumber).ToList(); //OTSTM2-16 exclude the positiveAdj and OTSTM2-1294 the positiveAdjHST
                    //        var counter = 0;
                    //        while (counter < numberOfDifferenceDistribution)
                    //        {
                    //            debitRows[counter % debitRows.Count()].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                    //            counter++;
                    //        }
                    //    }
                    //}
                }
                //OTSTM2-16 DOR + Negative Adj
                if (transaction.DOCTYPE.ToString() == "5")
                {
                    var rowCredit = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)accountsPayable.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = accountsPayable.AccountNumber, //OTSTM2-1124
                        DEBITAMT = Math.Abs(transaction.DOCAMNT),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = null,
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    result.Add(rowCredit);

                    //OTSTM2-1294
                    var claim = claims.Single(i => i.ID == transaction.ClaimId);
                    var isTaxApplicable = claim.IsTaxApplicable ?? false;

                    this.AddTypeFiveDistributions(transaction, fiAccounts, INTERID, ref result, ref seqNum, isTaxApplicable); //OTSTM2-1124
                }

            }
            return result;
        }

        //OTSTM2-16 GP distribution for Negative Adjustment
        private void AddTypeFiveDistributions(RrOtsPmTransaction transaction, List<FIAccount> fiAccounts, string INTERID, ref List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, ref int seqNum, bool isTaxApplicable)
        {
            //var negativeAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40");
            var negativeAdjAcc = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 4);

            //OTSTM2-1294
            var negativeAdjAccHST = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 4);
            var HSTBase = 0.13m;

            var rowDebitTi = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = transaction.VCHNUMWK,
                DOCTYPE = transaction.DOCTYPE,
                VENDORID = transaction.VENDORID,
                DSTSQNUM = seqNum++,
                DISTTYPE = (int)negativeAdjAcc.distribution_type_id,
                DistRef = null,
                ACTNUMST = negativeAdjAcc.AccountNumber, //OTSTM2-1124
                DEBITAMT = 0,
                CRDTAMNT = Math.Abs(transaction.PRCHAMNT),
                USERDEF1 = transaction.USERDEF1,
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null,
                //NULL DEX_ROW_ID                        
            };
            rrOtsPmTransDistributions.Add(rowDebitTi);

            //OTSTM2-1294
            if (isTaxApplicable)
            {
                var rowDebitTiHST = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)negativeAdjAccHST.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = negativeAdjAccHST.AccountNumber,
                    DEBITAMT = 0,
                    CRDTAMNT = Math.Abs(transaction.TAXAMNT.Value),
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = null,
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowDebitTiHST);
            }
        }

        //OTSTM2-16 one more param "positivePaymentAdj"
        private void AddTireTypes(RrOtsPmTransaction transaction, GpTireItem gpTireItem, List<FIAccount> fiAccounts, decimal totalTiAmount, decimal totalPiAmount, decimal positivePaymentAdj, bool includePi, string INTERID, ref List<RrOtsPmTransDistribution> rrOtsPmTransDistributions, ref int seqNum, bool isTaxApplicable, decimal prchTax1)
        {         
            //OTSTM2-1124
            var pltTi = fiAccounts.FirstOrDefault(a => a.Name == "PLT Stabilization" && a.AdminFICategoryID == 4);
            var pltPi = fiAccounts.FirstOrDefault(a => a.Name == "PLT" && a.AdminFICategoryID == 4);
            var mtTi = fiAccounts.FirstOrDefault(a => a.Name == "MT Stabilization" && a.AdminFICategoryID == 4);
            var mtPi = fiAccounts.FirstOrDefault(a => a.Name == "MT" && a.AdminFICategoryID == 4);
            var aglsTi = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS Stabilization" && a.AdminFICategoryID == 4);
            var aglsPi = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS" && a.AdminFICategoryID == 4);
            var indTi = fiAccounts.FirstOrDefault(a => a.Name == "IND Stabilization" && a.AdminFICategoryID == 4);
            var indPi = fiAccounts.FirstOrDefault(a => a.Name == "IND" && a.AdminFICategoryID == 4);
            var sotrTi = fiAccounts.FirstOrDefault(a => a.Name == "SOTR Stabilization" && a.AdminFICategoryID == 4);
            var sotrPi = fiAccounts.FirstOrDefault(a => a.Name == "SOTR" && a.AdminFICategoryID == 4);
            var motrTi = fiAccounts.FirstOrDefault(a => a.Name == "MOTR Stabilization" && a.AdminFICategoryID == 4);
            var motrPi = fiAccounts.FirstOrDefault(a => a.Name == "MOTR" && a.AdminFICategoryID == 4);
            var lotrTi = fiAccounts.FirstOrDefault(a => a.Name == "LOTR Stabilization" && a.AdminFICategoryID == 4);
            var lotrPi = fiAccounts.FirstOrDefault(a => a.Name == "LOTR" && a.AdminFICategoryID == 4);
            var gotrTi = fiAccounts.FirstOrDefault(a => a.Name == "GOTR Stabilization" && a.AdminFICategoryID == 4);
            var gotrPi = fiAccounts.FirstOrDefault(a => a.Name == "GOTR" && a.AdminFICategoryID == 4);

            //OTSTM2-1294
            var pltTiHST = fiAccounts.FirstOrDefault(a => a.Name == "PLT HST" && a.AdminFICategoryID == 3);
            var pltPiHST = fiAccounts.FirstOrDefault(a => a.Name == "PLT HST" && a.AdminFICategoryID == 4);
            var mtTiHST = fiAccounts.FirstOrDefault(a => a.Name == "MT HST" && a.AdminFICategoryID == 3);
            var mtPiHST = fiAccounts.FirstOrDefault(a => a.Name == "MT HST" && a.AdminFICategoryID == 4);
            var aglsTiHST = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS HST" && a.AdminFICategoryID == 3);
            var aglsPiHST = fiAccounts.FirstOrDefault(a => a.Name == "AG/LS HST" && a.AdminFICategoryID == 4);
            var indTiHST = fiAccounts.FirstOrDefault(a => a.Name == "IND HST" && a.AdminFICategoryID == 3);
            var indPiHST = fiAccounts.FirstOrDefault(a => a.Name == "IND HST" && a.AdminFICategoryID == 4);
            var sotrTiHST = fiAccounts.FirstOrDefault(a => a.Name == "SOTR HST" && a.AdminFICategoryID == 3);
            var sotrPiHST = fiAccounts.FirstOrDefault(a => a.Name == "SOTR HST" && a.AdminFICategoryID == 4);
            var motrTiHST = fiAccounts.FirstOrDefault(a => a.Name == "MOTR HST" && a.AdminFICategoryID == 3);
            var motrPiHST = fiAccounts.FirstOrDefault(a => a.Name == "MOTR HST" && a.AdminFICategoryID == 4);
            var lotrTiHST = fiAccounts.FirstOrDefault(a => a.Name == "LOTR HST" && a.AdminFICategoryID == 3);
            var lotrPiHST = fiAccounts.FirstOrDefault(a => a.Name == "LOTR HST" && a.AdminFICategoryID == 4);
            var gotrTiHST = fiAccounts.FirstOrDefault(a => a.Name == "GOTR HST" && a.AdminFICategoryID == 3);
            var gotrPiHST = fiAccounts.FirstOrDefault(a => a.Name == "GOTR HST" && a.AdminFICategoryID == 4);

            //OTSTM2-16 GP distribution for positive adjustment
            //var positiveAdjAcc = gpiAccounts.Single(a => a.account_number == "4365-90-40-40");
            var positiveAdjAcc = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 4); //OTSTM2-1124

            //OTSTM2-1294
            var positiveAdjAccHST = fiAccounts.FirstOrDefault(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 4);
            var HSTBase = 0.13m;

            if (positivePaymentAdj > 0)
            {
                var rowPositiveAdj = new RrOtsPmTransDistribution()
                {
                    VCHNUMWK = transaction.VCHNUMWK,
                    DOCTYPE = transaction.DOCTYPE,
                    VENDORID = transaction.VENDORID,
                    DSTSQNUM = seqNum++,
                    DISTTYPE = (int)positiveAdjAcc.distribution_type_id,
                    DistRef = null,
                    ACTNUMST = positiveAdjAcc.AccountNumber, //OTSTM2-1124
                    DEBITAMT = Math.Round(positivePaymentAdj, 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    USERDEF1 = transaction.USERDEF1,
                    USERDEF2 = "PADJ", //temporary flag for rounding calculation, will be removed at the end of the method
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    //NULL DEX_ROW_ID                        
                };
                rrOtsPmTransDistributions.Add(rowPositiveAdj);

                //OTSTM2-1294
                if (isTaxApplicable)
                {
                    var rowPositiveAdjHST = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)positiveAdjAccHST.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = positiveAdjAccHST.AccountNumber,
                        DEBITAMT = Math.Round(positivePaymentAdj * HSTBase, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PADJ HST", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowPositiveAdjHST);
                }
            }

            if (totalTiAmount > 0)
            {
                if (gpTireItem.PLTPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)pltTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = pltTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.MTPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)mtTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = mtTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.AGLSPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)aglsTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = aglsTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.INDPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)indTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = indTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.SOTRPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)sotrTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = sotrTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.MOTRPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)motrTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = motrTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.LOTRPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)lotrTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = lotrTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                if (gpTireItem.GOTRPercentage > 0)
                {
                    var rowDebitTi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)gotrTi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = gotrTi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalTiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "TI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitTi);
                }

                //Fixed cent difference caused by rounding
                var totalTiAmountTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI").Sum(c => c.DEBITAMT);
                var TiDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI").Select(c => c).ToList();
                if (totalTiAmountTmp != totalTiAmount)
                {
                    var difference = (totalTiAmountTmp - totalTiAmount);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < TiDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            TiDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }

                if (isTaxApplicable)
                {
                    if (gpTireItem.PLTPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)pltTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = pltTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.MTPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)mtTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = mtTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.AGLSPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)aglsTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = aglsTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.INDPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)indTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = indTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.SOTRPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)sotrTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = sotrTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.MOTRPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)motrTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = motrTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.LOTRPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)lotrTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = lotrTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    if (gpTireItem.GOTRPercentage > 0)
                    {
                        var rowDebitTiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)gotrTiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = gotrTiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "TI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitTiHST);
                    }

                    //Fixed cent difference caused by rounding
                    var totalTiAmountHst = Math.Round(totalTiAmount * HSTBase, 2, MidpointRounding.AwayFromZero);
                    var totalTiAmountHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Sum(c => c.DEBITAMT);
                    var TiHstDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Select(c => c).ToList();
                    if (totalTiAmountHstTmp != totalTiAmountHst)
                    {
                        var difference = (totalTiAmountHstTmp - totalTiAmountHst);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < TiHstDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                TiHstDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                }
            }

            if (includePi)
            {
                if (gpTireItem.PLTPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)pltPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = pltPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.MTPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)mtPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = mtPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.AGLSPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)aglsPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = aglsPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.INDPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)indPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = indPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.SOTRPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)sotrPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = sotrPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.MOTRPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)motrPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = motrPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.LOTRPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)lotrPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = lotrPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                if (gpTireItem.GOTRPercentage > 0)
                {
                    var rowDebitPi = new RrOtsPmTransDistribution()
                    {
                        VCHNUMWK = transaction.VCHNUMWK,
                        DOCTYPE = transaction.DOCTYPE,
                        VENDORID = transaction.VENDORID,
                        DSTSQNUM = seqNum++,
                        DISTTYPE = (int)gotrPi.distribution_type_id,
                        DistRef = null,
                        ACTNUMST = gotrPi.AccountNumber,
                        DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalPiAmount, 2, MidpointRounding.AwayFromZero),
                        CRDTAMNT = 0,
                        USERDEF1 = transaction.USERDEF1,
                        USERDEF2 = "PI", //temporary flag for rounding calculation, will be removed at the end of the method
                        INTERID = INTERID,
                        INTSTATUS = null,
                        INTDATE = null,
                        ERRORCODE = null,
                        //NULL DEX_ROW_ID                        
                    };
                    rrOtsPmTransDistributions.Add(rowDebitPi);
                }

                //Fixed cent difference caused by rounding
                var totalPiAmountTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI").Sum(c => c.DEBITAMT);
                var PiDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI").Select(c => c).ToList();
                if (totalPiAmountTmp != totalPiAmount)
                {
                    var difference = (totalPiAmountTmp - totalPiAmount);
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < PiDistributions.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            PiDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }

                if (isTaxApplicable)
                {
                    if (gpTireItem.PLTPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)pltPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = pltPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.PLTPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.MTPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)mtPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = mtPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.MTPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.AGLSPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)aglsPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = aglsPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.AGLSPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.INDPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)indPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = indPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.INDPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.SOTRPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)sotrPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = sotrPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.SOTRPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.MOTRPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)motrPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = motrPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.MOTRPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.LOTRPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)lotrPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = lotrPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.LOTRPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    if (gpTireItem.GOTRPercentage > 0)
                    {
                        var rowDebitPiHST = new RrOtsPmTransDistribution()
                        {
                            VCHNUMWK = transaction.VCHNUMWK,
                            DOCTYPE = transaction.DOCTYPE,
                            VENDORID = transaction.VENDORID,
                            DSTSQNUM = seqNum++,
                            DISTTYPE = (int)gotrPiHST.distribution_type_id,
                            DistRef = null,
                            ACTNUMST = gotrPiHST.AccountNumber,
                            DEBITAMT = Math.Round(gpTireItem.GOTRPercentage * totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero),
                            CRDTAMNT = 0,
                            USERDEF1 = transaction.USERDEF1,
                            USERDEF2 = "PI HST", //temporary flag for rounding calculation, will be removed at the end of the method
                            INTERID = INTERID,
                            INTSTATUS = null,
                            INTDATE = null,
                            ERRORCODE = null,
                            //NULL DEX_ROW_ID                        
                        };
                        rrOtsPmTransDistributions.Add(rowDebitPiHST);
                    }

                    //Fixed cent difference caused by rounding
                    var totalPiAmountHst = Math.Round(totalPiAmount * HSTBase, 2, MidpointRounding.AwayFromZero);
                    var totalPiAmountHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Sum(c => c.DEBITAMT);
                    var PiHstDistributions = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Select(c => c).ToList();
                    if (totalPiAmountHstTmp != totalPiAmountHst)
                    {
                        var difference = (totalPiAmountHstTmp - totalPiAmountHst);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < PiHstDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                PiHstDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                }
            }

            //HST rounding one more time, to fix HST 0.01 issue
            if (isTaxApplicable && prchTax1 > 0)
            {
                //var totalHst = positivePaymentAdj >= 0 ? Math.Round((totalTiAmount + totalPiAmount + positivePaymentAdj) * HSTBase, 2, MidpointRounding.AwayFromZero) : Math.Round((totalTiAmount + totalPiAmount) * HSTBase, 2, MidpointRounding.AwayFromZero);
                var totalHst = prchTax1;
                var totalHstTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST" || c.USERDEF2 == "PI HST" || c.USERDEF2 == "PADJ HST").Sum(c => c.DEBITAMT);
                var hstDistributionsTmp = new List<RrOtsPmTransDistribution>();
                if (totalTiAmount > 0)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "TI HST").Select(c => c).ToList();
                }
                else if (includePi)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PI HST").Select(c => c).ToList();
                }
                else if (positivePaymentAdj > 0)
                {
                    hstDistributionsTmp = rrOtsPmTransDistributions.Where(c => c.USERDEF2 == "PADJ HST").Select(c => c).ToList();
                }

                if (totalHstTmp != totalHst && hstDistributionsTmp.Count > 0)
                {
                    var difference = totalHstTmp - totalHst;
                    var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                    if (numberOfDifferenceDistribution < hstDistributionsTmp.Count)
                    {
                        for (var i = 0; i < numberOfDifferenceDistribution; i++)
                        {
                            hstDistributionsTmp[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                        }
                    }
                }
            }
            

            //Remove the temporary flag for rounding calculation here
            rrOtsPmTransDistributions.ForEach(c => c.USERDEF2 = null);
        }
        private GpTireItem FindTirePercentage(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private ProcessorSubmitClaimViewModel ProcessorClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            BusinessRuleSet.ClearBusinessRules();

            BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);

            return submitClaimModel;

        }
        private void ExecuteBusinessRuleForErrors(ProcessorSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }
        private ProcessorSubmitClaimViewModel ProcessorClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, ProcessorSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new NilActivityClaimRule());
            this.BusinessRuleSet.AddBusinessRule(new NegativeClosingInventoryRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(
                    BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }
        private void LoadStaffClaimSummary(ProcessorClaimSummaryModel processorClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            processorClaimSummary.ClaimStatus = claimStatus;
            processorClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy)
                ? string.Empty
                : string.Format("Under Review by {0}", claimStatus.ReviewedBy);

            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);
        }


        private void PopulatPaymentAdjustments(ProcessorPaymentViewModel processorPayment,
            ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments,
            List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            processorPayment.PTR =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PTR)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.SPS =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.SPS)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.PITOutbound =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.PITOutbound)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));
            processorPayment.DOR =
                claimPayments.Where(c => c.PaymentType == (int)ClaimPaymentType.DOR)
                    .Sum(c => processorPayment.UsingOldRounding ? Math.Round(c.Rate * c.Weight, 2) : Math.Round(c.Rate * c.Weight, 2, MidpointRounding.AwayFromZero));

            processorPayment.PaymentAdjustment = Math.Round(claimPaymentAdjustments.Sum(c => c.AdjustmentAmount), 2, MidpointRounding.AwayFromZero);
            processorPayment.isStaff = SecurityContextHelper.IsStaff();


            if (claimPaymentSummay != null)
            {
                processorPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                processorPayment.PaidDate = claimPaymentSummay.PaidDate;
                processorPayment.MailedDate = claimPaymentSummay.MailDate;
                processorPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                processorPayment.BatchNumber = claimPaymentSummay.BatchNumber;
            }
        }

        private void PopulateInventoryAdjustment(ProcessorClaimSummaryModel processorClaimSummary,
            List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            var query = inventoryAdjustments.GroupBy(c => new { c.Direction }).Select(c => new
            {
                Name = c.Key,
                Adjustmment = c.Sum(i => i.AdjustmentWeightOnroad + i.AdjustmentWeightOffroad)
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.Direction == 1)
                {
                    processorClaimSummary.AdjustIn = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

                if (c.Name.Direction == 2)
                {
                    processorClaimSummary.AdjustOut = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

                if (c.Name.Direction == 0)
                {
                    processorClaimSummary.OverallAdjustments = DataConversion.ConvertKgToTon((double)c.Adjustmment);
                }

            });
        }

        private void PopulateInboundOutbound(ProcessorClaimSummaryModel processorClaimSummary,
            List<ClaimDetailViewModel> claimDetails)
        {
            var spslist = claimDetails.Where(c => c.TransactionType == "SPS").ToList();
            var query = claimDetails
                .GroupBy(c => new { c.TransactionType, c.Direction })
                .Select(
                    c =>
                        new
                        {
                            Name = c.Key,
                            ActualWeight = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)(i.OnRoad + i.OffRoad)), 4, MidpointRounding.AwayFromZero)),
                            extraDOR = c.Sum(i => Math.Round(DataConversion.ConvertKgToTon((double)i.ExtraDORTotal), 4, MidpointRounding.AwayFromZero))
                        });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && c.Name.Direction)
                {
                    processorClaimSummary.PTR = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && c.Name.Direction)
                {
                    processorClaimSummary.PIT = c.ActualWeight;
                }

                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.SPS && !c.Name.Direction)
                {
                    processorClaimSummary.SPS = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.PIT && !c.Name.Direction)
                {
                    processorClaimSummary.PITOut = c.ActualWeight;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOR && !c.Name.Direction)
                {
                    processorClaimSummary.DOR = c.ActualWeight + c.extraDOR;
                }

            });
        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = currentClaimSummary.TotalOpeningEstimated != null ? (decimal)currentClaimSummary.TotalOpeningEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = currentClaimSummary.TotalOpening != null ? (decimal)currentClaimSummary.TotalOpening : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);
                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalOpeningEstimated = previousClaimSummary.TotalClosingEstimated != null ? (decimal)previousClaimSummary.TotalClosingEstimated : 0;
                    inventoryOpeningSummary.TotalOpening = previousClaimSummary.TotalClosing != null ? (decimal)previousClaimSummary.TotalClosing : 0;
                }
            }
            return inventoryOpeningSummary;
        }

        #endregion
    }
}
