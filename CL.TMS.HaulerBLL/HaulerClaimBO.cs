﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.GroupRate;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace CL.TMS.HaulerBLL
{
    public class HaulerClaimBO : BaseBO
    {

        private IClaimsRepository claimsRepository;
        private ITransactionRepository transactionRepository;
        private IVendorRepository vendorRepository;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;
        private readonly IConfigurationsRepository configurationsRepository;

        public HaulerClaimBO(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, IVendorRepository vendorRepository, ISettingRepository settingRepository, IConfigurationsRepository configurationsRepository)
        {
            this.claimsRepository = claimsRepository;
            this.transactionRepository = transactionRepository;
            this.gpRepository = gpRepository;
            this.vendorRepository = vendorRepository;
            this.settingRepository = settingRepository;
            this.configurationsRepository = configurationsRepository;
        }

        #region public methods
        public HaulerClaimSummaryModel LoadHaulerClaimSummary(int vendorId, int claimId)
        {
            var haulerClaimSummary = new HaulerClaimSummaryModel();

            //Load status
            var claim = claimsRepository.GetClaimWithSummaryPeriod(claimId);
            var status = EnumHelper.GetValueFromDescription<ClaimStatus>(claim.Status);
            haulerClaimSummary.ClaimCommonModel.Status = status;

            //Load claim calculation time
            if (claim.ClaimSummary != null)
                haulerClaimSummary.ClaimCommonModel.ClaimLastCalculationTime = claim.ClaimSummary.UpdatedDate;
            else
                haulerClaimSummary.ClaimCommonModel.ClaimLastCalculationTime = DateTime.UtcNow;

            //Load ClaimPeriod
            haulerClaimSummary.ClaimCommonModel.ClaimPeriod = claim.ClaimPeriod;
            if (haulerClaimSummary.ClaimCommonModel.ClaimPeriod.SubmitStart.Date > DateTime.Now.Date)
                haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;

            //Load Inventory Opening data
            var inventoryOpeningResult = LoadInventoryOpeningResult(claim, claimsRepository);
            haulerClaimSummary.TotalEligibleOpeningOnRoad = inventoryOpeningResult.TotalEligibleOpeningOnRoadEstimated;
            haulerClaimSummary.TotalEligibleOpeningOffRoad = inventoryOpeningResult.TotalEligibleOpeningOffRoadEstimated;
            haulerClaimSummary.TotalIneligibleOpeningOnRoad = inventoryOpeningResult.TotalIneligibleOpeningOnRoadEstimated;
            haulerClaimSummary.TotalIneligibleOpeningOffRoad = inventoryOpeningResult.TotalIneligibleOpeningOffRoadEstimated;

            //Load Inbound and Outbound Transactions
            var items = DataLoader.Items;
            var claimDetails = claimsRepository.LoadClaimDetails(claimId, items);
            PopulateInboundOutbound(haulerClaimSummary, claimDetails);

            //Populate WeightVariace
            PopulateWeightVariance(haulerClaimSummary, claimDetails);

            //Load Inventory Adjustment
            var inventoryAdjustments = claimsRepository.LoadClaimInventoryAdjustments(claimId);
            PopulateInventoryAdjustment(haulerClaimSummary, inventoryAdjustments);

            //Load yard count submission
            var claimYardCountSubmissions = claimsRepository.LoadYardCountSubmission(claimId);
            PopulateClaimYardCountSubmission(haulerClaimSummary, claimYardCountSubmissions);

            //Load sort yard capacity
            haulerClaimSummary.InventoryAdjustment.SortYandCapacity = claimsRepository.LoadVendorSortYardCapacity(vendorId);

            if (SecurityContextHelper.IsStaff())
            {
                //Load Staff Summary Info
                LoadStaffClaimSummary(haulerClaimSummary, claimId, vendorId, haulerClaimSummary.ClaimCommonModel.ClaimPeriod);

                if (status == ClaimStatus.Open)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = "Open";
                }
                if (status == ClaimStatus.Submitted)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = string.Format("Submitted by {0}", claim.SubmittedUser.FirstName + " " + claim.SubmittedUser.LastName);
                }

                if (status == ClaimStatus.Approved)
                {
                    haulerClaimSummary.ClaimStatus.UnderReview = "Approved";
                }
                haulerClaimSummary.ClaimStatus.isPreviousClaimOnHold = claimsRepository.IsPreviousClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate); //OTSTM2-499

                haulerClaimSummary.ClaimStatus.isPreviousAuditOnHold = claimsRepository.IsPreviousAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                haulerClaimSummary.ClaimStatus.isFutureClaimOnHold = claimsRepository.IsFutureClaimOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);

                haulerClaimSummary.ClaimStatus.isFutureAuditOnHold = claimsRepository.IsFutureAuditOnHold(claim.ParticipantId, claim.ClaimPeriod.StartDate);
            }

            ApplyUserSecurity(status, haulerClaimSummary);

            return haulerClaimSummary;
        }

        private void ApplyUserSecurity(ClaimStatus status, HaulerClaimSummaryModel haulerClaimSummary)
        {
            if (SecurityContextHelper.IsStaff())
            {
                if ((status == ClaimStatus.Approved) || (status == ClaimStatus.ReceivePayment) || (status == ClaimStatus.Open))
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    haulerClaimSummary.ClaimStatus.IsClaimReadonly = true;
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                }
                else
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = false;
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary) == SecurityResultType.EditSave);
                    if (!isEditable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff = true;
                    }
                    haulerClaimSummary.ClaimStatus.IsClaimReadonly = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummaryStatus) != SecurityResultType.EditSave);
                }
            }
            else
            {
                if (status != ClaimStatus.Open)
                {
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                }
                else
                {
                    var isEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary) == SecurityResultType.EditSave);
                    var isSubmitable = SecurityContextHelper.HasSubmitPermission;
                    if (!isEditable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant = true;
                    }
                    if (!isSubmitable)
                    {
                        haulerClaimSummary.ClaimCommonModel.ClaimSummarySecurity.SubmitButtonDisabled = true;
                    }
                }
            }
        }

        private InventoryOpeningSummary LoadInventoryOpeningResult(Claim claim, IClaimsRepository claimsRepository)
        {
            var claimPeriodDate = claim.ClaimPeriod.StartDate;
            var inventoryOpeningSummary = new InventoryOpeningSummary();
            if ((claim.Status == "Approved") || (claim.Status == "Receive Payment"))
            {
                var currentClaimSummary = claim.ClaimSummary;
                if (currentClaimSummary != null)
                {
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = currentClaimSummary.EligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = currentClaimSummary.EligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.EligibleOpeningOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = currentClaimSummary.IneligibleOpeningOnRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = currentClaimSummary.IneligibleOpeningOffRoadEstimated != null ? (decimal)currentClaimSummary.IneligibleOpeningOffRoadEstimated : 0;
                }
            }
            else
            {
                var previousClaimSummary = claimsRepository.GetPreviousClaimSummary(claim.ParticipantId, claimPeriodDate);

                if (previousClaimSummary != null)
                {
                    //Set current claim opening based on previous claim summary
                    inventoryOpeningSummary.TotalEligibleOpeningOnRoadEstimated = previousClaimSummary.EligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalEligibleOpeningOffRoadEstimated = previousClaimSummary.EligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.EligibleClosingOffRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOnRoadEstimated = previousClaimSummary.IneligibleClosingOnRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOnRoadEstimated : 0;
                    inventoryOpeningSummary.TotalIneligibleOpeningOffRoadEstimated = previousClaimSummary.IneligibleClosingOffRoadEstimated != null ? (decimal)previousClaimSummary.IneligibleClosingOffRoadEstimated : 0;
                }
            }
            return inventoryOpeningSummary;
        }
        private void LoadStaffClaimSummary(HaulerClaimSummaryModel haulerClaimSummary, int claimId, int vendorId, Period claimPeriod)
        {
            //Load Claim Status
            var claimStatus = claimsRepository.LoadClaimStatusViewModel(claimId);
            haulerClaimSummary.ClaimStatus = claimStatus;
            haulerClaimSummary.ClaimStatus.UnderReview = string.IsNullOrWhiteSpace(claimStatus.ReviewedBy) ? string.Empty : string.Format("Under Review by {0}", claimStatus.ReviewedBy);
            //Initialize claim review start date if necessary
            SetClaimReviewStartDate(claimStatus);

            //Load TireCountBalance
            var inventoryItems = claimsRepository.LoadClaimInventoryItems(vendorId, claimPeriod.EndDate);

            PopulateClaimTireBalance(haulerClaimSummary, inventoryItems);

            PopulateClaimTireBalanceWithoutAdj(haulerClaimSummary, inventoryItems);
        }

        public void SetClaimReviewStartDate(ClaimStatusViewModel claimStatus)
        {
            //Started----> Date on which the claim review started (first time the claim rep opens an assigned claim for review)
            if ((claimStatus.Started == null) && (claimStatus.AssignToUserId == SecurityContextHelper.CurrentUser.Id))
            {
                var reviewStartDate = DateTime.UtcNow;
                claimStatus.Started = reviewStartDate;
                claimsRepository.InitializeReviewStartDate(claimStatus.ClaimId, reviewStartDate);
            }
        }
        public HaulerPaymentViewModel LoadHaulerPayment(int claimId)
        {
            var haulerPayment = new HaulerPaymentViewModel();

            //OTSTM2-1139 No talk back needed
            //UpdateGpBatchForClaim(claimId);

            var claimPaymentSummay = claimsRepository.LoadClaimPaymentSummary(claimId);
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var claim = claimsRepository.FindClaimByClaimId(claimId);
            //OTSTM2-1215
            var claimPaymentDetails = claimsRepository.LoadClaimPaymentDetails(claimId);
            var stcTransactionBriefList = claimsRepository.GetStcTransactionBriefListByClaimId(claimId);

            //Fixed 613 rounding issue
            if (claim.ApprovalDate != null)
            {
                //For approved claims
                var roundingEffectiveDate = Convert.ToDateTime(ConfigurationManager.AppSettings["RoundingEffectiveDate"]);
                if (claim.ApprovalDate < roundingEffectiveDate.Date)
                {
                    haulerPayment.UsingOldRounding = true;
                }
            }

            PopulatPaymentAdjustments(haulerPayment, claimPaymentSummay, claimPayments, claimPaymentAdjustments);
            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                haulerPayment.HSTBase = 0.00m;
            }

            var claimPeriodDate = claim.ClaimPeriod.StartDate;

            //Rate Group Rate Changes for 3 popup
            var rateGroupRates = configurationsRepository.LoadRateGroupRates(claim.ClaimPeriod.StartDate, claim.ClaimPeriod.EndDate);
            var vendorGroups = configurationsRepository.LoadVendorGroups();
            //OTSTM2-1215 get STC event list by transacion id list
            var stcEventBriefList = configurationsRepository.LoadStcEventBriefList(stcTransactionBriefList);

            PopulateTransportationPremiumDetails(haulerPayment, claimPayments, rateGroupRates, vendorGroups);

            PopulateDOTPremiumDetails(haulerPayment, claimPayments, rateGroupRates, vendorGroups);

            //OTSTM2-1215 
            PopulateStcPremiumDetails(haulerPayment, claimPaymentDetails, stcEventBriefList);

            PopulateClawbackDetails(haulerPayment, claimPayments, rateGroupRates, vendorGroups);

            //haulerPayment.PaymentDueDate = claim.ChequeDueDate + DateTime.UtcNow.Date.Subtract(((DateTime)claim.ClaimOnHoldDate).Date);
            haulerPayment.PaymentDueDate = claim.ChequeDueDate; //OTSTM2-194

            return haulerPayment;
        }

        public List<InventoryItem> LoadInventoryItems(int vendorId, DateTime periodEndDate)
        {
            var inventoryItems = claimsRepository.LoadClaimInventoryItems(vendorId, periodEndDate);
            return inventoryItems;
        }

        //OTSTM2-1139 No talk back needed
        //private void UpdateGpBatchForClaim(int claimId)
        //{
        //    var needPullBatch = claimsRepository.IsClaimPosted(claimId);
        //    if (needPullBatch)
        //    {
        //        var gpTransactionNumber = gpRepository.GetTransactionNumber(claimId);
        //        if (!string.IsNullOrWhiteSpace(gpTransactionNumber))
        //        {
        //            var INTERID = AppSettings.Instance.GetSettingValue("GP:INTERID");
        //            var gpPaymentSummary = gpStagingRepository.GetPaymentSummary(gpTransactionNumber, INTERID);
        //            if (!string.IsNullOrWhiteSpace(gpPaymentSummary.ChequeNumber))
        //            {
        //                claimsRepository.UpdateClaimPaymentSummary(gpPaymentSummary, claimId);
        //            }
        //        }
        //    }
        //}

        #region submit claim rules
        public HaulerSubmitClaimViewModel HaulerClaimSubmitBusinessRule(HaulerSubmitClaimViewModel submitClaimModel)
        {
            var claim = claimsRepository.FindClaimByClaimId(submitClaimModel.claimId);

            ConditionCheck.Null(claim, "claim");

            var submitClaimRuleSetStrategy = SubmitClaimRuleStrategyFactory.LoadSubmitClaimRuleStrategy();

            CreateBusinessRuleSet(submitClaimRuleSetStrategy);

            var ruleContext = new Dictionary<string, object>{
                {"claimRepository", claimsRepository},
                {"transactionRepository", transactionRepository},
                {"submitClaimModel", submitClaimModel}
            };

            var updatedSubmitClaimForError = HaulerClaimSubmitErrors(claim, ruleContext, submitClaimModel);
            if (updatedSubmitClaimForError.Errors.Count > 0)
            {
                return updatedSubmitClaimForError;
            }

            var updatedSubmitClaimForWarning = HaulerClaimSubmitWarnings(claim, ruleContext, submitClaimModel);

            return updatedSubmitClaimForWarning;
        }

        private HaulerSubmitClaimViewModel HaulerClaimSubmitErrors(Claim claim, IDictionary<string, object> ruleContext, HaulerSubmitClaimViewModel submitClaimModel)
        {
            var error = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new PreviousClaimStillOpenRule());
            this.BusinessRuleSet.AddBusinessRule(new IncompleteAndSyncedRule());
            this.BusinessRuleSet.AddBusinessRule(new PendingStatusRule());

            ExecuteBusinessRuleForErrors(submitClaimModel, claim, ruleContext);
            return submitClaimModel;

        }

        private void ExecuteBusinessRuleForErrors(HaulerSubmitClaimViewModel submitClaimModel, Claim claim, IDictionary<string, object> ruleContext)
        {
            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Errors.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }
        }

        private HaulerSubmitClaimViewModel HaulerClaimSubmitWarnings(Claim claim, IDictionary<string, object> ruleContext, HaulerSubmitClaimViewModel submitClaimModel)
        {
            var warnings = new List<string>();

            this.BusinessRuleSet.ClearBusinessRules();

            this.BusinessRuleSet.AddBusinessRule(new HaulerNilActivityClaimsRule());
            this.BusinessRuleSet.AddBusinessRule(new HaulerNegativeClosingInventoryRule());
            this.BusinessRuleSet.AddBusinessRule(new LateClaimRule());

            ExecuteBusinessRules(claim, ruleContext);

            if (!BusinessRuleExecutioResult.IsValid)
            {
                submitClaimModel.Warnings.AddRange(BusinessRuleExecutioResult.Errors.Select(c => c.ErrorMessage).ToList());
            }

            return submitClaimModel;
        }

        #endregion

        #endregion

        #region private methods
        private void PopulateClawbackDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            haulerPayment.ClawbackPaymentViewModel = new ClawbackPaymentViewModel();

            var deliveryGroupIds = rateGroupRates.Where(c => c.PaymentType == 3).Select(c => c.ProcessorGroupId).Distinct().ToList();
            var paymentQuery = claimPayments.Where(c => c.PaymentType == 3).ToList();

            //Build empty view model with default rate, populate the data from claimpayments 
            haulerPayment.ClawbackPaymentViewModel.RowDatas = new List<ClawbackPaymentItem>();

            deliveryGroupIds.ForEach(c =>
            {
                var rowItem = new ClawbackPaymentItem();
                var deliveryGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)c).GroupName;
                rowItem.DeliveryGroupId = (int)c;
                rowItem.DeliveryGroupName = deliveryGroupName;
                var onRoadGroupRate = rateGroupRates.Where(r => r.ProcessorGroupId == c && r.PaymentType == 3 && r.ItemType == 1).FirstOrDefault();
                var onRoadRate = onRoadGroupRate != null ? onRoadGroupRate.Rate : 0m;
                var offRoadGroupRate = rateGroupRates.Where(r => r.ProcessorGroupId == c && r.PaymentType == 3 && r.ItemType == 2).FirstOrDefault();
                var offRoadRate = offRoadGroupRate != null ? offRoadGroupRate.Rate : 0m;

                rowItem.OnRoadRate = onRoadRate;
                rowItem.OffRoadRate = offRoadRate;

                var rowOnRoadPayment = paymentQuery.Where(p => p.PaymentType == 3 && p.ItemType == 1 && p.DeliveryZone == deliveryGroupName).FirstOrDefault();
                if (rowOnRoadPayment != null)
                {
                    rowItem.OnRoadWeight = rowOnRoadPayment.Weight;
                    rowItem.OnRoadRate = rowOnRoadPayment.Rate;
                    rowItem.OnRoadPremium = Math.Round(rowOnRoadPayment.Weight * rowOnRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    rowItem.OnRoadWeight = 0;
                    rowItem.OnRoadPremium = 0;
                }

                var rowOffRoadPayment = paymentQuery.Where(p => p.PaymentType == 3 && p.ItemType == 2 && p.DeliveryZone == deliveryGroupName).FirstOrDefault();
                if (rowOffRoadPayment != null)
                {
                    rowItem.OffRoadWeight = rowOffRoadPayment.Weight;
                    rowItem.OffRoadRate = rowOffRoadPayment.Rate;
                    rowItem.OffRoadPremium = Math.Round(rowOffRoadPayment.Weight * rowOffRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    rowItem.OffRoadWeight = 0;
                    rowItem.OffRoadPremium = 0;
                }
                haulerPayment.ClawbackPaymentViewModel.RowDatas.Add(rowItem);
            });

            haulerPayment.ClawbackPaymentViewModel.TotalOnRoadPreminum = haulerPayment.ClawbackPaymentViewModel.RowDatas.Sum(c => c.OnRoadPremium);
            haulerPayment.ClawbackPaymentViewModel.TotalOffRoadPremium = haulerPayment.ClawbackPaymentViewModel.RowDatas.Sum(c => c.OffRoadPremium);
            haulerPayment.ClawbackPaymentViewModel.TotalPremium = haulerPayment.ClawbackPaymentViewModel.TotalOnRoadPreminum + haulerPayment.ClawbackPaymentViewModel.TotalOffRoadPremium;
        }
        private void PopulateDOTPremiumDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            haulerPayment.DOTPaymentViewModel = new DOTPaymentViewModel();

            var pickupGroupIds = rateGroupRates.Where(c => c.PaymentType == 2).Select(c => c.CollectorGroupId).Distinct().ToList();
            var deliveryGroupIds = rateGroupRates.Where(c => c.PaymentType == 2).Select(c => c.ProcessorGroupId).Distinct().ToList();
            var rowSpan = deliveryGroupIds.Count + 1;
            var paymentQuery = claimPayments.Where(c => c.PaymentType == 2).ToList();

            //Build empty view model with default rate, populate the data from claimpayments 
            haulerPayment.DOTPaymentViewModel.RowDatas = new List<DOTPaymentItem>();
            haulerPayment.DOTPaymentViewModel.RowSpan = rowSpan;

            pickupGroupIds.ForEach(c =>
            {
                var pickupGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)c).GroupName;
                var firstDeliveryGroupId = (int)deliveryGroupIds[0];
                var firstDeliveryGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)firstDeliveryGroupId).GroupName;
                var firstRow = new DOTPaymentItem();
                firstRow.RowType = 0;
                firstRow.PickupGroupId = (int)c;
                firstRow.PickupGroupName = pickupGroupName;
                firstRow.DeliveryGroupId = firstDeliveryGroupId;
                firstRow.DeliveryGroupName = firstDeliveryGroupName;
                var offRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == firstDeliveryGroupId && r.PaymentType == 2 && r.ItemType == 2).FirstOrDefault();
                var offRoadRate = offRoadGroupRate != null ? offRoadGroupRate.Rate : 0m;
                firstRow.OffRoadRate = offRoadRate;

                var firstRowOffRoadPayment = paymentQuery.Where(p => p.PaymentType == 2 && p.ItemType == 2 && p.SourceZone == pickupGroupName && p.DeliveryZone == firstDeliveryGroupName).FirstOrDefault();
                if (firstRowOffRoadPayment != null)
                {
                    firstRow.OffRoadWeight = firstRowOffRoadPayment.Weight;
                    firstRow.OffRoadRate = firstRowOffRoadPayment.Rate;
                    firstRow.OffRoadPremium = Math.Round(firstRowOffRoadPayment.Weight * firstRowOffRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    firstRow.OffRoadWeight = 0;
                    firstRow.OffRoadPremium = 0;
                }

                haulerPayment.DOTPaymentViewModel.RowDatas.Add(firstRow);
                var totalOffRoadWright = firstRow.OffRoadWeight;
                var totalOffRoadPremium = firstRow.OffRoadPremium;
                for (var i = 1; i < deliveryGroupIds.Count; i++)
                {
                    var rowItem = new DOTPaymentItem();
                    rowItem.RowType = 1;
                    var deliveryGroupId = (int)deliveryGroupIds[i];
                    var deliveryGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)deliveryGroupId).GroupName;
                    rowItem.PickupGroupId = (int)c;
                    rowItem.PickupGroupName = pickupGroupName;
                    rowItem.DeliveryGroupId = deliveryGroupId;
                    rowItem.DeliveryGroupName = deliveryGroupName;
                    var itemOffRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == deliveryGroupId && r.PaymentType == 2 && r.ItemType == 2).FirstOrDefault();
                    var itemOffRoadRate = itemOffRoadGroupRate != null ? itemOffRoadGroupRate.Rate : 0m;
                    rowItem.OffRoadRate = itemOffRoadRate;

                    var rowOffRoadPayment = paymentQuery.Where(p => p.PaymentType == 2 && p.ItemType == 2 && p.SourceZone == pickupGroupName && p.DeliveryZone == deliveryGroupName).FirstOrDefault();
                    if (rowOffRoadPayment != null)
                    {
                        rowItem.OffRoadWeight = rowOffRoadPayment.Weight;
                        rowItem.OffRoadRate = rowOffRoadPayment.Rate;
                        rowItem.OffRoadPremium = Math.Round(rowOffRoadPayment.Weight * rowOffRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        rowItem.OffRoadWeight = 0;
                        rowItem.OffRoadPremium = 0;
                    }
                    haulerPayment.DOTPaymentViewModel.RowDatas.Add(rowItem);
                    totalOffRoadWright = totalOffRoadWright + rowItem.OffRoadWeight;
                    totalOffRoadPremium = totalOffRoadPremium + rowItem.OffRoadPremium;
                }

                var subTotal = new DOTPaymentItem();
                subTotal.RowType = 2;
                subTotal.DeliveryGroupName = "Total";
                subTotal.OffRoadWeight = totalOffRoadWright;
                subTotal.OffRoadPremium = totalOffRoadPremium;
                haulerPayment.DOTPaymentViewModel.RowDatas.Add(subTotal);
            });

            haulerPayment.DOTPaymentViewModel.TotalOffRoadPremium = haulerPayment.DOTPaymentViewModel.RowDatas.Where(c => c.RowType == 2).Sum(c => c.OffRoadPremium);
            haulerPayment.DOTPaymentViewModel.TotalOffRoadWeight= haulerPayment.DOTPaymentViewModel.RowDatas.Where(c => c.RowType == 2).Sum(c => c.OffRoadWeight);
            haulerPayment.DOTPaymentViewModel.TotalPremium = haulerPayment.DOTPaymentViewModel.TotalOffRoadPremium;
        }

        private void PopulateTransportationPremiumDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPayment> claimPayments, List<RateGroupRate> rateGroupRates, List<Group> vendorGroups)
        {
            haulerPayment.TransportationPremium = new TransportationPremium();

            var pickupGroupIds = rateGroupRates.Where(c=>c.PaymentType==1).Select(c => c.CollectorGroupId).Distinct().ToList();
            var deliveryGroupIds = rateGroupRates.Where(c => c.PaymentType == 1).Select(c => c.ProcessorGroupId).Distinct().ToList();
            var rowSpan = deliveryGroupIds.Count + 1;
            var paymentQuery = claimPayments.Where(c => c.PaymentType == 1).ToList();

            //Build empty view model with default rate, populate the data from claimpayments 
            haulerPayment.TransportationPremium.RowDatas = new List<TransportationPaymentItem>();
            haulerPayment.TransportationPremium.RowSpan = rowSpan;

            pickupGroupIds.ForEach(c =>
            {
                var pickupGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)c).GroupName;
                var firstDeliveryGroupId = (int)deliveryGroupIds[0];
                var firstDeliveryGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)firstDeliveryGroupId).GroupName;
                var firstRow = new TransportationPaymentItem();
                firstRow.RowType = 0;
                firstRow.PickupGroupId = (int)c;
                firstRow.PickupGroupName = pickupGroupName;
                firstRow.DeliveryGroupId = firstDeliveryGroupId;
                firstRow.DeliveryGroupName = firstDeliveryGroupName;
                var onRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == firstDeliveryGroupId && r.PaymentType == 1 && r.ItemType == 1).FirstOrDefault();
                var onRoadRate = onRoadGroupRate != null ? onRoadGroupRate.Rate : 0m;
                var offRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == firstDeliveryGroupId && r.PaymentType == 1 && r.ItemType == 2).FirstOrDefault();
                var offRoadRate = offRoadGroupRate != null ? offRoadGroupRate.Rate : 0m;
                firstRow.OnRoadRate = onRoadRate;
                firstRow.OffRoadRate = offRoadRate;

                var firstRowOnRoadPayment = paymentQuery.Where(p => p.PaymentType == 1 && p.ItemType == 1 && p.SourceZone == pickupGroupName && p.DeliveryZone == firstDeliveryGroupName).FirstOrDefault();
                if (firstRowOnRoadPayment != null)
                {
                    firstRow.OnRoadWeight = firstRowOnRoadPayment.Weight;
                    firstRow.OnRoadRate = firstRowOnRoadPayment.Rate;
                    firstRow.OnRoadPremium = Math.Round(firstRowOnRoadPayment.Weight * firstRowOnRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    firstRow.OnRoadWeight = 0;
                    firstRow.OnRoadPremium = 0;
                }
                var firstRowOffRoadPayment = paymentQuery.Where(p => p.PaymentType == 1 && p.ItemType == 2 && p.SourceZone == pickupGroupName && p.DeliveryZone == firstDeliveryGroupName).FirstOrDefault();
                if (firstRowOffRoadPayment != null)
                {
                    firstRow.OffRoadWeight = firstRowOffRoadPayment.Weight;
                    firstRow.OffRoadRate = firstRowOffRoadPayment.Rate;
                    firstRow.OffRoadPremium = Math.Round(firstRowOffRoadPayment.Weight * firstRowOffRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    firstRow.OffRoadWeight = 0;
                    firstRow.OffRoadPremium = 0;
                }

                haulerPayment.TransportationPremium.RowDatas.Add(firstRow);
                var totalOnRoadWeight = firstRow.OnRoadWeight;
                var totalOffRoadWright = firstRow.OffRoadWeight;
                var totalOnRoadPremium = firstRow.OnRoadPremium;
                var totalOffRoadPremium = firstRow.OffRoadPremium;
                for (var i = 1; i < deliveryGroupIds.Count; i++)
                {
                    var rowItem = new TransportationPaymentItem();
                    rowItem.RowType = 1;
                    var deliveryGroupId = (int)deliveryGroupIds[i];
                    var deliveryGroupName = vendorGroups.FirstOrDefault(g => g.Id == (int)deliveryGroupId).GroupName;
                    rowItem.PickupGroupId = (int)c;
                    rowItem.PickupGroupName = pickupGroupName;
                    rowItem.DeliveryGroupId = deliveryGroupId;
                    rowItem.DeliveryGroupName = deliveryGroupName;
                    var itemOnRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == deliveryGroupId && r.PaymentType == 1 && r.ItemType == 1).FirstOrDefault();
                    var itemOnRoadRate = itemOnRoadGroupRate != null ? itemOnRoadGroupRate.Rate : 0m;
                    var itemOffRoadGroupRate = rateGroupRates.Where(r => r.CollectorGroupId == c && r.ProcessorGroupId == deliveryGroupId && r.PaymentType == 1 && r.ItemType == 2).FirstOrDefault();
                    var itemOffRoadRate = itemOffRoadGroupRate != null ? itemOffRoadGroupRate.Rate : 0m;
                    rowItem.OnRoadRate = itemOnRoadRate;
                    rowItem.OffRoadRate = itemOffRoadRate;

                    var rowOnRoadPayment = paymentQuery.Where(p => p.PaymentType == 1 && p.ItemType == 1 && p.SourceZone == pickupGroupName && p.DeliveryZone == deliveryGroupName).FirstOrDefault();
                    if (rowOnRoadPayment != null)
                    {
                        rowItem.OnRoadWeight = rowOnRoadPayment.Weight;
                        rowItem.OnRoadRate = rowOnRoadPayment.Rate;
                        rowItem.OnRoadPremium = Math.Round(rowOnRoadPayment.Weight * rowOnRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        rowItem.OnRoadWeight = 0;
                        rowItem.OnRoadPremium = 0;
                    }

                    var rowOffRoadPayment = paymentQuery.Where(p => p.PaymentType == 1 && p.ItemType == 2 && p.SourceZone == pickupGroupName && p.DeliveryZone == deliveryGroupName).FirstOrDefault();
                    if (rowOffRoadPayment != null)
                    {
                        rowItem.OffRoadWeight = rowOffRoadPayment.Weight;
                        rowItem.OffRoadRate = rowOffRoadPayment.Rate;
                        rowItem.OffRoadPremium = Math.Round(rowOffRoadPayment.Weight * rowOffRoadPayment.Rate, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        rowItem.OffRoadWeight = 0;
                        rowItem.OffRoadPremium = 0;
                    }
                    haulerPayment.TransportationPremium.RowDatas.Add(rowItem);
                    totalOnRoadWeight = totalOnRoadWeight + rowItem.OnRoadWeight;
                    totalOnRoadPremium = totalOnRoadPremium + rowItem.OnRoadPremium;
                    totalOffRoadWright = totalOffRoadWright + rowItem.OffRoadWeight;
                    totalOffRoadPremium = totalOffRoadPremium + rowItem.OffRoadPremium;
                }

                var subTotal = new TransportationPaymentItem();
                subTotal.RowType = 2;
                subTotal.DeliveryGroupName = "Total";
                subTotal.OnRoadWeight = totalOnRoadWeight;
                subTotal.OffRoadWeight = totalOffRoadWright;
                subTotal.OnRoadPremium =totalOnRoadPremium;
                subTotal.OffRoadPremium = totalOffRoadPremium;
                haulerPayment.TransportationPremium.RowDatas.Add(subTotal);
            });

            haulerPayment.TransportationPremium.TotalOnRoadPreminum = haulerPayment.TransportationPremium.RowDatas.Where(c=>c.RowType==2).Sum(c => c.OnRoadPremium);
            haulerPayment.TransportationPremium.TotalOffRoadPremium = haulerPayment.TransportationPremium.RowDatas.Where(c => c.RowType == 2).Sum(c => c.OffRoadPremium);
            haulerPayment.TransportationPremium.TotalPremium = haulerPayment.TransportationPremium.TotalOnRoadPreminum + haulerPayment.TransportationPremium.TotalOffRoadPremium;

        }

        //OTSTM2-1215
        private void PopulateStcPremiumDetails(HaulerPaymentViewModel haulerPayment, List<ClaimPaymentDetail> claimPaymentDetails, List<STCEventBriefViewModel> stcEventBriefList)
        {
            haulerPayment.STCPremiumViewModel = new STCPremiumViewModel();
           
            var paymentQuery = claimPaymentDetails.Where(c => c.PaymentType == 19).ToList();
          
            haulerPayment.STCPremiumViewModel.RowDatas = new List<STCPaymentItemViewModel>();

            stcEventBriefList.ForEach(c =>
            {
                var rowTmp = new STCPaymentItemViewModel();
                rowTmp.EventNumber = c.EventNumber;
                rowTmp.STCNumber = c.STCNumber;
                var claimPaymentDetailOnRoadForSameTransaction = paymentQuery.Where(p => p.PaymentType == 19 && p.ItemType == 1 && p.TransactionId == c.TransactionId).ToList();
                if (claimPaymentDetailOnRoadForSameTransaction.Count > 0)
                {
                    rowTmp.OnRoadWeight = claimPaymentDetailOnRoadForSameTransaction.Sum(p => p.ActualWeight);
                    rowTmp.OnRoadRate = claimPaymentDetailOnRoadForSameTransaction.FirstOrDefault().Rate;
                    rowTmp.OnRoadPremium = Math.Round(rowTmp.OnRoadWeight * rowTmp.OnRoadRate, 2, MidpointRounding.AwayFromZero);
                }
                var claimPaymentDetailOffRoadForSameTransaction = paymentQuery.Where(p => p.PaymentType == 19 && p.ItemType == 2 && p.TransactionId == c.TransactionId).ToList();
                if (claimPaymentDetailOnRoadForSameTransaction.Count > 0)
                {
                    rowTmp.OffRoadWeight = claimPaymentDetailOffRoadForSameTransaction.Sum(p => p.ActualWeight);
                    rowTmp.OffRoadRate = claimPaymentDetailOffRoadForSameTransaction.FirstOrDefault().Rate;
                    rowTmp.OffRoadPremium = Math.Round(rowTmp.OffRoadWeight * rowTmp.OffRoadRate, 2, MidpointRounding.AwayFromZero);
                }
                haulerPayment.STCPremiumViewModel.RowDatas.Add(rowTmp);
            });          

            haulerPayment.STCPremiumViewModel.TotalOnRoadPreminum = haulerPayment.STCPremiumViewModel.RowDatas.Sum(c => c.OnRoadPremium);
            haulerPayment.STCPremiumViewModel.TotalOffRoadPremium = haulerPayment.STCPremiumViewModel.RowDatas.Sum(c => c.OffRoadPremium);
            haulerPayment.STCPremiumViewModel.TotalPremium = haulerPayment.STCPremiumViewModel.TotalOnRoadPreminum + haulerPayment.STCPremiumViewModel.TotalOffRoadPremium;

        }

        public void UpdateYardCountSubmission(int claimId, ItemRow itemRow)
        {
            var result = claimsRepository.LoadYardCountSubmission(claimId);
            var claimPeriod = claimsRepository.GetClaimPeriod(claimId);
            if (result.Count > 0)
            {
                result.ForEach(c =>
                {
                    var item = DataLoader.TransactionItems.Single(o => o.ID == c.ItemId);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate);
                    foreach (KeyValuePair<string, int> pair in itemRow.Cast<KeyValuePair<string, int>>())
                    {
                        if (pair.Key == item.ShortName)
                        {
                            c.Quantity = pair.Value;
                            c.Weight = c.Quantity * itemWeight.StandardWeight;
                        }
                    }
                });
                claimsRepository.SaveContext();
            }
            else
            {
                var itemList = new List<ClaimYardCountSubmission>();
                foreach (KeyValuePair<string, int> pair in itemRow.Cast<KeyValuePair<string, int>>())
                {
                    var claimYardCountSubmission = new ClaimYardCountSubmission();
                    var item = DataLoader.TransactionItems.Single(c => c.ShortName == pair.Key);
                    var itemWeight = DataLoader.ItemWeights.FirstOrDefault(i => i.ItemID == item.ID && i.EffectiveStartDate <= claimPeriod.StartDate && i.EffectiveEndDate >= claimPeriod.StartDate);
                    claimYardCountSubmission.ClaimId = claimId;
                    claimYardCountSubmission.ItemId = item.ID;
                    claimYardCountSubmission.Quantity = pair.Value;
                    claimYardCountSubmission.Weight = pair.Value * itemWeight.StandardWeight;
                    claimYardCountSubmission.UpdatedBy = SecurityContextHelper.CurrentUser.Id;
                    claimYardCountSubmission.UpdatedDate = DateTime.UtcNow;
                    itemList.Add(claimYardCountSubmission);
                }
                claimsRepository.AddClaimYardCountSubmission(itemList);
            }
        }

        private void PopulatPaymentAdjustments(HaulerPaymentViewModel haulerPayment, ClaimPaymentSummary claimPaymentSummay, List<ClaimPayment> claimPayments, List<ClaimInternalPaymentAdjust> claimPaymentAdjustments)
        {
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => haulerPayment.UsingOldRounding ? Math.Round(i.Rate * i.Weight, 2) : Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.NorthernPremium)
                {
                    haulerPayment.NorthernPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.DOTPremium)
                {
                    haulerPayment.DOTPremium = c.Amount;
                }
                //OTSTM2-1215
                if (c.Name == (int)ClaimPaymentType.STCPremium) 
                {
                    haulerPayment.STCPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPayment = 0 - c.Amount;
                }
            });

            var adjustmentQuery = claimPaymentAdjustments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                AdjustmentAmount = Math.Round(c.Sum(i => i.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)
            });
            adjustmentQuery.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPaymentAdjust = c.AdjustmentAmount;
                }
                else
                {
                    haulerPayment.PaymentAdjustment += c.AdjustmentAmount;
                }
            });
            haulerPayment.isStaff = SecurityContextHelper.IsStaff();
            if (claimPaymentSummay != null)
            {
                haulerPayment.ChequeNumber = claimPaymentSummay.ChequeNumber;
                haulerPayment.PaidDate = claimPaymentSummay.PaidDate;
                haulerPayment.MailedDate = claimPaymentSummay.MailDate;
                haulerPayment.AmountPaid = claimPaymentSummay.AmountPaid;
                haulerPayment.BatchNumber = claimPaymentSummay.BatchNumber;
            }
        }

        private void PopulateWeightVariance(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            decimal actualTotal = 0;
            var ptrClaimDetails = claimDetails.Where(c => c.TransactionType == "PTR").ToList();
            ptrClaimDetails.ForEach(c =>
            {
                actualTotal += c.TransactionItems.Sum(o => o.ActualWeight);
            });

            decimal estimatedTotal = 0;
            ptrClaimDetails.ForEach(c =>
            {
                estimatedTotal += c.TransactionItems.Sum(o => o.AverageWeight);
            });

            if (estimatedTotal == 0)
                haulerClaimSummary.InventoryAdjustment.WeightVariance = 0;
            else
                haulerClaimSummary.InventoryAdjustment.WeightVariance = Math.Round(((actualTotal - estimatedTotal) /
                                                                     estimatedTotal) * 100, 2, MidpointRounding.AwayFromZero);
        }

        private void PopulateClaimYardCountSubmission(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimYardCountSubmission> claimYardCountSubmissions)
        {
            haulerClaimSummary.InventoryAdjustment.ItemSubmission = new ItemRow();
            claimYardCountSubmissions.ForEach(c =>
            {
                var item = DataLoader.Items.FirstOrDefault(i => i.ID == c.ItemId);
                var propertyInfo = haulerClaimSummary.InventoryAdjustment.ItemSubmission.GetType().GetProperty(item.ShortName);
                propertyInfo.SetValue(haulerClaimSummary.InventoryAdjustment.ItemSubmission, c.Quantity, null);
            });
        }

        private void PopulateInventoryAdjustment(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimInventoryAdjustment> inventoryAdjustments)
        {
            var query = inventoryAdjustments.GroupBy(c => new { c.IsEligible, c.Direction }).Select(c => new
            {
                Name = c.Key,
                AdjustmmentOnroad = c.Sum(i => i.AdjustmentWeightOnroad),
                AdjustmmentOffroad = c.Sum(i => i.AdjustmentWeightOffroad)
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name.IsEligible && c.Name.Direction == 1)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentInboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentInboundOffRoad = c.AdjustmmentOffroad;
                }
                if (c.Name.IsEligible && c.Name.Direction == 2)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOutboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOutboundOffRoad = c.AdjustmmentOffroad;
                }

                if (c.Name.IsEligible && c.Name.Direction == 0)
                {
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOverallOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.EligibleAdjustmentOverallOffRoad = c.AdjustmmentOffroad;
                }

                if (!c.Name.IsEligible && c.Name.Direction == 1)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentInboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentInboundOffRoad = c.AdjustmmentOffroad;

                }
                if (!c.Name.IsEligible && c.Name.Direction == 2)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOutboundOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOutboundOffRoad = c.AdjustmmentOffroad;
                }
                if (!c.Name.IsEligible && c.Name.Direction == 0)
                {
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOverallOnRoad = c.AdjustmmentOnroad;
                    haulerClaimSummary.InventoryAdjustment.IneligibleAdjustmentOverallOffRoad = c.AdjustmmentOffroad;
                }
            });
        }

        private void PopulateInboundOutbound(HaulerClaimSummaryModel haulerClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            var query = claimDetails
              .GroupBy(c => new { c.TransactionType, c.Direction })
              .Select(c => new { Name = c.Key, OnRoad = c.Sum(i => i.EstimatedOnRoad), OffRoad = c.Sum(i => i.EstimatedOffRoad) });
            query.ToList().ForEach(c =>
            {
                //Inbound
                if (c.Name.TransactionType == TreadMarksConstants.TCR && c.Name.Direction)
                {
                    haulerClaimSummary.TCROnRoad = c.OnRoad;
                    haulerClaimSummary.TCROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.DOT && c.Name.Direction)
                {
                    haulerClaimSummary.DOTOnRoad = c.OnRoad;
                    haulerClaimSummary.DOTOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.STC && c.Name.Direction)
                {
                    haulerClaimSummary.STCOnRoad = c.OnRoad;
                    haulerClaimSummary.STCOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.UCR && c.Name.Direction)
                {
                    haulerClaimSummary.UCROnRoad = c.OnRoad;
                    haulerClaimSummary.UCROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.HIT && c.Name.Direction)
                {
                    haulerClaimSummary.HITOnRoad = c.OnRoad;
                    haulerClaimSummary.HITOffRoad = c.OffRoad;
                }
                //Outbound
                if (c.Name.TransactionType == TreadMarksConstants.PTR && !c.Name.Direction)
                {
                    haulerClaimSummary.PTROnRoad = c.OnRoad;
                    haulerClaimSummary.PTROffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.HIT && !c.Name.Direction)
                {
                    haulerClaimSummary.HITOutboundOnRoad = c.OnRoad;
                    haulerClaimSummary.HITOutboundOffRoad = c.OffRoad;
                }
                if (c.Name.TransactionType == TreadMarksConstants.RTR && !c.Name.Direction)
                {
                    haulerClaimSummary.RTROnRoad = c.OnRoad;
                    haulerClaimSummary.RTROffRoad = c.OffRoad;
                }

            });
        }

        private void PopulateClaimTireBalance(HaulerClaimSummaryModel haulerClaimSummary, List<InventoryItem> inventoryItems)
        {
            haulerClaimSummary.InventoryAdjustment.TireCountBalance = new ItemRow();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            var query = inventoryItems
                        .GroupBy(c => new { c.ItemId, c.Direction })
                        .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty) });

            query.ToList().ForEach(c =>
            {
                if (c.Name.ItemId == pltItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.PLT -= c.Qty;
                }
                if (c.Name.ItemId == mtItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MT -= c.Qty;
                }

                if (c.Name.ItemId == aglsItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.AGLS -= c.Qty;
                }
                if (c.Name.ItemId == indItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.IND += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.IND -= c.Qty;
                }
                if (c.Name.ItemId == sotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.SOTR -= c.Qty;
                }
                if (c.Name.ItemId == motrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.MOTR -= c.Qty;
                }
                if (c.Name.ItemId == lotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.LOTR -= c.Qty;
                }
                if (c.Name.ItemId == gotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalance.GOTR -= c.Qty;
                }

            });
        }

        private void PopulateClaimTireBalanceWithoutAdj(HaulerClaimSummaryModel haulerClaimSummary, List<InventoryItem> inventoryItems)
        {
            haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj = new ItemRow();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);


            //OTSTM2-489  it will display the Tire Count Balance before the yard
            //count adjustment
            var query = inventoryItems.Where(c => c.IsAdjustedItem != true)
                        .GroupBy(c => new { c.ItemId, c.Direction })
                        .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty) });


            query.ToList().ForEach(c =>
            {
                if (c.Name.ItemId == pltItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.PLT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.PLT -= c.Qty;
                }
                if (c.Name.ItemId == mtItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MT += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MT -= c.Qty;
                }

                if (c.Name.ItemId == aglsItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.AGLS += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.AGLS -= c.Qty;
                }
                if (c.Name.ItemId == indItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.IND += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.IND -= c.Qty;
                }
                if (c.Name.ItemId == sotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.SOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.SOTR -= c.Qty;
                }
                if (c.Name.ItemId == motrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.MOTR -= c.Qty;
                }
                if (c.Name.ItemId == lotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.LOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.LOTR -= c.Qty;
                }
                if (c.Name.ItemId == gotrItem.ID)
                {
                    if (c.Name.Direction)
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.GOTR += c.Qty;
                    else
                        haulerClaimSummary.InventoryAdjustment.TireCountBalanceWithoutAdj.GOTR -= c.Qty;
                }

            });
        }

        #endregion

        #region GP Integration

        public GpResponseMsg CreateBatch()
        {
            var msg = new GpResponseMsg();

            // STEP 1 GET CLAIMS THAT ARE APPROVED AND NOT IN A BATCH	
            var claimType = (int)ClaimType.Hauler;
            var allClaims = claimsRepository.LoadApprovedWithoutBatch(claimType, true);
            var allClaimIds = allClaims.Select(c => c.ID).ToList();
            //Remove claim if it is banking status is not approved
            var claims = claimsRepository.FindApprovedBankClaimIds(allClaimIds);

            if (!claims.Any())
            {
                msg.Warnings.Add("There is no eligible data to add to the batch at this time.");
            }
            else
            {
                //STEP 2 INSERT ENTRY INTO GpiBatch Table
                var gpiBatch = new GpiBatch()
                {
                    GpiTypeID = GpHelper.GetGpiTypeID(GpManager.Hauler),
                    GpiBatchCount = claims.Count(),
                    GpiStatusId = GpHelper.GetGpiStatusString(GpStatus.Extract),
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName
                };

                var minPostDate = gpRepository.GetAllGpFiscalYears()
                    .Where(r => r.EffectiveStartDate.Date <= DateTime.Now.Date && DateTime.Now.Date <= r.EffectiveEndDate.Date)
                    .Select(d => d.MinimumDocDate).FirstOrDefault();
                var INTERID = settingRepository.GetSettingValue("GP:INTERID") ?? "TEST1";
                var claimIds = claims.Select(c => c.ID).ToList();

                var transactionTypes = new List<string> { "TCR", "DOT", "STC" };
                var items = DataLoader.Items;
                var claimDetails = claimsRepository.LoadClaimDetailsForGP(claimIds, transactionTypes, items);
                //var gpiAccounts = DataLoader.GPIAccounts;
                var fiAccounts = DataLoader.FIAccounts; //OTSTM2-1124

                //Update database
                using (var transactionScope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //1. GP Batch
                    gpRepository.AddGpBatch(gpiBatch);
                    msg.ID = gpiBatch.ID;
                    //2. GP Batch entries
                    var gpiBatchEntries = AddBatchEntries(claims, gpiBatch.ID);
                    gpRepository.AddGpBatchEntries(gpiBatchEntries);

                    //3. GP Transactions and Transaction Distributions
                    var rrOtsPmTransaction = AddGPTransactions(claims, gpiBatch.ID, gpiBatchEntries, minPostDate, INTERID,
                        claimDetails, fiAccounts); //OTSTM2-1124
                    gpRepository.AddGpRrOtsPmTransactions(rrOtsPmTransaction.Keys.ToList());
                    var rrOtsTransDistributions = new List<RrOtsPmTransDistribution>();
                    rrOtsPmTransaction.Values.ToList().ForEach(c =>
                    {
                        rrOtsTransDistributions.AddRange(c);
                    });
                    gpRepository.AddGpRrOtsPmTransDistributions(rrOtsTransDistributions);

                    //4. Update claim
                    claimsRepository.UpdateClaimsForGPBatch(claimIds, gpiBatch.ID.ToString());
                    transactionScope.Complete();
                }
            }
            return msg;
        }

        private Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>> AddGPTransactions(List<Claim> claims, int gpiBatchId, List<GpiBatchEntry> gpiBatchEntries, DateTime minPostDate, string INTERID, List<ClaimDetailViewModel> claimDetails, List<FIAccount> fiAccounts)
        {
            //Step 4 Add RM_Transaction
            var result = new Dictionary<RrOtsPmTransaction, List<RrOtsPmTransDistribution>>();
            var rrOtsPmTransaction = new List<RrOtsPmTransaction>();

            claims.ForEach(c =>
            {
                var haulerPaymentViewModel = LoadHaulerPaymentForGP(c.ID);
                //DOCType=1
                var prchAmount1 = Math.Round(haulerPaymentViewModel.TransportationPremiumBatch + haulerPaymentViewModel.DOTPremium +
                    (haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust > 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0), 2, MidpointRounding.AwayFromZero);
                var prchAmount1ForTax = haulerPaymentViewModel.TransportationPremiumBatch + haulerPaymentViewModel.DOTPremium +
                    (haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0);
                var prchTax1 = Math.Round(prchAmount1ForTax * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var distributedPaymentAdjustment = Math.Round((haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust > 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0), 2, MidpointRounding.AwayFromZero);
                var distributedPaymentAdjustmentTax = Math.Round((haulerPaymentViewModel.PaymentAdjustment > 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var positiveTotal = prchAmount1 + prchTax1;

                //DOCType=5
                var prchAmount5 = Math.Round(haulerPaymentViewModel.IneligibleInventoryPayment + (haulerPaymentViewModel.IneligibleInventoryPaymentAdjust < 0 ? haulerPaymentViewModel.IneligibleInventoryPaymentAdjust : 0) + (haulerPaymentViewModel.PaymentAdjustment < 0 ? haulerPaymentViewModel.PaymentAdjustment : 0), 2, MidpointRounding.AwayFromZero);
                var prchTax5 = Math.Round((haulerPaymentViewModel.PaymentAdjustment < 0 ? haulerPaymentViewModel.PaymentAdjustment : 0) * haulerPaymentViewModel.HSTBase, 2, MidpointRounding.AwayFromZero);

                var gpTotal = positiveTotal + prchAmount5 + prchTax5;

                //Fixed cent difference caused by rounding
                if (gpTotal != haulerPaymentViewModel.GrandTotal)
                {
                    var difference = (gpTotal - haulerPaymentViewModel.GrandTotal);
                    //put all difference in prchAmount1
                    prchAmount1 -= difference;
                }


                var gpiBatchEntryId = gpiBatchEntries.Single(q => q.ClaimId == c.ID).ID;
                int dstSeqNum = 1;

                int GPPaymentTermsDays = 35;
                int testit = 0;
                GPPaymentTermsDays = Int32.TryParse(AppSettings.Instance.GetSettingValue("Threshold.GPPaymentTerms"), out testit) ? testit : 35;

                var typeOneTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "1"),
                    DOCTYPE = 1,
                    DOCAMNT = prchAmount1 + prchTax1,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = (haulerPaymentViewModel.HST == 0 && haulerPaymentViewModel.IsTaxExempt) ? "P-EXEMPT" : "P-HST",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount1,
                    TAXAMNT = prchTax1,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };

                if (prchAmount1 + prchTax1 != 0)
                {
                    //Add transaction distribution
                    var currentClaimDetails = claimDetails.Where(q => q.ClaimId == c.ID).ToList();
                    var typeOneDistrubutions = AddTypeOneDistributions(gpiBatchId, INTERID, prchAmount1 + prchTax1, haulerPaymentViewModel.TransportationPremiumBatch, haulerPaymentViewModel.DOTPremium, haulerPaymentViewModel.HSTBase, distributedPaymentAdjustment, distributedPaymentAdjustmentTax, typeOneTransaction, fiAccounts, currentClaimDetails, c.ClaimPeriod.StartDate, ref dstSeqNum); //OTSTM2-1124
                    result.Add(typeOneTransaction, typeOneDistrubutions);
                }

                var typeFiveTransaction = new RrOtsPmTransaction
                {
                    BACHNUMB = string.Format("Batch{0}", gpiBatchId),
                    VCHNUMWK = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    VENDORID = c.Participant.Number,
                    DOCNUMBR = string.Format("{0}-{1:MMyy}-{2}-{3}", c.Participant.Number, c.ClaimPeriod.StartDate, "1", "5"),
                    DOCTYPE = 5,
                    DOCAMNT = prchAmount5 + prchTax5,
                    DOCDATE = c.ClaimPeriod.EndDate < minPostDate ? minPostDate : c.ClaimPeriod.EndDate,
                    PSTGDATE = null,
                    VADDCDPR = "PRIMARY",
                    PYMTRMID = "Net " + GPPaymentTermsDays.ToString() + " days",
                    TAXSCHID = (haulerPaymentViewModel.HST == 0 && haulerPaymentViewModel.IsTaxExempt) ? "P-EXEMPT" : "P-HST",
                    DUEDATE = null,
                    PRCHAMNT = prchAmount5,
                    TAXAMNT = prchTax5,
                    PORDNMBR = null,
                    USERDEF1 = gpiBatchId.ToString(),
                    USERDEF2 = string.Format("{0}-{1:MMyy}-{2}", c.Participant.Number, c.ClaimPeriod.StartDate, "1"),
                    INTERID = INTERID,
                    INTSTATUS = null,
                    INTDATE = null,
                    ERRORCODE = null,
                    GpiBatchEntryId = gpiBatchEntryId
                };
                if (prchAmount5 + prchTax5 != 0)
                {
                    //Add transaction distribution
                    var typeFiveDistrubutions = AddTypeFiveDistributions(gpiBatchId, INTERID, prchAmount5, prchTax5, typeFiveTransaction, fiAccounts, ref dstSeqNum); //OTSTM2-1124
                    result.Add(typeFiveTransaction, typeFiveDistrubutions);
                }
            });
            return result;
        }
        private List<RrOtsPmTransDistribution> AddTypeOneDistributions(int gpiBatchId, string INTERID, decimal positiveTotal, decimal nortnernPremium, decimal dotPremium, decimal hstBase, decimal distributedPaymentAdjustment, decimal distributedPaymentAdjustmentTax, RrOtsPmTransaction typeOneTransaction, List<FIAccount> fiAccounts, List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate, ref int dstSeqNum)
        {
            var northernPremiumTax = Math.Round(nortnernPremium * hstBase, 2, MidpointRounding.AwayFromZero);
            var dotPremiumTax = Math.Round(dotPremium * hstBase, 2, MidpointRounding.AwayFromZero);
            var distributedAmount = nortnernPremium + dotPremium + northernPremiumTax + dotPremiumTax;

            var grandTotal = positiveTotal - distributedPaymentAdjustment - distributedPaymentAdjustmentTax;

            //Fixed cent difference caused by rounding
            if (distributedAmount != grandTotal)
            {
                var difference = (distributedAmount - grandTotal);
                var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                if (numberOfDifferenceDistribution < 2)
                {
                    for (var i = 0; i < numberOfDifferenceDistribution; i++)
                    {
                        if (i == 0)
                            northernPremiumTax -= (difference / numberOfDifferenceDistribution);
                        else
                            dotPremiumTax -= (difference / numberOfDifferenceDistribution);
                    }
                }
            }

            distributedAmount = grandTotal;

            var typeOneDistrubutions = new List<RrOtsPmTransDistribution>();
            //var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "H");
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Hauler Payable" && a.AdminFICategoryID == 3); //OTSTM2-1124
            //Northern Premium
            //var PLTAccount = gpiAccounts.Single(a => a.account_number == "4366-10-30-40");
            //var PLTHSTAccount = gpiAccounts.Single(a => a.account_number == "4382-10-30-40");
            //var MTAccount = gpiAccounts.Single(a => a.account_number == "4367-20-30-40");
            //var MTHSTAccount = gpiAccounts.Single(a => a.account_number == "4384-20-30-40");
            //var AGLSAccount = gpiAccounts.Single(a => a.account_number == "4368-30-30-40");
            //var AGLSHSTAccount = gpiAccounts.Single(a => a.account_number == "4387-30-30-40");
            //var INDAccount = gpiAccounts.Single(a => a.account_number == "4369-40-30-40");
            //var INDHSTAccount = gpiAccounts.Single(a => a.account_number == "4389-40-30-40");
            //var SOTRAccount = gpiAccounts.Single(a => a.account_number == "4371-50-30-40");
            //var SOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4398-50-30-40");
            //var MOTRAccount = gpiAccounts.Single(a => a.account_number == "4372-60-30-40");
            //var MOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4401-60-30-40");
            //var LOTRAccount = gpiAccounts.Single(a => a.account_number == "4373-70-30-40");
            //var LOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4403-70-30-40");
            //var GOTRAccount = gpiAccounts.Single(a => a.account_number == "4374-80-30-40");
            //var GOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4406-80-30-40");
            //OTSTM2-1124
            var PLTAccount = fiAccounts.Single(a => a.Name == "PLT" && a.AdminFICategoryID == 3);
            var PLTHSTAccount = fiAccounts.Single(a => a.Name == "PLT HST" && a.AdminFICategoryID == 3);
            var MTAccount = fiAccounts.Single(a => a.Name == "MT" && a.AdminFICategoryID == 3);
            var MTHSTAccount = fiAccounts.Single(a => a.Name == "MT HST" && a.AdminFICategoryID == 3);
            var AGLSAccount = fiAccounts.Single(a => a.Name == "AG/LS" && a.AdminFICategoryID == 3);
            var AGLSHSTAccount = fiAccounts.Single(a => a.Name == "AG/LS HST" && a.AdminFICategoryID == 3);
            var INDAccount = fiAccounts.Single(a => a.Name == "IND" && a.AdminFICategoryID == 3);
            var INDHSTAccount = fiAccounts.Single(a => a.Name == "IND HST" && a.AdminFICategoryID == 3);
            var SOTRAccount = fiAccounts.Single(a => a.Name == "SOTR" && a.AdminFICategoryID == 3);
            var SOTRHSTAccount = fiAccounts.Single(a => a.Name == "SOTR HST" && a.AdminFICategoryID == 3);
            var MOTRAccount = fiAccounts.Single(a => a.Name == "MOTR" && a.AdminFICategoryID == 3);
            var MOTRHSTAccount = fiAccounts.Single(a => a.Name == "MOTR HST" && a.AdminFICategoryID == 3);
            var LOTRAccount = fiAccounts.Single(a => a.Name == "LOTR" && a.AdminFICategoryID == 3);
            var LOTRHSTAccount = fiAccounts.Single(a => a.Name == "LOTR HST" && a.AdminFICategoryID == 3);
            var GOTRAccount = fiAccounts.Single(a => a.Name == "GOTR" && a.AdminFICategoryID == 3);
            var GOTRHSTAccount = fiAccounts.Single(a => a.Name == "GOTR HST" && a.AdminFICategoryID == 3);

            //DOT Premium
            //var DOTMOTRAccount = gpiAccounts.Single(a => a.account_number == "4376-60-30-40");
            //var DOTLOTRAccount = gpiAccounts.Single(a => a.account_number == "4377-70-30-40");
            //var DOTGOTRAccount = gpiAccounts.Single(a => a.account_number == "4378-80-30-40");
            //var DOTOTRHSTAccount = gpiAccounts.Single(a => a.account_number == "4390-90-30-40");
            //OTSTM2-1124           
            var DOTMOTRAccount = fiAccounts.Single(a => a.Name == "DOR MOTR" && a.AdminFICategoryID == 3);
            var DOTLOTRAccount = fiAccounts.Single(a => a.Name == "DOR LOTR" && a.AdminFICategoryID == 3);
            var DOTGOTRAccount = fiAccounts.Single(a => a.Name == "DOR GOTR" && a.AdminFICategoryID == 3);
            var DOTOTRHSTAccount = fiAccounts.Single(a => a.Name == "DOT-OTR HST" && a.AdminFICategoryID == 3);

            //Adjustment
            //var tiAdjustmentHST = gpiAccounts.Single(a => a.account_number == "4470-90-30-40");
            //var tiAdjustment = gpiAccounts.Single(a => a.account_number == "4475-90-30-40");
            //OTSTM2-1124
            var tiAdjustmentHST = fiAccounts.Single(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 3);
            var tiAdjustment = fiAccounts.Single(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 3);

            if (distributedAmount + distributedPaymentAdjustment + distributedPaymentAdjustmentTax > 0)
            {
                var rowAccountPayrable = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = 0;
                rowAccountPayrable.CRDTAMNT = distributedAmount + distributedPaymentAdjustment + distributedPaymentAdjustmentTax;
                typeOneDistrubutions.Add(rowAccountPayrable);
            }

            if (distributedAmount != 0)
            {
                if (nortnernPremium > 0)
                {
                    //Northern Premium distribution
                    var northernPremiumDistributions = new List<RrOtsPmTransDistribution>();
                    var northernGpTireItem = FindTirePercentageForNorthernPremium(currentClaimDetails, claimStartDate);
                    //Add distribution records
                    if (northernGpTireItem.PLTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)PLTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = PLTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.MTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.AGLSPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)AGLSAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = AGLSAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.INDPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)INDAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = INDAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.SOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)SOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = SOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)LOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = LOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)GOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = GOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(nortnernPremium * northernGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernPremiumDistributions.Add(rowDistribution);
                    }

                    //Fixed cent difference caused by rounding
                    var totalAmount = northernPremiumDistributions.Sum(c => c.DEBITAMT);
                    if (totalAmount != nortnernPremium)
                    {
                        var difference = (totalAmount - nortnernPremium);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < northernPremiumDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                northernPremiumDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }

                    typeOneDistrubutions.AddRange(northernPremiumDistributions);

                    //NorthernPremium Tax distribution
                    var northernTaxDistributions = new List<RrOtsPmTransDistribution>();
                    //Add distribution records
                    if (northernGpTireItem.PLTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)PLTHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = PLTHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.PLTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.MTPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MTHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MTHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.MTPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.AGLSPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)AGLSHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = AGLSHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.AGLSPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.INDPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)INDHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = INDHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.INDPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.SOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)SOTRHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = SOTRHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.SOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)MOTRHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = MOTRHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)LOTRHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = LOTRHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }
                    if (northernGpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)GOTRHSTAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = GOTRHSTAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(northernPremiumTax * northernGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        northernTaxDistributions.Add(rowDistribution);
                    }

                    //Fixed cent difference caused by rounding
                    var totalTaxAmount = northernTaxDistributions.Sum(c => c.DEBITAMT);
                    if (totalTaxAmount != northernPremiumTax)
                    {
                        var difference = (totalTaxAmount - northernPremiumTax);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < northernTaxDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                northernTaxDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                    typeOneDistrubutions.AddRange(northernTaxDistributions);
                }

                //DOT Premium
                if (dotPremium > 0)
                {
                    var dotPremiumDistributions = new List<RrOtsPmTransDistribution>();
                    var dotGpTireItem = FindTirePercentageForDOTPremium(currentClaimDetails, claimStartDate);
                    if (dotGpTireItem.MOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)DOTMOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = DOTMOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(dotPremium * dotGpTireItem.MOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        dotPremiumDistributions.Add(rowDistribution);
                    }
                    if (dotGpTireItem.LOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)DOTLOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = DOTLOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(dotPremium * dotGpTireItem.LOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        dotPremiumDistributions.Add(rowDistribution);
                    }
                    if (dotGpTireItem.GOTRPercentage > 0)
                    {
                        var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                        rowDistribution.DISTTYPE = (int)DOTGOTRAccount.distribution_type_id;
                        rowDistribution.ACTNUMST = DOTGOTRAccount.AccountNumber; //OTSTM2-1124
                        rowDistribution.DEBITAMT = Math.Round(dotPremium * dotGpTireItem.GOTRPercentage, 2, MidpointRounding.AwayFromZero);
                        rowDistribution.CRDTAMNT = 0;
                        dotPremiumDistributions.Add(rowDistribution);
                    }

                    //Fixed cent difference caused by rounding
                    var totalDOTAmount = dotPremiumDistributions.Sum(c => c.DEBITAMT);
                    if (totalDOTAmount != dotPremium)
                    {
                        var difference = (totalDOTAmount - dotPremium);
                        var numberOfDifferenceDistribution = (Math.Abs(difference) / 0.01M);
                        if (numberOfDifferenceDistribution < dotPremiumDistributions.Count)
                        {
                            for (var i = 0; i < numberOfDifferenceDistribution; i++)
                            {
                                dotPremiumDistributions[i].DEBITAMT -= (difference / numberOfDifferenceDistribution);
                            }
                        }
                    }
                    typeOneDistrubutions.AddRange(dotPremiumDistributions);

                    //DOT Tax
                    var rowTaxDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                    rowTaxDistribution.DISTTYPE = (int)DOTOTRHSTAccount.distribution_type_id;
                    rowTaxDistribution.ACTNUMST = DOTOTRHSTAccount.AccountNumber; //OTSTM2-1124
                    rowTaxDistribution.DEBITAMT = dotPremiumTax;
                    rowTaxDistribution.CRDTAMNT = 0;
                    typeOneDistrubutions.Add(rowTaxDistribution);
                }
            }

            if (distributedPaymentAdjustment > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)tiAdjustment.distribution_type_id;
                rowDistribution.ACTNUMST = tiAdjustment.AccountNumber; //OTSTM2-1124
                rowDistribution.DEBITAMT = distributedPaymentAdjustment;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }

            if (distributedPaymentAdjustmentTax > 0)
            {
                var rowDistribution = GetTransDistribution(typeOneTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowDistribution.DISTTYPE = (int)tiAdjustmentHST.distribution_type_id;
                rowDistribution.ACTNUMST = tiAdjustmentHST.AccountNumber; //OTSTM2-1124
                rowDistribution.DEBITAMT = distributedPaymentAdjustmentTax;
                rowDistribution.CRDTAMNT = 0;
                typeOneDistrubutions.Add(rowDistribution);
            }
            return typeOneDistrubutions;
        }
        private GpTireItem FindTirePercentageForDOTPremium(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            currentClaimDetails.Where(c => c.TransactionType == "DOT").ToList().ForEach(c =>
            {
                transactionItems.AddRange(c.TransactionItems);
            });
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private GpTireItem FindTirePercentageForNorthernPremium(List<ClaimDetailViewModel> currentClaimDetails, DateTime claimStartDate)
        {
            var transactionItems = new List<TransactionItem>();
            //Add all TCR transaction
            currentClaimDetails.Where(c => c.TransactionType == "TCR").ToList().ForEach(c =>
                {
                    transactionItems.AddRange(c.TransactionItems);
                });

            //OTSTM2-1214 comment out the followed code because STC transaction is not eligible any more
            //Add STC which has pickupzone from 'N'
            //currentClaimDetails.Where(c => c.TransactionType == "STC").ToList().ForEach(c =>
            //{
            //    //var pickupZone = ClaimHelper.GetZoneByPostCode(c.Transaction.FromPostalCode, 1);
            //    //if ((pickupZone != null) && (pickupZone.Name.StartsWith("N")))
            //    //{
            //    //    transactionItems.AddRange(c.TransactionItems);
            //    //}
            //    transactionItems.AddRange(c.TransactionItems);
            //    //1. get a transactionIdList in SpecialItemRateList
            //    //2. if transactionIdList contains c.transaction.id, then do c.transactionItems.AddRange(c.TransactionItems)
            //});
            var gpTireItem = ClaimHelper.GetTireItem(transactionItems, claimStartDate);

            return gpTireItem;
        }
        private List<RrOtsPmTransDistribution> AddTypeFiveDistributions(int gpiBatchId, string INTERID, decimal prchAmount5, decimal prchTax5, RrOtsPmTransaction typeFiveTransaction, List<FIAccount> fiAccounts, ref int dstSeqNum)
        {
            //TI Adjustment HST 4470-90-30-40  id=99
            //TI Adjustment 4475-90-30-40 id=104
            //Accounts Payable 2000-00-00-00 id=2
            var typeFiveDistrubutions = new List<RrOtsPmTransDistribution>();
            //var apAccount = gpiAccounts.FirstOrDefault(a => a.account_number == "2000-00-00-00" && a.registrant_type_ind.Trim() == "H");
            //var tiAdjustmentHST = gpiAccounts.Single(a => a.account_number == "4470-90-30-40");
            //var tiAdjustment = gpiAccounts.Single(a => a.account_number == "4475-90-30-40");
            //OTSTM2-1124
            var apAccount = fiAccounts.FirstOrDefault(a => a.Name == "Hauler Payable" && a.AdminFICategoryID == 3);
            var tiAdjustmentHST = fiAccounts.Single(a => a.Name == "Internal Payment Adjustments HST" && a.AdminFICategoryID == 3);
            var tiAdjustment = fiAccounts.Single(a => a.Name == "Internal Payment Adjustments" && a.AdminFICategoryID == 3);
            if (prchAmount5 != 0)
            {
                prchAmount5 = Math.Abs(prchAmount5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = prchAmount5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)tiAdjustment.distribution_type_id;
                rowTIAdjustment.ACTNUMST = tiAdjustment.AccountNumber; //OTSTM2-1124
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchAmount5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            if (prchTax5 != 0)
            {
                prchTax5 = Math.Abs(prchTax5);
                var rowAccountPayrable = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowAccountPayrable.DISTTYPE = (int)apAccount.distribution_type_id;
                rowAccountPayrable.ACTNUMST = apAccount.AccountNumber; //OTSTM2-1124
                rowAccountPayrable.DEBITAMT = prchTax5;
                rowAccountPayrable.CRDTAMNT = 0;
                typeFiveDistrubutions.Add(rowAccountPayrable);
                var rowTIAdjustment = GetTransDistribution(typeFiveTransaction, gpiBatchId, INTERID, dstSeqNum++);
                rowTIAdjustment.DISTTYPE = (int)tiAdjustmentHST.distribution_type_id;
                rowTIAdjustment.ACTNUMST = tiAdjustmentHST.AccountNumber; //OTSTM2-1124
                rowTIAdjustment.DEBITAMT = 0;
                rowTIAdjustment.CRDTAMNT = prchTax5;
                typeFiveDistrubutions.Add(rowTIAdjustment);
            }
            return typeFiveDistrubutions;
        }
        private RrOtsPmTransDistribution GetTransDistribution(RrOtsPmTransaction gpTransaction, int gpiBatchId, string INTERID, int dstSeqNum)
        {
            var result = new RrOtsPmTransDistribution()
            {
                VCHNUMWK = gpTransaction.VCHNUMWK,
                DOCTYPE = gpTransaction.DOCTYPE,
                VENDORID = gpTransaction.VENDORID,
                DSTSQNUM = dstSeqNum,
                DistRef = null,
                USERDEF1 = gpiBatchId.ToString(),
                USERDEF2 = null,
                INTERID = INTERID,
                INTSTATUS = null,
                INTDATE = null,
                ERRORCODE = null
            };
            return result;
        }
        private List<GpiBatchEntry> AddBatchEntries(List<Claim> claims, int gpiBatchId)
        {
            return claims.Select(c => new GpiBatchEntry
            {
                GpiBatchID = gpiBatchId,
                GpiTxnNumber = string.Format("{0}-{1}-{2:yyyy MM dd}", c.ID.ToString(), c.Participant.Number, c.ClaimPeriod.StartDate),
                GpiBatchEntryDataKey = c.ID.ToString(),
                GpiStatusID = "E",
                CreatedDate = DateTime.Now,
                CreatedBy = SecurityContextHelper.CurrentUser.UserName,
                LastUpdatedDate = DateTime.Now,
                LastUpdatedBy = SecurityContextHelper.CurrentUser.UserName,
                ClaimId = c.ID
            }).ToList();
        }
        private HaulerPaymentViewModel LoadHaulerPaymentForGP(int claimId)
        {
            var haulerPayment = new HaulerPaymentViewModel();
            var claimPayments = claimsRepository.LoadClaimPayments(claimId);
            var claimPaymentAdjustments = claimsRepository.LoadClaimPaymentInternalAdjusts(claimId);
            var query = claimPayments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                Amount = c.Sum(i => Math.Round(i.Rate * i.Weight, 2, MidpointRounding.AwayFromZero))
            });
            query.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.NorthernPremium)
                {
                    haulerPayment.NorthernPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.DOTPremium)
                {
                    haulerPayment.DOTPremium = c.Amount;
                }
                //OTSTM2-1215
                if (c.Name == (int)ClaimPaymentType.STCPremium)
                {
                    haulerPayment.STCPremium = c.Amount;
                }
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPayment = 0 - c.Amount;
                }
            });

            var adjustmentQuery = claimPaymentAdjustments.GroupBy(c => c.PaymentType).Select(c => new
            {
                Name = c.Key,
                AdjustmentAmount = Math.Round(c.Sum(i => i.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)
            });
            adjustmentQuery.ToList().ForEach(c =>
            {
                if (c.Name == (int)ClaimPaymentType.IneligibleInventoryPayment)
                {
                    haulerPayment.IneligibleInventoryPaymentAdjust = c.AdjustmentAmount;
                }
                else
                {
                    haulerPayment.PaymentAdjustment += c.AdjustmentAmount;
                }
            });
            var isTaxExempt = claimsRepository.IsTaxExempt(claimId);
            if (isTaxExempt)
            {
                haulerPayment.HSTBase = 0.00m;
            }
            haulerPayment.IsTaxExempt = isTaxExempt;

            return haulerPayment;
        }
        #endregion

    }
}