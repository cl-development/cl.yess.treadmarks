﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ClaimCalculator
{
    public class DOTPremiumPickup
    {
        public int PickupGroupId { get; set; }
        public decimal OffRoad { get; set; }
    }
}
