﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts;
using CL.TMS.DataContracts.Communication;
using CL.TMS.DataContracts.DomainEntities;
using System.Configuration;
using CL.Framework.Logging;
using System.Device.Location;

namespace CL.TMS.ReverseGeocode
{
    /// <summary>
    /// This class gives you distance difference based on lat and long and postalcode
    /// </summary>
    public static class DistanceLookup
    {
        //https://maps.googleapis.com/maps/api/distancematrix/json?origins=43.880000,-79.730000&destinations=K2E6T7&key=AIzaSyB0vQbz50LyGgRX2SkCknrOEUlLeBfQ6as

        //static string baseUri = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0},{1}&destinations={2}&key={3}";
        //static string key = ConfigurationManager.AppSettings["GoogleMapsDistanceMatrixAPI"];

        /// <summary>
        /// This method gives you distance (in meters) between origin and destination 
        /// </summary>
        public static double GetDistance(double originLat, double originLng, string destPostalCode)
        {            
            double distance = 0;

            try
            {
                var location = ReverseAddressLookup.RetrieveCoordinates(destPostalCode);

                if (location != null)
                {
                    var sCoord = new GeoCoordinate(originLat, originLng);
                    var dCoord = new GeoCoordinate(location.lat, location.lng);

                    distance = sCoord.GetDistanceTo(dCoord);
                }
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
            }

            return distance;
        }
    }
}


