﻿using CL.TMS.CollectorBLL;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.ExceptionHandling;
using CL.TMS.IRepository.Claims;
using CL.TMS.ServiceContracts.CollectorServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.ViewModel.GP;

namespace CL.TMS.CollectorServices
{
    public class CollectorClaimService : ICollectorClaimService
    {
        private CollectorClaimBO collectorClaimBO;
        private readonly IGpRepository gpRepository;
        private readonly ISettingRepository settingRepository;

        public CollectorClaimService(IClaimsRepository claimsRepository, ITransactionRepository transactionRepository, IGpRepository gpRepository, ISettingRepository settingRepository)
        {
            collectorClaimBO = new CollectorClaimBO(claimsRepository, transactionRepository, gpRepository, settingRepository);
        }

        public void AddReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            try
            {
                collectorClaimBO.AddReuseTires(claimId, reuseTiresItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void AddTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            try
            {
                collectorClaimBO.AddTireOrigin(claimId, tireOriginItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public CollectorClaimSummaryModel LoadCollectorClaimSummary(int claimId)
        {
            try
            {
                var result = collectorClaimBO.LoadCollectorClaimSummary(claimId);
                GC.Collect();
                return result;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void RemoveReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            try
            {
                collectorClaimBO.RemoveReuseTires(claimId, reuseTiresItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void RemoveTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            try
            {
                collectorClaimBO.RemoveTireOrigin(claimId, tireOriginItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow)
        {
            try
            {
                collectorClaimBO.UpdateReuseTires(claimId, reuseTiresItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public void UpdateTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow)
        {
            try
            {
                collectorClaimBO.UpdateTireOrigin(claimId, tireOriginItemRow);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }

        public CollectorSubmitClaimViewModel CollectorClaimSubmitBusinessRule(CollectorSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                return collectorClaimBO.CollectorClaimSubmitBusinessRule(submitClaimModel);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public CollectorClaimInboundReuseTireModel LoadInbound(int claimId)
        {
            try
            {
                return collectorClaimBO.LoadInboundTires(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public CollectorClaimInboundReuseTireModel LoadReuseTires(int claimId)
        {
            try
            {
                return collectorClaimBO.LoadReuseTires(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }


        public GpResponseMsg CreateBatch()
        {
            try
            {
                return collectorClaimBO.CreateBatch();
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public bool IsStaffTotalInboundOutbound(int claimId)
        {
            try
            {
                return collectorClaimBO.IsStaffTotalInboundOutbound(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return false;
        }

        public List<ClaimSupportingDocVM> LoadSupportingDocs(int claimId)
        {
            try
            {
                return collectorClaimBO.LoadSupportingDocs(claimId);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
            return null;
        }

        public void UpdateSupportingDocOption(int claimId, int defValue, string option)
        {
            try
            {
                collectorClaimBO.UpdateSupportingDocOption(claimId, defValue, option);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingServiceException(ex);
                if (rethrow)
                {
                    throw;
                }
            }
        }
    }
}
