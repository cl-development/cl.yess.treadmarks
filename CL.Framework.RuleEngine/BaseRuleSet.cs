﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace CL.Framework.RuleEngine
{
    /// <summary>
    /// Base rule set
    /// </summary>
    public class BaseRuleSet : IRuleSet
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public BaseRuleSet()
        {
            RuleSet = new List<BaseBusinessRule>();
        }
        #endregion 

        #region Properties
        /// <summary>
        /// Rule set
        /// </summary>
        public List<BaseBusinessRule> RuleSet { get; set; }
        #endregion 

        #region Public Methods
        /// <summary>
        /// Execute rule set, it will execute all contained business rule
        /// </summary>
        /// <param name="target"></param>
        /// <param name="ruleContext"></param>
        /// <returns></returns>
        public virtual ValidationResult Execute<T>(T target, IDictionary<string, object> ruleContext = null)
        {
            var validationResult = new ValidationResult();
            foreach (var item in RuleSet)
            {
                if (item.IsEnable)
                {
                    item.Execute(target, validationResult, ruleContext);
                    if (!validationResult.IsValid && item.TerminateOnFail)
                        break;
                }
            }
            return validationResult;
        }

        /// <summary>
        /// Initilaize all business rule
        /// </summary>
        public virtual void Initialize()
        {

        }
        /// <summary>
        /// Add business rule
        /// </summary>
        /// <param name="businessRule"></param>
        public void AddBusinessRule(BaseBusinessRule businessRule)
        {
            RuleSet.Add(businessRule);
        }
        /// <summary>
        /// Remove business rule
        /// </summary>
        /// <param name="businessRule"></param>
        public void RemoveBusinessRule(BaseBusinessRule businessRule)
        {
            RuleSet.Remove(businessRule);
        }
        /// <summary>
        /// Enable/disable business rule based on rule name
        /// </summary>
        /// <param name="ruleName"></param>
        /// <param name="isEnable"></param>
        public void ActivateBusinessRule(string ruleName, bool isEnable)
        {
            var result = RuleSet.First(p => (String.Compare(p.RuleName, ruleName, StringComparison.OrdinalIgnoreCase) == 0));
            if (result != null)
            {
                result.IsEnable = isEnable;
            }
        }
        public void ClearBusinessRules()
        {
            RuleSet.Clear();
        }
        #endregion


    
    }
}
