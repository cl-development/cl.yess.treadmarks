﻿using System.Collections.Generic;
using FluentValidation.Results;

namespace CL.Framework.RuleEngine
{
    /// <summary>
    /// Rule set interface
    /// </summary>
    public interface IRuleSet
    {
        /// <summary>
        /// Rule execution entry point for this ruleset
        /// </summary>
        /// <param name="target"></param>
        /// <param name="ruleName"></param>
        /// <param name="ruleContext"></param>
        /// <returns></returns>
        ValidationResult Execute<T>(T target, IDictionary<string, object> ruleContext = null);

        /// <summary>
        /// Business rule collection
        /// </summary>
        List<BaseBusinessRule> RuleSet { get; set; }

        /// <summary>
        /// Initialize rule set
        /// </summary>
        void Initialize();

        /// <summary>
        /// Add business rule
        /// </summary>
        /// <param name="businessRule"></param>
        void AddBusinessRule(BaseBusinessRule businessRule);

        /// <summary>
        /// Remove business rule
        /// </summary>
        /// <param name="businessRule"></param>
        void RemoveBusinessRule(BaseBusinessRule businessRule);

        /// <summary>
        /// Enable/Disable business rule
        /// </summary>
        /// <param name="ruleName"></param>
        void ActivateBusinessRule(string ruleName, bool isEnabled);

        void ClearBusinessRules();
    }
}
