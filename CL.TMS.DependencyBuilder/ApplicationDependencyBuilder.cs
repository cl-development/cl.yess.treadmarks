﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.HaulerServices;
using CL.TMS.RPMServices;
using CL.TMS.ProcessorServices;
using CL.TMS.StewardServices;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.Repository;
using CL.TMS.Repository.System;
using CL.TMS.Security.Implementations;
using CL.TMS.Security.Interfaces;
using CL.TMS.ServiceContracts;
using CL.TMS.ServiceContracts.StewardServices;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.ServiceContracts.RPMServices;
using CL.TMS.ServiceContracts.SystemServices;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.SystemServices;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.CollectorServices;
using CL.TMS.IRepository.Registrant;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.RegistrantServices;
using CL.TMS.Repository.Common;
using CL.TMS.Repository.Registrant;
using CL.TMS.IRepository.TSFSteward;
using CL.TMS.Repository.Claims;
using CL.TMS.IRepository.Claims;
using CL.TMS.Repository.RPM;
using CL.TMS.IRepository.RPM;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.SystemServices.CompanyBrandingServices;


namespace CL.TMS.DependencyBuilder
{
    public static class ApplicationDependencyBuilder
    {
        /// <summary>
        /// Register repository dependencies
        /// </summary>
        /// <param name="container"></param>
        public static void BuildRepositoryDependencies(UnityContainer container)
        {
            container.RegisterType<IAuthenticationRepository, AuthenticationRepository>(new PerRequestLifetimeManager());
            container.RegisterType<ISettingRepository, SettingRepository>();
            container.RegisterType<IInvitationRepository, InvitationRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IApplicationRepository, ApplicationRepository>();
            container.RegisterType<IApplicationInvitationRepository, ApplicationInvitationRepository>();
            container.RegisterType<IItemRepository, ItemRepository>();
            container.RegisterType<IAssetRepository, AssetRepository>();
            container.RegisterType<IAppUserRepository, AppUserRepository>();
            container.RegisterType<IVendorRepository, VendorRepository>();
            container.RegisterType<ITransactionRepository, TransactionRepository>();
            container.RegisterType<IMobileTransactionRepository, MobileTransactionRepository>();
            container.RegisterType<IApplicationBriefRepository, ApplicationBriefRepository>();
            container.RegisterType<IRegistrantManageRepository, RegistrantManageRepository>();
            container.RegisterType<IClaimsRepository, ClaimsRepository>();
            container.RegisterType<IGpRepository, GpRepository>();
            container.RegisterType<IReportingRepository, ReportingRepository>();
            container.RegisterType<ITSFRemittanceRepository, TSFRemittanceRepository>();
            container.RegisterType<IRetailConnectionRepository, RetailConnectionRepository>();
            container.RegisterType<IMessageRepository, MessageRepository>();
            container.RegisterType<IConfigurationsRepository, ConfigurationsRepository>();
            container.RegisterType<ICompanyBrandingRepository, CompanyBrandingRepository>();
            container.RegisterType<IEmailSettingsRepository, EmailSettingsRepository>();
            container.RegisterType<IProductCompositionRepository, ProductCompositionRepository>();
        }

        /// <summary>
        /// Register service dependencies
        /// </summary>
        /// <param name="container"></param>
        public static void BuildServiceDependencies(UnityContainer container)
        {
            //Singleton Event Aggregator
            container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());

            //Application Services
            container.RegisterType<IAuthentication, DBAuthentication>(new PerRequestLifetimeManager());
            container.RegisterType<IAppService, AppService>(new PerRequestLifetimeManager());

            container.RegisterType<IUserService, UserService>(new PerRequestLifetimeManager());
            container.RegisterType<IHaulerRegistrationService, HaulerRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<IHaulerRegistrationInvitationService, HaulerRegistrationInvitationService>(new PerRequestLifetimeManager());
            container.RegisterType<ICollectorRegistrationService, CollectorRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<ICollectorRegistrationInvitationService, CollectorRegistrationInvitationService>(new PerRequestLifetimeManager());
            container.RegisterType<IApplicationService, ApplicationService>(new PerRequestLifetimeManager());
            container.RegisterType<IRegistrantService, RegistrantService>(new PerRequestLifetimeManager());
            container.RegisterType<IApplicationInvitationService, ApplicationInvitationService>(new PerRequestLifetimeManager());
            container.RegisterType<IRPMRegistrationService, RPMRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<IRPMRegistrationInvitationService, RPMRegistrationInvitationService>(new PerRequestLifetimeManager());
            container.RegisterType<IStewardRegistrationService, StewardRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<IStewardRegistrationInvitationService, StewardRegistrationInvitationService>(new PerRequestLifetimeManager());

            container.RegisterType<IProcessorRegistrationService, ProcessorRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<IProcessorRegistrationInvitationService, ProcessorRegistrationInvitationService>(new PerRequestLifetimeManager());
            container.RegisterType<ITransactionService, TransactionService>(new PerRequestLifetimeManager());
            container.RegisterType<IDashBoardService, DashBoardService>(new PerRequestLifetimeManager());
            container.RegisterType<IClaimService, ClaimService>(new PerRequestLifetimeManager());
            container.RegisterType<IHaulerClaimService, HaulerClaimService>(new PerRequestLifetimeManager());
            container.RegisterType<IReportingService, ReportingService>(new PerRequestLifetimeManager());
            container.RegisterType<IRPMClaimService, RPMClaimService>(new PerRequestLifetimeManager());
            container.RegisterType<ICollectorClaimService, CollectorClaimService>(new PerRequestLifetimeManager());
            container.RegisterType<IHaulerTransactionService, HaulerTransactionService>(new PerRequestLifetimeManager());
            container.RegisterType<IFileUploadService, FileUploadService>(new PerRequestLifetimeManager());
            container.RegisterType<ITSFRemittanceService, TSFRemittanceService>(new PerRequestLifetimeManager());
            container.RegisterType<IProcessorClaimService, ProcessorClaimService>(new PerRequestLifetimeManager());

            container.RegisterType<IRetailConnectionService, RetailConnectionService>(new PerRequestLifetimeManager());
            container.RegisterType<IMessageService, MessageService>(new PerRequestLifetimeManager());

            container.RegisterType<IConfigurationsServices, ConfigurationsServices>(new PerRequestLifetimeManager());
            container.RegisterType<ICompanyBrandingServices, CompanyBrandingServices>(new PerRequestLifetimeManager());
            container.RegisterType<IEmailSettingsService, EmailSettingsService>(new PerRequestLifetimeManager());

            container.RegisterType<IProductCompositionServices, ProductCompositionService>(new PerRequestLifetimeManager());
        }

        /// <summary>
        /// Register dependencies for DataSync WebAPI project
        /// </summary>
        /// <param name="container"></param>
        public static void BuildDependenciesForCommunicator(UnityContainer container)
        {
            //Register repository dependencies
            container.RegisterType<ISettingRepository, SettingRepository>();
            container.RegisterType<IRegistrantManageRepository, RegistrantManageRepository>();
            container.RegisterType<IAppUserRepository, AppUserRepository>();
            container.RegisterType<IVendorRepository, VendorRepository>();
            container.RegisterType<IAssetRepository, AssetRepository>();
            container.RegisterType<IApplicationRepository, ApplicationRepository>();
            container.RegisterType<IApplicationInvitationRepository, ApplicationInvitationRepository>();
            container.RegisterType<ITransactionRepository, TransactionRepository>();
            container.RegisterType<IMobileTransactionRepository, MobileTransactionRepository>();
            container.RegisterType<IClaimsRepository, ClaimsRepository>();
            container.RegisterType<IGpRepository, GpRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<ITSFRemittanceRepository, TSFRemittanceRepository>();
            container.RegisterType<IMessageRepository, MessageRepository>();
            container.RegisterType<IConfigurationsRepository, ConfigurationsRepository>();


            //Register service dependencies
            container.RegisterType<IAppService, AppService>();
            container.RegisterType<IRegistrantService, RegistrantService>();
            container.RegisterType<ITransactionService, TransactionService>(new PerRequestLifetimeManager());
            container.RegisterType<ITSFRemittanceService, TSFRemittanceService>(new PerRequestLifetimeManager());
            container.RegisterType<IConfigurationsServices, ConfigurationsServices>(new PerRequestLifetimeManager());
            container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());
        }

        public static void BuildDependenciesForRetailConnectionService(UnityContainer container)
        {
            container.RegisterType<IRetailConnectionService, RetailConnectionService>(new PerRequestLifetimeManager());
            container.RegisterType<IRetailConnectionRepository, RetailConnectionRepository>();

        }
    }
}