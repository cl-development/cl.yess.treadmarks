﻿using CL.TMS.Caching;
using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.SystemBLL
{
    public class ProductCompositionBO
    {
        private IUserRepository userRepository;   
        private IProductCompositionRepository productCompositionRepository;

        public ProductCompositionBO(IUserRepository userRepository, IProductCompositionRepository productCompositionRepository)
        {
            this.userRepository = userRepository;

            this.productCompositionRepository = productCompositionRepository;
        }

        #region //Loading data
        public PaginationDTO<RecoverableMaterialsVM, int> LoadRecoverableMaterials(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return productCompositionRepository.LoadRecoverableMaterials(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }
        public PaginationDTO<MaterialCompositionsVM, int> LoadMaterialCompositions(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return productCompositionRepository.LoadMaterialCompositions(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public bool CheckMaterialNameUnique(string name) {
            return productCompositionRepository.CheckMaterialNameUnique(name);
        }
        #endregion //loading data

        #region //Save

        public bool SaveRecoverableMaterial(RecoverableMaterialsVM vm)
        {
            var result = false;
            var currentUser = SecurityContextHelper.CurrentUser;
            if (vm.ID > 0)
            {
                result=productCompositionRepository.UpdateRecoverableMaterial(vm, currentUser.Id, currentUser.Email, currentUser.FullName, TreadMarksConstants.AdminMenuActivity);
            }  else {
                result=productCompositionRepository.AddRecoverableMaterial(vm, currentUser.Id, currentUser.Email, currentUser.FullName, TreadMarksConstants.AdminMenuActivity);
            }    
            return result;
        }
        public bool SaveMaterialComposition(MaterialCompositionsVM vm)
        {
            var result = false;
            var currentUser = SecurityContextHelper.CurrentUser;

            result = productCompositionRepository.UpdateMaterialComposition(vm, currentUser.Id, currentUser.Email, currentUser.FullName, TreadMarksConstants.AdminMenuActivity);
       
            return result;
        }
        #endregion

        #region Estimated Weights
        public PaginationDTO<RecoveryEstimatedWeightListViewModel, int> LoadRecoveryEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return productCompositionRepository.LoadRecoveryEstimatedWeightList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }

        public PaginationDTO<SupplyEstimatedWeightViewModel, int> LoadSupplyEstimatedWeightList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return productCompositionRepository.LoadSupplyEstimatedWeightList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }

        public bool AddNewRecoveryEstimatedWeight(WeightDetailsVM commonWeightVM, long userId, int category)
        {
            if (productCompositionRepository.IsFutureWeightExists(category))//already exists future weight, not allow add more.
            {
                return false;
            }
            DateTime effectiveStartDate = new DateTime(commonWeightVM.EffectiveStartDate.Year, commonWeightVM.EffectiveStartDate.Month, 1);
            // save effectiveStartDate Date only. 
            commonWeightVM.EffectiveStartDate = effectiveStartDate.Date;

            if (effectiveStartDate.Date <= DateTime.Today.Date)//new weight effective date must be future date
            {
                return false;//Comment out for QATEST
            }
            var weightTransaction = new WeightTransaction();
            weightTransaction.CreatedDate = DateTime.UtcNow;
            weightTransaction.CreatedByID = userId;

            weightTransaction.EffectiveStartDate = commonWeightVM.EffectiveStartDate;
            weightTransaction.EffectiveEndDate = DateTime.MaxValue.Date;
            weightTransaction.Category = category;

            if (!string.IsNullOrEmpty(commonWeightVM.Note))
            {
                //add note
                var weightTransactionNote = new WeightTransactionNote();
                weightTransactionNote.WeightTransaction = weightTransaction;
                weightTransactionNote.Note = commonWeightVM.Note;
                weightTransactionNote.CreatedDate = DateTime.UtcNow;
                weightTransactionNote.UserID = userId;
                weightTransaction.WeightTransactionNotes = new List<WeightTransactionNote>();
                weightTransaction.WeightTransactionNotes.Add(weightTransactionNote);
            }

            var param = new WeightParamsDTO()
            {
                userId = userId,
                categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.WeightCategory, commonWeightVM.Category).Name
            };
            
            AddRecoveryEstimateWeights(commonWeightVM, weightTransaction, param);            

            productCompositionRepository.AddRecoveryEstimatedWeights(weightTransaction, param);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);

            return true;
        }

        private void AddRecoveryEstimateWeights(WeightDetailsVM commonWeightVM, WeightTransaction weightTransaction, WeightParamsDTO param)
        {
            getRecoveryEstimateWeightParam(param);
            weightTransaction.ItemWeights = new List<ItemWeight>();

            var weightItems = commonWeightVM.RecoveryEstimatedWeights.GetType().GetProperties();
            var paramItems = param.REW.GetType().GetProperties();
            foreach (var property in weightItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name == property.Name).GetValue(param.REW);
                var itemWeight = new ItemWeight()
                {
                    ItemID = paramID,
                    EffectiveStartDate = weightTransaction.EffectiveStartDate,
                    EffectiveEndDate = DateTime.MaxValue.Date,
                    WeightTransactionID = weightTransaction.ID,
                    StandardWeight = (decimal)property.GetValue(commonWeightVM.RecoveryEstimatedWeights),
                    CreatedBy = "System",
                    ModifiedBy = "System",
                    CreatedDate = DateTime.UtcNow,
                    ModifiedDate = DateTime.UtcNow
                };
                weightTransaction.ItemWeights.Add(itemWeight);
            }
        }
       
        private void getRecoveryEstimateWeightParam(WeightParamsDTO param)
        {
            var itemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "TransactionItem").DefinitionValue;
            param.REW = new RecoveryEstimatedWeightsParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,

                PLT = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "PLT" && x.ItemCategory == itemCategory).ID,
                MT = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "MT" && x.ItemCategory == itemCategory).ID,
                AGLS = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "AGLS" && x.ItemCategory == itemCategory).ID,
                IND = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "IND" && x.ItemCategory == itemCategory).ID,
                SOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "SOTR" && x.ItemCategory == itemCategory).ID,
                MOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "MOTR" && x.ItemCategory == itemCategory).ID,
                LOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "LOTR" && x.ItemCategory == itemCategory).ID,
                GOTR = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "GOTR" && x.ItemCategory == itemCategory).ID,
            };
        }        

        public WeightDetailsVM LoadRecoveryEstimatedWeightDetailsByTransactionID(int weightTransactionID, int category)
        {
            WeightDetailsVM weightsVM = new WeightDetailsVM()
            {
                Category = category,
                WeightTransactionID = weightTransactionID,
            };

            var param = new WeightParamsDTO()
            {
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.WeightCategory, category).Name
            };

            LoadRecoveryEstimatedWeightDetails(param, weightsVM);

            return weightsVM;
        }

        public SupplyEstimatedWeightViewModel LoadSupplyEstimatedWeightDetailsByItemID(int itemID, int category)
        {
            var result = productCompositionRepository.LoadSupplyEstimatedWeightDetailsByItemID(itemID, category);
            return result;
        }       

        private void LoadRecoveryEstimatedWeightDetails(WeightParamsDTO param, WeightDetailsVM weightsVM)
        {
            weightsVM.DecimalSize = 0;
            var weights = productCompositionRepository.GetItemWeights(weightsVM.WeightTransactionID);
            weightsVM = PopulateRecoveryEstimatedWeightVM(weights, weightsVM, param);
            weightsVM.EffectiveStartDate = weightsVM.WeightTransactionID == 0 ? DateTime.Now.AddMonths(1) : (DateTime)weights.FirstOrDefault().EffectiveStartDate;
            weightsVM.EffectiveEndDate = weightsVM.WeightTransactionID == 0 ? DateTime.MaxValue.Date : (DateTime)weights.FirstOrDefault().EffectiveEndDate;
            weightsVM.PreviousEffectiveStartDate = weightsVM.EffectiveStartDate;
        }

        private WeightDetailsVM PopulateRecoveryEstimatedWeightVM(IEnumerable<ItemWeight> result, WeightDetailsVM weightsVM, WeightParamsDTO param)
        {           
            getRecoveryEstimateWeightParam(param);
            weightsVM.RecoveryEstimatedWeights = new RecoveryEstimatedWeightsVM()
            {
                AGLS = result.FirstOrDefault(x => x.ItemID == param.REW.AGLS).StandardWeight,
                GOTR = result.FirstOrDefault(x => x.ItemID == param.REW.GOTR).StandardWeight,
                IND = result.FirstOrDefault(x => x.ItemID == param.REW.IND).StandardWeight,
                LOTR = result.FirstOrDefault(x => x.ItemID == param.REW.LOTR).StandardWeight,
                MOTR = result.FirstOrDefault(x => x.ItemID == param.REW.MOTR).StandardWeight,
                MT = result.FirstOrDefault(x => x.ItemID == param.REW.MT).StandardWeight,
                PLT = result.FirstOrDefault(x => x.ItemID == param.REW.PLT).StandardWeight,
                SOTR = result.FirstOrDefault(x => x.ItemID == param.REW.SOTR).StandardWeight,
            };
            
            return weightsVM;
        }

        public bool updateRecoveryEstimatedWeight(WeightDetailsVM commonWeightVM, long userId)
        {
            string categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.WeightCategory, commonWeightVM.Category).Name;
            var param = new WeightParamsDTO()
            {
                userId = userId,
                categoryName = categoryName
            };
            
            getRecoveryEstimateWeightParam(param);                

            commonWeightVM.EffectiveEndDate = DateTime.MaxValue.Date;
            commonWeightVM.EffectiveStartDate = commonWeightVM.EffectiveStartDate.Date;
            productCompositionRepository.UpdateRecoveryEstimatedWeights(commonWeightVM, param);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);
            return true;
        }

        public bool RemoveWeightTransaction(int weightTransactionID, int category)
        {
            var categoryName = AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.WeightCategory, category).Name;
            if (productCompositionRepository.RemoveWeightTransaction(weightTransactionID, categoryName))
            {
                CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);
            }
            return true;
        }

        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            var transactionNote = new WeightTransactionNote
            {
                WeightTransactionID = Convert.ToInt32(transactionID),
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            productCompositionRepository.AddTransactionNote(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return productCompositionRepository.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }

        public List<InternalNoteViewModel> LoadWeightTransactionNoteByID(int weightTransactionID)
        {
            return productCompositionRepository.LoadWeightTransactionNoteByID(weightTransactionID);
        }

        public bool updateSupplyEstimatedWeight(SupplyEstimatedWeightViewModel supplyEstimatedWeightVM, long userId)
        {          
            productCompositionRepository.updateSupplyEstimatedWeight(supplyEstimatedWeightVM);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.SupplyItemWeightKey);
            CachingHelper.RemoveItem(CachKeyManager.ItemKey);
            return true;
        }

        #endregion
    }
}