﻿using CL.Framework.BLL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Announcement;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.SystemBLL
{
    public class MessageBO:BaseBO
    {
        private IMessageRepository messageRepository;
        private IClaimsRepository claimsRepository;
        public MessageBO(IMessageRepository messageRepository, IClaimsRepository claimsRepository)
        {
            this.messageRepository = messageRepository;
            this.claimsRepository = claimsRepository;
        }

        #region Notification
        public PaginationDTO<NotificationViewModel, int> LoadAllNotifications(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string userName)
        {
            var result=messageRepository.LoadAllNotifications(pageIndex, pageSize, searchText, orderBy, sortDirection, userName);
            result.DTOCollection.ForEach(c => c.RegistrationNumber = this.claimsRepository.FindClaimByClaimId(c.ClaimId).Participant.Number);
            return result;
        }

        public List<Notification> LoadUnreadNotifications(string userName)
        {
            return messageRepository.LoadUnreadNotifications(userName);
        }

        public void AddNotification(Notification notification)
        {
            messageRepository.AddNotification(notification);
        }

        public void UpdateNotificationStatus(int notificationId, string newStatus)
        {
            messageRepository.UpdateNotificationStatus(notificationId, newStatus);
        }

        public void MarkAllAsRead(string userName)
        {
            messageRepository.MarkAllAsRead(userName);
        }

        public int TotalUnreadNotification(string userName)
        {
            return messageRepository.TotalUnreadNotification(userName);
        }

        public int TotalNotification(string userName)
        {
            return messageRepository.TotalNotification(userName);
        }

        public NotificationRedirectionViewModel RedirectToClaimSummaryPage(int notificationId)
        {
            var notification = messageRepository.GetNotificationById(notificationId);
            var result = new NotificationRedirectionViewModel
            {
                ClaimId = notification.ObjectId,
                RegistrationNumber = this.claimsRepository.FindClaimByClaimId(notification.ObjectId).Participant.Number
            };
            return result;
        }

        public List<AllNotificationsExportModel> GetAllNotificationsExport(string userName, string searchText, string sortColumn, string sortDirection)
        {
            return messageRepository.GetAllNotificationsExport(userName, searchText, sortColumn, sortDirection);
        }
        #endregion

        #region Activity
        public void AddActivity(Activity activity)
        {
            messageRepository.AddActivity(activity);
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            return messageRepository.LoadAllActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, claimId);
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllCommonActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectId, string ActivityArea, int ActivityType)
        {
            return messageRepository.LoadAllCommonActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, objectId, ActivityArea, ActivityType);
        }

        public PaginationDTO<ActivityViewModel, int> LoadRecoverableMaterialActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectID)
        {
            return messageRepository.LoadRecoverableMaterialActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, objectID);
        }
        public PaginationDTO<ActivityViewModel, int> LoadRateGroupsActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int activityType)
        {
            return messageRepository.LoadRateGroupsActivities(pageIndex, pageSize, searchText, orderBy, sortDirection, activityType);
        }
        public List<AllActivitiesExportModel> GetAllActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection)
        {
            return messageRepository.GetAllActivitiesExport(cid, searchText, sortColumn, sortDirection);
        }
        public List<AllActivitiesExportModel> GetProductCompositionActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            return messageRepository.GetProductCompositionActivitiesExport(searchText, sortColumn, sortDirection);
        }
        public int TotalActivity(int claimId)
        {
            return messageRepository.TotalActivity(claimId);
        }

        //OTSTM2-745
        public PaginationDTO<ActivityViewModel, int> LoadAllGPActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return messageRepository.LoadAllGPActivities(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }
        //OTSTM2-745
        public List<AllActivitiesExportModel> GetAllGPActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            return messageRepository.GetAllGPActivitiesExport(searchText, sortColumn, sortDirection);
        }

        public List<AllActivitiesExportModel> GetAllCommonActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection, string activityArea, int activityType)
        {
            return messageRepository.GetAllCommonActivitiesExport(cid, searchText, sortColumn, sortDirection, activityArea, activityType);
        }
        #endregion


        #region //Announcement
        public void AddAnnouncement(AnnouncementVM vm)
        {
            messageRepository.AddAnnouncement(vm, SecurityContextHelper.CurrentUser.Id);
        }
        public void UpdateAnnouncement(AnnouncementVM vm)
        {
            messageRepository.UpdateAnnouncement(vm, SecurityContextHelper.CurrentUser.Id);
        }
        public void RemoveAnnouncement(int announcementID)
        {
            messageRepository.RemoveAnnouncement(announcementID);
        }
        public AnnouncementVM LoadAnnouncementByID(int announcementID)
        {
            return messageRepository.LoadAnnouncementByID(announcementID);
        }
        public PaginationDTO<AnnouncementListVM, int> LoadAnnouncementList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return messageRepository.LoadAnnouncementList(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }
        public IEnumerable<AnnouncementVM> LoadDashboardAnnouncements()
        {
            return messageRepository.LoadDashboardAnnouncements();
        }
        #endregion
    }
}
