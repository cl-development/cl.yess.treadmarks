
﻿using CL.TMS.Common.Extension;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.SystemBLL
{
    public class RegistrantManageBO
    {
        IRegistrantManageRepository registrantManageRepository;
        public RegistrantManageBO(IRegistrantManageRepository registrantManageRepository)
        {
            this.registrantManageRepository = registrantManageRepository;
        }
        public PaginationDTO<RegistrantManageModel, long> GetAllRegistrantBrief(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            var result = registrantManageRepository.LoadAllRegistrant(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
            return result;
        }

        public List<RegistrantManageModel> LoadRegistrants(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            return registrantManageRepository.LoadRegistrants(searchText, sortcolumn, sortdirection, columnSearchText);
        }
    }
}