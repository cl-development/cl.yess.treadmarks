﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.SystemBLL
{
    public class ApplicationBO
    {
        private IApplicationRepository applicationRepository;
        private IVendorRepository vendorRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;

        //private ApplicationRepository clsApplicationRepository;

        public ApplicationBO(IApplicationRepository applicationRepository, IVendorRepository vendorRepository, IApplicationInvitationRepository applicationInvitationRepository)
        {
            this.applicationRepository = applicationRepository;
            this.vendorRepository = vendorRepository;
            this.applicationInvitationRepository = applicationInvitationRepository;
        }

        public Application GetApplicationByID(int id)
        {
            var result = applicationRepository.GetSingleApplication(id);
            return result;
        }

        public Application AddApplication(Application application)
        {
            //throw new NotImplementedException();

            applicationRepository.AddApplication(application);
            return application;
        }

        public Application GetSingleApplication(int id)
        {
            var result = applicationRepository.GetSingleApplication(id);
            return result;
        }

        public ApplicationEmailModel GetApprovedApplicantInformation(int applicationId)
        {
            var vendor = this.vendorRepository.GetSingleWithApplicationID(applicationId);
            var vendorAddress = vendor.VendorAddresses.FirstOrDefault(r => r.AddressType == 1);
            var appInvitation = applicationInvitationRepository.FindApplicationInvitationByAppID(applicationId);

            Contact vendorPrimaryContact = null;

            foreach (var address in vendor.VendorAddresses)
            {
                foreach (var contact in address.Contacts)
                {
                    if (contact.IsPrimary.Value)
                    {
                        vendorPrimaryContact = contact;
                        break;
                    }
                }

                if (vendorPrimaryContact != null)
                    break;
            }

            if (vendorPrimaryContact == null)
            {
                throw new NullReferenceException("No primary contact found for vendor: " + vendor.BusinessName);
            }

            var applicationHaulerApplicationModel = new ApplicationEmailModel()
            {
                PrimaryContactFirstName = vendorPrimaryContact.FirstName,
                PrimaryContactLastName = vendorPrimaryContact.LastName,
                RegistrationNumber = vendor.Number,
                BusinessName = vendor.BusinessName,
                BusinessLegalName = vendor.BusinessName,
                Address1 = vendorAddress.Address1,
                City = vendorAddress.City,
                ProvinceState = vendorAddress.Province,
                PostalCode = vendorAddress.PostalCode,
                Country = vendorAddress.Country,
                Email = vendorPrimaryContact.Email,
                ApplicationToken = applicationInvitationRepository.GetTokenByApplicationId(applicationId),
                InvitationToken = Guid.Parse(appInvitation.TokenID),
                AssignedToUserID = vendor.Application.AssignToUser
            };

            return applicationHaulerApplicationModel;
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize,
            string searchText, string orderBy, string sortDirection)
        {
            return applicationRepository.LoadApplicationNotes(applicationId, pageIndex, pageSize, searchText, orderBy,
                sortDirection);
        }

        public void AddApplicationNote(ApplicationNote applicationNote)
        {
            applicationRepository.AddApplicationNote(applicationNote);
        }

        public IEnumerable<ProcessorListModel> GetProcessorList()
        {
            return vendorRepository.GetProcessorList();
        }

        public void UpdateApplicatioExpireDate(int applicationId, int days)
        {
            applicationRepository.UpdateApplicatioExpireDate(applicationId, days);
        }
    }
}