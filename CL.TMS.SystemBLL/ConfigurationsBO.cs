﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Security.Authorization;
using CL.TMS.IRepository.Claims;
using System.Collections;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common;
using CL.TMS.Communication.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Common.Helper;
using CL.TMS.RuleEngine.SubmitClaimBusinessRules.OTS;
using CL.TMS.ClaimCalculator;
using System.Threading.Tasks;
using CL.TMS.DataContracts;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Caching;
using CL.Framework.Logging;
using CL.TMS.DataContracts.ViewModel.GroupRate;

namespace CL.TMS.SystemBLL
{
    public class ConfigurationsBO
    {
        private IUserRepository userRepository;
        private IVendorRepository vendorRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository; //OTSTM2-400
        private IConfigurationsRepository configurationsRepository;
        private ISettingRepository settingRepository;
        public ConfigurationsBO(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator,
            IMessageRepository messageRepository, IConfigurationsRepository configurationsRepository, ISettingRepository settingRepository)
        {
            this.userRepository = userRepository;
            this.vendorRepository = vendorRepository;
            this.eventAggregator = eventAggregator;
            this.messageRepository = messageRepository; //OTSTM2-400   
            this.configurationsRepository = configurationsRepository;
            this.settingRepository = settingRepository;
        }

        #region //loading rates
        public PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return configurationsRepository.LoadRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }
        public PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId)
        {
            return configurationsRepository.LoadPIRateList(pageIndex, pageSize, searchText, orderBy, sortDirection, categoryId);
        }
        public RateDetailsVM LoadRateDetailsByTransactionID(int rateTransactionID, int category, bool isSpecific)
        {
            RateDetailsVM ratesVM = new RateDetailsVM()
            {
                category = category,
                RateTransactionID = rateTransactionID,
                isSpecific = isSpecific
            };

            var param = new RateParamsDTO()
            {
                categoryName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.RateCategory, category).Name
            };

            LoadRateDetails(param, ratesVM);

            return ratesVM;
        }
        public bool updateAppSettings(AppSettingsVM appSettingsVM, long userId)
        {
            if (appSettingsVM.bDataValidationSuccess)
            {
                if (configurationsRepository.updateAppSettings(appSettingsVM, userId))
                {
                    CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType)
        {
            return configurationsRepository.LoadAppSettingNotesByType(noteType);
        }

        public AppSettingsVM LoadAppSettings()
        {
            return configurationsRepository.LoadAppSettings();
        }

        public bool IsCollectorRateTransactionExists(int category)
        {
            return configurationsRepository.IsCollectorRateTransactionExists(category);
        }

        public bool IsFutureCollectorRateTransactionExists(int category)
        {
            return configurationsRepository.IsFutureCollectorRateTransactionExists(category);
        }

        public bool IsFutureCollectorRateExists(int category)
        {
            return configurationsRepository.IsFutureCollectorRateExists(category);
        }

        public bool IsFutureRateExists(int category)
        {
            return configurationsRepository.IsFutureRateExists(category);
        }
        public bool IsFutureRateExists(int category, bool isSpecific)
        {
            return configurationsRepository.IsFutureRateExists(category, isSpecific);
        }

        /// <summary>
        /// Loading logic from: CL.TMS.HaulerBLL.HaulerClaimBO.PopulateNorthernPremiumRate(HaulerPaymentViewModel haulerPayment, DateTime claimPeriodDate)
        /// </summary>
        /// <param name="resut"></param>
        /// <returns></returns>
        private RateDetailsVM PopulateTransportationIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, TransportationIncentiveParam param)
        {
            ratesVM.northernPremiumRate = new NorthernPremiumRateVM();
            ratesVM.ineligibleInventoryPaymentRate = new IneligibleInventoryPaymentRateVM();
            ratesVM.DOTPremiumRate = new DOTPremiumRateVM();
            //Northern premium Rate

            var n1SturgeonFallItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == param.claimTypeHauler && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            ratesVM.northernPremiumRate.N1SturgeonFallRateOnRoad = n1SturgeonFallItemRate != null ? n1SturgeonFallItemRate.ItemRate : 0;

            Rate n2SturgeonFallItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            ratesVM.northernPremiumRate.N2SturgeonFallRateOnRoad = n2SturgeonFallItemRate != null ? n2SturgeonFallItemRate.ItemRate : 0;

            Rate n3SturgeonFallItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            ratesVM.northernPremiumRate.N3SturgeonFallRateOnRoad = n3SturgeonFallItemRate != null ? n3SturgeonFallItemRate.ItemRate : 0;

            Rate n4SturgeonFallItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.SturgeonFalls);
            ratesVM.northernPremiumRate.N4SturgeonFallRateOnRoad = n4SturgeonFallItemRate != null ? n4SturgeonFallItemRate.ItemRate : 0;


            Rate n1SouthItemOnRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N1SouthRateOnRoad = n1SouthItemOnRate != null ? n1SouthItemOnRate.ItemRate : 0;

            Rate n2SouthItemOnRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N2SouthRateOnRoad = n2SouthItemOnRate != null ? n2SouthItemOnRate.ItemRate : 0;

            Rate n3SouthItemOnRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N3SouthRateOnRoad = n3SouthItemOnRate != null ? n3SouthItemOnRate.ItemRate : 0;

            Rate n4SouthItemOnRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOnRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N4SouthRateOnRoad = n4SouthItemOnRate != null ? n4SouthItemOnRate.ItemRate : 0;


            Rate n1SouthItemRateOffRoad = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n1ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N1SouthRateOffRoad = n1SouthItemRateOffRoad != null ? n1SouthItemRateOffRoad.ItemRate : 0;

            Rate n2SouthItemRateOffRoad = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n2ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N2SouthRateOffRoad = n2SouthItemRateOffRoad != null ? n2SouthItemRateOffRoad.ItemRate : 0;

            Rate n3SouthItemRateOffRoad = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n3ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N3SouthRateOffRoad = n3SouthItemRateOffRoad != null ? n3SouthItemRateOffRoad.ItemRate : 0;

            Rate n4SouthItemRateOffRoad = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeNorth && c.ClaimType == 3 && c.ItemType == param.itemTypeOffRoad && c.SourceZoneID == param.n4ZoneId && c.GreaterZone == TreadMarksConstants.South);
            ratesVM.northernPremiumRate.N4SouthRateOffRoad = n4SouthItemRateOffRoad != null ? n4SouthItemRateOffRoad.ItemRate : 0;

            //DOTPreminum Rate
            //ClaimType == 3 : Hauler
            //PaymentType == 2 : DOT Premium

            //For northern pick ups, rate will be based on Pick up zone only.
            Rate n1ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n1ZoneId);
            ratesVM.DOTPremiumRate.N1OffRoadRate = n1ZoneItemRate != null ? n1ZoneItemRate.ItemRate : 0;

            Rate n2ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n2ZoneId);
            ratesVM.DOTPremiumRate.N2OffRoadRate = n2ZoneItemRate != null ? n2ZoneItemRate.ItemRate : 0;

            Rate n3ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n3ZoneId);
            ratesVM.DOTPremiumRate.N3OffRoadRate = n3ZoneItemRate != null ? n3ZoneItemRate.ItemRate : 0;

            Rate n4ZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.n4ZoneId);
            ratesVM.DOTPremiumRate.N4OffRoadRate = n4ZoneItemRate != null ? n4ZoneItemRate.ItemRate : 0;

            // For South DOT pick ups, rate will be based on pickup zone and drop off zone.
            Rate mooseCreekZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.mooseCreekZoneId);
            ratesVM.DOTPremiumRate.MooseCreekOffRoadRate = mooseCreekZoneItemRate != null ? mooseCreekZoneItemRate.ItemRate : 0;

            Rate gtaZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.gtaZoneId);
            ratesVM.DOTPremiumRate.GTAOffRoadRate = gtaZoneItemRate != null ? gtaZoneItemRate.ItemRate : 0;

            Rate wtcZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.wtcZoneId);
            ratesVM.DOTPremiumRate.WTCOffRoadRate = wtcZoneItemRate != null ? wtcZoneItemRate.ItemRate : 0;

            Rate sturgeonFallsZoneItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeDOT && c.ClaimType == 3 && c.SourceZoneID == param.southZoneId && c.DeliveryZoneID == param.sturgeonFallsZoneId);
            ratesVM.DOTPremiumRate.SturgeonFallsOffRoadRate = sturgeonFallsZoneItemRate != null ? sturgeonFallsZoneItemRate.ItemRate : 0;

            //Processor TI Rate
            //ClaimType == 3 : Hauler
            //PaymentType == 3 : Processor TI
            //ItemType == 1 : On-Road
            //ItemType == 2 : Off-Road
            Rate mooseCreekZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOnRoad);
            ratesVM.ineligibleInventoryPaymentRate.MooseCreakOnRoadRate = mooseCreekZoneOnRoadItemRate != null ? mooseCreekZoneOnRoadItemRate.ItemRate : 0;

            Rate mooseCreekZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.mooseCreekZoneId && c.ItemType == param.itemTypeOffRoad);
            ratesVM.ineligibleInventoryPaymentRate.MooseCreakOffRoadRate = mooseCreekZoneOffRoadItemRate != null ? mooseCreekZoneOffRoadItemRate.ItemRate : 0;

            Rate gtaZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOnRoad);
            ratesVM.ineligibleInventoryPaymentRate.GTAOnRoadRate = gtaZoneOnRoadItemRate != null ? gtaZoneOnRoadItemRate.ItemRate : 0;

            Rate gtaZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.gtaZoneId && c.ItemType == param.itemTypeOffRoad);
            ratesVM.ineligibleInventoryPaymentRate.GTAOffRoadRate = gtaZoneOffRoadItemRate != null ? gtaZoneOffRoadItemRate.ItemRate : 0;

            Rate wtcZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOnRoad);
            ratesVM.ineligibleInventoryPaymentRate.WTCOnRoadRate = wtcZoneOnRoadItemRate != null ? wtcZoneOnRoadItemRate.ItemRate : 0;

            Rate wtcZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.wtcZoneId && c.ItemType == param.itemTypeOffRoad);
            ratesVM.ineligibleInventoryPaymentRate.WTCOffRoadRate = wtcZoneOffRoadItemRate != null ? wtcZoneOffRoadItemRate.ItemRate : 0;

            Rate sturgeonFallsZoneOnRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOnRoad);
            ratesVM.ineligibleInventoryPaymentRate.SturgeonFallsOnRoadRate = sturgeonFallsZoneOnRoadItemRate != null ? sturgeonFallsZoneOnRoadItemRate.ItemRate : 0;

            Rate sturgeonFallsZoneOffRoadItemRate = result.FirstOrDefault(c => c.PaymentType == param.paymentTypeProcessorTI && c.ClaimType == 4 && c.DeliveryZoneID == param.sturgeonFallsZoneId && c.ItemType == param.itemTypeOffRoad);
            ratesVM.ineligibleInventoryPaymentRate.SturgeonFallsOffRoadRate = sturgeonFallsZoneOffRoadItemRate != null ? sturgeonFallsZoneOffRoadItemRate.ItemRate : 0;

            return ratesVM;
        }

        private RateDetailsVM PopulateCollectorAllowanceRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            int colloctorTireType = (int)ClaimType.Collector;
            int itemCategory = (int)ItemCategoryEnum.CollectorTireType; //5
            ratesVM.collectorAllowanceRate = new CollectorAllowanceRateVM()
            {
                PLT = result.Where(i => i.item.ShortName == "PLT" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                MT = result.Where(i => i.item.ShortName == "MT" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                AGLS = result.Where(i => i.item.ShortName == "AGLS" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                IND = result.Where(i => i.item.ShortName == "IND" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                SOTR = result.Where(i => i.item.ShortName == "SOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                MOTR = result.Where(i => i.item.ShortName == "MOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                LOTR = result.Where(i => i.item.ShortName == "LOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
                GOTR = result.Where(i => i.item.ShortName == "GOTR" && i.item.IsActive && i.item.ItemCategory == itemCategory).FirstOrDefault().ItemRate,
            };
            return ratesVM;
        }
        private RateDetailsVM PopulateManufacturingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            var tempRPM = DataLoader.RPMSPSTransactionProducts;
            ratesVM.manufacturingIncentiveRate = new ManufacturingIncentiveRateVM()
            {
                Calendared = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Calendared")).ID).ItemRate,
                Extruded = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Extruded")).ID).ItemRate,
                Molded = result.FirstOrDefault(x => x.ItemID == tempRPM.FirstOrDefault(r => r.ShortName.Contains("Molded")).ID).ItemRate
            };
            return ratesVM;
        }
        private RateDetailsVM PopulateProcessingIncentiveRateVM(List<Rate> result, RateDetailsVM ratesVM, ProcessingIncentiveParam param)
        {
            var SPS = result.Where(x => x.PaymentType == param.spsPaymentType);
            var PIT = result.Where(x => x.PaymentType == param.pitPaymentType);
            ratesVM.processingIncentiveRate = new ProcessingIncentiveRateVM()
            {
                #region //SPS OnRoad
                TDP1OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OnRoad).ItemRate,
                TDP1FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOnRoad).ItemRate,
                TDP2OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OnRoad).ItemRate,
                TDP2FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOnRoad).ItemRate,
                TDP3OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OnRoad).ItemRate,
                TDP3FeedstockOnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOnRoad).ItemRate,
                TDP4OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OnRoad).ItemRate,
                TDP5OnRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OnRoad).ItemRate,
                #endregion

                #region //SPS OffRoad
                TDP1OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1OffRoad).ItemRate,
                TDP1FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp1FeedstockOffRoad).ItemRate,
                TDP2OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2OffRoad).ItemRate,
                TDP2FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp2FeedstockOffRoad).ItemRate,
                TDP3OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3OffRoad).ItemRate,
                TDP3FeedstockOffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp3FeedstockOffRoad).ItemRate,
                TDP4OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp4OffRoad).ItemRate,
                TDP5OffRoad = SPS.FirstOrDefault(x => x.ItemID == param.tdp5OffRoad).ItemRate,
                #endregion

                #region //PIT
                TDP1 = PIT.FirstOrDefault(x => x.ItemID == param.tdp1).ItemRate,
                TDP2 = PIT.FirstOrDefault(x => x.ItemID == param.tdp2).ItemRate,
                TDP3 = PIT.FirstOrDefault(x => x.ItemID == param.tdp3).ItemRate,
                TDP4FF = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FF).ItemRate,
                TDP4FFNoPI = PIT.FirstOrDefault(x => x.ItemID == param.tdp4FFNoPI).ItemRate,
                TDP5FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5FT).ItemRate,
                TDP5NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp5NT).ItemRate,
                TDP6NP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6NP).ItemRate,
                TDP6FP = PIT.FirstOrDefault(x => x.ItemID == param.tdp6FP).ItemRate,
                TDP7NT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7NT).ItemRate,
                TDP7FT = PIT.FirstOrDefault(x => x.ItemID == param.tdp7FT).ItemRate,
                TransferFibreRubber = PIT.FirstOrDefault(x => x.ItemID == param.transferFibreRubber).ItemRate,
                #endregion
            };
            return ratesVM;
        }
        private RateDetailsVM PopulateTireStewardshipFeesRateVM(List<Rate> result, RateDetailsVM ratesVM, TireStewardshipFeeParam param)
        {
            if ((result == null) || (result.Count == 0))
            {
                ratesVM.tireStewardshipFeesRate = new TireStewardshipFeesRateVM(param);
            }
            else
            {
                ratesVM.tireStewardshipFeesRate = new TireStewardshipFeesRateVM(param)
                {
                    C1 = result.FirstOrDefault(i => i.ItemID == param.c1) != null ? result.FirstOrDefault(i => i.ItemID == param.c1).ItemRate : 0,
                    C2 = result.FirstOrDefault(i => i.ItemID == param.c2) != null ? result.FirstOrDefault(i => i.ItemID == param.c2).ItemRate : 0,
                    C3 = result.FirstOrDefault(i => i.ItemID == param.c3) != null ? result.FirstOrDefault(i => i.ItemID == param.c3).ItemRate : 0,
                    C4 = result.FirstOrDefault(i => i.ItemID == param.c4) != null ? result.FirstOrDefault(i => i.ItemID == param.c4).ItemRate : 0,
                    C5 = result.FirstOrDefault(i => i.ItemID == param.c5) != null ? result.FirstOrDefault(i => i.ItemID == param.c5).ItemRate : 0,
                    C6 = result.FirstOrDefault(i => i.ItemID == param.c6) != null ? result.FirstOrDefault(i => i.ItemID == param.c6).ItemRate : 0,
                    C7 = result.FirstOrDefault(i => i.ItemID == param.c7) != null ? result.FirstOrDefault(i => i.ItemID == param.c7).ItemRate : 0,
                    C8 = result.FirstOrDefault(i => i.ItemID == param.c8) != null ? result.FirstOrDefault(i => i.ItemID == param.c8).ItemRate : 0,
                    C9 = result.FirstOrDefault(i => i.ItemID == param.c9) != null ? result.FirstOrDefault(i => i.ItemID == param.c9).ItemRate : 0,
                    C10 = result.FirstOrDefault(i => i.ItemID == param.c10) != null ? result.FirstOrDefault(i => i.ItemID == param.c10).ItemRate : 0,
                    C11 = result.FirstOrDefault(i => i.ItemID == param.c11) != null ? result.FirstOrDefault(i => i.ItemID == param.c11).ItemRate : 0,
                    C12 = result.FirstOrDefault(i => i.ItemID == param.c12) != null ? result.FirstOrDefault(i => i.ItemID == param.c12).ItemRate : 0,
                    C13 = result.FirstOrDefault(i => i.ItemID == param.c13) != null ? result.FirstOrDefault(i => i.ItemID == param.c13).ItemRate : 0,
                    C14 = result.FirstOrDefault(i => i.ItemID == param.c14) != null ? result.FirstOrDefault(i => i.ItemID == param.c14).ItemRate : 0,
                    C15 = result.FirstOrDefault(i => i.ItemID == param.c15) != null ? result.FirstOrDefault(i => i.ItemID == param.c15).ItemRate : 0,
                    C16 = result.FirstOrDefault(i => i.ItemID == param.c16) != null ? result.FirstOrDefault(i => i.ItemID == param.c16).ItemRate : 0,
                    C17 = result.FirstOrDefault(i => i.ItemID == param.c17) != null ? result.FirstOrDefault(i => i.ItemID == param.c17).ItemRate : 0,
                    C18 = result.FirstOrDefault(i => i.ItemID == param.c18) != null ? result.FirstOrDefault(i => i.ItemID == param.c18).ItemRate : 0,
                };
            }
            return ratesVM;
        }
        private RateDetailsVM PopulatePenaltyRateVM(List<Rate> result, RateDetailsVM ratesVM)
        {
            int iclaimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue;
            int ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue;//6
            ratesVM.penaltyRate = new PenaltyRateVM()
            {
                StewardRemittancePenaltyRate = result.FirstOrDefault(x => x.PaymentType == ratePaymentType && x.ClaimType == iclaimType) != null ? result.FirstOrDefault(x => x.PaymentType == ratePaymentType && x.ClaimType == iclaimType).ItemRate : 0
            };
            return ratesVM;
        }

        #endregion

        #region AddRates       
        public bool AddNewRate(RateDetailsVM commonRateVM, long userId, int category)
        {
            if (category == (int)ClaimType.Processor)
            {
                //check global processor exists future rate only.
                if (!commonRateVM.isSpecific && configurationsRepository.IsFutureRateExists(category, false))
                {
                    return false;
                }
            }
            else
            {
                if (configurationsRepository.IsFutureRateExists(category))//already exists future rate, not allow add more.
                {
                    return false;
                }
            }
            DateTime effectiveStartDate = new DateTime(commonRateVM.effectiveStartDate.Year, commonRateVM.effectiveStartDate.Month, 1);
            // save effectiveStartDate Date only. 
            commonRateVM.effectiveStartDate = effectiveStartDate.Date;

            if (effectiveStartDate.Date <= DateTime.Today.Date)//new rate effective date must be future date
            {
                return false;///QATEST
            }
            var rateTransaction = new RateTransaction();
            rateTransaction.CreatedDate = DateTime.UtcNow;
            rateTransaction.CreatedByID = userId;

            rateTransaction.EffectiveStartDate = commonRateVM.effectiveStartDate;
            if (commonRateVM.isSpecific)
            {
                rateTransaction.EffectiveEndDate = commonRateVM.effectiveEndDate.AddMonths(1).AddDays(-1).Date;
            }
            else
            {
                rateTransaction.EffectiveEndDate = DateTime.MaxValue.Date;
            }

            rateTransaction.Category = category;

            if (!string.IsNullOrEmpty(commonRateVM.note))
            {
                //add note
                var rateTransactionNote = new RateTransactionNote();
                rateTransactionNote.RateTransaction = rateTransaction;
                rateTransactionNote.Note = commonRateVM.note;
                rateTransactionNote.CreatedDate = DateTime.UtcNow;
                rateTransactionNote.UserID = userId;
                rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(rateTransactionNote);
            }

            var param = new RateParamsDTO()
            {
                userId = userId,
                categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, commonRateVM.category).Name
            };
            if (param.categoryName == TreadMarksConstants.TransportationIncentiveRates)
            {
                AddHaulerRatesToRateTransaction(commonRateVM, rateTransaction, param);
            }
            else if (param.categoryName == TreadMarksConstants.CollectionAllowanceRates)
            {
                if (!BondCollectorRatesToRateTransaction(commonRateVM, rateTransaction, param))
                {
                    return false;//failed adding rate;
                }
            }
            else if (param.categoryName == TreadMarksConstants.ProcessingIncentiveRates)
            {
                rateTransaction.IsSpecificRate = commonRateVM.isSpecific;
                AddProcessingIncentiveRates(commonRateVM, rateTransaction, param);
                if (commonRateVM.isSpecific && commonRateVM.piSpecificRate != null && commonRateVM.piSpecificRate.AssignedProcessIDs != null)
                {
                    AddPIRateMapping(commonRateVM, rateTransaction);
                }
            }
            else if (param.categoryName == TreadMarksConstants.ManufacturingIncentiveRates)
            {
                AddManufacturingIncentiveRates(commonRateVM, rateTransaction, param);
            }
            else if (param.categoryName == TreadMarksConstants.TireStewardshipFeeRates)
            {
                AddStewardRatesToRateTransaction(commonRateVM, rateTransaction, param);
            }
            //else if (param.categoryName == TreadMarksConstants.EstimatedWeightsRates)
            //{
            //    AddEstimateWeightRates(commonRateVM, rateTransaction, param);
            //    //CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);
            //}
            else if (param.categoryName == TreadMarksConstants.RemittancePenaltyRates)
            {
                AddStewardRemittancePenaltyRates(commonRateVM, rateTransaction, param);
            }

            configurationsRepository.AddRates(rateTransaction, param);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.RateKey);

            return true;
        }

        public IEnumerable<VendorClaimRateGroupInfo> GetVendorGroupInfoByDateVid(int vendorId, DateTime claimPeriod, DateTime? claimPeriodEnd, string rateGroupCategory)
        {
            return configurationsRepository.GetVendorGroupInfoByDateVid(vendorId, claimPeriod, claimPeriodEnd, rateGroupCategory);
        }
        private void AddHaulerRatesToRateTransaction(RateDetailsVM transactionIncentiveRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            getTransactionIncentiveParam(param);
            rateTransaction.Rates = new List<Rate>();
            #region adding Northern Premium Rate

            var n1SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n1SturgeonFallRateOnRoad);

            var n1SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n1SouthRateOnRoad);

            var n1SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N1SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n1SouthRateOffRoad);

            var n2SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n2SturgeonFallRateOnRoad);

            var n2SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n2SouthRateOnRoad);

            var n2SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N2SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n2SouthRateOffRoad);

            var n3SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n3SturgeonFallRateOnRoad);

            var n3SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n3SouthRateOnRoad);

            var n3SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N3SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n3SouthRateOffRoad);

            var n4SturgeonFallRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.SturgeonFalls,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SturgeonFallRateOnRoad
            };
            rateTransaction.Rates.Add(n4SturgeonFallRateOnRoad);

            var n4SouthRateOnRoad = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOnRoad
            };
            rateTransaction.Rates.Add(n4SouthRateOnRoad);

            var n4SouthRateOffRoad = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeNorth,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                GreaterZone = TreadMarksConstants.South,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.northernPremiumRate.N4SouthRateOffRoad
            };
            rateTransaction.Rates.Add(n4SouthRateOffRoad);

            #endregion

            #region adding DOT Premium Rate
            //N1 - N4
            var n1DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n1ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N1OffRoadRate
            };
            rateTransaction.Rates.Add(n1DOTRate);

            var n2DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n2ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N2OffRoadRate
            };
            rateTransaction.Rates.Add(n2DOTRate);

            var n3DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n3ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N3OffRoadRate
            };
            rateTransaction.Rates.Add(n3DOTRate);

            var n4DOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.n4ZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.N4OffRoadRate
            };
            rateTransaction.Rates.Add(n4DOTRate);

            var mooseCreekDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.MooseCreekOffRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekDOTRate);

            var gtaDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.GTAOffRoadRate
            };
            rateTransaction.Rates.Add(gtaDOTRate);

            var wtcDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.WTCOffRoadRate
            };
            rateTransaction.Rates.Add(wtcDOTRate);

            var sturgeonFallsDOTRate = new Rate
            {
                PaymentType = param.TI.paymentTypeDOT,
                ClaimType = param.TI.claimTypeHauler,
                SourceZoneID = param.TI.southZoneId,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.DOTPremiumRate.SturgeonFallsOffRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsDOTRate);

            #endregion

            #region adding TI Rate

            var mooseCreekTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOnRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekTIOnRoadRate);

            var mooseCreekTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.mooseCreekZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.MooseCreakOffRoadRate
            };
            rateTransaction.Rates.Add(mooseCreekTIOffRoadRate);

            var gtaTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOnRoadRate
            };
            rateTransaction.Rates.Add(gtaTIOnRoadRate);

            var gtaTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.gtaZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.GTAOffRoadRate
            };
            rateTransaction.Rates.Add(gtaTIOffRoadRate);

            var wtcTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOnRoadRate
            };
            rateTransaction.Rates.Add(wtcTIOnRoadRate);

            var wtcTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.wtcZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.WTCOffRoadRate
            };
            rateTransaction.Rates.Add(wtcTIOffRoadRate);

            var sturgeonFallsTIOnRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOnRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOnRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsTIOnRoadRate);

            var sturgeonFallsTIOffRoadRate = new Rate
            {
                ItemType = param.TI.itemTypeOffRoad,
                PaymentType = param.TI.paymentTypeProcessorTI,
                ClaimType = param.TI.claimTypeProcessor,
                DeliveryZoneID = param.TI.sturgeonFallsZoneId,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = transactionIncentiveRateVM.ineligibleInventoryPaymentRate.SturgeonFallsOffRoadRate
            };
            rateTransaction.Rates.Add(sturgeonFallsTIOffRoadRate);

            #endregion
        }
        private void AddStewardRatesToRateTransaction(RateDetailsVM tireStewardshipFeeVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            getTireStewardshipFeeParam(param);

            var tsfItems = tireStewardshipFeeVM.tireStewardshipFeesRate.GetType().GetProperties().Where(x => x.PropertyType.Name == "Decimal");

            var paramItems = param.TFS.GetType().GetProperties();
            foreach (var property in tsfItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.TFS);
                var tsfRate = new Rate
                {
                    ItemID = paramID,
                    ClaimType = param.TFS.claimType,
                    EffectiveStartDate = rateTransaction.EffectiveStartDate,
                    EffectiveEndDate = DateTime.MaxValue.Date,
                    RateTransaction = rateTransaction,
                    ItemRate = (decimal)property.GetValue(tireStewardshipFeeVM.tireStewardshipFeesRate),
                };
                rateTransaction.Rates.Add(tsfRate);
            }
        }
        private void AddManufacturingIncentiveRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            getMaunfacturingIncentiveParam(param);
            var calendared = new Rate
            {
                ItemID = param.MI.calendaredId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Calendared,
            };
            var extruded = new Rate
            {
                ItemID = param.MI.extrudedId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Extruded,
            };
            var molded = new Rate
            {
                ItemID = param.MI.moldedId,
                ClaimType = param.MI.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.manufacturingIncentiveRate.Molded,
            };
            rateTransaction.Rates.Add(calendared);
            rateTransaction.Rates.Add(extruded);
            rateTransaction.Rates.Add(molded);
        }

        //assume all rate exists if period exists, those are created by year-end script together
        private bool BondCollectorRatesToRateTransaction(RateDetailsVM collectorRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            List<Period> futurePeriodList = configurationsRepository.LoadFuturePeriodByType(DateTime.Now, ((int)ClaimType.Collector));
            List<Period> bondPeriodList = configurationsRepository.LoadFuturePeriodByType(collectorRateVM.effectiveStartDate, ((int)ClaimType.Collector));
            List<Rate> rates = configurationsRepository.LoadRatesForPeriods(bondPeriodList.AsEnumerable().Select(r => r.ID));

            Period startingPeriod = configurationsRepository.GetPeriodByDate(collectorRateVM.effectiveStartDate, ((int)ClaimType.Collector));

            rates.ForEach(i =>
            {
                i.RateTransactionID = rateTransaction.ID;
                switch (i.item.ShortName)
                {
                    case "PLT":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.PLT;
                        break;
                    case "MT":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.MT;
                        break;
                    case "AGLS":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.AGLS;
                        break;
                    case "IND":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.IND;
                        break;
                    case "SOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.SOTR;
                        break;
                    case "MOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.MOTR;
                        break;
                    case "LOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.LOTR;
                        break;
                    case "GOTR":
                        i.ItemRate = collectorRateVM.collectorAllowanceRate.GOTR;
                        break;
                    default:
                        break;
                }
            });
            return true;
        }
        private void AddProcessingIncentiveRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            var piRate = new Rate();
            getProcessingIncentiveParam(param);


            var piItems = commonRateVM.processingIncentiveRate.GetType().GetProperties();
            var paramItems = param.PI.GetType().GetProperties();
            foreach (var property in piItems)
            {
                int paramID = (int)paramItems.FirstOrDefault(x => x.Name.ToLower() == property.Name.ToLower()).GetValue(param.PI);
                if (property.Name.IndexOf("OnRoad") > -1)
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.spsPaymentType,
                        ItemType = param.PI.onRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
                else if (property.Name.IndexOf("OffRoad") > -1)
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.spsPaymentType,
                        ItemType = param.PI.offRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
                else
                {
                    piRate = new Rate
                    {
                        ItemID = paramID,
                        ClaimType = param.PI.claimType,
                        PaymentType = param.PI.pitPaymentType,
                        ItemType = param.PI.offRoad,
                        EffectiveStartDate = rateTransaction.EffectiveStartDate,
                        EffectiveEndDate = commonRateVM.isSpecific ? rateTransaction.EffectiveEndDate : DateTime.MaxValue.Date,
                        RateTransaction = rateTransaction,
                        IsSpecificRate = commonRateVM.isSpecific,
                        ItemRate = (decimal)property.GetValue(commonRateVM.processingIncentiveRate)
                    };
                    rateTransaction.Rates.Add(piRate);
                }
            }
        }
        private void AddPIRateMapping(RateDetailsVM commonRateVM, RateTransaction rateTransaction)
        {
            rateTransaction.PIRateMappings = new List<VendorRate>();
            var piRate = new VendorRate();
            foreach (var item in commonRateVM.piSpecificRate.AssignedProcessIDs)
            {
                piRate = new VendorRate()
                {
                    RateTransactionID = rateTransaction.ID,
                    VendorID = item.ItemID.Value
                };
                rateTransaction.PIRateMappings.Add(piRate);
            }
        }

        private void AddStewardRemittancePenaltyRates(RateDetailsVM commonRateVM, RateTransaction rateTransaction, RateParamsDTO param)
        {
            rateTransaction.Rates = new List<Rate>();
            var piRate = new Rate();
            getStewareRemittancePenaltyParam(param);
            var penaltyRate = new Rate
            {
                PaymentType = param.PE.ratePaymentType,
                ClaimType = param.PE.claimType,
                EffectiveStartDate = rateTransaction.EffectiveStartDate,
                EffectiveEndDate = DateTime.MaxValue.Date,
                RateTransaction = rateTransaction,
                ItemRate = commonRateVM.penaltyRate.StewardRemittancePenaltyRate,
            };
            rateTransaction.Rates.Add(penaltyRate);
        }
        #endregion

        #region Update Rates
        public bool updateRate(RateDetailsVM commonRateVM, long userId)
        {
            string categoryName = Configuration.AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, commonRateVM.category).Name;
            var param = new RateParamsDTO()
            {
                userId = userId,
                categoryName = categoryName
            };
            if (categoryName == TreadMarksConstants.TransportationIncentiveRates)
            {
                getTransactionIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.CollectionAllowanceRates)
            {
                getCollectorRateParam(param);
            }
            else if (categoryName == TreadMarksConstants.ProcessingIncentiveRates)
            {
                getProcessingIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.ManufacturingIncentiveRates)
            {
                getMaunfacturingIncentiveParam(param);
            }
            else if (categoryName == TreadMarksConstants.TireStewardshipFeeRates)
            {
                getTireStewardshipFeeParam(param);
            }
            else if (param.categoryName == TreadMarksConstants.RemittancePenaltyRates)
            {
                getStewareRemittancePenaltyParam(param);
            }
            //else if (categoryName == TreadMarksConstants.EstimatedWeightsRates)
            //{
            //    getEstimateWeightParam(param);
            //    //CachingHelper.RemoveItem(CachKeyManager.ItemWeightKey);
            //}

            commonRateVM.effectiveEndDate = commonRateVM.isSpecific ? commonRateVM.effectiveEndDate.AddMonths(1).AddDays(-1).Date : DateTime.MaxValue.Date;
            commonRateVM.effectiveStartDate = commonRateVM.effectiveStartDate.Date;
            configurationsRepository.UpdateRates(commonRateVM, param);
            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.RateKey);
            return true;
        }
        #endregion

        #region prepare parameters and load data
        private void getCollectorRateParam(RateParamsDTO param)
        {
            param.CA = new CollectorAllowanceParam()
            {
            };
        }
        private void getTransactionIncentiveParam(RateParamsDTO param)
        {
            param.TI = new TransportationIncentiveParam()
            {
                //payment Type
                paymentTypeNorth = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.NorthernPremium).DefinitionValue,//1
                paymentTypeDOT = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.DOTPremium).DefinitionValue,//2
                paymentTypeProcessorTI = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorTIRates).DefinitionValue,//3
                //claim type
                claimTypeHauler = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Hauler).DefinitionValue,
                claimTypeProcessor = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                //item type
                itemTypeOnRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OnRoad).DefinitionValue,
                itemTypeOffRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, TreadMarksConstants.OffRoad).DefinitionValue,
                pickupRateGroup = TreadMarksConstants.PickupGroup,
                deliveryRateGroup = TreadMarksConstants.DeliveryGroup
            };
        }
        private void getMaunfacturingIncentiveParam(RateParamsDTO param)
        {
            var tempRPM = DataLoader.RPMSPSTransactionProducts;
            param.MI = new ManufacturingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.RPM).DefinitionValue,
                calendaredId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Calendared")).ID,
                extrudedId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Extruded")).ID,
                moldedId = tempRPM.FirstOrDefault(r => r.ShortName.Contains("Molded")).ID,
            };
        }
        private void getTireStewardshipFeeParam(RateParamsDTO param)
        {
            param.TFS = new TireStewardshipFeeParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,
                c1 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C1").ID,
                c2 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C2").ID,
                c3 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C3").ID,
                c4 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C4").ID,
                c5 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C5").ID,
                c6 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C6").ID,
                c7 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C7").ID,
                c8 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C8").ID,
                c9 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C9").ID,
                c10 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C10").ID,
                c11 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C11").ID,
                c12 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C12").ID,
                c13 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C13").ID,
                c14 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C14").ID,
                c15 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C15").ID,
                c16 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C16").ID,
                c17 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C17").ID,
                c18 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C18").ID,
                cn1 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C1").Name,
                cn2 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C2").Name,
                cn3 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C3").Name,
                cn4 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C4").Name,
                cn5 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C5").Name,
                cn6 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C6").Name,
                cn7 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C7").Name,
                cn8 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C8").Name,
                cn9 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C9").Name,
                cn10 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C10").Name,
                cn11 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C11").Name,
                cn12 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C12").Name,
                cn13 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C13").Name,
                cn14 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C14").Name,
                cn15 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C15").Name,
                cn16 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C16").Name,
                cn17 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C17").Name,
                cn18 = DataLoader.Items.FirstOrDefault(x => x.ShortName.Trim().ToUpper() == "C18").Name,
            };
        }
        private void getProcessingIncentiveParam(RateParamsDTO param)
        {
            var itemParams = new
            {
                pitItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue,
                spsItemCategory = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue,
                onRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OnRoad").DefinitionValue,
                offRoad = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemType, "OffRoad").DefinitionValue,
                spsPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorSPS").DefinitionValue,//4
                pitPaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, "ProcessorPITOutbound").DefinitionValue//5
            };

            var itemList = new
            {
                spsOnRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.onRoad),
                spsOffRoad = DataLoader.Items.Where(i => i.ItemCategory == itemParams.spsItemCategory && i.ItemType == itemParams.offRoad),
                pit = DataLoader.Items.Where(i => i.ItemCategory == itemParams.pitItemCategory)
            };

            param.PI = new ProcessingIncentiveParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue,
                pitItemCategory = itemParams.pitItemCategory,
                spsItemCategory = itemParams.spsItemCategory,
                onRoad = itemParams.onRoad,
                offRoad = itemParams.offRoad,
                spsPaymentType = itemParams.spsPaymentType,
                pitPaymentType = itemParams.pitPaymentType,

                spsOffRoad = itemList.spsOffRoad,
                spsOnRoad = itemList.spsOnRoad,
                pit = itemList.pit,

                #region //SPS OnRoad
                tdp1OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OnRoad = itemList.spsOnRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //SPS OffRoad
                tdp1OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && !i.Description.Contains("Feedstock")).ID,
                tdp1FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP1") && i.Description.Contains("Feedstock")).ID,
                tdp2OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && !i.Description.Contains("Feedstock")).ID,
                tdp2FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP2") && i.Description.Contains("Feedstock")).ID,
                tdp3OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && !i.Description.Contains("Feedstock")).ID,
                tdp3FeedstockOffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP3") && i.Description.Contains("Feedstock")).ID,
                tdp4OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP4")).ID,
                tdp5OffRoad = itemList.spsOffRoad.FirstOrDefault(i => i.ShortName.Contains("TDP5")).ID,
                #endregion

                #region //PIT
                tdp1 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP1")).ID,
                tdp2 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP2")).ID,
                tdp3 = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP3")).ID,
                tdp4FF = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && !i.Description.Contains("PI")).ID,
                tdp4FFNoPI = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP4") && i.Description.Contains("PI")).ID,
                tdp5FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5FT")).ID,
                tdp5NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP5NT")).ID,
                tdp6NP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6NP")).ID,
                tdp6FP = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP6FP")).ID,
                tdp7NT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7NT")).ID,
                tdp7FT = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TDP7FT")).ID,
                transferFibreRubber = itemList.pit.FirstOrDefault(i => i.ShortName.Contains("TransferFiberRubber")).ID,
                #endregion
            };
            param.PI.items = new int[] {
                #region // SPS On-Road
                param.PI.tdp1OnRoad,
                param.PI.tdp1FeedstockOnRoad,
                param.PI.tdp2OnRoad,
                param.PI.tdp2FeedstockOnRoad,
                param.PI.tdp3OnRoad,
                param.PI.tdp3FeedstockOnRoad,
                param.PI.tdp4OnRoad,
                param.PI.tdp5OnRoad,
                #endregion

                #region //SPS OffRoad
                param.PI.tdp1OffRoad,
                param.PI.tdp1FeedstockOffRoad,
                param.PI.tdp2OffRoad,
                param.PI.tdp2FeedstockOffRoad,
                param.PI.tdp3OffRoad,
                param.PI.tdp3FeedstockOffRoad,
                param.PI.tdp4OffRoad,
                param.PI.tdp5OffRoad,
                #endregion

                #region //PIT
                param.PI.tdp1,
                param.PI.tdp2,
                param.PI.tdp3,
                param.PI.tdp4FF,
                param.PI.tdp4FFNoPI,
                param.PI.tdp5FT,
                param.PI.tdp5NT,
                param.PI.tdp6NP,
                param.PI.tdp6FP,
                param.PI.tdp7NT,
                param.PI.tdp7FT,
                param.PI.transferFibreRubber,
                #endregion
            };
        }
        private void getStewareRemittancePenaltyParam(RateParamsDTO param)
        {
            param.PE = new PenaltyParam()
            {
                claimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue,
                ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue//6
            };
        }
        private void loadTI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 3;
            getTransactionIncentiveParam(param);
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                ratesVM.tiVM = new TransportationIncentiveViewModel()
                {
                    EffectiveDate = DateTime.Now.AddMonths(1),
                    DeliveryRateGroupId = 0,
                    PickupRateGroupId = 0,
                    TIRateGroupList = new TIRateGroupList()
                    {
                        deliveryRateGroups = configurationsRepository.LoadEffectiveRateGroups(TreadMarksConstants.DeliveryGroup),
                        pickupRateGroups = configurationsRepository.LoadEffectiveRateGroups(TreadMarksConstants.PickupGroup),
                    },
                };
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rateTransaction = configurationsRepository.getRateTransactionByID(ratesVM.RateTransactionID);
                if (rateTransaction.PickupRateGroupId.HasValue && rateTransaction.DeliveryRateGroupId.HasValue)
                {
                    ratesVM.tiVM = getTIVMByRateTransaction(rateTransaction, param);
                }
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM.effectiveStartDate = (DateTime)rateTransaction.EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rateTransaction.EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rateTransaction.EffectiveStartDate;
            }
        }
        private void loadCollector(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            DateTime now = DateTime.Now;
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                int iclaimType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Collector).DefinitionValue;
                var tiRates = configurationsRepository.GetCurrentCollectorRates(iclaimType).ToList();
                ratesVM = PopulateCollectorAllowanceRateVM(tiRates, ratesVM);
                ratesVM.collectorAllowanceRate.selectableTransactionPeriods = configurationsRepository.LoadFuturePeriodByType(now, (int)ClaimType.Collector);
                ratesVM.effectiveStartDate = (DateTime)tiRates.FirstOrDefault().period.StartDate;//new DateTime(DateTime.Now.Year + 1, 1, 1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
                ratesVM.collectorAllowanceRate.AvailableLatestPeriod = configurationsRepository.GetLatestPeriod(ratesVM.category);
                ratesVM.collectorAllowanceRate.CurrentPeriod = configurationsRepository.GetPeriodByDate(now, (int)ClaimType.Collector);
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateCollectorAllowanceRateVM(rates, ratesVM);
                ratesVM.collectorAllowanceRate.selectableTransactionPeriods = configurationsRepository.LoadFuturePeriodByType(now, (int)ClaimType.Collector);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().period.StartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().period.EndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().period.StartDate;
                ratesVM.collectorAllowanceRate.AvailableLatestPeriod = configurationsRepository.GetLatestPeriod(ratesVM.category);
                ratesVM.collectorAllowanceRate.CurrentPeriod = configurationsRepository.GetPeriodByDate(ratesVM.effectiveStartDate, (int)ClaimType.Collector);
            }
        }
        private void loadPI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            getProcessingIncentiveParam(param);
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Processor).DefinitionValue };
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).ToList();
                ratesVM = PopulateProcessingIncentiveRateVM(tiRates, ratesVM, param.PI);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = ratesVM.isSpecific ? DateTime.Now.AddMonths(1) : DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateProcessingIncentiveRateVM(rates, ratesVM, param.PI);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
            if (ratesVM.isSpecific)
            {
                ratesVM.piSpecificRate = new PISpecificRateVM()
                {
                    AssignedProcessIDs = configurationsRepository.LoadAssignedProcessorIDs(ratesVM.RateTransactionID).OrderBy(x => x.ItemName).ToList(),
                    AvailabledProcessIDs = configurationsRepository.LoadAvailabledProcessorIDs().OrderBy(x => x.ItemName).ToList()
                };
            }
        }
        private void loadMI(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.RPM).DefinitionValue };
                var rpmItemIDs = DataLoader.Items.Where(x => x.ItemCategory == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue).Select(x => x.ID).ToList(); // == 8
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(x => x.ItemID.HasValue && rpmItemIDs.Contains(x.ItemID.Value)).ToList();
                ratesVM = PopulateManufacturingIncentiveRateVM(tiRates, ratesVM);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateManufacturingIncentiveRateVM(rates, ratesVM);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }
        private void loadTSF(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 2;
            getTireStewardshipFeeParam(param);
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue };
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(x => x.ItemID.HasValue && param.TFS.items.Contains(x.ItemID.Value)).ToList();
                ratesVM = PopulateTireStewardshipFeesRateVM(tiRates, ratesVM, param.TFS);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulateTireStewardshipFeesRateVM(rates, ratesVM, param.TFS);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }
        //private void loadWeight(RateParamsDTO param, RateDetailsVM ratesVM)
        //{
        //    ratesVM.decimalsize = 0;
        //    var rates = configurationsRepository.GetItemWeights(ratesVM.RateTransactionID);
        //    ratesVM = PopulateEstimateWeightRateVM(rates, ratesVM, param);
        //    ratesVM.effectiveStartDate = ratesVM.RateTransactionID == 0 ? DateTime.Now.AddMonths(1) : (DateTime)rates.FirstOrDefault().EffectiveStartDate;
        //    ratesVM.effectiveEndDate = ratesVM.RateTransactionID == 0 ? DateTime.MaxValue.Date : (DateTime)rates.FirstOrDefault().EffectiveEndDate;
        //    ratesVM.previousEffectiveStartDate = ratesVM.effectiveStartDate;
        //}
        private void loadPenalty(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            ratesVM.decimalsize = 0;
            if (ratesVM.RateTransactionID == 0)//initial create new rate
            {
                var iclaimTypes = new int[] { AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ClaimType, TreadMarksConstants.Steward).DefinitionValue };
                int ratePaymentType = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.RemittancePenalty).DefinitionValue;//6
                var tiRates = configurationsRepository.GetCurrentRates(iclaimTypes).Where(c => c.PaymentType.HasValue && c.PaymentType.Value == ratePaymentType).ToList();
                ratesVM = PopulatePenaltyRateVM(tiRates, ratesVM);
                ratesVM.effectiveStartDate = DateTime.Now.AddMonths(1);
                ratesVM.effectiveEndDate = DateTime.MaxValue.Date;
            }
            else
            {
                var rates = configurationsRepository.LoadRateTransactionByID(ratesVM.RateTransactionID);
                ratesVM.notes = configurationsRepository.LoadRateTransactionNoteByID(ratesVM.RateTransactionID);
                ratesVM = PopulatePenaltyRateVM(rates, ratesVM);
                ratesVM.effectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
                ratesVM.effectiveEndDate = (DateTime)rates.FirstOrDefault().EffectiveEndDate;
                ratesVM.previousEffectiveStartDate = (DateTime)rates.FirstOrDefault().EffectiveStartDate;
            }
        }

        private void LoadRateDetails(RateParamsDTO param, RateDetailsVM ratesVM)
        {
            switch (param.categoryName)
            {
                case TreadMarksConstants.TransportationIncentiveRates:
                    loadTI(param, ratesVM);
                    break;
                case TreadMarksConstants.CollectionAllowanceRates:
                    loadCollector(param, ratesVM);
                    break;
                case TreadMarksConstants.ProcessingIncentiveRates:
                    loadPI(param, ratesVM);
                    break;
                case TreadMarksConstants.ManufacturingIncentiveRates:
                    loadMI(param, ratesVM);
                    break;
                case TreadMarksConstants.TireStewardshipFeeRates:
                    loadTSF(param, ratesVM);
                    break;
                //case TreadMarksConstants.EstimatedWeightsRates:
                //    loadWeight(param, ratesVM);
                //    break;
                case TreadMarksConstants.RemittancePenaltyRates:
                    loadPenalty(param, ratesVM);
                    break;
            }
        }
        #endregion

        #region Remove Rates
        public bool RemoveRateTransaction(int rateTransactionID, int category)
        {
            var categoryName = AppDefinitions.Instance.GetDefinitionByValue(Configuration.DefinitionCategory.RateCategory, category).Name;
            if (category != (int)ClaimType.Collector)
            {
                if (configurationsRepository.RemoveRateTransaction(rateTransactionID, categoryName))
                {
                    CachingHelper.RemoveItem(CachKeyManager.RateKey);
                }
            }
            else
            {
                if (configurationsRepository.UnBondCollectorRatesToRateTransaction(rateTransactionID, category))
                {
                    CachingHelper.RemoveItem(CachKeyManager.RateKey);
                }
            }
            return true;
        }
        #endregion

        #region //Notes
        public InternalNoteViewModel AddTransactionNote(int transactionID, string notes)
        {
            var transactionNote = new RateTransactionNote
            {
                RateTransactionID = Convert.ToInt32(transactionID),
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            configurationsRepository.AddTransactionNote(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }

        public InternalNoteViewModel AddTransactionNote(string transactionID, string notes)
        {
            var transactionNote = new AdminSectionNotes
            {
                Category = transactionID,
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            configurationsRepository.AddAppSettingNotes(transactionNote);

            return new InternalNoteViewModel() { Note = transactionNote.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = transactionNote.CreatedDate };
        }

        public List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID)
        {
            return configurationsRepository.LoadRateTransactionNoteByID(rateTransactionID);
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return configurationsRepository.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }

        public List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText)
        {
            return configurationsRepository.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);
        }

        #endregion

        #region //Account Thresholds
        public AccountThresholdsVM LoadAccountThresholds()
        {
            return configurationsRepository.LoadAccountThresholds();
        }
        public bool UpdateAccountThresholds(AccountThresholdsVM vm)
        {
            configurationsRepository.UpdateAccountThresholds(vm, SecurityContextHelper.CurrentUser.Id);

            //Refresh Cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
            return true;
        }
        #endregion

        #region Transaction Thresholds
        public TransactionThresholdsVM LoadTransactionThresholds()
        {
            return configurationsRepository.LoadTransactionThresholds();
        }

        public bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM)
        {
            configurationsRepository.UpdateTransactionThresholds(transactionThresholdsVM);

            //refresh cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);

            return true;
        }
        #endregion

        #region Vendor Group
        public PaginationDTO<VendorGroupListViewModel, int> LoadVendorGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category)
        {
            return configurationsRepository.LoadVendorGroupList(pageIndex, pageSize, searchText, orderBy, sortDirection, category);
        }

        public List<VendorGroupListViewModel> LoadEffectiveVendorGroupList(DateTime effectiveDate, string groupType)//category : CollectorGroup/ProcessorGroup;
        {
            return configurationsRepository.LoadEffectiveVendorGroupList(effectiveDate, groupType);
        }

        public bool AddNewVendorGroup(VendorGroupDetailCommonViewModel newGroup, long userId, string category)
        {
            configurationsRepository.AddNewVendorGroup(newGroup, userId, category);
            //add activity log here
            string msg = string.Empty, groupType = string.Empty;
            string nameDesc = "<strong>{0} - {1}</strong>";
            nameDesc = string.Format(nameDesc, newGroup.Name, newGroup.Description);
            switch (category)//2:Collector 4:Processor
            {//activity.Message = string.Format(msg, activity.InitiatorName);
                case "CollectorGroup":
                    msg = "{0} added " + nameDesc + " as a Collector Group.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                    break;
                case "ProcessorGroup":
                    msg = "{0} added " + nameDesc + " as a Processor Group.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                    break;
                default:
                    break;
            }
            this.AddActivity(msg, groupType, false);
            return true;
        }

        public bool UpdateVendorGroup(VendorGroupDetailCommonViewModel updatedGroup, long userId)
        {
            Group vendorGroupOld = configurationsRepository.UpdateVendorGroup(updatedGroup, userId);
            //add activity log here
            string groupType = string.Empty, detailMsg = string.Empty, groupTypeName = string.Empty; ;
            switch (vendorGroupOld.GroupType)//2:Collector 4:Processor
            {
                case "CollectorGroup":
                    groupTypeName = " Collector Group";
                    groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                    break;
                case "ProcessorGroup":
                    groupTypeName = " Processor Group";
                    groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                    break;
                default:
                    break;
            }

            string nameDesc = "<strong>{0} - {1}</strong>";
            nameDesc = string.Format(nameDesc, vendorGroupOld.GroupName, vendorGroupOld.GroupDescription);

            if (updatedGroup.Name != vendorGroupOld.GroupName)
            {
                detailMsg = string.Format(" changed the name of " + groupTypeName + " from <strong>{0}</strong> to <strong>{1}</strong>.", vendorGroupOld.GroupName, updatedGroup.Name);
                detailMsg = "{0}" + detailMsg;
                this.AddActivity(detailMsg, groupType, false);
            }

            if (updatedGroup.Description != vendorGroupOld.GroupDescription)
            {
                detailMsg = string.Format(" changed the description of " + groupTypeName + " <strong>{0}</strong>.", updatedGroup.Name);
                detailMsg = "{0}" + detailMsg;
                this.AddActivity(detailMsg, groupType, false);
            }
            return true;
        }

        public bool RemoveVendorGroup(int groupId)
        {
            Group vendorGroup = configurationsRepository.RemoveVendorGroup(groupId);
            //add activity log here
            string msg = string.Empty, groupType = string.Empty;
            string nameDesc = "<strong>{0} - {1}</strong>";
            nameDesc = string.Format(nameDesc, vendorGroup.GroupName, vendorGroup.GroupDescription);
            switch (vendorGroup.GroupType)//2:Collector 4:Processor
            {
                case TreadMarksConstants.CollectorGroup: //"CollectorGroup":
                    msg = "{0} deleted " + nameDesc + " from Collector Groups";
                    groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                    break;
                case TreadMarksConstants.ProcessorGroup:
                    msg = "{0} deleted " + nameDesc + " from Processor Groups";
                    groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                    break;
                default:
                    break;
            }
            this.AddActivity(msg, groupType, false);
            return true;
        }

        public bool VendorGroupUniqueNameCheck(string name, string category)
        {
            return configurationsRepository.VendorGroupUniqueNameCheck(name, category);
        }

        public bool IsGroupMappingExisting(int id)
        {
            return configurationsRepository.IsGroupMappingExisting(id);
        }

        //public bool IsGroupMappingExisting(int? RateGroupId, int? GroupId, int? VendorId)
        //{
        //    return configurationsRepository.IsGroupMappingExisting(RateGroupId, GroupId, VendorId);
        //}

        #endregion

        private void AddActivity(string msg, string activityType, bool systemLog = false, DateTime? logDT = null)
        {
            msg = msg.Trim(' ').TrimEnd('.') + ".";
            var activity = new Activity();
            activity.Initiator = systemLog ? AppSettings.Instance.SystemUser.UserName : SecurityContextHelper.CurrentUser.UserName;
            activity.InitiatorName = systemLog ? AppSettings.Instance.SystemUser.FirstName + " " + AppSettings.Instance.SystemUser.LastName : SecurityContextHelper.CurrentUser.FullName;
            activity.Assignee = string.Empty;
            activity.AssigneeName = string.Empty;
            activity.CreatedTime = logDT ?? DateTime.Now;
            activity.ActivityType = TreadMarksConstants.AdminMenuActivity;

            activity.ActivityArea = activityType;// TreadMarksConstants.RPM;

            activity.ObjectId = 0;
            if (systemLog)
            {
                activity.Message = string.Format("{0} <strong>approved</strong> this claim.", activity.InitiatorName);

                LogManager.LogInfo(string.Format("Collector Auto Approve Trace: UTC time {0} : [in/out ]", DateTime.UtcNow.ToString()));
            }
            else
            {
                //Zach Dryman changed the <strong>Rubber</strong> composition of <strong>Passenger & Light Truck Tires</strong> from <strong>0%</strong> to <strong>85</strong>%
                activity.Message = string.Format(msg, activity.InitiatorName);
            }

            this.messageRepository.AddActivity(activity);
        }

        #region Rate Group
        public PaginationDTO<RateGroupListViewModel, int> LoadRateGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category)
        {
            return configurationsRepository.LoadRateGroupList(pageIndex, pageSize, searchText, orderBy, sortDirection, category);
        }

        public IEnumerable<ItemModel<string>> GetVendorGroupList(string category)
        {
            return configurationsRepository.GetVendorGroupList(category);
        }

        public IEnumerable<ItemModel<string>> GetAssociatedVendorGroupList(int rateGroupId)
        {
            //var rateTransactionCategory = AppDefinitions.Instance.TypeDefinitions[DefinitionCategory.RateCategory.ToString()].FirstOrDefault(c => c.Code == "TransportationIncentivesRates").DefinitionValue;
            return configurationsRepository.GetAssociatedVendorGroupList(rateGroupId);
        }

        public IEnumerable<ItemModel<int>> LoadVendors(int vendorGroupId, int rateGroupId)
        {
            return configurationsRepository.LoadVendors(vendorGroupId, rateGroupId);
        }

        public List<ItemModel<int>> LoadDefaultVendors(int vendorGroupId, string category)
        {
            return configurationsRepository.LoadDefaultVendors(vendorGroupId, category);
        }

        public bool RateGroupUniqueNameCheck(string name, string category)
        {
            return configurationsRepository.RateGroupUniqueNameCheck(name, category);
        }

        public void CreateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId)
        {
            DateTime now = DateTime.Now;
            var rateGroup = new RateGroup();
            rateGroup.Category = rateGroupViewModel.Category;
            rateGroup.RateGroupName = rateGroupViewModel.RateGroupName;
            rateGroup.CreatedBy = userId;
            rateGroup.CreatedDate = now;

            rateGroup.VendorRateGroups = new List<VendorRateGroup>();

            //step 1: clone VendorRateGroup by effective rate group rate
            var rateTransactionCategory = AppDefinitions.Instance.TypeDefinitions[DefinitionCategory.RateCategory.ToString()].FirstOrDefault(c => c.Code == "TransportationIncentivesRates").DefinitionValue;
            var effectiveVendorRateGroups = configurationsRepository.GetEffectiveVendorRateGroups(rateGroupViewModel.Category, rateTransactionCategory);
            effectiveVendorRateGroups.ForEach(c =>
            {
                var vendorRateGroup = new VendorRateGroup
                {
                    RateGroup = rateGroup,
                    GroupId = c.GroupId,
                    VendorId = c.VendorId,
                    CreatedBy = userId,
                    CreatedDate = now
                };
                rateGroup.VendorRateGroups.Add(vendorRateGroup);
            });
            //order by GroupId, to prepare the list remove range method(index,count)
            rateGroup.VendorRateGroups = rateGroup.VendorRateGroups.OrderBy(c => c.GroupId).ToList();

            //step 2: edit the cloned one by group selection and moving
            if (rateGroupViewModel.SourceGroup.GroupId != 0 && rateGroupViewModel.DestinationGroup.GroupId != 0)
            {
                if (rateGroupViewModel.SourceGroup.VendorIds.Any())
                {
                    if (rateGroup.VendorRateGroups.Any(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId))
                    {
                        //remove
                        var index = rateGroup.VendorRateGroups.FindIndex(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId);
                        var count = rateGroup.VendorRateGroups.Where(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId).Count();
                        rateGroup.VendorRateGroups.RemoveRange(index, count);
                    }
                    //insert
                    var insertSourceVendorIds = rateGroupViewModel.SourceGroup.VendorIds.ToList();
                    insertSourceVendorIds.ForEach(c =>
                    {
                        var vendorRateGroup = new VendorRateGroup
                        {
                            RateGroup = rateGroup,
                            VendorId = c,
                            GroupId = rateGroupViewModel.SourceGroup.GroupId,
                            CreatedDate = now,
                            CreatedBy = userId
                        };
                        rateGroup.VendorRateGroups.Add(vendorRateGroup);
                    });
                }
                else
                {
                    if (rateGroup.VendorRateGroups.Any(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId))
                    {
                        //remove
                        var index = rateGroup.VendorRateGroups.FindIndex(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId);
                        var count = rateGroup.VendorRateGroups.Where(c => c.GroupId == rateGroupViewModel.SourceGroup.GroupId).Count();
                        rateGroup.VendorRateGroups.RemoveRange(index, count);
                    }
                }


                if (rateGroupViewModel.DestinationGroup.VendorIds.Any())
                {
                    if (rateGroup.VendorRateGroups.Any(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId))
                    {
                        //remove
                        var index = rateGroup.VendorRateGroups.FindIndex(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId);
                        var count = rateGroup.VendorRateGroups.Where(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId).Count();
                        rateGroup.VendorRateGroups.RemoveRange(index, count);
                    }
                    //insert
                    var insertDestinationVendorIds = rateGroupViewModel.DestinationGroup.VendorIds.ToList();
                    insertDestinationVendorIds.ForEach(c =>
                    {
                        var vendorRateGroup = new VendorRateGroup
                        {
                            RateGroup = rateGroup,
                            VendorId = c,
                            GroupId = rateGroupViewModel.DestinationGroup.GroupId,
                            CreatedDate = now,
                            CreatedBy = userId
                        };
                        rateGroup.VendorRateGroups.Add(vendorRateGroup);
                    });
                }
                else
                {
                    if (rateGroup.VendorRateGroups.Any(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId))
                    {
                        //remove
                        var index = rateGroup.VendorRateGroups.FindIndex(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId);
                        var count = rateGroup.VendorRateGroups.Where(c => c.GroupId == rateGroupViewModel.DestinationGroup.GroupId).Count();
                        rateGroup.VendorRateGroups.RemoveRange(index, count);
                    }
                }
            }

            configurationsRepository.CreateRateGroup(rateGroup);

            //add activity log here
            string msg = string.Empty, groupType = string.Empty;
            string nameDesc = "<strong>{0}</strong>";
            nameDesc = string.Format(nameDesc, rateGroupViewModel.RateGroupName);

            switch (rateGroup.Category)//2:Collector 4:Processor
            {
                case TreadMarksConstants.PickupGroup:
                    msg = "{0} added " + nameDesc + " as a Pickup Rate Group.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                    break;
                case TreadMarksConstants.DeliveryGroup:
                    msg = "{0} added " + nameDesc + " as a Deliver Rate Group.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                    break;
                default:
                    break;
            }
            this.AddActivity(msg, groupType, false);

        }

        public void UpdateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId)
        {
            //Get original Rate Group Name
            var originalRateGroupName = configurationsRepository.GetRateGroupNameById(rateGroupViewModel.Id);

            configurationsRepository.UpdateRateGroup(rateGroupViewModel, userId);
            string parseit = string.Empty;
            if (rateGroupViewModel.logContent != null)
            {
                parseit = rateGroupViewModel.logContent.Replace("<h4>Are you sure you want to Move <br/>", "").Replace("?</h4>", "").Replace("<br/><br/>", "<br/>");
            }
            string[] logs = parseit.Split(new string[] { ", <br/>" }, StringSplitOptions.None);
            if (logs != null && logs.Length > 0)
            {
                Array.ForEach(logs, item => item = "{0} " + item);
            }
            DateTime dtNow = DateTime.Now;
            //add activity log here
            //rateGroupViewModel.RateGroupName
            string msg = string.Empty, groupType = string.Empty;
            string nameOrg = "<strong>{0}</strong>";
            nameOrg = string.Format(nameOrg, originalRateGroupName);
            string nameDesc = "<strong>{0}</strong>";
            nameDesc = string.Format(nameDesc, rateGroupViewModel.RateGroupName);

            //rate group name
            if (originalRateGroupName != rateGroupViewModel.RateGroupName)
            {
                switch (rateGroupViewModel.Category)//2:Collector 4:Processor
                {
                    case TreadMarksConstants.PickupGroup:
                        msg = "{0} changed the name of Pickup Rate Group from " + nameOrg + " to " + nameDesc + ".";
                        groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                        break;
                    case TreadMarksConstants.DeliveryGroup:
                        msg = "{0} changed the name of Delivery Rate Group from " + nameOrg + " to " + nameDesc + ".";
                        groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                        break;
                    default:
                        break;
                }
                this.AddActivity(msg, groupType, false, dtNow);
            }

            string regNumber = "<strong>{0}</strong>", businessName = "<strong>{0}</strong>", fromGroup = "<strong>{0}</strong>", toGroup = "<strong>{0}</strong>";
            if (rateGroupViewModel.VendorsMovedFromSourceToDestination != null)
            {
                rateGroupViewModel.VendorsMovedFromSourceToDestination.ForEach(c =>
                {
                    regNumber = businessName = fromGroup = toGroup = "<strong>{0}</strong>";
                    regNumber = string.Format(regNumber, c.ItemName.Split(' ').First());
                    businessName = string.Format(businessName, string.Join(" ", c.ItemName.Split(' ').Skip(1)));
                    fromGroup = string.Format(fromGroup, rateGroupViewModel.SourceGroup.GroupName);
                    toGroup = string.Format(toGroup, rateGroupViewModel.DestinationGroup.GroupName);
                    msg = "{0} moved " + regNumber + " - " + businessName + " from " + fromGroup + " to " + toGroup + ".";
                    switch (rateGroupViewModel.Category)//2:Collector 4:Processor
                    {
                        case TreadMarksConstants.PickupGroup:
                            groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                            break;
                        case TreadMarksConstants.DeliveryGroup:
                            groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                            break;
                        default:
                            break;
                    }
                    this.AddActivity(msg, groupType, false, dtNow);
                });
            }

            if (rateGroupViewModel.VendorsMovedFromDestinationToSource != null)
            {
                rateGroupViewModel.VendorsMovedFromDestinationToSource.ForEach(c =>
                {
                    regNumber = businessName = fromGroup = toGroup = "<strong>{0}</strong>";
                    regNumber = string.Format(regNumber, c.ItemName.Split(' ').First());
                    businessName = string.Format(businessName, string.Join(" ", c.ItemName.Split(' ').Skip(1)));
                    fromGroup = string.Format(fromGroup, rateGroupViewModel.DestinationGroup.GroupName);
                    toGroup = string.Format(toGroup, rateGroupViewModel.SourceGroup.GroupName);
                    msg = "{0} moved " + regNumber + " - " + businessName + " from " + fromGroup + " to " + toGroup + ".";
                    switch (rateGroupViewModel.Category)//2:Collector 4:Processor
                    {
                        case TreadMarksConstants.PickupGroup:
                            groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                            break;
                        case TreadMarksConstants.DeliveryGroup:
                            groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                            break;
                        default:
                            break;
                    }
                    this.AddActivity(msg, groupType, false, dtNow);
                });

            }
        }

        public bool RemoveVendorRateGroupMapping(int RateGroupId)
        {
            RateGroup rateGroup = configurationsRepository.RemoveVendorRateGroupMapping(RateGroupId);
            //add activity log here
            string msg = string.Empty, groupType = string.Empty;
            string nameDesc = "<strong>{0}</strong>";
            nameDesc = string.Format(nameDesc, rateGroup.RateGroupName);

            switch (rateGroup.Category)//2:Collector 4:Processor
            {
                case TreadMarksConstants.PickupGroup:
                    msg = "{0} deleted " + nameDesc + " from Pickup Rate Groups.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupPickup;
                    break;
                case TreadMarksConstants.DeliveryGroup:
                    msg = "{0} deleted  " + nameDesc + " from Deliver Rate Groups.";
                    groupType = TreadMarksConstants.AdminActivityRateGroupDelivery;
                    break;
                default:
                    break;
            }
            this.AddActivity(msg, groupType, false);
            return true;
        }
        //public RateGroupViewModel LoadRateGroup(int id)
        //{
        //    return configurationsRepository.LoadRateGroup(id);
        //}

        //public void DeleteRateGroup(int id)
        //{
        //    configurationsRepository.DeleteRateGroup(id);
        //}
        #endregion

        public List<ItemModel<string>> LoadRateGroups(string category)
        {
            return configurationsRepository.LoadRateGroups(category);
        }

        public TransportationIncentiveViewModel LoadNewTransportationIncentiveRates(int pickupRateGroupId, int deliveryRateGroupId)
        {
            var pickupGroups = configurationsRepository.LoadGroups(pickupRateGroupId);
            var deliveryGroups = configurationsRepository.LoadGroups(deliveryRateGroupId);
            var rates = configurationsRepository.GetCurrentRateGroupRates();
            var param = new RateParamsDTO();
            getTransactionIncentiveParam(param);

            var result = new TransportationIncentiveViewModel
            {
                PickupRateGroupId = pickupRateGroupId,
                DeliveryRateGroupId = deliveryRateGroupId,
                pickupGroupNumber = pickupGroups != null ? pickupGroups.Count : 0,
                deliveryGroupNumber = deliveryGroups != null ? deliveryGroups.Count : 0,
                TIRateGroupList = new TIRateGroupList()
                {
                    pickupRateGroups = configurationsRepository.LoadEffectiveRateGroups(TreadMarksConstants.PickupGroup),
                    deliveryRateGroups = configurationsRepository.LoadEffectiveRateGroups(TreadMarksConstants.DeliveryGroup)
                }
            };

            mapRateGropuRateVM(pickupGroups, deliveryGroups, rates, param, result);

            return result;
        }
        public TransportationIncentiveViewModel LoadTransportationIncentiveViewModel(int rateTransactionId)
        {
            var rateTransation = configurationsRepository.LoadRateTransaction(rateTransactionId);
            var param = new RateParamsDTO();
            getTransactionIncentiveParam(param);

            return getTIVMByRateTransaction(rateTransation, param);
        }

        private TransportationIncentiveViewModel getTIVMByRateTransaction(RateTransaction rateTransation, RateParamsDTO param)
        {
            var prevRateTrans = configurationsRepository.getPreviousRateTransaction(rateTransation.Category, 1);
            var prevDT = DateTime.MinValue;
            if (prevRateTrans != null)
                prevDT = prevRateTrans.EffectiveStartDate;
            var pickupGroups = configurationsRepository.LoadGroups((int)rateTransation.PickupRateGroupId);
            var deliveryGroups = configurationsRepository.LoadGroups((int)rateTransation.DeliveryRateGroupId);

            var result = new TransportationIncentiveViewModel()
            {
                RateTransactionId = rateTransation.ID,
                EffectiveDate = rateTransation.EffectiveStartDate,
                previousEffectiveDate = prevDT,
                PickupRateGroupId = (int)rateTransation.PickupRateGroupId,
                DeliveryRateGroupId = (int)rateTransation.DeliveryRateGroupId,
                deliveryGroupNumber = deliveryGroups != null ? deliveryGroups.Count : 0,
                pickupGroupNumber = pickupGroups != null ? pickupGroups.Count : 0,
                TIRateGroupList = new TIRateGroupList()
                {
                    pickupRateGroups = configurationsRepository.LoadRateGroups(TreadMarksConstants.PickupGroup),
                    deliveryRateGroups = configurationsRepository.LoadRateGroups(TreadMarksConstants.DeliveryGroup)
                }
            };
            //Build Transportation Incentive View Model
            mapRateGropuRateVM(pickupGroups, deliveryGroups, rateTransation.RateGroupRates, param, result);

            return result;
        }

        private TransportationIncentiveViewModel mapRateGropuRateVM(
                List<ItemModel<int>> pickupGroups,
                List<ItemModel<int>> deliveryGroups,
                IEnumerable<RateGroupRate> data,
                RateParamsDTO param,
                TransportationIncentiveViewModel result
            )
        {
            var stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
            var groups = from pk in pickupGroups
                         from dv in deliveryGroups
                         select new { pk, dv };

            var rateT = (from pg in groups
                         join rg in data.DefaultIfEmpty()
                         on
                         new
                         {
                             pkID = pg.pk.ItemValue,
                             dvID = pg.dv.ItemValue
                         }
                         equals
                         new
                         {
                             pkID = rg.CollectorGroupId ?? 0,
                             dvID = rg.ProcessorGroupId ?? 0
                         }
                         into rateg
                         select new
                         {
                             PickupGroupId = pg.pk.ItemValue,
                             PickupGroupName = pg.pk.ItemName,
                             DeliveryGroupId = pg.dv.ItemValue,
                             DeliveryGroupName = pg.dv.ItemName,
                             Rates = rateg.Select(x => new { itemType = x.ItemType, rate = x.Rate, paymentType = x.PaymentType })
                         }).ToList();


            foreach (var x in rateT)
            {
                //Build Transportation Premium View Model
                var temp = x.Rates.FirstOrDefault(c => c.itemType == param.TI.itemTypeOnRoad && c.paymentType == param.TI.paymentTypeNorth);
                decimal onRate = temp != null ? temp.rate : 0;
                temp = x.Rates.FirstOrDefault(c => c.itemType == param.TI.itemTypeOffRoad && c.paymentType == param.TI.paymentTypeNorth);
                decimal offRate = temp != null ? temp.rate : 0;
                var transP = new TransportationPremiumItem()
                {
                    PickupGroupId = x.PickupGroupId,
                    PickupGroupName = x.PickupGroupName,
                    DeliveryGroupId = x.DeliveryGroupId,
                    DeliveryGroupName = x.DeliveryGroupName,
                    OnRoadRate = onRate,
                    OffRoadRate = offRate
                };
                result.TransportationPremium.TransportationPremiumItems.Add(transP);

                //Build DOT Premium View Model        
                temp = x.Rates.FirstOrDefault(c => c.itemType == param.TI.itemTypeOffRoad && c.paymentType == param.TI.paymentTypeDOT);
                offRate = temp != null ? temp.rate : 0;
                var dotP = new DOTPremiumItem()
                {
                    PickupGroupId = x.PickupGroupId,
                    PickupGroupName = x.PickupGroupName,
                    DeliveryGroupId = x.DeliveryGroupId,
                    DeliveryGroupName = x.DeliveryGroupName,
                    OffRoadRate = offRate
                };
                result.DOTPremium.DOTPremiumItems.Add(dotP);
            }

            //Build Processor TI Rate

            var rateP = (from dv in deliveryGroups
                         join rg in data.DefaultIfEmpty()
                         on dv.ItemValue equals rg.ProcessorGroupId
                         into rateg
                         select new
                         {
                             DeliveryGroupId = dv.ItemValue,
                             DeliveryGroupName = dv.ItemName,
                             Rates = rateg.Where(x => x.PaymentType == param.TI.paymentTypeProcessorTI)
                                    .Select(x => new { itemType = x.ItemType, rate = x.Rate })
                         }).ToList();

            foreach (var x in rateP)
            {
                var temp = x.Rates.FirstOrDefault(c => c.itemType == param.TI.itemTypeOnRoad);
                decimal onRate = temp != null ? temp.rate : 0;
                temp = x.Rates.FirstOrDefault(c => c.itemType == param.TI.itemTypeOffRoad);
                decimal offRate = temp != null ? temp.rate : 0;

                var procT = new ProcessorTIItem
                {
                    DeliveryGroupId = x.DeliveryGroupId,
                    DeliveryGroupName = x.DeliveryGroupName,
                    OnRoadRate = onRate,
                    OffRoadRate = offRate
                };
                result.ProcessorTI.ProcessorTIItems.Add(procT);
            }
            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:0000}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            return result;
        }

        public void SaveNewTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId)
        {
            var param = new RateParamsDTO();
            getTransactionIncentiveParam(param);
            var rateTransaction = new RateTransaction
            {
                CreatedByID = userId,
                CreatedDate = DateTime.UtcNow,
                Category = transportationIncentiveViewModel.category, //Transportation Incentives Rates RateCategory in Type Definition table
                EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                EffectiveEndDate = DateTime.MaxValue,
                IsSpecificRate = false,
                PickupRateGroupId = transportationIncentiveViewModel.PickupRateGroupId,
                DeliveryRateGroupId = transportationIncentiveViewModel.DeliveryRateGroupId
            };

            if (!string.IsNullOrWhiteSpace(transportationIncentiveViewModel.InternalNote))
            {
                rateTransaction.RateTransactionNotes = new List<RateTransactionNote>();
                rateTransaction.RateTransactionNotes.Add(new RateTransactionNote
                {
                    RateTransaction = rateTransaction,
                    Note = transportationIncentiveViewModel.InternalNote,
                    CreatedDate = DateTime.UtcNow,
                    UserID = userId
                });
            };

            //Save transportation incentive rates
            rateTransaction.RateGroupRates = new List<RateGroupRate>();
            //Transportation Premium
            transportationIncentiveViewModel.TransportationPremium.TransportationPremiumItems.ForEach(c =>
            {
                var onRoadRateGroupRate = new RateGroupRate
                {
                    RateTransaction = rateTransaction,
                    CollectorGroupId = c.PickupGroupId,
                    ProcessorGroupId = c.DeliveryGroupId,
                    ItemType = param.TI.itemTypeOnRoad,
                    PaymentType = param.TI.paymentTypeNorth,
                    Rate = c.OnRoadRate,
                    EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                    EffectiveEndDate = DateTime.MaxValue
                };
                rateTransaction.RateGroupRates.Add(onRoadRateGroupRate);
                var offRoadRateGroupRate = new RateGroupRate
                {
                    RateTransaction = rateTransaction,
                    CollectorGroupId = c.PickupGroupId,
                    ProcessorGroupId = c.DeliveryGroupId,
                    ItemType = param.TI.itemTypeOffRoad,
                    PaymentType = param.TI.paymentTypeNorth,
                    Rate = c.OffRoadRate,
                    EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                    EffectiveEndDate = DateTime.MaxValue
                };
                rateTransaction.RateGroupRates.Add(offRoadRateGroupRate);
            });

            //DOT Premium
            transportationIncentiveViewModel.DOTPremium.DOTPremiumItems.ForEach(c =>
            {
                var offRoadRateGroupRate = new RateGroupRate
                {
                    RateTransaction = rateTransaction,
                    CollectorGroupId = c.PickupGroupId,
                    ProcessorGroupId = c.DeliveryGroupId,
                    ItemType = param.TI.itemTypeOffRoad,
                    PaymentType = param.TI.paymentTypeDOT,
                    Rate = c.OffRoadRate,
                    EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                    EffectiveEndDate = DateTime.MaxValue
                };
                rateTransaction.RateGroupRates.Add(offRoadRateGroupRate);
            });

            //Processor TI
            transportationIncentiveViewModel.ProcessorTI.ProcessorTIItems.ForEach(c =>
            {
                var onRoadRateGroupRate = new RateGroupRate
                {
                    RateTransaction = rateTransaction,
                    ProcessorGroupId = c.DeliveryGroupId,
                    ItemType = param.TI.itemTypeOnRoad,
                    PaymentType = param.TI.paymentTypeProcessorTI,
                    Rate = c.OnRoadRate,
                    EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                    EffectiveEndDate = DateTime.MaxValue
                };
                rateTransaction.RateGroupRates.Add(onRoadRateGroupRate);
                var offRoadRateGroupRate = new RateGroupRate
                {
                    RateTransaction = rateTransaction,
                    ProcessorGroupId = c.DeliveryGroupId,
                    ItemType = param.TI.itemTypeOffRoad,
                    PaymentType = param.TI.paymentTypeProcessorTI,
                    Rate = c.OffRoadRate,
                    EffectiveStartDate = transportationIncentiveViewModel.EffectiveDate.Date,
                    EffectiveEndDate = DateTime.MaxValue
                };
                rateTransaction.RateGroupRates.Add(offRoadRateGroupRate);
            });

            configurationsRepository.UpdateExitingRateEndDate(transportationIncentiveViewModel.category, transportationIncentiveViewModel.EffectiveDate.AddDays(-1), 0);
            configurationsRepository.AddRateTransaction(rateTransaction);
        }
        public void UpdateTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId)
        {
            var param = new RateParamsDTO()
            {
                userId = userId
            };
            getTransactionIncentiveParam(param);
            configurationsRepository.UpdateTransportationIncentiveRates(transportationIncentiveViewModel, param);
        }

        public List<RateGroupRate> LoadRateGroupRates(DateTime periodStartDate, DateTime periodEndDate)
        {
            return configurationsRepository.LoadRateGroupRates(periodStartDate, periodEndDate);
        }

        public List<VendorRateGroup> LoadVendorRateGroups(int rateGroupId)
        {
            return configurationsRepository.LoadVendorRateGroups(rateGroupId);
        }
        #region STC Premium
        public PaginationDTO<STCPremiumListViewModel, int> LoadSTCPremiumList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return configurationsRepository.LoadSTCPremiumList(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public STCEventsCountViewModel LoadSTCEventsCount()
        {
            return configurationsRepository.GetSTCEventsCount();
        }

        public STCPremiumListViewModel LoadSTCEventDetailsByEventNumber(string eventNumber)
        {
            return configurationsRepository.LoadSTCEventDetailsByEventNumber(eventNumber);
        }

        public bool AddNewSTCEvent(STCPremiumListViewModel newEvent, long userId)
        {
            configurationsRepository.AddNewSTCEvent(newEvent, userId);

            return true;
        }

        public bool UpdateSTCEvent(STCPremiumListViewModel updatedEvent, long userId)
        {
            return configurationsRepository.UpdateSTCEvent(updatedEvent, userId);
        }

        public bool RemoveSTCEvent(int id, string eventNumber)
        {
            return configurationsRepository.RemoveSTCEvent(id, eventNumber);
        }
        #endregion

        #region TCR Service Threshold
        public PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO)
        {
            return configurationsRepository.LoadTCRServiceThresholds(paramDTO);
        }
        public TCRServiceThresholdListViewModel LoadTCRServiceThresholdList()
        {
            return configurationsRepository.LoadTCRServiceThresholdList();
        }

        public bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            configurationsRepository.AddTCRServiceThreshold(vm, userId);

            return true;
        }

        public bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId)
        {
            return configurationsRepository.UpdateTCRServiceThreshold(vm, userId);
        }

        public bool RemoveTCRServiceThresohold(int id, long userId)
        {
            return configurationsRepository.RemoveTCRServiceThresohold(id, userId);
        }
        public IEnumerable<TCRListViewModel> ExportTCRServiceThresholds()
        {
            return configurationsRepository.ExportTCRServiceThresholds();
        }
        #endregion     
    }
}
