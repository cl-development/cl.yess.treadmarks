﻿using CL.TMS.Caching;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.IRepository.Registrant;
using CL.TMS.IRepository.System;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;

namespace CL.TMS.SystemBLL
{
    public class CompanyBrandingBO
    {
        private IUserRepository userRepository;
        private IVendorRepository vendorRepository;
        private IEventAggregator eventAggregator;
        private IMessageRepository messageRepository;
        private ICompanyBrandingRepository companyBrandingRepository;

        public CompanyBrandingBO(IUserRepository userRepository, IVendorRepository vendorRepository, IEventAggregator eventAggregator,
            IMessageRepository messageRepository, ICompanyBrandingRepository companyBrandingRepository)
        {
            this.userRepository = userRepository;
            this.vendorRepository = vendorRepository;
            this.eventAggregator = eventAggregator;
            this.messageRepository = messageRepository;
            this.companyBrandingRepository = companyBrandingRepository;
        }

        #region //Loading data

        public TermAndConditionVM LoadDetailsByID(int applicationTypeID)
        {
            var vm = new TermAndConditionVM() {
                applicationTypeID= applicationTypeID,
                termConditionID = 0,
                applicationName = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.ApplicationType, applicationTypeID).Name
            };
          
            var result = companyBrandingRepository.GetTermConditionByID(applicationTypeID);
            if (result != null)
            {
                vm.content = result.Content;
                vm.termConditionID = result.ID;              
            }
            vm.notes = companyBrandingRepository.LoadNoteByID(applicationTypeID);
            return vm;
        }
        public string LoadTermsAndConditionsContent(int? termAndConditionID, string applicationName) {
            int applicationTypeID = AppDefinitions.Instance.FindDefinitionByName(DefinitionCategory.ApplicationType, applicationName).DefinitionValue;
            return companyBrandingRepository.GetTermConditionContentByID(termAndConditionID, applicationTypeID);
        }
        public int GetCurrentTermsAndConditionsID(string applicationName) {
            int applicationTypeID = AppDefinitions.Instance.FindDefinitionByName(DefinitionCategory.ApplicationType, applicationName).DefinitionValue;
            return companyBrandingRepository.GetCurrentTermConditionID(applicationTypeID);
        }
        #endregion //loading data

        #region //Save

        public bool SaveTermCondition(TermAndConditionVM vm, long userId)
        {         
            companyBrandingRepository.SaveTermCondition(vm, userId);
            //Refresh Cache
            //CachingHelper.RemoveItem(CachKeyManager.RateKey);

            return true;
        }

        #endregion 

        #region //Remove 

        public bool RemoveTermCondition(int id)
        {
            if (companyBrandingRepository.RemoveTermCondition(id))
            {
                // CachingHelper.RemoveItem(CachKeyManager.RateKey);
            }
            return true;
        }

        #endregion Remove 

        #region //Notes
        public InternalNoteViewModel AddNote(int applicationTypeID, string notes)
        {
            var note = new TermsAndConditionsNote
            {
                ApplicationTypeID = Convert.ToInt32(applicationTypeID),
                Note = notes,
                CreatedDate = DateTime.UtcNow,
                UserID = SecurityContextHelper.CurrentUser.Id
            };

            companyBrandingRepository.AddNote(note);

            return new InternalNoteViewModel() { Note = note.Note, AddedBy = SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, AddedOn = note.CreatedDate };
        }

        public List<InternalNoteViewModel> LoadNoteByID(int applicationTypeID)
        {
            return companyBrandingRepository.LoadNoteByID(applicationTypeID);
        }

        public List<InternalNoteViewModel> ExportNotesToExcel(int applicationTypeID, bool sortReverse, string sortcolumn, string searchText)
        {
            return companyBrandingRepository.ExportNotesToExcel(applicationTypeID, sortReverse, sortcolumn, searchText);
        }

        #endregion

        public CompanyInformationVM LoadCompanyInformation()
        {            
           return companyBrandingRepository.LoadCompanyInformation();
        }

        public void UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath)
        {
            companyBrandingRepository.UpdateCompanyInformation(companyInformationVM, logoFilePath);
            //refresh cache
            CachingHelper.RemoveItem(CachKeyManager.AppSettingsKey);
        }

        public string LoadCompanyLogoUrl()
        {
            return companyBrandingRepository.LoadCompanyLogoUrl();
        }        
    }
}