﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Extension;

namespace CL.TMS.SystemBLL
{
    public class ApplicatoinBriefBO
    {
        private IApplicationBriefRepository applicationBriefRepository;
        public ApplicatoinBriefBO(IApplicationBriefRepository applicationBriefRepository)
        {
            this.applicationBriefRepository = applicationBriefRepository;
        }

        public PaginationDTO<ApplicationViewModel, int> GetAllApplicationBrief(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText)
        {
            return  applicationBriefRepository.LoadAllApplicationBrief(pageIndex, pageSize, searchText, orderBy, sortDirection, columnSearchText);
        }

        public List<ApplicationViewModel> LoadApplications(string searchText, string sortcolumn, string sortdirection, Dictionary<string, string> columnSearchText)
        {
            return applicationBriefRepository.LoadApplications(searchText, sortcolumn, sortdirection, columnSearchText);
        }

        public ApplicationViewModel GetFirstSubmitApplication()
        {
            return applicationBriefRepository.GetFirstSubmitApplication();
        }
    }
}

