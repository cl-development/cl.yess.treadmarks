﻿using CL.Framework.BLL;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Communication.Events;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository;
using CL.TMS.IRepository.System;
using CL.TMS.RuleEngine.PasswordBusinessRules;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.Security.Implementations;
using FluentValidation.Results;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using static CL.TMS.Common.Helper.CommonModelHelper;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.SystemBLL
{
    public class UserBO : BaseBO
    {
        private IUserRepository userRepository;
        private IInvitationRepository invitationRepository;
        private IMessageRepository messageRepository;
        private IEventAggregator eventAggregator;

        public UserBO(IUserRepository userRepository, IInvitationRepository invitationRepository, IMessageRepository messageRepository, IEventAggregator eventAggregator)
        {
            this.userRepository = userRepository;
            this.invitationRepository = invitationRepository;
            this.messageRepository = messageRepository;
            this.eventAggregator = eventAggregator;
        }

        public User FindUserByUserId(long userId)
        {
            return userRepository.FindUserByUserId(userId);
        }

        public User FindUserByEmailAddress(string emailAddress)
        {
            return userRepository.FindUserByEmailAddress(emailAddress);
        }

        public void AddPasswordResetToken(long userId, string token, string secretKey)
        {
            userRepository.AddPasswordResetToken(userId, token, secretKey);
        }

        public IReadOnlyList<string> GetPasswordResetToken(long userId)
        {
            return userRepository.GetPasswordResetToken(userId);
        }

        public string FindUserNameByUserId(long userId)
        {
            return userRepository.FindUserNameByUserId(userId);
        }

        public string FindEmailAddressByUsername(string Username)
        {
            return userRepository.FindEmailAddressByUsername(Username);
        }

        public User ResetPassword(string userName, string password, int passwordExpirationInDays)
        {
            var user = userRepository.FindUserByUserName(userName);
            ConditionCheck.Null(user, "user");

            //1. Initialize reset password business rule set strategy
            var businessRuleSetStrategy = PasswordRuleStrategyFactory.LoadPasswordRuleStrategy();

            //2. Create business rule set
            CreateBusinessRuleSet(businessRuleSetStrategy);

            //3. Execute business rules
            var ruleContext = new Dictionary<string, object>
            {
                {"password", password},
                {"userRepository", userRepository}
            };
            ExecuteBusinessRules(user, ruleContext);

            //4. Throw validation exception if validation is failed
            if (!BusinessRuleExecutioResult.IsValid)
            {
                throw new CLValidaitonException("Reset password is failed", BusinessRuleExecutioResult);
            }

            var hashedPassword = PBKDF2PasswordHasher.Create(password);

            userRepository.ResetUserPassword(user, hashedPassword, passwordExpirationInDays);

            return user;
        }

        public bool EmailIsExisting(string emailAddress)
        {
            return invitationRepository.EmailIsExisting(emailAddress);
        }

        public bool EmailIsExistingForParticipant(string emailAddress, int participantId, bool isVendor)
        {
            return invitationRepository.EmailIsExistingForParticipant(emailAddress, participantId, isVendor);
        }
        public void CreateUserInvitation(Invitation invitation)
        {
            invitation.InvitationRoleIds.ForEach(c =>
            {
                var invitationRole = new InvitationRole
                {
                    Role = c
                };
                invitation.InvitationRoles.Add(invitationRole);
            });

            invitation.UpdateDate = DateTime.Now;
            invitationRepository.CreateInviation(invitation);
        }

        public void CreateParticipantDefaultUserInvitation(Invitation invitation)
        {
            //Add default participant admin role
            var invitationRole = new InvitationRole
            {
                Role = AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "ParticipantAdmin").DefinitionValue
            };
            invitation.InvitationRoles.Add(invitationRole);
            invitationRepository.CreateInviation(invitation);
        }

        public Invitation FindInvitation(Guid guid)
        {
            return invitationRepository.FindInvitation(guid);
        }

        public void UpdateInvitation(Invitation invitation)
        {
            invitationRepository.UpdateInvitation(invitation);
        }

        public void UpdateInvitationRedirectUrl(int VendorId, string Url)
        {
            var invitation = invitationRepository.FindInvitationByVendorId(VendorId);

            invitation.RedirectUrl = Url;
            invitationRepository.UpdateRedirectUrl(invitation);
        }

        public bool ConfirmInvitaion(string password, string userName, DateTime passwordExpiationInDays, string redirectUrl, Guid InviteGUID)
        {
            var invitation = invitationRepository.FindInvitationByGUID(InviteGUID);
            if (invitation == null)
                return false;
            invitation.Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Accepted);

            var isParticipantUser = false;
            if ((invitation.VendorId != null) || (invitation.CustomerId != null))
            {
                isParticipantUser = true;
            }
            if (isParticipantUser)
            {
                CreateParticipantNewUser(password, passwordExpiationInDays, redirectUrl, invitation);
            }
            else
            {
                CreateStaffNewUser(password, passwordExpiationInDays, redirectUrl, invitation);
            }
            return true;
        }

        /// <summary>
        /// Create Staff New User
        /// </summary>
        /// <param name="password"></param>
        /// <param name="passwordExpiationInDays"></param>
        /// <param name="redirectUrl"></param>
        /// <param name="invitation"></param>
        private void CreateStaffNewUser(string password, DateTime passwordExpiationInDays, string redirectUrl, Invitation invitation)
        {
            //Create a new user
            var user = new User();
            user.UserName = invitation.UserName;
            user.Email = invitation.EmailAddress;
            user.FirstName = invitation.FirstName;
            user.LastName = invitation.LastName;
            user.Password = password;
            user.CreateDate = DateTime.Now;
            user.LastLoginDate = DateTime.Now;
            user.ModifiedDate = null;
            user.LastPasswordResetDate = null;
            user.PasswordExpirationDate = passwordExpiationInDays;

            if (!string.IsNullOrWhiteSpace(redirectUrl))
            {
                user.RedirectUserAfterLogin = true;
                user.RedirectUserAfterLoginUrl = redirectUrl;
            }

            AddStaffUserRoles(invitation, user);

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                invitationRepository.UpdateInvitation(invitation);
                userRepository.AddUser(user);
                transationScope.Complete();
            }
        }

        private void CreateParticipantNewUser(string password, DateTime passwordExpiationInDays, string redirectUrl, Invitation invitation)
        {
            //Create a new user
            var user = new User();
            user.UserName = invitation.UserName;
            user.Email = invitation.EmailAddress;
            user.FirstName = invitation.FirstName;
            user.LastName = invitation.LastName;
            user.Password = password;
            user.CreateDate = DateTime.Now;
            user.LastLoginDate = DateTime.Now;
            user.ModifiedDate = null;
            user.LastPasswordResetDate = null;
            user.PasswordExpirationDate = passwordExpiationInDays;

            if (!string.IsNullOrWhiteSpace(redirectUrl))
            {
                user.RedirectUserAfterLogin = true;
                user.RedirectUserAfterLoginUrl = redirectUrl;
            }

            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                invitationRepository.UpdateInvitation(invitation);
                userRepository.AddUser(user);

                if (invitation.VendorId != null && invitation.VendorId > 0)
                {
                    invitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var vendorUser = new VendorUser
                        {
                            UserID = user.ID,
                            VendorID = (int)invitation.VendorId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddVendorUser(vendorUser);
                    });
                }
                if (invitation.CustomerId != null && invitation.CustomerId > 0)
                {
                    //Add record to customeruser table for customer user
                    invitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var customerUser = new CustomerUser()
                        {
                            UserId = user.ID,
                            CustomerId = (int)invitation.CustomerId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddCustomerUser(customerUser);
                    });
                }
                transationScope.Complete();
            }
        }

        private void AddStaffUserRoles(Invitation invitation, User user)
        {
            //Convert invitation roles to user roles
            var allRoles = userRepository.LoadAllUserRoles();
            invitation.InvitationRoles.ToList().ForEach(c =>
            {
                var role = allRoles.Find(r => r.ID == c.Role);
                if (role != null)
                {
                    user.Roles.Add(role);
                }
            });

            //Convert invitation claims workflow to user claims workflow
            invitation.InvitationClaimsWorkflows.ToList().ForEach(c =>
            {
                var userClaimsWorkflow = new UserClaimsWorkflow();
                userClaimsWorkflow.AccountName = c.AccountName;
                userClaimsWorkflow.ActionGear = c.ActionGear;
                userClaimsWorkflow.Workflow = c.Workflow;
                userClaimsWorkflow.User = user;
                user.UserClaimsWorkflows.Add(userClaimsWorkflow);
            });

            //Convert invitation applications workflow to user applications workflow
            invitation.InvitationApplicationsWorkflows.ToList().ForEach(c =>
            {
                var userApplicationsWorkflow = new UserApplicationsWorkflow();
                userApplicationsWorkflow.AccountName = c.AccountName;
                userApplicationsWorkflow.Routing = c.Routing;
                userApplicationsWorkflow.ActionGear = c.ActionGear;
                userApplicationsWorkflow.User = user;
                user.UserApplicationsWorkflows.Add(userApplicationsWorkflow);
            });

        }

        private void AddUserRoles(Invitation invitation, User user)
        {
            //Add user role
            var allRoles = userRepository.LoadAllUserRoles();
            invitation.InvitationRoles.ToList().ForEach(c =>
            {
                var role = allRoles.Find(r => r.ID == c.Role);
                if (role != null)
                {
                    user.Roles.Add(role);
                }
            });
        }

        public void ResetRedirectUrlForSecondInvitation(Invitation invitation, User user)
        {
            invitation.Status = EnumHelper.GetEnumDescription(UserInvitationStatus.Accepted);
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                invitationRepository.UpdateInvitation(invitation);
                userRepository.ResetloginRedirectUrl(user.ID, invitation.RedirectUrl);
                transationScope.Complete();
            }
        }

        public Invitation FindInvitationByUserName(string userName)
        {
            return invitationRepository.FindInvitationByUserName(userName);
        }

        public Invitation FindInvitationByUserName(string userName, int vendorId, bool isVendor)
        {
            return invitationRepository.FindInvitationByUserName(userName, vendorId, isVendor);
        }
        public void EditUserAndInvitation(Invitation editInvitation)
        {
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                var invitation = invitationRepository.FindInvitationByUserName(editInvitation.EmailAddress);
                var user = userRepository.FindUserByEmailAddress(editInvitation.EmailAddress);

                //update invitation
                invitation.FirstName = editInvitation.FirstName;
                invitation.LastName = editInvitation.LastName;
                invitationRepository.UpdateInvitation(invitation);

                //delete the old user details first
                userRepository.DeleteUserDetails(user.ID);

                //Reload iser for fixing the issue of failure of lazy loading
                user = userRepository.FindUserByUserId(user.ID);

                user.FirstName = editInvitation.FirstName;
                user.LastName = editInvitation.LastName;
                user.Rowversion = editInvitation.Rowversion;
                AddUserRoles(editInvitation, user);

                userRepository.UpdateUser(user);
                transationScope.Complete();
            }
        }

        public void EditParticipantUserAndInvitation(Invitation editInvitation)
        {
            using (var transationScope = TransactionScopeBuilder.CreateReadCommitted())
            {
                var invitation = new Invitation();
                if (editInvitation.VendorId != null)
                {
                    invitation = invitationRepository.FindParticipantInvitation(editInvitation.EmailAddress, (int)editInvitation.VendorId, true);
                }
                else
                {
                    invitation = invitationRepository.FindParticipantInvitation(editInvitation.EmailAddress, (int)editInvitation.CustomerId, false);
                }

                var user = userRepository.FindUserByEmailAddress(editInvitation.EmailAddress);

                //update invitation only when invitation is existing
                if ((invitation != null) && (invitation.ID > 0))
                {
                    invitation.FirstName = editInvitation.FirstName;
                    invitation.LastName = editInvitation.LastName;
                    invitationRepository.UpdateInvitation(invitation);
                }
                //Update user
                user.FirstName = editInvitation.FirstName;
                user.LastName = editInvitation.LastName;
                user.Rowversion = editInvitation.Rowversion;
                userRepository.UpdateUser(user);

                //Update vendoruser or customeruser
                if (editInvitation.VendorId != null && editInvitation.VendorId > 0)
                {
                    invitationRepository.CleanVendorUser((int)editInvitation.VendorId, user.ID);
                    editInvitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var vendorUser = new VendorUser
                        {
                            UserID = user.ID,
                            VendorID = (int)editInvitation.VendorId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddVendorUser(vendorUser);
                    });
                }
                if (editInvitation.CustomerId != null && editInvitation.CustomerId > 0)
                {
                    invitationRepository.CleanCustomerUser((int)editInvitation.CustomerId, user.ID);
                    editInvitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var customerUser = new CustomerUser()
                        {
                            UserId = user.ID,
                            CustomerId = (int)editInvitation.CustomerId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddCustomerUser(customerUser);
                    });
                }

                transationScope.Complete();
            }
        }

        public List<UserInviteModel> LoadInvitations(string searchText, int vendorId, string orderBy, string sortDirection)
        {
            return invitationRepository.LoadInvitations(searchText, vendorId, orderBy, sortDirection);
        }

        public IDictionary<long, string> GetUsersByGroup(string[] groupName)
        {
            return userRepository.GetUsersByGroup(groupName);
        }

        public IDictionary<long, string> GetUsersByApplicationsWorkflow(string accountName)
        {
            return userRepository.GetUsersByApplicationsWorkflow(accountName);
        }

        public Dictionary<long, string> GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType)
        {
            return userRepository.GetUsersByClaimsWorkflow(claimId, accountName, claimType);
        }

        public void RemoveAfterLoginRedirectUrl(int UserId)
        {
            userRepository.RemoveAfterloginRedirectUrl(UserId);
        }

        public void ResetloginRedirectUrl(long userId, string redirectURL)
        {
            userRepository.ResetloginRedirectUrl(userId, redirectURL);
        }

        public User FindUserByUserName(string username)
        {
            return userRepository.FindUserByUserName(username);
        }

        public User FindParticipantUserDetailsByEmail(string email, int participantId, bool isVendor)
        {
            var user = userRepository.FindUserByEmailAddress(email);
            //Load participant user roles, which is from vendoruser table or customeruser table
            var userRoles = userRepository.LoadParticipantUserRoles(user.ID, participantId, isVendor);
            userRoles.ForEach(c =>
            {
                user.Roles.Add(c);
            });
            return user;
        }
        public User FindParticipantUserDetailsByEmailView(string email, int participantId, bool isVendor)
        {
            var user = userRepository.FindUserByEmailAddress(email);
            var userRoles = userRepository.LoadParticipantUserRolesView(user.ID, participantId, isVendor);
            userRoles.ForEach(c =>
            {
                user.Roles.Add(c);
            });
            return user;
        }
        public List<Claim> LoadParticipantUserRolesClaims(long userId, int participantId, bool isVendor)
        {
            var userRoles = userRepository.LoadParticipantUserRoles(userId, participantId, isVendor);
            var claims = new List<Claim>();
            userRoles.ForEach(c =>
            {
                claims.Add(new Claim(ClaimTypes.Role, c.Name));
            });

            //Load PermissionAction claim
            var permissionClaims = userRepository.LoadParticipantUserPermissionClaims(userId, participantId, isVendor);
            claims.AddRange(permissionClaims);
            return claims;
        }

        public void ActiveUserByEmail(string email, bool isActive)
        {
            userRepository.ActiveUserByEmail(email, isActive);
        }

        //OTSTM2-1013
        public void UnlockStaffUserByEmail(string email)
        {
            userRepository.UnlockStaffUserByEmail(email);
        }

        public void UnlockParticipantUserById(long userId)
        {
            userRepository.UnlockParticipantUserById(userId);
        }

        public PaginationDTO<UserInviteModel, long> LoadInvitations(int pageIndex, int pageSize, string searchText,
            string orderBy, string sortDirection, int vendorId)
        {
            return invitationRepository.LoadInvitations(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
        }

        public List<User> GetClaimWorkflowUsers(string accountName, string workflowName)
        {
            return userRepository.GetClaimWorkflowUsers(accountName, workflowName);
        }

        public Invitation FindInvitationByApplicationId(int applicationId)
        {
            return invitationRepository.FindInvitationByApplicationId(applicationId);
        }

        public Invitation FindInvitationByVendorId(int vendorId)
        {
            return invitationRepository.FindInvitationByVendorId(vendorId);
        }

        public Invitation FindInvitation(int vendorId, string email, bool isVendor)
        {
            return invitationRepository.FindInvitation(vendorId, email, isVendor);
        }
        public void AddAdditonalUser(long userId, int participantId, bool isVendor, Invitation invitation)
        {
            if (isVendor)
            {
                //Add record to vendoruser table for vendor user
                var result = invitationRepository.FindVendorUser(userId, participantId);
                if (result == null)
                {
                    invitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var vendorUser = new VendorUser
                        {
                            UserID = userId,
                            VendorID = participantId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddVendorUser(vendorUser);
                    });
                }
            }
            else
            {
                //Add record to customeruser table for customer user
                var result = invitationRepository.FindCustomerUser(userId, participantId);
                if (result == null)
                {
                    //Add record to customeruser table for customer user
                    invitation.InvitationRoles.ToList().ForEach(c =>
                    {
                        var customerUser = new CustomerUser()
                        {
                            UserId = userId,
                            CustomerId = participantId,
                            IsActive = true,
                            RoleId = c.Role
                        };
                        invitationRepository.AddCustomerUser(customerUser);
                    });
                }
            }
        }

        public List<Claim> LoadUserVendorClaims(long userId)
        {
            return userRepository.GetUserVendors(userId);
        }

        public void ActiveParticipantUser(long userId, int vendorId, bool isVendor, bool isActive)
        {
            userRepository.ActiveParticipantUser(userId, vendorId, isVendor, isActive);
        }

        #region Staff User Security Changes
        public PaginationDTO<RoleListViewModel, int> GetAllRoles(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            return userRepository.GetAllRoles(pageIndex, pageSize, searchText, orderBy, sortDirection);
        }

        public List<string> GetUserNameByRoleName(string roleName)
        {
            return userRepository.GetUserNameByRoleName(roleName);
        }

        public List<RolePermissionViewModel> GetRolePermissions(string roleName)
        {
            return userRepository.GetRolePermissions(roleName);
        }

        public List<RolePermissionViewModel> GetDefaultRolePermissions()
        {
            return DataLoader.GetDefaultRolePermissions;
        }

        public void AddNewUserRole(RoleViewModel roleViewModel, string userName)
        {
            var appResources = DataLoader.GetAppResources;
            var appPermissions = DataLoader.GetAppPermissions;
            userRepository.AddNewUserRole(roleViewModel, userName, appResources, appPermissions);
        }
        public void UpdateUserRole(RoleViewModel roleViewModel, string userName)
        {
            var appPermissions = DataLoader.GetAppPermissions;
            userRepository.UpdateUserRole(roleViewModel, userName, appPermissions);
        }
        public bool DeleteUserRole(int roleID)
        {
            return userRepository.DeleteUserRole(roleID);
        }
        public bool RoleNameExists(string roleName)
        {
            return userRepository.RoleNameExists(roleName);
        }

        public List<RoleSelectionViewModel> GetStaffUserRoles()
        {
            return userRepository.GetStaffUserRoles();
        }

        public void CreateStaffUserInvitation(Invitation invitation, StaffUserViewModel staffUserViewModel)
        {
            //Populating invitation roles
            staffUserViewModel.AssignedRoles.ForEach(c =>
            {
                var invitationRole = new InvitationRole();
                invitationRole.Role = c.Id;
                invitationRole.Invitation = invitation;
                invitation.InvitationRoles.Add(invitationRole);
            });

            var stewardClaimsWorkflow = new InvitationClaimsWorkflow();
            stewardClaimsWorkflow.AccountName = staffUserViewModel.StewardClaimsWorkflow.AccountName;
            stewardClaimsWorkflow.Workflow = staffUserViewModel.StewardClaimsWorkflow.Workflow;
            stewardClaimsWorkflow.ActionGear = staffUserViewModel.StewardClaimsWorkflow.ActionGear;
            stewardClaimsWorkflow.Invitation = invitation;
            invitation.InvitationClaimsWorkflows.Add(stewardClaimsWorkflow);

            var collectorClaimsWorkflow = new InvitationClaimsWorkflow();
            collectorClaimsWorkflow.AccountName = staffUserViewModel.CollectorClaimsWorkflow.AccountName;
            collectorClaimsWorkflow.Workflow = staffUserViewModel.CollectorClaimsWorkflow.Workflow;
            collectorClaimsWorkflow.ActionGear = staffUserViewModel.CollectorClaimsWorkflow.ActionGear;
            collectorClaimsWorkflow.Invitation = invitation;
            invitation.InvitationClaimsWorkflows.Add(collectorClaimsWorkflow);

            var haulerClaimsWorkflow = new InvitationClaimsWorkflow();
            haulerClaimsWorkflow.AccountName = staffUserViewModel.HaulerClaimsWorkflow.AccountName;
            haulerClaimsWorkflow.Workflow = staffUserViewModel.HaulerClaimsWorkflow.Workflow;
            haulerClaimsWorkflow.ActionGear = staffUserViewModel.HaulerClaimsWorkflow.ActionGear;
            haulerClaimsWorkflow.Invitation = invitation;
            invitation.InvitationClaimsWorkflows.Add(haulerClaimsWorkflow);

            var processorClaimsWorkflow = new InvitationClaimsWorkflow();
            processorClaimsWorkflow.AccountName = staffUserViewModel.ProcessorClaimsWorkflow.AccountName;
            processorClaimsWorkflow.Workflow = staffUserViewModel.ProcessorClaimsWorkflow.Workflow;
            processorClaimsWorkflow.ActionGear = staffUserViewModel.ProcessorClaimsWorkflow.ActionGear;
            processorClaimsWorkflow.Invitation = invitation;
            invitation.InvitationClaimsWorkflows.Add(processorClaimsWorkflow);

            var rpmClaimsWorkflow = new InvitationClaimsWorkflow();
            rpmClaimsWorkflow.AccountName = staffUserViewModel.RPMClaimsWorkflow.AccountName;
            rpmClaimsWorkflow.Workflow = staffUserViewModel.RPMClaimsWorkflow.Workflow;
            rpmClaimsWorkflow.ActionGear = staffUserViewModel.RPMClaimsWorkflow.ActionGear;
            rpmClaimsWorkflow.Invitation = invitation;
            invitation.InvitationClaimsWorkflows.Add(rpmClaimsWorkflow);

            var stewardApplicationsWorkflow = new InvitationApplicationsWorkflow();
            stewardApplicationsWorkflow.AccountName = staffUserViewModel.StewardApplicationsWorkflow.AccountName;
            stewardApplicationsWorkflow.Routing = staffUserViewModel.StewardApplicationsWorkflow.Routing;
            stewardApplicationsWorkflow.ActionGear = staffUserViewModel.StewardApplicationsWorkflow.ActionGear;
            stewardApplicationsWorkflow.Invitation = invitation;
            invitation.InvitationApplicationsWorkflows.Add(stewardApplicationsWorkflow);

            var collectordApplicationsWorkflow = new InvitationApplicationsWorkflow();
            collectordApplicationsWorkflow.AccountName = staffUserViewModel.CollectorApplicationsWorkflow.AccountName;
            collectordApplicationsWorkflow.Routing = staffUserViewModel.CollectorApplicationsWorkflow.Routing;
            collectordApplicationsWorkflow.ActionGear = staffUserViewModel.CollectorApplicationsWorkflow.ActionGear;
            collectordApplicationsWorkflow.Invitation = invitation;
            invitation.InvitationApplicationsWorkflows.Add(collectordApplicationsWorkflow);

            var haulerApplicationsWorkflow = new InvitationApplicationsWorkflow();
            haulerApplicationsWorkflow.AccountName = staffUserViewModel.HaulerApplicationsWorkflow.AccountName;
            haulerApplicationsWorkflow.Routing = staffUserViewModel.HaulerApplicationsWorkflow.Routing;
            haulerApplicationsWorkflow.ActionGear = staffUserViewModel.HaulerApplicationsWorkflow.ActionGear;
            haulerApplicationsWorkflow.Invitation = invitation;
            invitation.InvitationApplicationsWorkflows.Add(haulerApplicationsWorkflow);

            var processorApplicationsWorkflow = new InvitationApplicationsWorkflow();
            processorApplicationsWorkflow.AccountName = staffUserViewModel.ProcessorApplicationsWorkflow.AccountName;
            processorApplicationsWorkflow.Routing = staffUserViewModel.ProcessorApplicationsWorkflow.Routing;
            processorApplicationsWorkflow.ActionGear = staffUserViewModel.ProcessorApplicationsWorkflow.ActionGear;
            processorApplicationsWorkflow.Invitation = invitation;
            invitation.InvitationApplicationsWorkflows.Add(processorApplicationsWorkflow);

            var rpmApplicationsWorkflow = new InvitationApplicationsWorkflow();
            rpmApplicationsWorkflow.AccountName = staffUserViewModel.RPMApplicationsWorkflow.AccountName;
            rpmApplicationsWorkflow.Routing = staffUserViewModel.RPMApplicationsWorkflow.Routing;
            rpmApplicationsWorkflow.ActionGear = staffUserViewModel.RPMApplicationsWorkflow.ActionGear;
            rpmApplicationsWorkflow.Invitation = invitation;
            invitation.InvitationApplicationsWorkflows.Add(rpmApplicationsWorkflow);

            invitationRepository.CreateInviation(invitation);
        }

        public StaffUserViewModel LoadStaffUserDetails(string email)
        {
            var user = userRepository.FindUserDetailsByEmail(email);
            var staffUserViewModel = new StaffUserViewModel();
            staffUserViewModel.Id = user.ID;
            staffUserViewModel.FirstName = user.FirstName;
            staffUserViewModel.LastName = user.LastName;
            staffUserViewModel.Email = user.Email;
            var assignedRoles = new List<RoleSelectionViewModel>();
            user.Roles.ToList().ForEach(c =>
            {
                //convert role to assigned user role
                var roleSelectionViewModel = new RoleSelectionViewModel();
                roleSelectionViewModel.Id = c.ID;
                roleSelectionViewModel.RoleName = c.Name;
                assignedRoles.Add(roleSelectionViewModel);
            });
            staffUserViewModel.AssignedRoles = assignedRoles;
            var allStaffUserRoles = GetStaffUserRoles();
            staffUserViewModel.AvailableRoles = allStaffUserRoles.Except(assignedRoles).ToList();

            staffUserViewModel.WorkflowList = new List<string>
            {
                TreadMarksConstants.NoneWorkflow,
                TreadMarksConstants.RepresentativeWorkflow,
                TreadMarksConstants.LeadWorkflow,
                TreadMarksConstants.SupervisorWorkflow,
                TreadMarksConstants.Approver1Workflow,
                TreadMarksConstants.Approver2Workflow
            };

            staffUserViewModel.StewardWorkflowList = new List<string>
            {
                TreadMarksConstants.NoneWorkflow,
                TreadMarksConstants.TSFClerkWorkflow
            };

            if (user.UserClaimsWorkflows.Count == 0)
            {

                staffUserViewModel.StewardClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
                staffUserViewModel.StewardClaimsWorkflow.ActionGear = false;
                staffUserViewModel.CollectorClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
                staffUserViewModel.CollectorClaimsWorkflow.ActionGear = false;
                staffUserViewModel.HaulerClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
                staffUserViewModel.HaulerClaimsWorkflow.ActionGear = false;
                staffUserViewModel.ProcessorClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
                staffUserViewModel.ProcessorClaimsWorkflow.ActionGear = false;
                staffUserViewModel.RPMClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
                staffUserViewModel.RPMClaimsWorkflow.ActionGear = false;

            }
            else
            {
                //Convert claims workflows
                staffUserViewModel.StewardClaimsWorkflow.Workflow = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.StewardAccount).Workflow;
                staffUserViewModel.StewardClaimsWorkflow.ActionGear = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.StewardAccount).ActionGear;
                staffUserViewModel.CollectorClaimsWorkflow.Workflow = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.CollectorAccount).Workflow;
                staffUserViewModel.CollectorClaimsWorkflow.ActionGear = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.CollectorAccount).ActionGear;
                staffUserViewModel.HaulerClaimsWorkflow.Workflow = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.HaulerAccount).Workflow;
                staffUserViewModel.HaulerClaimsWorkflow.ActionGear = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.HaulerAccount).ActionGear;
                staffUserViewModel.ProcessorClaimsWorkflow.Workflow = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.ProcessorAccount).Workflow;
                staffUserViewModel.ProcessorClaimsWorkflow.ActionGear = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.ProcessorAccount).ActionGear;
                staffUserViewModel.RPMClaimsWorkflow.Workflow = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.RPMAccount).Workflow;
                staffUserViewModel.RPMClaimsWorkflow.ActionGear = user.UserClaimsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.RPMAccount).ActionGear;
            }

            if (user.UserApplicationsWorkflows.Count == 0)
            {
                staffUserViewModel.StewardApplicationsWorkflow.Routing = false;
                staffUserViewModel.StewardApplicationsWorkflow.ActionGear = false;
                staffUserViewModel.CollectorApplicationsWorkflow.Routing = false;
                staffUserViewModel.CollectorApplicationsWorkflow.ActionGear = false;
                staffUserViewModel.HaulerApplicationsWorkflow.Routing = false;
                staffUserViewModel.HaulerApplicationsWorkflow.ActionGear = false;
                staffUserViewModel.ProcessorApplicationsWorkflow.Routing = false;
                staffUserViewModel.ProcessorApplicationsWorkflow.ActionGear = false;
                staffUserViewModel.RPMApplicationsWorkflow.Routing = false;
                staffUserViewModel.RPMApplicationsWorkflow.ActionGear = false;

            }
            else
            {
                //Convert application workflows
                staffUserViewModel.StewardApplicationsWorkflow.Routing = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.StewardAccount).Routing;
                staffUserViewModel.StewardApplicationsWorkflow.ActionGear = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.StewardAccount).ActionGear;
                staffUserViewModel.CollectorApplicationsWorkflow.Routing = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.CollectorAccount).Routing;
                staffUserViewModel.CollectorApplicationsWorkflow.ActionGear = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.CollectorAccount).ActionGear;
                staffUserViewModel.HaulerApplicationsWorkflow.Routing = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.HaulerAccount).Routing;
                staffUserViewModel.HaulerApplicationsWorkflow.ActionGear = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.HaulerAccount).ActionGear;
                staffUserViewModel.ProcessorApplicationsWorkflow.Routing = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.ProcessorAccount).Routing;
                staffUserViewModel.ProcessorApplicationsWorkflow.ActionGear = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.ProcessorAccount).ActionGear;
                staffUserViewModel.RPMApplicationsWorkflow.Routing = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.RPMAccount).Routing;
                staffUserViewModel.RPMApplicationsWorkflow.ActionGear = user.UserApplicationsWorkflows.FirstOrDefault(c => c.AccountName == TreadMarksConstants.RPMAccount).ActionGear;
            }
            return staffUserViewModel;
        }

        public void EditStaffUserDetail(StaffUserViewModel staffUserViewModel)
        {
            //Update invitation for first name and last name
            var invitation = invitationRepository.FindInvitationByUserName(staffUserViewModel.Email);
            invitation.FirstName = staffUserViewModel.FirstName;
            invitation.LastName = staffUserViewModel.LastName;
            invitationRepository.UpdateInvitation(invitation);

            //Update user and related tables
            var user = userRepository.FindUserByEmailAddress(staffUserViewModel.Email);

            //Assuming all workflows are sent as null
            if (staffUserViewModel.StewardClaimsWorkflow == null)
                throw new Exception("EditStaffUserDetail: ClaimsWorkflow null");

            //delete the old user details first
            userRepository.DeleteUserDetails(user.ID);

            //Reload user for fixing the issue of failure of lazy loading
            user = userRepository.FindUserByUserId(user.ID);
            user.FirstName = staffUserViewModel.FirstName;
            user.LastName = staffUserViewModel.LastName;

            //Add user roles
            var allRoles = userRepository.LoadAllUserRoles();
            staffUserViewModel.AssignedRoles.ToList().ForEach(c =>
            {
                var role = allRoles.Find(r => r.ID == c.Id);
                if (role != null)
                {
                    user.Roles.Add(role);
                }
            });

            //user claims workflow
            var stewardClaimsWorkflow = new UserClaimsWorkflow();
            stewardClaimsWorkflow.AccountName = staffUserViewModel.StewardClaimsWorkflow.AccountName;
            stewardClaimsWorkflow.Workflow = staffUserViewModel.StewardClaimsWorkflow.Workflow;
            stewardClaimsWorkflow.ActionGear = staffUserViewModel.StewardClaimsWorkflow.ActionGear;
            stewardClaimsWorkflow.User = user;
            user.UserClaimsWorkflows.Add(stewardClaimsWorkflow);

            var collectorClaimsWorkflow = new UserClaimsWorkflow();
            collectorClaimsWorkflow.AccountName = staffUserViewModel.CollectorClaimsWorkflow.AccountName;
            collectorClaimsWorkflow.Workflow = staffUserViewModel.CollectorClaimsWorkflow.Workflow;
            collectorClaimsWorkflow.ActionGear = staffUserViewModel.CollectorClaimsWorkflow.ActionGear;
            collectorClaimsWorkflow.User = user;
            user.UserClaimsWorkflows.Add(collectorClaimsWorkflow);

            var haulerClaimsWorkflow = new UserClaimsWorkflow();
            haulerClaimsWorkflow.AccountName = staffUserViewModel.HaulerClaimsWorkflow.AccountName;
            haulerClaimsWorkflow.Workflow = staffUserViewModel.HaulerClaimsWorkflow.Workflow;
            haulerClaimsWorkflow.ActionGear = staffUserViewModel.HaulerClaimsWorkflow.ActionGear;
            haulerClaimsWorkflow.User = user;
            user.UserClaimsWorkflows.Add(haulerClaimsWorkflow);

            var processorClaimsWorkflow = new UserClaimsWorkflow();
            processorClaimsWorkflow.AccountName = staffUserViewModel.ProcessorClaimsWorkflow.AccountName;
            processorClaimsWorkflow.Workflow = staffUserViewModel.ProcessorClaimsWorkflow.Workflow;
            processorClaimsWorkflow.ActionGear = staffUserViewModel.ProcessorClaimsWorkflow.ActionGear;
            processorClaimsWorkflow.User = user;
            user.UserClaimsWorkflows.Add(processorClaimsWorkflow);

            var rpmClaimsWorkflow = new UserClaimsWorkflow();
            rpmClaimsWorkflow.AccountName = staffUserViewModel.RPMClaimsWorkflow.AccountName;
            rpmClaimsWorkflow.Workflow = staffUserViewModel.RPMClaimsWorkflow.Workflow;
            rpmClaimsWorkflow.ActionGear = staffUserViewModel.RPMClaimsWorkflow.ActionGear;
            rpmClaimsWorkflow.User = user;
            user.UserClaimsWorkflows.Add(rpmClaimsWorkflow);


            //Convert invitation applications workflow to user applications workflow
            var stewardApplicationsWorkflow = new UserApplicationsWorkflow();
            stewardApplicationsWorkflow.AccountName = staffUserViewModel.StewardApplicationsWorkflow.AccountName;
            stewardApplicationsWorkflow.Routing = staffUserViewModel.StewardApplicationsWorkflow.Routing;
            stewardApplicationsWorkflow.ActionGear = staffUserViewModel.StewardApplicationsWorkflow.ActionGear;
            stewardApplicationsWorkflow.User = user;
            user.UserApplicationsWorkflows.Add(stewardApplicationsWorkflow);

            var collectordApplicationsWorkflow = new UserApplicationsWorkflow();
            collectordApplicationsWorkflow.AccountName = staffUserViewModel.CollectorApplicationsWorkflow.AccountName;
            collectordApplicationsWorkflow.Routing = staffUserViewModel.CollectorApplicationsWorkflow.Routing;
            collectordApplicationsWorkflow.ActionGear = staffUserViewModel.CollectorApplicationsWorkflow.ActionGear;
            collectordApplicationsWorkflow.User = user;
            user.UserApplicationsWorkflows.Add(collectordApplicationsWorkflow);

            var haulerApplicationsWorkflow = new UserApplicationsWorkflow();
            haulerApplicationsWorkflow.AccountName = staffUserViewModel.HaulerApplicationsWorkflow.AccountName;
            haulerApplicationsWorkflow.Routing = staffUserViewModel.HaulerApplicationsWorkflow.Routing;
            haulerApplicationsWorkflow.ActionGear = staffUserViewModel.HaulerApplicationsWorkflow.ActionGear;
            haulerApplicationsWorkflow.User = user;
            user.UserApplicationsWorkflows.Add(haulerApplicationsWorkflow);

            var processorApplicationsWorkflow = new UserApplicationsWorkflow();
            processorApplicationsWorkflow.AccountName = staffUserViewModel.ProcessorApplicationsWorkflow.AccountName;
            processorApplicationsWorkflow.Routing = staffUserViewModel.ProcessorApplicationsWorkflow.Routing;
            processorApplicationsWorkflow.ActionGear = staffUserViewModel.ProcessorApplicationsWorkflow.ActionGear;
            processorApplicationsWorkflow.User = user;
            user.UserApplicationsWorkflows.Add(processorApplicationsWorkflow);

            var rpmApplicationsWorkflow = new UserApplicationsWorkflow();
            rpmApplicationsWorkflow.AccountName = staffUserViewModel.RPMApplicationsWorkflow.AccountName;
            rpmApplicationsWorkflow.Routing = staffUserViewModel.RPMApplicationsWorkflow.Routing;
            rpmApplicationsWorkflow.ActionGear = staffUserViewModel.RPMApplicationsWorkflow.ActionGear;
            rpmApplicationsWorkflow.User = user;
            user.UserApplicationsWorkflows.Add(rpmApplicationsWorkflow);

            userRepository.UpdateUser(user);

            //AddActivityEditStaffUser(oldStaffUserViewModel, staffUserViewModel);
        }
        #endregion

        private void AddActivityEditStaffUser(StaffUserViewModel oldUser, StaffUserViewModel newUser)
        {
            var activity = new Activity();
            var assignerUser = SecurityContextHelper.CurrentUser;
            UserReference assigneeUser = null;

            List<StaffUserViewModel> rt = oldUser.DetailedCompare(newUser);

            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName) : string.Empty;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName) : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.AdminMenuActivity;

            activity.ActivityArea = TreadMarksConstants.AdminActivityStaffUsers;
            if (true)
            {
                activity.Message = string.Format("{0} <strong>Edit</strong> user <strong># {1}</strong> from values to values <strong># {2}</strong>", activity.InitiatorName, oldUser.FirstName + " " + oldUser.LastName, oldUser.Email);
            }

            activity.ObjectId = (int)newUser.Id;

            this.messageRepository.AddActivity(activity);
            //Publish activity event
            eventAggregator.GetEvent<ActivityEvent>().Publish(activity.ObjectId);

        }

    }
}