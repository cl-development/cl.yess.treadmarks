﻿using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.ValidationEngine
{
    /// <summary>
    /// Validator interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IValidator<T> where T : class
    {
        /// <summary>
        /// Execute validation
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        ValidationResults Validate(T target);
        /// <summary>
        /// Execute validation based on rule set
        /// </summary>
        /// <param name="target"></param>
        /// <param name="rulesets"></param>
        /// <returns></returns>
        ValidationResults Validate(T target, params string[] rulesets);
    }
}
