﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Security.Implementations
{
    internal static class PBKDF2HashHandler
    {
        private const int _saltByteSize = 32;
        private const int _hashByteSize = 32;
        private const int _pbkdf2Iterations = 1000;

        private const int iterationIndex = 0;
        private const int saltIndex = 1;
        private const int pbkdf2Index = 2;

        /// <summary>
        /// Create a salted PBKDF2 has for the password
        /// </summary>
        /// <param name="password">The password to hash</param>
        /// <returns>Salted hash in the format [iterations: salt: hash]</returns>
        internal static string CreateSaltedHashedPassword(string password)
        {
            byte[] salt = CreateSalt(_saltByteSize);
            byte[] hash = PBKDF2(password, salt, _pbkdf2Iterations, _hashByteSize);

            return _pbkdf2Iterations + ":" + Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="correctHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        internal static bool ValidateSaltedHashedPassword(string password, string correctHash)
        {
            // Extract the parameters from the hash
            char[] delimiter = { ':' };
            string[] split = correctHash.Split(delimiter);
            int iterations = Int32.Parse(split[iterationIndex]);
            byte[] salt = Convert.FromBase64String(split[saltIndex]);
            byte[] hash = Convert.FromBase64String(split[pbkdf2Index]);

            byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }

        /// <summary>
        /// Create a RNGCrypto salt
        /// </summary>
        /// <param name="saltByteSize">The RNGCrypto salt size</param>
        /// <returns>RNGCrypto salt</returns>
        private static byte[] CreateSalt(int saltByteSize)
        {
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[saltByteSize];
            csprng.GetBytes(salt);

            return salt;
        }

        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }

        /// <summary>
        /// Create a PBKDF2-SHA256 Hash
        /// </summary>
        /// <param name="password">The Password to hash</param>
        /// <param name="salt">The Salt to use. Preferably RNGCrypto</param>
        /// <param name="iterations">The PBKDF2 iterations</param>
        /// <param name="outputByteSize">The hash size</param>
        /// <returns></returns>
        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputByteSize)
        {
            byte[] pbkdf2Bytes;
            using (PBKDF2 pbkdf2 = new PBKDF2(password, salt, iterations))
            {
                pbkdf2Bytes = pbkdf2.GetBytes(outputByteSize);
            }

            return pbkdf2Bytes;
        }
    }
}
