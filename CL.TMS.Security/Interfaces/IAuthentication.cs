﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.Security.Interfaces
{
    public interface IAuthentication
    {
        AuthenticationUser VerifyUser(string userName, string userPassword, bool shouldLockOut, int maxLoginAttempts);
        ClaimsIdentity CreateIdentity(AuthenticationUser authenticationUser, string authenticationType);

        string GetAfterLoginRedirectUrl(long userId);
        bool ConfirmPassword(string userLoginName, string password);
    }
}
