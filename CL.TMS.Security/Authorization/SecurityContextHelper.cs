﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.System;
using Microsoft.Owin.Security;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Roles;

namespace CL.TMS.Security.Authorization
{
    public static class SecurityContextHelper
    {
        public static void RemoveCurrentUserRolePermissonClaims()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var identity = user.Identity as ClaimsIdentity;
            var userRoleClaims = user.Claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            userRoleClaims.ForEach(c =>
            {
                identity.RemoveClaim(c);
            });
            var userPermissionClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions).ToList();
            userPermissionClaims.ForEach(c =>
            {
                identity.RemoveClaim(c);
            });
        }

        public static void AddUserRolePermissionClaims(List<Claim> claims)
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var identity = user.Identity as ClaimsIdentity;
            claims.ForEach(c =>
            {
                if (!identity.Claims.Contains(c))
                {
                    identity.AddClaim(c);
                }
            });
        }

        public static bool HasUserRole()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            return user.HasClaim(c => c.Type == ClaimTypes.Role);
        }
        public static bool IsStaff()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var hasUserRoles = user.HasClaim(c => c.Type == ClaimTypes.Role);
            return (hasUserRoles && !IsParticipant());
        }

        public static bool IsParticipant()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var hasUserRole = user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleRead) || user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleWrite)
                   || user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleSubmit) || user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleParticipantAdmin);
            //mulitple vendor scenarios
            var noUserRole = !user.HasClaim(c => c.Type == ClaimTypes.Role);
            return (noUserRole || hasUserRole);
        }

        public static VendorReference CurrentVendor
        {
            get
            {
                if (HttpContext.Current.Session["SelectedVendor"] != null)
                {
                    return HttpContext.Current.Session["SelectedVendor"] as VendorReference;
                }
                if (IsOneUserVendorClaim)
                {
                    var result = new VendorReference();
                    var user = HttpContext.Current.User as ClaimsPrincipal;
                    var claim = user.Claims.Where(c => c.Type == "UserVendor").First();
                    result.VendorId = Convert.ToInt32(claim.Value);
                    return result;
                }
                return null;
            }
        }
        public static List<Claim> UserVendorClaims
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                return user.Claims.Where(c => c.Type == "UserVendor").ToList();
            }
        }

        public static bool IsOneUserVendorClaim
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claims = user.Claims.Where(c => c.Type == "UserVendor").ToList();
                return claims.Count == 1;
            }
        }

        public static UserReference CurrentUser
        {
            get
            {
                return new UserReference
                {
                    Id = ((ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Sid) == "") ? 0 : Convert.ToInt64(ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Sid))),
                    FirstName = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.GivenName),
                    LastName = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Surname),
                    UserName = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Name),
                    Email = ResourceActionAuthorizationHandler.GetResourceValue(ClaimTypes.Email)
                };
            }
        }

        [Obsolete]
        public static bool HasPrivilege(string resource, int permissionLevel)
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var permissionValue = string.Format("{0},{1}", resource, permissionLevel);
            return user.HasClaim(TreadMarksConstants.RolePermissions, permissionValue);
        }

        public static ClaimsWorkflowViewModel StewardClaimsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ClaimsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ClaimsWorkflowViewModel(TreadMarksConstants.StewardAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.StewardAccount)
                        {
                            result.Workflow = claimValues[1];
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ClaimsWorkflowViewModel CollectorClaimsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ClaimsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ClaimsWorkflowViewModel(TreadMarksConstants.CollectorAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.CollectorAccount)
                        {
                            result.Workflow = claimValues[1];
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ClaimsWorkflowViewModel HaulerClaimsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ClaimsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ClaimsWorkflowViewModel(TreadMarksConstants.HaulerAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.HaulerAccount)
                        {
                            result.Workflow = claimValues[1];
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ClaimsWorkflowViewModel ProcessorClaimsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ClaimsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ClaimsWorkflowViewModel(TreadMarksConstants.ProcessorAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.ProcessorAccount)
                        {
                            result.Workflow = claimValues[1];
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ClaimsWorkflowViewModel RPMClaimsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ClaimsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ClaimsWorkflowViewModel(TreadMarksConstants.RPMAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.RPMAccount)
                        {
                            result.Workflow = claimValues[1];
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ApplicationsWorkflowViewModel StewardApplicationsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ApplicationsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ApplicationsWorkflowViewModel(TreadMarksConstants.StewardAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.StewardAccount)
                        {
                            result.Routing = Convert.ToBoolean(claimValues[1]);
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ApplicationsWorkflowViewModel CollectorApplicationsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ApplicationsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ApplicationsWorkflowViewModel(TreadMarksConstants.CollectorAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.CollectorAccount)
                        {
                            result.Routing = Convert.ToBoolean(claimValues[1]);
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ApplicationsWorkflowViewModel HaulerApplicationsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ApplicationsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ApplicationsWorkflowViewModel(TreadMarksConstants.HaulerAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.HaulerAccount)
                        {
                            result.Routing = Convert.ToBoolean(claimValues[1]);
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ApplicationsWorkflowViewModel ProcessorApplicationsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ApplicationsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ApplicationsWorkflowViewModel(TreadMarksConstants.ProcessorAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.ProcessorAccount)
                        {
                            result.Routing = Convert.ToBoolean(claimValues[1]);
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        public static ApplicationsWorkflowViewModel RPMApplicationsWorkflow
        {
            get
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claimsWorkflowClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.ApplicationsWorkflow).ToList();
                if (claimsWorkflowClaims.Count > 0)
                {
                    var result = new ApplicationsWorkflowViewModel(TreadMarksConstants.RPMAccount);
                    claimsWorkflowClaims.ForEach(c =>
                    {
                        var claimValues = c.Value.Split(new char[] { ',' });
                        if (claimValues[0] == TreadMarksConstants.RPMAccount)
                        {
                            result.Routing = Convert.ToBoolean(claimValues[1]);
                            result.ActionGear = Convert.ToBoolean(claimValues[2]);
                        }
                    });
                    return result;
                }
                return null;
            }
        }

        /// <summary>
        /// Vendor context security check
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public static bool IsValidVendorContext(int vendorId)
        {
            //Only apply to participant
            if (IsParticipant())
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var vendorClaims = user.Claims.Where(c => c.Type == "UserVendor").ToList();
                var isValidVendor = false;
                foreach (var claim in vendorClaims)
                {
                    if (Convert.ToInt32(claim.Value) == vendorId)
                    {
                        isValidVendor = true;
                        break;
                    }
                }
                return isValidVendor;
            }
            else
            {
                return true;
            }
        }

        public static bool RequirePageReditect(int? vendorId)
        {
            var currentVendor = SecurityContextHelper.CurrentUser;
            return currentVendor.Id == vendorId;
        }

        #region Participant Permission Check

        public static bool IsParticipantAdmin()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            return user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleParticipantAdmin);
        }

        public static bool IsRead()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            return user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleRead);
        }

        public static bool IsReadOnly()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            var claimsCount = user.Claims.Where(c => c.Type == ClaimTypes.Role).Count();
            return claimsCount == 1 && user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleRead);
        }

        public static bool IsWrite()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            return user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleWrite);
        }

        public static bool IsSubmit()
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            return user.HasClaim(ClaimTypes.Role, TreadMarksConstants.RoleSubmit);
        }

        public static bool HasSubmitPermission
        {
            get
            {
                return IsParticipantAdmin() || IsSubmit();
            }
        }

        public static bool HasEditable
        {
            get
            {
                return IsParticipantAdmin() || IsWrite();
            }
        }

        #endregion
        /// <summary>
        /// for both staff and participant
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public static bool IsEditable(string resourceName)
        {
            if (IsStaff())
            {
                return ResourceActionAuthorizationHandler.CheckUserSecurity(resourceName) == SecurityResultType.EditSave;
            }
            else
            {
                return HasEditable || IsSubmit();
            }
        }
    }
}
