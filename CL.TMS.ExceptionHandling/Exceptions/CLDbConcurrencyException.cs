﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ExceptionHandling.Exceptions
{
    /// <summary>
    /// Database Concurrency Exception
    /// </summary>
    public class CLDbConcurrencyException:BaseException
    {
        public CLDbConcurrencyException(string message) : base(message)
        {
            
        }
    }
}
