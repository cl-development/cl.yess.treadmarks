﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ExceptionHandling.Exceptions
{
    /// <summary>
    /// Validation exception
    /// </summary>
    public class CLValidaitonException:BaseException
    {
        public ValidationResult ValidationDetails { get; private set; }
        public CLValidaitonException(string message, ValidationResult validationResults):base(message)
        {
            ValidationDetails = validationResults;
        }

        public CLValidaitonException(string message)
            : base(message)
        {

        }
    }
}
