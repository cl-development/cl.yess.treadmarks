﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ExceptionHandling.Exceptions
{
    /// <summary>
    /// Base Exception
    /// </summary>
    public class BaseException:Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public BaseException()
        {
            Initialize();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        public BaseException(string message) : base(message)
        {
            Initialize();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public BaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Initialize();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public BaseException(string message, Exception innerException) : base(message, innerException)
        {
            Initialize();
        }

        #endregion 

        #region Properties
        public Guid Id { get; set; }
        #endregion 

        #region Private Methods
        private void Initialize()
        {
            Id = Guid.NewGuid();
        }
        #endregion 
    }
}
