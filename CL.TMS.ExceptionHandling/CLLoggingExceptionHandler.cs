﻿using CL.Framework.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ExceptionHandling
{
    /// <summary>
    /// Logging exception handler
    /// </summary>
    public class CLLoggingExceptionHandler: IExceptionHandler
    {
        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            var message = string.Format("Exception occured with instance id {0}", handlingInstanceId);
            LogManager.LogExceptionWithMessage(message, exception);
            return exception;
        }
    }
}
