﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ExceptionHandling
{
   /// <summary>
   /// A helper class to handle exception occurs at both presentation layer and service layer
   /// </summary>
    public static class ExceptionHandlingHelper
    {
        /// <summary>
        /// Handling service layer exception
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static bool HandlingServiceException(Exception exception)
        {
            return ExceptionPolicy.HandleException(exception, ExceptionHandlingConfig.ServicePolicy);
        }

        /// <summary>
        /// Handling presentation layer exception
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static bool HandlingClientException(Exception exception)
        {
            return ExceptionPolicy.HandleException(exception, ExceptionHandlingConfig.UIPolicy);
        }
    }
}
