﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.BLL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.Validation;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.IRepository;
using CL.TMS.RuleEngine.InvitationBusinessRules;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Enum;

namespace CL.TMS.ApplicaitonBLL
{
    public class InvitationBO:BaseBO
    {
        private IInvitationRepository repository;
        private IApplicationRepository applicationRepository;
        private IApplicationInvitationRepository applicationInvitationRepository;

        public InvitationBO(IInvitationRepository invitationRepository)
        {
            repository = invitationRepository;          
        }
        public InvitationBO(IInvitationRepository invitationRepository, IApplicationRepository appRepository, IApplicationInvitationRepository appInvitationRepository)
        {
            repository = invitationRepository;
            applicationInvitationRepository = appInvitationRepository;
            applicationRepository = appRepository;
        }
        public void AddInvitation(Invitation invitation)
        {
            //Pseudo code for data validation at business layer if required
            //var user = new AuthenticationUser(); //it should be passed from service layer
            //var userValidator = new AuthenticationUserValidator();
            //var result = userValidator.Validate(user);
            //if (result.IsValid)
            //{
            //    //business logic 
            //}

            //1. Initialize business rule set strategy
            var businessRuleSetStrategy = InvitationRuleStrategyFactory.LoadInvitationRuleStrategy();

            //2. Create business rule set
            CreateBusinessRuleSet(businessRuleSetStrategy);

            //3. Execute business rules
            ExecuteBusinessRules(invitation);

            //4. Throw validaiton excetion if validation is failed
            if (!BusinessRuleExecutioResult.IsValid)
            {
                throw new CLValidaitonException("Invation validation is failed",BusinessRuleExecutioResult);
            }

            //5. call data access layer code to perform data persistence
           // repository.DoAction(invitation);
        }

        public void ValidateAndCreateApplicationInvitation(string emailID, string participantType)
        {
            try
            {
                ApplicationInvitation appInvitation = applicationInvitationRepository.GetApplicationInvitationByEmailID(emailID, participantType);

                //Create New record ApplicationInvitation
                if (appInvitation == null)
                {
                    appInvitation = CreateAndSendToken(emailID, participantType);
                }
                // Use existing ApplicationInvitation, Check Status 
                else
                {
                    //Already clicked the email link
                    if (appInvitation.ApplicationID != null)
                    {
                       
                        string message = "";
                        switch ((ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), applicationRepository.GetSingleApplication(appInvitation.ApplicationID.Value).Status))
                        {
                            case ApplicationStatusEnum.Approved:
                                {
                                    this.responseMessages.Add(string.Format("You have already been registered for email: {0} and participant type: {1}", emailID, participantType));
                                    this.responseStatus = WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Open:
                                {
                                    ResendInvite(appInvitation);
                                    break;
                                }
                            case ApplicationStatusEnum.Submitted:
                                {
                                    this.responseMessages.Add(string.Format("You have already submitted your application for email: {0} and participant type: {1}", emailID, participantType));
                                    this.responseStatus = WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.BackToApplicant:
                                {
                                    this.responseMessages.Add(string.Format("Your application is already in progress for email: {0} and participant type: {1}", emailID, participantType));
                                    this.responseStatus = WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.OnHold:
                                {
                                    this.responseMessages.Add(string.Format("Your application is on hold for email: {0} and participant type: {1}", emailID, participantType));
                                    this.responseStatus = WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Assigned:
                                {
                                    this.responseMessages.Add(string.Format("Your application is already in progress for email: {0} and participant type: {1}", emailID, participantType));
                                    this.responseStatus = WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Denied:
                                {
                                    appInvitation = CreateAndSendToken(emailID, participantType);
                                    break;
                                }
                        }

                    }
                    //Never clicked the email link
                    else
                    {
                        ResendInvite(appInvitation);
                    }


                }
            }
            catch (Exception ex)
            {
                this.responseMessages.Add(ex.Message.ToString());
                this.responseStatus = WebServiceResponseStatus.ERROR;
            }
        }

        private void ResendInvite(ApplicationInvitation appInvitaion)
        {
            if (ComposeAndSendEmail(appInvitaion))
            {
                this.responseMessages.Add(string.Format(string.Format("Application invitation is resent on email: {0}, Please check your email.",
                    appInvitaion.Email)));
                this.responseStatus = WebServiceResponseStatus.OK;
            }
        }

        private ApplicationInvitation CreateAndSendToken(string emailID, string participantType)
        {
            int participantTypeID = applicationInvitationRepository.GetParticipantTypeID(participantType);
            ApplicationInvitation appInvitaion = new ApplicationInvitation();
            Guid guid = Guid.NewGuid();
            appInvitaion.Email = emailID;
            appInvitaion.ParticipantTypeID = participantTypeID;
            appInvitaion.TokenID = guid.ToString();
            appInvitaion.ParticipantTypeName = participantType;
            if (ComposeAndSendEmail(appInvitaion))
            {
                //appInvitationServeice.Add(appInvitaion);
                this.responseMessages.Add(string.Format(string.Format("Application invitation is sent on email: {1}, Please check your email.",
                    participantType, emailID)));
                this.responseStatus = WebServiceResponseStatus.OK;
            }
            return appInvitaion;
        }

        public bool ComposeAndSendEmail(ApplicationInvitation appInvitation)
        {
            bool isEmailed = false;
            //try
            //{
            //    TMS.Common.Email.Email emailer = new Common.Email.Email(Convert.ToInt32(AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpPort")),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpServer"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpUserName"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.smtpPassword"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.dataElementBegin"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.dataElementEnd"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.defaultEmailTemplateDirectory"),
            //              AppInvitationRepository.GetApplicationSettingParamValue("Email.defaultFrom"),
            //              Convert.ToBoolean(AppInvitationRepository.GetApplicationSettingParamValue("Email.useSSL")));
            //    //string body = GetInvitaionParamValue("Invitation.InviteBody");

            //    string invitationSubject = AppInvitationRepository.GetApplicationSettingParamValue("Application.InvitationEmailSubject").Replace("*ParticipantType*", appInvitation.ParticipantTypeName);
            //    string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\ApplicationInviteEmailTemplate.html");
            //    string body = System.IO.File.ReadAllText(htmlbody);
            //    var siteURl = Path.Combine(AppInvitationRepository.GetApplicationSettingParamValue("Settings.DomainName"),
            //        AppInvitationRepository.GetApplicationSettingParamValue("Settings.ApplicationInvitationAbsolutePath")
            //        );
            //    string combinedPathUrl = string.Format("{0}{1}/{2}", AppInvitationRepository.GetApplicationSettingParamValue("Settings.DomainName"),
            //        AppInvitationRepository.GetApplicationSettingParamValue("Settings.ApplicationInvitationAbsolutePath"),
            //        appInvitation.TokenID);
            //    body = body.Replace("@siteUrl", combinedPathUrl).Replace("@participantType", appInvitation.ParticipantTypeName);

            //    // body = body.Replace("@InviteGUID", appInvitation.Guid);
            //    // body = body.Replace("@ExpireDate", inv.ExpireDate.ToString());

            //    AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

            //    var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\RethinkTires.jpg"), MediaTypeNames.Image.Jpeg);
            //    var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\follow_us_on_twitter.jpg"), MediaTypeNames.Image.Jpeg);
            //    var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);
            //    var oTSEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\OTS_Email_Logo.jpg"), MediaTypeNames.Image.Jpeg);
            //    rethinkTiresLog.ContentId = "RethinkTires";
            //    followUsOnTwitter.ContentId = "follow_us_on_twitter";
            //    treadMarksLogo.ContentId = "TreadMarksLogo";
            //    oTSEmailLogo.ContentId = "OTS_Email_Logo";

            //    alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
            //    alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
            //    alternateViewHTML.LinkedResources.Add(treadMarksLogo);
            //    alternateViewHTML.LinkedResources.Add(oTSEmailLogo);


            //    emailer.SendEmail(null, appInvitation.Email, null, null, invitationSubject, body, appInvitation, null, alternateViewHTML);


                isEmailed = true;
                return isEmailed;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}


        }

        #region IResponseMessage<IAppUserModel> Members

        private WebServiceResponseStatus responseStatus = WebServiceResponseStatus.NONE;
      
        public int ResponseStatus
        {
            get { return (int)this.responseStatus; }
            set { this.responseStatus = (WebServiceResponseStatus)Enum.Parse(typeof(WebServiceResponseStatus), value.ToString()); }
        }

        private List<string> responseMessages = new List<string>();
      
        public List<string> ResponseMessages
        {
            get { return this.responseMessages; }
            set { this.responseMessages = value; }
        }

        
      
        #endregion


    }
}
