﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ApplicaitonBLL
{
    public class ApplicationBO
    {
        private IApplicationRepository applicationRepository;

        private ApplicationRepository clsApplicationRepository;

        public ApplicationBO(IApplicationRepository applicationRepository)
        {
            this.applicationRepository = applicationRepository;
            this.clsApplicationRepository = new ApplicationRepository();
        }

        public Application GetApplicationByID(int id)
        {
           // return applicationRepository.GetSingleApplication(id);
            return null;
        }

        public Application AddApplication(Application application)
        {
            clsApplicationRepository.AddApplication(application);
            return application;
        }
    }
}
