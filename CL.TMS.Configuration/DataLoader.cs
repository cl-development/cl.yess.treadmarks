﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Caching;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Repository.System;
using System.Globalization;
using CL.TMS.Repository.Registrant;
using CL.TMS.Common.Enum;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.DataContracts.ViewModel.Common;

namespace CL.TMS.Configuration
{
    public static class DataLoader
    {
        /// <summary>
        /// Load all items
        /// </summary>
        /// <returns></returns>
        public static List<Item> Items
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.ItemKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<Item>>(CachKeyManager.ItemKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadAllItems();
                CachingHelper.AddItem(CachKeyManager.ItemKey, result);
                return result;
            }
        }

        /// <summary>
        /// Load current item weight_Recovery Estimated Weights
        /// </summary>
        /// <returns></returns>
        public static List<ItemWeight> ItemWeights
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.ItemWeightKey))
                {
                    //Load data from cache
                    var cacheResult = CachingHelper.GetItem<List<ItemWeight>>(CachKeyManager.ItemWeightKey);

                    //Check effectivedate
                    var latestItem = cacheResult.OrderByDescending(c => c.EffectiveEndDate).FirstOrDefault();
                    if (latestItem.EffectiveEndDate >= DateTime.Now) return cacheResult;

                    //reload data from database
                    var repo = new SettingRepository();
                    var dbResult = repo.LoadRecoveryItemWeights(DateTime.Now);
                    CachingHelper.SaveItem(CachKeyManager.ItemWeightKey, dbResult);
                    return dbResult;
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadRecoveryItemWeights(DateTime.Now);
                CachingHelper.AddItem(CachKeyManager.ItemWeightKey, result, DateTime.Now.AddDays(60));
                return result;
            }
        }

        /// <summary>
        /// Load current item weight_Supply Estimated Weights
        /// </summary>
        /// <returns></returns>
        public static List<ItemWeight> SupplyItemWeights
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.SupplyItemWeightKey))
                {
                    //Load data from cache
                    var cacheResult = CachingHelper.GetItem<List<ItemWeight>>(CachKeyManager.SupplyItemWeightKey);                   
                    return cacheResult;
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadSupplyItemWeights();
                CachingHelper.AddItem(CachKeyManager.SupplyItemWeightKey, result);
                return result;
            }
        }

        /// <summary>
        /// Load Processor Specific Rates
        /// </summary>
        public static List<SpecificRateModel> ProcessorSpecificRates
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.ProcessorSpecificRateKey))//each time edit/create new Rate, clear this key[CachKeyManager.RateKey] from [CachingHelper]: CachingHelper.RemoveItem("Rates")
                {
                    //Load data from cache
                    var cacheResult = CachingHelper.GetItem<List<SpecificRateModel>>(CachKeyManager.ProcessorSpecificRateKey);

                    //Check effectivedate                    
                    var latestItem = cacheResult.OrderByDescending(c => c.EffectiveEndDate).FirstOrDefault();
                    if (latestItem.EffectiveEndDate >= DateTime.Now) return cacheResult;

                    //reload data from database
                    var repo = new SettingRepository();
                    var dbResult = repo.LoadSpecificRates();
                    CachingHelper.SaveItem(CachKeyManager.ProcessorSpecificRateKey, dbResult);
                    return dbResult;
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadSpecificRates();
                CachingHelper.AddItem(CachKeyManager.ProcessorSpecificRateKey, result, DateTime.Now.AddDays(60));
                return result;
            }
        }
        
        /// <summary>
        /// Load Rate Group Rates
        /// </summary>
        public static List<RateGroupRate> RateGroupRates
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.RateGroupRateKey))
                {
                    //Load data from cache
                    var cacheResult = CachingHelper.GetItem<List<RateGroupRate>>(CachKeyManager.RateGroupRateKey);
                    return cacheResult;
                }
                else
                {
                    var repo = new SettingRepository();
                    var dbResult = repo.LoadRateGroupRates();
                    CachingHelper.SaveItem(CachKeyManager.RateGroupRateKey, dbResult);
                    return dbResult;
                }
            }
        }

        /// <summary>
        /// Load current rates
        /// </summary>
        /// <returns></returns>
        public static List<Rate> Rates
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.RateKey))//each time edit/create new Rate, clear this key[CachKeyManager.RateKey] from [CachingHelper]: CachingHelper.RemoveItem("Rates")
                {
                    //Load data from cache
                    var cacheResult = CachingHelper.GetItem<List<Rate>>(CachKeyManager.RateKey);

                    //Check effectivedate                    
                    var latestItem = cacheResult.OrderByDescending(c => c.EffectiveEndDate).FirstOrDefault();
                    if (latestItem.EffectiveEndDate >= DateTime.Now) return cacheResult;

                    //reload data from database
                    var repo = new SettingRepository();
                    var dbResult = repo.LoadRates(DateTime.Now);
                    CachingHelper.SaveItem(CachKeyManager.RateKey, dbResult);
                    return dbResult;
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadRates(DateTime.Now);
                CachingHelper.AddItem(CachKeyManager.RateKey, result, DateTime.Now.AddDays(60));
                return result;
            }
        }

        /// <summary>
        /// Load Transaction Type
        /// </summary>
        public static List<TransactionType> TransactionTypes
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.TransactionTypeKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<TransactionType>>(CachKeyManager.TransactionTypeKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadTransactionTypes();
                CachingHelper.AddItem(CachKeyManager.TransactionTypeKey, result);
                return result;
            }
        }

        /// <summary>
        /// Load Postal Code
        /// </summary>
        public static List<PostalCode> PostalCodes
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.PostalCodeKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<PostalCode>>(CachKeyManager.PostalCodeKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadPostalCodes();
                CachingHelper.AddItem(CachKeyManager.PostalCodeKey, result);
                return result;

            }
        }

        /// <summary>
        /// Load Regions - OTSTM2-215
        /// </summary>
        public static List<Region> Regions
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.RegionKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<Region>>(CachKeyManager.RegionKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadRegions();
                CachingHelper.AddItem(CachKeyManager.RegionKey, result);
                return result;
            }
        }

        /// <summary>
        /// Load Zones
        /// </summary>
        public static List<Zone> Zones
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.ZoneKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<Zone>>(CachKeyManager.ZoneKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadZones();
                CachingHelper.AddItem(CachKeyManager.ZoneKey, result);
                return result;
            }
        }
        /// <summary>
        /// Get Transaction Items
        /// </summary>
        public static List<Item> TransactionItems
        {
            get { return Items.Where(c => c.ItemCategory == 5).ToList(); }
        }

        /// <summary>
        /// Get Processor SPS Products
        /// </summary>
        public static List<Item> ProcessorSPSTransactionProducts
        {
            get { return Items.Where(c => c.ItemCategory == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue).ToList(); }
        }

        /// <summary>
        /// Get Processor PIT Products
        /// </summary>
        public static List<Item> ProcessorPITTransactionProducts
        {
            get { return Items.Where(c => c.ItemCategory == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue).ToList(); }
        }

        /// <summary>
        /// Get RPM SPS Products
        /// </summary>
        public static List<Item> RPMSPSTransactionProducts
        {
            get { return Items.Where(c => c.ItemCategory == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue).ToList(); }
        }

        public static List<TypeDefinition> MaterialTypeList
        {
            get { return AppDefinitions.Instance.TypeDefinitions["MaterialType"].ToList(); }
        }

        public static List<TypeDefinition> DispositionReasonList
        {
            get { return AppDefinitions.Instance.TypeDefinitions["DispositionReason"].ToList(); }
        }

        //OTSTM2-1016 Load all 249 countries of ISO
        public static List<string> CountryNames
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.CountryNameKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<string>>(CachKeyManager.CountryNameKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var countryList = new List<string>() { "Canada", "United States of America (the)" };
                var otherCountryList = repository.LoadCountries().Where(i => !countryList.Contains(i.Name)).OrderBy(c => c.Name).Select(c => c.Name).ToList();
                countryList.AddRange(otherCountryList);
                CachingHelper.AddItem(CachKeyManager.CountryNameKey, countryList);
                return countryList;
            }
        }

        /// <summary>
        /// Gets the list of countries based on ISO 3166-1
        /// </summary>
        /// <returns>Returns the list of countries based on ISO 3166-1</returns>
        public static List<string> GetCountriesByIso3166()
        {
            List<RegionInfo> countries = new List<RegionInfo>();
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo country = new RegionInfo(culture.LCID);
                if (countries.Where(p => p.Name == country.Name).Count() == 0)
                    countries.Add(country);
            }

            var countryList = new List<string>() { "Canada", "United States" };
            var otherCountryList = countries.Where(i => !countryList.Contains(i.EnglishName)).OrderBy(p => p.EnglishName).Select(p => p.EnglishName).ToList();
            countryList.AddRange(otherCountryList);

            return countryList;
        }

        /// <summary>
        /// Gets the list of countries based on ISO 3166-1
        /// </summary>
        /// <returns>Returns the list of countries based on ISO 3166-1</returns>
        public static List<string> GetCountriesByIso3166WithSpecialHandleForHongKong()
        {
            List<RegionInfo> countries = new List<RegionInfo>();
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo country = new RegionInfo(culture.LCID);
                if (countries.Where(p => p.Name == country.Name).Count() == 0)
                    countries.Add(country);
            }

            var countryList = new List<string>() { "Canada", "United States" };
            var otherCountryList = countries.Where(i => !countryList.Contains(i.EnglishName)).OrderBy(p => p.EnglishName).Select(p => p.EnglishName).ToList();
            countryList.AddRange(otherCountryList);

            if (countryList.Contains("Hong Kong"))
            {
                countryList.Add("Hong Kong S.A.R.");
            }
            if (countryList.Contains("Hong Kong S.A.R."))
            {
                countryList.Add("Hong Kong");
            }

            return countryList;
        }

        /// <summary>
        /// Get Application Items
        /// </summary>
        public static List<Item> ApplicationItems
        {
            get { return Items.Where(c => c.ItemCategory == 1).ToList(); }
        }

        /// <summary>
        /// Load GP Integration Accounts
        /// </summary>
        public static List<GpiAccount> GPIAccounts
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.GpiAccountKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<GpiAccount>>(CachKeyManager.GpiAccountKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadGPIAccounts();
                CachingHelper.AddItem(CachKeyManager.GpiAccountKey, result);
                return result;
            }
        }

        /// <summary>
        /// Load FI Accounts for Successor
        /// </summary>
        public static List<FIAccount> FIAccounts
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.FIAccountKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<FIAccount>>(CachKeyManager.FIAccountKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadFIAccounts();
                CachingHelper.AddItem(CachKeyManager.FIAccountKey, result);
                return result;
            }
        }

        //OTSTM2-486
        public static List<BusinessActivity> BusinessActivities
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.BusinessActivityKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<BusinessActivity>>(CachKeyManager.BusinessActivityKey);
                }

                var repo = new SettingRepository();
                var result = repo.LoadAllBusinessActivies();
                CachingHelper.AddItem(CachKeyManager.BusinessActivityKey, result);
                return result;
            }
        }

        //OTSTM2-486
        public static List<BusinessActivity> CollectorBusinessActivities
        {
            get { return BusinessActivities.Where(c => c.BusinessActivityType == (int)BusinessActivityTypeEnum.Collector).ToList(); }
        }

        public static List<RolePermissionViewModel> GetDefaultRolePermissions
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.RolePermissionsKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<RolePermissionViewModel>>(CachKeyManager.RolePermissionsKey);
                }

                var repo = new SettingRepository();
                var result = repo.GetDefaultRolePermissions();
                CachingHelper.AddItem(CachKeyManager.RolePermissionsKey, result);
                return result;
            }
        }

        public static List<AppResource> GetAppResources
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.AppResourcesKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<AppResource>>(CachKeyManager.AppResourcesKey);
                }

                var repo = new SettingRepository();
                var result = repo.GetAppResources();
                CachingHelper.AddItem(CachKeyManager.AppResourcesKey, result);
                return result;
            }
        }

        public static List<AppPermission> GetAppPermissions
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.AppPermissionsKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<AppPermission>>(CachKeyManager.AppPermissionsKey);
                }

                var repo = new SettingRepository();
                var result = repo.GetAppPermissions();
                CachingHelper.AddItem(CachKeyManager.AppPermissionsKey, result);
                return result;

            }
        }

        public static List<ParticipantType> ParticipantTypes {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.ParticipantTypeKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<ParticipantType>>(CachKeyManager.ParticipantTypeKey);
                }
                //Load data from database
                var repository = new SettingRepository();
                var result = repository.LoadAllParticipantTypes();
                CachingHelper.AddItem(CachKeyManager.ParticipantTypeKey, result);
                return result;
            }
        }

        public static List<Setting> GetAppSettings
        {
            get
            {
                if (CachingHelper.ContainsKey(CachKeyManager.AppSettingsKey))
                {
                    //Load data from cache
                    return CachingHelper.GetItem<List<Setting>>(CachKeyManager.AppSettingsKey);
                }

                var repo = new SettingRepository();
                var result = repo.GetAll().ToList();
                CachingHelper.AddItem(CachKeyManager.AppSettingsKey, result);
                return result;
            }
        }
    }
}