﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.Security;

namespace CL.TMS.Configuration
{
    public static class TransactionHelper
    {
        public static List<string> GetAvailableTireTypeListTireTypeList(string transactionType)
        {
            var list = new List<string>();

            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.DOT:
                    list.AddRange(new string[] { TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.STC:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.UCR:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.HIT:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.RTR:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.PTR:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
                case TreadMarksConstants.DOR:
                    list.AddRange(new string[] { TreadMarksConstants.PLT, TreadMarksConstants.MT, TreadMarksConstants.AGLS, TreadMarksConstants.IND, TreadMarksConstants.SOTR, TreadMarksConstants.MOTR, TreadMarksConstants.LOTR, TreadMarksConstants.GOTR });
                    break;
            }

            return list;
        }

        public static List<TransactionSupportingDocumentViewModel> GetAvailableSupportingDocumentList(string transactionType)
        {
            var list = new List<TransactionSupportingDocumentViewModel>();

            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.DOT:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDScaleTicket, DocumentFormat = 3, Required = false, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.HIT:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.STC:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.PTR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDScaleTicket, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.UCR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.RTR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDBillOfLading, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 3, DocumentName = TreadMarksConstants.SDInvoice, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.PIT:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDPaperForm, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDScaleTicket, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.SPS:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDScaleTicket, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDInvoice, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.SPSR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDInvoice, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
                case TreadMarksConstants.DOR:
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 1, DocumentName = TreadMarksConstants.SDScaleTicket, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    list.Add(new TransactionSupportingDocumentViewModel() { UniqueId = 2, DocumentName = TreadMarksConstants.SDInvoice, DocumentFormat = 3, Required = true, OriginalDocumentFormat = null });
                    break;
            }

            return list;
        }

        public static int GetTransactionTireTypeId(string tireTypeCode)
        {
            var item = DataLoader.TransactionItems.Single(c => c.ShortName == tireTypeCode);

            return item.ID;
        }

        public static List<TransactionTireTypeViewModel> GetTransactionTireTypeViewModelList(List<string> tireTypeList)
        {
            var list = new List<TransactionTireTypeViewModel>();
            foreach (var tireType in tireTypeList)
            {
                var item = DataLoader.TransactionItems.Single(c => c.ShortName == tireType);
                var itemWeight = DataLoader.ItemWeights.FirstOrDefault(c => c.ItemID == item.ID && c.EffectiveStartDate <= DateTime.Now && c.EffectiveEndDate >= DateTime.Now);

                list.Add(new TransactionTireTypeViewModel() { TireType = tireType, Count = null, StandardWeight = (itemWeight != null) ? itemWeight.StandardWeight : 0.00M, OriginalCount = null });
            }

            return list;
        }

        public static List<TransactionProductViewModel> GetTransactionProductViewModelList(int productCategoryId, int VendorId, DateTime? effectiveStartDate = null, DateTime? effectiveEndDate = null)
        {
            // to get the backward compatibility, lets set it back to current time
            if (effectiveStartDate == null)
            {
                effectiveStartDate = effectiveEndDate = DateTime.Now;
            }

            var list = new List<TransactionProductViewModel>();

            List<Item> itemList = null;

            if (productCategoryId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorSPSItem").DefinitionValue)
            {
                itemList = DataLoader.ProcessorSPSTransactionProducts;
                if (itemList == null) return null;

                //OTSTM2-1198: TODO load rate from specific rate table first
                foreach (var item in itemList)
                {
                    decimal ItemRate = 0.00M;
                    var specificRate = DataLoader.ProcessorSpecificRates.FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorSPS).DefinitionValue && c.EffectiveStartDate <= effectiveStartDate && c.EffectiveEndDate >= effectiveEndDate && c.VendorID == VendorId);
                    if (specificRate != null)
                    {
                        ItemRate = specificRate.ItemRate;
                    }
                    else
                    {
                        Rate globalRate;
                        globalRate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorSPS).DefinitionValue && c.EffectiveStartDate <= effectiveStartDate && c.EffectiveEndDate >= effectiveEndDate);
                        ItemRate = globalRate != null ? globalRate.ItemRate : 0.00M;
                    }
                    list.Add(new TransactionProductViewModel() { ProductId = item.ID, ProductCode = item.ShortName, ProductName = item.Description, Rate = ItemRate });
                }
            }

            if (productCategoryId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "ProcessorPITItem").DefinitionValue)
            {
                itemList = DataLoader.ProcessorPITTransactionProducts;
                if (itemList == null) return null;

                //OTSTM2-1198: TODO load rate from specific rate table first
                foreach (var item in itemList)
                {
                    decimal ItemRate = 0.00M;
                    var specificRate = DataLoader.ProcessorSpecificRates.FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= effectiveStartDate && c.EffectiveEndDate >= effectiveEndDate && c.VendorID == VendorId);
                    if (specificRate != null)
                    {
                        ItemRate = specificRate.ItemRate;
                    }
                    else
                    {
                        Rate globalRate;
                        globalRate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == item.ID && c.PaymentType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.RatePaymentType, TreadMarksConstants.ProcessorPITOutbound).DefinitionValue && c.EffectiveStartDate <= effectiveStartDate && c.EffectiveEndDate >= effectiveEndDate);
                        ItemRate = globalRate != null ? globalRate.ItemRate : 0.00M;
                    }
                    list.Add(new TransactionProductViewModel() { ProductId = item.ID, ProductCode = item.ShortName, ProductName = item.Description, Rate = ItemRate });
                }
            }

            if (productCategoryId == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.ItemCategory, "RPMSPSItem").DefinitionValue)
            {
                itemList = DataLoader.RPMSPSTransactionProducts;
                if (itemList == null) return null;

                foreach (var item in itemList)
                {
                    var itemRate = DataLoader.Rates.FirstOrDefault(c => c.ItemID == item.ID && c.EffectiveStartDate <= effectiveStartDate && c.EffectiveEndDate >= effectiveEndDate);

                    list.Add(new TransactionProductViewModel() { ProductId = item.ID, ProductCode = item.ShortName, ProductName = item.Description, Rate = (itemRate != null) ? itemRate.ItemRate : 0.00M });
                }
            }

            return list;
        }

        public static void initializeFormDate(TransactionFormDateViewModel FormDate, Period claimPeriod, SecurityResultType permission)
        {
            if ((permission == SecurityResultType.EditSave) || SecurityContextHelper.HasSubmitPermission)
            {
                FormDate.MinDate = (SecurityContextHelper.CurrentVendor.ActivationDate.HasValue && SecurityContextHelper.CurrentVendor.ActivationDate > claimPeriod.StartDate) ? SecurityContextHelper.CurrentVendor.ActivationDate.Value.Date : claimPeriod.StartDate.Date;
            }
            if (SecurityContextHelper.IsStaff())
            {
                FormDate.MinDate = (SecurityContextHelper.CurrentVendor.ActivationDate.HasValue) ? SecurityContextHelper.CurrentVendor.ActivationDate.Value.Date : DateTime.MinValue.Date;
            }
            FormDate.MaxDate = (claimPeriod.EndDate > DateTime.Now) ? DateTime.Now.Date : claimPeriod.EndDate.Date;

            FormDate.MinDate = FormDate.MinDate.Value.AddDays(1);
            FormDate.MaxDate = FormDate.MaxDate.Value.AddDays(1);

            // Form date focus setting
            FormDate.FocusDate = (SecurityContextHelper.CurrentVendor.ActivationDate.HasValue && SecurityContextHelper.CurrentVendor.ActivationDate > claimPeriod.StartDate) ? SecurityContextHelper.CurrentVendor.ActivationDate.Value.Date : claimPeriod.StartDate.Date;
            FormDate.FocusDate = FormDate.FocusDate.Date.ToUniversalTime();
        }
    }
}
