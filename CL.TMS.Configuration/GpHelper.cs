﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Configuration
{
    public static class GpHelper
    {
        public static GpStatus GetGpiStatus(string gpiStatusId)
        {
            switch (gpiStatusId.ToUpper())
            {
                case "P":
                    return GpStatus.PostedQueued;
                    break;
                case "P2":
                    return GpStatus.PostedImported;
                    break;
                case "X":
                    return GpStatus.FailedStaging;
                    break;
                case "X2":
                    return GpStatus.FailedImport;
                    break;
                case "H":
                    return GpStatus.Hold;
                    break;
                case "E":
                    return GpStatus.Extract;
                default:
                    return GpStatus.StatusUnknown;
                    break;
            }
        }

        public static string GetGpiStatusString(GpStatus gpStatus)
        {
            switch (gpStatus)
            {
                case GpStatus.PostedQueued:
                    return "P";
                    break;
                case GpStatus.PostedImported:
                    return "P2";
                    break;
                case GpStatus.FailedStaging:
                    return "X";
                    break;
                case GpStatus.FailedImport:
                    return "X2";
                    break;
                case GpStatus.Hold:
                    return "H";
                    break;
                case GpStatus.Extract:
                    return "E";
                default:
                    return string.Empty;
                    break;
            }
        }



        public static string GetGpiTypeID(GpManager gpManager)
        {
            if (gpManager == GpManager.Collector)
                return "C";
            else if (gpManager == GpManager.Hauler)
                return "H";
            else if (gpManager == GpManager.Participant)
                return "R";
            else if (gpManager == GpManager.Processor)
                return "P";
            else if (gpManager == GpManager.RPM)
                return "M";
            else if (gpManager == GpManager.Steward)
                return "S";
            else
                return string.Empty;
        }

    }
}
