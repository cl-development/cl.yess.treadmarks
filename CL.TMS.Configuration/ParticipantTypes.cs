﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.IRepository.System;
using CL.TMS.Repository.System;

namespace CL.TMS.Configuration
{
    public class ParticipantTypes
    {
        private static readonly Lazy<ParticipantTypes> lazy = new Lazy<ParticipantTypes>(() => new ParticipantTypes());
        private ISettingRepository repository;
        private ParticipantTypes()
        {
            repository = new SettingRepository();
            LoadParticipantTypes();
        }

        private void LoadParticipantTypes()
        {
            ParticipantTypeCollection = repository.LoadAllParticipantTypes();
        }

        public static ParticipantTypes Instance
        {
            get { return lazy.Value; }
        }

        public List<ParticipantType> ParticipantTypeCollection { get; private set; }
    }
}
