﻿using System;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;

namespace CL.TMS.Repository.Common
{
    public class ApplicationInvitationRepository : BaseRepository<ApplicationBoundedContext, ApplicationInvitation, int>, IApplicationInvitationRepository
    {
        private EntityStore<ParticipantType, int> participantTypeArchiveStore;
        public ApplicationInvitationRepository()
        {
            participantTypeArchiveStore = new EntityStore<ParticipantType, int>(context);
        }

        public ApplicationInvitation GetById(Guid ID)
        {
            var appInvitation = entityStore.All.FirstOrDefault(z => z.TokenID == ID.ToString());
            return AutoMapper.Mapper.Map<ApplicationInvitation>(appInvitation);
        }

        public void Update(ApplicationInvitation appInvitation)
        {
            ApplicationInvitation updatedapplication = entityStore.All.FirstOrDefault(m => m.ID == appInvitation.ID);
            updatedapplication.ApplicationID = appInvitation.ApplicationID;
            entityStore.Update(appInvitation);

        }

        public ApplicationInvitation UpdateInvitationDatetime(int applicaitonId)
        {
            ApplicationInvitation updatedapplication = entityStore.All.FirstOrDefault(m => m.ApplicationID == applicaitonId);
            updatedapplication.InvitationDate = DateTime.UtcNow;
            entityStore.Update(updatedapplication);
            return updatedapplication;
        }

        public Guid GetTokenByApplicationId(int applicationId)
        {
            Guid token;

            string tokenStr = entityStore.All.FirstOrDefault(r => r.ApplicationID == applicationId).TokenID;

            Guid.TryParse(tokenStr, out token);

            return token;
        }

        public string GetEmailByApplicationId(int applicationId)
        {
            string tokenStr = entityStore.All.FirstOrDefault(r => r.ApplicationID == applicationId).Email;

            return tokenStr;
        }

        public ApplicationInvitation GetApplicationInvitationByEmailID(string email, string participantType)
        {
            //testing code
            //var test = entityStore.All.Where(v => v.Email == email && v.ParticipantTypeName == participantType).FirstOrDefault();
            //below line throw exception randomly due to SQL connection object used by EF failed, detail is in system Event log.

            var applicationInvitation = entityStore.All.FirstOrDefault(v => v.Email == email && v.ParticipantTypeName == participantType);
            return AutoMapper.Mapper.Map<ApplicationInvitation>(applicationInvitation);
        }
        public int GetParticipantTypeID(string participantName)
        {
            int participantTypeID = participantTypeArchiveStore.All.FirstOrDefault(v => v.Name == participantName.Trim()).ID;
            return participantTypeID;
        }

        public ApplicationInvitation AddApplicationInvitation(ApplicationInvitation appInvitation)
        {
            entityStore.Insert(appInvitation);
            return appInvitation;
        }

        public ApplicationInvitation FindApplicationInvitationByAppID(int appId)
        {
            if (appId < 0) { throw new ArgumentOutOfRangeException("appId"); }

            return entityStore.All.FirstOrDefault(c => c.ApplicationID == appId);
        }
    }
}
