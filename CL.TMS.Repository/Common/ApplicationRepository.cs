﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CL.Framework.Common;
using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using System.Data.SqlTypes;
using CL.TMS.Framework.DTO;
using CL.TMS.Common.Extension;

namespace CL.TMS.Repository.Common
{
    public class ApplicationRepository : BaseRepository<ApplicationBoundedContext, Application, int>, IApplicationRepository
    {
        private EntityStore<Vendor, int> vendorArchiveStore;
        private EntityStore<ApplicationInvitation, int> applicationInvitationStore;
        public ApplicationRepository()
        {
            vendorArchiveStore = new EntityStore<Vendor, int>(context);
            applicationInvitationStore = new EntityStore<ApplicationInvitation, int>(context);
        }

        public void UpdateFormObject(int id, string formObject, string updatedBy)
        {
            ConditionCheck.Bounds(id, "Argument is out of bounds", int.MaxValue, 0);
            ConditionCheck.NullOrEmpty(formObject, "Argument cannot be null");

            Application app = entityStore.FindById(id);
            if ((app.Status != "Approved") && (app.Status != "BankInformationSubmitted") && (app.Status != "BankInformationApproved"))
            {
                app.FormObject = formObject;
                entityStore.Update(app, c => c.FormObject);
            }
        }

        public string GetFormObject(int id)
        {
            ConditionCheck.Bounds(id, "Argument is out of bounds", int.MaxValue, 0);
            Application app = entityStore.FindById(id);
            string formObject = app.FormObject;
            return formObject;

        }

        public Application GetSingleApplication(int id)
        {
            var application = entityStore.All.FirstOrDefault(z => z.ID == id);
            return AutoMapper.Mapper.Map<Application>(application);
        }

        public Application GetApplicationByTokenId(Guid tokenId)
        {
            string tokenIdStr = tokenId.ToString();
            ApplicationInvitation invitation = applicationInvitationStore.All.Where(a => a.TokenID == tokenIdStr).FirstOrDefault();
            if (invitation == null)
            {
                throw new NullReferenceException();
            }

            int? applicationId = invitation.ApplicationID;
            if (applicationId == null)
            {
                throw new NullReferenceException();
            }
            Application application = entityStore.FindById(applicationId.GetValueOrDefault());
            return application;
        }

        public Application AddApplication(Application app)
        {
            entityStore.Insert(app);
            return app;
        }

        public void UpdateApplication(Application application)
        {
            var obj = entityStore.FindById(application.ID);

            //if application is Approved or BankInformationSubmitted, no auto save
            if ((obj.Status != "Approved") && (obj.Status != "BankInformationSubmitted") && (obj.Status != "BankInformationApproved"))
            {
                obj.FormObject = application.FormObject;
                obj.UserIP = application.UserIP;
                obj.Company = application.Company;
                obj.Contact = application.Contact;               
                entityStore.Update(obj, c => c.FormObject, c => c.UserIP, c => c.Company, c => c.Contact);
            }
        }
        public void UpdateApplicationEntity(Application application)
        {
            entityStore.Update(application);
        }
        public void UpdateApplicationGraph(Application application)
        {
            entityStore.InsertOrUpdateGraph(application);
        }

        public void AddApplicationAttachments(int applicationId, IEnumerable<AttachmentModel> attachments)
        {
            ConditionCheck.Bounds(applicationId, "Argument is out of bounds", int.MaxValue, 0);
            var application = entityStore.FindById(applicationId);

            foreach (var attachmentModel in attachments)
            {
                var attachment = AutoMapper.Mapper.Map<Attachment>(attachmentModel);
                application.Attachments.Add(attachment);
            }

            entityStore.Update(application);
        }

        public IEnumerable<AttachmentModel> GetApplicationAttachments(int applicationId, bool bBankInfo = false)
        {
            ConditionCheck.Bounds(applicationId, "Argument is out of bounds", int.MaxValue, 0);

            var attachments = entityStore.FindById(applicationId).Attachments;
            //var attachmentsBank = attachments.Where(a => (a.FilePath.ToString().IndexOf("\\bankinfo\\") > -1));
            var attachmentsBank = attachments.Where(a => a.IsBankingRelated);
            if (bBankInfo)
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachmentsBank);
                return attachmentModelList;
            }
            else
            {
                var attachmentModelList = AutoMapper.Mapper.Map<IEnumerable<AttachmentModel>>(attachments);
                return attachmentModelList;
            }

        }

        public AttachmentModel GetApplicationAttachment(int applicationId, string fileUniqueName)
        {
            ConditionCheck.Bounds(applicationId, "Argument is out of bounds", int.MaxValue, 0);

            var attachment = entityStore.FindById(applicationId).Attachments.Single(r => r.UniqueName == fileUniqueName);

            var attachmentModel = AutoMapper.Mapper.Map<AttachmentModel>(attachment);

            return attachmentModel;
        }

        public void RemoveApplicationAttachment(int applicationId, string fileUniqueName)
        {
            ConditionCheck.Bounds(applicationId, "Argument is out of bounds", int.MaxValue, 0);

            var currentContext = (context as ApplicationBoundedContext);
            var application = currentContext.Applications.FirstOrDefault(a => a.ID == applicationId);
            var attachment = application.Attachments.FirstOrDefault(a => a.UniqueName == fileUniqueName);

            if (attachment != null)
            {
                application.Attachments.Remove(attachment);
                //currentContext.Attachments.Remove(attachment);
                attachment.FileName = "DELETE";
                currentContext.Attachments.Attach(attachment);
                var attachmentEntry = currentContext.Entry(attachment);
                attachmentEntry.State = EntityState.Modified;
                attachmentEntry.Property(a => a.FileName).IsModified = true;

                currentContext.SaveChanges();
            }
        }

        public void UpdateAttachmentDescription(string fileUniqueName, string description)
        {
            var currentContext = (context as ApplicationBoundedContext);
            var attachment = currentContext.Attachments.FirstOrDefault(r => r.UniqueName == fileUniqueName);

            if (attachment != null)
            {
                attachment.Description = description;

                currentContext.SaveChanges();
            }
        }

        public void SetStatus(int applicationId, ApplicationStatusEnum status, string denyReasons, long assignToUser = 0)
        {
            Application application = entityStore.FindById(applicationId);

            application.Status = status.ToString();
            switch (status)
            {
                case ApplicationStatusEnum.Submitted:
                    application.SubmittedDate = DateTime.UtcNow;
                    application.StatusIndex = 1;
                    break;
                case ApplicationStatusEnum.Assigned:
                case ApplicationStatusEnum.OnHold:
                case ApplicationStatusEnum.OffHold:
                    application.StatusIndex = 2;
                    break;
                case ApplicationStatusEnum.Open:
                    application.StatusIndex = 3;
                    break;
                case ApplicationStatusEnum.BackToApplicant:
                    int days = 7;
                    try
                    {
                        var dbContext = context as ApplicationBoundedContext;
                        var setting = dbContext.Settings.FirstOrDefault(i => i.Key == "Application.InvitationExpiryDays");
                        days = int.Parse(setting.Value);
                    }
                    catch (Exception ex)
                    {
                        days = 7;
                    }
                    application.StatusIndex = 3;
                    application.ExpireDate = DateTime.UtcNow.AddDays(days);
                    break;
                case ApplicationStatusEnum.Denied:
                    application.StatusIndex = 5;
                    break;
                case ApplicationStatusEnum.BankInformationSubmitted:
                case ApplicationStatusEnum.BankInformationApproved:
                    application.BankPolicyAgreementAcceptedCheck = true;
                    application.StatusIndex = 4;
                    break;
                default:
                    application.StatusIndex = 3;
                    break;
            }
            if (!string.IsNullOrEmpty(denyReasons))
            {
                application.DenyReasons = denyReasons;
            }
            if (-1 == assignToUser) //un-assign
            {
                application.AssignToUser = null;
            }
            else if (0 != assignToUser)
            {
                application.AssignToUser = assignToUser;
            }

            entityStore.Update(application);
        }


        public Application FindApplicationByAppID(int id)
        {
            if (id < 0) { throw new ArgumentOutOfRangeException("id"); }

            return entityStore.FindById(id);
        }


        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotes(int applicationId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.ApplicationId == applicationId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            ApplicationId = note.ApplicationId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
             .Take(pageSize)
             .ToList();

            return new PaginationDTO<ApplicationNoteViewMode, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForVendor(int vendorId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.VendorId == vendorId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            VendorId = note.VendorId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
             .Take(pageSize)
             .ToList();

            return new PaginationDTO<ApplicationNoteViewMode, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<ApplicationNoteViewMode, int> LoadApplicationNotesForCustomer(int customerId, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.CustomerId == customerId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            CustomerId = note.CustomerId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

            //Apllying seach text
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
             .Take(pageSize)
             .ToList();

            return new PaginationDTO<ApplicationNoteViewMode, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public void AddApplicationNote(ApplicationNote applicationNote)
        {
            var dbContext = context as ApplicationBoundedContext;
            dbContext.ApplicationNotes.Add(applicationNote);
            dbContext.SaveChanges();
        }


        public List<ApplicationNoteViewMode> LoadApplicationNotesForExport(int applicationId, string searchText,
            string sortcolumn, string sortdirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.ApplicationId == applicationId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            ApplicationId = note.ApplicationId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query =
                        query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "CreatedDate" : sortcolumn;

            if (sortdirection == "asc")
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn);
            else
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn + " descending");

            return query.ToList();
        }

        public List<ApplicationNoteViewMode> LoadVendorNotesForExport(int vendorId, string searchText,
            string sortcolumn, string sortdirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.VendorId == vendorId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            VendorId = note.VendorId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query =
                        query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "CreatedDate" : sortcolumn;

            if (sortdirection == "asc")
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn);
            else
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn + " descending");

            return query.ToList();
        }

        public List<ApplicationNoteViewMode> LoadCustomerNotesForExport(int customerId, string searchText,
            string sortcolumn, string sortdirection)
        {
            var dbContext = context as ApplicationBoundedContext;
            var query = from note in dbContext.ApplicationNotes.AsNoTracking()
                        join user in dbContext.Users
                            on note.UserId equals user.ID
                        where note.CustomerId == customerId
                        select new ApplicationNoteViewMode
                        {
                            ID = note.ID,
                            CustomerId = note.CustomerId,
                            Note = note.Note,
                            CreatedDate = note.CreatedDate,
                            UserId = note.UserId,
                            AddedBy = user.FirstName.Trim() + " " + user.LastName.Trim()
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if (DateTime.TryParse(searchText, out dateValue))
                {
                    query =
                        query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                }
                else
                {
                    query = query.Where(c => c.Note.Contains(searchText) || c.AddedBy.Contains(searchText));
                }
            }

            sortcolumn = string.IsNullOrWhiteSpace(sortcolumn) ? "CreatedDate" : sortcolumn;

            if (sortdirection == "asc")
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn);
            else
                query = query.AsQueryable<ApplicationNoteViewMode>().OrderBy(sortcolumn + " descending");

            return query.ToList();
        }

        public void UpdateApplicatioNoteWithVendorId(int vendorId, int applicationId)
        {
            var dbContext = context as ApplicationBoundedContext;
            var applicationNotes = dbContext.ApplicationNotes.Where(c => c.ApplicationId == applicationId).ToList();
            applicationNotes.ForEach(c =>
            {
                c.VendorId = vendorId;
            });
            dbContext.SaveChanges();
        }

        public void UpdateApplicatioNoteWithCustomerId(int customerId, int applicationId)
        {
            var dbContext = context as ApplicationBoundedContext;
            var applicationNotes = dbContext.ApplicationNotes.Where(c => c.ApplicationId == applicationId).ToList();
            applicationNotes.ForEach(c =>
            {
                c.CustomerId = customerId;
            });
            dbContext.SaveChanges();
        }

        public void UpdateApplicatioExpireDate(int applicationId, int days)
        {
            var obj = entityStore.FindById(applicationId);
            obj.ExpireDate = DateTime.UtcNow.AddDays(days);
            entityStore.Update(obj, c => c.ExpireDate);
        }
    }
}
