﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.Claims;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using CL.Framework.Common;
using System.Data.Entity;
using System.Net.Mail;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.FinancialIntegration;
using CL.TMS.Caching;

namespace CL.TMS.Repository.System
{
    public class GpRepository : BaseRepository<GpBoundedContext, GpiBatch, int>, IGpRepository
    {
        #region Public Methods
        public void AddGpBatch(GpiBatch gpiBatch)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.GpiBatches.Add(gpiBatch);
            gpContext.SaveChanges();
        }
        public void AddGpBatchEntry(GpiBatchEntry gpiBatchEntry)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.GpiBatchEntries.Add(gpiBatchEntry);
            gpContext.SaveChanges();
        }
        public void AddGpBatchEntries(List<GpiBatchEntry> gpiBatchEntries)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.GpiBatchEntries.AddRange(gpiBatchEntries);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsCustomers(List<RrOtsRmCustomer> customers)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmCustomers.AddRange(customers);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsCustomer(RrOtsRmCustomer customer)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmCustomers.Add(customer);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsVendor(RrOtsPmVendor vendor)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmVendors.Add(vendor);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsVendors(List<RrOtsPmVendor> vendors)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmVendors.AddRange(vendors);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmCashReceipts(List<RrOtsRmCashReceipt> cashReceipts)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmCashReceipts.AddRange(cashReceipts);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmApplies(List<RrOtsRmApply> rrOtsRmApplies)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmApplies.AddRange(rrOtsRmApplies);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmTransactions(List<RrOtsRmTransaction> rrOtsRmTransactions)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmTransactions.AddRange(rrOtsRmTransactions);
            gpContext.SaveChanges();
        }
        public void AddRrOtsRmTransDistributions(List<RrOtsRmTransDistribution> rrOtsRmTransDistributions)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsRmTransDistributions.AddRange(rrOtsRmTransDistributions);
            gpContext.SaveChanges();
        }
        public void AddRrOtsPmTransDistribution(RrOtsPmTransDistribution rrOtsPmTransDistribution)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmTransDistributions.Add(rrOtsPmTransDistribution);
            gpContext.SaveChanges();
        }
        public void AddRrOtsPmTransDistributions(List<RrOtsPmTransDistribution> rrOtsPmTransDistributions)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmTransDistributions.AddRange(rrOtsPmTransDistributions);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsPmTransactions(List<RrOtsPmTransaction> rrOtsPmTransactions)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmTransactions.AddRange(rrOtsPmTransactions);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsPmTransDistributions(List<RrOtsPmTransDistribution> rrOtsPmTransDistributions)
        {
            var gpContext = context as GpBoundedContext;
            gpContext.RrOtsPmTransDistributions.AddRange(rrOtsPmTransDistributions);
            gpContext.SaveChanges();
        }
        public List<GpFiscalYear> GetAllGpFiscalYears()
        {
            return (context as GpBoundedContext).GpFiscalYears.ToList();        
        }
        public List<GpClaimDto> GetClaimsByGpBatchId(int gpiBatchId)
        {
            var gpContext = context as GpBoundedContext;
            var query = from c in gpContext.Claims
                join e in gpContext.GpiBatchEntries on c.ID.ToString() equals e.GpiBatchEntryDataKey
                //join b in gpContext.GpiBatches on e.GpiBatchID equals b.ID
                //join v in gpContext.Vendors on c.ParticipantId equals v.ID
                //where b.ID == gpiBatchId
                where e.GpiBatchID == gpiBatchId
                select new GpClaimDto()
                {
                    ClaimId = c.ID,
                    //RegistrationNumber = v.Number,
                    RegistrationNumber = c.Participant.Number,
                    Period = c.ClaimPeriod,
                    ParticipantId = c.ParticipantId,
                    TotalClaimAmount = Math.Round(c.ClaimsAmountTotal.Value,2),
                    //GpiBatch = b,
                    GpiBatchId = gpiBatchId,
                    //GpiBatchEntry = e,
                    GpiBatchEntryId = e.ID,
                    IsTaxApplicable = c.IsTaxApplicable //OTSTM2-1294
                };
            return query.ToList();
        }
        public List<GpTsfClaimDto> GetTsfClaimsCheckBookByGpBatchId(int gpiBatchId)
        {
            var gpContext = context as GpBoundedContext;
            var query = from c in gpContext.TSFClaims
                        join e in gpContext.GpiBatchEntries on c.ID.ToString() equals e.GpiBatchEntryDataKey
                        join b in gpContext.GpiBatches on e.GpiBatchID equals b.ID
                        join cb in gpContext.GpChequeBooks on c.Account equals cb.account_number into gj
                        from subCheckBook in gj.DefaultIfEmpty()
                        where c.PaymentAmount > 0 && b.ID == gpiBatchId
                        select new GpTsfClaimDto()
                        {
                            ClaimId = c.ID,
                            RegistrationNumber = c.Customer.RegistrationNumber,
                            GpCheckBookName = subCheckBook.name ?? string.Empty,
                            Period = c.Period,
                            GpiBatch = b,
                            GpiBatchEntry = e,
                            PaymentAmount = c.PaymentAmount ?? 0,
                            DepositDate = c.DepositDate,
                            ReceiptDate = c.ReceiptDate,
                            CurrencyType = c.CurrencyType,
                        };
            return query.ToList();
        }
        public List<GpTsfClaimDto> GetTsfClaimsRegistrantByGpBatchId(int gpiBatchId)
        {
            var gpContext = context as GpBoundedContext;
            var query = from c in gpContext.TSFClaims
                        join e in gpContext.GpiBatchEntries on c.ID.ToString() equals e.GpiBatchEntryDataKey
                        join b in gpContext.GpiBatches on e.GpiBatchID equals b.ID
                        join r in gpContext.Customers on c.CustomerID.ToString() equals r.RegistrationNumber into rc
                        from subCustomer in rc.DefaultIfEmpty()
                        where (c.TotalRemittancePayable > 0 || c.Credit > 0 || c.AdjustmentTotal != 0) && b.ID == gpiBatchId
                        select new GpTsfClaimDto()
                        {
                            ClaimId = c.ID,
                            RegistrationNumber = c.Customer.RegistrationNumber,
                            Period = c.Period,
                            GpiBatch = b,
                            GpiBatchEntry = e,
                            DepositDate = c.DepositDate,
                            ReceiptDate = c.ReceiptDate,
                            IsTaxExempt = subCustomer.IsTaxExempt,
                            ApplicableTaxesHst = c.ApplicableTaxesHst,
                            TotalTsfDue = Math.Round(c.TotalTSFDue, 2),
                            Credit = Math.Round(c.Credit ?? 0, 2),
                            Penalties = Math.Round( c.PenaltiesManually ?? c.Penalties, 2),
                            Interest = c.Interest,
                            TotalRemittancePayable = Math.Round(c.TotalRemittancePayable, 2),
                            InternalPaymentAdjustment = Math.Round(c.AdjustmentTotal ?? 0, 2) //OTSTM2-652
                        };
            return query.ToList();
        }
        public IQueryable<GpChequeBook> GetAllGpCheckBooks()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.GpChequeBooks;
        }
        public IQueryable<GpiAccount> GetAllGpiAccounts()
        {
            return (context as GpBoundedContext).GpiAccounts;
        }
        public GpiAccount GetP2GpiAccountByAccountNumber(string accountNumber)
        {
            return GetAllGpiAccounts().Where(g => g.account_number == accountNumber && g.registrant_type_ind == "P").Join(
                GetAllGpiAccounts().Where(g => g.registrant_type_ind == "PT"),
                g1 => g1.tire_type_id,
                g2 => g2.tire_type_id,
                (g1, g2) => new {g2 = g2}
                ).Select(i =>i.g2).First();
        }
        public IQueryable<GpiBatch> GetAllGpBatches()
        {
            return entityStore.All;
        }
        public IQueryable<GpiBatch> GetGpBatchesByFilter(string gpiTypeId = "")
        {         
            return GetAllGpBatches().Where(b => (b.GpiTypeID == gpiTypeId || string.IsNullOrEmpty(gpiTypeId)));
        }
        public IQueryable<BankInformation> GetAllBankingInformation()
        {
            return (context as GpBoundedContext).BankInformations;
        }
        public GpiBatch GetGpBatch(int gpiBatchId)
        {
            return entityStore.FindById(gpiBatchId);
        }
        public GpiBatchEntry GetGpBatchEntry(int gpiBatchEntryId)
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.GpiBatchEntries.First(e => e.ID == gpiBatchEntryId);
        }
        public List<GpiBatchEntry> GetGpBatchEntriesByBatchId(int gpiBatchId)
        {
            return GetGpBatch(gpiBatchId).GpiBatchEntries.ToList();
        }
        public IQueryable<RrOtsRmCustomer> GetAllRrOtsRmCustomers()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsRmCustomers;
        }
        public IQueryable<RrOtsPmVendor> GetAllRrOtsPmVendors()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsPmVendors;
        }
        public List<RrOtsRmCustomer> GetRrOtsRmCustomersByBatchId(int gpiBatchId)
        {
            var gpBatchEntryIds = GetGpBatchEntriesByBatchId(gpiBatchId).Select(e => e.ID).ToList();
            return GetAllRrOtsRmCustomers().Where(v => gpBatchEntryIds.Contains((int)v.gpibatchentry_id)).ToList();
        }
        public List<RrOtsRmApply> GetRrOtsRmApplyByBatchId(int gpiBatchId)
        {
            return GetAllRrOtsRmApplies().Where(a => a.USERDEF1 == gpiBatchId.ToString()).ToList();
        }
        public List<RrOtsRmCashReceipt> GetRrOtsRmCashReceiptByBatchId(int gpiBatchId)
        {
            var gpBatchEntryIds = GetGpBatchEntriesByBatchId(gpiBatchId).Select(e => e.ID).ToList();
            return GetAllRrOtsRmCashReceipt().Where(c => gpBatchEntryIds.Contains((int) c.gpibatchentry_id)).ToList();
        }
        public List<RrOtsRmTransDistribution> GetRrOtsRmTransDistributionByBatchId(int gpiBatchId)
        {
            return GetAllRrOtsRmTransDistributions().Where(t => t.USERDEF1 == gpiBatchId.ToString()).OrderBy(i => i.DEX_ROW_ID).ToList();
        }
        public List<RrOtsPmTransDistribution> GetRrOtsPmTransDistributionByBatchId(int gpiBatchId)
        {
            return GetAllRrOtsPmTransDistributions().Where(t => t.USERDEF1 == gpiBatchId.ToString()).ToList();
        }
        public List<RrOtsRmTransaction> GetRrOtsRmTransactionByBatchId(int gpiBatchId)
        {
            return GetAllRrOtsRmTransactions().Where(t => t.USERDEF1 == gpiBatchId.ToString()).OrderBy(i => i.DEX_ROW_ID).ToList();
        }
        public List<RrOtsPmVendor> GetRrOtsPmVendorsByBatchId(int gpiBatchId)
        {
            var gpBatchEntryIds = GetGpBatchEntriesByBatchId(gpiBatchId).Select(e => e.ID).ToList();
            return GetAllRrOtsPmVendors().Where(v => gpBatchEntryIds.Contains((int) v.gpibatchentry_id)).ToList();
        }
        public IQueryable<RrOtsPmTransaction> GetAllRrOtsPmTransactions()
        {
            return (context as GpBoundedContext).RrOtsPmTransactions;
        }
        public List<RrOtsPmTransaction> GetRrOtsPmTransactionByBatchId(int gpiBatchId)
        {
            return GetAllRrOtsPmTransactions().Where(i => i.USERDEF1 == gpiBatchId.ToString()).ToList();
        }

        public void UpdateGpBatch(GpiBatch gpBatch)
        {
            var gpContext = context as GpBoundedContext;
            var row = gpContext.GpiBatches.FirstOrDefault(e => e.ID == gpBatch.ID);
            if (row != null)
            {
                //TODO map other fields
                row.GpiStatusId = gpBatch.GpiStatusId;
                row.LastUpdatedDate = gpBatch.LastUpdatedDate;
                row.LastUpdatedBy = gpBatch.LastUpdatedBy;
                gpContext.SaveChanges();
            }
        }
        public void UpdateGpBatchEntry(GpiBatchEntry gpiBatchEntry)
        {
            var gpContext = context as GpBoundedContext;
            var row = gpContext.GpiBatchEntries.FirstOrDefault(e => e.ID == gpiBatchEntry.ID);
            if (row != null)
            {
                //TODO map other fields
                row.LastUpdatedDate = gpiBatchEntry.LastUpdatedDate;
                row.LastUpdatedBy = gpiBatchEntry.LastUpdatedBy;
                row.GpiStatusID = gpiBatchEntry.GpiStatusID;
                row.Message = gpiBatchEntry.Message;
                gpContext.SaveChanges();
            }
        }

        public void UpdateGpBatchEntries(List<int> gpiBatchEntryIds, string status, string currentUser, string message)
        {
            var gpContext = context as GpBoundedContext;
            var query = gpContext.GpiBatchEntries.Where(c => gpiBatchEntryIds.Contains(c.ID));
            query.ToList().ForEach(c =>
            {
                c.LastUpdatedBy = currentUser;
                c.LastUpdatedDate = DateTime.Now;
                c.GpiStatusID = status;
                c.Message = message;
            });
             gpContext.SaveChanges();
        }

        public void UpdateGpBatchEntries(List<GpiBatchEntry> gpiBatchEntries)
        {
            var gpContext = context as GpBoundedContext;
            var batchEntryIds = gpiBatchEntries.Select(e => e.ID);

            foreach (var row in gpContext.GpiBatchEntries.Where(e => batchEntryIds.Contains(e.ID)))
            {
                var entry = gpiBatchEntries.Single(e => e.ID == row.ID);
                row.LastUpdatedDate = entry.LastUpdatedDate;
                row.LastUpdatedBy = entry.LastUpdatedBy;
                row.GpiStatusID = entry.GpiStatusID;
                row.Message = entry.Message;
            }
            gpContext.SaveChanges();

        }
        public void UpdateRrOtsRmCustomer(RrOtsRmCustomer customer)
        {
            var gpContext = context as GpBoundedContext;
            var row = gpContext.RrOtsRmCustomers.FirstOrDefault(c => c.CUSTNMBR == customer.CUSTNMBR && c.ADRSCODE == customer.ADRSCODE && c.USERDEF1 == customer.USERDEF1 && c.INTERID == customer.INTERID);
            if (row != null)
            {
                //TODO map other fields
                row.INTSTATUS = customer.INTSTATUS;
                row.INTDATE = customer.INTDATE;
                row.ERRORCODE = customer.ERRORCODE;

                gpContext.SaveChanges();
            }
        }
        public void UpdateRrOtsPmVendor(RrOtsPmVendor vendor)
        {

            var gpContext = context as GpBoundedContext;
            var row = gpContext.RrOtsPmVendors.FirstOrDefault(v => v.VENDORID == vendor.VENDORID && v.VADDCDPR == vendor.VADDCDPR && v.USERDEF1 == vendor.USERDEF1 && v.INTERID == vendor.INTERID);
            if (row != null)
            {
                //TODO map other fields
                row.INTSTATUS = vendor.INTSTATUS;
                row.INTDATE = vendor.INTDATE;
                row.ERRORCODE = vendor.ERRORCODE;
                gpContext.SaveChanges();
            }
        }

        public void UpdateRrOtsPmTransaction(string batchId)
        {
            var gpContext = context as GpBoundedContext;
            var transactions = gpContext.RrOtsPmTransactions.Where(c => c.USERDEF1 == batchId).ToList();
            transactions.ForEach(c => c.PSTGDATE = DateTime.Now);
            gpContext.SaveChanges();
        }

        public void DeleteGpEntry(GpiBatchEntry gpiBatchEntry)
        {
            var gpContext = context as GpBoundedContext;
            var row = gpContext.GpiBatchEntries.FirstOrDefault(e => e.ID == gpiBatchEntry.ID);
            gpContext.GpiBatchEntries.Remove(row);

            foreach (var vendor in gpContext.RrOtsPmVendors.Where(v => v.gpibatchentry_id == gpiBatchEntry.ID))
            {
                gpContext.RrOtsPmVendors.Remove(vendor);
            }
            foreach (var customer in gpContext.RrOtsRmCustomers.Where(c => c.gpibatchentry_id == gpiBatchEntry.ID))
            {
                gpContext.RrOtsRmCustomers.Remove(customer);
            }
            foreach (var cashReceipt in gpContext.RrOtsRmCashReceipts.Where(c => c.gpibatchentry_id == gpiBatchEntry.ID))
            {
                gpContext.RrOtsRmCashReceipts.Remove(cashReceipt);
            }
            var transDistributionRmRemoves = new List<RrOtsRmTransDistribution>();
            var rrOtsRmApplyRemoves = new List<RrOtsRmApply>();
            var rrOtsRmTransactionRemoves = new List<RrOtsRmTransaction>();
            foreach (var transactionRM in gpContext.RrOtsRmTransactions.Where(t => t.gpibatchentry_id == gpiBatchEntry.ID).ToList())
            {
                var transDistributionRmTmpRemoves = gpContext.RrOtsRmTransDistributions.Where(t => t.USERDEF1 == gpiBatchEntry.GpiBatchID.ToString() && t.DOCNUMBR == transactionRM.DOCNUMBR.ToString());
                var rrOtsRmApplyTmpRemoves = gpContext.RrOtsRmApplies.Where(t => t.USERDEF1 == gpiBatchEntry.GpiBatchID.ToString() && t.APTODCNM == transactionRM.DOCNUMBR);
                transDistributionRmRemoves.AddRange(transDistributionRmTmpRemoves);
                rrOtsRmApplyRemoves.AddRange(rrOtsRmApplyTmpRemoves);
                rrOtsRmTransactionRemoves.Add(transactionRM);
            }
            gpContext.RrOtsRmTransDistributions.RemoveRange(transDistributionRmRemoves);
            gpContext.RrOtsRmApplies.RemoveRange(rrOtsRmApplyRemoves);
            gpContext.RrOtsRmTransactions.RemoveRange(rrOtsRmTransactionRemoves);

            var transDistributionPmRemoves = new List<RrOtsPmTransDistribution>();
            var rrOtsPmTransactionRemoves = new List<RrOtsPmTransaction>();
            foreach (var transactionPM in gpContext.RrOtsPmTransactions.Where(t => t.GpiBatchEntryId == gpiBatchEntry.ID).ToList())
            {
                var transDistributionPmTmpRemoves = gpContext.RrOtsPmTransDistributions.Where(t => t.USERDEF1 == gpiBatchEntry.GpiBatchID.ToString() && t.VCHNUMWK == transactionPM.VCHNUMWK);
                transDistributionPmRemoves.AddRange(transDistributionPmTmpRemoves);
                rrOtsPmTransactionRemoves.Add(transactionPM);
            }
            gpContext.RrOtsPmTransDistributions.RemoveRange(transDistributionPmRemoves);
            gpContext.RrOtsPmTransactions.RemoveRange(rrOtsPmTransactionRemoves);

            var gpiBatch = gpContext.GpiBatches.FirstOrDefault(b => b.ID == gpiBatchEntry.GpiBatchID);
            gpiBatch.GpiBatchCount -= 1;
            if (gpiBatch.GpiBatchCount == 0)
            {
                gpContext.GpiBatches.Remove(gpiBatch);
            }
            gpContext.SaveChanges();
        }
        public PaginationDTO<GPListViewModel, int> GetAllGpBatchesPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string type, Dictionary<string, string> GpiStatus)
        {
            var query = this.GetAllGpBatchesList(searchText, orderBy, sortDirection, type, GpiStatus);

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<GPListViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        public PaginationDTO<GPModalListViewModal, int> GetGpTransactionsByBatchIdPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int batchId, Dictionary<string, string> GpiStatus)
        {
            var query = GetGpTransactionsByBatchIdList(searchText, orderBy, sortDirection, batchId, GpiStatus);

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<GPModalListViewModal, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        public List<GPListViewModel> GetAllGpBatchesList(string searchText, string orderBy, string sortDirection, string type, Dictionary<string, string> GpiStatus)
        {
            using (var dbContext = new GpBoundedContext())
            {
                var query = dbContext.GpiBatches.Where(c => c.GpiTypeID == type).Select(gpiBatch => new GPListViewModel()
                {
                    BatchNum = gpiBatch.ID,
                    Count = gpiBatch.GpiBatchCount,
                    GpiStatusId = gpiBatch.GpiStatusId,
                    Modified = gpiBatch.LastUpdatedDate,
                    Message = (gpiBatch.Message == null ? string.Empty : gpiBatch.Message)
                });

                var newList = new List<GPListViewModel>();
                query.ToList().ForEach(c =>
                {
                    var keyValuePair = GpiStatus.Single(x => x.Key == c.GpiStatusId);
                    c.Status = (keyValuePair.Value == null ? string.Empty : keyValuePair.Value);
                    newList.Add(c);
                });

                query = newList.AsQueryable();

                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = query.Where(a => a.Status.ToLower().Contains(searchText.ToLower())
                                            || a.Message.ToLower().Contains(searchText.ToLower())
                                            || a.BatchNum.ToString().Contains(searchText)
                                            || a.Count.ToString().Contains(searchText)
                                            || a.Modified.ToString(TreadMarksConstants.DateFormat).Contains(searchText));
                }

                orderBy = string.IsNullOrWhiteSpace(orderBy) ? "BatchNum" : orderBy;

                #region order
                switch (orderBy)
                {
                    case "BatchNum":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.BatchNum);
                        else
                            query = query.OrderByDescending(a => a.BatchNum);
                        break;
                    case "Count":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Count);
                        else
                            query = query.OrderByDescending(a => a.Count);
                        break;
                    case "Modified":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Modified);
                        else
                            query = query.OrderByDescending(a => a.Modified);
                        break;
                    case "Status":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Status);
                        else
                            query = query.OrderByDescending(a => a.Status);
                        break;
                    case "Message":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Message);
                        else
                            query = query.OrderByDescending(a => a.Message);
                        break;
                }
                #endregion

                return query.ToList();
            }
        }
        public List<GPModalListViewModal> GetGpTransactionsByBatchIdList(string searchText, string orderBy, string sortDirection, int batchId, Dictionary<string, string> GpiStatus)
        {
            using (var dbContext = new GpBoundedContext())
            {
                var query = dbContext.GpiBatchEntries.Where(c => c.GpiBatchID == batchId).Select(gpiBatchEntry => new GPModalListViewModal()
                {
                    GPTransactionNum = (gpiBatchEntry.GpiTxnNumber == null ? string.Empty : gpiBatchEntry.GpiTxnNumber),
                    GpiStatusId = (gpiBatchEntry.GpiStatusID == null ? string.Empty : gpiBatchEntry.GpiStatusID),
                    GpiBatchEntryId = (gpiBatchEntry.ID.ToString() == null ? string.Empty : gpiBatchEntry.ID.ToString()),
                    Message = (gpiBatchEntry.Message == null ? string.Empty : gpiBatchEntry.Message)
                });

                var newList = new List<GPModalListViewModal>();
                query.ToList().ForEach(c =>
                {
                    var keyValuePair = GpiStatus.Single(x => x.Key == c.GpiStatusId);
                    c.Status = (keyValuePair.Value == null ? string.Empty : keyValuePair.Value);
                    newList.Add(c);
                });

                query = newList.AsQueryable();

                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = query.Where(a => a.Status.ToLower().Contains(searchText.ToLower())
                                            || a.Message.ToLower().Contains(searchText.ToLower())                                            
                                            || a.GPTransactionNum.Contains(searchText));
                }

                orderBy = string.IsNullOrWhiteSpace(orderBy) ? "GPTransactionNum" : orderBy;

                #region orderBy
                switch (orderBy)
                {
                    case "GPTransactionNum":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.GPTransactionNum);
                        else
                            query = query.OrderByDescending(a => a.GPTransactionNum);
                        break;
                    case "Status":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Status);
                        else
                            query = query.OrderByDescending(a => a.Status);
                        break;
                    case "Message":
                        if (sortDirection == "asc")
                            query = query.OrderBy(a => a.Message);
                        else
                            query = query.OrderByDescending(a => a.Message);
                        break;
                }
                #endregion

                return query.ToList();
            }
        }

        public IQueryable<FIAccount> GetAdminFinancialIntegration()
        {
            return (context as GpBoundedContext).FIAccounts;
        }
        public bool FIUpdateData(List<FinancialIntegrationVM> vm, long? userID)
        {
            bool result = false;
            var context=new GpBoundedContext();
            foreach (var item in vm) {
                var record = context.FIAccounts.Where(x => x.ID == item.ID).FirstOrDefault();
                if (record != null && record.AccountNumber != item.AccountNumber)
                {
                    record.AccountNumber = item.AccountNumber;
                    record.ModifiedDate = DateTime.Now;
                    record.ModifiedByID = userID;
                    result = true;
                }               
            }
            if (result)
            {
                context.SaveChanges();
                //OTSTM2-1124 Refresh Cache
                CachingHelper.RemoveItem(CachKeyManager.FIAccountKey); 
            }
            return result;
        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gpiBatchId"></param>
        /// <returns>a list of rr_ots_rm_trans_distributions </returns>



        #region Private Methods
        private IQueryable<TSFClaim> GetAllTsfClaims()
        {
            return (context as GpBoundedContext).TSFClaims;
        }
        private IQueryable<GpiBatchEntry> GetAllGpiBatchEntries()
        {
            return (context as GpBoundedContext).GpiBatchEntries;
        }
        private IQueryable<Rate> GetAllRates()
        {
            return (context as GpBoundedContext).Rates;
        }
        private IQueryable<Item> GetAllItems()
        {
            return (context as GpBoundedContext).Items;
        }
        private IQueryable<GpiAccount> GpAllGpiAccounts()
        {
            return (context as GpBoundedContext).GpiAccounts;
        }

        //OTSTM2-1124
        private IQueryable<FIAccount> GpAllFIAccounts()
        {
            return (context as GpBoundedContext).FIAccounts;
        }
        private IQueryable<TSFClaimDetail> GetAllTsfClaimDetails()
        {
            return (context as GpBoundedContext).TSFClaimDetails;
        }
        private IQueryable<RrOtsRmTransDistribution> GetAllRrOtsRmTransDistributions()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsRmTransDistributions;
        }
        private IQueryable<RrOtsPmTransDistribution> GetAllRrOtsPmTransDistributions()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsPmTransDistributions;
        }
        public List<GpRrOtsRmTransDistributionsDto> GetRrOtsRmTransDistributionsForRemittance(int gpiBatchId)
        {
            var gpContext = context as GpBoundedContext;
            var creditNote = 7;
            var distributionTypeIdCredit = 19;

            //OTSTM2-1124 comment out
            //var query = GetAllTsfClaims().Join(GetAllGpiBatchEntries(),
            //    c => c.ID.ToString(),
            //    be => be.GpiBatchEntryDataKey,
            //    (c, be) => new
            //    {
            //        summary = c,
            //        gpiBatchEntry = be
            //    }).SelectMany(ga => gpContext.GpiAccounts,
            //        (a, b) => new
            //        {
            //            summary = a.summary,
            //            gpiBatchEntry = a.gpiBatchEntry,
            //            gpiAccount = b
            //        }).Where(i => i.gpiBatchEntry.GpiBatchID == gpiBatchId && i.gpiAccount.registrant_type_ind == "S").ToList();

            //OTSTM2-1124
            var query = GetAllTsfClaims().Join(GetAllGpiBatchEntries(),
                c => c.ID.ToString(),
                be => be.GpiBatchEntryDataKey,
                (c, be) => new
                {
                    summary = c,
                    gpiBatchEntry = be
                }).SelectMany(ga => gpContext.FIAccounts,
                    (a, b) => new
                    {
                        summary = a.summary,
                        gpiBatchEntry = a.gpiBatchEntry,
                        fiAccount = b
                    }).Where(i => i.gpiBatchEntry.GpiBatchID == gpiBatchId && i.fiAccount.AdminFICategoryID == 1).ToList();

            //OTSTM2-1124 comment out
            //var result1Debit = query.Where(r => r.gpiAccount.account_number == "1220-00-00-00" && r.gpiAccount.distribution_type_id == 3 && (r.summary.TotalTSFDue > 0 || r.summary.AdjustmentTotal > 0) && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = Math.Round(r.summary.TotalTSFDue + (decimal) (r.summary.AdjustmentTotal.HasValue ? (r.summary.AdjustmentTotal > 0 ? r.summary.AdjustmentTotal : 0) : 0), 2, MidpointRounding.AwayFromZero),
            //        CRDTAMNT = 0,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var result1Debit = query.Where(r => r.fiAccount.Name == "Stewards Receivable" && r.fiAccount.distribution_type_id == 3 && (r.summary.TotalTSFDue > 0 || r.summary.AdjustmentTotal > 0) && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = 1,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = Math.Round(r.summary.TotalTSFDue + (decimal)(r.summary.AdjustmentTotal.HasValue ? (r.summary.AdjustmentTotal > 0 ? r.summary.AdjustmentTotal : 0) : 0), 2, MidpointRounding.AwayFromZero),
                    CRDTAMNT = 0,
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var result1Credit = query.Where(r => r.gpiAccount.account_number == "1220-00-00-00" && r.gpiAccount.distribution_type_id == 3 && (r.summary.Credit > 0 || r.summary.AdjustmentTotal < 0) && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = (short)creditNote,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = 0,
            //        CRDTAMNT = Math.Round((r.summary.Credit ?? 0) + Math.Abs((decimal)(r.summary.AdjustmentTotal.HasValue ? (r.summary.AdjustmentTotal < 0 ? r.summary.AdjustmentTotal : 0) : 0)), 2, MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var result1Credit = query.Where(r => r.fiAccount.Name == "Stewards Receivable" && r.fiAccount.distribution_type_id == 3 && (r.summary.Credit > 0 || r.summary.AdjustmentTotal < 0) && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
               .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
               {
                   RMDTYPAL = (short)creditNote,
                   DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
                   CUSTNMBR = r.summary.RegistrationNumber,
                   DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                   ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = 0,
                   CRDTAMNT = Math.Round((r.summary.Credit ?? 0) + Math.Abs((decimal)(r.summary.AdjustmentTotal.HasValue ? (r.summary.AdjustmentTotal < 0 ? r.summary.AdjustmentTotal : 0) : 0)), 2, MidpointRounding.AwayFromZero),
                   PAYMENTTYPE = r.summary.PaymentType
               });

            //OTSTM2-652 begin

            //OTSTM2-1124 comment out
            //var resultPositiveAdjustmentOverall = query.Where(r => r.gpiAccount.account_number == "4005-90-00-99" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = 0,
            //        CRDTAMNT = Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t=>t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t=>t.AdjustmentAmount),2,MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var resultPositiveAdjustmentOverall = query.Where(r => r.fiAccount.Name == "Internal Payment Adjustments" && r.fiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = 1,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = 0,
                    CRDTAMNT = Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero),
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var resultPositiveAdjustmentAssessment = query.Where(r => r.gpiAccount.account_number == "4004-90-00-99" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = 0,
            //        CRDTAMNT = Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var resultPositiveAdjustmentAssessment = query.Where(r => r.fiAccount.Name == "Audit Payment Adjustments" && r.fiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = 1,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = 0,
                    CRDTAMNT = Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero),
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //commented out before OTSTM2-1124
            //var resultPositiveAdjustmentPenalty = query.Where(r => r.gpiAccount.account_number == "4065-90-00-00" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = 0,
            //        CRDTAMNT = Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124 comment out
            //var resultNegativeAdjustmentOverall = query.Where(r => r.gpiAccount.account_number == "4005-90-00-99" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = (short)creditNote,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(distributionTypeIdCredit),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
            //        CRDTAMNT = 0,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var resultNegativeAdjustmentOverall = query.Where(r => r.fiAccount.Name == "Internal Payment Adjustments" && r.fiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = (short)creditNote,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(distributionTypeIdCredit),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.Overall && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
                    CRDTAMNT = 0,
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var resultNegativeAdjustmentAssessment = query.Where(r => r.gpiAccount.account_number == "4004-90-00-99" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = (short)creditNote,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(distributionTypeIdCredit),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
            //        CRDTAMNT = 0,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var resultNegativeAdjustmentAssessment = query.Where(r => r.fiAccount.Name == "Audit Payment Adjustments" && r.fiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = (short)creditNote,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(distributionTypeIdCredit),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditAssessment && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
                    CRDTAMNT = 0,
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var resultNegativeAdjustmentPenalty = query.Where(r => r.gpiAccount.account_number == "4065-90-00-00" && r.gpiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            //r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = (short)creditNote,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(distributionTypeIdCredit),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
            //        CRDTAMNT = 0,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var resultNegativeAdjustmentPenalty = query.Where(r => r.fiAccount.Name == "Audit Penalties / TSF Penalty" && r.fiAccount.distribution_type_id == 9 && r.gpiBatchEntry.GpiBatchID == gpiBatchId &&
            r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = (short)creditNote,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(distributionTypeIdCredit),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    DEBITAMT = Math.Abs(Math.Round(r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount < 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount), 2, MidpointRounding.AwayFromZero)),
                    CRDTAMNT = 0,
                    PAYMENTTYPE = r.summary.PaymentType
                });
            //OTSTM2-652 end

            //OTSTM2-1124 comment out, because this account number is not used in batch, double checked QA DB, there is no record with this account number in RrOtsRmTransDistribution Table
            //var result2 = query.Where(r => r.gpiAccount.account_number == "2170-00-00-00" && r.summary.TotalTSFDue > 0 && r.summary.ApplicableTaxesGst > 0 && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = 0,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        CRDTAMNT = Math.Round(r.summary.ApplicableTaxesGst, 2,MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });            

            //OTSTM2-1124 comment out
            //var result3 = query.Where(r => r.gpiAccount.account_number == "2200-00-00-00" && r.summary.TotalTSFDue > 0 && r.summary.ApplicableTaxesHst > 0 && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = 0,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        CRDTAMNT = Math.Round(r.summary.ApplicableTaxesHst, 2, MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var result3 = query.Where(r => r.fiAccount.Name == "Tax Receivable" && r.summary.TotalTSFDue > 0 && r.summary.ApplicableTaxesHst > 0 && r.gpiBatchEntry.GpiBatchID == gpiBatchId)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = 1,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    DEBITAMT = 0,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    CRDTAMNT = Math.Round(r.summary.ApplicableTaxesHst, 2, MidpointRounding.AwayFromZero),
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var result4Debit = GetAllTsfClaims().Join(gpContext.TSFClaimDetails,
            //    s => s.ID,
            //    tcd => tcd.TSFClaimID,
            //    (s, tcd) => new
            //    {
            //        summary = s,
            //        tcd = tcd
            //    }).Join(GetAllGpiBatchEntries(),
            //        stcd => stcd.summary.ID.ToString(),
            //        be => be.GpiBatchEntryDataKey,
            //        (stcd, be) => new
            //        {
            //            stcd = stcd,
            //            be = be
            //        }).Where(a => a.stcd.tcd.TSFDue > 0)
            //    .Join(GetAllItems().Where(i => i.ItemCategory == 4),
            //        stcdbe => stcdbe.stcd.tcd.ItemID,
            //        i => i.ID,
            //        (stcdbe, i) => new
            //        {
            //            summary = stcdbe.stcd.summary,
            //            tcd = stcdbe.stcd.tcd,
            //            b = stcdbe.be,
            //            item = i
            //        })
            //    .Where(b => b.item.EffectiveStartDate <= b.summary.Period.StartDate && b.summary.Period.StartDate <= b.item.EffectiveEndDate)
            //    .Join(GpAllGpiAccounts(),
            //    i => i.item.ShortName.ToString(),
            //    ga => ga.ShortName.ToString(),
            //    (i, ga) => new
            //    {
            //        summary = i.summary,
            //        TSFClaimDetail = i.tcd,
            //        batchEntry = i.b,
            //        gpiAccount = ga
            //    }).Where(b => b.batchEntry.GpiBatchID == gpiBatchId && b.gpiAccount.registrant_type_ind == "S")
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = 0,
            //        CRDTAMNT = r.TSFClaimDetail.TSFDue,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var result4Debit = GetAllTsfClaims().Join(gpContext.TSFClaimDetails,
                s => s.ID,
                tcd => tcd.TSFClaimID,
                (s, tcd) => new
                {
                    summary = s,
                    tcd = tcd
                }).Join(GetAllGpiBatchEntries(),
                    stcd => stcd.summary.ID.ToString(),
                    be => be.GpiBatchEntryDataKey,
                    (stcd, be) => new
                    {
                        stcd = stcd,
                        be = be
                    }).Where(a => a.stcd.tcd.TSFDue > 0)
                .Join(GetAllItems().Where(i => i.ItemCategory == 4),
                    stcdbe => stcdbe.stcd.tcd.ItemID,
                    i => i.ID,
                    (stcdbe, i) => new
                    {
                        summary = stcdbe.stcd.summary,
                        tcd = stcdbe.stcd.tcd,
                        b = stcdbe.be,
                        item = i
                    })
                .Where(b => b.item.EffectiveStartDate <= b.summary.Period.StartDate && b.summary.Period.StartDate <= b.item.EffectiveEndDate)
                .Join(GpAllFIAccounts(),
                i => i.item.ShortName.ToString(),
                ga => ga.Name.ToString(),
                (i, ga) => new
                {
                    summary = i.summary,
                    TSFClaimDetail = i.tcd,
                    batchEntry = i.b,
                    fiAccount = ga
                }).Where(b => b.batchEntry.GpiBatchID == gpiBatchId && b.fiAccount.AdminFICategoryID == 1)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = 1,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                    ACTNUMST = r.fiAccount.AccountNumber,
                    DEBITAMT = 0,
                    CRDTAMNT = r.TSFClaimDetail.TSFDue,
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var result4Credit = GetAllTsfClaims().Join(gpContext.TSFClaimDetails,
            //    s => s.ID,
            //    tcd => tcd.TSFClaimID,
            //    (s, tcd) => new
            //    {
            //        summary = s,
            //        tcd = tcd
            //    }).Join(GetAllGpiBatchEntries(),
            //        stcd => stcd.summary.ID.ToString(),
            //        be => be.GpiBatchEntryDataKey,
            //        (stcd, be) => new
            //        {
            //            stcd = stcd,
            //            be = be
            //        }).Where(a => a.stcd.tcd.CreditTSFDue > 0)
            //    .Join(GetAllItems().Where(i => i.ItemCategory == 4),
            //        stcdbe => stcdbe.stcd.tcd.ItemID,
            //        i => i.ID,
            //        (stcdbe, i) => new
            //        {
            //            summary = stcdbe.stcd.summary,
            //            tcd = stcdbe.stcd.tcd,
            //            b = stcdbe.be,
            //            item = i
            //        })
            //    .Where(b => b.item.EffectiveStartDate <= b.tcd.NegativeAdjDate && b.tcd.NegativeAdjDate <= b.item.EffectiveEndDate)
            //    .Join(GpAllGpiAccounts(),
            //    i => i.item.ShortName.ToString(),
            //    ga => ga.ShortName.ToString(),
            //    (i, ga) => new
            //    {
            //        summary = i.summary,
            //        TSFClaimDetail = i.tcd,
            //        batchEntry = i.b,
            //        gpiAccount = ga
            //    }).Where(b => b.batchEntry.GpiBatchID == gpiBatchId && b.gpiAccount.registrant_type_ind == "S")
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = (short)creditNote,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)distributionTypeIdCredit,
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = r.TSFClaimDetail.CreditTSFDue,
            //        CRDTAMNT = 0,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124
            var result4Credit = GetAllTsfClaims().Join(gpContext.TSFClaimDetails,
                s => s.ID,
                tcd => tcd.TSFClaimID,
                (s, tcd) => new
                {
                    summary = s,
                    tcd = tcd
                }).Join(GetAllGpiBatchEntries(),
                    stcd => stcd.summary.ID.ToString(),
                    be => be.GpiBatchEntryDataKey,
                    (stcd, be) => new
                    {
                        stcd = stcd,
                        be = be
                    }).Where(a => a.stcd.tcd.CreditTSFDue > 0)
                .Join(GetAllItems().Where(i => i.ItemCategory == 4),
                    stcdbe => stcdbe.stcd.tcd.ItemID,
                    i => i.ID,
                    (stcdbe, i) => new
                    {
                        summary = stcdbe.stcd.summary,
                        tcd = stcdbe.stcd.tcd,
                        b = stcdbe.be,
                        item = i
                    })
                .Where(b => b.item.EffectiveStartDate <= b.tcd.NegativeAdjDate && b.tcd.NegativeAdjDate <= b.item.EffectiveEndDate)
                .Join(GpAllFIAccounts(),
                i => i.item.ShortName.ToString(),
                ga => ga.Name.ToString(),
                (i, ga) => new
                {
                    summary = i.summary,
                    TSFClaimDetail = i.tcd,
                    batchEntry = i.b,
                    fiAccount = ga
                }).Where(b => b.batchEntry.GpiBatchID == gpiBatchId && b.fiAccount.AdminFICategoryID == 1)
                .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
                {
                    RMDTYPAL = (short)creditNote,
                    DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, creditNote),
                    CUSTNMBR = r.summary.RegistrationNumber,
                    DISTTYPE = (int)distributionTypeIdCredit,
                    ACTNUMST = r.fiAccount.AccountNumber,
                    DEBITAMT = r.TSFClaimDetail.CreditTSFDue,
                    CRDTAMNT = 0,
                    PAYMENTTYPE = r.summary.PaymentType
                });

            //OTSTM2-1124 comment out
            //var result5 = query.Where(
            //        r =>
            //            r.gpiAccount.account_number == "4065-90-00-00" && 
            //            (
            //                (r.summary.TotalTSFDue > 0 && (r.summary.PenaltiesManually ?? r.summary.Penalties) > 0) || 
            //                (r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
            //            ) &&
            //            r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = 0,
            //        //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
            //        CRDTAMNT =  Math.Round((r.summary.PenaltiesManually ?? r.summary.Penalties) + 
            //                                (r.summary.TSFClaimInternalPaymentAdjustments.Where(t => 
            //                                    t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount)
            //                                ), 2, MidpointRounding.AwayFromZero),
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });           

            //OTSTM2-1124
            var result5 = query.Where(
                   r =>
                       r.fiAccount.Name == "Audit Penalties / TSF Penalty" &&
                       (
                           (r.summary.TotalTSFDue > 0 && (r.summary.PenaltiesManually ?? r.summary.Penalties) > 0) ||
                           (r.summary.TSFClaimInternalPaymentAdjustments.Where(t => t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Count() > 0)
                       ) &&
                       r.gpiBatchEntry.GpiBatchID == gpiBatchId)
               .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
               {
                   RMDTYPAL = 1,
                   DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
                   CUSTNMBR = r.summary.RegistrationNumber,
                   DISTTYPE = (int)(r.fiAccount.distribution_type_id),
                   ACTNUMST = r.fiAccount.AccountNumber,
                   DEBITAMT = 0,
                    //need to add MidpointRounding.AwayFromZero as for some reason it is round .5 down instead of up
                    CRDTAMNT = Math.Round((r.summary.PenaltiesManually ?? r.summary.Penalties) +
                                           (r.summary.TSFClaimInternalPaymentAdjustments.Where(t =>
                                               t.PaymentType == (int)TSFInternalAdjustmentPaymentType.AuditPenalty && t.AdjustmentAmount > 0 && t.Status != TSFInternalAdjustmentStatus.Voided.ToString()).Sum(t => t.AdjustmentAmount)
                                           ), 2, MidpointRounding.AwayFromZero),
                   PAYMENTTYPE = r.summary.PaymentType
               });

            //OTSTM2-1124 comment out, because this account number is not used in batch, double checked QA DB, there is no record with this account number in RrOtsRmTransDistribution Table
            //var result6 = query.Where(
            //        r =>
            //            r.gpiAccount.account_number == "4070-90-00-00" && r.summary.Interest > 0 && r.summary.TotalTSFDue > 0 &&
            //            r.gpiBatchEntry.GpiBatchID == gpiBatchId)
            //    .AsEnumerable().Select(r => new GpRrOtsRmTransDistributionsDto()
            //    {
            //        RMDTYPAL = 1,
            //        DOCNUMBR = string.Format("{0}-{1:MM/yy}-{2}", r.summary.RegistrationNumber, r.summary.Period.EndDate, "1"),
            //        CUSTNMBR = r.summary.RegistrationNumber,
            //        DISTTYPE = (int)(r.gpiAccount.distribution_type_id),
            //        ACTNUMST = r.gpiAccount.account_number,
            //        DEBITAMT = 0,
            //        CRDTAMNT = r.summary.Interest,
            //        PAYMENTTYPE = r.summary.PaymentType
            //    });

            //OTSTM2-1124 comment out
            //var main = (((((((((((result1Debit.Concat(result1Credit)).Concat(result2)).Concat(result3)).Concat(result4Debit.Concat(result4Credit))).Concat(result5)).Concat(result6))
            //    .Concat(resultPositiveAdjustmentOverall)).Concat(resultPositiveAdjustmentAssessment))
            //    .Concat(resultNegativeAdjustmentOverall)).Concat(resultNegativeAdjustmentAssessment)).Concat(resultNegativeAdjustmentPenalty)).OrderBy(r => r.DOCNUMBR).ThenBy(r => r.ACTNUMST);

            //OTSTM2-1124
            var main = (((((((((result1Debit.Concat(result1Credit)).Concat(result3)).Concat(result4Debit.Concat(result4Credit))).Concat(result5))
                .Concat(resultPositiveAdjustmentOverall)).Concat(resultPositiveAdjustmentAssessment))
                .Concat(resultNegativeAdjustmentOverall)).Concat(resultNegativeAdjustmentAssessment)).Concat(resultNegativeAdjustmentPenalty)).OrderBy(r => r.DOCNUMBR).ThenBy(r => r.ACTNUMST);
            return main.ToList();        
        }

        public string GetTransactionNumber(int claimId)
        {
            var gpContext = context as GpBoundedContext;
            var query = from gpiBatchEntry in gpContext.GpiBatchEntries.AsNoTracking()
                        join rrOtsPmTransaction in gpContext.RrOtsPmTransactions.AsNoTracking()
                        on gpiBatchEntry.ID equals rrOtsPmTransaction.GpiBatchEntryId
                        where gpiBatchEntry.ClaimId == claimId
                        select rrOtsPmTransaction.DOCNUMBR;
            return query.FirstOrDefault();
        }

        public int GetBatchIdByTSFClaimId(int claimId)
        {
            var gpContext = context as GpBoundedContext;
            var query = from gpiBatchEntry in gpContext.GpiBatchEntries.AsNoTracking()
                        join gpiBatch  in gpContext.GpiBatches.AsNoTracking()
                        on gpiBatchEntry.GpiBatchID equals gpiBatch.ID
                        where gpiBatchEntry.GpiBatchEntryDataKey == claimId.ToString()
                        && gpiBatch.GpiTypeID == "S"
                        select gpiBatchEntry.GpiBatchID;
            return query.FirstOrDefault();
        }

        private IQueryable<RrOtsRmCashReceipt> GetAllRrOtsRmCashReceipt()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsRmCashReceipts;
        }
        private IQueryable<RrOtsRmTransaction> GetAllRrOtsRmTransactions()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsRmTransactions;
        }
        private IQueryable<RrOtsRmApply> GetAllRrOtsRmApplies()
        {
            var gpContext = context as GpBoundedContext;
            return gpContext.RrOtsRmApplies;
        }
        #endregion
    }
}
