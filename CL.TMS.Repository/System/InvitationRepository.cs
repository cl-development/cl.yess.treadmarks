﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using CL.TMS.Common.Extension;
using Claim = System.Security.Claims.Claim;

namespace CL.TMS.Repository.System
{
    public class InvitationRepository : BaseRepository<SystemBoundedContext, Invitation, long>, IInvitationRepository
    {
        public List<UserInviteModel> LoadInvitations(string searchText, int vendorId, string orderBy, string sortDirection)
        {
            if (vendorId > 0)
            {
                return LoadParticipantUserInvitations(searchText, orderBy, sortDirection, vendorId);
            }
            else
            {
                var now = DateTime.Now.Date;
                var dbContext = context as SystemBoundedContext;
                var query = from i in dbContext.Invitations
                            join usr in dbContext.Users
                            on i.EmailAddress equals usr.Email into userInvitation
                            from subUser in userInvitation.DefaultIfEmpty()
                            select new UserInviteModel
                            {
                                ID = i.ID,
                                FirstName = i.FirstName,
                                LastName = i.LastName,
                                InviteEmail = i.EmailAddress,
                                ExpireDate = i.ExpireDate,
                                InvitedDate = i.InviteDate,
                                InviteSendDate=i.InviteSendDate,
                                Status = i.Status,
                                UserEmail = i.EmailAddress,
                                Inactive = subUser != null ? subUser.Inactive : false,
                                IsLocled = subUser != null ? subUser.IsLocked : false,
                                DisplayStatus = (subUser != null) ? ((subUser.Inactive) ? "Inactive" : (subUser.IsLocked ? "Locked" : "Active")) : ((i.ExpireDate < now) ? "Invited Expired" : "Invite Sent"), //OTSTM2-1013
                                VendorId = (i.VendorId != null) ? (int)i.VendorId : ((i.CustomerId != null) ? (int)i.CustomerId : 0),
                                Rowversion = i.Rowversion
                            };

                //Load staff user only
                query = query.Where(c => c.VendorId == 0);
                //Applying filter
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query=filterSearchText(searchText,query);
                }

                //Applying sorting
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");
                
                return query.ToList();
            }
        }

        private IQueryable<UserInviteModel> filterSearchText(string searchText, IQueryable<UserInviteModel> query)
        {
            searchText = searchText.ToLower();
            DateTime dateValue;
            if (DateTime.TryParse(searchText, out dateValue))
            {               
                if ((searchText.Length <= 7) && (dateValue.Day == 8))//i.e. 2016-02
                {
                    var nextMonth = dateValue.AddMonths(1);
                    query = query.Where(c => (DbFunctions.TruncateTime(c.InviteSendDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.InviteSendDate) <= DbFunctions.TruncateTime(nextMonth)));
                }
                else
                {
                    query = query.Where(c => DbFunctions.TruncateTime(c.InviteSendDate) == DbFunctions.TruncateTime(dateValue));
                }
            }
            else
            {                
                query = query.Where(c => c.FirstName.ToLower().Contains(searchText)
                                        || c.LastName.ToLower().Contains(searchText)
                                        || c.InviteEmail.ToLower().Contains(searchText)
                                        || c.DisplayStatus.ToLower().Contains(searchText)
                                        || c.InviteSendDate.ToString().Contains(searchText)
                    );
            }
            return query;
        }

        private List<UserInviteModel> LoadParticipantUserInvitations(string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var now = DateTime.Now.Date;
            var dbContext = context as SystemBoundedContext;
            var query = from i in dbContext.Invitations
                        join usr in dbContext.Users
                        on i.EmailAddress equals usr.Email into userInvitation
                        from subUser in userInvitation.DefaultIfEmpty()
                        select new UserInviteModel
                        {
                            ID = i.ID,
                            FirstName = i.FirstName,
                            LastName = i.LastName,
                            InviteEmail = i.EmailAddress,
                            ExpireDate = i.ExpireDate,
                            InvitedDate = i.InviteDate,
                            InviteSendDate=i.InviteSendDate,
                            Status = i.Status,
                            UserEmail = i.EmailAddress,
                            IsLocled = (subUser != null) ? subUser.IsLocked : false,
                            VendorId = (i.VendorId != null) ? (int)i.VendorId : ((i.CustomerId != null) ? (int)i.CustomerId : 0),
                            IsVendor = (i.VendorId != null),
                            UserId = (subUser != null) ? subUser.ID : 0,
                            Rowversion = i.Rowversion
                        };

            //Load participant user only
            var queryResult = query.Where(c => c.VendorId == vendorId).ToList();
            if (queryResult.Count > 0)
            {
                var isVendor = queryResult.First().IsVendor;
                if (isVendor)
                {
                    var vendorUsers = dbContext.VendorUsers.Where(c => c.VendorID == vendorId).ToList();
                    queryResult.ForEach(c =>
                    {
                        if (c.UserId > 0)
                        {
                            var vendorUser = vendorUsers.FirstOrDefault(q => q.UserID == c.UserId);
                            if (vendorUser != null)
                            {
                                c.Inactive = !vendorUser.IsActive;
                                c.DisplayStatus = vendorUser.IsActive ? (c.IsLocled ? "Locked" : "Active") : "Inactive"; //OTSTM2-1013
                            }
                            else
                            {
                                c.Inactive = true;
                                c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                            }
                        }
                        else
                        {
                            c.Inactive = true;
                            c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                        }
                    });
                }
                else
                {
                    var customerUsers = dbContext.CustomerUsers.Where(c => c.CustomerId == vendorId).ToList();
                    queryResult.ForEach(c =>
                    {
                        if (c.UserId > 0)
                        {
                            var customerUser = customerUsers.FirstOrDefault(q => q.UserId == c.UserId);
                            if (customerUser != null)
                            {
                                c.Inactive = !customerUser.IsActive;
                                c.DisplayStatus = customerUser.IsActive ? (c.IsLocled ? "Locked" : "Active") : "Inactive"; //OTSTM2-1013
                            }
                            else
                            {
                                c.Inactive = true;
                                c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                            }
                        }
                        else
                        {
                            c.Inactive = true;
                            c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                        }
                    });
                }
            }
            var secondQuery = queryResult.AsQueryable();

            //Apllying seach text
            //if (!string.IsNullOrWhiteSpace(searchText))
            //{
            //    DateTime dateValue;
            //    if (DateTime.TryParse(searchText, out dateValue))
            //    {
            //        //dateValue = dateValue.AddDays(TMS.Common.TreadMarksConstants.InviteExpirationDays);
            //        if ((searchText.Length <= 7) && (dateValue.Day == 8))//i.e. 2016-02
            //        {
            //            var nextMonth = dateValue.AddMonths(1);
            //            secondQuery = secondQuery.Where(c => (c.InviteSendDate.Date >= dateValue.Date) && (c.InviteSendDate.Date <= nextMonth.Date));
            //        }
            //        else
            //        {
            //            secondQuery = secondQuery.Where(c => c.InviteSendDate.Date == dateValue.Date);
            //        }
            //    }
            //    else
            //    {
            //        searchText = searchText.ToLower();
            //        secondQuery = secondQuery.Where(c => c.FirstName.ToLower().Contains(searchText) || c.LastName.ToLower().Contains(searchText)
            //              || c.InviteEmail.ToLower().Contains(searchText) || c.DisplayStatus.ToLower().Contains(searchText)
            //              || c.InviteSendDate.ToString().Contains(searchText));
            //    }
            //}
            //Applying filter
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                secondQuery = filterSearchText(searchText, secondQuery);
            }

            //Applying sorting
            secondQuery = sortDirection == "asc" ? secondQuery.OrderBy(orderBy) : secondQuery.OrderBy(orderBy + " descending");
            return secondQuery.ToList();
        }

        public bool EmailIsExisting(string emailAddress)
        {
            return entityStore.All.Any(c => c.EmailAddress == emailAddress);
        }

        public bool EmailIsExistingForParticipant(string emailAddress, int participantId, bool isVendor)
        {
            if (isVendor)
            {
                return entityStore.All.Any(c => c.EmailAddress == emailAddress && c.VendorId == participantId);
            }
            else
            {
                return entityStore.All.Any(c => c.EmailAddress == emailAddress && c.CustomerId == participantId);
            }
        }

        public void CreateInviation(Invitation invitation)
        {
            entityStore.Insert(invitation);
        }

        public Invitation FindInvitation(Guid guid)
        {
            return entityStore.AllIncluding(c => c.InvitationRoles).FirstOrDefault(c => c.InviteGUID == guid);
        }

        public Invitation FindInvitationByUserName(string userName)
        {
            return entityStore.All.FirstOrDefault(c => c.UserName.ToLower() == userName.ToLower() && c.VendorId == null && c.CustomerId == null);
        }

        public Invitation FindInvitationByUserName(string userName, int vendorId, bool isVendor)
        {
            if (isVendor)
            {
                return entityStore.All.FirstOrDefault(c => c.UserName.ToLower() == userName.ToLower() && c.VendorId == vendorId);
            }
            else
            {
                return entityStore.All.FirstOrDefault(c => c.UserName.ToLower() == userName.ToLower() && c.CustomerId == vendorId);
            }
        }

        public Invitation FindParticipantInvitation(string userName, int vendorId, bool isVendor)
        {
            if (isVendor)
            {
                return entityStore.All.FirstOrDefault(c => c.UserName == userName && c.VendorId == vendorId);
            }
            else
            {
                return entityStore.All.FirstOrDefault(c => c.UserName == userName && c.CustomerId == vendorId);
            }
        }
        public void UpdateInvitation(Invitation invitation)
        {
            entityStore.Update(invitation);
        }

        public void UpdateStatus(Invitation invitation)
        {
            entityStore.Update(invitation, c => c.Status);
        }
        public void UpdateRedirectUrl(Invitation invitation)
        {
            entityStore.Update(invitation);
        }

        public Invitation FindInvitationByGUID(Guid InviteGUID)
        {
            return entityStore.All.FirstOrDefault(c => c.InviteGUID == InviteGUID);
        }

        public Invitation FindInvitationByApplicationGUID(Guid ApplicationGUID)
        {
            return entityStore.All.FirstOrDefault(c => c.RedirectUrl.Contains(ApplicationGUID.ToString()));
        }

        public VendorUser FindVendorUser(long userId, int vendorId)
        {
            var dbContext = context as SystemBoundedContext;
            return dbContext.VendorUsers.FirstOrDefault(c => c.UserID == userId && c.VendorID == vendorId);
        }

        public CustomerUser FindCustomerUser(long userId, int customerId)
        {
            var dbContext = context as SystemBoundedContext;
            return dbContext.CustomerUsers.FirstOrDefault(c => c.UserId == userId && c.CustomerId == customerId);
        }
        public void AddVendorUser(VendorUser vendorUser)
        {
            var dbContext = context as SystemBoundedContext;
            dbContext.VendorUsers.Add(vendorUser);
            dbContext.SaveChanges();
        }

        public void CleanVendorUser(int vendorId, long userId)
        {
            var dbContext = context as SystemBoundedContext;
            var vendorUsers = dbContext.VendorUsers.Where(c => c.VendorID == vendorId && c.UserID == userId).ToList();
            dbContext.VendorUsers.RemoveRange(vendorUsers);
            dbContext.SaveChanges();
        }

        public void CleanCustomerUser(int customerId, long userId)
        {
            var dbContext = context as SystemBoundedContext;
            var customerUsers = dbContext.CustomerUsers.Where(c => c.CustomerId == customerId && c.UserId == userId).ToList();
            dbContext.CustomerUsers.RemoveRange(customerUsers);
            dbContext.SaveChanges();
        }
        public void AddCustomerUser(CustomerUser customerUser)
        {
            var dbContext = context as SystemBoundedContext;
            dbContext.CustomerUsers.Add(customerUser);
            dbContext.SaveChanges();
        }
        public PaginationDTO<UserInviteModel, long> LoadInvitations(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            if (vendorId > 0)
            {
                return LoadParticipantUserInvitations(pageIndex, pageSize, searchText, orderBy, sortDirection, vendorId);
            }
            else
            {
                var now = DateTime.Now.Date;
                var dbContext = context as SystemBoundedContext;
                var query = from i in dbContext.Invitations
                            join usr in dbContext.Users
                            on i.EmailAddress equals usr.Email into userInvitation
                            from subUser in userInvitation.DefaultIfEmpty()
                            select new UserInviteModel
                            {
                                ID = i.ID,
                                FirstName = i.FirstName,
                                LastName = i.LastName,
                                InviteEmail = i.EmailAddress,
                                ExpireDate = i.ExpireDate,
                                InvitedDate = i.InviteDate,
                                InviteSendDate=i.InviteSendDate,
                                Status = i.Status,
                                UserEmail = i.EmailAddress,
                                Inactive = (subUser != null) ? subUser.Inactive : false,
                                IsLocled = (subUser != null) ? subUser.IsLocked : false,
                                VendorId = (i.VendorId != null) ? (int)i.VendorId : ((i.CustomerId != null) ? (int)i.CustomerId : 0),
                                DisplayStatus = (subUser != null) ? (subUser.Inactive ? "Inactive" : (subUser.IsLocked ? "Locked" : "Active")) : ((i.ExpireDate < now) ? "Invite Expired" : "Invite Sent"), //OTSTM2-1013
                                Rowversion = i.Rowversion
                            };

                //Load staff user only
                query = query.Where(c => c.VendorId == 0);

                //Apllying seach text
                //if (!string.IsNullOrWhiteSpace(searchText))
                //{
                //    DateTime dateValue;
                //    if (DateTime.TryParse(searchText, out dateValue))
                //    {
                //        dateValue = dateValue.AddDays(TMS.Common.TreadMarksConstants.InviteExpirationDays);
                //        if ((searchText.Length <= 7) && (dateValue.Day == 8))//i.e. 2016-02
                //        {
                //            var nextMonth = dateValue.AddMonths(1);
                //            query = query.Where(c => (DbFunctions.TruncateTime(c.ExpireDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.ExpireDate) <= DbFunctions.TruncateTime(nextMonth)));
                //        }
                //        else
                //        {
                //            query = query.Where(c => DbFunctions.TruncateTime(c.ExpireDate) == DbFunctions.TruncateTime(dateValue));
                //        }
                //    }
                //    else
                //    {
                //        searchText = searchText.ToLower();
                //        query = query.Where(c => c.FirstName.ToLower().Contains(searchText) || c.LastName.ToLower().Contains(searchText)
                //              || c.InviteEmail.ToLower().Contains(searchText) || c.DisplayStatus.ToLower().Contains(searchText)
                //              || DbFunctions.AddDays(c.ExpireDate, -1 * TMS.Common.TreadMarksConstants.InviteExpirationDays).ToString().Contains(searchText));
                //    }
                //}
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query = filterSearchText(searchText, query);
                }
                //Applying sorting
                query = sortDirection == "asc" ? query.OrderBy(orderBy) : query.OrderBy(orderBy + " descending");

                var totalRecords = query.Count();
                var result = query.Skip(pageIndex)
                        .Take(pageSize)
                        .ToList();

                return new PaginationDTO<UserInviteModel, long>
                {
                    DTOCollection = result,
                    TotalRecords = totalRecords,
                    PageNumber = pageIndex + 1,
                    PageSize = pageSize
                };
            }
        }

        private PaginationDTO<UserInviteModel, long> LoadParticipantUserInvitations(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int vendorId)
        {
            var now = DateTime.Now.Date;
            var dbContext = context as SystemBoundedContext;
            var query = from i in dbContext.Invitations
                        join usr in dbContext.Users
                        on i.EmailAddress equals usr.Email into userInvitation
                        from subUser in userInvitation.DefaultIfEmpty()
                        select new UserInviteModel
                        {
                            ID = i.ID,
                            FirstName = i.FirstName,
                            LastName = i.LastName,
                            InviteEmail = i.EmailAddress,
                            ExpireDate = i.ExpireDate,
                            InvitedDate = i.InviteDate,
                            InviteSendDate=i.InviteSendDate,
                            Status = i.Status,
                            UserEmail = i.EmailAddress,
                            IsLocled = (subUser != null) ? subUser.IsLocked : false,
                            VendorId = (i.VendorId != null) ? (int)i.VendorId : ((i.CustomerId != null) ? (int)i.CustomerId : 0),
                            IsVendor = (i.VendorId != null),
                            UserId = (subUser != null) ? subUser.ID : 0,
                            Rowversion = i.Rowversion
                        };

            //Load participant user only
            var queryResult = query.Where(c => c.VendorId == vendorId).ToList();
            if (queryResult.Count > 0)
            {
                var isVendor = queryResult.First().IsVendor;
                if (isVendor)
                {
                    var vendorUsers = dbContext.VendorUsers.Where(c => c.VendorID == vendorId).ToList();
                    queryResult.ForEach(c =>
                    {
                        if (c.UserId > 0)
                        {
                            var vendorUser = vendorUsers.FirstOrDefault(q => q.UserID == c.UserId);
                            if (vendorUser != null)
                            {
                                c.Inactive = !vendorUser.IsActive;
                                c.DisplayStatus = vendorUser.IsActive ? (c.IsLocled ? "Locked" : "Active") : "Inactive"; //OTSTM2-1013
                            }
                            else
                            {
                                c.Inactive = true;
                                c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                            }
                        }
                        else
                        {
                            c.Inactive = true;
                            c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                        }
                    });
                }
                else
                {
                    var customerUsers = dbContext.CustomerUsers.Where(c => c.CustomerId == vendorId).ToList();
                    queryResult.ForEach(c =>
                    {
                        if (c.UserId > 0)
                        {
                            var customerUser = customerUsers.FirstOrDefault(q => q.UserId == c.UserId);
                            if (customerUser != null)
                            {
                                c.Inactive = !customerUser.IsActive;
                                c.DisplayStatus = customerUser.IsActive ? (c.IsLocled ? "Locked" : "Active") : "Inactive"; //OTSTM2-1013
                            }
                            else
                            {
                                c.Inactive = true;
                                c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                            }
                        }
                        else
                        {
                            c.Inactive = true;
                            c.DisplayStatus = (c.ExpireDate < now) ? "Invite Expired" : "Invite Sent";
                        }
                    });
                }
            }


            var secondQuery = queryResult.AsQueryable();

            //Apllying seach text
            //if (!string.IsNullOrWhiteSpace(searchText))
            //{
            //    DateTime dateValue;
            //    if (DateTime.TryParse(searchText, out dateValue))
            //    {
            //        dateValue = dateValue.AddDays(TMS.Common.TreadMarksConstants.InviteExpirationDays);
            //        if ((searchText.Length <= 7) && (dateValue.Day == 8))//i.e. 2016-02
            //        {
            //            var nextMonth = dateValue.AddMonths(1);
            //            secondQuery = secondQuery.Where(c => (c.ExpireDate.Date >= dateValue.Date) && (c.ExpireDate.Date <= nextMonth.Date));
            //        }
            //        else
            //        {
            //            secondQuery = secondQuery.Where(c => c.ExpireDate.Date == dateValue.Date);
            //        }
            //    }
            //    else
            //    {
            //        searchText = searchText.ToLower();
            //        secondQuery = secondQuery.Where(c => c.FirstName.ToLower().Contains(searchText) || c.LastName.ToLower().Contains(searchText)
            //              || c.InviteEmail.ToLower().Contains(searchText) || c.DisplayStatus.ToLower().Contains(searchText)
            //              || c.ExpireDate.AddDays(-1 * TMS.Common.TreadMarksConstants.InviteExpirationDays).ToString().Contains(searchText));
            //    }
            //}
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                secondQuery = filterSearchText(searchText, secondQuery);
            }
            //Applying sorting
            secondQuery = sortDirection == "asc" ? secondQuery.OrderBy(orderBy) : secondQuery.OrderBy(orderBy + " descending");

            var totalRecords = secondQuery.Count();
            var result = secondQuery.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<UserInviteModel, long>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public Invitation FindInvitationByApplicationId(int applicationId)
        {
            return entityStore.All.FirstOrDefault(c => c.ApplicationId == applicationId);
        }

        public Invitation FindInvitationByVendorId(int vendorId)
        {
            return entityStore.All.FirstOrDefault(c => c.VendorId == vendorId);
        }

        public Invitation FindInvitation(int vendorId, string email, bool isVendor)
        {
            if (isVendor)
            {
                return entityStore.All.FirstOrDefault(c => c.VendorId == vendorId && c.EmailAddress.ToLower() == email.ToLower());
            }
            else
            {
                return entityStore.All.FirstOrDefault(c => c.CustomerId == vendorId && c.EmailAddress.ToLower() == email.ToLower());
            }
        }
    }
}
