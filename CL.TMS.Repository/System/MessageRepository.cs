﻿using CL.TMS.Common;
using CL.TMS.Common.Extension;
using CL.TMS.DAL;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Announcement;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Framework.DAL;
using CL.TMS.Framework.DTO;
using CL.TMS.IRepository.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CL.TMS.Repository.System
{
    public class MessageRepository : BaseRepository<MessageBoundedContext, Notification, int>, IMessageRepository
    {
        #region Notification

        public void AddNotification(Notification notification)
        {
            var dbContext = context as MessageBoundedContext;
            dbContext.Notifications.Add(notification);
            dbContext.SaveChanges();
        }

        //OTSTM2-635
        public bool AddBatchPostNotification(Notification notification)
        {
            var isAdded = false;
            var dbContext = new MessageBoundedContext();
            var existingNotification = dbContext.Notifications.FirstOrDefault(c => c.ObjectId == notification.ObjectId && c.AssignerInitial == "TM");
            if (existingNotification == null)
            {
                dbContext.Notifications.Add(notification);
                dbContext.SaveChanges();
                isAdded = true;
            }
            return isAdded;
        }

        public PaginationDTO<NotificationViewModel, int> LoadAllNotifications(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string userName)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from notification in dbContext.Notifications
                        where (notification.Assignee == userName)
                        select new NotificationViewModel
                        {
                            ID = notification.ID,
                            CreatedDate = notification.CreatedTime,
                            Message = notification.Message,
                            ClaimId = notification.ObjectId,
                            RegistrationNumber = string.Empty
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<NotificationViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<NotificationViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<NotificationViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<Notification> LoadUnreadNotifications(string userName)
        {
            var dbContext = context as MessageBoundedContext;
            var lastUnreadNotification = dbContext.Notifications.Where(c => c.Status == "Unread" && c.Assignee == userName).OrderBy(c => c.CreatedTime).FirstOrDefault();
            if (lastUnreadNotification != null)
            {
                var query = dbContext.Notifications.Where(c => c.Status != "Hidden" && c.Assignee == userName).OrderByDescending(c => c.CreatedTime).Where(c => c.ID >= lastUnreadNotification.ID).Take(50);
                return query.ToList();
            }
            else
            {
                var query = dbContext.Notifications.Where(c => c.Status == "Read" && c.Assignee == userName).OrderByDescending(c => c.CreatedTime).Take(50);
                return query.ToList();
            }
        }

        public void MarkAllAsRead(string userName)
        {
            var dbContext = context as MessageBoundedContext;
            dbContext.Notifications.Where(c => c.Status == "Unread" && c.Assignee == userName).ToList().ForEach(c => c.Status = "Read");
            dbContext.SaveChanges();
        }

        public int TotalUnreadNotification(string userName)
        {
            var dbContext = context as MessageBoundedContext;
            return dbContext.Notifications.Where(c => c.Status == "Unread" && c.Assignee == userName).Count();
        }

        public int TotalNotification(string userName)
        {
            var dbContext = context as MessageBoundedContext;
            return dbContext.Notifications.Where(c => c.Assignee == userName).Count();
        }

        public void UpdateNotificationStatus(int notificationId, string newStatus)
        {
            var dbContext = context as MessageBoundedContext;
            var notification = dbContext.Notifications.FirstOrDefault(c => c.ID == notificationId);
            if (notification != null)
            {
                notification.Status = newStatus;
                dbContext.SaveChanges();
            }
        }

        public Notification GetNotificationById(int notificationId)
        {
            var dbContext = context as MessageBoundedContext;
            return dbContext.Notifications.FirstOrDefault(c => c.ID == notificationId);
        }

        public List<AllNotificationsExportModel> GetAllNotificationsExport(string userName, string searchText, string sortColumn, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from notification in dbContext.Notifications
                        where (notification.Assignee == userName)
                        select new AllNotificationsExportModel
                        {
                            CreatedDate = notification.CreatedTime,
                            Message = notification.Message.Trim().Replace("<strong>", "").Replace("</strong>", "")
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<AllNotificationsExportModel>().OrderBy(sortColumn);
            else
                query = query.AsQueryable<AllNotificationsExportModel>().OrderBy(sortColumn + " descending");
            return query.ToList();
        }

        #endregion Notification

        #region Activity

        public void AddActivity(Activity activity)
        {
            var dbContext = context as MessageBoundedContext;
            dbContext.Activities.Add(activity);
            dbContext.SaveChanges();
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where (activity.ObjectId == claimId) &&
                        (activity.ActivityType == TreadMarksConstants.ClaimActivity || activity.ActivityType == TreadMarksConstants.TsfClaimActivity)
                        select new ActivityViewModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ActivityViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<ActivityViewModel, int> LoadAllCommonActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectId, string activityArea, int activityType)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where (activity.ObjectId == objectId || objectId == 0) && (activity.ActivityArea == activityArea) && (activity.ActivityType == activityType)
                        select new ActivityViewModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
            { query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy); }
            else
            {
                orderBy = "ID";
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy + " descending"); // when the datetime is exactly same it should use ID for sorting 
            }
            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ActivityViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public PaginationDTO<ActivityViewModel, int> LoadRecoverableMaterialActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectID)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where activity.ActivityType == TreadMarksConstants.AdminMenuActivity
                        select new ActivityViewModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ActivityViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }
        public PaginationDTO<ActivityViewModel, int> LoadRateGroupsActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int activityType)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where activity.ActivityType == activityType
                        select new ActivityViewModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message,
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ActivityViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        public List<AllActivitiesExportModel> GetAllActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where (activity.ObjectId == cid)
                        select new AllActivitiesExportModel
                        {
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message.Trim().Replace("<strong>", "").Replace("</strong>", "")
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn);
            else
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn + " descending");
            return query.ToList();
        }

        public List<AllActivitiesExportModel> GetAllCommonActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection, string activityArea, int activityType)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where (activity.ObjectId == cid || cid == 0) && (activity.ActivityArea == activityArea) && (activity.ActivityType == activityType)
                        select new AllActivitiesExportModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message.Trim().Replace("<strong>", "").Replace("</strong>", "")
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn);
            else
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn + " descending", "ID" + " descending"); // when the datetime is exactly same it should use ID for sorting 
            return query.ToList();
        }

        public List<AllActivitiesExportModel> GetProductCompositionActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where (activity.ActivityType == TreadMarksConstants.AdminMenuActivity)
                        select new AllActivitiesExportModel
                        {
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message.Trim().Replace("<strong>", "").Replace("</strong>", "")
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn);
            else
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn + " descending");
            return query.ToList();
        }

        public int TotalActivity(int claimid)
        {
            var dbContext = context as MessageBoundedContext;
            return dbContext.Activities.Where(c => c.ObjectId == claimid).Count();
        }

        //OTSTM2-745
        public PaginationDTO<ActivityViewModel, int> LoadAllGPActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where activity.ActivityType == TreadMarksConstants.GPClaimActivity
                        select new ActivityViewModel
                        {
                            ID = activity.ID,
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message,
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText)
                                            || c.Initiator.ToString().Contains(searchText)
                    );
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy);
            else
                query = query.AsQueryable<ActivityViewModel>().OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<ActivityViewModel, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize
            };
        }

        //OTSTM2-745
        public List<AllActivitiesExportModel> GetAllGPActivitiesExport(string searchText, string sortColumn, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;
            var query = from activity in dbContext.Activities
                        where activity.ActivityType == TreadMarksConstants.GPClaimActivity
                        select new AllActivitiesExportModel
                        {
                            CreatedDate = activity.CreatedTime,
                            Initiator = activity.InitiatorName,
                            Message = activity.Message.Trim().Replace("<strong>", "").Replace("</strong>", "")
                        };
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                DateTime dateValue;
                if ((DateTime.TryParse(searchText, out dateValue)) && !searchText.Any(x => char.IsLetter(x)))//&& searchText doesn't contains Letter
                {
                    if ((searchText.Length <= 7) && (dateValue.Day == 1))//i.e. 2016-02
                    {
                        var nextMonth = dateValue.AddMonths(1);
                        query = query.Where(c => (DbFunctions.TruncateTime(c.CreatedDate) >= DbFunctions.TruncateTime(dateValue)) && (DbFunctions.TruncateTime(c.CreatedDate) <= DbFunctions.TruncateTime(nextMonth)));
                    }
                    else
                    {
                        query = query.Where(c => DbFunctions.TruncateTime(c.CreatedDate) == DbFunctions.TruncateTime(dateValue));
                    }
                }
                else
                {
                    searchText = searchText.ToLower();
                    query = query.Where(c => c.Message.ToString().Contains(searchText));
                }
            }

            if (sortDirection == "asc")
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn);
            else
                query = query.AsQueryable<AllActivitiesExportModel>().OrderBy(sortColumn + " descending");
            return query.ToList();
        }

        #endregion Activity

        #region //Announcement

        public void AddAnnouncement(AnnouncementVM vm, long userID)
        {
            var dbContext = context as MessageBoundedContext;
            var announcement = new Announcement()
            {
                Subject = vm.Subject,
                Description = vm.Description,
                StartDate = vm.StartDate.Date,
                EndDate = vm.EndDate.Date,
                Steward = vm.Steward,
                Collector = vm.Collector,
                Hauler = vm.Hauler,
                Processor = vm.Processor,
                RPM = vm.RPM,
                Staff = vm.Staff,
                CreatedDate = DateTime.UtcNow, //.Date,
                CreatedByID = userID
            };
            dbContext.Announcements.Add(announcement);
            dbContext.SaveChanges();
        }

        public void UpdateAnnouncement(AnnouncementVM vm, long userID)
        {
            var dbContext = context as MessageBoundedContext;
            var announcement = dbContext.Announcements.Find(vm.ID);
            if (announcement != null)
            {
                announcement.Subject = vm.Subject;
                announcement.Description = vm.Description;
                announcement.StartDate = vm.StartDate.Date;
                announcement.EndDate = vm.EndDate.Date;
                announcement.Steward = vm.Steward;
                announcement.Collector = vm.Collector;
                announcement.Hauler = vm.Hauler;
                announcement.Processor = vm.Processor;
                announcement.RPM = vm.RPM;
                announcement.Staff = vm.Staff;
                announcement.ModifiedDate = DateTime.UtcNow; //.Date;
                announcement.ModifiedByID = userID;
                dbContext.SaveChanges();
            }
        }

        public void RemoveAnnouncement(int announcementID)
        {
            var dbContext = context as MessageBoundedContext;
            var announcement = dbContext.Announcements.Find(announcementID);
            if (announcement != null)
            {
                dbContext.Announcements.Remove(announcement);
                dbContext.SaveChanges();
            }
        }

        public AnnouncementVM LoadAnnouncementByID(int announcementID)
        {
            var dbContext = context as MessageBoundedContext;
            var result = dbContext.Announcements
                        .Where(x => x.ID == announcementID)
                        .Select(vm => new AnnouncementVM()
                        {
                            Subject = vm.Subject,
                            Description = vm.Description,
                            StartDate = vm.StartDate,
                            EndDate = vm.EndDate,
                            Steward = vm.Steward,
                            Collector = vm.Collector,
                            Hauler = vm.Hauler,
                            Processor = vm.Processor,
                            RPM = vm.RPM,
                            Staff = vm.Staff
                        }).FirstOrDefault();
            return result;
        }

        public PaginationDTO<AnnouncementListVM, int> LoadAnnouncementList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection)
        {
            var dbContext = context as MessageBoundedContext;

            var query = dbContext.Announcements
                        .Select(c => new AnnouncementListVM()
                        {
                            ID = c.ID,
                            Subject = c.Subject,
                            StartDate = c.StartDate,
                            EndDate = c.EndDate,
                            DateAdded = c.CreatedDate,
                            AddedBy = c.CreatedByUser.FirstName + " " + c.CreatedByUser.LastName,
                            ModifiedDate = c.ModifiedDate,
                            ModifiedBy = c.ModifiedByUser.FirstName + " " + c.ModifiedByUser.LastName
                        });

            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "DateAdded" : orderBy;

            if (sortDirection == "asc")
                query = query.OrderBy(orderBy);
            else
                query = query.OrderBy(orderBy + " descending");

            var totalRecords = query.Count();
            var result = query.Skip(pageIndex)
                    .Take(pageSize)
                    .ToList();

            return new PaginationDTO<AnnouncementListVM, int>
            {
                DTOCollection = result,
                TotalRecords = totalRecords,
                PageNumber = pageIndex + 1,
                PageSize = pageSize,
            };
        }

        public IEnumerable<AnnouncementVM> LoadDashboardAnnouncements()
        {
            // var temp = SecurityContextHelper.CurrentVendor;

            var now = DateTime.Now.Date;
            var nextDay = now.AddDays(1);
            var dbContext = context as MessageBoundedContext;
            var result = dbContext.Announcements
                        .Where(x => x.StartDate < nextDay && x.EndDate >= now)
                        .Select(vm => new AnnouncementVM()
                        {
                            Subject = vm.Subject,
                            Description = vm.Description,
                            StartDate = vm.StartDate,
                            EndDate = vm.EndDate,
                            Steward = vm.Steward,
                            CreateDate=vm.CreatedDate,
                            Collector = vm.Collector,
                            Hauler = vm.Hauler,
                            Processor = vm.Processor,
                            RPM = vm.RPM,
                            Staff = vm.Staff
                        });
            return result;
        }

        #endregion //Announcement
    }
}
