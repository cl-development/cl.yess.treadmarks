﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.GpStaging;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.System;
using CL.TMS.DataContracts.ViewModel.GP;

namespace CL.TMS.Repository.System
{
    public class FIStagingRepository : BaseRepository<GpStagingBoundedContext, int>, IFIStagingRepository
    {
        public void AddGpRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmCustomers.Add(customer);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsPmVendor(RR_OTS_PM_VENDORS vendor)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmVendors.Add(vendor);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmTransaction(RR_OTS_RM_TRANSACTIONS transaction)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmTransactions.Add(transaction);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmTransDistributionsRange(List<RR_OTS_RM_TRANS_DISTRIBUTIONS> transDistributions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmTransDistributions.AddRange(transDistributions);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmCashReceipts(List<RR_OTS_RM_CASH_RECEIPT> cashReceipts)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmCashReceipts.AddRange(cashReceipts);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsRmApplies(List<RR_OTS_RM_APPLY> applies)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmApplies.AddRange(applies);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsPmTransaction(RR_OTS_PM_TRANSACTIONS transaction)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransactions.Add(transaction);
            gpContext.SaveChanges();
        }
        public void AddGpRrOtsPmTransDistributions(List<RR_OTS_PM_TRANS_DISTRIBUTIONS> transDistributions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransDistributions.AddRange(transDistributions);
            gpContext.SaveChanges();

        }

        public void AddGpRrOtsPmTransDistribution(RR_OTS_PM_TRANS_DISTRIBUTIONS transDistribution)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransDistributions.Add(transDistribution);
            gpContext.SaveChanges();
        }                
        public IQueryable<RR_OTS_RM_CUSTOMERS> GetAllRrOtsRmCustomers()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsRmCustomers;
        }     
        public IQueryable<RR_OTS_PM_VENDORS> GetAllRrOtsPmVendors()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsPmVendors;
        }
        public IQueryable<RR_OTS_VW_Vendors_Error> GetAllRrOtsVwVendorsError()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOtsVwVendorsErrors;
        }
        public IQueryable<RR_OTS_VW_Customers_Error> GetAllRrOtsVwCustomersError()
        {
            return (context as GpStagingBoundedContext).RrOtsVwCustomersErrors;
        }
        public IQueryable<RR_OTS_VW_Receivables_Invoice_Error> GetAllRrOtsVwReceivablesInvoiceErrors()
        {
            return (context as GpStagingBoundedContext).RrOtsVwReceivablesInvoiceErrors;
        }
        public IQueryable<RR_OTS_VW_Cash_Receipt_Error> GetAllRrOtsVwCashReceiptErrors()
        {
            return (context as GpStagingBoundedContext).RrOtsVwCashReceiptErrors;
        }
        public IQueryable<RR_OTS_VW_Cash_Receipt_Apply_Error> GetAllRrOtsVwCashReceiptApplyErrors()
        {
            return (context as GpStagingBoundedContext).RrOtsVwCashReceiptApplyErrors;
        }
        public IQueryable<RR_OTS_VW_Payables_Invoice_Error> GetAllRrOtsVwPayablesInvoiceErrors()
        {
            return (context as GpStagingBoundedContext).RrOtsVwPayablesInvoiceErrors;
        }
        public IQueryable<RR_OTS_RM_TRANSACTIONS> GetAllRrOtsRmTransactions()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsRmTransactions;
        }
        public IQueryable<RR_OTS_RM_TRANS_DISTRIBUTIONS> GetAllRrOtsRmTransDistributions()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsRmTransDistributions;
        }
        public IQueryable<RR_OTS_RM_CASH_RECEIPT> GetAllRrOtsRmCashReceipts()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsRmCashReceipts;
        }
        public IQueryable<RR_OTS_RM_APPLY> GetAllRrOtsRmApplies()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsRmApplies;
        }
        public IQueryable<RR_OTS_PM_TRANS_DISTRIBUTIONS> GetAllRrOtsPmTransDistributions()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsPmTransDistributions;
        }
        public void UpdateRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer)
        {
            var gpContext = context as GpStagingBoundedContext;
            var row = gpContext.RrOTsRmCustomers.SingleOrDefault(c => c.CUSTNMBR == customer.CUSTNMBR && c.ADRSCODE == customer.ADRSCODE && c.INTERID == customer.INTERID);
            if (row != null)
            {
                row.CUSTNAME = customer.CUSTNAME;
                row.CUSTCLAS = customer.CUSTCLAS;
                row.CNTCPRSN = customer.CNTCPRSN;
                row.TAXSCHID = customer.TAXSCHID;
                row.ADDRESS1 = customer.ADDRESS1;
                row.ADDRESS2 = customer.ADDRESS2;
                row.ADDRESS3 = customer.ADDRESS3;
                row.COUNTRY = customer.COUNTRY;
                row.CITY = customer.CITY;
                row.STATE = customer.STATE;
                row.ZIP = customer.ZIP;
                row.PHNUMBR1 = customer.PHNUMBR1;
                row.FAX = customer.FAX;
                row.CURNCYID = customer.CURNCYID;
                row.PYMTRMID = customer.PYMTRMID;
                row.USERDEF1 = customer.USERDEF1;
                row.USERDEF2 = customer.USERDEF2;
                row.INACTIVE = customer.INACTIVE;
                row.INTSTATUS = customer.INTSTATUS;
                row.INTDATE = customer.INTDATE;
                row.ERRORCODE = customer.ERRORCODE;
            }
            gpContext.SaveChanges();
        }
        public void UpdateRrOtsPmVendor(RR_OTS_PM_VENDORS vendor)
        {
            var gpContext = context as GpStagingBoundedContext;
            var row = gpContext.RrOTsPmVendors.SingleOrDefault(v => v.VADDCDPR == vendor.VADDCDPR && v.VENDORID == vendor.VENDORID && v.INTERID == vendor.INTERID);
            if (row != null)
            {
                row.VENDNAME = vendor.VENDNAME;
                row.VNDCLSID = vendor.VNDCLSID;
                row.VNDCNTCT = vendor.VNDCNTCT;
                row.ADDRESS1 = vendor.ADDRESS1;
                row.ADDRESS2 = vendor.ADDRESS2;
                row.ADDRESS3 = vendor.ADDRESS3;
                row.CITY = vendor.CITY;
                row.STATE = vendor.STATE;
                row.ZIPCODE = vendor.ZIPCODE;
                row.COUNTRY = vendor.COUNTRY;
                row.PHNUMBR1 = vendor.PHNUMBR1;
                row.FAXNUMBR = vendor.FAXNUMBR;
                row.CURNCYID = vendor.CURNCYID;
                row.TAXSCHID = vendor.TAXSCHID;
                row.PYMTRMID = vendor.PYMTRMID;
                row.USERDEF1 = vendor.USERDEF1;
                row.USERDEF2 = vendor.USERDEF2;
                row.VENDSTTS = vendor.VENDSTTS;
                row.INTSTATUS = vendor.INTSTATUS;
                row.INTDATE = vendor.INTDATE;
                row.ERRORCODE = vendor.ERRORCODE;
                row.EmailCcAddress = vendor.EmailCcAddress;
                row.EmailToAddress = vendor.EmailToAddress;
                row.BANKNAME = vendor.BANKNAME;
                row.EFTBankNumber = vendor.EFTBankNumber;
                row.EFTBankAcct = vendor.EFTBankAcct;
                row.EFTTransitNumber = vendor.EFTTransitNumber;

                //row.DEX_ROW_ID = vendor.;
            }
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsRmCustomer(RR_OTS_RM_CUSTOMERS customer)
        {
            var gpContext = context as GpStagingBoundedContext;
            var row = gpContext.RrOTsRmCustomers.SingleOrDefault(c => c.CUSTNMBR == customer.CUSTNMBR && c.ADRSCODE == customer.ADRSCODE && c.INTERID == customer.INTERID);
            if (row != null)
            {
                gpContext.RrOTsRmCustomers.Remove(row);
            }
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsPmVendor(RR_OTS_PM_VENDORS vendor)
        {
            var gpContext = context as GpStagingBoundedContext;
            var row = gpContext.RrOTsPmVendors.SingleOrDefault(v => v.VADDCDPR == vendor.VADDCDPR && v.VENDORID == vendor.VENDORID && v.INTERID == vendor.INTERID);
            if (row != null)
            {
                gpContext.RrOTsPmVendors.Remove(row);
            }
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsRmCashReceipts(List<RR_OTS_RM_CASH_RECEIPT> cashReceipts)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmCashReceipts.RemoveRange(cashReceipts);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsRmTransactions(List<RR_OTS_RM_TRANSACTIONS> transactions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmTransactions.RemoveRange(transactions);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsRmTransDistributions(List<RR_OTS_RM_TRANS_DISTRIBUTIONS> transDistributions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmTransDistributions.RemoveRange(transDistributions);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsRmApplies(List<RR_OTS_RM_APPLY> applies)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsRmApplies.RemoveRange(applies);
            gpContext.SaveChanges();
        }
        public IQueryable<RR_OTS_PM_TRANSACTIONS> GetAllRrOtsPmTransactions()
        {
            var gpContext = context as GpStagingBoundedContext;
            return gpContext.RrOTsPmTransactions;
        }
        public void DeleteRrOtsPmTransactions(List<RR_OTS_PM_TRANSACTIONS> transactions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransactions.RemoveRange(transactions);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsPmTransaction(RR_OTS_PM_TRANSACTIONS transaction)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransactions.Remove(transaction);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsPmTransDistributions(List<RR_OTS_PM_TRANS_DISTRIBUTIONS> transDistributions)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransDistributions.RemoveRange(transDistributions);
            gpContext.SaveChanges();
        }
        public void DeleteRrOtsPmTransDistribution(RR_OTS_PM_TRANS_DISTRIBUTIONS transDistribution)
        {
            var gpContext = context as GpStagingBoundedContext;
            gpContext.RrOTsPmTransDistributions.Remove(transDistribution);
            gpContext.SaveChanges();
        }

        public GpPaymentSummary GetPaymentSummary(string transactionNumber, string INTERID)
        {
            var gpContext = context as GpStagingBoundedContext;
            var gpPayment = gpContext.RrOtsPmApplies.Where(c => c.TRX_NUMBER.Trim().ToLower() == transactionNumber.Trim().ToLower() && c.INTERID == INTERID).OrderByDescending(c => c.CHQ_EFT_NUMBER).FirstOrDefault();
            var result = new GpPaymentSummary();
            if (gpPayment!=null)
            {
                result.TransactionNumber = transactionNumber;
                result.ChequeNumber = gpPayment.CHQ_EFT_NUMBER; //  gpPayments.Select(c => c.CHQ_EFT_NUMBER).Aggregate((i, j) => i + "," + j);
                result.AmountPaid = gpPayment.DOCAMNT;
                result.PaidDate = gpPayment.DOCDATE;
            }            
            return result;
        }
    }
}
