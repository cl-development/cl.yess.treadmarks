﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.Registrant;
using CL.TMS.DataContracts.ViewModel.Registrant;

namespace CL.TMS.Repository.Registrant
{
    public class AppUserRepository : BaseRepository<RegistrantBoundedContext, AppUser, int>, IAppUserRepository
    {
        //Needs refactoring
        private const string OrderAsc = "asc";
        private const string OrderDesc = "desc";

        public IReadOnlyList<AppUserModel> GetAppUserList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection, string currentVendorNumber)
        {
            var appUserList = new List<AppUserModel>();
            var dbContext = context as RegistrantBoundedContext;

            if (!string.IsNullOrEmpty(searchValue))
            {
                if (orderDirection == OrderAsc)
                {
                    if (orderColumnIndex == 0)
                    {
                        //appUserList = entityStore.All.Where(r => r.Number.Contains(searchValue)).OrderBy(r => r.Number).Skip(skip).Take(take).ToList();  
                        appUserList = (from appUser in dbContext.AppUsers.AsNoTracking()
                                       where appUser.Number.Contains(searchValue) && appUser.Number.Contains(currentVendorNumber)
                                       select new AppUserModel()
                                       {
                                           ID = appUser.ID,
                                           VendorID = appUser.VendorID,
                                           VendorName = appUser.Vendor.BusinessName,
                                           VendorNumber = appUser.Vendor.Number,
                                           VendorAppUserID = appUser.VendorAppUserID,
                                           Number = appUser.Number,
                                           Name = appUser.Name,
                                           EMail = appUser.EMail,
                                           AccessTypeID = appUser.AccessTypeID,
                                           CreatedOn = appUser.CreatedOn,
                                           ModifiedOn = appUser.ModifiedOn,
                                           Metadata = appUser.Metadata,
                                           LastAccessOn = appUser.LastAccessOn,
                                           SyncOn = appUser.SyncOn,
                                           Active = appUser.Active
                                       }).OrderBy(r => r.Number).Skip(skip).Take(take).ToList();
                    }
                }
                else
                {
                    //appUserList = entityStore.All.Where(r => r.Number.Contains(searchValue)).OrderByDescending(r => r.Number).Skip(skip).Take(take).ToList();
                    appUserList = (from appUser in dbContext.AppUsers.AsNoTracking()
                                   where appUser.Number.Contains(searchValue) && appUser.Number.Contains(currentVendorNumber)
                                   select new AppUserModel()
                                   {
                                       ID = appUser.ID,
                                       VendorID = appUser.VendorID,
                                       VendorName = appUser.Vendor.BusinessName,
                                       VendorNumber = appUser.Vendor.Number,
                                       VendorAppUserID = appUser.VendorAppUserID,
                                       Number = appUser.Number,
                                       Name = appUser.Name,
                                       EMail = appUser.EMail,
                                       AccessTypeID = appUser.AccessTypeID,
                                       CreatedOn = appUser.CreatedOn,
                                       ModifiedOn = appUser.ModifiedOn,
                                       Metadata = appUser.Metadata,
                                       LastAccessOn = appUser.LastAccessOn,
                                       SyncOn = appUser.SyncOn,
                                       Active = appUser.Active
                                   }).OrderByDescending(r => r.Number).Skip(skip).Take(take).ToList();
                }
            }
            else
            {
                if (orderDirection == OrderAsc)
                {
                    if (orderColumnIndex >= 0)
                    {
                        //appUserList = entityStore.All.OrderBy(r => r.ID).Skip(skip).Take(take).ToList();
                        appUserList = (from appUser in dbContext.AppUsers.AsNoTracking()
                                       where appUser.Number.Contains(currentVendorNumber)
                                       select new AppUserModel()
                                       {
                                           ID = appUser.ID,
                                           VendorID = appUser.VendorID,
                                           VendorName = appUser.Vendor.BusinessName,
                                           VendorNumber = appUser.Vendor.Number,
                                           VendorAppUserID = appUser.VendorAppUserID,
                                           Number = appUser.Number,
                                           Name = appUser.Name,
                                           EMail = appUser.EMail,
                                           AccessTypeID = appUser.AccessTypeID,
                                           CreatedOn = appUser.CreatedOn,
                                           ModifiedOn = appUser.ModifiedOn,
                                           Metadata = appUser.Metadata,
                                           LastAccessOn = appUser.LastAccessOn,
                                           SyncOn = appUser.SyncOn,
                                           Active = appUser.Active
                                       }).OrderBy(r => r.ID).Skip(skip).Take(take).ToList();
                    }
                }
                else
                {
                    //appUserList = entityStore.All.OrderByDescending(r => r.ID).Skip(skip).Take(take).ToList();
                    appUserList = (from appUser in dbContext.AppUsers.AsNoTracking()
                                   where appUser.Number.Contains(currentVendorNumber)
                                   select new AppUserModel()
                                   {
                                       ID = appUser.ID,
                                       VendorID = appUser.VendorID,
                                       VendorName = appUser.Vendor.BusinessName,
                                       VendorNumber = appUser.Vendor.Number,
                                       VendorAppUserID = appUser.VendorAppUserID,
                                       Number = appUser.Number,
                                       Name = appUser.Name,
                                       EMail = appUser.EMail,
                                       AccessTypeID = appUser.AccessTypeID,
                                       CreatedOn = appUser.CreatedOn,
                                       ModifiedOn = appUser.ModifiedOn,
                                       Metadata = appUser.Metadata,
                                       LastAccessOn = appUser.LastAccessOn,
                                       SyncOn = appUser.SyncOn,
                                       Active = appUser.Active
                                   }).OrderByDescending(r => r.ID).Skip(skip).Take(take).ToList();
                }
            }

            return appUserList;
        }

        public IReadOnlyList<AppUserModel> GetAppUserList(string searchValue, string RegistrationNumber)
        {
            var dbContext = context as RegistrantBoundedContext;

            var query = (from appUser in dbContext.AppUsers.AsNoTracking()
                         select new AppUserModel()
                         {
                             ID = appUser.ID,
                             VendorID = appUser.VendorID,
                             VendorName = appUser.Vendor.BusinessName,
                             VendorNumber = appUser.Vendor.Number,
                             VendorAppUserID = appUser.VendorAppUserID,
                             Number = appUser.Number,
                             Name = appUser.Name,
                             EMail = appUser.EMail,
                             AccessTypeID = appUser.AccessTypeID,
                             CreatedOn = appUser.CreatedOn,
                             ModifiedOn = appUser.ModifiedOn,
                             Metadata = appUser.Metadata,
                             LastAccessOn = appUser.LastAccessOn,
                             SyncOn = appUser.SyncOn,
                             Active = appUser.Active
                         });
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(i => i.Number.Contains(searchValue));
            }
            if (!string.IsNullOrEmpty(RegistrationNumber))
            {
                query = query.Where(i => i.Number.StartsWith(RegistrationNumber));
            }

            return query.OrderBy(c => c.Number).Take(50).ToList();
        }

        public void ActivateAppUser(int qrCodeId)
        {
            var qrCode = entityStore.FindById(qrCodeId);
            qrCode.Active = true;
            qrCode.ModifiedOn = DateTime.Now;
            entityStore.Update(qrCode);
        }

        public void DeActivateAppUser(int qrCodeId)
        {
            var qrCode = entityStore.FindById(qrCodeId);
            qrCode.Active = false;
            qrCode.ModifiedOn = DateTime.Now;
            entityStore.Update(qrCode);
        }

        public void AddAppUser(int amountToAdd, int vendorId, string createdBy)
        {
            if (amountToAdd <= default(int)) { throw new ArgumentOutOfRangeException("amountToAdd"); }
            if (vendorId <= default(int)) { throw new ArgumentOutOfRangeException("vendorId"); }
            if (string.IsNullOrEmpty(createdBy)) { throw new ArgumentNullException("createdBy"); }

            IVendorRepository vendor = new VendorRepository();
            string vendorNumber = vendor.GetVendorNumber(vendorId);

            int maxAppUserIdPerVendor = 0;
            if (entityStore.All.Any(s => s.VendorID == vendorId))
            {
                maxAppUserIdPerVendor = entityStore.All.Where(s => s.VendorID == vendorId).Max(r => r.VendorAppUserID);
            }

            for (int i = 1; i <= amountToAdd; i++)
            {
                int UID = (maxAppUserIdPerVendor + i);
                AppUser appUser = new AppUser()
                {
                    Number = string.Format("{0}-{1}", vendorNumber, UID.ToString()),
                    Active = true,
                    VendorAppUserID = UID,
                    VendorID = vendorId,
                    CreatedOn = DateTime.Now,
                };

                entityStore.Insert(appUser);
            }
        }

        public IList<AppUser> GetUpdatedAppUserList(DateTime lastUpdated)
        {
            return entityStore.All.Where(r => r.CreatedOn > lastUpdated || r.ModifiedOn > lastUpdated).ToList();
        }
        public AppUser GetAppUser(int vendorId, int vendorAppUserId)
        {
            return entityStore.All.Where(r => r.VendorID == vendorId && r.VendorAppUserID == vendorAppUserId).FirstOrDefault();
        }

    }
}
