﻿using System;
using System.Collections.Generic;
using System.Linq;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DAL;
using CL.TMS.Framework.DAL;
using CL.TMS.IRepository.Registrant;

namespace CL.TMS.Repository.Registrant
{
    public class AssetRepository : BaseRepository<RegistrantBoundedContext, Asset, int>, IAssetRepository
    {
        //Needs refactoring
        private const string OrderAsc = "asc";
        private const string OrderDesc = "desc";

        public IReadOnlyList<Asset> IPadList(int skip, int take, string searchValue, int orderColumnIndex, string orderDirection)
        {

            var iPadList = new List<Asset>();

            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = CheckForUnassignedIPadSearchValue(searchValue);
                if (orderDirection == OrderAsc)
                {
                    if (orderColumnIndex == 0)
                    {
                        iPadList = entityStore.All.Where(r => r.AssetTag.ToLower().Contains(searchValue.ToLower()) || (string.IsNullOrEmpty(searchValue) ? r.Vendor == null : r.Vendor.Number.ToLower().Contains(searchValue.ToLower()))).OrderBy(r => r.ID).Skip(skip).Take(take).ToList();
                    }
                    else
                    {
                        iPadList = entityStore.All.Where(r => r.AssetTag.ToLower().Contains(searchValue.ToLower()) || (string.IsNullOrEmpty(searchValue) ? r.Vendor == null : r.Vendor.Number.ToLower().Contains(searchValue.ToLower()))).OrderBy(r => r.Vendor.Number).Skip(skip).Take(take).ToList();
                    }
                }
                else
                {
                    if (orderColumnIndex == 0)
                    {
                        iPadList = entityStore.All.Where(r => r.AssetTag.ToLower().Contains(searchValue.ToLower()) || (string.IsNullOrEmpty(searchValue) ? r.Vendor == null : r.Vendor.Number.ToLower().Contains(searchValue.ToLower()))).OrderByDescending(r => r.ID).Skip(skip).Take(take).ToList();
                    }
                    else
                    {
                        iPadList = entityStore.All.Where(r => r.AssetTag.ToLower().Contains(searchValue.ToLower()) || (string.IsNullOrEmpty(searchValue) ? r.Vendor == null : r.Vendor.Number.ToLower().Contains(searchValue.ToLower()))).OrderByDescending(r => r.Vendor.Number).Skip(skip).Take(take).ToList();
                    }
                }
            }
            else
            {
                if (orderDirection == OrderAsc)
                {
                    if (orderColumnIndex == 0)
                    {
                        iPadList = entityStore.All.OrderBy(r => r.ID).Skip(skip).Take(take).ToList();
                    }
                    else
                    {
                        iPadList = entityStore.All.OrderBy(r => r.Vendor.Number).Skip(skip).Take(take).ToList();
                    }
                }
                else
                {
                    if (orderColumnIndex == 0)
                    {
                        iPadList = entityStore.All.OrderByDescending(r => r.ID).Skip(skip).Take(take).ToList();
                    }
                    else
                    {
                        iPadList = entityStore.All.OrderByDescending(r => r.Vendor.Number).Skip(skip).Take(take).ToList();
                    }
                }

            }
            return iPadList;
        }

        public IReadOnlyList<Asset> IPadList(string searchValue)
        {

            var iPadList = new List<Asset>();
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = CheckForUnassignedIPadSearchValue(searchValue);
                iPadList = entityStore.All.Where(r => r.AssetTag.ToLower().Contains(searchValue.ToLower()) || (string.IsNullOrEmpty(searchValue) ? r.Vendor == null : r.Vendor.Number.ToLower().Contains(searchValue.ToLower()))).ToList();
            }
            else
            {
                iPadList = entityStore.All.ToList();
            }
            return iPadList;
        }

        public IReadOnlyList<Asset> IPadList()
        {
            return entityStore.All.ToList();
        }

        public void AddIPad(int amountToAdd, int? vendorId, int assetTypeID, string createdBy)
        {
            //Fixed for first ever iPad being added
            int maxIPadId = entityStore.All.Where(r => r.AssetType == assetTypeID).Max(r => (int?)r.AssetTypeUID) ?? 0;

            //TO DO: Get iPad number prefix from App Settings
            string numberPrefix = "OTSIPAD";

            for (int i = 1; i <= amountToAdd; i++)
            {
                int assetTypeUID = (maxIPadId + i);
                var iPad = new Asset()
                {
                    AssetTag = string.Format("{0}{1}", numberPrefix, assetTypeUID.ToString()),
                    Active = true,
                    VendorID = vendorId,
                    AssetTypeUID = assetTypeUID,
                    AssetType = assetTypeID,
                    CreatedOn = DateTime.Now,
                    CreatedBy = createdBy,
                };
                entityStore.Insert(iPad);
            }
        }

        public void AssignIPad(int iPadId, int vendorId, string unAssignBy)
        {
            var iPad = entityStore.FindById(iPadId);
            iPad.VendorID = vendorId;
            iPad.UpdatedOn = DateTime.Now;
            iPad.UpdatedBy = unAssignBy;
            entityStore.Update(iPad);
        }

        public void UnAssignIPad(int iPadId, string unAssignBy)
        {
            var iPad = entityStore.FindById(iPadId);
            iPad.VendorID = null;
            iPad.UpdatedOn = DateTime.Now;
            iPad.UpdatedBy = unAssignBy;
            entityStore.Update(iPad);
        }

        public void ActiveIPad(int iPadId, string activatedBy)
        {
            var iPad = entityStore.FindById(iPadId);
            iPad.Active = true;
            iPad.UpdatedOn = DateTime.Now;
            iPad.UpdatedBy = activatedBy;
            entityStore.Update(iPad);
        }

        public void DeActiveIPad(int iPadId, string deActivatedBy)
        {
            var iPad = entityStore.FindById(iPadId);
            iPad.Active = false;
            iPad.UpdatedOn = DateTime.Now;
            iPad.UpdatedBy = deActivatedBy;
            entityStore.Update(iPad);
        }

        public string CheckForUnassignedIPadSearchValue(string searchValue)
        {
            string Unassigned = "unassigned";
            string unAssignedSearchValue = searchValue;
            if (Unassigned.Contains(unAssignedSearchValue.ToLower()))
            {
                unAssignedSearchValue = string.Empty;
            }
            return unAssignedSearchValue;
        }
    }
}
