﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.PDFGenerator.Encryption
{
    public class EncryptionUtility
    {
        public static byte[] Encrypt(byte[] data)
        {
            /// OTS         973ac15a09fa425db002077b4d83193876a3e382eb975906e0befbe9f1582d9f
            ///	eTracks     470198149aee084b5c74e32ebe8f363a021e8c3dcdba7d7e0ec9d9821e3cc8d8
            ///	Yess        945de40a4bba13ae7f6967798f3e3085ab1a1967637ffd1ab1c0c6250a2c6e84
            ///	3rd         b67ef0e6eb433cac0fc6a0f09b67577f0e5d225693604bd09bf635135a1a8acd

            //byte[] otsKey = new byte[] {
            //    0x97, 0x3a, 0xc1, 0x5a, 0x09, 0xfa, 0x42, 0x5d, 0xb0, 0x02, 0x07, 0x7b, 0x4d, 0x83, 0x19, 0x38,
            //    0x76, 0xa3, 0xe3, 0x82, 0xeb, 0x97, 0x59, 0x06, 0xe0, 0xbe, 0xfb, 0xe9, 0xf1, 0x58, 0x2d, 0x9f
            //};

            //byte[] etracksKey = new byte[] {
            //    0x47, 0x01, 0x98, 0x14, 0x9a, 0xee, 0x08, 0x4b, 0x5c, 0x74, 0xe3, 0x2e, 0xbe, 0x8f, 0x36, 0x3a,
            //    0x02, 0x1e, 0x8c, 0x3d, 0xcd, 0xba, 0x7d, 0x7e, 0x0e, 0xc9, 0xd9, 0x82, 0x1e, 0x3c, 0xc8, 0xd8
            //};

            //byte[] yesssKey = new byte[] {
            //    0x94, 0x5d, 0xe4, 0x0a, 0x4b, 0xba, 0x13, 0xae, 0x7f, 0x69, 0x67, 0x79, 0x8f, 0x3e, 0x30, 0x85,
            //    0xab, 0x1a, 0x19, 0x67, 0x63, 0x7f, 0xfd, 0x1a, 0xb1, 0xc0, 0xc6, 0x25, 0x0a, 0x2c, 0x6e, 0x84
            //};

            //byte[] crmKey = new byte[] {
            //    0xb6, 0x7e, 0xf0, 0xe6, 0xeb, 0x43, 0x3c, 0xac, 0x0f, 0xc6, 0xa0, 0xf0, 0x9b, 0x67, 0x57, 0x7f,
            //    0x0e, 0x5d, 0x22, 0x56, 0x93, 0x60, 0x4b, 0xd0, 0x9b, 0xf6, 0x35, 0x13, 0x5a, 0x1a, 0x8a, 0xcd };

            //QRCodeEncryptionKey
            string strQRCodeEncryptionKey = ConfigurationManager.AppSettings["QRCodeEncryptionKey"];

            byte[] byteQRCodeEncryptionKey = StringToByteArray(strQRCodeEncryptionKey);

            byte[] encryptedBytes;


            using (AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider())
            {
                byte[] iv = cryptoServiceProvider.IV;
                cryptoServiceProvider.Key = byteQRCodeEncryptionKey;
                cryptoServiceProvider.IV = iv;
                cryptoServiceProvider.Padding = PaddingMode.Zeros;
                cryptoServiceProvider.Mode = CipherMode.ECB;

                ICryptoTransform transform = cryptoServiceProvider.CreateEncryptor();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                    }

                    encryptedBytes = stream.ToArray();
                }
            }

            return encryptedBytes;
        }
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
