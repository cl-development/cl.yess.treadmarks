﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.PDFGenerator.Encryption;
using CL.TMS.PDFGenerator.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using ZXing;

namespace CL.TMS.PDFGenerator
{
    public class Badges
    {
        #region Fields
        byte[] Content;
        string TemplatePath;
        int BadgesPerPage = 12;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates an object based on PDF tempalte file path
        /// </summary>     
        public Badges(string templatePath)
        {
            TemplatePath = templatePath;
        }
        public Badges(byte[] content)
        {
            this.Content = content;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Create Badges from PDF tempalte
        /// Fills form fields and stamps Barcode
        /// </summary>     
        #region CreateBadgesFromTempalte
        private List<byte[]> CreateBadgesFromTempalte(List<BadgeModel> badgeList)
        {
            List<byte[]> list = new List<byte[]>();

            //Inserts 6 Badges in byte stream list
            for (int index = 0; index < badgeList.Count; index++)
            {
                using (PdfReader reader = new PdfReader(TemplatePath))
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        using (PdfStamper stamper = new PdfStamper(reader, stream))
                        {
                            PdfContentByte cb = stamper.GetOverContent(1);

                            //Identify form field and insert values
                            Dictionary<string, string> formValues = new Dictionary<string, string>();
                            formValues.Add("Registration", badgeList[index].RegistrationNumber + "-" + badgeList[index].UserID);
                            formValues.Add("CompanyName", badgeList[index].BusinessName);

                            foreach (string key in formValues.Keys)
                            {
                                stamper.AcroFields.SetField(key, formValues[key]);
                            }

                            //Form - no longer fillable
                            stamper.FormFlattening = true;

                            //Insert barcode now
                            BarcodeWriter writer = new BarcodeWriter();
                            writer.Format = BarcodeFormat.QR_CODE;

                            writer.Options.Margin = 0;
                            writer.Options.Width = 82;
                            writer.Options.Height = writer.Options.Width;

                            var data = new
                            {
                                r = badgeList[index].RegistrationNumber,
                                w = badgeList[index].UserID
                            };

                            //JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            string qrText = JsonConvert.SerializeObject(data);
                            byte[] compressedBytes = CompressionUtitity.Compress(qrText);
                            byte[] encryptedBytes = EncryptionUtility.Encrypt(compressedBytes);
                            string encryptedString = Encoding.GetEncoding("iso-8859-1").GetString(encryptedBytes);

                            using (System.Drawing.Image imageDrawing = writer.Write(encryptedString))
                            {
                                Image image = Image.GetInstance(imageDrawing, ImageFormat.Png);
                                //image.SetAbsolutePosition(221, 120);

                                //stupid image takes absolute position
                                image.SetAbsolutePosition(215, 488);

                                //needs to align/scale it properly with different sizes (depends on size of device name)                          
                                //image.ScaleAbsolute(158, 50);
                                cb.AddImage(image);
                            }
                        }

                        list.Add(stream.ToArray());
                    }
                }
            }
            return list;
        }
        #endregion

        /// <summary>        
        /// Merges the Badges byte stream on single page
        /// shows only desired no. of Badges - saving printer ink
        /// </summary>     
        #region MergeOnSinglePage
        private byte[] MergeOnSinglePage(List<byte[]> sourceFiles)
        {
            float BadgeWidth = 257;
            float BadgeHeight = 149;
            float PageTop = 0;
            float X = 0;
            float Y = 0;
            float LineLength = 10;

            Document document = new Document();
            //document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            document.SetPageSize(new Rectangle(865, 649));
            using (MemoryStream ms = new MemoryStream())
            {
                PdfWriter copy = PdfWriter.GetInstance(document, ms);
                //Open the document
                document.Open();
                PdfContentByte pdfContentByte = copy.DirectContent;
                document.NewPage();
                // Iterate through all pdf documents
                for (int fileCounter = 0; fileCounter < sourceFiles.Count; fileCounter++)
                {
                    // Create pdf reader
                    PdfReader reader = new PdfReader(sourceFiles[fileCounter]);

                    PdfImportedPage importedPage = copy.GetImportedPage(reader, 1);
                    //document.SetPageSize(reader.GetPageSizeWithRotation(1));

                    switch (fileCounter)
                    {
                        case 0:
                            X = -5; Y = PageTop;
                            break;

                        case 1:
                            X = BadgeWidth + X; Y = PageTop;
                            break;

                        case 2:
                            X = BadgeWidth + X; Y = PageTop;
                            break;

                        case 3:
                            X = -5; Y = PageTop - BadgeHeight;
                            break;

                        case 4:
                            X = BadgeWidth + X; Y = PageTop - BadgeHeight;
                            break;
                        case 5:
                            X = BadgeWidth + X; Y = PageTop - BadgeHeight;
                            break;
                        case 6:
                            X = -5; Y = PageTop - (BadgeHeight * 2);
                            break;
                        case 7:
                            X = BadgeWidth + X; Y = PageTop - (BadgeHeight * 2);
                            break;
                        case 8:
                            X = BadgeWidth + X; Y = PageTop - (BadgeHeight * 2);
                            break;
                        case 9:
                            X = -5; Y = PageTop - (BadgeHeight * 3);
                            break;
                        case 10:
                            X = BadgeWidth + X; Y = PageTop - (BadgeHeight * 3);
                            break;
                        case 11:
                            X = BadgeWidth + X; Y = PageTop - (BadgeHeight * 3);
                            break;
                    }


                    DrawHorizontalLines(BadgeWidth, BadgeHeight, LineLength, pdfContentByte);

                    DrawVerticalLines(BadgeWidth, BadgeHeight, LineLength, pdfContentByte);

                    pdfContentByte.AddTemplate(importedPage, X, Y);

                    copy.FreeReader(reader);
                    reader.Close();
                }
                document.Close();
                return ms.ToArray();
            }
        }

        private static void DrawVerticalLines(float BadgeWidth, float BadgeHeight, float LineLength, PdfContentByte pdfContentByte)
        {
            float LineXVerOffset = 39f;
            float LineYVerOffset = 34;
            float LineYVer = 28;

            //Vertical Line botton
            pdfContentByte.MoveTo(LineXVerOffset, LineYVer);
            pdfContentByte.LineTo(LineXVerOffset, LineYVer + LineLength);

            pdfContentByte.MoveTo(BadgeWidth + LineXVerOffset, LineYVer);
            pdfContentByte.LineTo(BadgeWidth + LineXVerOffset, LineYVer + LineLength);

            pdfContentByte.MoveTo(BadgeWidth * 2 + LineXVerOffset, LineYVer);
            pdfContentByte.LineTo(BadgeWidth * 2 + LineXVerOffset, LineYVer + LineLength);

            pdfContentByte.MoveTo(BadgeWidth * 3 - 2 + LineXVerOffset, LineYVer);
            pdfContentByte.LineTo(BadgeWidth * 3 - 2 + LineXVerOffset, LineYVer + LineLength);


            //Vertical Line top
            pdfContentByte.MoveTo(LineXVerOffset, LineYVerOffset + BadgeHeight * 4);
            pdfContentByte.LineTo(LineXVerOffset, LineYVerOffset + BadgeHeight * 4 + LineLength);

            pdfContentByte.MoveTo(BadgeWidth + LineXVerOffset, LineYVerOffset + BadgeHeight * 4);
            pdfContentByte.LineTo(BadgeWidth + LineXVerOffset, LineYVerOffset + BadgeHeight * 4 + LineLength);

            pdfContentByte.MoveTo(BadgeWidth * 2 + LineXVerOffset, LineYVerOffset + BadgeHeight * 4);
            pdfContentByte.LineTo(BadgeWidth * 2 + LineXVerOffset, LineYVerOffset + BadgeHeight * 4 + LineLength);

            pdfContentByte.MoveTo(BadgeWidth * 3 + LineXVerOffset - 2, LineYVerOffset + BadgeHeight * 4);
            pdfContentByte.LineTo(BadgeWidth * 3 + LineXVerOffset - 2, LineYVerOffset + BadgeHeight * 4 + LineLength);

            pdfContentByte.Stroke();
        }

        private static void DrawHorizontalLines(float BadgeWidth, float BadgeHeight, float LineLength, PdfContentByte pdfContentByte)
        {
            float LineXHor = 28;
            float LineYHor = 783;

            //Horizontal lines
            for (int l = 1; l < 6; l++)
            {
                int XOffset = 1;
                pdfContentByte.SetLineWidth(1);
                if (l != 1)
                {
                    pdfContentByte.MoveTo(LineXHor, LineYHor - (BadgeHeight * l));
                    pdfContentByte.LineTo(LineXHor + LineLength, LineYHor - (BadgeHeight * l));

                    pdfContentByte.MoveTo(XOffset + LineXHor + (BadgeWidth * 3) + LineLength, LineYHor - (BadgeHeight * l));
                    pdfContentByte.LineTo(XOffset + LineXHor + (BadgeWidth * 3) + LineLength * 2, LineYHor - (BadgeHeight * l));
                }

                if (l != 5)
                {
                    int lineGap = 8;
                    pdfContentByte.MoveTo(LineXHor, LineYHor - (BadgeHeight * l) - lineGap);
                    pdfContentByte.LineTo(LineXHor + LineLength, LineYHor - (BadgeHeight * l) - lineGap);


                    pdfContentByte.MoveTo(XOffset + LineXHor + (BadgeWidth * 3) + LineLength, LineYHor - (BadgeHeight * l) - lineGap);
                    pdfContentByte.LineTo(XOffset + LineXHor + (BadgeWidth * 3) + LineLength * 2, LineYHor - (BadgeHeight * l) - lineGap);
                }
                pdfContentByte.Stroke();
            }
        }
        #endregion

        /// <summary>        
        /// Concat one page behind another 
        /// </summary>     
        public void Concat(byte[] secondContent)
        {
            int pageOffset = 0;
            ArrayList master = new ArrayList();
            int f = 0;
            Document document = null;
            PdfCopy writer = null;

            using (MemoryStream firstPDF = new MemoryStream(this.Content))
            {
                using (MemoryStream secondPDF = new MemoryStream(secondContent))
                {
                    Stream[] fileNames = new Stream[] { firstPDF, secondPDF };

                    using (MemoryStream outFile = new MemoryStream())
                    {
                        while (f < fileNames.Length)
                        {
                            // we create a reader for a certain document
                            PdfReader reader = new PdfReader(fileNames[f]);
                            reader.ConsolidateNamedDestinations();
                            // we retrieve the total number of pages
                            int n = reader.NumberOfPages;
                            pageOffset += n;
                            if (f == 0)
                            {
                                // step 1: creation of a document-object
                                document = new Document(reader.GetPageSizeWithRotation(1));
                                // step 2: we create a writer that listens to the document
                                writer = new PdfCopy(document, outFile);
                                // step 3: we open the document
                                document.Open();
                            }
                            // step 4: we add content
                            for (int i = 0; i < n;)
                            {
                                ++i;
                                if (writer != null)
                                {
                                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                                    writer.AddPage(page);
                                }
                            }

                            //Error: Additional information: Document iTextSharp.text.pdf.PdfReader has already been added.
                            //PRAcroForm form = reader.AcroForm;
                            //if ( form != null && writer != null )
                            //{
                            //    writer.AddDocument( reader );
                            //}

                            f++;
                        }
                        // step 5: we close the document
                        if (document != null)
                        {
                            document.Close();
                        }

                        this.Content = outFile.ToArray();
                    }
                }
            }
        }

        /// <summary>        
        /// Generates device Badges based on list provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateBadges(List<BadgeModel> BadgeList)
        {
            int lastPageItemsCount = BadgeList.Count % BadgesPerPage;
            int totalPageCount = (int)Math.Ceiling((double)BadgeList.Count / BadgesPerPage);

            for (int index = 0; index < totalPageCount; index++)
            {
                if (index == 0)
                    this.Content = MergeOnSinglePage(CreateBadgesFromTempalte(BadgeList.GetRange(index, BadgeList.Count < BadgesPerPage ? BadgeList.Count : BadgesPerPage)));
                else if ((index + 1 == totalPageCount && lastPageItemsCount > 0))
                    Concat(MergeOnSinglePage(CreateBadgesFromTempalte(BadgeList.GetRange(index * BadgesPerPage, lastPageItemsCount))));
                else
                    Concat(MergeOnSinglePage(CreateBadgesFromTempalte(BadgeList.GetRange(index * BadgesPerPage, BadgesPerPage))));
            }
        }

        /// <summary>        
        /// Generates device Badge based on model provided
        /// Call GetBytes() to get the generated stream
        /// </summary>     
        public void GenerateSingleBadge(BadgeModel Badge)
        {
            this.Content = MergeOnSinglePage(CreateBadgesFromTempalte(new List<BadgeModel>() { Badge }));
        }

        public byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                this.Save(stream);

                return stream.ToArray();
            }
        }
        public void Save(Stream stream)
        {
            if (this.Content.Length > 0)
            {
                stream.Write(this.Content, 0, this.Content.Length);
            }
        }

        #endregion
    }
}

