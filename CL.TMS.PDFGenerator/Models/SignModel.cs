﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.PDFGenerator.Models
{
    public class SignModel
    {
        public string RegistrationNumber
        {
            get;
            set;
        }
        public string UserID
        {
            get;
            set;
        }

        public string BusinessName
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public SignModel Clone()
        {
            return (SignModel)this.MemberwiseClone();
        }
    }
}
