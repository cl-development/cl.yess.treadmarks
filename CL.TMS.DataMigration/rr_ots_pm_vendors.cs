//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class rr_ots_pm_vendors
    {
        public string VENDORID { get; set; }
        public string VENDNAME { get; set; }
        public string VADDCDPR { get; set; }
        public string VNDCLSID { get; set; }
        public string VNDCNTCT { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string COUNTRY { get; set; }
        public string PHNUMBR1 { get; set; }
        public string FAXNUMBR { get; set; }
        public string CURNCYID { get; set; }
        public string TAXSCHID { get; set; }
        public string PYMTRMID { get; set; }
        public string USERDEF1 { get; set; }
        public string USERDEF2 { get; set; }
        public Nullable<int> VENDSTTS { get; set; }
        public string INTERID { get; set; }
        public Nullable<int> INTSTATUS { get; set; }
        public Nullable<System.DateTime> INTDATE { get; set; }
        public string ERRORCODE { get; set; }
        public int DEX_ROW_ID { get; set; }
        public long gpibatchentry_id { get; set; }
    
        public virtual gpibatch_entry gpibatch_entry { get; set; }
    }
}
