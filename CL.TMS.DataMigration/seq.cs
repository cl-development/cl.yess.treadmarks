//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class seq
    {
        public string seq_name { get; set; }
        public Nullable<int> start { get; set; }
        public Nullable<int> increment { get; set; }
        public Nullable<int> curval { get; set; }
        public string flag { get; set; }
    }
}
