﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    class RegistrantDataMgr
    {
        private otsdbEntitiesLegacy dbContext;
        public RegistrantDataMgr()
        {
            dbContext = new otsdbEntitiesLegacy();
        }

        public IEnumerable<registrant> GetAllByRegType(int regTypeId)
        {
            //return this.dbContext.registrants.Where(r => r.registrant_type_id == regTypeId).Take(1);
            //return this.dbContext.registrants.Where(r => r.registrant_type_id == regTypeId).OrderBy(r=>r.registration_number);
            return this.dbContext.registrants.OrderByDescending(r => r.registration_number);
            /*return this.dbContext.registrants.Where(r => r.registration_number == 3000023
                || r.registration_number == 3000153
                ).OrderBy(r => r.registration_number);*/

        }
        public IEnumerable<registrant> GetStewards(int regTypeId)
        {
            //return this.dbContext.registrants.Where(r => r.registrant_type_id == regTypeId).Take(1);
            return this.dbContext.registrants.Where(r => r.registrant_type_id == regTypeId).OrderBy(r=>r.registration_number);
            //return this.dbContext.registrants.OrderByDescending(r => r.registration_number);
            /*return this.dbContext.registrants.Where(r => r.registration_number == 3000023
                || r.registration_number == 3000153
                ).OrderBy(r => r.registration_number);*/

        }
        public IEnumerable<registrant_sub_type> GetAllSubTypes()
        {
            return this.dbContext.registrant_sub_type;
        }

        public IEnumerable<registrant_activity_history> GetRegActivityHistory(decimal regNo)
        {
            return this.dbContext.registrant_activity_history.Where(r => r.registration_number == regNo).OrderByDescending(r => r.active_state_change_date);
        }

        public IEnumerable<tsf_claim_summary> GetTSFClaimSummary(decimal regNo)
        {
            return this.dbContext.tsf_claim_summary.Where(r => r.registration_number == regNo && r.record_state == "F").OrderBy(r => r.tsf_claim_summary_id);
        }

        public IEnumerable<tsf_claim_detail> GetTSFClaimDetails(long claimId)
        {
            return this.dbContext.tsf_claim_detail.Where(r => r.tsf_claim_id == claimId).OrderBy(r => r.tsf_claim_detail_id);
        }

        public IEnumerable<enhanced_notes> GetEnhancedNotes(string id_type, long clientId)
        {
            //id_type=tsf_claim_summary
            return this.dbContext.enhanced_notes.Where(r => r.id_type == id_type && r.client_id == clientId).OrderBy(r => r.note_id);
        }

    }
}


