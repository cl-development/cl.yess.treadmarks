//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class collector_detail_tm
    {
        public int id { get; set; }
        public System.DateTime date_collected { get; set; }
        public string form_type { get; set; }
        public decimal form_number { get; set; }
        public string hauler_number { get; set; }
        public string generated_tires { get; set; }
        public int plt { get; set; }
        public int mt { get; set; }
        public int ag_ls { get; set; }
        public int ind { get; set; }
        public int sotr { get; set; }
        public int motr { get; set; }
        public int lotr { get; set; }
        public long gotr { get; set; }
        public string eligible_for_payment { get; set; }
        public string uid { get; set; }
        public int claim_period { get; set; }
        public string registration_number { get; set; }
        public string approve { get; set; }
        public string record_modified { get; set; }
        public string comment { get; set; }
        public string is_duplicate { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_by { get; set; }
        public string device_name { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string modified_by { get; set; }
        public Nullable<int> scan_detail_id { get; set; }
        public string ots_doc_received { get; set; }
    
        public virtual collector_detail_tm_ext collector_detail_tm_ext { get; set; }
    }
}
