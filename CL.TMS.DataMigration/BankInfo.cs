﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DataMigration
{
    public partial class BankInfo
    {
        public string EmailToAddress { get; set; }
        public string EmailCcAddress { get; set; }
        public string SERIES { get; set; }
        public string CustomerVendor_ID { get; set; }
        public string ADRSCODE { get; set; }
        public string VENDORID { get; set; }
        public string CUSTNMBR { get; set; }
        public string EFTUseMasterID { get; set; }
        public string EFTBankType { get; set; }
        public string FRGNBANK { get; set; }
        public string INACTIVE { get; set; }
        public string BANKNAME { get; set; }
        public string EFTBankAcct { get; set; }
        public string EFTBankBranch { get; set; }
        public string GIROPostType { get; set; }
        public string EFTBankCode { get; set; }
        public string EFTBankBranchCode { get; set; }
        public string EFTBankCheckDigit { get; set; }
        public string BSROLLNO { get; set; }
        public string IntlBankAcctNum { get; set; }
        public string SWIFTADDR { get; set; }
        public string CustVendCountryCode { get; set; }
        public string DeliveryCountryCode { get; set; }
        public string BNKCTRCD { get; set; }
        public string CBANKCD { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string ADDRESS4 { get; set; }
        public string RegCode1 { get; set; }
        public string RegCode2 { get; set; }
        public string BankInfo7 { get; set; }
        public string EFTTransitRoutingNo { get; set; }
        public string EFTTransitRoutingNo_Str { get; set; }
        public string CURNCYID { get; set; }
        public string EFTTransferMethod { get; set; }
        public string EFTAccountType { get; set; }
        public string EFTPrenoteDate { get; set; }
        public string EFTTerminationDate { get; set; }
        public string DEX_ROW_ID { get; set; }
        public string VendorIDTM { get; set; }
    }
}
