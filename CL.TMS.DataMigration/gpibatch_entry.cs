//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CL.TMS.DataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class gpibatch_entry
    {
        public gpibatch_entry()
        {
            this.rr_ots_pm_transactions = new HashSet<rr_ots_pm_transactions>();
            this.rr_ots_pm_vendors = new HashSet<rr_ots_pm_vendors>();
            this.rr_ots_rm_cash_receipt = new HashSet<rr_ots_rm_cash_receipt>();
            this.rr_ots_rm_customers = new HashSet<rr_ots_rm_customers>();
            this.rr_ots_rm_transactions = new HashSet<rr_ots_rm_transactions>();
        }
    
        public long gpibatchentry_id { get; set; }
        public long gpibatch_id { get; set; }
        public string gpi_txn_number { get; set; }
        public string gpibatchentry_data_key { get; set; }
        public string gpistatus_id { get; set; }
        public System.DateTime created_date { get; set; }
        public string created_by { get; set; }
        public System.DateTime last_updated_date { get; set; }
        public string last_updated_by { get; set; }
        public string message { get; set; }
        public long int_status { get; set; }
        public Nullable<System.DateTime> int_date { get; set; }
    
        public virtual gpibatch gpibatch { get; set; }
        public virtual gpistatu gpistatu { get; set; }
        public virtual ICollection<rr_ots_pm_transactions> rr_ots_pm_transactions { get; set; }
        public virtual ICollection<rr_ots_pm_vendors> rr_ots_pm_vendors { get; set; }
        public virtual ICollection<rr_ots_rm_cash_receipt> rr_ots_rm_cash_receipt { get; set; }
        public virtual ICollection<rr_ots_rm_customers> rr_ots_rm_customers { get; set; }
        public virtual ICollection<rr_ots_rm_transactions> rr_ots_rm_transactions { get; set; }
    }
}
