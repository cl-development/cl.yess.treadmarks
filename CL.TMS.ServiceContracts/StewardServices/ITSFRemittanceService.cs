﻿using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.Framework.DTO;
using CL.TMS.ServiceContracts.SystemServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.Communication;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections;

namespace CL.TMS.ServiceContracts.StewardServices
{

    public interface ITSFRemittanceService : IGPService
    {
        PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceByFilter(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        PaginationDTO<TSFRemittanceBriefModel, long> GetAllITSFRemittanceByFilterByCustomer(string RegistertionNo, int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, Dictionary<string, string> columnSearchText);
        List<TSFRemittanceBriefModel> LoadRemittanceByRegNo(string searchText, string sortcolumn, string sortdirection, string regNo, Dictionary<string, string> columnSearchText);

        IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod);
        IReadOnlyList<PeriodListModel> GetTSFRateDateList(DateTime PeriodDate);
        TSFRemittanceModel GetTSFRemittanceById(int id, int customerID);
        void AddRemittanceDetail(TSFRemittanceItemModel model);
        void UpdatePenaltiesManually(int id, double penaltiesManually, string modifiedBy, bool? IsPenaltyOverride = false);
        void UpdateHST(int tsfClaimId, string modifiedBy, bool IsTaxApplicable = true);

        void UpdateSupportingDocumentValue(int id, string docOptionValue, string modifiedBy);
        GpResponseMsg UpdatePaymentType(int claimID, string paymentType, string modifiedBy);
        GpResponseMsg UpdateCurrencyType(int claimID, string currencyType, string modifiedBy);
        IReadOnlyList<RateListModel> GetRateList(DateTime reportingPeriod, int itemid);
        RateListModel GetRate(DateTime reportingPeriod, int itemid); //OTSTM2-567
        int AddTSFClaim(TSFRemittanceModel model, ref bool isCreated);
        TSFRemittanceItemModel GetRemittanceDetail(int claimId, int detailId);
        void DeleteRemittanceDetail(int claimId, int detailId);
        void SetRemittanceStatus(int claimID, string status, string updatedBy, long assignedtouser, bool isAssigningToMe);
        decimal? SetRemittanceStatus(int claimID, string status, string updatedBy, string submitted, string received, string deposit, string referenceNo, string amount, string PaymentType, long assignedtouser);
        void SetRemittanceStatusAuto(int claimID, string status, string updatedBy, string amount, long assignedtouser);
        TSFRemittanceModel GetTSFRemittanceByClaimId(int id);
        TSFRemittanceBriefModel GetFirstSubmitRemittance();
        void AddTSFRemittanceNote(TSFRemittanceNote claimNote);
        TSFClaim GetLastApprovedTSFRemittance(int customerID, int claimID);
        bool IsAnyPastPenalty(int customerID, int claimID);
        Customer GetCustomerByCustomerId(int id); //OTSTM2-21

        //OTSTM2-485
        PaginationDTO<TSFClaimInternalAdjustment, int> LoadTSFClaimInternalAdjustments(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId, bool isStaff = false);
        bool AddInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult);
        void RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId);
        TSFClaimAdjustmentModalResult GetClaimAdjustmentModalResult(int internalAdjustType, int internalAdjustId);
        void EditInternalAdjustment(int claimId, TSFClaimAdjustmentModalResult modalResult);
        List<InternalNoteViewModel> GetInternalNotesInternalAdjustment(int ClaimInternalPaymentAdjustId);
        InternalNoteViewModel AddNotesHandlerInternalAdjustment(int ClaimInternalPaymentAdjustId, string notes);
        List<InternalNoteViewModel> ExportToExcelInternalAdjustmentNotes(int ClaimInternalPaymentAdjustId, string searchText, string sortcolumn, bool sortReverse);
        bool HasData(int TSFClaimId);
        bool RemoveTSFClaim(int tsfClaimId);
        IList GetClaimPeriodForBreadCrumb(int customerId);
        IEnumerable GetClaimPeriods(int periodTypeID);
        VendorReference GetVendorReference(int tsfClaimId);
    }
}
