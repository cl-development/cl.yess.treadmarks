﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.StewardServices
{
    public interface IStewardRegistrationInvitationService
    {
        ApplicationInvitation GetApplicationInvitationByEmail(string emailID, string participantType);
        int GetParticipantTypeID(string participantName);
        bool ComposeAndSendEmail(ApplicationInvitation appInvitation);
     //   ApplicationInvitationModel GetByTokenID(Guid guid);
        void Update(ApplicationInvitation appInvitation);
        Guid GetTokenByApplicationId(int applicationId);
        ApplicationInvitation GetByTokenID(Guid guid);
        string GetEmailByApplicationId(int applicationID);
    }
}
