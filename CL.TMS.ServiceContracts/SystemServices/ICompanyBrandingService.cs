﻿using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using System.Collections.Generic;

namespace CL.TMS.ServiceContracts.CompanyBrandingServices
{
    public interface ICompanyBrandingServices
    {
        TermAndConditionVM LoadDetailsByID(int applicationTypeID);
        string LoadTermsAndConditionContent(int? termsAndCondtionsID, string applicationType);
        int GetCurrentTermsAndConditionID(string applicationType);
        bool SaveTermCondition(TermAndConditionVM vm, long userId);    

        InternalNoteViewModel AddNote(int transactionID, string notes);
        List<InternalNoteViewModel> LoadNoteByID(int transactionID);
        List<InternalNoteViewModel> ExportNotesToExcel(int transactionID, bool sortReverse, string sortcolumn, string searchText);

        CompanyInformationVM LoadCompanyInformation();
        void UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath);

        string LoadCompanyLogoUrl();       

    }
}