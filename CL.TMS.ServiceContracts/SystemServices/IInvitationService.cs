﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IInvitationService
    {
        Invitation CreateInvitation(string firstName, string lastName, string emailaddress, string createdBy, string userHostAddress, string redirectUrl = "");

        PaginationDTO<Invitation, int> LoadInvitations(int pageIndex, int pageSize);

        Invitation FindByUserName(string userName);

        int EditUserAndInvitation(string firstName, string lastName, string emailAddress);

        int ConfirmInvitation(string password, string emailAddress, DateTime passwordExpiration, string redirectUrl = "");

        Invitation FindByInvitationGuid(Guid id);

        void InvitationResentEmail(Invitation invitation);

        PaginationDTO<Invitation, int> Search(string searchValue);
    }
}
