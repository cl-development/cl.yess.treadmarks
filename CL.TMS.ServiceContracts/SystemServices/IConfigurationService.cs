﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.GroupRate;
using CL.TMS.DataContracts.ViewModel.Claims;

namespace CL.TMS.ServiceContracts.ConfigurationsServices
{
    public interface IConfigurationsServices
    {
        RateDetailsVM LoadRateDetailsByTransactionID(int RateTransactionID, int category, bool isSpecific);
        PaginationDTO<RateListViewModel, int> LoadRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        PaginationDTO<PIRateListViewModel, int> LoadPIRateList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int categoryId);
        bool AddNewRate(RateDetailsVM newRate, long userId, int category);
        bool updateRate(RateDetailsVM rateVM, long userId);
        bool RemoveRateTransaction(int rateTransactionID, int category);
        bool IsFutureRateExists(int category);
        bool IsFutureRateExists(int category, bool isSpecific);
        bool IsFutureCollectorRateExists(int category);
        InternalNoteViewModel AddTransactionNote(int transactionID, string notes);
        InternalNoteViewModel AddTransactionNote(string transactionID, string notes);
        List<InternalNoteViewModel> LoadRateTransactionNoteByID(int rateTransactionID);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(int parentId, bool sortReverse, string sortcolumn, string searchText);
        List<InternalNoteViewModel> ExportInternalNotesToExcel(string parentId, bool sortReverse, string sortcolumn, string searchText);
        bool IsCollectorRateTransactionExists(int category);
        bool IsFutureCollectorRateTransactionExists(int category);
        AppSettingsVM LoadAppSettings();
        List<InternalNoteViewModel> LoadAppSettingNotesByType(string noteType);

        #region //Account Thresholds
        AccountThresholdsVM LoadAccountThresholds();
        bool UpdateAccountThresholds(AccountThresholdsVM vm);
        bool updateAppSettings(AppSettingsVM appSettingsVM, long userId);
        #endregion

        #region Transaction Thresholds
        TransactionThresholdsVM LoadTransactionThresholds();
        bool UpdateTransactionThresholds(TransactionThresholdsVM transactionThresholdsVM);
        #endregion

        #region Vendor Group
        PaginationDTO<VendorGroupListViewModel, int> LoadVendorGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category);
        List<VendorGroupListViewModel> LoadEffectiveVendorGroupList(DateTime effectiveDate, string category);

        bool AddNewVendorGroup(VendorGroupDetailCommonViewModel newGroup, long userId, string category);
        bool UpdateVendorGroup(VendorGroupDetailCommonViewModel updatedGroup, long userId);
        bool RemoveVendorGroup(int groupId);
        bool VendorGroupUniqueNameCheck(string name, string category);
        bool IsGroupMappingExisting(int id);
        //bool IsGroupMappingExisting(int? RateGroupId, int? GroupId, int? VendorId);
        #endregion
        #region Rate Group
        PaginationDTO<RateGroupListViewModel, int> LoadRateGroupList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string category);
        IEnumerable<ItemModel<string>> GetVendorGroupList(string category);
        IEnumerable<ItemModel<string>> GetAssociatedVendorGroupList(int rateGroupId);
        IEnumerable<ItemModel<int>> LoadVendors(int vendorGroupId, int rateGroupId);
        List<ItemModel<int>> LoadDefaultVendors(int vendorGroupId, string category);
        bool RateGroupUniqueNameCheck(string name, string category);

        bool CreateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId);

        bool UpdateRateGroup(RateGroupViewModel rateGroupViewModel, Int64 userId);
        bool RemoveVendorRateGroupMapping(int RateGroupId);
        //RateGroupViewModel LoadRateGroup(int id);
        //bool DeleteRateGroup(int id);
        #endregion
        List<ItemModel<string>> LoadRateGroups(string category);
        TransportationIncentiveViewModel LoadNewTransportationIncentiveRates(int pickupRateGroupId, int deliveryRateGroupId);
        bool SaveNewTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId);
        TransportationIncentiveViewModel LoadTransportationIncentiveViewModel(int rateTransactionId);
        bool UpdateTransportationIncentiveRates(TransportationIncentiveViewModel transportationIncentiveViewModel, long userId);

        List<RateGroupRate> LoadRateGroupRates(DateTime periodStartDate, DateTime periodEndDate);

        List<VendorRateGroup> LoadVendorRateGroups(int rateGroupId);
        IEnumerable<VendorClaimRateGroupInfo> GetVendorGroupInfoByDateVid(int vendorId, DateTime claimPeriod, DateTime? claimPeriodEnd, string rateGroupCategory);

        #region STC Premium
        PaginationDTO<STCPremiumListViewModel, int> LoadSTCPremiumList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        STCEventsCountViewModel LoadSTCEventsCount();
        STCPremiumListViewModel LoadSTCEventDetailsByEventNumber(string eventNumber);
        bool AddNewSTCEvent(STCPremiumListViewModel newEvent, long userId);
        bool UpdateSTCEvent(STCPremiumListViewModel updatedEvent, long userId);
        bool RemoveSTCEvent(int id, string eventNumber);
        #endregion

        #region TCR Service Threshold
        PaginationDTO<TCRListViewModel, int> LoadTCRServiceThresholds(PagingParamDTO paramDTO);
        TCRServiceThresholdListViewModel LoadTCRServiceThresholdList();
        bool AddTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool UpdateTCRServiceThreshold(TCRServiceThresholdViewModel vm, long userId);
        bool RemoveTCRServiceThresohold(int id, long userId);
        IEnumerable<TCRListViewModel> ExportTCRServiceThresholds();
        #endregion

    }
}
