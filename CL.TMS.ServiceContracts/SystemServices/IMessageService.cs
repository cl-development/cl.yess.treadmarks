﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Announcement;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IMessageService
    {
        #region Notification
        List<Notification> LoadUnreadNotifications(string userName);
        PaginationDTO<NotificationViewModel, int> LoadAllNotifications(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string userName);

        void AddNotification(Notification notification);

        void UpdateNotificationStatus(int notificationId, string newStatus);

        int TotalUnreadNotification(string userName);
        int TotalNotification(string userName);

        NotificationRedirectionViewModel RedirectToClaimSummaryPage(int notificationId);

        void MarkAllAsRead(string userName);

        List<AllNotificationsExportModel> GetAllNotificationsExport(string userName, string searchText, string sortColumn, string sortDirection);
        #endregion

        #region Activity
        void AddActivity(Activity activity);
        PaginationDTO<ActivityViewModel, int> LoadAllActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int claimId);
        PaginationDTO<ActivityViewModel, int> LoadAllCommonActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectId, string ActivityArea, int ActivityType);
        PaginationDTO<ActivityViewModel, int> LoadRecoverableMaterialActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int objectID);
        PaginationDTO<ActivityViewModel, int> LoadRateGroupsActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int activityType);
        List<AllActivitiesExportModel> GetAllActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection);
        List<AllActivitiesExportModel> GetProductCompositionActivitiesExport(string searchText, string sortColumn, string sortDirection);

        int TotalActivity(int claimId);
        //OTSTM2-745
        PaginationDTO<ActivityViewModel, int> LoadAllGPActivities(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        List<AllActivitiesExportModel> GetAllGPActivitiesExport(string searchText, string sortColumn, string sortDirection);
        List<AllActivitiesExportModel> GetAllCommonActivitiesExport(int cid, string searchText, string sortColumn, string sortDirection, string activityArea, int activityType);
        #endregion

        #region //Announcement
        void AddAnnouncement(AnnouncementVM vm);
        void UpdateAnnouncement(AnnouncementVM vm);
        void RemoveAnnouncement(int announcementID);
        AnnouncementVM LoadAnnouncementByID(int announcementID);
        PaginationDTO<AnnouncementListVM, int> LoadAnnouncementList(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection);
        IEnumerable<AnnouncementVM> LoadDashboardAnnouncements();
        #endregion

    }
}
