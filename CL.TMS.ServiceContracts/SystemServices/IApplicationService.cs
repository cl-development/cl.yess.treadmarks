﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IApplicationService
    {
        Application GetByApplicationID(int ID);
        //Application GetByID(int ID);

        Application AddApplication(Application application);

        Application GetSingleApplication(int id);

        void SetApplicationStatus(int applicationId, ApplicationStatusEnum applicationStatus, string denyReasons, string updatedBy, long assignToUser = 0);

        ApplicationEmailModel GetApprovedApplicantInformation(int applicationId);

        IEnumerable<ProcessorListModel> GetProcessorList();
        void UpdateApplicatioExpireDate(int applicationId, int days);
    }
}
