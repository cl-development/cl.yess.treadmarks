﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IGPService
    {
        GpResponseMsg CreateBatch();
    }
}
