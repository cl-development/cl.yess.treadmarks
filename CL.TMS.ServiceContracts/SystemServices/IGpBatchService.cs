﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Framework.DTO;

namespace CL.TMS.ServiceContracts.SystemServices
{
    public interface IGpBatchService
    {
        Object[] GetExport(int gpiBatchId);
        ResponseMsgBase PutOnHold(int gpiBatchEntryId);
        ResponseMsgBase PutOffHold(int gpiBatchEntryId);
        ResponseMsgBase PostBatch(int gpiBatchId, string type);
        ResponseMsgBase RemoveFromBatch(int gpiBatchId);        
        List<GpBatchEntryDto> GetGpTransactionsByBatchId(int gpBatchId);

        List<GpBatchDto> GetGpBatches(string type);

        /// <summary>
        /// updates status of batches that are currently queued
        /// </summary>
        /// <returns></returns>
        void GpStatusCheck();

        PaginationDTO<GPListViewModel, int> GetAllGpBatchesPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, string type);
        PaginationDTO<GPModalListViewModal, int> GetGpTransactionsByBatchIdPagination(int pageIndex, int pageSize, string searchText, string orderBy, string sortDirection, int batchId);

        //OTSMTM2-745
        void AddGPActivityForCreateBatch(int batchId);
    }
}
