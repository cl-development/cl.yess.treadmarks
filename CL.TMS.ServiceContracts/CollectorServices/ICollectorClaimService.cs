﻿using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Collector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.ServiceContracts.CollectorServices
{
    public interface ICollectorClaimService : IGPService
    {
        CollectorClaimSummaryModel LoadCollectorClaimSummary(int claimId);
        void AddTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow);
        void UpdateTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow);
        void RemoveTireOrigin(int claimId, TireOriginItemRow tireOriginItemRow);
        void AddReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow);
        void UpdateReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow);
        void RemoveReuseTires(int claimId, ReuseTiresItemRow reuseTiresItemRow);
        CollectorSubmitClaimViewModel CollectorClaimSubmitBusinessRule(CollectorSubmitClaimViewModel submitClaimModel);

        CollectorClaimInboundReuseTireModel LoadInbound(int claimId);
        CollectorClaimInboundReuseTireModel LoadReuseTires(int claimId);
        bool IsStaffTotalInboundOutbound(int claimId);
        List<ClaimSupportingDocVM> LoadSupportingDocs(int claimId);
        void UpdateSupportingDocOption(int claimId, int defValue, string option);
    }
}
