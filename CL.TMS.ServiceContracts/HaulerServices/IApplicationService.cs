﻿using CL.TMS.DataContracts.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.ServiceContracts.HaulerServices
{
    public interface IApplicationService// : IGenericService<IApplication>
    {
        Application AddApplication(Application application);
        Application GetByID(int ID);
    }
}
