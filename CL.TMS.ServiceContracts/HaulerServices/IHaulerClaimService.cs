﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.ServiceContracts.SystemServices;

namespace CL.TMS.ServiceContracts.HaulerServices
{
    public interface IHaulerClaimService : IGPService
    {
        HaulerClaimSummaryModel LoadClaimSummaryData(int vendorId, int claimId);
        HaulerPaymentViewModel LoadHaulerPayment(int claimId);
        void UpdateYardCountSubmission(int claimId, ItemRow itemRow);
        HaulerSubmitClaimViewModel HaulerClaimSubmitBusinessRule(HaulerSubmitClaimViewModel submitClaimModel);
        List<InventoryItem> LoadInventoryItems(int vendorId, DateTime periodEndDate);
    }
}
