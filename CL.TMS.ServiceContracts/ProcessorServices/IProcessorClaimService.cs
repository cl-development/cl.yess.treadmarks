﻿using CL.TMS.DataContracts.ViewModel.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.ViewModel.GP;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.ViewModel.Configurations;

namespace CL.TMS.ServiceContracts.ProcessorServices
{
    public interface IProcessorClaimService : IGPService
    {
        ProcessorClaimSummaryModel LoadProcessorClaimSummary(int vendorId, int claimId);
        ProcessorSubmitClaimViewModel CheckClaimSubmitBusinessRule(ProcessorSubmitClaimViewModel submitClaimModel);
        RateDetailsVM LoadPIRatesByTransactionID(int RateTransactionID);

    }
}
