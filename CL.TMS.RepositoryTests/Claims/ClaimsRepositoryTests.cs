﻿using CL.Framework.Common;
using CL.TMS.DAL;
using CL.TMS.Repository.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CL.TMS.Repository.Claims.Tests
{
    [TestClass]
    public class ClaimsRepositoryTests
    {
        [TestMethod]
        public void InitializeClaimForActiveVendorsTest()
        {
            var claimRepository = new ClaimsRepository();
            claimRepository.InitializeClaimForActiveVendors();
        }

        [TestMethod]
        public void LoadClaimsForVendorTest()
        {
            //Arrange
            var claimRepository = new ClaimsRepository();

            //Act
            var result = claimRepository.LoadVendorClaims(0, 50, "", "", "", 4514, new Dictionary<string, string>());

            //Assert
            Assert.IsTrue(result.DTOCollection.Count > 0);
        }

        [TestMethod]
        public void LoadInventoryOpeningSummaryTest()
        {
            //Arrange
            var claimRepository = new ClaimsRepository();

            //Act
            var result = claimRepository.LoadInventoryOpeningSummary(4514);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void LoadHaulerClaimItems()
        {
            //Arrange
            var claimRepository = new ClaimsRepository();

            //Act
            var result = claimRepository.LoadHaulerClaimItems(189);

            //Assert
            Assert.IsTrue(result.Count == 8);

        }
        [TestMethod]
        public void GetTopClaimTest()
        {
            //Arrange
            var claimRepository = new ClaimsRepository();

            //Act
            var result = claimRepository.GetTopClaimForMe(259);

            //Assert
            Assert.IsTrue(result != null);

        }
        [TestMethod]
        public void UpdateApplicationCompanyContact()
        {
            var dbContext = new ApplicationBoundedContext();
            var allApplications = dbContext.Applications.ToList();

            allApplications.ForEach(c =>
            {
                if (c.FormObject != null)
                {
                    ///need JsonConvert Custom Deserializer to solve the customize mapping.
                    var sParseVal = new List<string>
                    {
                        "//BusinessLocation/BusinessName",
                        "//ContactInformationList/ContactInformation/FirstName",
                        "//ContactInformationList/ContactInformation/LastName",
                        "//ContactInformationList/ContactInformation/PhoneNumber"
                    };
                    var result = CommonFun.ParseJsonByXpath(c.FormObject, "BusinessLocation", sParseVal);
                    if (result != null)
                    {
                        c.Company = result[0];
                        if (string.IsNullOrWhiteSpace(result[3]))
                            c.Contact = result[1] + " " + result[2];
                        else
                            c.Contact = result[1] + " " + result[2] + "," + result[3];
                    }
                }
            });

            dbContext.SaveChanges();

        }
    }
}