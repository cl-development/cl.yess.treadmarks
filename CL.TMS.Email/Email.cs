﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.Email
{
    public class Email
    {
        static string badPropertyName = "#NO-PROP#";
        private readonly int _smtpPort;
        private readonly string _smtpServer;
        private readonly string _smtpUserName;
        private readonly string _smtpPassword;
        private readonly bool _smtpUseSSL = false;
        private string _dataElementBegin { get; set; }
        private string _dataElementEnd { get; set; }
        private readonly string _defaultEmailTemplateDirectory;
        private readonly string _defaultFrom;

        //OTSTM2-72
        Queue<MailMessage> _messages = new Queue<MailMessage>();

        public Email(int smtpPort, string smtpServer, string smtpUserName, string smtpPassword, string dataElementBegin, string dataElementEnd, string defaultEmailTemplateDirectory, string defaultFrom, bool smtpUseSSL)
        {
            _smtpPort = smtpPort;
            _smtpServer = smtpServer;
            _smtpUserName = smtpUserName;
            _smtpPassword = smtpPassword;
            _dataElementBegin = dataElementBegin;
            _dataElementEnd = dataElementEnd;
            _defaultEmailTemplateDirectory = defaultEmailTemplateDirectory;
            _defaultFrom = defaultFrom;
            _smtpUseSSL = smtpUseSSL;
        }

        public string GetInvitaionParamValue(string paramKey)
        {
            //return dbContext.Settings.FirstOrDefault(t => t.Key.Equals(paramKey)).Value;
            return null;
        }
        //straight send email method.  from, to, cc, and bcc should be loaded with email addresses.  
        public bool SendEmail(string from, string to, string cc, string bcc, string subject, string body, List<AttachmentType> attachments, string secondToEmailAddr = "")
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, body);
            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            if (!string.IsNullOrEmpty(secondToEmailAddr))
            {
                message.To.Add(secondToEmailAddr);
            }
            return SendEmail(message);
        }

        //SendEmail where information from the passed dataObject can be used in the from, to, cc, bcc, subject, and/or body of the message.  Surround the
        //field name from the dataObject in whatever the dataElementBegin and dataElementEnd settings specify..
        public bool SendEmail(string from, string to, string cc, string bcc, string subject, string body, Object dataObject, List<AttachmentType> attachments)
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, body, dataObject);
            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            return SendEmail(message);
        }

        //straight send email method.  from, to, cc, and bcc should be loaded with email addresses.  The body of the email should be UTF8 Encoded.  
        public bool SendEmail(string from, string to, string cc, string bcc, string subject, AttachmentType body, List<AttachmentType> attachments)
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, System.Text.Encoding.UTF8.GetString(body.Content));

            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            return SendEmail(message);
        }

        //SendEmail where information from the passed dataObject can be used in the from, to, cc, bcc, subject, and/or body of the message.  Body of the message in this case is
        //sent via the Content field in the AttachmentType.  
        public bool SendEmail(string from, string to, string cc, string bcc, string subject, AttachmentType body, Object dataObject, List<AttachmentType> attachments)
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, System.Text.Encoding.UTF8.GetString(body.Content), dataObject);
            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            return SendEmail(message);
        }
        //This SendMailFromTemplate call gets the body of the message from a file that is specified after the subject.  We will look for the file given whatever
        //path they sent first, then we will look for the file by applying the defaultEmailTemplateDirectory setting as the next attempt to find the file.  
        //If not in either path, we are throwing an exception back to the client.  
        public bool SendMailFromTemplate(string from, string to, string cc, string bcc, string subject, string filename, Object dataObject, List<AttachmentType> attachments)
        {
            if (!File.Exists(filename))
            {
                string tmpFileName = _defaultEmailTemplateDirectory + filename;
                if (!File.Exists(tmpFileName))
                    throw new FileNotFoundException("Email Service:SendMailFromTemplate cannot find email template file: ", filename);
                filename = tmpFileName;
            }

            StreamReader fileStream = new StreamReader(filename);
            string fileContent = fileStream.ReadToEnd();
            fileStream.Close();

            subject = LoadProperties(subject, dataObject, _dataElementBegin, _dataElementEnd);
            fileContent = LoadProperties(fileContent, dataObject, _dataElementBegin, _dataElementEnd);
            return SendEmail(from, to, cc, bcc, subject, fileContent, dataObject, attachments);
        }

        //this function expects a list of files to attach to the message as well as a file name to use for a template as the body of the email.
        public bool SendMailFromTemplateWithFileAttachment(string from, string to, string cc, string bcc, string subject, string filename, Object dataObject, List<string> fileNamesToAttach)
        {
            if (!File.Exists(filename))
            {
                string tmpFileName = _defaultEmailTemplateDirectory + filename;
                if (!File.Exists(tmpFileName))
                    throw new FileNotFoundException("Email Service:SendMailFromTemplateWithFileAttachment cannot find email template file: ", filename);
                filename = tmpFileName;
            }
            StreamReader fileStream = new StreamReader(filename);
            string fileContent = fileStream.ReadToEnd();
            fileStream.Close();

            subject = LoadProperties(subject, dataObject, _dataElementBegin, _dataElementEnd);
            fileContent = LoadProperties(fileContent, dataObject, _dataElementBegin, _dataElementEnd);

            MailMessage message = GetMessage(from, to, cc, bcc, subject, fileContent);
            if (fileNamesToAttach != null)
            {
                foreach (string file in fileNamesToAttach)
                {
                    string tmpFile = file;
                    if (!File.Exists(tmpFile))
                    {
                        string tmpFileName = _defaultEmailTemplateDirectory + file;
                        if (!File.Exists(tmpFileName))
                            throw new FileNotFoundException("Email Service:SendMailFromTemplateWithFileAttachment cannot find email attachment file: ", file);
                        tmpFile = tmpFileName;
                    }

                    StreamReader tmpStream = new StreamReader(tmpFile);
                    string attachContent = tmpStream.ReadToEnd();
                    tmpStream.Close();

                    string justFileName = tmpFile;
                    if (filename.Contains("\\"))
                        justFileName = tmpFile.Substring(tmpFile.LastIndexOf("\\") + 1);

                    string mimeType = MimeMapping.GetMimeMapping(justFileName);
                    Attachment curAttachment = new Attachment(new MemoryStream(Encoding.ASCII.GetBytes(attachContent)), mimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = justFileName;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = justFileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            return SendEmail(message);
        }

        //in case someone wants to construct their own email message, they can still send it via our common interface.
        public bool SendEmail(MailMessage message)
        {
            SmtpClient emailClient;
            if (String.IsNullOrEmpty(_smtpServer))
                //email is being setup using system.net mailsettings in the web.config file
                //rather than specifying an smtp server.
                emailClient = new SmtpClient();
            else
                emailClient = new SmtpClient(_smtpServer);

            emailClient.Port = _smtpPort;
            if (_smtpUserName != null && !_smtpUserName.Equals(""))
            {
                emailClient.Credentials = new System.Net.NetworkCredential(_smtpUserName, _smtpPassword);
            }
            emailClient.EnableSsl = _smtpUseSSL;
         
            emailClient.Send(message);
            message.Dispose();
            emailClient.Dispose();
            return true;
        }

        private MailMessage GetMessage(string from, string to, string cc, string bcc, string subject, string body)
        {
            MailMessage message = new MailMessage();
            if (from != null)
                message.From = new MailAddress(from);
            if (from == null)
                message.From = new MailAddress(_defaultFrom);
            if (from == null)
                from = _defaultFrom;
            if (to != null)
                message.To.Add(to);
            if (cc != null)
                message.CC.Add(cc);
            if (bcc != null)
                message.Bcc.Add(bcc);
            if (subject != null)
                message.Subject = subject;
            if (body != null)
                message.Body = body;
            message.IsBodyHtml = true;
            return message;
        }

        private MailMessage GetMessage(string from, string to, string cc, string bcc, string subject, string body, Object dataObject)
        {
            MailMessage message = new MailMessage();
            if (from != null && from.Contains(_dataElementBegin))
                from = LoadProperties(from, dataObject, _dataElementBegin, _dataElementEnd);

            if (to != null && to.Contains(_dataElementBegin))
                to = LoadProperties(to, dataObject, _dataElementBegin, _dataElementEnd);

            if (cc != null && cc.Contains(_dataElementBegin))
                cc = LoadProperties(cc, dataObject, _dataElementBegin, _dataElementEnd);

            if (bcc != null && bcc.Contains(_dataElementBegin))
                bcc = LoadProperties(bcc, dataObject, _dataElementBegin, _dataElementEnd);

            if (subject != null && subject.Contains(_dataElementBegin))
                subject = LoadProperties(subject, dataObject, _dataElementBegin, _dataElementEnd);

            if (body != null && body.Contains(_dataElementBegin))
                body = LoadProperties(body, dataObject, _dataElementBegin, _dataElementEnd);

            return GetMessage(from, to, cc, bcc, subject, body);
        }

        //will replace any references to the passed object with the values from that reference in the passed object.
        static public string LoadProperties(string content, Object dataObject, string dataElementBegin, string dataElementEnd)
        {
            while (content.Contains(dataElementBegin))
            {
                int begIdx = content.IndexOf(dataElementBegin);
                int endIdx = content.IndexOf(dataElementEnd);
                string dataElement;
                if (begIdx >= 0 && endIdx > 0)
                {
                    string dataMarker = content.Substring(begIdx, (endIdx - begIdx) + dataElementEnd.Length);
                    dataElement = dataMarker.Replace(dataElementBegin, "");
                    dataElement = dataElement.Replace(dataElementEnd, "");
                    string dataValue = GetPropertyValue(dataObject, dataElement).ToString();
                    content = content.Replace(dataMarker, dataValue);
                }

            }
            return content;
        }

        //the reflection code to deal with getting values based on property name from a data object.
        static object GetPropertyValue(Object fromObject, string propertyName)
        {
            Type objectType = fromObject.GetType();
            PropertyInfo propInfo = objectType.GetProperty(propertyName);
            if (propInfo == null && propertyName.Contains('.'))
            {
                string firstProp = propertyName.Substring(0, propertyName.IndexOf('.'));
                propInfo = objectType.GetProperty(firstProp);
                if (propInfo == null)//property name is invalid
                {
                    return badPropertyName;
                    //throw new ArgumentException(String.Format("Property {0} is not a valid property of {1}.", firstProp, fromObject.GetType().ToString()));
                }
                return GetPropertyValue(propInfo.GetValue(fromObject, null), propertyName.Substring(propertyName.IndexOf('.') + 1));
            }
            else
            {
                return propInfo.GetValue(fromObject, null);
            }
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize((object)this);
        }

        public bool SendEmail(string from, string to, string cc, string bcc, string subject, string body, Object dataObject, List<AttachmentType> attachments, AlternateView alternateView)
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, body, dataObject);

            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }
            message.AlternateViews.Add(alternateView);

            return SendEmail(message);
        }

        //OTSTM2-132 BCC value update
        public bool SendEmailWithSecondBcc(string from, string to, string cc, string bcc, string subject, string body, List<AttachmentType> attachments, AlternateView alternateView,string anotherBccEmailAdd = "")
        {
            MailMessage message = GetMessage(from, to, cc, bcc, subject, body);
            if (attachments != null)
            {
                foreach (AttachmentType curAttach in attachments)
                {
                    string justFileName = curAttach.FileName;
                    if (justFileName.Contains("\\"))
                        justFileName = justFileName.Substring(justFileName.LastIndexOf("\\") + 1);

                    Attachment curAttachment = new Attachment(new MemoryStream(curAttach.Content), curAttach.MimeType);
                    ContentDisposition disposition = curAttachment.ContentDisposition;
                    disposition.FileName = curAttach.FileName;
                    disposition.Size = curAttach.Content.Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    curAttachment.Name = curAttach.FileName;
                    message.Attachments.Add(curAttachment);
                }
            }

            if (!string.IsNullOrEmpty(anotherBccEmailAdd))
            {
                message.Bcc.Add(anotherBccEmailAdd);
            }

            message.AlternateViews.Add(alternateView);

            return SendEmail(message);
        }

        //OTSTM2-72 Email
        public bool SendEmailBulk(string from, List<string> To, string cc, string bcc, string subject, string body, Object dataObject, List<AttachmentType> attachments, AlternateView alternateView)
        {
            SmtpClient emailClient;
            if (String.IsNullOrEmpty(_smtpServer))
                emailClient = new SmtpClient();
            else
                emailClient = new SmtpClient(_smtpServer);

            emailClient.Port = _smtpPort;
            if (_smtpUserName != null && !_smtpUserName.Equals(""))
            {
                emailClient.Credentials = new System.Net.NetworkCredential(_smtpUserName, _smtpPassword);
            }
            emailClient.EnableSsl = _smtpUseSSL;

            Parallel.ForEach(To.AsParallel(), recipient =>
            {
                MailMessage message = new MailMessage(from, recipient)
                {
                    Subject = subject,
                    Body = body
                };
                message.AlternateViews.Add(alternateView);
                try
                {
                    lock (emailClient)
                    {
                        emailClient.Send(message);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });

            emailClient.Dispose();
            return true;
        }
    }
}
