﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.Repository.System;
using CL.TMS.Resources;
using FluentValidation.Validators;

namespace CL.TMS.FluentValidation.Validation
{
    public class UniqueEmailValidator : PropertyValidator
    {
        public UniqueEmailValidator()
            : base(MessageResource.InvitationAlreadyExists)
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var emailAddress = context.PropertyValue as string;
            var invitationRepository = new InvitationRepository();
            var result = invitationRepository.EmailIsExisting(emailAddress);
            return !result;
        }
    }
}
