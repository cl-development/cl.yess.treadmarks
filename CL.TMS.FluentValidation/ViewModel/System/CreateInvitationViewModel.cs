﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.FluentValidation.Validation;
using FluentValidation.Attributes;

namespace CL.TMS.FluentValidation.ViewModel.System
{
    [Validator(typeof(CreateInvitationValidator))]
    public class CreateInvitationViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public bool IsCallCentre { get; set; }
        public bool IsAudit { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsScanningClerk { get; set; }
        public bool IsTSFClerk { get; set; }

        public bool IsClaimsRepresentative { get; set; }
        public bool IsClaimsLead { get; set; }
        public bool IsClaimsSupervisor { get; set; }
        public bool IsClaimsApprover1 { get; set; }
        public bool IsClaimsApprover2 { get; set; }

        public bool IsCollector { get; set; }
        public bool IsHauler { get; set; }
        public bool IsProcessor { get; set; }
        public bool IsRPM { get; set; }

        public bool IsRead { get; set; }

        public bool IsWrite { get; set; }

        public bool IsSubmit { get; set; }

        public bool IsParticipantAdmin { get; set; }
        public string RowversionString { get; set; }
        public string Status { get; set; }
    }
}
