﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using Claim = CL.TMS.DataContracts.DomainEntities.Claim;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class TSFRemittanceBoundedContext : TMBaseContext<TSFRemittanceBoundedContext>
    {
        public TSFRemittanceBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<TSFClaim> TSFClaims { get; set; }
        public DbSet<TSFClaimDetail> TSFClaimDetails { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TSFRemittanceNote> TSFRemittanceNotes { get; set; }
        public DbSet<TSFClaimInternalPaymentAdjustment> TSFClaimInternalPaymentAdjustment { get; set; }  //OTSTM2-485
        public DbSet<TSFClaimInternalAdjustmentNote> TSFClaimInternalAdjustmentNote { get; set; } //OTSTM2-485

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //For performance to ignore the following schema - Update the list when start using this context 
            //modelBuilder.Ignore<Contact>();     

            modelBuilder.Entity<Customer>().HasMany<Address>(r => r.CustomerAddresses).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerAddress"); m.MapLeftKey("CustomerId"); m.MapRightKey("AddressId"); });
            modelBuilder.Entity<Customer>().HasMany<Item>(r => r.Items).WithMany(u => u.Customers).Map(m => { m.ToTable("CustomerItem"); m.MapLeftKey("CustomerId"); m.MapRightKey("ItemId"); });

            modelBuilder.Entity<TSFClaim>().HasMany<Attachment>(r => r.Attachments)
            .WithMany(u => u.tsfClaims)
            .Map(m => { m.ToTable("TSFClaimAttachment"); m.MapLeftKey("ClaimId"); m.MapRightKey("AttachmentId"); });

            //TSFClaim
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalTSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesGst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePostHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePreHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Penalties).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PenaltiesManually).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Interest).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalRemittancePayable).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PaymentAmount).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.BalanceDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Credit).HasPrecision(18, 3);

            //TSFClaimDetail
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFDue).HasPrecision(18, 3);

            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);

            modelBuilder.Entity<TSFClaimInternalPaymentAdjustment>().Property(c => c.AdjustmentAmount).HasPrecision(18, 3); //OTSTM2-485

            //For performance to ignore the following schema
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<VendorStorageSite>();
            modelBuilder.Ignore<InvitationRole>();
            modelBuilder.Ignore<Claim>();
        }
    }
}
