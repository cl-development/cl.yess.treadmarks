﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class CompanyBrandingBoundedContext : TMBaseContext<CompanyBrandingBoundedContext>
    {
        public CompanyBrandingBoundedContext()
        {

        }

        public DbSet<TermsAndConditions> TermAndConditions { get; set; }
        public DbSet<TermsAndConditionsNote> Notes { get; set; }
        public DbSet<User> Users { get; set; }

        //OTSTM2-989
        public DbSet<Setting> AppSettings { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<UserClaimsWorkflow>();
            modelBuilder.Ignore<UserApplicationsWorkflow>(); 
        }
    }
}
