﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using Claim = CL.TMS.DataContracts.DomainEntities.Claim;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class ClaimsBoundedContext : TMBaseContext<ClaimsBoundedContext>
    {
        public ClaimsBoundedContext()
        {
            //For batch claim calculation only
            //((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 3600;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
        }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Period> Periods { get; set; }

        public DbSet<ClaimDetail> ClaimDetails { get; set; }
        public DbSet<Item> Items { get; set; }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<ClaimNote> ClaimNotes { get; set; }
        public DbSet<ClaimProcess>  ClaimProcesses { get; set; }

        public DbSet<Inventory> Inventories { get; set; }

        public DbSet<ClaimInventoryAdjustment> ClaimInventoryAdjustments { get; set; }

        public DbSet<ClaimYardCountSubmission> ClaimYardCountSubmissions { get; set; }

        public DbSet<VendorStorageSite> VendorStorageSites { get; set; }

        public DbSet<ClaimInternalPaymentAdjust> ClaimInternalPaymentAdjusts { get; set; }
        public DbSet<TransactionAdjustment> TransactionAdjustments { get; set; }

        public DbSet<ClaimPaymentSummary> ClaimPaymentSummaries { get; set; }
        public DbSet<ClaimPayment> ClaimPayments { get; set; }

        public DbSet<Attachment> Attachments { get; set; }

        public DbSet<ClaimSummary> ClaimSummaries { get; set; }

        public DbSet<ClaimTireOrigin> ClaimTireOrigins { get; set; }
        public DbSet<ClaimReuseTires> ClaimReuseTires { get; set; }

        public DbSet<TransactionItem> TransactionItems { get; set; }

        public DbSet<ClaimInventoryAdjustItem> ClaimInventoryAdjustItems { get; set; }

        public DbSet<GpiBatchEntry> GpBatchEntries { get; set; }

        public DbSet<ClaimPaymentDetail> ClaimPaymentDetails { get; set; }

        public DbSet<ClaimInventory> ClaimInventories { get; set; }

        public DbSet<ClaimInventoryDetail> ClaimInventoryDetails { get; set; }

        //OTSTM2-270
        public DbSet<ClaimInternalAdjustmentNote> ClaimInternalAdjustmentNotes { get; set; }

        //OTSTM2 - 95
        public DbSet<ClaimSupportingDocument> ClaimSupportingDocuments { get; set; }

        //OTSTM2-1198
        public DbSet<VendorRate> VendorRates { get; set; }

        public DbSet<Rate> Rates { get; set; }

        public DbSet<RateTransactionNote> RateTransactionNotes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Define one-to-one foreign key association
            modelBuilder.Entity<ClaimPaymentSummary>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimPaymentSummary)
                .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimProcess>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimProcess)
                .Map(m => m.MapKey("ClaimId"));

            modelBuilder.Entity<ClaimSummary>()
                .HasRequired(a => a.Claim)
                .WithOptional(u => u.ClaimSummary)
                .Map(m => m.MapKey("ClaimID"));


            modelBuilder.Entity<Vendor>().HasMany<Address>(r => r.VendorAddresses).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorAddress"); m.MapLeftKey("VendorID"); m.MapRightKey("AddressID"); });
            modelBuilder.Entity<Vendor>().HasMany<Item>(r => r.Items).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorItem"); m.MapLeftKey("VendorID"); m.MapRightKey("ItemID"); });

            modelBuilder.Entity<Claim>().HasMany<Attachment>(r => r.Attachments)
                .WithMany(u => u.Claims)
                .Map(m => { m.ToTable("ClaimAttachment"); m.MapLeftKey("ClaimId"); m.MapRightKey("AttachmentId"); });

            modelBuilder.Entity<ClaimPayment>().Property(c => c.Rate).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPayment>().Property(c =>c.Weight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentSummary>().Property(c => c.AmountPaid).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPaymentAdjustment>().Property(c => c.Weight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentAdjustment>().Property(c => c.Rate).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimInventoryAdjustment>().Property(c => c.AdjustmentWeightOnroad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimInventoryAdjustment>().Property(c => c.AdjustmentWeightOffroad).HasPrecision(18, 4);

            modelBuilder.Entity<Inventory>().Property(c => c.Weight).HasPrecision(18, 4);
            modelBuilder.Entity<Inventory>().Property(c => c.ActualWeight).HasPrecision(18, 4);

            modelBuilder.Entity<TransactionItem>().Property(c => c.AverageWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ActualWeight).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledIn).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.ScheduledOut).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.Rate).HasPrecision(18, 4);
            modelBuilder.Entity<TransactionItem>().Property(c => c.RecyclablePercentage).HasPrecision(18, 4);


            modelBuilder.Entity<ClaimInternalPaymentAdjust>().Property(c => c.AdjustmentAmount).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimInventoryAdjustItem>().Property(c => c.WeightAdjustment).HasPrecision(18, 4);

            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleClosingOffRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleClosingOnRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleOpeningOffRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleOpeningOnRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleClosingOffRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleClosingOnRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleOpeningOffRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleOpeningOnRoad).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.TotalOpening).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.TotalClosing).HasPrecision(18, 4);

            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleClosingOffRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleClosingOnRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleOpeningOffRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.EligibleOpeningOnRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleClosingOffRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleClosingOnRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleOpeningOffRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.IneligibleOpeningOnRoadEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.TotalClosingEstimated).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimSummary>().Property(c => c.TotalOpeningEstimated).HasPrecision(18, 4);

            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.Rate).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.ActualWeight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.AverageWeight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimPaymentDetail>().Property(c => c.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountCredit).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.AdjustmentTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.TotalTax).HasPrecision(18, 3);

            modelBuilder.Entity<ClaimInventory>().Property(c => c.Weight).HasPrecision(18, 4);
            modelBuilder.Entity<ClaimInventory>().Property(c => c.ActualWeight).HasPrecision(18, 4);

            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);

            //Fixing Claim concurrency issues
            modelBuilder.Entity<Claim>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimSummary>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimDetail>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimPayment>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimInternalPaymentAdjust>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimInventoryAdjustment>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimInventoryAdjustItem>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimPaymentSummary>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimPaymentAdjustment>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimProcess>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimSummaryAdjustment>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<ClaimNote>().Ignore(t => t.Rowversion);

            //For performance to ignore the following schema
            modelBuilder.Ignore<RolePermission>();
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();

            modelBuilder.Ignore<Asset>();
            modelBuilder.Ignore<VendorActiveHistory>();
            modelBuilder.Ignore<AppUser>();
            modelBuilder.Ignore<BusinessActivity>();
            modelBuilder.Ignore<Application>();

        }
    }
}