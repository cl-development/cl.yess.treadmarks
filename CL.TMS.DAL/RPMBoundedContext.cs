﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DAL
{
    public class RPMBoundedContext: TMBaseContext<RPMBoundedContext>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<RetailerProduct> RetailerProducts { get; set; }
        public DbSet<Retailer> Retailers { get; set; }
        public DbSet<RetailerNote> RetailerNotes { get; set; }
        public DbSet<ProductCategory> ProductCategoris { get; set; }
        public DbSet<CategoryNote> CategoryNotes { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendor>().HasMany<Address>(r => r.VendorAddresses).WithMany(u => u.Vendors).Map(m => { m.ToTable("VendorAddress"); m.MapLeftKey("VendorID"); m.MapRightKey("AddressID"); });

            //For performance to ignore the following schema
            modelBuilder.Ignore<Asset>();
            modelBuilder.Ignore<AppUser>();
            modelBuilder.Ignore<BankInformation>();
            modelBuilder.Ignore<BusinessActivity>();
            modelBuilder.Ignore<VendorStorageSite>();
            modelBuilder.Ignore<Inventory>();
            modelBuilder.Ignore<Item>();
            modelBuilder.Ignore<VendorActiveHistory>();
            modelBuilder.Ignore<VendorSupportingDocument>();

            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            //modelBuilder.Ignore<Contact>();
            modelBuilder.Ignore<Customer>();
            modelBuilder.Ignore<Application>();
            modelBuilder.Ignore<Claim>();
            modelBuilder.Ignore<TSFClaim>();

            base.OnModelCreating(modelBuilder);

        }
    }
}
