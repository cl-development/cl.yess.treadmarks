﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Site.LicenseText')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Site.LicenseText', 'TreadMarks is used under license from Ontario Tire Stewardship');
END
go
