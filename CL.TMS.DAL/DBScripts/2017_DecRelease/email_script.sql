/****** Object:  Table [dbo].[Email]    Script Date: 12/13/2017 1:21:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[HeaderSection] [nvarchar](max) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[ButtonSection] [nvarchar](max) NULL,
	[ButtonLabel] [nvarchar](100) NULL,
	[IsEnabled] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedByID] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedByID] [bigint] NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailElement]    Script Date: 12/13/2017 1:21:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailElement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ElementType] [nvarchar](50) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_EmailElements] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Email] ON 

GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (4, N'PasswordReset', N'Password - Reset Request', N'Reset Your TreadMarks Password', N'Reset Your TreadMarks Password', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align: center;">You have requested to reset your TreadMarks Online password. If you did not request your password to be reset, please notify us immediately by calling the number below.</p>

                                            <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="center">To reset your password, click the <strong>Reset Password</strong> button below. This link will remain active for a limited time.</p>
                                            
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" width="100%" width="100%" width="100%" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            
                                            <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="center">
                                                <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                            </p>

                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>', N'Reset Password', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (20, N'NewApplicationRequest', N'New Application - Request', N'TreadMarks New Application Request', N'TreadMarks New Application Request', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;"><strong>Account Type: @participantType</strong></p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">You have requested an application to become a Program Participant with Ontario Tire Stewardship.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">In order to open an account with us, you must fill out and submit an online application. Click the <strong>Begin Application</strong> button below to proceed to your application. This link will remain active for a limited time.</p>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" width="100%" width="100%" width="100%" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                            </p>
                                            <!--   <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@ExpireDate</strong>.</p>
    -->
                                            <!--OTSTM2-346-->
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on <strong>@expireDate</strong>.</p>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>', N'Begin Application', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (21, N'NewApplicationBacktoApplicant', N'New Application - Back to Applicant', N'TreadMarks Application Requires Your Attention', N'TreadMarks Application Requires Your Attention', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;"><strong>Account Type: @ApplicationType</strong></p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Ontario Tire Stewardship is requesting additional information for your application. Please fill in the highlighted fields and re-submit your application to us for further processing.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">To update your application, click the <strong>Update Application</strong> button below.</p>

                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@ViewApplication" style="font-family:''Arial'',sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Update Application', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (22, N'NewApplicationDenied', N'New Application - Denied', N'Your TreadMarks Application is Denied', N'Your TreadMarks Application is Denied', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #d43f3a!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;"><strong>Application Type: @ApplicationType</strong></p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody><tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Ontario Tire Stewardship has denied your Application to become a Program Participant.</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you would like to register again, please request and complete a new application. If you require assistance with completing an Application, please call us at the number below.</p>
                                    </td>
                                    
                                </tr>                                
                            </tbody></table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your application has been declined for the following reasons:</p>
                                                                    <div>@ValidationErrorMessages</div>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (23, N'ApprovedApplicationSteward', N'Approved Application - Steward', N'Your TreadMarks Steward Application is Approved', N'Your TreadMarks Steward Application is Approved', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody><tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Congratulations! Your Application has been approved and you are now a registered Steward with Ontario Tire Stewardship, under Ontario’s Used Tires Program.</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please note your account number listed above is specific to your legal name and business address. Account numbers are the primary number we use to identify Program Participants and should be used when communicating with us in any correspondence. If any of your business information for this account changes, please ensure to advise us immediately.</p>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    
                                    <td>

                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Stewards have a legislated responsibility to report the types and number of tires supplied into the Ontario market and to remit corresponding Tire Stewardship Fees (TSF). Through these fees alone, collected by OTS, the ongoing collection, storage, transportation, processing, reuse and recycling of used tires is able to continue.</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Stewards have 90 days to report and send payment for tires supplied from September 2009 to the current period from the day they have been notified of the existence of the OTS program.</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Tire Stewardship Fee Remittances can be completed and submitted through your TreadMarks Online account. If you have no new tire sales for a given month, you must still submit a NIL ($0) Remittance.</p>
                                        <p></p><p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Payments pertaining to these Remittances can be made through your banking institution online, or by mailing a cheque to:
                                            <br><strong>
                                                TSF Processing Unit            <br>300 The East Mall, Suite 100
                                                <br>
                                                Toronto, Ontario, M9B 6B7
                                                <br>
                                            </strong>
                                            <br>
                                            For EFT payments, OTS’ banking information is:
                                            <br>
                                            <strong>
                                                Royal Bank of Canada
                                                <br>
                                                290 The West Mall
                                                <br>
                                                Etobicoke Ontario, Canada M9C 1C6
                                            </strong>
                                            <br>
                                            Transit Number: <strong>5422</strong>
                                            <br>
                                            Institution Number: <strong>003</strong>
                                            <br>
                                            Account Number: <strong>1002328</strong>
                                            <br>
                                            Swift Code: <strong>ROYCCAT2</strong>
                                            <br>
                                            <br>
                                            You are almost done! To create your user account and password, click the <strong>
                                                Create User Account
                                            </strong> button below
                                        </p>

                                    </td>
                                    
                                </tr>

                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.RethinkTires.ca</a> for additional program information under the Steward, Program Participant section. The website provides information and training instructions on how and when to file a Remittance, program policies and procedures (Steward Guidebook), and key dates specific to your role.</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information and updates, visit our website or call the number below.</p>
                                    </td>
                                    
                                </tr>                                
                            </tbody></table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>

                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create User Account', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (57, N'ApprovedApplicationHauler', N'Approved Application - Hauler', N'Your TreadMarks Hauler Application is Approved', N'Your TreadMarks Hauler Application is Approved', N'  <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'  <table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Congratulations! Your Application has been approved and you are now a registered Hauler with Ontario Tire Stewardship, under Ontario’s Used Tires Program.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please note your account number listed above is specific to your legal name and business address. Account numbers are the primary number we use to identify Program Participants and should be used when communicating with us in any correspondence. If any of your business information for this account changes, please ensure to advise us immediately.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">All Haulers registered with OTS are required to submit information to us on a monthly basis. Access to this information is provided via your TreadMarks Online account.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">You’re almost done! To create your user account and password, click the <strong>Create User Account</strong> button below.</p>

                                            <p style="background-color:#d43f3a;color:#fff; padding:15px; font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">IMPORTANT: After you have accepted the invitation and logged into TreadMarks, you will be directed to a page where you must provide Banking and EFT information. This information is required in order to receive payments from Ontario Tire Stewardship.</p>
                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.RethinkTires.ca</a> for additional program information under the Hauler, Program Participant section. The website provides information and training instructions on how and when to file a claim, program policies and procedures (Hauler Guidebook), and key dates specific to your role.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information and updates, visit our website or call the number below.</p>

                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create User Account', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (59, N'ApprovedApplicationRPM', N'Approved Application - RPM', N'Your TreadMarks RPM Application is Approved', N'Your TreadMarks RPM Application is Approved', N' <table cellpadding="0" cellspacing="0" width="100%" width="100%" width="100%" width="100%" border="0" align="left" bgcolor="#fff">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>

                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Congratulations! Your Application has been approved and you are now a registered Recycled Product Manufacturer (RPM) with Ontario Tire Stewardship, under Ontario’s Used Tires Program.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please note your account number listed above is specific to your legal name and business address. Account numbers are the primary number we use to identify Program Participants and should be used when communicating with us in any correspondence. If any of your business information for this account changes, please ensure to advise us immediately.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">All RPMs registered with OTS are required to submit information to us on a monthly basis. Access to this information is provided via your TreadMarks Online account.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">You’re almost done! To create your user account and password, click the <strong>Create User Account</strong> button below.</p>

                                            <p style="background-color:#d43f3a;color:#fff; padding:15px; font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">IMPORTANT: After you have accepted the invitation and logged into TreadMarks, you will be directed to a page where you must provide Banking and EFT information. This information is required in order to receive payments from Ontario Tire Stewardship.</p>
                                            
                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.RethinkTires.ca</a> for additional program information under the RPM, Program Participant section. The website provides information and training instructions on how and when to file a claim, program policies and procedures (RPM Guidebook), and key dates specific to your role.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information and updates, visit our website or call the number below.</p>

                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create User Account', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (60, N'BankInformationRejected', N'Bank Information - Rejected', N'Your TreadMarks Banking Information has been Rejected', N'Your TreadMarks Banking Information has been Rejected', N'<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
    <tr>
        <td>
            <br><h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                Registration Number: @RegistrationNumber<br />
                @BusinessName<br />
                @BusinessAddress1,<br />
                @BusinessCity, @BusinessProvinceState, @BusinessCountry<br />
                @BusinessPostalCode
            </p>
        </td>        
    </tr>
</table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Hello there. OTS is rejecting the banking information you submitted for this account. Your banking information was rejected either because your legal/operating name on the cheque does not match our records, or you did not provide us with a voided cheque.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;"><strong>What you need to do next:</strong> The next time you sign into your OTS account you will be prompted to re-enter your banking information online again. First, you will enter your banking information. Next, select the upload option and attach your voided cheque into the supporting documents panel before submitting.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please ensure that the void cheque you provide matches our records. Uploading your void cheque when submitting your banking information will help expedite the review process. You may also fax or mail your voided cheque upon submission. Voided cheques received after 10 days will be rejected.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;padding-left: 50px"><em>If you are not uploading your voided cheque, please fax or mail it to:</em></p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;padding-left: 50px"><strong><em>Fax:</em></strong><em> <br>Attention: Call Centre <br>1-866-884-7372</em></p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;padding-left: 50px"><strong><em>Mail:</em></strong><em> <br>Ontario Tire Stewardship <br>Attention: Call Centre <br>300 The East Mall, Suite 100 <br>Toronto, Ontario, M9B 6B7</em></p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information or assistance, visit our website or call the number below.</p>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (61, N'NewUserAccountInvitation', N'New User Account - Invitation', N'TreadMarks New User Account Invitation', N'TreadMarks New User Account Invitation', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
    <tr>
        <td>
            <br><h1 bgcolor="#8cc547" style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
        </td>
    </tr>
</table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">You have been invited by your organization’s administrator to join TreadMarks!</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">In order to join, you must first create your user account and password. Click the <strong>Create Account</strong> button below to proceed. This link will remain active for a limited time.</p>
                                        </td>
                                        
                                    </tr>               
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>                     
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:8px 35px" align="left">
                                                                        <a href="@siteUrl@InviteGUID" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@ExpireDate</strong>.</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create Account', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (62, N'RemittanceBacktoParticipant', N'Remittance - Back to Participant', N'TreadMarks Remittance Requires Your Attention', N'TreadMarks Remittance Requires Your Attention', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                                        <tr>

                                                            <td>
                                                                <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                                                <h1 style="color: Red !important;font-family: Arial;font-size: 18px;line-height: 150%;text-align:center;font-weight:bold;font-weight:600;">
                                                                    [*EmailTitle*]
                                                                </h1>
                                                                <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;font-weight:bold;">Remittance Period: @MonthYear</p>
                                                                <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                                            </td>

                                                        </tr>
                                                    </table>', N' <table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Ontario Tire Stewardship has requested that you review your Remittance and re-submit.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To view your Remittance, please login to TreadMarks using the <strong>Login</strong> button below.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">If you require assistance with your Remittance please call us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>                                            
                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@LoginURL" style="font-family:''Arial'',sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (63, N'RemittanceSubmitted', N'Remittance - Submitted', N'TreadMarks Remittance Invoice', N'TreadMarks Remittance Invoice', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold; margin-top:30px;">Remittance Period: @MonthYear</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold;">Confirmation Number: @ConfirmationNumber</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold;">Your TSF Balance Due: @BalanceDue</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Thank you for submitting your TSF remittance.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Payment pertaining to this Remittance Invoice can be paid through:</p>
                                            <ul style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">

                                                <li>
                                                    <p>Your banking institution online and referencing your 7-digit OTS registration number as the account number</p>
                                                </li>
                                                <li>
                                                    <p>Electronic fund transfer (EFT)</p>
                                                    <p style="margin-left:20px;">
                                                        <strong>
                                                            Royal Bank of Canada
                                                            <br />
                                                            290 The West Mall
                                                            <br />
                                                            Etobicoke, Ontario M9C 1C6
                                                        </strong>
                                                        <br />
                                                        Transit Number: <strong>
                                                            5422
                                                        </strong>
                                                        <br />
                                                        Institution Number: <strong>
                                                            003
                                                        </strong>
                                                        <br />
                                                        Account Number: <strong>
                                                            1002328
                                                        </strong>
                                                        <br />
                                                        Swift Code: <strong>
                                                            ROYCCAT2
                                                        </strong>
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>By mailing a cheque to:</p>
                                                    <p style="margin-left:20px;">
                                                        <strong>
                                                            Ontario Tire Stewardship
                                                            <br />
                                                            TSF Processing Department
                                                            <br />
                                                            300 The East Mall, Suite 100
                                                            <br />
                                                            Toronto, Ontario, M9B 6B7
                                                        </strong>
                                                    </p>
                                                </li>
                                            </ul>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">If you require assistance with your Remittance please call us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (64, N'RemittanceSubmittedwithCreditDue', N'Remittance - Submitted with Credit Due', N'TreadMarks Remittance Submission', N'TreadMarks Remittance Submission', N' <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold; margin-top:30px;">Remittance Period: @MonthYear</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold;">Confirmation Number: @ConfirmationNumber</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 100%;text-align:center;font-weight:bold;">Your TSF Balance Due: @BalanceDue</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Thank you for submitting your TSF remittance.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your tire credit request (if applicable) will be reviewed alongside the supporting documents. OTS will contact you if any additional information is required.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">If you require assistance with your Remittance please call us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (65, N'InactiveParticipantProcessor', N'Inactive Participant - Processor', N'TreadMarks Has Inactivated a Processor Account', N'TreadMarks Has Inactivated a Processor Account', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">@InactiveRegNo</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveBusinessName</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveAddress</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 350%;text-align:center;font-weight:bold;">Inactive Effective: @InactiveDate</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Dear @ActiveRegType,
                                        </p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">This notice is to inform you that the above account number has been made inactive in our system. There should be no more transactions made with this account on or after the effective date.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (66, N'InactiveParticipantCollector', N'Inactive Participant - Collector', N'TreadMarks Has Inactivated a Collector Account', N'TreadMarks Has Inactivated a Collector Account', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">@InactiveRegNo</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveBusinessName</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveAddress</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 350%;text-align:center;font-weight:bold;">Inactive Effective: @InactiveDate</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Dear @ActiveRegType,
                                        </p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">This notice is to inform you that the above account number has been made inactive in our system. There should be no more transactions made with this account on or after the effective date.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (67, N'InactiveParticipantHauler', N'Inactive Participant - Hauler', N'TreadMarks Has Inactivated a Hauler Account', N'TreadMarks Has Inactivated a Hauler Account', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">@InactiveRegNo</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveBusinessName</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveAddress</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 350%;text-align:center;font-weight:bold;">Inactive Effective: @InactiveDate</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Dear @ActiveRegType,
                                        </p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">This notice is to inform you that the above account number has been made inactive in our system. There should be no more transactions made with this account on or after the effective date.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (68, N'InactiveParticipantRPM', N'Inactive Participant - RPM', N'TreadMarks Has Inactivated a RPM Account', N'TreadMarks Has Inactivated a RPM Account', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">@InactiveRegNo</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveBusinessName</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">@InactiveAddress</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 350%;text-align:center;font-weight:bold;">Inactive Effective: @InactiveDate</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Dear @ActiveRegType,
                                        </p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">This notice is to inform you that the above account number has been made inactive in our system. There should be no more transactions made with this account on or after the effective date.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (70, N'NewTransactionAdded', N'New Transaction - Added', N'A New Transaction in TreadMarks Requires Your Attention', N'A New Transaction in TreadMarks Requires Your Attention', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">Transaction Type: @_TransactionType</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Transaction Number: @_TransactionNumber</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Created By: @_CreatedBy</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Created On: @_CreatedOn</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">A new transaction has been created which requires your approval.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To view and Accept/Reject the transaction, please login to TreadMarks using the <strong>Login</strong> button below.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (71, N'TransactionAdjusted', N'Transaction - Adjusted', N'A Transaction Adjustment in TreadMarks Requires Your Attention', N'A Transaction Adjustment in TreadMarks Requires Your Attention', N' <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold; margin-top:30px;">Transaction Type: @_TransactionType</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Transaction Number: @_TransactionNumber</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Created By: @_InitiatedBy</p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Created On: @_ClaimPeriod</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">A transaction has been adjusted which requires your approval.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To view and Accept/Reject the transaction adjustment, please login to TreadMarks using the <strong>Login</strong> button below.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (72, N'RetailConnectionRetailerApproved', N'Retail Connection - Retailer Approved ', N'Your TreadMarks Retail Connection Retailer Has Been Approved', N'Your TreadMarks Retail Connection Retailer Has Been Approved', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Retailer: @RetailerName</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your Retailer in TreadMarks’ Retail Connection has been approved by OTS. You may now add products for sale at this Retailer within the Retail Connection interface on TreadMarks Online.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To add products to this Retailer, please click the <strong>Login</strong> button below.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (73, N'RetailConnectionProductApproved', N'Retail Connection - Product Approved ', N'Your TreadMarks Retail Connection Product Has Been Approved', N'Your TreadMarks Retail Connection Product Has Been Approved', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                            [*EmailTitle*]
                                        </h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 80%;text-align:center;font-weight:bold;">Product: @ProductName</p>
                                        <br />
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your Product in TreadMarks’ Retail Connection has been approved by OTS. You may now view this product on the RethinkTires App, available in the iOS and Android App Stores.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To add more products in Retail Connection, please click the <strong>Login</strong> button below.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (74, N'ClaimSubmitted', N'Claim - Submitted', N'Your TreadMarks Claim Has Been Submitted and Received', N'Your TreadMarks Claim Has Been Submitted and Received', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                                                    <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                                                        [*EmailTitle*]
                                                                    </h1>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Registration Number: @RegistrationNumber<br>
                                                                        @BusinessName<br>
                                                                        Claim Period: @ClaimPeriod
                                                                    </p>                                                                    
                                                                    <br>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your Claim submission has been received and is in queue to be reviewed by our staff.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br>
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (75, N'ClaimBacktoParticipant', N'Claim - Back to Participant ', N'Your TreadMarks Claim Requires Attention', N'Your TreadMarks Claim Requires Attention', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                                                    <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                                                        [*EmailTitle*]
                                                                    </h1>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Registration Number: @RegistrationNumber<br>
                                                                        @BusinessName<br>
                                                                        Claim Period: @ClaimPeriod
                                                                    </p>                                                                    
                                                                    <br>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Ontario Tire Stewardship has reviewed your Claim and needs more information from you. Please review your Claim and re-submit.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">To view your Claim, please login to TreadMarks using the <strong>Login</strong> button below.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you require assistance with your Claim, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                                                    <br />
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@siteUrl" style="font-family: ''Arial'',sans-serif; padding: 10px 50px; font-size: 16px; color: #362c23; text-align: center; font-weight: bold; line-height: 48px; background: #f4ad69; border-radius: 4px; border: 0px; width: 231px; text-decoration: none; margin: 0 auto;" target="_blank">@ButtonLabel</a>
                                                                    </p>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Login', 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (76, N'ClaimApproved', N'Claim - Approved', N'Your TreadMarks Claim Has Been Approved', N'Your TreadMarks Claim Has Been Approved', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                                                    <h1 style="color: #505050!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">
                                                                        [*EmailTitle*]
                                                                    </h1>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Registration Number: @RegistrationNumber<br>
                                                                        @BusinessName<br>
                                                                        Claim Period: @ClaimPeriod
                                                                    </p>                                                                    
                                                                    <br>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Your Claim submission has been reviewed and approved by our staff.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">If you have any questions regarding this e-mail, please contact us at <span style="white-space:nowrap;">1-888-687-2202</span> or email <a href="mailto:info@rethinktires.ca">info@rethinktires.ca</a>.</p>
                                            <br />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br>
                                                                        @CompanyName
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', NULL, 1, 1, CAST(0x0000A82E00000000 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (105, N'ApprovedApplicationCollector', N'Approved Application - Collector', N'Your TreadMarks Collector Application is Approved', N'Your TreadMarks Collector Application is Approved', N' <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Congratulations! Your Application has been approved and you are now a registered Collector with Ontario Tire Stewardship, under Ontario’s Used Tires Program.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Attached is your Collector QR Code which is to be used to complete transactions at this registered business address only. Please print this sign and have the sign available for completing transactions with a registered OTS Hauler. Do not distribute your QR code. If you are unable to print this attachment, please contact OTS.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please note your account number listed above is specific to your legal name and business address. Account numbers are the primary number we use to identify Program Participants and should be used when communicating with us in any correspondence. If any of your business information for this account changes, please ensure to advise us immediately.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">All Collectors registered with OTS are required to submit information to us on a monthly basis. Access to this information is provided via your TreadMarks Online account.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">You’re almost done! To create your user account and password, click the <strong>Create User Account</strong> button below.</p>

                                            <p style="background-color:#d43f3a;color:#fff; padding:15px; font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">IMPORTANT: After you have accepted the invitation and logged into TreadMarks, you will be directed to a page where you must provide Banking and EFT information. This information is required in order to receive payments from Ontario Tire Stewardship.</p>

                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.RethinkTires.ca</a> for additional program information under the Collector, Program Participant section. The website provides information and training instructions on how and when to file a claim, program policies and procedures (Collector Guidebook), and key dates specific to your role.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information and updates, visit our website or call the number below.</p>

                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create User Account', 1, 1, CAST(0x0000A83200A11618 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (106, N'ApprovedApplicationProcessor', N'Approved Application - Processor', N'Your TreadMarks Processor Application is Approved', N'Your TreadMarks Processor Application is Approved', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">@Date</p>
                                        <h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                <tbody>
                                    <tr>
                                        
                                        <td>                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Congratulations! Your Application has been approved and you are now a registered Processor with Ontario Tire Stewardship, under Ontario’s Used Tires Program.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please note your account number listed above is specific to your legal name and business address. Account numbers are the primary number we use to identify Program Participants and should be used when communicating with us in any correspondence. If any of your business information for this account changes, please ensure to advise us immediately.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">All Processors registered with OTS are required to submit information to us on a monthly basis. Access to this information is provided via your TreadMarks Online account.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">You’re almost done! To create your user account and password, click the <strong>Create User Account</strong> button below.</p>

                                            <p style="background-color:#d43f3a;color:#fff; padding:15px; font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">IMPORTANT: After you have accepted the invitation and logged into TreadMarks, you will be directed to a page where you must provide Banking and EFT information. This information is required in order to receive payments from Ontario Tire Stewardship.</p>
                                            
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.RethinkTires.ca</a> for additional program information under the Processor, Program Participant section. The website provides information and training instructions on how and when to file a claim, program policies and procedures (Processor Guidebook), and key dates specific to your role.</p>
                                            <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">For more information and updates, visit our website or call the number below.</p>

                                        </td>
                                        
                                    </tr>                                    
                                </tbody>
                            </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#ffffff" style="margin-top:-15px;">
                                                        <tbody>
                                                            <tr>

                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                        Regards,<br />
                                                                        @CompanyName
                                                                    </p>
                                                                    <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                        <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>', N'Create User Account', 1, 1, CAST(0x0000A83200A3A094 AS DateTime), 338, NULL, NULL)
GO
INSERT [dbo].[Email] ([ID], [Name], [DisplayName], [Subject], [Title], [HeaderSection], [Body], [ButtonSection], [ButtonLabel], [IsEnabled], [SortOrder], [CreatedDate], [CreatedByID], [ModifiedDate], [ModifiedByID]) VALUES (107, N'WelcomeLetterPrimaryContact', N'Welcome Letter - Primary Contact', N'Welcome to TreadMarks!', N'Welcome to TreadMarks!', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="background-color:#ffffff !important">
                                <tr>
                                    
                                    <td>
                                        <br><h1 style="color: #8cc547!important;font-family: Arial;font-size: 18px;line-height: 180%;text-align:center;font-weight:bold;font-weight:600;">[*EmailTitle*]</h1>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                            Registration Number: @RegistrationNumber<br />
                                            @BusinessName<br />
                                            @BusinessAddress1,<br />
                                            @BusinessCity, @BusinessProvinceState,<br />
                                            @BusinessPostalCode, @BusinessCountry
                                        </p>
                                        <p style="color: #505050;font-family: Arial;font-size: 15px;">Dear @BusinessLegalName,</p>
                                    </td>
                                    
                                </tr>
                            </table>', N'<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:justify;">
                                                                        From all of us here at Ontario Tire Stewardship, welcome to <strong> TreadMarks Online! </strong>
                                                                    </p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:justify;">You will use TreadMarks Online to access your Remittances, Transactions, Claims and Applications. You will also be able to access and manage user permissions on your account.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:justify;">You’re almost ready! To activate your account and create a password, click the <strong>Accept Invitation</strong> button below.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:justify;">Please visit our website at <a href="http://www.RethinkTires.ca/">www.rethinktires.ca</a> for additional program information. The website provides information and training instructions on how and when to file, program policies and procedures, and key dates specific to your role in the Used Tire Program.</p>
                                                                    <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:justify;">For more information and updates, visit us at <a href="http://www.RethinkTires.ca/">www.rethinktires.ca</a> or call us toll-free at 1-888-687-2202.</p>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>', N'<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left" bgcolor="#fff" style="margin-top:-17px;">
                                                        <tr>

                                                            <td>
                                                                <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;">
                                                                    Regards,<br />
                                                                    @CompanyName
                                                                </p>
                                                                <p style="color:#362c23;font-family:''Arial'',sans-serif;text-align:center;line-height:19px;font-size:14px;margin:0 0 20px;padding:0px 35px" align="left">
                                                                    <a href="@AcceptInvitationUrl" target="_blank" style="font-family:Arial,sans-serif;padding:10px 50px;font-size:16px;color:#362c23;text-align:center;font-weight:bold;line-height:48px;background:#f4ad69;border-radius:4px;border:0px;width:231px;text-decoration:none;margin:0 auto;">@ButtonLabel</a>
                                                                </p>
                                                                <p style="color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:center;">The invitation expires on: <strong>@InvitationExpirationDateTime</strong>.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>', N'Accept Invitation', 1, 1, CAST(0x0000A83200D06292 AS DateTime), 338, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Email] OFF
GO
SET IDENTITY_INSERT [dbo].[EmailElement] ON 

GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (1, N'Container', N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width" />
    <title>[*PageTitle*]</title>
    <style type="text/css">
        /***********
        Originally based on The MailChimp Reset from Fabio Carneiro, MailChimp User Experience Design
        More info and templates on Github: https://github.com/mailchimp/Email-Blueprints
        http://www.mailchimp.com &amp; http://www.fabio-carneiro.com

        INLINE: Yes.
        ***********/
        /* Client-specific Styles */
        #outlook a {
            padding: 0;
        }
        /* Force Outlook to provide a "view in browser" menu link. */
        body {
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }
        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
        .ExternalClass {
            width: 100%;
        }
            /* Force Hotmail to display emails at full width */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }
        /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
        #backgroundTable {
            margin: 0;
            padding: 0;
            width: 100% !important;
            line-height: 100% !important;
        }
        /* End reset */

        /* Some sensible defaults for images
        1. "-ms-interpolation-mode: bicubic" works to help ie properly resize images in IE. (if you are resizing them using the width and height attributes)
        2. "border:none" removes border when linking images.
        3. Updated the common Gmail/Hotmail image display fix: Gmail and Hotmail unwantedly adds in an extra space below images when using non IE browsers. You may not always want all of your images to be block elements. Apply the "image_fix" class to any image you need to fix.

        Bring inline: Yes.
        */
        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a img {
            border: none;
        }

        .image_fix {
            display: block;
        }

        /** Yahoo paragraph fix: removes the proper spacing or the paragraph (p) tag. To correct we set the top/bottom margin to 1em in the head of the document. Simple fix with little effect on other styling. NOTE: It is also common to use two breaks instead of the paragraph tag but I think this way is cleaner and more semantic. NOTE: This example recommends 1em. More info on setting web defaults: http://www.w3.org/TR/CSS21/sample.html or http://meiert.com/en/blog/20070922/user-agent-style-sheets/

        Bring inline: Yes.
        **/
        p {
            margin: 0.6em 0;
        }

        /** Hotmail header color reset: Hotmail replaces your header color styles with a green color on H2, H3, H4, H5, and H6 tags. In this example, the color is reset to black for a non-linked header, blue for a linked header, red for an active header (limited support), and purple for a visited header (limited support).  Replace with your choice of color. The !important is really what is overriding Hotmail''''s styling. Hotmail also sets the H1 and H2 tags to the same size.

        Bring inline: Yes.
        **/
        h1, h2, h3, h4, h5, h6 {
            color: black !important;
        }

            h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
                color: blue !important;
            }

                h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active {
                    color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
                }

                h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
                    color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
                }

        /** Outlook 07, 10 Padding issue: These "newer" versions of Outlook add some padding around table cells potentially throwing off your perfectly pixeled table.  The issue can cause added space and also throw off borders completely.  Use this fix in your header or inline to safely fix your table woes.

        More info: http://www.ianhoar.com/2008/04/29/outlook-2007-borders-and-1px-padding-on-table-cells/
        http://www.campaignmonitor.com/blog/post/3392/1px-borders-padding-on-table-cells-in-outlook-07/

        H/T @edmelly

        Bring inline: No.
        **/
        table td {
            border-collapse: collapse;
        }

        /** Remove spacing around Outlook 07, 10 tables

        More info : http://www.campaignmonitor.com/blog/post/3694/removing-spacing-from-around-tables-in-outlook-2007-and-2010/

        Bring inline: Yes
        **/
        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email, bring your styles inline.  Your link colors will be uniform across clients when brought inline.

        Bring inline: Yes. */
        a {
            color: orange;
        }

        /* Or to go the gold star route...
        a:link { color: orange; }
        a:visited { color: blue; }
        a:hover { color: green; }
        */

        /***************************************************
        ****************************************************
        MOBILE TARGETING

        Use @media queries with care.  You should not bring these styles inline -- so it''''s recommended to apply them AFTER you bring the other stlying inline.

        Note: test carefully with Yahoo.
        Note 2: Don''''t bring anything below this line inline.
        ****************************************************
        ***************************************************/

        /* NOTE: To properly use @media queries and play nice with yahoo mail, use attribute selectors in place of class, id declarations.
        table[class=classname]
        Read more: http://www.campaignmonitor.com/blog/post/3457/media-query-issues-in-yahoo-mail-mobile-email/
        */
        @media only screen and (max-device-width: 480px) {

            /* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.

            Inspired by Campaign Monitor''''s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.

            Step 1 (Step 2: line 224)
            */
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: black; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: orange !important; /* or whatever your want */
                pointer-events: auto;
                cursor: default;
            }
        }

        /* More Specific Targeting */

        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            /* You guessed it, ipad (tablets, smaller screens, etc) */

            /* Step 1a: Repeating for the iPad */
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: blue; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: orange !important;
                pointer-events: auto;
                cursor: default;
            }
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 2) {
            /* Put your iPhone 4g styles in here */
        }

        /* Following Android targeting from:
        http://developer.android.com/guide/webapps/targeting.html
        http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/  */
        @media only screen and (-webkit-device-pixel-ratio:.75) {
            /* Put CSS for low density (ldpi) Android layouts in here */
        }

        @media only screen and (-webkit-device-pixel-ratio:1) {
            /* Put CSS for medium density (mdpi) Android layouts in here */
        }

        @media only screen and (-webkit-device-pixel-ratio:1.5) {
            /* Put CSS for high density (hdpi) Android layouts in here */
        }
        /* end Android targeting */
    </style>

    <!-- Targeting Windows Mobile -->
    <!--[if IEMobile 7]>
    <style type="text/css">

    </style>
    <![endif]-->
    <!-- ***********************************************
    ****************************************************
    END MOBILE TARGETING
    ****************************************************
    ************************************************ -->
    <!--[if gte mso 9]>
    <style>
        /* Target Outlook 2007 and 2010 */
    </style>
    <![endif]-->
</head>
<body bgcolor="#f0f0ed" style="background-color:#f0f0ed !important">
    <!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
    <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor="#f0f0ed" style="background-color:#f0f0ed !important">
        <tr>
            <td>
                <!-- Container -->
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td>
                            [*TopHeader*]
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- TreadMarks Branding -->
                            [*Header*]

                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" style="background-color:#ffffff !important">
                                <tbody>
                                    <tr>
                                        <td width="40">&nbsp;</td>
                                        <td width="520" valign="center">
                                            [*HeaderSection*]

                                            <!-- Body -->
                                            [*Body*]

                                            <!-- ButtonSection -->
                                            [*ButtonSection*]
                                        </td>
                                        <td width="40">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- Footer -->
                            [*Footer*]

                        </td>
                    </tr>
                </table>

                <!-- End example table -->
                <!-- Yahoo Link color fix updated: Simply bring your link styling inline. -->
                <!--<a href="http://htmlemailboilerplate.com" target ="_blank" title="Styling Links" style="color: orange; text-decoration: none;">Coloring Links appropriately</a>-->
                <!-- Gmail/Hotmail image display fix: Gmail and Hotmail unwantedly adds in an extra space below images when using non IE browsers.  This can be especially painful when you putting images on top of each other or putting back together an image you spliced for formatting reasons.  Either way, you can add the ''''image_fix'''' class to remove that space below the image.  Make sure to set alignment (don''''t use float) on your images if you are placing them inline with text.-->
                <!--<img class="image_fix" src="full path to image" alt="Your alt text" title="Your title text" width="x" height="x" />-->
                <!-- Step 2: Working with telephone numbers (including sms prompts).  Use the "mobile_link" class with a span tag to control what number links and what doesn''''t in mobile clients. -->
                <!--<span class="mobile_link">123-456-7890</span>-->
            </td>
        </tr>
    </table>
    <!-- End of wrapper table -->
</body>
</html>', CAST(0x0000A82500B4A05D AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (2, N'TopHeader', N'<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" bgcolor="#8cc547" style="background-color:#8cc547 !important">
                                <tr bgcolor="#8cc547" style="background-color:#8cc547 !important">
                                    <td align="left" bgcolor="#8cc547" style="background-color:#8cc547 !important; padding:1%">
                                        [*CompanyLogo*]
                                    </td>
[*FacebookLogo*]
 [*TwitterLogo*]
                                                                   </tr>
                            </table>', CAST(0x0000A82500B4A061 AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (3, N'Header', N'<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#fff" style="background-color:#ffffff !important">
    <tr>
        <td valign="center" bgcolor="#4f4f4f" style="background-color:#4f4f4f !important" align="center" height="90" colspan="3"><img class="image_fix" src="cid:@ApplicationLogo" width="250" height="60" style="margin-top:10px;" /></td>
    </tr>
</table>
', CAST(0x0000A82500B4A061 AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (4, N'Footer', N'<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#8cc547" style="background-color:#8cc547 !important">
                                        <tbody>
                                            <tr>
                                                <td colspan="5" valign="bottom" align="center">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="30">&nbsp;</td>
                                                <td>
                                                    <p style="color: #ffffff;font-family:Arial;font-size:11px;line-height:150%;text-align:left;">
                                                        <strong>@CompanyName</strong><br>
                                                        @AddressLine1, @AddressLine2<br>
                                                        @CompanyCity, @CompanyProvince @CompanyPostalCode
                                                    </p>
                                                </td>
                                                <td width="40"></td>
                                                <td width="180" valign="bottom">
                                                    <p style="color: #ffffff;font-family:Arial;font-size:14px;line-height:140%;text-align:right;">
                                                        Questions? Need Help?<br>
                                                        <span class="mobile_link" style="font-size:19px;">@CompanyPhone</span>
                                                    </p>
                                                </td>
                                                <td width="30">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="30">&nbsp;</td>
                                                <td colspan="3">
                                                    <p style="color: #ffffff;font-family:Arial;font-size:10px;line-height:120%;text-align:left;">
                                                        © @NewDatetimeNowYear – @CompanyName – All Rights Reserved
                                                    </p>
                                                </td>
                                                <td width="30">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>', CAST(0x0000A82500B4A061 AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (10, N'CompanyLogo', N'<a href="@WebsiteURL" target="_blank"><img src="cid:@CompanyLogo" border="0" height="50" width="168" /></a>', CAST(0x0000A6EE00000000 AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (12, N'FacebookLogo', N'<td width="40" valign="middle" height="40" align="left" target="_blank" bgcolor="#8cc547" style="background-color:#8cc547 !important"><a href="@FacebookURL" target="_blank"><img class="image_fix" src="cid:@FacebookLogo" border="0" height="28" width="28"  style="margin-right:8px"/></a></td>', CAST(0x0000A6EE00000000 AS DateTime))
GO
INSERT [dbo].[EmailElement] ([ID], [ElementType], [Content], [CreatedDate]) VALUES (13, N'TwitterLogo', N'<td width="40" valign="middle" height="40" align="left" target="_blank" bgcolor="#8cc547" style="background-color:#8cc547 !important"><a href="@TwitterURL" target="_blank"><img class="image_fix" src="cid:@TwitterLogo" border="0" height="28" width="28"  style="margin-top:2px;margin-right:8px"/></a></td>', CAST(0x0000A6EE00000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[EmailElement] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_Name]    Script Date: 12/13/2017 1:21:56 PM ******/
ALTER TABLE [dbo].[Email] ADD  CONSTRAINT [uq_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Email]  WITH CHECK ADD  CONSTRAINT [FK_Email_CreatedByUser] FOREIGN KEY([CreatedByID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Email] CHECK CONSTRAINT [FK_Email_CreatedByUser]
GO
