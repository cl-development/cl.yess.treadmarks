﻿USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetRegistrantSpotlights]    Script Date: 12/8/2017 10:57:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_GetRegistrantSpotlights] 
	@keyWords nvarchar(255),@section nvarchar(50), @pageLimit int
AS
BEGIN
	DECLARE @wordCount int = (SELECT count(*) FROM [dbo].[fnSplitSpotlight](@keyWords, ' '));
	DECLARE @moduleType nvarchar(50);
	DECLARE @sectionOrder int;
  
	DECLARE @topCount bigint =  0x7fffffffffffffff;
		
	DECLARE @tmp TABLE (
		ID int,
		vendorID int,        
		PeriodStartDate datetime,        
		PlaceHolder1 nvarchar(100),
		PlaceHolder2 nvarchar(65),
		PlaceHolder3 nvarchar(65),
		PlaceHolder4 nvarchar(65),
		PlaceHolder5 nvarchar(65),
		ModuleType int,
		[Status] nvarchar(50),
		Section nvarchar(50),
		SortOrder int		

	);

	DECLARE @result TABLE (
		ID int,
		vendorID int,        		
		PeriodStartDate datetime,        
		PlaceHolder1 nvarchar(100),
		PlaceHolder2 nvarchar(65),
		PlaceHolder3 nvarchar(65),
		PlaceHolder4 nvarchar(65),
		PlaceHolder5 nvarchar(65),
		ModuleType int,
		[Status] nvarchar(50),
		Section nvarchar(50),
		SortOrder int		
	);

	DECLARE MY_CURSOR CURSOR 
	  LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
	SELECT DISTINCT ModuleType, SortOrder
	FROM @tmp
	ORDER BY SortOrder;



	INSERT INTO @tmp ([ID], vendorID, PeriodStartDate,
		 PlaceHolder1, PlaceHolder2, PlaceHolder3, PlaceHolder4, PlaceHolder5,
		 ModuleType, [Status], Section, SortOrder)
	SELECT *
	FROM
	(
		SELECT top (@topCount)
			v.ID 'ID',
			v.ID 'vendorID',
			v.DateReceived PeriodStartDate,
			v.Number 'PlaceHolder1',
			v.BusinessName 'PlaceHolder2',
			v.OperatingName	'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			v.VendorType 'ModuleType',
			CONVERT(VARCHAR(1), v.IsActive) 'Status',
			'Applications' 'Section',
			4 SortOrder
		FROM [dbo].[Vendor] v 
		WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Applications' or tblWords.splitdata='All')>0)
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount and @wordCount > 0) OR
		v.Keywords like '%'+@keyWords+'%') order by v.IsActive desc, v.number

		UNION All  

		SELECT top (@topCount)
			c.ID 'ID',
			c.ID 'vendorID',
			c.DateReceived PeriodStartDate,			
			c.RegistrationNumber 'PlaceHolder1',
			c.BusinessName 'PlaceHolder2',
			c.OperatingName	'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			c.CustomerType 'ModuleType',
			CONVERT(VARCHAR(1), c.IsActive) 'Status',
			'Applications' 'Section',
			4 SortOrder
		FROM [dbo].[Customer] c
		WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Applications' or tblWords.splitdata='All')>0)
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.Keywords like '%' + tblWords.splitData + '%'
		) = @wordCount AND @wordCount > 0) OR
		c.Keywords like '%'+@keyWords+'%') order by c.IsActive desc, c.RegistrationNumber

		UNION All

			SELECT top (@topCount)
			c.ID 'ID',
			c.ParticipantID 'vendorID',	
			p.StartDate PeriodStartDate,					
			v.number 'PlaceHolder1',
			p.shortname 'PlaceHolder2',
			FORMAT(coalesce(c.ClaimsAmountTotal, 0), N'C', N'en-ca') 'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			c.Claimtype 'ModuleType',
			c.Status 'Status',
			'Claims' 'Section',
			1 SortOrder
			FROM [dbo].Claim c
			inner join Vendor v
			on v.id=c.participantid
			inner join Period p
			on p.id=c.claimperiodid

			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Claims' or tblWords.splitdata='All')>0) 
			AND (((	SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.number like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			v.number like '%'+@keyWords+'%') order by PeriodStartDate desc
		
		UNION All

			SELECT top (@topCount)
			c.ID 'ID',
			c.registrationnumber 'vendorID',	
			p.StartDate PeriodStartDate,										
			c.registrationnumber 'PlaceHolder1',
			p.ShortName 'PlaceHolder2',
			FORMAT(coalesce(c.totaltsfdue - coalesce(c.credit, 0) + coalesce(c.adjustmenttotal, 0), 0), N'C', N'en-ca') 'PlaceHolder3',
			'' 'PlaceHolder4',
			'' 'PlaceHolder5',
			7 'ModuleType',
			c.RecordState 'Status',
			'Remittances' 'Section',
			2 SortOrder
			FROM [dbo].TSFClaim c
			inner join Period p
			on p.id=c.periodid
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Remittances' or tblWords.splitdata='All')>0)
			AND ((( SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.registrationnumber like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			c.registrationnumber like '%'+@keyWords+'%') order by p.startdate desc, c.registrationnumber			
	
		UNION ALL

		SELECT distinct --top 1000
			u.ID 'ID',
			v.ID 'vendorID',		
			u.CreateDate 'PeriodStartDate',									
			concat(u.firstname, ' ', u.lastname) 'PlaceHolder1',
			u.email 'PlaceHolder2',			
			v.number 'PlaceHolder3',
			v.operatingname 'PlaceHolder4',
			'' 'PlaceHolder5',
			6 'ModuleType',
			--CONVERT(VARCHAR(1), vu.IsActive) 'Status',
			--CONVERT(VARCHAR(1), vu_status.IsActive) 'Status',
			CASE 
				WHEN vu_status.IsActive = 1 
				THEN 
					CASE
						WHEN u.IsLocked = 1
						THEN 'Locked'
						ELSE 'Active'
					END
				ELSE 'Inactive' 
			END AS 'Status',
			'Users' 'Section',
			5 SortOrder
			from [User] u 
			
			--inner join vendoruser vu
			--on vu.userid=u.id
			--Remove this after fixing bug in vendoruser (multiple IsActive rows)
			inner join (select VendorID, UserID from vendoruser group by VendorID, UserID) vu
			on vu.UserID=u.id
			
			inner join vendor v
			on vu.vendorid=v.id
			
			CROSS APPLY (select top 1 IsActive from vendoruser where UserID=u.id and VendorID=v.id) vu_status 
			
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Users' or tblWords.splitdata='All')>0)
			AND (((SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE v.number like '%' + tblWords.splitData + '%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			v.number like '%'+@keyWords+'%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%'+@keyWords+'%')

	UNION ALL

	SELECT distinct --top 1000
			u.ID 'ID',
			c.ID 'vendorID',		
			u.CreateDate 'PeriodStartDate',									
			concat(u.firstname, ' ', u.lastname) 'PlaceHolder1',
			u.email 'PlaceHolder2',			
			c.registrationnumber 'PlaceHolder3',
			c.operatingname 'PlaceHolder4',
			'' 'PlaceHolder5',
			6 'ModuleType',
			--CONVERT(VARCHAR(1), cu.IsActive) 'Status',
			--CONVERT(VARCHAR(1), cu_status.IsActive) 'Status',
			CASE 
				WHEN cu_status.IsActive = 1 
				THEN 
					CASE
						WHEN u.IsLocked = 1
						THEN 'Locked'
						ELSE 'Active'
					END
				ELSE 'Inactive' 
			END AS 'Status',
			'Users' 'Section',
			5 SortOrder
			from [User] u 
			
			--inner join CustomerUser cu
			--on cu.userid=u.id
			--Remove this after fixing bug in vendoruser (multiple IsActive rows)
			inner join (select customerid, UserID from CustomerUser group by customerid, UserID) cu
			on cu.UserID=u.id
			
			inner join Customer c
			on cu.customerid=c.id
			
			CROSS APPLY (select top 1 IsActive from CustomerUser where UserID=u.id and customerid=c.id) cu_status 
		
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Users' or tblWords.splitdata='All')>0)
			AND (((SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE c.registrationnumber like '%' + tblWords.splitData + '%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			c.registrationnumber like '%'+@keyWords+'%' 
			or concat(u.FirstName, ' ', u.LastName, ' ', u.email) like '%'+@keyWords+'%')
		
		UNION ALL

		SELECT top 2000
			t.ID 'ID',
			isnull(isnull(vi.ID,vo.id), 0) 'vendorID',
			t.transactiondate 'PeriodStartDate',											
			cast(t.friendlyid as nvarchar(10)) 'PlaceHolder1',
			t.transactiontypecode 'PlaceHolder2',			
			FORMAT (t.transactiondate, 'yyyy-MM-dd') 'PlaceHolder3',
			isnull(isnull(t.badge, vi.number),'') 'PlaceHolder4',
			isnull(vo.number,'') 'PlaceHolder5',
			7 'ModuleType',
			t.status 'Status',
			'Transactions' 'Section',
			3 SortOrder
			from [Transaction] t 
			left outer join vendor vi
			on t.incomingid=vi.id
			left outer join vendor vo
			on t.outgoingid=vo.id
			WHERE ((SELECT count(*) IsToBeSearched FROM [dbo].[fnSplitSpotlight](@section, ' ') tblWords
			where tblWords.splitdata='Transactions' or tblWords.splitdata='All')>0) 
			AND ((( SELECT count(*)
			FROM [dbo].[fnSplitSpotlight](@keyWords, ' ') tblWords
			WHERE concat(t.friendlyid, ' ', t.badge) like '%' + tblWords.splitData + '%'
			) = @wordCount AND @wordCount > 0) OR
			concat(t.friendlyid,' ', t.badge) like '%'+@keyWords+'%') order by t.TransactionDate desc, t.FriendlyId

	) main
	
	--ORDER BY main.Section, main.ModuleType;

	IF @pageLimit <= 0
	BEGIN
		SET @pageLimit = (SELECT count(*) FROM @tmp);
	END


	OPEN MY_CURSOR
	FETCH NEXT FROM MY_CURSOR INTO @moduleType, @sectionOrder
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		INSERT INTO @result
		SELECT TOP (@pageLimit) t.*
		FROM @tmp t
		WHERE t.ModuleType = @moduleType and t.SortOrder = @sectionOrder
		--ORDER BY t.[Status] desc, t.PlaceHolder1
		;

		FETCH NEXT FROM MY_CURSOR INTO @moduleType, @sectionOrder
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR

	SELECT *
	FROM @result;

END

GO

