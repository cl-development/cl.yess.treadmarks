﻿USE tmdb
go

--user access permission control for Admin/System Thresholds -- update from Account Thresholds to System Threshold
If NOT EXISTS (select * from AppResource where Name = 'Admin/System Thresholds' and ResourceMenu = 'Admin/System Thresholds' and ResourceScreen = 'Admin/System Thresholds')
BEGIN

update [AppResource] set Name = 'Admin/System Thresholds', ResourceMenu = 'Admin/System Thresholds', ResourceScreen = 'Admin/System Thresholds' where DisplayOrder = 260000
	
END
go

--add new AppSetting items for Transaction Thresholds
IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountPLT')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountPLT', '1100');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountMT')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountMT', '450');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountAGLS')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountAGLS', '60');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountIND')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountIND', '300');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountSOTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountSOTR', '5');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountMOTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountMOTR', '5');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountLOTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountLOTR', '5');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.IndividualTireCountGOTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.IndividualTireCountGOTR', '5');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountDOR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountDOR', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountDOT')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountDOT', '15');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountHIT')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountHIT', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountPTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountPTR', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountRTR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountRTR', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountSTC')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountSTC', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountTCR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountTCR', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.TotalTireCountUCR')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.TotalTireCountUCR', '1850');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.CBIndividualTireCount')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.CBIndividualTireCount', '1');
END
go

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Threshold.CBTotalTireCount')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Threshold.CBTotalTireCount', '1');
END
go