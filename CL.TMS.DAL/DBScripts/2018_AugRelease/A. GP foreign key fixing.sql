﻿USE [tmdb]
GO
IF (OBJECT_ID('dbo.FK_GpiBatch_GpiBatch', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.GpiBatch DROP CONSTRAINT FK_GpiBatch_GpiBatch
END

IF (OBJECT_ID('dbo.FK_GpiBatchEntry_Claim', 'F') IS NULL)
BEGIN
	ALTER TABLE GpiBatchEntry ALTER COLUMN ClaimId INT NULL

	update GpiBatchEntry set ClaimId = null where ClaimId = 0

	ALTER TABLE [dbo].[GpiBatchEntry]  WITH CHECK ADD  CONSTRAINT [FK_GpiBatchEntry_Claim] FOREIGN KEY([ClaimId])
	REFERENCES [dbo].[Claim] ([ID])
END
GO