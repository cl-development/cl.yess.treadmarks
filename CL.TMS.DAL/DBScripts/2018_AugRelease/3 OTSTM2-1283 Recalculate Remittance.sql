-- take export of remittances excluded from recalc
select * from TSFClaim 
where 
CreatedDate>'2018-07-01' 
and RecordState !='Approved'
and IsTaxApplicable!=1
and TotalRemittancePayable-Credit<=0


-- take export of remittances getting recalc
select p.StartDate Period, t.* from TSFClaim t
inner join Period p
on p.id=PeriodID
where 
CreatedDate>'2018-07-01' 
and RecordState !='Approved'
and IsTaxApplicable!=1
and TotalRemittancePayable-Credit>0

update TSFClaim 
set TotalTSFDue = (TotalTSFDue + round((TotalRemittancePayable-Credit)*0.13, 2)), 
ApplicableTaxesHst = round((TotalRemittancePayable-Credit)*0.13, 2) , 
BalanceDue = (BalanceDue + round((TotalRemittancePayable-Credit)*0.13, 2)), 
IsTaxApplicable = 1
where 
CreatedDate>'2018-07-01' 
and RecordState !='Approved'
and IsTaxApplicable!=1
and TotalRemittancePayable-Credit>0