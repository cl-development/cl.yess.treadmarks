﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1263 Implement tax to processors and rpm
Purpose	    :   1. add taxapplicable flag for processors and rpm         
Created By  :   Joe Zhou July 09th, 2018
*****************************************************************/
USE [tmdb]
GO

ALTER TABLE "Claim"
 ADD "IsTaxApplicable" bit NULL;

ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_ClaimS_IsTaxApp_1]  DEFAULT ('0') FOR [IsTaxApplicable];
go