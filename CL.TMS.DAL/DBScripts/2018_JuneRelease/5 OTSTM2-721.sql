﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_TSFStatusOverview]    Script Date: 5/07/18 2:01:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--  Name:        sp_Rpt_TSFStatusOverview
--  Author:		 Hefen Zhou
--  Create date: 05-02-2018
--  Description: Store procedure for loading Steward TSF Status Overview (Yearly)
-- =============================================
Create PROCEDURE [dbo].[sp_Rpt_TSFStatusOverview]
	@startDate DateTime=null, 
	@endDate DateTime=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select 
		Year(p.StartDate) as [Year], 
		Month(p.StartDate) as [Month], 	
		(sum(tc.TSFDue)-sum(tc.CreditTSFDue)) as [Revenue]
	from TSFClaimDetail tc
		join Item i on tc.ItemID=i.ID
		join TSFClaim t on tc.TSFClaimID=t.ID
		join Period p on t.PeriodID=p.ID
	where (@startDate is null or p.StartDate>=@startDate) and (@endDate is null or p.EndDate<=@endDate)
	group by Year(p.StartDate),Month(p.StartDate)
	order by [Year],[Month]
END
