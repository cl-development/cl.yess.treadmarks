﻿USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_ProcessorTIPIReport]    Script Date: 5/1/2018 9:57:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_Rpt_ProcessorTIPIReport] 
	@startDate DateTime, @endDate DateTime
AS
BEGIN
	select 
	v.Number as 'Processor',
	CASE --OTSTM2-1225
		WHEN rt.IsSpecificRate = 0 or rt.IsSpecificRate is null THEN 'Global'
		ELSE 'Specific' 
	END AS 'RateType',
	convert(nvarchar(10),p.StartDate, 101) as 'ClaimPeriod',
	1 as 'SubmissionNumber',
	c.[Status] as 'RecordState',
	CAST(coalesce(cp.totalTiAmount, 0.00) as decimal(10,2)) as 'TIPI',
	CAST((coalesce(c.ClaimsAmountTotal,0) - coalesce(cp.totalTiAmount, 0.00) - coalesce(c.AdjustmentTotal,0)) as decimal(10,2)) as 'PI',
	coalesce(c.AdjustmentTotal,0) as 'Adjustments',	
	CAST(coalesce(c.ClaimsAmountTotal,0) as decimal(10, 2)) as 'Payment'
	from Claim c left join   
	(
		select ClaimID, SUM( CAST(c.Rate*c.Weight as decimal(10,2))) as totalTiAmount
		from ClaimPayment c
		where c.PaymentType = 6 -- PTR
		GROUP BY c.ClaimID
	) cp on cp.ClaimID = c.ID inner join 
	Period p on p.ID = c.ClaimPeriodID inner join Vendor v on v.ID = c.ParticipantID
	left join VendorRate vr on vr.VendorId=v.ID --OTSTM2-1225
	left join RateTransaction rt on rt.ID=vr.RateTransactionId and p.StartDate >= rt.EffectiveStartDate and p.StartDate <= rt.EffectiveEndDate --OTSTM2-1225
	where v.VendorType = 4
	and (@startDate <= p.StartDate or @startDate is null)
	and (@endDate >= p.StartDate or @endDate is null)
	order by v.Number, p.StartDate;
END