USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllServiceThresholds]    Script Date: 9/20/18 10:21:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[sp_GetAllServiceThresholds]
as 
begin
	select	
		vt.ID,
		vt.VendorID, 
		v.Number RegNumber,
		v.BusinessName, 
		v.IsActive,
		vt.[Description], 
		vt.CreatedDate,
		vt.ThresholdDays,
		vt.ModifiedDate,
		--t.TransactionTypeCode, --note if we group with transactionType, then the list result will duplicate vendor base on different type.
		DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate())) DaysSinceLastServiced,
		case when (vt.ThresholdDays - DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate()))) > 0
		then (vt.ThresholdDays - DATEDIFF(day, convert(date, max(t.TransactionDate)), convert(date, getdate())))
		else 0 end DaysLeft,
		convert(date, max(t.TransactionDate)) LastServicedDate,
		vt.IsDeleted
	from VendorServiceThreshold vt 
		inner join Vendor v on v.ID=vt.VendorID
		left  join Claim c  on c.ParticipantID=v.ID
		left  join ClaimDetail cd on c.ID=cd.ClaimId
		left join [Transaction] t on (t.ID=cd.TransactionId) and (t.TransactionTypeCode='TCR' or t.TransactionTypeCode='DOT')
	--where vt.IsDeleted is null or vt.IsDeleted=0
	group by
		vt.ID, 
		vt.VendorID,
		v.Number, 
		v.BusinessName, 
		v.IsActive,
		vt.[Description], 
		vt.ThresholdDays,
		vt.CreatedDate,
		vt.ModifiedDate,
		--t.TransactionTypeCode,
		vt.IsDeleted
end