﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

if col_length('dbo.Invitation','InviteSendDate') is null
begin
	ALTER TABLE "Invitation"  ADD "InviteSendDate" datetime;
end
go
-- adjust some Yess db invitation expire day=1
update Invitation 
	set ExpireDate=DATEADD(day,7,invitedate)
	where id in (
		select id from Invitation where DATEDIFF(day,InviteDate,Expiredate)<7
	)
-- update InviteSendDate as expire date -7.
update Invitation set InviteSendDate =DATEADD(day,-7,expiredate)