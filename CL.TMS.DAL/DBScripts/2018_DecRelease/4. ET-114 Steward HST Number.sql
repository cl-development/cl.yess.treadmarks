use [tmdb]
go

if not exists (select * from AppSetting where [Key] ='Settings.TSFTaxApplicablePeriod')
begin
insert into AppSetting ([Key], Value) values ('Settings.TSFTaxApplicablePeriod', '2018-08-01');
end


if not exists (select * from AppSetting where [Key] ='Settings.StewardHstNumber')
begin
	insert into AppSetting ([Key], [Value]) values ('Settings.StewardHstNumber', '874044803');
end

go