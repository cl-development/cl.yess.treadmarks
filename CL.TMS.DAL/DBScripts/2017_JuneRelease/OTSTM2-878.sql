﻿use tmdb
DELETE FROM TSFRemittanceNote
WHERE ID NOT IN (select MIN(ID) from TSFRemittanceNote group by TSFClaimId, Note, UserId )

DELETE FROM Activity
WHERE ID NOT IN (select MIN(ID) from Activity group by Message, ObjectId, Initiator, DATEADD(ms, -DATEPART(ms, CreatedTime), CreatedTime)) --time in same second, same person add same Message to same objectID, is duplicate.

go
