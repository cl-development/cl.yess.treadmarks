USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_Rpt_StewardOldTireCreditReport]    Script Date: 5/8/2017 11:18:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_Rpt_StewardOldTireCreditReport] 
	@startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
DECLARE @TSFNegAdjSwitchDate datetime;
select @TSFNegAdjSwitchDate = [Value] from AppSetting where [Key] = 'Settings.TSFNegAdjSwitchDate';
BEGIN
select * from
(SELECT  TSFClaim.ID[ClaimID], CAST( CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 100) + CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 120) AS varchar(10)) [NegativeAdjDate], SUM(TSFClaimDetail.NegativeAdjustment)+ SUM(TSFClaimDetail.CreditNegativeAdj)[tires], SUM(TSFClaimDetail.CreditTSFDue)[creditPayableVal], 
	
	plt_ve_adj=SUM(case when [Item].ShortName='PLT' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	pltAmount=SUM(case when [Item].ShortName='PLT' then CreditTSFDue else 0 end),
	
	mt_ve_adj=SUM(case when [Item].ShortName='MT' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	mtAmount=SUM(case when [Item].ShortName='MT' then CreditTSFDue else 0 end),

	agls_ve_adj=SUM(case when [Item].ShortName='AGLS' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	aglsAmount=SUM(case when [Item].ShortName='AGLS' then CreditTSFDue else 0 end),
	
	ind_ve_adj=SUM(case when [Item].ShortName='IND' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	indAmount=SUM(case when [Item].ShortName='IND' then CreditTSFDue else 0 end),
	
	sotr_ve_adj=SUM(case when [Item].ShortName='SOTR' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	sotrAmount=SUM(case when [Item].ShortName='SOTR' then CreditTSFDue else 0 end),
	
	motr_ve_adj=SUM(case when [Item].ShortName='MOTR' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	motrAmount=SUM(case when [Item].ShortName='MOTR' then CreditTSFDue else 0 end),
	
	lotr_ve_adj=SUM(case when [Item].ShortName='LOTR' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	lotrAmount=SUM(case when [Item].ShortName='LOTR' then CreditTSFDue else 0 end),
	
	gotr_ve_adj=SUM(case when [Item].ShortName='GOTR' then (NegativeAdjustment + CreditNegativeAdj) else 0 end),
	gotrAmount=SUM(case when [Item].ShortName='GOTR' then CreditTSFDue else 0 end),

	NegativeAdjustment = SUM([TSFClaimDetail].NegativeAdjustment),
	CreditNegativeAdjTotal = SUM([TSFClaimDetail].CreditNegativeAdj),
	creditPayable = SUM([TSFClaimDetail].CreditTSFDue)
    FROM    [dbo].[TSFClaimDetail]
    INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
    INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
    INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
    INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)
	AND [Item].ID between 39 and 46
	group by TSFClaim.ID, [NegativeAdjDate]
) as subQuery1 left join 
(select TSFClaim.ID[ClaimID], SUM([TSFClaimDetail].CreditTSFDue)[totalCredit] FROM    [dbo].[TSFClaimDetail]
    INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
    INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
    INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
    INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)
	group by TSFClaim.ID)as subQuery3 on subQuery1.[ClaimID] = subQuery3.[ClaimID]
	 left join 
( select distinct TSFClaim.ID [ClaimID],[Period].[ShortName] [Period], CAST( CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 100) + CONVERT(CHAR(4), TSFClaimDetail.NegativeAdjDate , 120) AS varchar(10)) [NegativeAdjDate],[Customer].RegistrationNumber [regNo],[Customer].BusinessName [legalName],[TSFClaim].SubmissionDate [webSubmissionDate],
  [TSFClaim].CreatedUser [submittedBy],
  stewardType = CASE 
				 WHEN [Customer].RegistrantSubTypeID  = 1 THEN 'Original Equipment Manufacture (OEM)'
				 WHEN [Customer].RegistrantSubTypeID  = 2 THEN 'Tire Manufacture/Brand Owner'
				 ELSE 'First Importer'
			  END,
  [OEM] = CASE 
			 WHEN [Customer].RegistrantSubTypeID  = 1 THEN 'Yes'
			 ELSE 'No'
		  END,
  [tsfStatus] = [TSFClaim].[RecordState],
  [receiptDate] = [TSFClaim].[ReceiptDate],
  [depositDate] = [TSFClaim].[DepositDate],
  [ChequeEFT] = [TSFClaim].ChequeReferenceNumber,
  [chequeAmountVal] = [TSFClaim].PaymentAmount,
  [taxesDue] = 'N/A',
  [interest] = 'N/A',
  [penaltiesVal] = CASE 
					 WHEN [TSFClaim].[PenaltiesManually] != null THEN [TSFClaim].[PenaltiesManually]
					 ELSE [TSFClaim].Penalties
				  END,
  [registrantStatus] = CASE 
						 WHEN [Customer].[IsActive] = 1 THEN 'Active'
						 ELSE 'Inactive'
					  END,
  [statusChangeDate] = [Customer].[ActiveStateChangeDate]
	FROM  [dbo].[TSFClaimDetail]
		INNER JOIN [dbo].[TSFClaim] ON [TSFClaimDetail].[TSFClaimID] = [TSFClaim].[ID]
		INNER JOIN [dbo].[Period] ON [TSFClaim].[PeriodID] = [Period].[ID]
		INNER JOIN [dbo].[Item] ON [TSFClaimDetail].[ItemID] = [Item].[ID]
		INNER JOIN [dbo].[Customer] ON [TSFClaim].[CustomerID] = [Customer].[ID]
    WHERE ([Period].[StartDate] >= @startDate) AND ([Period].[EndDate] <= @endDate) 
	AND ((@registrationNumber IS NULL) OR (@registrationNumber IS NOT NULL AND ([TSFClaim].[RegistrationNumber] =  CAST( @registrationNumber AS nvarchar(max)))))
	AND (TSFClaimDetail.NegativeAdjustment >0 or TSFClaimDetail.CreditNegativeAdj >0)) as subQuery2 
	on subQuery1.[ClaimID] = subQuery2.[ClaimID] and subQuery1.[NegativeAdjDate] = subQuery2 .[NegativeAdjDate]
END


GO

