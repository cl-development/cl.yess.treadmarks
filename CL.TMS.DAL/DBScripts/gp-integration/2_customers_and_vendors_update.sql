-- OTSFI-15

	update dbo.[Customer]
	set InBatch = 1, gpUpdate = 0;

	update dbo.[Vendor]
	set InBatch = 1, gpUpdate = 0;

	update dbo.[Customer]
	set [gpUpdate] = 1
	where LastUpdatedDate >= '2016-04-01 00:00:00.000';

	update dbo.[Customer]
	set [inBatch] = 0
	where CreatedDate >= '2016-04-01 00:00:00.000';

	update dbo.[Vendor]
	set [gpUpdate] = 1
	where LastUpdatedDate >= '2016-04-01 00:00:00.000';

	update dbo.[Vendor]
	set [inBatch] = 0
	where CreatedDate >= '2016-04-01 00:00:00.000';
