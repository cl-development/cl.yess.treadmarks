USE [tmdb]
GO
/****** Object:  Table [dbo].[Report]    Script Date: 5/26/2016 10:54:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report](
	[ID] [int] NOT NULL,
	[ReportName] [nvarchar](50) NOT NULL,
	[ReportCategoryID] [int] NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReportCategory]    Script Date: 5/26/2016 10:54:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportCategory](
	[ID] [int] NOT NULL,
	[ReportCategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReportCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (2, N'Registrant Cumulative Weekly Report ', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (4, N'Steward Revenue & Supply - New Tire Types', 1)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (5, N'Processor TIPI Report ', 4)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (7, N'Steward Revenue & Supply - Old Tire Types', 1)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (8, N'TSF Extract - In Batch Only Report (GP)', 1)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (9, N'Web Listing Collector Report Postal K', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (10, N'Web Listing Collector Report Postal L', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (11, N'Web Listing Collector Report Postal M', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (12, N'Web Listing Collector Report Postal N', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (13, N'Web Listing Collector Report Postal P', 6)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (15, N'Hauler Collector Comparison Report', 3)
GO
INSERT [dbo].[Report] ([ID], [ReportName], [ReportCategoryID]) VALUES (16, N'Web Listing Hauler Report', 6)
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (1, N'Steward')
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (2, N'Collector')
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (3, N'Hauler')
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (4, N'Processor')
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (5, N'RPM')
GO
INSERT [dbo].[ReportCategory] ([ID], [ReportCategoryName]) VALUES (6, N'Registrant')
GO
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_Report] FOREIGN KEY([ReportCategoryID])
REFERENCES [dbo].[ReportCategory] ([ID])
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_Report]
GO
