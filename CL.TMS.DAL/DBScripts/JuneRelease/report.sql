USE [tmdb];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Report]([ID], [ReportName], [ReportCategoryID])
SELECT 17, N'Hauler Volume Report', 3 UNION ALL
-- OTS does not want report displayed
-- SELECT 18, N'Detail Hauler Volume Report Based On Scale Weight', 3 UNION ALL
-- SELECT 19, N'Processor Volume Report', 4 UNION ALL
-- SELECT 20, N'RPM Data', 5 UNION ALL
-- SELECT 21, N'Processor Disposition of Residual', 4 UNION ALL
SELECT 22, N'Steward Non Filers', 1
COMMIT;
RAISERROR (N'[dbo].[Report]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

