-- UPDATE COLLECTOR ADJUSTMENTS
UPDATE [tmdb].[dbo].[GpiAccount]
SET account_number = '4195-90-20-40'
WHERE account_number = '4100-90-20-99';

-- UPDATE COLLECTOR ADJUSTMENTS TAX
UPDATE [tmdb].[dbo].[GpiAccount]
SET account_number = '4196-90-20-40'
WHERE account_number = '4105-90-20-40';