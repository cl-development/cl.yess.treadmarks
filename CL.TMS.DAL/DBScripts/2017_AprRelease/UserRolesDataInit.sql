﻿
IF COL_LENGTH('Role', 'Description') IS NOT NULL
BEGIN
    ALTER TABLE [Role] DROP COLUMN [Description]
    --ALTER TABLE [Role] DROP COLUMN [RowVersion]
END
IF COL_LENGTH('Role', 'RoleType') IS NULL
BEGIN
    ALTER TABLE [Role] ADD [RoleType] int NOT NULL DEFAULT(0);
    ALTER TABLE [Role] ADD [CreateBy] [varchar](50) NOT NULL DEFAULT('System');
    ALTER TABLE [Role] ADD [CreateDate] datetime NOT NULL DEFAULT('2017-04-12');
    ALTER TABLE [Role] ADD [UpdateDate] datetime NOT NULL DEFAULT('2017-04-12');
END
go

update [Role] set CreateBy='System'

IF NOT EXISTS (SELECT * FROM [Role] WHERE [Name] = 'SuperAdmin')
BEGIN
	insert into [Role] 	(Name,			RoleType,	CreateBy,	CreateDate,			UpdateDate)
				VALUES	('SuperAdmin',	1,			'System',	GETDATE(),			GETDATE())
	insert into [UserRole]	(UserID,	RoleID) VALUES((select ID from [User] where Email = 'test@test.com'), (select ID from [Role] where Name = 'SuperAdmin'))
END
go

DECLARE @RoleId INT
DECLARE @AppPermissionId INT
DECLARE @AppResourceId INT
DECLARE @name NVARCHAR(100)
DECLARE @getid CURSOR

SET @RoleId = (select distinct r.ID from [User] u join UserRole ur on ur.UserID=u.ID join [Role] r on ur.RoleID=r.ID where (r.Name = 'SuperAdmin'))
SET @AppPermissionId = (select ID from [AppPermission] where Name='EditSave')
SET @getid = CURSOR FOR
SELECT AppResource.Id
FROM   AppResource 

OPEN @getid
FETCH NEXT
FROM @getid INTO @AppResourceId
WHILE @@FETCH_STATUS = 0
BEGIN
	insert into [RolePermission]	(RoleId,	AppResourceId,	AppPermissionId)
							VALUES	(@RoleId,	@AppResourceId,	@AppPermissionId)
    FETCH NEXT
    FROM @getid INTO @AppResourceId
END

CLOSE @getid
DEALLOCATE @getid
go



--------------------------------------- NEED REVIEW ---------------------------------------
delete from [UserRole] where RoleID not in (select r.ID from [Role] r join UserRole ur on ur.RoleID=r.ID where r.Name in ( 'Read','Write','Submit','ParticipantAdmin', 'SuperAdmin'));
GO

delete from [Role] where Name not in ('Read','Write','Submit','ParticipantAdmin', 'SuperAdmin');

GO
--------------------------------------- NEED REVIEW ---------------------------------------
-- ToDo: Drop the following tables 
drop table PermissionRole;
drop table Permission;
drop table [action];
drop table [resource];
drop table userclaimsrole;
drop table userclaimsrouting;
drop table invitationclaimsrole;
drop table invitationclaimsrouting;

GO


--------------------------- migrate role for participant user ---------------------------
----------------------------------- hard code version -----------------------------------
DECLARE @RoleID INT, @AppResourceId INT, @AppPermissionId INT, @AppPermissionReadOnlyId INT, @iCounter INT;
SET @RoleID=11;--11:Read 12:Write 13:Submit 14:ParticipantAdmin
WHILE @RoleID <= 14
BEGIN 
    IF (@RoleID = 11) (select @AppPermissionId = (select Id from AppPermission where Name = 'ReadOnly')) --Read -> 'ReadOnly'
	ELSE select @AppPermissionId = (select Id from AppPermission where Name = 'EditSave') -- others -> 'EditSave'
	select @AppPermissionReadOnlyId = (select Id from AppPermission where Name = 'ReadOnly');

	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 1  , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 2  , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 3  , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 4  , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 16 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 27 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 38 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 49 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 50 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 51 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 52 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 53 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 54 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 64 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 65 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 69 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 83 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 96 , @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 109, @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 122, @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 135, @AppPermissionId);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 141, @AppPermissionReadOnlyId);--"Retail Connection" is for both Staff user and Participant user
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 142, @AppPermissionReadOnlyId);--"Retail Connection_All Retail Connection_Retail Connection"
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 136, @AppPermissionId);
	--select @RoleID, @AppResourceId, @AppPermissionId ;

	-- Set No Access for the following staff only Resources
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 137, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 138, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 146, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 147, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 154, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 155, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 162, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 163, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 164, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 165, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 170, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 171, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 172, 1);
	insert into RolePermission (RoleId,AppResourceId,AppPermissionId) values(@RoleId, 173, 1);

    SET @RoleID = @RoleID + 1;
END

DECLARE @AppResUsersId INT;
SET @AppResUsersId = (select Id from AppResource where Name='Users' and ResourceMenu='Users' and ResourceScreen = 'Users');
--select * from RolePermission  where (RoleId in (11,12,13) and AppResourceId = @AppResUsersId)
update RolePermission set AppPermissionId = 1 where (RoleId in (11,12,13) and AppResourceId = @AppResUsersId)
update RolePermission set AppPermissionId = (select Id from AppPermission where Name ='EditSave') where (RoleId in (14) and AppResourceId = @AppResUsersId)
update RolePermission set AppPermissionId = (select Id from AppPermission where Name ='EditSave') where (RoleId in (14) and AppResourceId in (141,142))
go

--------------------------------------- CREATE SP ---------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_AssignRoleToAllStaffUsersByRoleId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[sp_AssignRoleToAllStaffUsersByRoleId]
END

/****** Object:  StoredProcedure [dbo].[sp_AssignRoleToAllStaffUsersByRoleId]    Script Date:  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_AssignRoleToAllStaffUsersByRoleId] 
	@RoleId INT
AS
BEGIN
	DECLARE @userID INT, @iCounter INT, @roleName NVARCHAR(100)
	set @iCounter = 0
	IF EXISTS (select Name from [Role] where ID = @RoleId)
	BEGIN
		set @roleName = (select Name from [Role] where ID = @RoleId);
		DECLARE @getUserIDs CURSOR
		SET @getUserIDs = CURSOR FOR select u.ID from [User] u join Invitation inv on inv.UserName=u.UserName and inv.VendorId is null and inv.CustomerId is null order by u.ID

		OPEN @getUserIDs
		FETCH NEXT
		FROM @getUserIDs INTO @userID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF not EXISTS (SELECT * from UserRole ur WHERE ur.RoleID = @RoleId and ur.UserID = @userID)
			BEGIN
				insert into [UserRole] (UserID,RoleID) VALUES	(@userID,@RoleId)
				set @iCounter = @iCounter + 1;
			END

			FETCH NEXT
			FROM @getUserIDs INTO @userID
		END
		CLOSE @getUserIDs
		DEALLOCATE @getUserIDs
		select CONVERT(varchar(10), @iCounter) + ' users were assigned role: ' + @roleName 
	END
	ELSE
	BEGIN
		select 'Role not exist.'
	END
END

go

