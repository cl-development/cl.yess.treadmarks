USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[sp_Rpt_CollectorTireOrigin]    Script Date: 2/13/2017 1:56:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Rpt_CollectorTireOrigin] 
    @startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
BEGIN
declare  @startDateVar DateTime, @endDateVar DateTime, @registrationNumberVar nvarchar(256)
set @startDateVar = @startDate
set @endDateVar = @endDate
set @registrationNumberVar = @registrationNumber

select 
p.ShortName ClaimPeriod,
v.Number CollectorRegNumber,
v.BusinessName CollectorLegalName,
v.OperatingName CollectorOperatingName,
c.Status,
v.IsActive CollectorStatus,
t.Description OriginDescription,

tires.PLT,
tires.MT,
tires.AGLS,
tires.IND,
tires.SOTR,
tires.MOTR,
tires.LOTR,
tires.GOTR,

tires.PltEstWeight,
tires.MtEstWeight,
tires.AglsEstWeight,
tires.IndEstWeight,
tires.SotrEstWeight,
tires.MotrEstWeight,
tires.LotrEstWeight,
tires.GotrEstWeight

from Claim c 
inner join
(
	select 
	cto.ClaimId,
	cto.TireOriginValue,
	SUM(case when itemid = 65 then quantity else 0 end)  'PLT',
	SUM(case when itemid = 66 then quantity else 0 end)  'MT',
	SUM(case when itemid = 67 then quantity else 0 end)  'AGLS',
	SUM(case when itemid = 68 then quantity else 0 end)  'IND',
	SUM(case when itemid = 69 then quantity else 0 end)  'SOTR',
	SUM(case when itemid = 70 then quantity else 0 end)  'MOTR',
	SUM(case when itemid = 71 then quantity else 0 end)  'LOTR',
	SUM(case when itemid = 72 then quantity else 0 end)  'GOTR',
	
	SUM(case when itemid = 65 then AverageWeight else 0 end)  'PltEstWeight',
	SUM(case when itemid = 66 then AverageWeight else 0 end)  'MtEstWeight',
	SUM(case when itemid = 67 then AverageWeight else 0 end)  'AglsEstWeight',
	SUM(case when itemid = 68 then AverageWeight else 0 end)  'IndEstWeight',
	SUM(case when itemid = 69 then AverageWeight else 0 end)  'SotrEstWeight',
	SUM(case when itemid = 70 then AverageWeight else 0 end)  'MotrEstWeight',
	SUM(case when itemid = 71 then AverageWeight else 0 end)  'LotrEstWeight',
	SUM(case when itemid = 72 then AverageWeight else 0 end)  'GotrEstWeight'
	
	from ClaimTireOrigin cto	
	group by cto.ClaimId, cto.TireOriginValue
) as tires on tires.ClaimId=c.ID
inner join Period p
on c.ClaimPeriodID=p.ID
inner join Vendor v
on c.ParticipantID=v.ID
inner join TypeDefinition t
on t.Category='TireOrigin' and tires.TireOriginValue=t.DefinitionValue

where 
(@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.StartDate  <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
order by v.Number, p.StartDate

END
