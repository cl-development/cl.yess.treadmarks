﻿USE [tmdb]
GO

/****** Object:  Table [dbo].[Announcement]    Script Date: 2018-03-22 7:37:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if not exists (select * from sysobjects where name='Announcement' And xtype='U')
begin
	CREATE TABLE [dbo].[Announcement](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Subject] [varchar](1000) not NULL,
		[Description] [varchar](max) not NULL,
		[StartDate] [datetime] not NULL,
		[EndDate] [datetime] not NULL,

		[Steward] [bit] not null,
		[Collector] [bit] not null,
		[Hauler] [bit] not null,
		[Processor] [bit] not null,
		[RPM] [bit] not null,
		[Staff] [bit] not null,

		[CreatedDate] [datetime] NOT NULL,
		[CreatedByID] [bigint] NOT NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedByID] [bigint] NULL,
	 CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[Announcement]  WITH CHECK ADD  CONSTRAINT [FK_Announcement_CreatedByUser] FOREIGN KEY([CreatedByID])
	REFERENCES [dbo].[User] ([ID])
	ALTER TABLE [dbo].[Announcement]  WITH CHECK ADD  CONSTRAINT [FK_Announcemen_ModifiedUser] FOREIGN KEY([ModifiedByID])
	REFERENCES [dbo].[User] ([ID])
end
GO
