update report set Description='This report produces three tabs in Excel. The first tab is for supplied Tire Counts and the other tabs are for Credits.<br/>
The Tire Count tab pulls Steward details regarding:<br/>
� Submission Information<br/>
� Payment Information<br/>
� Tire Class and Adjustment Details<br/>
The Credit tabs pull Steward details regarding:<br/>
� Submission Information for Negative Adjustments<br/>
� Payment Information<br/>
� Tire Class and Adjustment Details<br/>' where id=4