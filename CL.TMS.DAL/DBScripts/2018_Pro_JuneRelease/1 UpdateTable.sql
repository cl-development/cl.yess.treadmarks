﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************
Name        :   OTSTM2-1090 New menu of Dashboard
Purpose	    :   1. add Dashboard menu to appresource
            :   2. create permission for new AppResource Menu Dashboard   		
Created By  :   Joe Zhou April 26th, 2018
*****************************************************************/
USE [tmdb]
GO

IF OBJECT_ID('dbo.[UQ_AppResource_DisplayOrder]') IS NULL 
begin
	ALTER TABLE dbo.AppResource ADD CONSTRAINT UQ_AppResource_DisplayOrder UNIQUE (DisplayOrder); 
end

--== OTSTM2-1090 begin ==--
--user Dashboard menu item Display Order set as 400000 (after company branding before report) 
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard' and Name = 'Dashboard')

BEGIN
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()

insert into [AppResource]	(Name,		ResourceMenu,		ResourceScreen,		ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES	

			('Dashboard',				'Dashboard',		'Dashboard',			0,				80000,		@CreateBy,	@dt,		@dt),
			('StaffDashboard',			'Dashboard',		'Dashboard',			1,				81000,		@CreateBy,	@dt,		@dt),
			('Announcements',			'Dashboard',		'Dashboard',		    2,				82100,		@CreateBy,	@dt,		@dt),
			('Year to Date TSF Dollar',	'Dashboard',		'Dashboard',		    2,				83100,		@CreateBy,	@dt,		@dt),
			('Year to Date TSF Units',	'Dashboard',		'Dashboard',		    2,				84100,		@CreateBy,	@dt,		@dt),
			('TSF Status Overview',		'Dashboard',		'Dashboard',		    2,				85100,		@CreateBy,	@dt,		@dt),		
			('Top Ten Stewards',		'Dashboard',		'Dashboard',		    2,				86100,		@CreateBy,	@dt,		@dt),
			('STC Events',				'Dashboard',		'Dashboard',		    2,				87100,		@CreateBy,	@dt,		@dt)
		
END
go

--create permission for new appResource Admin/Financial Integration

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int,  @iReadOnly int, @iEditSave int
Declare @getRoleIds cursor, @newAnnounce int,@newMetric1 int,@newMetric2 int, @newMetric3 int, @newMetric4 int, 
		@newSTC int,@newDashboard int, @newAdminAnnouncements int
set @superAdminID=(select ID from role where name='Super Admin')
set @getRoleIds = cursor for select ID from [Role]

set @newResourceID=(select id from AppResource	where Name = 'Dashboard'				and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newDashboard=(select id from AppResource	where Name = 'StaffDashboard'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newAnnounce=(select id from AppResource	where Name = 'Announcements'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric1=(select id from AppResource		where Name = 'Year to Date TSF Dollar'	and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric2=(select id from AppResource		where Name = 'Year to Date TSF Units'	and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric3=(select id from AppResource		where Name = 'TSF Status Overview'		and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')
set @newMetric4=(select id from AppResource		where Name = 'Top Ten Stewards'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')

set @newSTC=(select id from AppResource			where Name = 'STC Events'			and ResourceMenu = 'Dashboard' and ResourceScreen = 'Dashboard')

set @newAdminAnnouncements=(select id from AppResource	where Name = 'Admin/Announcements' and ResourceMenu = 'Admin/Announcements' and ResourceScreen = 'Admin/Announcements')

select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iReadOnly = Id from AppPermission where Name = 'ReadOnly'
select @iEditSave = Id from AppPermission where Name = 'EditSave'

set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	IF not EXISTS 
	(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
	WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
	BEGIN
		if (@RoleId = @superAdminID) 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newDashboard, @iEditSave)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAnnounce, @iEditSave)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric1, @iEditSave)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric2, @iEditSave)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric3, @iEditSave)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric4, @iEditSave)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newSTC, @iEditSave)
			end 
		else 
			begin 
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iReadOnly)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newDashboard, @iReadOnly)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAnnounce, @iReadOnly)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric1, @iReadOnly)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric2, @iReadOnly)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric3, @iReadOnly)
     			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newMetric4, @iReadOnly)
				insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newSTC, @iEditSave)
			end
	END

	if not exists (SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newAdminAnnouncements and rp.RoleId = @RoleId)
		begin
			if (@RoleId = @superAdminID)
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAdminAnnouncements, @iEditSave)
				end
			else 
				begin
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newAdminAnnouncements, @iNoAccess)
				end
		end
	else
		begin
			if (@RoleId = @superAdminID)
				begin 
					update [RolePermission] set AppPermissionId=@iEditSave where RoleId=@RoleId and AppResourceId=@newAdminAnnouncements
				end
			else 
				begin
					update [RolePermission] set AppPermissionId=@iNoAccess  where RoleId=@RoleId and AppResourceId=@newAdminAnnouncements
				end
		end

	set @iCounter = @iCounter + 1;
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getRoleIds
go
--== OTSTM2-1090 end ==--

--== OTSTM2-630 begin ==--
if ((select COUNT(*) from [AppResource] where [Name] = 'Activities' and [ResourceMenu]='Applications' ) = 0) 
begin
declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()
insert into [AppResource]	(Name,			ResourceMenu,			ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES			('Activities',	'Applications',			'Steward Application',		2,				131122,			@CreateBy,	@dt,		@dt),
							('Activities',	'Applications',			'Collector Application',	2,				131223,			@CreateBy,	@dt,		@dt),
							('Activities',	'Applications',			'Hauler Application',		2,				131322,			@CreateBy,	@dt,		@dt),
							('Activities',	'Applications',			'Processor Application',	2,				131422,			@CreateBy,	@dt,		@dt),
							('Activities',	'Applications',			'RPM Application',			2,				131522,			@CreateBy,	@dt,		@dt)

end
go

DECLARE @RoleID INT, @AppResourceId INT, @AppPermissionId INT, @iCounter INT, @NewRateResId INT;
DECLARE @getResIDs CURSOR
DECLARE @getRoleIds CURSOR
SET @getResIDs = CURSOR FOR select Id from [AppResource] where Name = 'Activities' and ResourceMenu='Applications'
SET @getRoleIds = CURSOR FOR select ID from [Role] where [Name] not in ('Read','Write','Submit','ParticipantAdmin')

set @iCounter = 0

(select @AppPermissionId = (select Id from AppPermission where Name = 'EditSave'))

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId--, @AppPermissionId
WHILE @@FETCH_STATUS = 0
BEGIN
	OPEN @getResIDs FETCH NEXT FROM @getResIDs INTO @NewRateResId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @NewRateResId and rp.RoleId = @RoleId)
		BEGIN
			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @NewRateResId, @AppPermissionId)
			--select  @RoleId, @NewRateResId, @AppPermissionId, @iCounter 
		END
		FETCH NEXT
		FROM @getResIDs INTO @NewRateResId
		set @iCounter = @iCounter + 1;
	END
	CLOSE @getResIDs
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId--, @AppPermissionId
END

CLOSE @getRoleIds

DEALLOCATE @getResIDs
DEALLOCATE @getRoleIds
go

--== OTSTM2-630 end ==--

-- OTSTM2-1081 announcements admin menu
update [AppResource] set DisplayOrder = 900000 where Name = 'Admin/Reports' and ResourceMenu = 'Admin/Reports' and ResourceScreen = 'Admin/Reports'
update [AppResource] set DisplayOrder = 800000 where Name = 'Admin/Announcements' and ResourceMenu = 'Admin/Announcements' and ResourceScreen = 'Admin/Announcements'

IF NOT EXISTS (SELECT * FROM AppSetting WHERE [KEY] = 'Site.ApplicationInstance')
BEGIN
	INSERT INTO AppSetting ("Key", "Value") VALUES ('Site.ApplicationInstance', 'PRO');--Sucessor
END
go
