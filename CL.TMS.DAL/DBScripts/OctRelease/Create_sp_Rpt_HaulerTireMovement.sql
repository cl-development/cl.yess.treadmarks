﻿USE [tmdb]
GO

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[sp_Rpt_HaulerTireMovement]') and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].sp_Rpt_HaulerTireMovement
END

/****** Object:  StoredProcedure [dbo].[sp_Rpt_HaulerTireMovement]    Script Date:  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].sp_Rpt_HaulerTireMovement 
    @startDate DateTime, @endDate DateTime, @registrationNumber nvarchar(256)
AS
BEGIN
declare  @startDateVar DateTime, @endDateVar DateTime, @registrationNumberVar nvarchar(256)
set @startDateVar = @startDate
set @endDateVar = @endDate
set @registrationNumberVar = @registrationNumber

select ClaimIDs.ReportingPeriod, ClaimIDs.HaulerRegistrationNumber, ClaimIDs.HaulerRegistrationName, ClaimIDs.HaulerStatus, SubmissionNumber='N/A', ClaimIDs.RecordState 
 ,coalesce(tireCollected.TotalT_Collected,0)[TotalT_Collected], coalesce(tireCollected.PLT_Collected,0)[PLT_Collected], coalesce(tireCollected.MT_Collected,0)[MT_Collected], coalesce(tireCollected.AGLS_Collected,0)[AGLS_Collected], coalesce(tireCollected.IND_Collected,0)[IND_Collected], coalesce(tireCollected.SOTR_Collected,0)[SOTR_Collected], coalesce(tireCollected.MOTR_Collected,0)[MOTR_Collected], coalesce(tireCollected.LOTR_Collected,0)[LOTR_Collected], coalesce(tireCollected.GOTR_Collected,0)[GOTR_Collected]
 ,coalesce(tireDelivered.NTotalT_Delivered,0)[NTotalT_Delivered], coalesce(tireDelivered.NPLT_Delivered,0)[NPLT_Delivered], coalesce(tireDelivered.NMT_Delivered,0)[NMT_Delivered], coalesce(tireDelivered.NAGLS_Delivered,0)[NAGLS_Delivered], coalesce(tireDelivered.NIND_Delivered,0)[NIND_Delivered], coalesce(tireDelivered.NSOTR_Delivered,0)[NSOTR_Delivered], coalesce(tireDelivered.NMOTR_Delivered,0)[NMOTR_Delivered], coalesce(tireDelivered.NLOTR_Delivered,0)[NLOTR_Delivered], coalesce(tireDelivered.NGOTR_Delivered,0)[NGOTR_Delivered]
 ,coalesce(tireAdjusted.TotalT_Adjustments,0)[TotalT_Adjustments], coalesce(tireAdjusted.PLT_Adjustments,0)[PLT_Adjustments], coalesce(tireAdjusted.MT_Adjustments,0)[MT_Adjustments], coalesce(tireAdjusted.AGLS_Adjustments,0)[AGLS_Adjustments], coalesce(tireAdjusted.IND_Adjustments,0)[IND_Adjustments], coalesce(tireAdjusted.SOTR_Adjustments,0)[SOTR_Adjustments], coalesce(tireAdjusted.MOTR_Adjustments,0)[MOTR_Adjustments], coalesce(tireAdjusted.LOTR_Adjustments,0)[LOTR_Adjustments], coalesce(tireAdjusted.GOTR_Adjustments,0)[GOTR_Adjustments]
 from
 (
	(select coalesce(tblRpt.ClaimId, columnInfos.ClaimId) as 'ClaimId', columnInfos.ReportPeriod[ReportingPeriod], columnInfos.HaulerRegistrationNumber, columnInfos.HaulerRegistrationName,columnInfos.HaulerStatus, columnInfos.RecordState
		from 
		(
			(
				select distinct ClaimId from tmdb.dbo.RptClaimInventoryDetail rcid 	
				where rcid.VendorType = 3  and rcid.TransactionProcessingStatus != 'Invalidated' 
				and (@startDateVar is null or @startDateVar <= rcid.PeriodStartDate) AND (@endDateVar is null or rcid.PeriodEndDate <= @endDateVar)  AND (@registrationNumberVar is null or rcid.RegistrationNumber = @registrationNumberVar)
			)
			union
			( 
				select distinct c.ID[ClaimId] from ClaimInventoryAdjustment adj join Claim c on adj.ClaimId=c.ID join Period p on c.ClaimPeriodID=p.ID join Vendor v on c.ParticipantID=v.ID
				where (v.VendorType = 3 ) and (@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.EndDate <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
			)
		)tblRpt
			full outer join
		(
			select c.ID[ClaimId],p.ShortName[ReportPeriod],v.Number[HaulerRegistrationNumber],v.BusinessName[HaulerRegistrationName]
			,case when v.IsActive = 1
					then 'Active'	
					else 'InActive'
			end as 'HaulerStatus',
				c.[Status] as 'RecordState'
			from Claim c join Vendor v on c.ParticipantID=v.ID join Period p on c.ClaimPeriodID=p.ID
			where (v.VendorType = 3 ) and (@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.EndDate <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
		)columnInfos on tblRpt.ClaimId = columnInfos.ClaimId
	)
 )ClaimIDs --Some claim only has Inbound/outbound transactions; or don't have any transactions
 left join
 (
    SELECT 
        main_Collected.claimID,
        convert(nvarchar(10), main_Collected.ReportPeriod, 101) as 'ReportingPeriod',
        1 as 'SubmissionNumber',
        main_Collected.HaulerRegistrationNumber,
        main_Collected.HaulerRegistrationName,
        main_Collected.HaulerStatus,
        main_Collected.RecordState,
        sum(main_Collected.TotalT_Collected) as TotalT_Collected,
        sum(main_Collected.PLT_Collected) as PLT_Collected,
        sum(main_Collected.MT_Collected) as MT_Collected,
        sum(main_Collected.AGLS_Collected) as AGLS_Collected,
        sum(main_Collected.IND_Collected) as IND_Collected,
        sum(main_Collected.SOTR_Collected) as SOTR_Collected,
        sum(main_Collected.MOTR_Collected) as MOTR_Collected,
        sum(main_Collected.LOTR_Collected) as LOTR_Collected,
        sum(main_Collected.GOTR_Collected) as GOTR_Collected
        FROM
        (
            select  
            c.ID as 'claimID',
            p.ShortName as 'ReportPeriod',
            v.Number as 'HaulerRegistrationNumber',
            v.BusinessName as 'HaulerRegistrationName',
            case when v.IsActive = 1
                then 'Active'	
                else 'InActive'
            end as 'HaulerStatus',
            c.[Status] as 'RecordState',
            coalesce(transactionInbound.TotalT_Collected,0) as 'TotalT_Collected',
            coalesce(transactionInbound.PLT_Collected,0) as 'PLT_Collected',
            coalesce(transactionInbound.MT_Collected,0) as 'MT_Collected',
            coalesce(transactionInbound.AGLS_Collected,0) as 'AGLS_Collected',
            coalesce(transactionInbound.IND_Collected,0) as 'IND_Collected',
            coalesce(transactionInbound.SOTR_Collected,0) as 'SOTR_Collected',
            coalesce(transactionInbound.MOTR_Collected,0) as 'MOTR_Collected',
            coalesce(transactionInbound.LOTR_Collected,0) as 'LOTR_Collected',
            coalesce(transactionInbound.GOTR_Collected,0) as 'GOTR_Collected'
            from claim c
            inner join [Period] p on p.id = c.ClaimPeriodID
            inner join [Vendor] v on v.id = c.ParticipantID 
            inner join claimdetail cd on c.id = cd.claimid 
            inner join [transaction] t on t.id = cd.TransactionId 
            left outer join
            (
                select piv.ClaimId, piv.TransactionId, piv.PeriodName[ReportingPeriod], piv.RegistrationNumber[HaulerRegistrationNumber], (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  [TotalT_Collected], 
                    sum([PLT]) as [PLT_Collected], sum([MT]) as [MT_Collected], sum([AGLS]) as [AGLS_Collected], sum([IND]) as [IND_Collected], sum([SOTR]) as [SOTR_Collected], sum([MOTR]) as [MOTR_Collected], sum([LOTR]) as [LOTR_Collected], sum([GOTR]) as [GOTR_Collected]
                from
                (
                    select piv.ClaimId, piv.TransactionId, piv.PeriodName, piv.RegistrationNumber,
                        coalesce([PLT],0) as 'PLT', 
                        coalesce([MT],0) as 'MT', 
                        coalesce([AGLS],0) as 'AGLS', 
                        coalesce([IND],0) as 'IND', 
                        coalesce([SOTR],0) as 'SOTR', 
                        coalesce([MOTR],0) as 'MOTR', 
                        coalesce([LOTR],0) as 'LOTR', 
                        coalesce([GOTR],0) as 'GOTR'
                    from
                    (
                        select rcid.* 
                        from tmdb.dbo.RptClaimInventoryDetail rcid 	
                        where rcid.Direction = 1 and rcid.VendorType = 3  and rcid.TransactionProcessingStatus != 'Invalidated' 
                    ) src
                    PIVOT
                    (
                        sum([Quantity])
                        for ItemShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
                    ) piv	 	
                ) piv
                group by piv.ClaimId, piv.TransactionId, piv.PeriodName, piv.RegistrationNumber
            ) as transactionInbound on transactionInbound.TransactionId = t.ID and transactionInbound.ClaimId = c.id
            where v.VendorType = 3 
            and (@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.StartDate  <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
        ) as main_Collected
        group by main_Collected.claimID, main_Collected.ReportPeriod, main_Collected.HaulerRegistrationNumber, main_Collected.HaulerRegistrationName, main_Collected.HaulerStatus, main_Collected.RecordState
)tireCollected on ClaimIDs.ClaimId = tireCollected.ClaimId
left join
(
    SELECT 
    main_Delivered.claimID,
    sum(main_Delivered.NTotalT_Delivered) * -1 as NTotalT_Delivered,
    sum(main_Delivered.NPLT_Delivered) * -1  as NPLT_Delivered,
    sum(main_Delivered.NMT_Delivered) * -1  as NMT_Delivered,
    sum(main_Delivered.NAGLS_Delivered) * -1  as NAGLS_Delivered,
    sum(main_Delivered.NIND_Delivered) * -1  as NIND_Delivered,
    sum(main_Delivered.NSOTR_Delivered) * -1  as NSOTR_Delivered,
    sum(main_Delivered.NMOTR_Delivered) * -1  as NMOTR_Delivered,
    sum(main_Delivered.NLOTR_Delivered) * -1  as NLOTR_Delivered,
    sum(main_Delivered.NGOTR_Delivered) * -1  as NGOTR_Delivered
    FROM
    (
        select  
        c.ID as 'claimID',
        coalesce(transactionoutBound.NTotalT_Delivered,0) as 'NTotalT_Delivered',
        coalesce(transactionoutBound.NPLT_Delivered,0) 'NPLT_Delivered',
        coalesce(transactionoutBound.NMT_Delivered,0) 'NMT_Delivered',
        coalesce(transactionoutBound.NAGLS_Delivered,0) 'NAGLS_Delivered',
        coalesce(transactionoutBound.NIND_Delivered,0) 'NIND_Delivered',
        coalesce(transactionoutBound.NSOTR_Delivered,0) 'NSOTR_Delivered',
        coalesce(transactionoutBound.NMOTR_Delivered,0) 'NMOTR_Delivered',
        coalesce(transactionoutBound.NLOTR_Delivered,0) 'NLOTR_Delivered',
        coalesce(transactionoutBound.NGOTR_Delivered,0) 'NGOTR_Delivered'
        from claim c
        inner join [Period] p on p.id = c.ClaimPeriodID
        inner join [Vendor] v on v.id = c.ParticipantID 
        inner join claimdetail cd on c.id = cd.claimid 
        inner join [transaction] t on t.id = cd.TransactionId 
        left outer join
        (
            select piv.ClaimId, piv.TransactionId, piv.PeriodName[ReportingPeriod], piv.RegistrationNumber[HaulerRegistrationNumber], (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as  [NTotalT_Delivered], 
                sum([PLT]) as [NPLT_Delivered], sum([MT]) as [NMT_Delivered], sum([AGLS]) as [NAGLS_Delivered], sum([IND]) as [NIND_Delivered], sum([SOTR]) as [NSOTR_Delivered], sum([MOTR]) as [NMOTR_Delivered], sum([LOTR]) as [NLOTR_Delivered], sum([GOTR]) as [NGOTR_Delivered]
            from
            (
                select piv.ClaimId, piv.TransactionId, piv.PeriodName, piv.RegistrationNumber,
                    coalesce([PLT],0) as 'PLT', 
                    coalesce([MT],0) as 'MT', 
                    coalesce([AGLS],0) as 'AGLS', 
                    coalesce([IND],0) as 'IND', 
                    coalesce([SOTR],0) as 'SOTR', 
                    coalesce([MOTR],0) as 'MOTR', 
                    coalesce([LOTR],0) as 'LOTR', 
                    coalesce([GOTR],0) as 'GOTR'
                from
                (
                    select rcid.* 
                    from tmdb.dbo.RptClaimInventoryDetail rcid 	
                    where rcid.Direction = 0 and rcid.VendorType = 3  and rcid.TransactionProcessingStatus != 'Invalidated' 
                ) src
                PIVOT
                (
                    sum([Quantity])
                    for ItemShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
                ) piv	 	
            ) piv
            group by piv.ClaimId, piv.TransactionId, piv.PeriodName, piv.RegistrationNumber
        ) as transactionoutBound on transactionoutBound.TransactionId = t.ID and transactionoutBound.ClaimId = c.id
        where v.VendorType = 3 
        and (@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.StartDate  <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
    ) as main_Delivered
    group by main_Delivered.claimID
)tireDelivered on ClaimIDs.claimID = tireDelivered.claimID
left join
(
    SELECT 
    main_Adjusted.claimID,
    sum(main_Adjusted.TotalT) as TotalT_Adjustments,
    sum(main_Adjusted.PLT) as PLT_Adjustments,
    sum(main_Adjusted.MT) as MT_Adjustments,
    sum(main_Adjusted.AGLS) as AGLS_Adjustments,
    sum(main_Adjusted.IND) as IND_Adjustments,
    sum(main_Adjusted.SOTR) as SOTR_Adjustments,
    sum(main_Adjusted.MOTR) as MOTR_Adjustments,
    sum(main_Adjusted.LOTR) as LOTR_Adjustments,
    sum(main_Adjusted.GOTR) as GOTR_Adjustments
    FROM
    (
        select  
        distinct c.ID as 'claimID',
        coalesce(InventoryAdjustment.TotalT,0) as 'TotalT',
        coalesce(InventoryAdjustment.PLT,0) 'PLT',
        coalesce(InventoryAdjustment.MT,0) 'MT',
        coalesce(InventoryAdjustment.AGLS,0) 'AGLS',
        coalesce(InventoryAdjustment.IND,0) 'IND',
        coalesce(InventoryAdjustment.SOTR,0) 'SOTR',
        coalesce(InventoryAdjustment.MOTR,0) 'MOTR',
        coalesce(InventoryAdjustment.LOTR,0) 'LOTR',
        coalesce(InventoryAdjustment.GOTR,0) 'GOTR'
        from claim c
        inner join [Period] p on p.id = c.ClaimPeriodID
        inner join [Vendor] v on v.id = c.ParticipantID 
        left join claimdetail cd on c.id = cd.claimid 
        left join [transaction] t on t.id = cd.TransactionId 
        left outer join
        (
            select piv.ClaimId, (sum(PLT) + sum(MT) + sum(AGLS) +sum(IND) +sum(SOTR) +sum(MOTR) +sum(LOTR) +sum(GOTR)) as [TotalT], 
                sum([PLT]) as [PLT], sum([MT]) as [MT], sum([AGLS]) as [AGLS], sum([IND]) as [IND], sum([SOTR]) as [SOTR], sum([MOTR]) as [MOTR], sum([LOTR]) as [LOTR], sum([GOTR]) as [GOTR]
            from
            (
                select piv.ClaimId,
                    coalesce([PLT],0) as 'PLT', 
                    coalesce([MT],0) as 'MT', 
                    coalesce([AGLS],0) as 'AGLS', 
                    coalesce([IND],0) as 'IND', 
                    coalesce([SOTR],0) as 'SOTR', 
                    coalesce([MOTR],0) as 'MOTR', 
                    coalesce([LOTR],0) as 'LOTR', 
                    coalesce([GOTR],0) as 'GOTR'
                from
                (
                    select adj.ClaimId[ClaimId], adj.ID[AdjID], adjI.ItemId[AdjItemId], i.ShortName[ItemShortName], SUM(adjI.QtyAdjustment) [Quantity]
                    from ClaimInventoryAdjustment adj join ClaimInventoryAdjustItem adjI on adjI.ClaimInventoryAdjustmentId=adj.ID join Item i on adjI.ItemId=i.ID
                    where adj.AdjustmentType=2 or adj.AdjustmentType=3
                    group by adj.ClaimId, adj.ID, adjI.ItemId, i.ShortName 

                ) src
                PIVOT
                (
                    sum([Quantity])
                    for ItemShortName in ([PLT], [MT], [AGLS], [IND], [SOTR], [MOTR], [LOTR], [GOTR])
                ) piv	 	
            ) piv
            group by piv.ClaimId
        ) as InventoryAdjustment on InventoryAdjustment.ClaimId = c.id
        where v.VendorType = 3 
        and (@startDateVar is null or @startDateVar <= p.StartDate) AND (@endDateVar is null or p.StartDate  <= @endDateVar)  AND (@registrationNumberVar is null or v.Number = @registrationNumberVar)
    ) as main_Adjusted
    group by main_Adjusted.claimID
)tireAdjusted on ClaimIDs.claimID = tireAdjusted.claimID

END
GO
