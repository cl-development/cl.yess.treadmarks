﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [tmdb]
GO

declare @CreateBy nvarchar(1000);
declare @dt datetime;
set @CreateBy = 'System'
set @dt = GETDATE()
--user access permission control for Admin/Rates and Weights  --display order:23xxxx
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Rates And Weights' and ResourceScreen = 'Admin/Rates And Weights' and Name = 'Admin/Rates And Weights' and DisplayOrder = 230000)
BEGIN

update AppResource set Name = 'Admin/Rates And Weights', ResourceMenu = 'Admin/Rates And Weights', ResourceScreen = 'Admin/Rates And Weights' where ResourceMenu='Admin/Rates' and ResourceScreen = 'Admin/Rates' and Name = 'Admin/Rates' and DisplayOrder = 230000

insert into [AppResource]	(Name,						ResourceMenu,			ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
			VALUES			
				('Rates And Weights',				'Admin/Rates And Weights',	'Rates And Weights',		1,				231000,			@CreateBy,	@dt,		@dt),
				('Tire Stewardship Fees',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231010,			@CreateBy,	@dt,		@dt),
				('Remittance Penalty',				'Admin/Rates And Weights',	'Rates And Weights',		2,				231011,			@CreateBy,	@dt,		@dt),
				('Collection Allowances',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231012,			@CreateBy,	@dt,		@dt),
				('Transportation Incentives',		'Admin/Rates And Weights',	'Rates And Weights',		2,				231013,			@CreateBy,	@dt,		@dt),
				('Processing Incentives',			'Admin/Rates And Weights',	'Rates And Weights',		2,				231014,			@CreateBy,	@dt,		@dt),
				('Manufacturing Incentives',		'Admin/Rates And Weights',	'Rates And Weights',		2,				231015,			@CreateBy,	@dt,		@dt),
				('Estimated Weights',				'Admin/Rates And Weights',	'Rates And Weights',		2,				231016,			@CreateBy,	@dt,		@dt)
END

--user access permission control for Admin/Company Branding/Terms and Conditions --display order:24xxxx
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Company Branding')
BEGIN
insert into [AppResource]	
	(Name,						ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Company Branding',	'Admin/Company Branding',		'Admin/Company Branding',	0,				240000,			@CreateBy,	@dt,		@dt),
	('Company Information',		'Admin/Company Branding',		'Company Information',		1,				241000,			@CreateBy,	@dt,		@dt),
	('Terms and Conditions',	'Admin/Company Branding',		'Terms & Conditions',		1,				241100,			@CreateBy,	@dt,		@dt)
END

--user access permission control for Admin/Application Settings --display order:25xxxx
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Application Settings')
BEGIN
insert into [AppResource]	
	(Name,							ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Application Settings',	'Admin/Application Settings',	'Admin/Application Settings',	0,			250000,			@CreateBy,	@dt,		@dt)	
END

--user access permission control for Admin/Account Thresholds --display order:26xxxx
if NOT EXISTS (select * from AppResource where ResourceMenu = 'Admin/Account Thresholds')
BEGIN
insert into [AppResource]	
	(Name,							ResourceMenu,					ResourceScreen,				ResourceLevel,	DisplayOrder,	CreateBy,	CreateDate,	UpdateDate)
	VALUES			
	('Admin/Account Thresholds',	'Admin/Account Thresholds',		'Admin/Account Thresholds',	0,				260000,			@CreateBy,	@dt,		@dt)	
END
go

--display order:27xxxx
update AppResource set DisplayOrder=270000 where Name='Admin/Announcements' and ResourceMenu='Admin/Announcements' and ResourceScreen='Admin/Announcements'; 
go
--create permission for new appResource Admin/Rates and Weights
DECLARE  @AppResourceExistingRatesId INT, @AppPermissionId INT, @AppResUsersId INT, @iCounter INT, @NewRateResId INT, @RoleId INT;
DECLARE @getNewRateResIDs CURSOR
DECLARE @getRoleIds CURSOR

set @AppResourceExistingRatesId = (select Id from AppResource where ResourceMenu='Admin/Rates And Weights' and ResourceScreen='Admin/Rates And Weights' and Name='Admin/Rates And Weights') --fetch existing [admin/rate] id
SET @getNewRateResIDs = CURSOR FOR select Id from [AppResource] where ResourceMenu='Admin/Rates And Weights' and ResourceLevel > 0 --new [AppResource] inserted
SET @getRoleIds = CURSOR FOR select  rp.RoleId, rp.AppPermissionId from RolePermission rp join AppPermission per on rp.AppPermissionId=per.Id where AppResourceId=@AppResourceExistingRatesId 
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId, @AppPermissionId
WHILE @@FETCH_STATUS = 0
BEGIN
	OPEN @getNewRateResIDs FETCH NEXT FROM @getNewRateResIDs INTO @NewRateResId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @NewRateResId and rp.RoleId = @RoleId)
		BEGIN
			insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @NewRateResId, @AppPermissionId)
			--select  @RoleId, @NewRateResId, @AppPermissionId, @iCounter 
		END
		FETCH NEXT
		FROM @getNewRateResIDs INTO @NewRateResId
		set @iCounter = @iCounter + 1;
	END
	CLOSE @getNewRateResIDs
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId, @AppPermissionId
END

CLOSE @getRoleIds

DEALLOCATE @getNewRateResIDs
DEALLOCATE @getRoleIds
go

--create permission for new appResource new Admin menu items

Declare @superAdminID int, @newResourceID int, @iCounter int, @RoleId int, @iNoAccess int, @iEditSave int
Declare @getNewSourceIDs cursor
DECLARE @getRoleIds CURSOR
set @superAdminID=(select ID from role where name='Super Admin')
SET @getRoleIds = CURSOR FOR select ID from [Role]
set @getNewSourceIDs=cursor for select id from appresource where ResourceMenu in ('Admin/Company Branding', 'Admin/Account Thresholds', 'Admin/Application Settings')
select @iNoAccess = Id from AppPermission where Name = 'NoAccess'
select @iEditSave = Id from AppPermission where Name = 'EditSave'
set @iCounter = 1

OPEN @getRoleIds FETCH NEXT FROM @getRoleIds INTO @RoleId
WHILE @@FETCH_STATUS = 0
BEGIN
	OPEN @getNewSourceIDs FETCH NEXT FROM @getNewSourceIDs INTO @newResourceID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF not EXISTS 
		(SELECT rp.Id, rp.AppResourceId[resID], res.Name FROM [RolePermission] rp join [AppPermission] per on rp.AppPermissionId=per.Id join [Role] ro on rp.RoleId=ro.ID join AppResource res on rp.AppResourceId=res.Id 
		WHERE rp.AppResourceId = @newResourceID and rp.RoleId = @RoleId)
		BEGIN
			if (@RoleId = @superAdminID) 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iEditSave)
					--select  @RoleId, @newResourceID,  @iCounter[counter],'admin' 
				end 
			else 
				begin 
					insert into [RolePermission] (RoleId, AppResourceId, AppPermissionId) VALUES	(@RoleId, @newResourceID, @iNoAccess)
					--select  @RoleId, @newResourceID,  @iCounter[counter] 
				end
		END
		FETCH NEXT
		FROM @getNewSourceIDs INTO @newResourceID
		set @iCounter = @iCounter + 1;
	END
	CLOSE @getNewSourceIDs
	FETCH NEXT
	FROM @getRoleIds INTO @RoleId
END

CLOSE @getRoleIds

DEALLOCATE @getNewSourceIDs
DEALLOCATE @getRoleIds
go