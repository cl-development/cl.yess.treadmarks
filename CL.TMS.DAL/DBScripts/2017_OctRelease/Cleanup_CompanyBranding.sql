select  v.Reason, REPLACE(v.Reason, ' OTS ', ' '), v.* from VendorActiveHistory v where v.Reason like '%OTS%'

select  v.Reason, REPLACE(v.Reason, ' OTS ', ' '), v.* from CustomerActiveHistory v where v.Reason like '%OTS%'

update VendorActiveHistory set Reason=REPLACE(Reason, ' OTS ', ' ') where Reason like '%OTS%';

update CustomerActiveHistory set Reason=REPLACE(Reason, ' OTS ', ' ') where Reason like '%OTS%'


update TypeDefinition set [Description]=REPLACE([Description], ' OTS ', ' ') where code='subcollector';

update TypeDefinition set [Description]=REPLACE([Description], ' OTS ', ' ') where Description like '%OTS%' and code='ApprovedOTS' and Category='TireOrigin';

update TypeDefinition set [Description]=REPLACE([Description], ' an OTS ', ' a ') where Description like '%an OTS%' and code='TireRims' and Category='MaterialType';