USE [tmdb]
GO

/****** Object:  StoredProcedure [dbo].[sp_InitializeClaimPeriod_Collector]    Script Date: 9/11/2017 4:56:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Tony Cai>
-- Create date: <Oct 29, 2015>
-- Description:	Initialize claim period for the current year
-- =============================================
CREATE PROCEDURE [dbo].[sp_InitializeClaimPeriod_Collector] 
 @claimYear as int
AS
BEGIN
	Declare @AddedKey int;
	Declare @ItemRate float;
	Declare @counter int;
	
	SET NOCOUNT ON;
	IF NOT EXISTS (select top 1 * from period where PeriodType=2 and YEAR(startdate)=@claimYear)
    Begin
		--Insert Collector claim period
		
		--Insert Q1
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Jan.-Mar.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-01-01' as datetime), cast(cast(@claimYear as varchar)+'-03-31' as datetime), cast(cast(@claimYear as varchar)+'-04-01' as datetime),cast(cast(@claimYear as varchar)+'-06-30' as datetime), 1,1, 0, 0);
	
		--Add Rates for this quarter
		SELECT @AddedKey = @@IDENTITY;
		
		set @counter=65;
		while @counter <= 72
		begin
				--Get current effective ItemRate for this item
				select @ItemRate = ItemRate from Rate r where r.ClaimType=2 and ItemID=@counter and
					PeriodID in 
					(select id from Period p where p.PeriodType=2 and GETDATE() between StartDate and EndDate);			

				INSERT INTO "Rate" ("ItemID", "ClaimType", "PeriodID", "ItemRate", "EffectiveStartDate", "EffectiveEndDate") VALUES (@counter, 2, @AddedKey, @ItemRate, '2009-01-01', '9999-12-31');
				
			set @counter=@counter+1;
		end 

		
		--Insert Q2
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Apr.-Jun.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-04-01' as datetime), cast(cast(@claimYear as varchar)+'-06-30' as datetime), cast(cast(@claimYear as varchar)+'-07-01' as datetime),cast(cast(@claimYear as varchar)+'-09-30' as datetime),1,1, 0, 0);

		SELECT @AddedKey = @@IDENTITY;
		
		set @counter=65;
		while @counter <= 72
		begin
				--Get current effective ItemRate for this item
				select @ItemRate = ItemRate from Rate r where r.ClaimType=2 and ItemID=@counter and
					PeriodID in 
					(select id from Period p where p.PeriodType=2 and GETDATE() between StartDate and EndDate);			

				INSERT INTO "Rate" ("ItemID", "ClaimType", "PeriodID", "ItemRate", "EffectiveStartDate", "EffectiveEndDate") VALUES (@counter, 2, @AddedKey, @ItemRate, '2009-01-01', '9999-12-31');
				
			set @counter=@counter+1;
		end 

		--Insert Q3
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Jul.-Sep.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-07-01' as datetime), cast(cast(@claimYear as varchar)+'-09-30' as datetime), cast(cast(@claimYear as varchar)+'-10-01' as datetime),cast(cast(@claimYear as varchar)+'-12-31' as datetime),1,1, 0, 0);
	
		SELECT @AddedKey = @@IDENTITY;
		
		set @counter=65;
		while @counter <= 72
		begin
				--Get current effective ItemRate for this item
				select @ItemRate = ItemRate from Rate r where r.ClaimType=2 and ItemID=@counter and
					PeriodID in 
					(select id from Period p where p.PeriodType=2 and GETDATE() between StartDate and EndDate);			

				INSERT INTO "Rate" ("ItemID", "ClaimType", "PeriodID", "ItemRate", "EffectiveStartDate", "EffectiveEndDate") VALUES (@counter, 2, @AddedKey, @ItemRate, '2009-01-01', '9999-12-31');
				
			set @counter=@counter+1;
		end 

		--Insert Q4
		insert into Period(PeriodType, ShortName, StartDate,EndDate,SubmitStart,SubmitEnd,EnableSubmit,ShowPeriod,IsAdjustment,IsSpecialEvent)
		values(2,'Oct.-Dec.'+cast(@claimYear as varchar),cast(cast(@claimYear as varchar)+'-10-01' as datetime), cast(cast(@claimYear as varchar)+'-12-31' as datetime), cast(cast(@claimYear+1 as varchar)+'-01-01' as datetime),cast(cast(@claimYear as varchar)+'-03-31' as datetime),1,1, 0, 0);

		SELECT @AddedKey = @@IDENTITY;
		
		set @counter=65;
		while @counter <= 72
		begin
				--Get current effective ItemRate for this item
				select @ItemRate = ItemRate from Rate r where r.ClaimType=2 and ItemID=@counter and
					PeriodID in 
					(select id from Period p where p.PeriodType=2 and GETDATE() between StartDate and EndDate);			

				INSERT INTO "Rate" ("ItemID", "ClaimType", "PeriodID", "ItemRate", "EffectiveStartDate", "EffectiveEndDate") VALUES (@counter, 2, @AddedKey, @ItemRate, '2009-01-01', '9999-12-31');
				
			set @counter=@counter+1;
		end 


		-- Update SubmitStart and SubmitEnd for Collector
		update Period set SubmitStart=DATEADD(dd,1,EndDate), SubmitEnd=DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,DATEADD(mm,3,EndDate))+1,0)) where PeriodType=2 and YEAR(startdate)=@claimYear;
		
	End
END





GO

