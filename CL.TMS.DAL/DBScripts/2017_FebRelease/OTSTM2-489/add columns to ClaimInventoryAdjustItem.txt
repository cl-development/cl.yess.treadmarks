ALTER TABLE "ClaimInventoryAdjustItem"
	ADD "QtyBeforeAdjustment" INT NULL;

ALTER TABLE "ClaimInventoryAdjustItem"
	ADD "QtyAfterAdjustment" INT NULL;