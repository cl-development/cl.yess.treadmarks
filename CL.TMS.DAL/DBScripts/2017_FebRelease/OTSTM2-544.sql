﻿--== OTSTM2-544 begin ==--
IF COL_LENGTH('[Transaction]', 'TransactionAdjustmentID') IS NULL
BEGIN
    ALTER TABLE [Transaction]
    ADD [TransactionAdjustmentID] int null  CONSTRAINT FK_Transaction_TransactionAdjustment FOREIGN KEY (TransactionAdjustmentID) REFERENCES TransactionAdjustment(ID)
END
GO
update [Transaction] set [Transaction].[TransactionAdjustmentID] = adj.ID 
from [TransactionAdjustment] adj join [Transaction] t on adj.OriginalTransactionId=t.ID where adj.Status = 'Accepted'
GO
--== OTSTM2-544 end ==--