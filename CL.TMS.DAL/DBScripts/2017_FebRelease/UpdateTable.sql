﻿USE [tmdb]

--== OTSTM2-543 begin ==--
update TypeDefinition
set Description = 'Tires removed from purchased vehicles/equipment recycling (i.e Auto Dismantlers)'
where Category='TireOriginFieldLevelValidation' and DefinitionValue=2;
go

insert into TypeDefinition (Name,Code,Description,Category,DefinitionValue,DisplayOrder) 
values ('SubCollector','SubCollector','Tires were collected from my approved OTS Sub-Collector(s)','TireOriginFieldLevelValidation',4,4);
go
--== OTSTM2-543 end ==--

--== OTSTM2-647 begin ==--
update Period set [ShortName] = REPLACE([ShortName], ' ', '.')
where ShortName like '% %'
GO
--== OTSTM2-647 end ==--

--== OTSTM2-485 begin ==--
alter table TSFClaim add AdjustmentTotal decimal(18,3) null
go

alter table TSFClaimInternalPaymentAdjustment add [Status] nvarchar(50) not null default 'Open'
go
--== OTSTM2-485 end ==--

--== OTSTM2-652 begin ==--
insert into GpiAccount (id, account_number,description,distribution_type_id,registrant_type_ind) values (174, '4005-90-00-99','Overall Adjustment - Payments Internal Adjustments',9,'S')
insert into GpiAccount (id, account_number,description,distribution_type_id,registrant_type_ind) values (175, '4004-90-00-99','Audit Assessment - Payments Internal Adjustments',9,'S')
go
--== OTSTM2-652 end ==--

--== OTSTM2-523 begin ==--
ALTER TABLE ClaimNote ALTER COLUMN [Note] VARCHAR (MAX) NULL;
ALTER TABLE TSFRemittanceNote ALTER COLUMN [Note] VARCHAR (MAX) NULL;
ALTER TABLE ClaimInternalAdjustmentNote ALTER COLUMN [Note] VARCHAR (MAX) NULL;
ALTER TABLE TSFClaimInternalAdjustmentNote ALTER COLUMN [Note] VARCHAR (MAX) NULL;

go
--== OTSTM2-523 end ==--

--== OTSTM2-756 rename column name from ClaimId to TSFClaimId ==--
EXEC sp_RENAME 'TSFClaimInternalPaymentAdjustment.ClaimId' , 'TSFClaimId', 'COLUMN'

--== OTSTM2-544 begin ==--
IF COL_LENGTH('[Transaction]', 'TransactionAdjustmentID') IS NULL
BEGIN
    ALTER TABLE [Transaction]
    ADD [TransactionAdjustmentID] int null  CONSTRAINT FK_Transaction_TransactionAdjustment FOREIGN KEY (TransactionAdjustmentID) REFERENCES TransactionAdjustment(ID)
END
GO
update [Transaction] set [Transaction].[TransactionAdjustmentID] = adj.ID 
from [TransactionAdjustment] adj join [Transaction] t on adj.OriginalTransactionId=t.ID where adj.Status = 'Accepted'
GO
--== OTSTM2-544 end ==--