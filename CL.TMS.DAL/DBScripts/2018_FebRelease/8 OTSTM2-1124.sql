﻿use tmdb

--add a new column "distribution_type_id" into FIAccount Table
IF COL_LENGTH('[FIAccount]', 'distribution_type_id') IS NULL
BEGIN
    ALTER TABLE [FIAccount]
    ADD [distribution_type_id] bigint null;    
END
go

--remove empty after AccountDescription
If exists (select * from FIAccount where AccountDescription = 'MI- Stabilization Pyt ON ROAD PLT ')
Begin
	update FIAccount set AccountDescription = 'MI- Stabilization Pyt ON ROAD PLT'  where AccountDescription = 'MI- Stabilization Pyt ON ROAD PLT '
end
go

If exists (select * from FIAccount where AccountDescription = 'PI- Stabilization Pyt ON ROAD PLT ')
Begin
	update FIAccount set AccountDescription = 'PI- Stabilization Pyt ON ROAD PLT'  where AccountDescription = 'PI- Stabilization Pyt ON ROAD PLT '
End
go

--add distribution_type_id
update FIAccount set distribution_type_id = 3  where AccountDescription = 'Stewards Receivable'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'PLT Class 1'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'MT Class 2'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 3'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 4'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 5'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 6'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 7'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 8'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 9'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 10'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'OTR Class 11'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 12'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 13'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 14'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 15'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 16'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 17'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Solid&Resilient Class 18'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Overall adjustment - Payments Internal Adjustments'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Audit Assessment - Payments Internal Adjustments'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Audit Penalty will - Payments Internal Adjustments'
update FIAccount set distribution_type_id = 9  where AccountDescription = 'Tax Payable (HST)'
update FIAccount set distribution_type_id = 2  where AccountDescription = 'Accounts (Collector) Payable'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - PLT'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - MT'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - AG/LS'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - IND' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - SOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - MOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - LOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'CA - GOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- TM Adjustments' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - PLT HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - MT HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - AG/LS HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - IND HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - SOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - MOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - LOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'CA - GOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- TM Adjustment HST' 
update FIAccount set distribution_type_id = 2  where AccountDescription = 'Accounts (Hauler) Payable' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium PLT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium MT'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium AG/LS' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium IND' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium SOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium MOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium LOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- Premium GOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI-DOT MOTR'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI-DOT LOTR'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI-DOT GOTR'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'TI- TM Adjustments'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- PLT HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- MT HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- AG/LS HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- IND HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- SOTR HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- MOTR HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI-LOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- GOTR HST' 
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- DOT OTR HST'
update FIAccount set distribution_type_id = 10  where AccountDescription = 'TI- TM Adjustment HST' 
update FIAccount set distribution_type_id = 2  where AccountDescription = 'Accounts (Processor) Payable' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt ON ROAD PLT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt ON ROAD MT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD AG&LS' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD IND' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD SOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD MOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD LOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD GOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - PLT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - MT'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - AG/LS'
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - IND' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - SOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - MOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - LOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI - GOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'PI TM Adjustments' 
update FIAccount set distribution_type_id = 2  where AccountDescription = 'Accounts (Manufacturer) Payable' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt ON ROAD PLT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt ON ROAD MT' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD AG&LS' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD IND' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD SOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD MOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD LOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI- Stabilization Pyt OFF ROAD GOTR' 
update FIAccount set distribution_type_id = 6  where AccountDescription = 'MI TM Adjustments' 
go

--add a new column "Name" into FIAccount Table
IF COL_LENGTH('[FIAccount]', 'Name') IS NULL
BEGIN
    ALTER TABLE [FIAccount]
    ADD [Name] varchar(500) null;    
END
go

--copy all data from AccountLabel column to Name column
UPDATE FIAccount SET Name = AccountLabel
go

--rename the Name to avoid duplicated Name
update FIAccount set Name = 'PLT HST'  where AccountDescription = 'CA - PLT HST' 
update FIAccount set Name = 'MT HST'  where AccountDescription = 'CA - MT HST' 
update FIAccount set Name = 'AG/LS HST'  where AccountDescription = 'CA - AG/LS HST' 
update FIAccount set Name = 'IND HST'  where AccountDescription = 'CA - IND HST' 
update FIAccount set Name = 'SOTR HST'  where AccountDescription = 'CA - SOTR HST' 
update FIAccount set Name = 'MOTR HST'  where AccountDescription = 'CA - MOTR HST' 
update FIAccount set Name = 'LOTR HST'  where AccountDescription = 'CA - LOTR HST' 
update FIAccount set Name = 'GOTR HST'  where AccountDescription = 'CA - GOTR HST' 
update FIAccount set Name = 'Internal Payment Adjustments HST'  where AccountDescription = 'TI- TM Adjustment HST' 

update FIAccount set Name = 'PLT HST'  where AccountDescription = 'TI- PLT HST' 
update FIAccount set Name = 'MT HST'  where AccountDescription = 'TI- MT HST' 
update FIAccount set Name = 'AG/LS HST'  where AccountDescription = 'TI- AG/LS HST' 
update FIAccount set Name = 'IND HST'  where AccountDescription = 'TI- IND HST' 
update FIAccount set Name = 'SOTR HST'  where AccountDescription = 'TI- SOTR HST' 
update FIAccount set Name = 'MOTR HST'  where AccountDescription = 'TI- MOTR HST' 
update FIAccount set Name = 'LOTR HST'  where AccountDescription = 'TI-LOTR HST' 
update FIAccount set Name = 'GOTR HST'  where AccountDescription = 'TI- GOTR HST' 
update FIAccount set Name = 'Internal Payment Adjustments HST'  where AccountDescription = 'TI- TM Adjustment HST' 

update FIAccount set Name = 'DOR MOTR'  where AccountDescription = 'TI-DOT MOTR' 
update FIAccount set Name = 'DOR LOTR'  where AccountDescription = 'TI-DOT LOTR' 
update FIAccount set Name = 'DOR GOTR'  where AccountDescription = 'TI-DOT GOTR' 
update FIAccount set Name = 'DOT-OTR HST'  where AccountDescription = 'TI- DOT OTR HST' 

update FIAccount set Name = 'PLT Stabilization'  where AccountDescription = 'PI- Stabilization Pyt ON ROAD PLT'
update FIAccount set Name = 'MT Stabilization'  where AccountDescription = 'PI- Stabilization Pyt ON ROAD MT' 
update FIAccount set Name = 'AG/LS Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD AG&LS' 
update FIAccount set Name = 'IND Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD IND' 
update FIAccount set Name = 'SOTR Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD SOTR' 
update FIAccount set Name = 'MOTR Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD MOTR' 
update FIAccount set Name = 'LOTR Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD LOTR' 
update FIAccount set Name = 'GOTR Stabilization'  where AccountDescription = 'PI- Stabilization Pyt OFF ROAD GOTR' 
go

--change "Tax Receivable " to "Tax Receivable"
If exists (select * from FIAccount where AccountDescription = 'Tax Payable (HST)' and AdminFICategoryID = 1 and Name = 'Tax Receivable ')
Begin 
	update FIAccount set Name = 'Tax Receivable'  where AccountDescription = 'Tax Payable (HST)' and AdminFICategoryID = 1 and Name = 'Tax Receivable '
End
go

--rename the values of Name column for Steward Tire Supply to be consistent with the ShortName column in Item Table
update FIAccount set Name = 'C1'  where AccountDescription = 'PLT Class 1' 
update FIAccount set Name = 'C2'  where AccountDescription = 'MT Class 2' 
update FIAccount set Name = 'C3'  where AccountDescription = 'OTR Class 3' 
update FIAccount set Name = 'C4'  where AccountDescription = 'OTR Class 4' 
update FIAccount set Name = 'C5'  where AccountDescription = 'OTR Class 5' 
update FIAccount set Name = 'C6'  where AccountDescription = 'OTR Class 6' 
update FIAccount set Name = 'C7'  where AccountDescription = 'OTR Class 7' 
update FIAccount set Name = 'C8'  where AccountDescription = 'OTR Class 8' 
update FIAccount set Name = 'C9'  where AccountDescription = 'OTR Class 9' 
update FIAccount set Name = 'C10'  where AccountDescription = 'OTR Class 10' 
update FIAccount set Name = 'C11'  where AccountDescription = 'OTR Class 11' 
update FIAccount set Name = 'C12'  where AccountDescription = 'Solid&Resilient Class 12'
update FIAccount set Name = 'C13'  where AccountDescription = 'Solid&Resilient Class 13' 
update FIAccount set Name = 'C14'  where AccountDescription = 'Solid&Resilient Class 14' 
update FIAccount set Name = 'C15'  where AccountDescription = 'Solid&Resilient Class 15' 
update FIAccount set Name = 'C16'  where AccountDescription = 'Solid&Resilient Class 16' 
update FIAccount set Name = 'C17'  where AccountDescription = 'Solid&Resilient Class 17' 
update FIAccount set Name = 'C18'  where AccountDescription = 'Solid&Resilient Class 18' 
go

