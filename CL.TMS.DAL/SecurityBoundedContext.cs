﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DAL
{
    public class SecurityBoundedContext : TMBaseContext<SecurityBoundedContext>
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserPasswordArchive> UsersPasswordsArchive { get; set; }

        public DbSet<VendorUser> VendorUsers { get; set; }
        public DbSet<CustomerUser> CustomerUsers { get; set; }

        //New user security
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserApplicationsWorkflow> UserApplicationsWorkflows { get; set; }
        public DbSet<UserClaimsWorkflow> UserClaimsWorkflows { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany<Role>(r => r.Roles).WithMany(u => u.Users).Map(m => { m.ToTable("UserRole"); m.MapLeftKey("UserID"); m.MapRightKey("RoleID"); });
        }
    }
}
