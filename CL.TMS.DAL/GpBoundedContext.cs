﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class GpBoundedContext : TMBaseContext<GpBoundedContext>
    {
        public GpBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<BankInformation> BankInformations { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<GpChequeBook> GpChequeBooks { get; set; }
        public DbSet<GpiAccount> GpiAccounts { get; set; }
        public DbSet<GpiBatch> GpiBatches { get; set; }
        public DbSet<GpiBatchEntry> GpiBatchEntries { get; set; }
        public DbSet<GpFiscalYear> GpFiscalYears { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<RrOtsRmApply> RrOtsRmApplies { get; set; }
        public DbSet<RrOtsRmCashReceipt> RrOtsRmCashReceipts { get; set; }
        public DbSet<RrOtsRmCustomer> RrOtsRmCustomers { get; set; }
        public DbSet<RrOtsPmVendor> RrOtsPmVendors { get; set; }
        public DbSet<RrOtsPmTransaction> RrOtsPmTransactions { get; set; }
        public DbSet<RrOtsPmTransDistribution> RrOtsPmTransDistributions { get; set; }
        public DbSet<RrOtsRmTransaction> RrOtsRmTransactions { get; set; }
        public DbSet<RrOtsRmTransDistribution> RrOtsRmTransDistributions { get; set; }
        public DbSet<TSFClaim> TSFClaims { get; set; }
        public DbSet<TSFClaimDetail> TSFClaimDetails { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<FIAccount> FIAccounts { get; set; } //OTSTM2-1124
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.ClaimsAmountCredit).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.AdjustmentTotal).HasPrecision(18, 3);
            modelBuilder.Entity<Claim>().Property(c => c.TotalTax).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPayment>().Property(c => c.Rate).HasPrecision(18, 3);
            modelBuilder.Entity<ClaimPayment>().Property(c => c.Weight).HasPrecision(18, 4);

            //Rates
            modelBuilder.Entity<Rate>().Property(c => c.ItemRate).HasPrecision(18, 3);

            //TSFClaim
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalTSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesGst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.ApplicableTaxesHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePostHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TSFDuePreHst).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Penalties).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PenaltiesManually).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Interest).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.TotalRemittancePayable).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.PaymentAmount).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.BalanceDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaim>().Property(c => c.Credit).HasPrecision(18, 3);

            //RrOtsRmTransDistribution
            modelBuilder.Entity<RrOtsRmTransDistribution>().Property(c => c.DEBITAMT).HasPrecision(19, 2);

            //TSFClaimDetail
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.TSFDue).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFRate).HasPrecision(18, 3);
            modelBuilder.Entity<TSFClaimDetail>().Property(c => c.CreditTSFDue).HasPrecision(18, 3);

            modelBuilder.Entity<Claim>().Ignore(c => c.ClaimPaymentSummary);
            modelBuilder.Entity<Claim>().Ignore(c => c.ClaimSummary);
            modelBuilder.Entity<Claim>().Ignore(c => c.ClaimProcess);

            //Fixed 635 concurrency issues
            modelBuilder.Entity<GpiBatch>().Ignore(t => t.Rowversion);
            modelBuilder.Entity<GpiBatchEntry>().Ignore(t => t.Rowversion);

            //Financial Integration Account
            modelBuilder.Entity<FIAccount>().Ignore(t => t.Rowversion);

        }
    }
}
