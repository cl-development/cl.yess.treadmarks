﻿using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.DAL
{
    public class MessageBoundedContext: TMBaseContext<MessageBoundedContext>
    {
        public MessageBoundedContext()
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<UserPasswordArchive>();
            modelBuilder.Ignore<Role>();
            modelBuilder.Ignore<UserClaimsWorkflow>();
            modelBuilder.Ignore<UserApplicationsWorkflow>();
        }
    }
}
