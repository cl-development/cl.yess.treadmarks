﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Framework.DAL;
using System.Data.Entity.Infrastructure;

namespace CL.TMS.DAL
{
    public class EmailSettingsBoundedContext : TMBaseContext<EmailSettingsBoundedContext>
    {
        public EmailSettingsBoundedContext()
        {

        }

        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailElement> EmailElements { get; set; }
        public DbSet<Setting> AppSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            
        }
    }
}
