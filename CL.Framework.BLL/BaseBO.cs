﻿using System.Collections.Generic;
using CL.Framework.Common;
using CL.Framework.RuleEngine;
using FluentValidation.Results;

namespace CL.Framework.BLL
{
    /// <summary>
    /// Base business object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseBO
    {
        #region Public Methods
        /// <summary>
        /// Business rule set
        /// </summary>
        public IRuleSet BusinessRuleSet { get; set; }

        /// <summary>
        /// Storing all validation results from executing the corresponding business rules
        /// </summary>
        public ValidationResult BusinessRuleExecutioResult { get; set; }

        /// <summary>
        /// Execute business rule
        /// </summary>
        /// <param name="target"></param>
        /// <param name="ruleContext"></param>
        public void ExecuteBusinessRules<T>(T target, IDictionary<string, object> ruleContext = null)
        {
            if (BusinessRuleSet != null)
            {
                BusinessRuleExecutioResult=BusinessRuleSet.Execute(target,ruleContext);
            }
        }

        /// <summary>
        /// Create the associated business rule set through the strategy design pattern
        /// </summary>
        /// <param name="businessRuleSetStrategy"></param>
        public virtual void CreateBusinessRuleSet(IBusinessRuleSetStrategy businessRuleSetStrategy)
        {
            BusinessRuleSet = businessRuleSetStrategy.CreateBusinessRuleSet();
        }

        #endregion 
    }
}
