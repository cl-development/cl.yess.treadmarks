﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CL.Framework.RuleEngine;

namespace CL.Framework.BLL
{
    /// <summary>
    /// A strategy interface for business rule set
    /// </summary>
    public interface IBusinessRuleSetStrategy
    {
        BaseRuleSet CreateBusinessRuleSet();
    }
}
