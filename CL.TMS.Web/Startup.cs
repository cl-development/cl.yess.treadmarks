﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CL.TMS.Web.Startup))]
namespace CL.TMS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
