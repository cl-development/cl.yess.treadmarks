﻿using AutoMapper;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.ExceptionHandling;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Resources;
using CL.TMS.Security;
using CL.TMS.Security.Interfaces;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.ExceptionHelper;
using CL.TMS.UI.Common.Helper;
using CL.TMS.UI.Common.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Common;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.Security.Authorization;
using System.Security.Claims;
using System.Configuration;
using CL.TMS.Configuration;
using CL.Framework.Logging;
using CL.TMS.UI.Common.UserInterface;

namespace CL.TMS.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        #region Fields

        private const string DefaultUserNameCookie = "TMS_LastSignedInUser";
        private const string PasswordResetRequiredAction = "1";
        private const string ForgotPasswordAction = "2";
        private string uploadRepositoryPath;

        private IAuthentication authentication;
        private IUserService userService;
        private IRegistrantService registrantService;
        private IClaimService claimService;

        #endregion Fields

        #region Constructors

        public AccountController(IAuthentication authentication, IUserService userService, IRegistrantService registrantService, IClaimService claimService)
        {
            this.authentication = authentication;
            this.userService = userService;
            this.registrantService = registrantService;
            this.claimService = claimService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        #endregion Constructors

        #region Action Methods

        /// <summary>
        ///  GET: /Account/Login
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            AuthenticationUser model = new AuthenticationUser();
            if (null != System.Web.HttpContext.Current.Request.QueryString)
            {
                if (null != System.Web.HttpContext.Current.Request.QueryString["username"])
                {
                    model.UserName = Request.QueryString["username"];
                    if (!string.IsNullOrWhiteSpace(Request.QueryString["vendorid"]))
                    {
                        model.VendorId = Convert.ToInt32(Request.QueryString["vendorid"]);
                    }
                }
            }

            //OTSTM2 - 1006, was not showing any error while redirecting to this page
            if (TempData["ModelState"] != null)
            {
                ModelState.Merge(TempData["ModelState"] as ModelStateDictionary);
            }

            //Remmeber me
            if (!string.IsNullOrWhiteSpace(RememberUserName))
            {
                model.UserName = RememberUserName;
                model.RememberMe = true;
            }
            return View(model);
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(AuthenticationUser model, string returnUrl = "/")
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = authentication.VerifyUser(model.UserName, model.Password, true, Convert.ToInt16(SiteSettings.Instance.GetSettingValue("UserPolicy.MaxLoginAttemptBeforeLock")));
#if DEBUG
            result.Status = SignInStatus.Success;
#endif
            switch (result.Status)
            {
                case SignInStatus.Success:
                    {
                        //Clear Global search session
                        Session["SelectedVendor"] = null;
                        if (result.UserVendorClaims.Count == 1)
                        {
                            var claim = result.UserVendorClaims.First();
                            var vendorId = Convert.ToInt32(claim.Value);
                            ParticipantPageHelper.SetCurrentVendor(registrantService, vendorId);
                        }
                        else
                        {
                            //User has multiple vendors, to check the caller is from welcome letter
                            if (model.VendorId > 0)
                            {
                                ParticipantPageHelper.SetCurrentVendor(registrantService, model.VendorId);
                            }
                        }

                        ViewBag.loadTopMenu = true;
                        TempData["ViewData"] = ViewData;

                        return SignInRedirectResult(model, returnUrl, result);
                    }
                case SignInStatus.PasswordResetRequired:
                    {
                        var userId = result.ID;
                        int i = 5;
                        int PasswordResetTokenExpiresInMinutes = (int.TryParse(UI.Common.Helper.SiteSettings.Instance.GetSettingValue("UserPolicy.PasswordResetTokenExpiresInMinutes"), out i)) ? i : 5;
                        var token = SetPasswordResetToken(userId, model.UserName, PasswordResetTokenExpiresInMinutes);
                        return RedirectToAction("ResetPassword", new { token = token, uid = userId, returnUrl = returnUrl, ops = PasswordResetRequiredAction });
                    }
                case SignInStatus.LockedOut:
                    {
                        ModelState.AddModelError("", MessageResource.AccountIsLocked);
                        return View(model);
                    }
                case SignInStatus.Inactive:
                    {
                        ModelState.AddModelError("", @MessageResource.AccountIsInactive);
                        return View(model);
                    }
                default:
                    {
                        ModelState.AddModelError("", @MessageResource.InvalidLoginAttempt);
                        return View(model);
                    }
            }
        }

        [RedirectAction(false)]
        public ActionResult Logout()
        {
            ClearSessions();
            AuthenticationManager.SignOut();
            ModelState.Clear();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPassword model)
        {
            ViewBag.StatusMessage = string.Empty;
            if (ModelState.IsValid)
            {
                var emailAddress = model.EmailAddress.Trim();
                var user = userService.FindUserByEmailAddress(emailAddress);
                if (user == null)
                {
                    ModelState.AddModelError("", MessageResource.InvalidEmailAddress);
                }
                else
                {
                    if (user.Inactive)
                    {
                        ModelState.AddModelError("", MessageResource.InactiveUserAccount);
                    }
                    else
                    {
                        int i = 5;
                        int PasswordResetTokenExpiresInMinutes = (int.TryParse(UI.Common.Helper.SiteSettings.Instance.GetSettingValue("UserPolicy.PasswordResetTokenExpiresInMinutes"), out i)) ? i : 5;

                        var token = Base64.EncodeBase64Url(SetPasswordResetToken(user.ID, user.UserName, PasswordResetTokenExpiresInMinutes));
                        var dominName = SiteSettings.Instance.DomainName;
                        var passwordResetAbsolutePath = SiteSettings.Instance.PasswordResetAbsolutePath;

                        var url = string.Format("{0}{1}?token={2}&uid={3}&ops={4}", dominName, passwordResetAbsolutePath, token, user.ID, ForgotPasswordAction);

                        if (EmailContentHelper.IsEmailEnabled("PasswordReset"))
                            ViewBag.StatusMessage = ComposeAndSendEMail(url, emailAddress);
                        else
                            ViewBag.StatusMessage = "This email is disabled. Please contact us for assistance.";
                    }
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RequestAccount()
        {
            var model = new RequestAccountViewModel();
            return View(model);
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult RequestAccount(RequestAccountViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var vendorReference = registrantService.GetVendorByNumber(viewModel.RegistrationNumber);
                if (vendorReference == null)
                {
                    viewModel.Message = "Registration number is invalid";
                    return View(viewModel);
                }
                else
                {
                    //Check if invitation is sent already
                    var invitationIsExisting = userService.EmailIsExistingForParticipant(viewModel.PrimaryEmail, vendorReference.VendorId, vendorReference.IsVendor);
                    if (invitationIsExisting)
                    {
                        viewModel.Message = "User account is already existing";
                        return View(viewModel);
                    }
                    else
                    {
                        if (vendorReference.PrimaryContact == null)
                        {
                            viewModel.Message = "Primary contact information is mising";
                            return View(viewModel);
                        }
                        else
                        {
                            if (string.Compare(vendorReference.PrimaryContact.Email, viewModel.PrimaryEmail, true) == 0)
                            {
                                //Send participant admin account invitation
                                if (EmailContentHelper.IsEmailEnabled("WelcomeLetterPrimaryContact"))
                                {
                                    SendWelcomeEmailToVendor(vendorReference);
                                    viewModel.Message = "Invitation is sent to the primary contact";
                                }
                                else
                                {
                                    viewModel.Message = "This email is disabled. Please contact us for assistance.";
                                }
                            }
                            else
                            {
                                viewModel.Message = "Primary contact email is not matched";
                                return View(viewModel);
                            }
                        }
                    }
                }
                viewModel.IsReadOnly = true;
                return View(viewModel);
            }
            else
            {
                ModelState.AddModelError("Invalid", "Invalid data");
                return View(viewModel);
            }
        }

        private void SendWelcomeEmailToVendor(VendorReference vendorReference)
        {
            string emailBodyLocation = string.Empty;
            string subject = string.Empty;
            var applicationEmailModel = new ApplicationEmailModel();
            applicationEmailModel.RegistrationNumber = vendorReference.RegistrationNumber;
            applicationEmailModel.Email = vendorReference.PrimaryContact.Email;
            applicationEmailModel.BusinessName = vendorReference.BusinessName;
            applicationEmailModel.BusinessLegalName = vendorReference.BusinessName;
            applicationEmailModel.Address1 = vendorReference.PrimaryContact.Address.Address1;
            applicationEmailModel.City = vendorReference.PrimaryContact.Address.City;
            applicationEmailModel.ProvinceState = vendorReference.PrimaryContact.Address.Province;
            applicationEmailModel.PostalCode = vendorReference.PrimaryContact.Address.PostalCode;
            applicationEmailModel.Country = vendorReference.PrimaryContact.Address.Country;
            var userIp = Request.UserHostAddress;
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            //OTSTM2-637
            //emailBodyLocation = SiteSettings.Instance.GetSettingValue("Email.RequestAccountEmailTempl");
            //subject = MessageResource.RequestAccountEmailSubject;

            if (vendorReference.IsVendor)
            {
                var redirectUrl = string.Empty;
                if (vendorReference.Status == "Approved")
                    redirectUrl = "/System/FinancialIntegration/BankingInformation/" + vendorReference.VendorId;
                var invitation = ModelAdapter.ToInvitation(vendorReference.PrimaryContact.FirstName, vendorReference.PrimaryContact.LastName, vendorReference.PrimaryContact.Email, userIp, expireDate, siteUrl, redirectUrl, vendorReference.VendorId, 0);
                userService.CreateParticipantDefaultUserInvitation(invitation);
                applicationEmailModel.InvitationToken = invitation.InviteGUID;
            }
            else
            {
                var invitation = ModelAdapter.ToCustomerInvitation(vendorReference.PrimaryContact.FirstName, vendorReference.PrimaryContact.LastName, vendorReference.PrimaryContact.Email, userIp, expireDate, siteUrl, null, vendorReference.VendorId, 0);
                invitation.CreatedBy = invitation.EmailAddress;
                userService.CreateParticipantDefaultUserInvitation(invitation);
                applicationEmailModel.InvitationToken = invitation.InviteGUID;
            }

            string bccEmailAddr = SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath");

            if (EmailContentHelper.IsEmailEnabled("WelcomeLetterPrimaryContact"))
                ComposeAndSendApplicationApprovalEmail(applicationEmailModel, null, bccEmailAddr);
        }

        private bool ComposeAndSendApplicationApprovalEmail(ApplicationEmailModel model, string ccEmailAddr = null, string bccEmailAddr = null)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(model.Email);
            MailMessage message = new MailMessage(from, to) { };

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, emailBodyLocation);
            //string emailBody = System.IO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("WelcomeLetterPrimaryContact");
            string subject = EmailContentHelper.GetSubjectByName("WelcomeLetterPrimaryContact");

            var inviteUrl = string.Format("{0}{1}{2}", SiteSettings.Instance.DomainName, "/System/Invitation/AcceptInvitation/", model.InvitationToken);

            DateTime expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.AcceptInvitationUrl, inviteUrl)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.InvitationExpirationDateTime, expireDate.ToString(TreadMarksConstants.DateFormat) + " at " + expireDate.ToString("hh:mm tt"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            Task.Factory.StartNew(() =>
            {
                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, ccEmailAddr, bccEmailAddr, subject, emailBody, model, null, alternateViewHTML);
            });
            return true;
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string token = "", long uid = 0, string ops = "", string returnUrl = "/")
        {
            var user = new UserPasswordResetModel();

            if (TempData["ModelState"] != null)
            {
                ModelState.Merge(TempData["ModelState"] as ModelStateDictionary);
            }

            var userName = userService.FindUserNameByUserId(uid);
            if (!ValidatePasswordReset(token, uid, ops, userName))
            {
                TempData["ModelState"] = ModelState;
                return RedirectToAction("Login");
            }

            user.ID = uid;
            user.UserName = userName;
            user.Token = token;
            user.Ops = ops;

            return View(user);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(UserPasswordResetModel userPasswordRestModel, string returnUrl = "/")
        {
            if (ModelState.IsValid)
            {
                if (
                    !ValidatePasswordReset(userPasswordRestModel.Token, userPasswordRestModel.ID,
                        userPasswordRestModel.Ops, userPasswordRestModel.UserName))
                {
                    TempData["ModelState"] = ModelState;
                    return RedirectToAction("Login");
                }

                try
                {
                    int i = 0;
                    int passwordExpirationInDays = (int.TryParse(UI.Common.Helper.SiteSettings.Instance.GetSettingValue("UserPolicy.PasswordExpiresInDays"), out i)) ? i : 180;

                    var user = userService.ResetPassword(userPasswordRestModel.UserName, userPasswordRestModel.Password, passwordExpirationInDays);

                    if (user != null)
                    {
                        var authenticationUser = Mapper.Map<User, AuthenticationUser>(user);
                        authenticationUser.UserVendorClaims = userService.LoadUserVendorClaims(user.ID);
                        return SignInRedirectResult(authenticationUser, returnUrl, authenticationUser);
                    }

                    return Redirect(returnUrl);
                }
                catch (CLValidaitonException ex)
                {
                    var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                    if (rethrow)
                    {
                        //throw;
                    }
                    HandleClientException.AddValidationErrors(ModelState, ex);
                    TempData["ModelState"] = ModelState;
                    return RedirectToAction("ResetPassword", new { token = userPasswordRestModel.Token, uid = userPasswordRestModel.ID, ops = userPasswordRestModel.Ops }); ;
                }
            }
            else
            {
                TempData["ModelState"] = ModelState;
                return RedirectToAction("ResetPassword", new { token = userPasswordRestModel.Token, uid = userPasswordRestModel.ID, ops = userPasswordRestModel.Ops }); ;
            }
        }

        #endregion Action Methods

        #region Private Methods

        private bool ValidatePasswordReset(string token, long uid, string ops, string userName)
        {
            //OTSTM2 - 1006, was showing multiple/same messages as they were not cleared for same state
            if (ModelState.ContainsKey(""))
                ModelState[""].Errors.Clear();

            if (string.IsNullOrEmpty(token))
            {
                ModelState.AddModelError("", MessageResource.InvalidPasswordRestRequest);
                return false;
            }

            var userPasswordResetTokenInfo = userService.GetPasswordResetToken(uid);
            if (userPasswordResetTokenInfo.Count == 0)
            {
                if (ops != "0")
                {
                    ModelState.AddModelError("", MessageResource.ResetPasswordDone);
                    return false;
                }
                ModelState.AddModelError("", MessageResource.NoResetRequestFound);
                return false;
            }

            var passwordResetToken = userPasswordResetTokenInfo[0];
            if (string.IsNullOrEmpty(passwordResetToken))
            {
                if (ops != "0")
                {
                    ModelState.AddModelError("", MessageResource.ResetPasswordDone);
                    return false;
                }
                ModelState.AddModelError("", MessageResource.NoResetRequestFound);
                return false;
            }

            var modelPasswordRestTokenDecoded = Base64.DecodeBase64Url(token);
            if (modelPasswordRestTokenDecoded != passwordResetToken)
            {
                if (ops != "0")
                {
                    ModelState.AddModelError("", MessageResource.PasswordResetExpired);
                    return false;
                }
                ModelState.AddModelError("", MessageResource.UnableProcessPasswordReset);
                return false;
            }

            if (!string.Equals(modelPasswordRestTokenDecoded, passwordResetToken))
            {
                ModelState.AddModelError("", MessageResource.UnableProcessPasswordReset);
                return false;
            }

            var passwordResetTokenSecretKey = userPasswordResetTokenInfo[1];
            if (string.IsNullOrEmpty(passwordResetTokenSecretKey))
            {
                ModelState.AddModelError("", MessageResource.UnableProcessPasswordReset);
                return false;
            }

            var tokenData = Token.Decrypt(modelPasswordRestTokenDecoded, passwordResetTokenSecretKey);

            var userData = tokenData.Split(',');

            if (string.IsNullOrEmpty(userData[0]))
            {
                ModelState.AddModelError("", MessageResource.InvalidPasswordResetUserName);
                return false;
            }

            if (string.IsNullOrEmpty(userData[1]))
            {
                ModelState.AddModelError("", MessageResource.InvalidPasswordResetSession);
                return false;
            }

            if (string.IsNullOrEmpty(userName))
            {
                userName = userService.FindUserNameByUserId(uid);
            }

            var tokenUserName = userData[0];
            if (!string.Equals(tokenUserName, userName))
            {
                ModelState.AddModelError("", MessageResource.PasswordResetUserNameNotMatched);
                return false;
            }

            DateTime sessionTimeOut;
            DateTime.TryParse(userData[1], out sessionTimeOut);

            if (sessionTimeOut <= DateTime.UtcNow)
            {
                ModelState.AddModelError("", MessageResource.PasswordResetSessionTimeout);
                return false;
            }

            return true;
        }

        private string SetPasswordResetToken(long userId, string userName, int expirationInMinutes)
        {
            string secretKey = Token.GenerateRandomSecretKey();

            string token = CreatePasswordResetToken(userName, secretKey, expirationInMinutes);

            userService.AddPasswordResetToken(userId, token, secretKey);

            return token;
        }

        private string CreatePasswordResetToken(string userName, string secretKey, int tokenExpirationInMinutes)
        {
            string tokenData = userName + "," + DateTime.UtcNow.AddMinutes(tokenExpirationInMinutes);

            string token = Token.Encrypt(tokenData, secretKey);

            return token;
        }

        private ActionResult SignInRedirectResult(AuthenticationUser model, string returnUrl, AuthenticationUser result)
        {
            //Add stewardtype to the current user
            result.StewardType = SiteSettings.Instance.SiteStewardType;

            var id = authentication.CreateIdentity(result, DefaultAuthenticationTypes.ApplicationCookie);

            //Fixed bug (Malar) for user mapping to multiple vendors
            if (result.UserVendorClaims.Count > 1 && model.VendorId > 0)
            {
                var userRoles = userService.LoadParticipantUserRoles(result.ID, SecurityContextHelper.CurrentVendor.VendorId, SecurityContextHelper.CurrentVendor.IsVendor);
                userRoles.ForEach(c =>
                {
                    if (!id.Claims.Contains(c))
                    {
                        id.AddClaim(c);
                    }
                });
            }

            //Fixed OTSTM2-452 
            if (result.UserVendorClaims.Count == 1)
            {
                var claim = result.UserVendorClaims.First();
                var vendorId = Convert.ToInt32(claim.Value);
                ParticipantPageHelper.SetCurrentVendor(registrantService, vendorId);
            }

            var isPersistent = model.RememberMe;

            if (isPersistent)
            {
                RememberUserName = model.UserName;
            }
            else
            {
                RememberUserName = string.Empty;
            }

            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true, ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(UserPolicy.NonPersistentAuthTicketExpiresInMinutes) }, id);

            return Redirect(returnUrl);
        }

        private string RememberUserName
        {
            get
            {
                string userName = string.Empty;

                string userNameCookieName = DefaultUserNameCookie;
                var userNameCookie = HttpContext.Request.Cookies.Get(userNameCookieName);

                if (userNameCookie != null)
                {
                    userName = userNameCookie.Value;
                }

                return userName;
            }
            set
            {
                string userNameCookieName = DefaultUserNameCookie;
                int userNameCookieExpiresInDays = UserPolicy.RememberUserNameExpiresInDays != 0 ? UserPolicy.RememberUserNameExpiresInDays : 365;

                var userCookie = HttpContext.Request.Cookies.Get(userNameCookieName);

                if (userCookie == null && string.IsNullOrEmpty(value)) { }
                else if (userCookie == null && !string.IsNullOrEmpty(value))
                {
                    HttpCookie cookie = new HttpCookie(userNameCookieName);
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddDays(userNameCookieExpiresInDays);
                    HttpContext.Response.Cookies.Add(cookie);
                }
                else if (userCookie != null && string.IsNullOrEmpty(value))
                {
                    HttpContext.Request.Cookies.Remove(userNameCookieName);
                    userCookie.Expires = DateTime.Now.AddDays(-1d);
                    HttpContext.Response.Cookies.Set(userCookie);
                }
                else if (userCookie.Value != value)
                {
                    userCookie.Value = value;
                    userCookie.Expires = DateTime.Now.AddDays(userNameCookieExpiresInDays);
                    HttpContext.Response.Cookies.Set(userCookie);
                }
                else
                {
                    userCookie.Expires = DateTime.Now.AddDays(userNameCookieExpiresInDays);
                    HttpContext.Response.Cookies.Set(userCookie);
                }
            }
        }


        private string ComposeAndSendEMail(string url, string emailAddress)
        {
            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, EmailTemplateResource.EmailTemplateLocation);
            //string emailBody = System.IO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("PasswordReset");
            string subject = EmailContentHelper.GetSubjectByName("PasswordReset");

            emailBody = emailBody.Replace(EmailTemplateResource.siteURL, url)
                            .Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString());

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, EmailTemplateResource.RethinkTireImage), MediaTypeNames.Image.Jpeg);
            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, EmailTemplateResource.FollowOnTwitterImage), MediaTypeNames.Image.Jpeg);
            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, EmailTemplateResource.TreadMarkLogoImage), MediaTypeNames.Image.Jpeg);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            //rethinkTiresLog.ContentId = EmailTemplateResource.RethinkTireID; ;
            //followUsOnTwitter.ContentId = EmailTemplateResource.FollowOnTwitterID;
            treadMarksLogo.ContentId = EmailTemplateResource.TreadMarkLogoID;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.EmailSmtpPort),
                SiteSettings.Instance.EmailSmtpServer,
                SiteSettings.Instance.EmailSmtpUserName,
                SiteSettings.Instance.EmailSmtpPassword,
                SiteSettings.Instance.EmailDataElementBegin,
                SiteSettings.Instance.EmailDataElementEnd,
                SiteSettings.Instance.EmailDefaultEmailTemplateDirectory,
                SiteSettings.Instance.EmailDefaultFrom,
                Convert.ToBoolean(SiteSettings.Instance.EmailUseSSL));

            Task.Factory.StartNew(() =>
            {
                email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailAddress, null, null, subject, emailBody, null, null, alternateViewHTML);
            });

            return string.Format(MessageResource.PasswordResetConfirmation, emailAddress);
        }

        private void ClearSessions()
        {
            if (Session["SelectedVendor"] != null)
            {
                Session["SelectedVendor"] = null;
            }
            if (Session["PreviousSearchWord"] != null)
            {
                Session["PreviousSearchWord"] = null;
            }
            if (Session["IsAdminLogin"] != null)
            {
                Session["IsAdminLogin"] = null;
            }
        }

        #endregion Private Methods

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Helpers

        #region AdminMenu        
        [HttpPost]
        public ActionResult AdminLoginConfirmation(string password)
        {
            var userLoginName = SecurityContextHelper.CurrentUser.Email;
            var result = authentication.ConfirmPassword(userLoginName, password);

#if DEBUG
            result = true;
#endif
            if (result)
            {
                var user = System.Web.HttpContext.Current.User as ClaimsPrincipal;
                //Clear Global search session
                Session["SelectedVendor"] = null;
                Session["IsAdminLogin"] = user;
            }
            else
            {
                Session["IsAdminLogin"] = null;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClearSessionForAdminLogin()
        {
            if (Session["IsAdminLogin"] != null)
            {
                Session["IsAdminLogin"] = null;
            }

            return Json("true", JsonRequestBehavior.AllowGet);
        }
        public ActionResult AdminEmptyPage()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.loadTopMenu = true;
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage");
            }
        }

        public ActionResult AdminLoginPage()
        {
            if (!SecurityContextHelper.IsStaff())
            {
                return RedirectToAction("Logout", "Account");
            }
            else
            {
                if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminAccess) > CL.TMS.Security.SecurityResultType.ReadOnly)
                {
                    ControllerContext.RouteData.DataTokens["area"] = "System";
                    return View();
                }
                else
                {
                    return RedirectToAction("Logout", "Account");
                }
            }

        }
        #endregion
    }
}