﻿using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.PDFGenerator.Models;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Communication.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using System.Text;
using CL.TMS.DataContracts.ViewModel.Common;
using System.Security.Claims;
using Microsoft.Owin.Security;
using CL.TMS.Web.Infrastructure;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.DataContracts.ViewModel.Reporting;
using CL.TMS.UI.Common.UserInterface;
using System.Threading;
using CL.TMS.ClaimCalculator;
using System.Threading.Tasks;
using CL.Framework.Logging;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.ServiceContracts.ProcessorServices;
using CL.TMS.ServiceContracts.RPMServices;
using System.IO;
using CL.TMS.Security;
using CL.TMS.DataContracts.ViewModel.Collector;
using CL.TMS.DataContracts.ViewModel.Processor;

namespace CL.TMS.Web.Controllers
{
    public class DefaultController : BaseController
    {
        private readonly IRegistrantService registrantService;
        private readonly IApplicationService applicationService;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserService userService;
        private readonly IClaimService claimService;
        private readonly IMessageService messageService;
        private readonly IHaulerClaimService haulerClaimService;
        private readonly IReportingService reportingService;
        private readonly ICollectorClaimService collectorClaimService;
        private readonly IProcessorClaimService processorClaimService;
        private readonly IRPMClaimService rpmClaimService;
        public DefaultController(IRegistrantService registrantService, IApplicationService applicationService, IFileUploadService fileUploadService, IUserService userService, IClaimService claimService, IMessageService messageService, IHaulerClaimService haulerClaimService, IReportingService reportingService,
            ICollectorClaimService collectorClaimService, IProcessorClaimService processorClaimService, IRPMClaimService rpmClaimService)
        {
            this.registrantService = registrantService;
            this.applicationService = applicationService;
            this.fileUploadService = fileUploadService;
            this.userService = userService;
            this.claimService = claimService;
            this.messageService = messageService;
            this.haulerClaimService = haulerClaimService;
            this.reportingService = reportingService;
            this.collectorClaimService = collectorClaimService;
            this.processorClaimService = processorClaimService;
            this.rpmClaimService = rpmClaimService;
        }

        //Notification Load test only
        public string UpdateStatusForAllClaims()
        {
            //1. Get all under review claims select * from Claim where status='Under Review' and AssignToUserId is not null;
            var claimIds = claimService.GetAllUnderReviewClaimsIds().ToList();
            //2. Updated status to 3   if ((fromStatus == 2) && (toStatus == 3)), reviewedBy --db get one user 
            claimIds.ForEach(c =>
            {
                UpdateClaimWorkflowStatus(c, 24881, 2, 3); //24881: Frank Staff
            });

            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";

        }

        //Notification Load test only
        /// <summary>
        /// Only for the scenario from 2 to 3
        /// </summary>
        /// <param name="claimId"></param>
        /// <param name="reviewedBy"></param>
        /// <param name="fromStatus"></param>
        /// <param name="toStatus"></param>
        private void UpdateClaimWorkflowStatus(int claimId, long reviewedBy, int fromStatus, int toStatus)
        {
            claimService.UpdateClaimWorkflowStatus(claimId, reviewedBy, fromStatus, toStatus);
            //Add notification
            var assigner = SecurityContextHelper.CurrentUser;
            var assignee = userService.FindUserByUserId(reviewedBy);
            var assigneeUser = new UserReference
            {
                Id = assignee.ID,
                Email = assignee.Email,
                FirstName = assignee.FirstName,
                LastName = assignee.LastName,
                UserName = assignee.UserName
            };

            var claim = claimService.FindClaimById(claimId);
            AddNotification(assigner, assigneeUser, claim, false);
        }

        //Notification Load test only
        private void AddNotification(UserReference assignerUser, UserReference assigneeUser, StaffAllClaimsModel claim, bool isReviewNotApprove)
        {
            var notification = new Notification();
            notification.Assigner = assignerUser.UserName;
            notification.AssignerName = string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName);
            notification.AssignerInitial = string.Format("{0}{1}", assignerUser.FirstName[0], assignerUser.LastName[0]);
            notification.Assignee = assigneeUser.UserName;
            notification.AssigneeName = string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName);
            notification.CreatedTime = DateTime.Now;
            notification.NotificationType = TreadMarksConstants.ClaimNotification;
            notification.Status = "Unread";  //Add by Frank
            switch (claim.Type)
            {
                case ClaimType.Collector:
                    notification.NotificationArea = TreadMarksConstants.Collector;
                    break;
                case ClaimType.Hauler:
                    notification.NotificationArea = TreadMarksConstants.Hauler;
                    break;
                case ClaimType.Processor:
                    notification.NotificationArea = TreadMarksConstants.Processor;
                    break;
                case ClaimType.RPM:
                    notification.NotificationArea = TreadMarksConstants.RPM;
                    break;
            }
            notification.ObjectId = claim.ClaimId;
            if (isReviewNotApprove)
            {
                notification.Message = string.Format("<strong>{0}</strong> assigned <strong>{1}</strong> - <strong>{2} Claim</strong> to you for <strong>review</strong>.", notification.AssignerName, claim.RegNumber, claim.Period);
            }
            else
            {
                notification.Message = string.Format("<strong>{0}</strong> assigned <strong>{1}</strong> - <strong>{2} Claim</strong> to you for <strong>approval</strong>.", notification.AssignerName, claim.RegNumber, claim.Period);
            }

            messageService.AddNotification(notification);

            //Publish notification event
            var notificationPayload = new NotificationPayload
            {
                NotificationId = notification.ID,
                Receiver = notification.Assignee
            };
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<NotificationEvent>().Publish(notificationPayload);
        }

        public string RecalculateMissingClaimReport()
        {
            var claimIds = claimService.GetMissingReportDataClaimIds();
            foreach (var claimId in claimIds)
            {
                var claimReportPayload = new ClaimReportPayload { ClaimId = claimId, TransactionId = 0, IsSynchronous = true };
                DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);
            }
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }

        public string RecalculateClaimReportAll(int? vendorType = null)
        {
            int iCounter = 0;
            var claimIds = claimService.GetAllClaimsToReCalculate(vendorType);
            foreach (var claimId in claimIds)
            {
                var claimReportPayload = new ClaimReportPayload { ClaimId = claimId, TransactionId = 0, IsSynchronous = true };
                DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);
                iCounter++;
            }
            LogManager.LogWarning(string.Format("Recalculate Claim Report All completed:{0} claims, vendor type:{1}", claimIds.Count().ToString(), vendorType != null ? vendorType.ToString() : "All"));
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }
        public ActionResult RecalculateClaimReport(int claimId)
        {
            var claimReportPayload = new ClaimReportPayload { ClaimId = claimId, TransactionId = 0 };
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ClaimInventoryDetailEvent>().Publish(claimReportPayload);
            return Json("Start claim inventory report calculating", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Recalculate(int claimId)
        {
            var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId };
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            return Json("Start claim calculating", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            //default welcome page
            return RedirectToAction("EmptyPage", "Common", new { area = "System" });
        }

        public string RecalculateClaimInventoryForApprovedClaims()
        {
            var claimIds = claimService.GetAllApprovedClaimIdsForInventoryCal();
            claimIds.ForEach(c =>
            {
                claimService.RecalculateClaimInventory(c);
            });
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }

        public string RecalculateVendorInventoryForApprovedClaims(int? vendorId = null)
        {
            var claimIds = claimService.GetAllApprovedClaimIdsForInventoryCal(vendorId);
            claimIds.ForEach(c =>
            {
                claimService.RecalculateVendorInventory(c);
            });
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }

        public string RecalculateVendorInventoryForClaim(int claimId)
        {
            claimService.RecalculateVendorInventory(claimId);
            return string.Format("Recalculating vendor inventory for claim: {0}", claimId);
        }

        public string ReCalculateAll(int? vendorType = null)
        {
            LogManager.LogInfo("Start claim calculation");
            var claimIds = claimService.GetAllClaimsToReCalculate(vendorType);
            Parallel.ForEach(claimIds, c =>
            {
                ClaimCalculationHelper.CalculateClaim(c);
            });
            Task.WaitAll();
            LogManager.LogInfo("Complete claim calculation");
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }


        public string ReCalculateForMissingClaims()
        {
            var claimIds = claimService.GetMissingCollectorClaimIds();
            foreach (var claimId in claimIds)
            {
                var claimCalculationPayload = new ClaimCalculationPayload { ClaimId = claimId, IsSynchronous = true };
                DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ClaimCalculationEvent>().Publish(claimCalculationPayload);
            }
            return "\nClaimIds =" + string.Join(",", claimIds) + "\n";
        }

        //Default/UpdateRegistrantKeywords?registrantType=
        public string UpdateRegistrantKeywords(string registrantType)
        {
            try
            {
                var tmp = -1;
                if (int.TryParse(registrantType, out tmp))
                {
                    registrantService.UpdateRegistrantKeywords(tmp);
                }
                else
                {
                    return "Invalid Parameter for registrantType";
                }
                return "Success";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        [HttpPost]
        [RedirectAction(false)]
        public NewtonSoftJsonResult GlobalSearch(string searchvalue)
        {
            if (Session == null) return new NewtonSoftJsonResult { Data = new { status = false, redirectUrl = "" } };

            var redirectUrl = "";

            if (string.IsNullOrWhiteSpace(searchvalue))
            {
                Session["SelectedVendor"] = null;
                redirectUrl = "/System/Common/EmptyPage";
            }
            else
            {
                var userName = SecurityContextHelper.CurrentUser.UserName.ToLower();
                if (SecurityContextHelper.IsStaff())
                {
                    var vendor = registrantService.GetVendorByNumber(searchvalue);
                    if (vendor != null)
                    {
                        //Keep it in session
                        if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId))
                        {
                            Session["SelectedVendor"] = vendor;
                        }
                        redirectUrl = "/System/Common/EmptyPage";
                    }
                }
                else
                {
                    var userId = SecurityContextHelper.CurrentUser.Id;
                    var vendor = registrantService.GetVendorByNumber(searchvalue, userId, userName);
                    if (vendor != null)
                    {
                        //Keep it in session
                        if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId))
                        {
                            Session["SelectedVendor"] = vendor;
                        }

                        //Refill user roles when switch user for participant user
                        RefillUserRoles(vendor);
                        if (string.IsNullOrWhiteSpace(vendor.RedirectUrl))
                            redirectUrl = "/System/Common/EmptyPage";
                        else
                            redirectUrl = vendor.RedirectUrl;
                    }
                }
            }
            ViewBag.loadTopMenu = true;
            TempData["ViewData"] = ViewData;
            return new NewtonSoftJsonResult { Data = new { status = true, redirectUrl = redirectUrl } };
        }

        private void RefillUserRoles(VendorReference vendor)
        {
            SecurityContextHelper.RemoveCurrentUserRolePermissonClaims();
            var userId = SecurityContextHelper.CurrentUser.Id;
            var userRoles = userService.LoadParticipantUserRoles(userId, vendor.VendorId, vendor.IsVendor);
            SecurityContextHelper.AddUserRolePermissionClaims(userRoles);
            var userIdendtity = this.User.Identity as ClaimsIdentity;
            HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(UserPolicy.NonPersistentAuthTicketExpiresInMinutes)
            }, userIdendtity);

        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodesAlliPadsQRCodes)]
        public void QRCodeSignsGen(string businessName, string regNumber, string userID, string address)
        {
            var vendor = registrantService.GetVendorByNumber(regNumber.Split('-')[0]);
            var vendorAddress = registrantService.GetVendorAddressList(vendor.VendorId).FirstOrDefault(r => r.AddressType == 1);

            var signModel = new SignModel()
            {
                BusinessName = vendor.BusinessName,
                RegistrationNumber = vendor.RegistrationNumber,
                UserID = userID.Trim(),
                Address = string.Format("{0}, {1}, {2}, {3}, {4}", vendorAddress.Address1, vendorAddress.City, vendorAddress.Province, vendorAddress.PostalCode, vendorAddress.Country),
            };

            IPadQRCodeLabelGenerator.SingleSign(signModel);
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodesAlliPadsQRCodes)]
        public void QRCodeSignsGenDummy(string businessName, string regNumber, string userID, string address)
        {            
            var signModel = new SignModel()
            {
                BusinessName = businessName,
                RegistrationNumber = regNumber,
                UserID = userID.Trim(),
                Address = address,
            };

            IPadQRCodeLabelGenerator.SingleSign(signModel);
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodesAlliPadsQRCodes)]
        public void QRCodeBadgeGen(string businessName, string regNumber, string userID)
        {
            var vendor = registrantService.GetVendorByNumber(regNumber.Split('-')[0]);

            var badgeModel = new BadgeModel()
            {
                BusinessName = vendor.BusinessName,
                RegistrationNumber = vendor.RegistrationNumber,
                UserID = userID.Trim()
            };

            IPadQRCodeLabelGenerator.SingleBadge(badgeModel);
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodesAlliPadsQRCodes)]
        public void QRCodeBadgeCollectionGen(List<BadgeModel> badgeModelCollection)
        {
            IPadQRCodeLabelGenerator.BadgeCollection(badgeModelCollection);
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.iPadsQRCodesAlliPadsQRCodes)]
        public void QRCodeBadgeCollection(string searchValue = "", bool activated = true)
        {
            var appUserList = new List<AppUserModel>();
            appUserList = registrantService.GetAppUserList(searchValue.Trim()).ToList();

            var qrCodeBadgeModelList = new List<BadgeModel>();
            foreach (var appUserModel in appUserList)
            {
                if (appUserModel.Active == activated)
                {
                    var badge = new BadgeModel()
                    {
                        BusinessName = appUserModel.VendorName,
                        RegistrationNumber = appUserModel.VendorNumber,
                        UserID = appUserModel.VendorAppUserID.ToString()
                    };

                    qrCodeBadgeModelList.Add(badge);
                }
            }

            if (qrCodeBadgeModelList.Count > 0)
            {
                this.QRCodeBadgeCollectionGen(qrCodeBadgeModelList);
            }
        }

        public void DeviceLabelGen(string deviceId, string businessName, string regNumber, string userID, string barcodeGroup)
        {
            var badgeModel = new DeviceLabelModel()
            {
                DeviceID = deviceId.Trim(),
                BusinessName = businessName.Trim(),
                RegistrationNumber = regNumber.Trim(),
                BarcodeGroup = barcodeGroup.Trim()
            };

            IPadQRCodeLabelGenerator.DeviceLabel(badgeModel);
        }

        private void DeviceLabelCollectionGen(List<DeviceLabelModel> deviceLabelCollection)
        {
            IPadQRCodeLabelGenerator.DeviceLabelCollection(deviceLabelCollection);
        }

        public void DeviceLabelCollection(string searchValue = "", bool activated = true, bool assigned = true)
        {
            var iPadList = new List<IPadModel>();
            if (!string.IsNullOrEmpty(searchValue.Trim()))
            {
                iPadList = registrantService.IPadList(searchValue.Trim()).ToList();
            }
            else
            {
                iPadList = registrantService.IPadList().ToList();

            }

            var iPadModelList = new List<DeviceLabelModel>();
            foreach (var iPadModel in iPadList)
            {
                if (iPadModel.Activated == activated && iPadModel.IsAssigned == assigned)
                {
                    var ipad = new DeviceLabelModel()
                    {
                        BarcodeGroup = "H",
                        DeviceID = iPadModel.Number,
                        BusinessName = iPadModel.AssignedToName,
                        RegistrationNumber = iPadModel.AssignedToNumber,
                    };

                    iPadModelList.Add(ipad);
                }
            }

            if (iPadModelList.Count > 0)
            {
                this.DeviceLabelCollectionGen(iPadModelList);
            }
        }

        public string ApproveLateCollectorClaimForSupport(string registrationNumber, int claimPeriodId)
        {
            claimService.ApproveLateCollectorClaimForSupport(registrationNumber, claimPeriodId);
            return "Done!";
        }

        [AllowAnonymous]
        public ActionResult ApplicationSubmittedPage()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult GetProcessorList()
        {
            var processorList = applicationService.GetProcessorList();

            return Json(processorList, JsonRequestBehavior.AllowGet);
        }

        //OTSTM2-449 backup (manually)
        public string AutoSwitchForAllCustomers()
        {
            var changeBy = (int)AppSettings.Instance.SystemUser.ID;

            registrantService.AutoSwitchForAllCustomers(changeBy);

            return "\nChanged by =" + string.Join(",", changeBy) + "\n";

        }
        #region Internal diagnostic method
        public string TotalInboundOutbound()
        {
            var claimIds = claimService.GetAllClaimsToReCalculateByStatus(2, ClaimStatus.Submitted.ToString()).ToList();//2:collector
            claimIds.AddRange(claimService.GetAllClaimsToReCalculateByStatus(2, "Under Review").ToList());
            claimIds.AddRange(claimService.GetAllClaimsToReCalculateByStatus(2, ClaimStatus.Approved.ToString()).ToList());
            int counter = 0;
            var result = new List<string>();
            claimIds.ForEach(c =>
            {
                bool isTotalInboundOutbound = this.collectorClaimService.IsStaffTotalInboundOutbound(c);
                counter++;
                string sResult;

                if (!isTotalInboundOutbound)
                {
                    sResult = c.ToString();
                    System.Diagnostics.Debug.WriteLine(c.ToString());
                    sResult += ";";
                    result.Add(sResult);
                }
            });
            return string.Join(System.Environment.NewLine, result) + "\n";
        }
        public string FindCollectorClaimInboundOutboundMismatching()
        {
            var claimIds = claimService.GetAllClaimsToReCalculateByStatus(2, ClaimStatus.Submitted.ToString()).ToList();//2:collector
            claimIds.AddRange(claimService.GetAllClaimsToReCalculateByStatus(2, "Under Review").ToList());
            claimIds.AddRange(claimService.GetAllClaimsToReCalculateByStatus(2, ClaimStatus.Approved.ToString()).ToList());
            int counter = 0;
            var result = new List<string>();
            claimIds.ForEach(id =>
            {
                bool inboundOutboundMatchClaimsRule = true;
                string error;
                List<ClaimTireOrigin> tireOrigins = claimService.LoadClaimTireOrigins(id);
                var collectorClaimSummary = new CollectorClaimSummaryModel();

                //Load Tire Origin
                PopulateInbound(collectorClaimSummary, tireOrigins);

                //Load outbound
                List<Item> items = DataLoader.Items;
                List<ClaimDetailViewModel> claimDetails = claimService.LoadClaimDetails(id, items);
                PopulateOutbound(collectorClaimSummary, claimDetails);

                ItemRow TotalInbound = collectorClaimSummary.TotalInbound;
                ItemRow TotalOutbound = collectorClaimSummary.TotalOutbound;

                counter++;
                //CollectorInboundOutboundMatchClaimRule
                bool isNilActivity = (TotalInbound.IsAllFieldsZero && TotalOutbound.IsAllFieldsZero);
                if (!isNilActivity)
                {
                    if (TotalInbound.PLT == TotalOutbound.PLT && TotalInbound.MT == TotalOutbound.MT
                            && TotalInbound.AGLS == TotalOutbound.AGLS && TotalInbound.IND == TotalOutbound.IND
                            && TotalInbound.SOTR == TotalOutbound.SOTR && TotalInbound.MOTR == TotalOutbound.MOTR
                            && TotalInbound.LOTR == TotalOutbound.LOTR && TotalInbound.GOTR == TotalOutbound.GOTR)
                    {
                        inboundOutboundMatchClaimsRule = true;
                    }
                    else
                    {
                        inboundOutboundMatchClaimsRule = false;
                        var claim = claimService.FindClaimById(id);
                        string sResult;
                        sResult = claim.RegNumber + "," + claim.Period + ", " + claim.Status + "," + id.ToString();
                        if (TotalInbound.PLT != TotalOutbound.PLT) sResult += ", PLT";
                        if (TotalInbound.MT != TotalOutbound.MT) sResult += ", MT";
                        if (TotalInbound.AGLS != TotalOutbound.AGLS) sResult += ", AGLS";
                        if (TotalInbound.IND != TotalOutbound.IND) sResult += ", IND";
                        if (TotalInbound.SOTR != TotalOutbound.SOTR) sResult += ", SOTR";
                        if (TotalInbound.MOTR != TotalOutbound.MOTR) sResult += ", MOTR";
                        if (TotalInbound.LOTR != TotalOutbound.LOTR) sResult += ", LOTR";
                        if (TotalInbound.GOTR != TotalOutbound.GOTR) sResult += ", GOTR";
                        sResult += ";";
                        result.Add(sResult);
                        System.Diagnostics.Debug.WriteLine(sResult);
                    }
                }
            });

            return string.Join(System.Environment.NewLine, result) + "\n";
        }

        public ActionResult FindCollectorClaimInboundOutboundMismatchingExport()
        {
            string resultStr = FindCollectorClaimInboundOutboundMismatching().Replace(";", " ");

            resultStr = "VendorNbr," + "claimPeriod," + "claimStatus," + "claimID," + "TireTypes\r\n" + resultStr;
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            var filename = string.Format("CollectorClaimInboundOutboundMismatching{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public string allProcessorsInboundOutboundClosingInventory()
        {
            var allProcessors = claimService.getAllVenderByType(4);

            var result = new List<string>();
            allProcessors.ForEach(c =>
            {
                var allClaimIdsByVendorIDs = claimService.LoadAllClaimIdsByVendorID(c);
                allClaimIdsByVendorIDs.ForEach(i =>
                {
                    var pClaimSummary = processorClaimService.LoadProcessorClaimSummary(c, i);
                    string sResult;
                    sResult = pClaimSummary.ClaimCommonModel.RegistrationNumber + "," +
                                pClaimSummary.ClaimCommonModel.ClaimPeriod.ShortName + "," +
                                pClaimSummary.TotalInbound.ToString() + "," +
                                pClaimSummary.TotalOutbound.ToString() + "," +
                                pClaimSummary.ClosingInventory.ToString() + ";";
                    result.Add(sResult);
                });
            });

            return string.Join(System.Environment.NewLine, result) + "\n"; ;
        }
        public ActionResult ProcessorInboundOutbountClosingInventoryExport()
        {
            string resultStr = allProcessorsInboundOutboundClosingInventory().Replace(";", " ");

            resultStr = "RegisterNumber," + "Period," + "TotalInbound," + "TotalOutbound," + "ClosingInventory\r\n" + resultStr;
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            var filename = string.Format("ProcessorInboundOutbountClosingInventoryExport{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        /// <summary>
        /// this is a copy of CL.TMS.CollectorBLL.CollectorClaimBO.collectorClaimSummary
        /// </summary>
        /// <param name="collectorClaimSummary"></param>
        /// <param name="tireOrigins"></param>
        private void PopulateInbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimTireOrigin> tireOrigins)
        {
            var query = tireOrigins.GroupBy(c => new { c.TireOriginValue })
                    .Select(c => new { Name = c.Key, ItemList = c.ToList() });
            query.ToList().ForEach(c =>
            {
                var tireOriginItemRow = new TireOriginItemRow();
                tireOriginItemRow.TireOriginValue = c.Name.TireOriginValue;
                tireOriginItemRow.TireOrigin = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).Description;
                tireOriginItemRow.DisplayOrder = AppDefinitions.Instance.GetDefinitionByValue(DefinitionCategory.TireOrigin, tireOriginItemRow.TireOriginValue).DisplayOrder;
                c.ItemList.ForEach(i =>
                {
                    var item = DataLoader.TransactionItems.FirstOrDefault(p => p.ID == i.ItemId);
                    var propertyInfo = tireOriginItemRow.GetType().GetProperty(item.ShortName);
                    propertyInfo.SetValue(tireOriginItemRow, i.Quantity, null);
                });
                collectorClaimSummary.InboundList.Add(tireOriginItemRow);
            });

            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            collectorClaimSummary.TotalInbound.PLT = tireOrigins.Where(c => c.ItemId == pltItem.ID).Sum(c => c.Quantity);

            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            collectorClaimSummary.TotalInbound.MT = tireOrigins.Where(c => c.ItemId == mtItem.ID).Sum(c => c.Quantity);

            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            collectorClaimSummary.TotalInbound.AGLS = tireOrigins.Where(c => c.ItemId == aglsItem.ID).Sum(c => c.Quantity);

            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            collectorClaimSummary.TotalInbound.IND = tireOrigins.Where(c => c.ItemId == indItem.ID).Sum(c => c.Quantity);

            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            collectorClaimSummary.TotalInbound.SOTR = tireOrigins.Where(c => c.ItemId == sotrItem.ID).Sum(c => c.Quantity);

            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            collectorClaimSummary.TotalInbound.MOTR = tireOrigins.Where(c => c.ItemId == motrItem.ID).Sum(c => c.Quantity);

            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            collectorClaimSummary.TotalInbound.LOTR = tireOrigins.Where(c => c.ItemId == lotrItem.ID).Sum(c => c.Quantity);

            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            collectorClaimSummary.TotalInbound.GOTR = tireOrigins.Where(c => c.ItemId == gotrItem.ID).Sum(c => c.Quantity);

        }

        /// <summary>
        /// this is a copy of CL.TMS.CollectorBLL.CollectorClaimBO.PopulateOutbound
        /// </summary>
        /// <param name="collectorClaimSummary"></param>
        /// <param name="claimDetails"></param>
        /// <param name="inventoryAdjustments"></param>
        private void PopulateOutbound(CollectorClaimSummaryModel collectorClaimSummary, List<ClaimDetailViewModel> claimDetails)
        {
            #region Load TCR
            var tcrClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR).ToList();
            var pltItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.PLT += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            var mtItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MT += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            var aglsItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.AGLS += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            var indItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.IND += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            var sotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.SOTR += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            var motrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            var lotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            var gotrItem = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            tcrClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.TCR.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region  Load DOT
            var dotClaimDetails = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT).ToList();
            var motrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.MOTR += (int)c.TransactionItems.Where(i => i.ItemID == motrItemDot.ID).Sum(q => q.Quantity);
            });
            var lotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.LOTR += (int)c.TransactionItems.Where(i => i.ItemID == lotrItemDot.ID).Sum(q => q.Quantity);
            });
            var gotrItemDot = DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);
            dotClaimDetails.ForEach(c =>
            {
                collectorClaimSummary.DOT.GOTR += (int)c.TransactionItems.Where(i => i.ItemID == gotrItemDot.ID).Sum(q => q.Quantity);
            });
            #endregion

            #region Load TotalEligible
            var tcrClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & c.IsEligible).ToList();
            int pltEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                pltEligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.PLT = pltEligible;

            int mtEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                mtEligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MT = mtEligible;

            int aglsEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                aglsEligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.AGLS = aglsEligible;

            int indEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                indEligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.IND = indEligible;

            int sotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                sotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.SOTR = sotrEligible;

            int motrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                motrEligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrEligibleDot = 0;
            var dotClaimDetailsEligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & c.IsEligible).ToList();
            dotClaimDetailsEligible.ForEach(c =>
            {
                motrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.MOTR = motrEligible + motrEligibleDot;

            int lotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                lotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                lotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.LOTR = lotrEligible + lotrEligibleDot;

            int gotrEligible = 0;
            tcrClaimDetailsEligible.ForEach(c =>
            {
                gotrEligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrEligibleDot = 0;
            dotClaimDetailsEligible.ForEach(c =>
            {
                gotrEligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalEligible.GOTR = gotrEligible + gotrEligibleDot;

            #endregion

            #region Load TotalIneligible
            var tcrClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.TCR & !c.IsEligible).ToList();
            int pltIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                pltIneligible += (int)c.TransactionItems.Where(i => i.ItemID == pltItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.PLT = pltIneligible;

            int mtIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                mtIneligible += (int)c.TransactionItems.Where(i => i.ItemID == mtItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MT = mtIneligible;

            int aglsIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                aglsIneligible += (int)c.TransactionItems.Where(i => i.ItemID == aglsItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.AGLS = aglsIneligible;

            int indIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                indIneligible += (int)c.TransactionItems.Where(i => i.ItemID == indItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.IND = indIneligible;

            int sotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                sotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == sotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.SOTR = sotrIneligible;

            int motrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            int motrIneligibleDot = 0;
            var dotClaimDetailsIneligible = claimDetails.Where(c => c.TransactionType == TreadMarksConstants.DOT & !c.IsEligible).ToList();
            dotClaimDetailsIneligible.ForEach(c =>
            {
                motrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == motrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.MOTR = motrIneligible + motrIneligibleDot;

            int lotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            int lotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                lotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == lotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.LOTR = lotrIneligible + lotrIneligibleDot;

            int gotrIneligible = 0;
            tcrClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligible += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            int gotrIneligibleDot = 0;
            dotClaimDetailsIneligible.ForEach(c =>
            {
                gotrIneligibleDot += (int)c.TransactionItems.Where(i => i.ItemID == gotrItem.ID).Sum(q => q.Quantity);
            });
            collectorClaimSummary.TotalIneligible.GOTR = gotrIneligible + gotrIneligibleDot;

            #endregion

            #region Load TotalOutbound
            collectorClaimSummary.TotalOutbound.PLT = collectorClaimSummary.TotalEligible.PLT + collectorClaimSummary.TotalIneligible.PLT;
            collectorClaimSummary.TotalOutbound.MT = collectorClaimSummary.TotalEligible.MT + collectorClaimSummary.TotalIneligible.MT;
            collectorClaimSummary.TotalOutbound.AGLS = collectorClaimSummary.TotalEligible.AGLS + collectorClaimSummary.TotalIneligible.AGLS;
            collectorClaimSummary.TotalOutbound.IND = collectorClaimSummary.TotalEligible.IND + collectorClaimSummary.TotalIneligible.IND;
            collectorClaimSummary.TotalOutbound.SOTR = collectorClaimSummary.TotalEligible.SOTR + collectorClaimSummary.TotalIneligible.SOTR;
            collectorClaimSummary.TotalOutbound.MOTR = collectorClaimSummary.TotalEligible.MOTR + collectorClaimSummary.TotalIneligible.MOTR;
            collectorClaimSummary.TotalOutbound.LOTR = collectorClaimSummary.TotalEligible.LOTR + collectorClaimSummary.TotalIneligible.LOTR;
            collectorClaimSummary.TotalOutbound.GOTR = collectorClaimSummary.TotalEligible.GOTR + collectorClaimSummary.TotalIneligible.GOTR;
            #endregion

        }
        public string FindHaulerClaimWrongTireCount()
        {
            var claimIds = claimService.GetAllClaimsToReCalculate(3).ToList();

            //Load Tire Count Balance in Claim Summary Page
            var uiResult = new List<HaulerClaimTireCountVM>();
            claimIds.ForEach(c =>
            {
                var claim = claimService.FindClaimByClaimId(c);
                var inventoryItems = haulerClaimService.LoadInventoryItems(claim.ParticipantId, claim.ClaimPeriod.EndDate);
                var itemRow = PopulateClaimTireBalance(inventoryItems);
                var haulerClaimTireCountVM = new HaulerClaimTireCountVM();
                haulerClaimTireCountVM.VendorId = claim.ParticipantId;
                haulerClaimTireCountVM.ClaimId = claim.ID;
                haulerClaimTireCountVM.ClaimStatus = claim.Status;
                haulerClaimTireCountVM.StartDate = claim.ClaimPeriod.StartDate;
                haulerClaimTireCountVM.RegistrationNumber = claim.Participant.Number;
                haulerClaimTireCountVM.ItemTireCount = itemRow;
                uiResult.Add(haulerClaimTireCountVM);
            });

            //Load Hauler Movement Report for all hauler claims
            var reportDetailsViewModel = new ReportDetailsViewModel();
            reportDetailsViewModel.StartDate = Convert.ToDateTime("2016-04-01");
            reportDetailsViewModel.EndDate = DateTime.Now;
            var reportResult = reportingService.GetHaulerTireMovementReportVM(reportDetailsViewModel).ToList();

            //Find the claim which has wrong tire count balance
            var result = new List<string>();

            uiResult.ForEach(c =>
            {
                var startDate = c.StartDate.Date;
                var registrationNumber = c.RegistrationNumber;
                var findReportResults = reportResult.Where(r => r.HaulerRegistrationNumber == registrationNumber && r.StartDate.Date <= startDate).ToList();
                var reportPLT = findReportResults.Sum(r => r.PLT_Collected + r.NPLT_Delivered + r.PLT_Adjustments);
                var reportMT = findReportResults.Sum(r => r.MT_Collected + r.NMT_Delivered + r.MT_Adjustments);
                var reportAGLS = findReportResults.Sum(r => r.AGLS_Collected + r.NAGLS_Delivered + r.AGLS_Adjustments);
                var reportIND = findReportResults.Sum(r => r.IND_Collected + r.NIND_Delivered + r.IND_Adjustments);
                var reportSOTR = findReportResults.Sum(r => r.SOTR_Collected + r.NSOTR_Delivered + r.SOTR_Adjustments);
                var reportMOTR = findReportResults.Sum(r => r.MOTR_Collected + r.NMOTR_Delivered + r.MOTR_Adjustments);
                var reportLOTR = findReportResults.Sum(r => r.LOTR_Collected + r.NLOTR_Delivered + r.LOTR_Adjustments);
                var reportGOTR = findReportResults.Sum(r => r.GOTR_Collected + r.NGOTR_Delivered + r.GOTR_Adjustments);

                if ((c.ItemTireCount.PLT != reportPLT) || (c.ItemTireCount.MT != reportMT) || (c.ItemTireCount.AGLS != reportAGLS)
                   || (c.ItemTireCount.IND != reportIND) || (c.ItemTireCount.SOTR != reportSOTR) || (c.ItemTireCount.MOTR != reportMOTR)
                   || (c.ItemTireCount.LOTR != reportLOTR) || (c.ItemTireCount.GOTR != reportGOTR))
                {
                    string sResult;
                    sResult = c.ClaimId.ToString() + "," + c.RegistrationNumber + "," + c.StartDate.Date.ToShortDateString() + ", " + c.ClaimStatus;
                    if (c.ItemTireCount.PLT != reportPLT) sResult += ", PLT";
                    if (c.ItemTireCount.MT != reportMT) sResult += ", MT";
                    if (c.ItemTireCount.AGLS != reportAGLS) sResult += ", AGLS";
                    if (c.ItemTireCount.IND != reportIND) sResult += ", IND";
                    if (c.ItemTireCount.SOTR != reportSOTR) sResult += ", SOTR";
                    if (c.ItemTireCount.MOTR != reportMOTR) sResult += ", MOTR";
                    if (c.ItemTireCount.LOTR != reportLOTR) sResult += ", LOTR";
                    if (c.ItemTireCount.GOTR != reportGOTR) sResult += ", GOTR";
                    sResult += ";";
                    result.Add(sResult);
                }
            });

            return string.Join(System.Environment.NewLine, result) + "\n";
        }

        public ActionResult FindHaulerClaimWrongTireCountExport()
        {
            string resultStr = FindHaulerClaimWrongTireCount().Replace(";", " ");

            resultStr = "claimID," + "VendorNbr," + "claimPeriod," + "claimStatus," + "TireTypes\r\n" + resultStr;
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            var filename = string.Format("HaulerTireBalanceCompareResult{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        /// <summary>
        /// this is copy of "D:\TM.Jly\CL.TMS.HaulerBLL\HaulerClaimBO.cs PopulateClaimTireBalance"
        /// </summary>
        /// <param name="inventoryItems"></param>
        /// <returns></returns>
        private ItemRow PopulateClaimTireBalance(List<InventoryItem> inventoryItems)
        {
            ItemRow rol = new ItemRow();
            var pltItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.PLT);
            var mtItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MT);
            var aglsItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.AGLS);
            var indItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.IND);
            var sotrItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.SOTR);
            var motrItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.MOTR);
            var lotrItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.LOTR);
            var gotrItem = CL.TMS.Configuration.DataLoader.TransactionItems.FirstOrDefault(p => p.ShortName == TreadMarksConstants.GOTR);

            var query = inventoryItems
                        .GroupBy(c => new { c.ItemId, c.Direction })
                        .Select(c => new { Name = c.Key, Qty = c.Sum(i => i.Qty) });
            query.ToList().ForEach(c =>
            {
                if (c.Name.ItemId == pltItem.ID)
                {
                    if (c.Name.Direction)
                        rol.PLT += c.Qty;
                    else
                        rol.PLT -= c.Qty;
                }
                if (c.Name.ItemId == mtItem.ID)
                {
                    if (c.Name.Direction)
                        rol.MT += c.Qty;
                    else
                        rol.MT -= c.Qty;
                }

                if (c.Name.ItemId == aglsItem.ID)
                {
                    if (c.Name.Direction)
                        rol.AGLS += c.Qty;
                    else
                        rol.AGLS -= c.Qty;
                }
                if (c.Name.ItemId == indItem.ID)
                {
                    if (c.Name.Direction)
                        rol.IND += c.Qty;
                    else
                        rol.IND -= c.Qty;
                }
                if (c.Name.ItemId == sotrItem.ID)
                {
                    if (c.Name.Direction)
                        rol.SOTR += c.Qty;
                    else
                        rol.SOTR -= c.Qty;
                }
                if (c.Name.ItemId == motrItem.ID)
                {
                    if (c.Name.Direction)
                        rol.MOTR += c.Qty;
                    else
                        rol.MOTR -= c.Qty;
                }
                if (c.Name.ItemId == lotrItem.ID)
                {
                    if (c.Name.Direction)
                        rol.LOTR += c.Qty;
                    else
                        rol.LOTR -= c.Qty;
                }
                if (c.Name.ItemId == gotrItem.ID)
                {
                    if (c.Name.Direction)
                        rol.GOTR += c.Qty;
                    else
                        rol.GOTR -= c.Qty;
                }

            });
            return rol;
        }
        #endregion

        public ActionResult CalculatePaymentSubtotalHST(int iVendorType = 2)
        {
            List<int> claimIDs = claimService.LoadAllClaimIdsByVendorTypeAndStatus();//default: collector, approved claims
            List<string> claimIDswith5 = new List<string>();
            List<decimal> hstValues = new List<decimal>();
            int totalIDnumber = claimIDs.Count;
            int counter = 0;
            claimIDs.ForEach(i =>
            {
                {
                    decimal hstBeforeRounding = 0m;
                    int oneDigit = 0;
                    string sIdOf5 = string.Empty;
                    switch (iVendorType)
                    {
                        case 2:
                            CL.TMS.DataContracts.ViewModel.Collector.CollectorPaymentViewModel paymentResult = this.collectorClaimService.LoadCollectorClaimSummary(i).CollectorPayment;
                            hstBeforeRounding = (paymentResult.SubTotal * paymentResult.HSTBase);
                            oneDigit = ((int)(Math.Floor(hstBeforeRounding * 1000)) % 10);
                            break;
                        case 3:
                            hstBeforeRounding = this.haulerClaimService.LoadHaulerPayment(i).HST;
                            break;
                        case 4:
                            hstBeforeRounding = this.processorClaimService.LoadProcessorClaimSummary(0, i).ProcessorPayment.GroundTotal;
                            break;
                        case 5:
                            hstBeforeRounding = this.rpmClaimService.LoadRPMPayment(i).GroundTotal;
                            break;
                        default: break;
                    }
                    hstValues.Add(hstBeforeRounding);
                    if (5 == oneDigit)
                    {
                        sIdOf5 = i.ToString();
                        claimIDswith5.Add(i.ToString());
                    }
                }
            });

            var strList = claimIDswith5.Select(x => x.ToString()).ToList();
            string resultStr = string.Join(",", strList.ToArray());
            string resultStrMultiLines = string.Join("\r\n", strList.ToArray());
            var bytes = Encoding.UTF8.GetBytes(resultStr);
            var bytesLines = Encoding.UTF8.GetBytes(resultStrMultiLines);
            var filename = string.Format("OTSTM2-613_{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            //return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
            return File(bytesLines, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
    }
}