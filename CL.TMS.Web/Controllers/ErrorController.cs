﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common;

namespace CL.TMS.Web.Controllers
{
    public class ErrorController : BaseController
    {
        //
        // GET: /Error/
        public ActionResult Antiforgery()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult UnauthorizedPage()
        {
            return View();
        }

      
        
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult PageNotFound()
        {
            return View();
        }
	}
}