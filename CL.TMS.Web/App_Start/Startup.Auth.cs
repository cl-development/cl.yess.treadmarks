﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using CL.TMS.Security;
using Microsoft.Owin.StaticFiles;

namespace CL.TMS.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie

            //A middleware for supporting document security
            app.Use(async (context, next) =>
            {
                if (context.Request.Path.StartsWithSegments(new PathString("/upload"))
                    && !context.Request.Path.StartsWithSegments(new PathString("/upload/companylogo")))
                {
                    var userCookie = context.Request.Cookies[".ASPNETTMSAUTH"];
                    if (string.IsNullOrWhiteSpace(userCookie))
                    {
                        context.Response.Write("<html><body><h2 style='color:red'>Unauthorized!</h2><p>You have attempted to access resources which you are not authorized.</p></body></html>");
                        return;
                    }
                    await next();
                }
                else
                {
                    await next();
                }

            });

            app.UseStaticFiles();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LogoutPath = new PathString("/Account/Login"),
                ExpireTimeSpan = TimeSpan.FromMinutes(UserPolicy.NonPersistentAuthTicketExpiresInMinutes), 
                CookieName = ".ASPNETTMSAUTH",
                LoginPath = new PathString("/Account/Login"),
                SlidingExpiration = true,
                CookieSecure=CookieSecureOption.SameAsRequest,
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = ctx =>
                    {
                        if (!IsAjaxRequest(ctx.Request))
                        {
                            ctx.Response.Redirect(ctx.RedirectUri);
                        }
                    }
                }
            });
        }

        private static bool IsAjaxRequest(IOwinRequest request)
        {
            IReadableStringCollection query = request.Query;
            if ((query != null) && (query["X-Requested-With"] == "XMLHttpRequest"))
            {
                return true;
            }
            IHeaderDictionary headers = request.Headers;
            return ((headers != null) && (headers["X-Requested-With"] == "XMLHttpRequest"));
        }
    }
}