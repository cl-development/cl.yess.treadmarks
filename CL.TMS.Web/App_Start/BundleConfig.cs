﻿using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace CL.TMS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region General Styles
            bundles.Add(new StyleBundle("~/css/normalize").Include(
                "~/css/normalize.css"));

            bundles.Add(new StyleBundle("~/css/common").Include(
                "~/css/common.css"));

            bundles.Add(new StyleBundle("~/css/bootstrap").Include(
                "~/css/bootstrap.css"
                ));

            bundles.Add(new StyleBundle("~/css/customize").Include(
                  "~/css/customize.css",
                 "~/css/documentation.css"
             ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/css/jquery-webui-popover").Include(
                     "~/css/jquery.webui-popover.css"));

            bundles.Add(new StyleBundle("~/css/jquery-webui-popover1.2.18").Include(
                     "~/js/webui-popover-1.2.18/jquery.webui-popover.css"));

            bundles.Add(new StyleBundle("~/css/datetimepicker").Include(
                "~/css/bootstrap-datetimepicker.css"));

            bundles.Add(new StyleBundle("~/css/datatable-header").Include(
                "~/css/datatable-header.css",
                "~/css/cube/datatable_styles.css?v=@ViewBag.versionid"));

            bundles.Add(new StyleBundle("~/css/datatable-tm").Include(
                "~/css/datatable-tm.css"));

            bundles.Add(new StyleBundle("~/css/jquery.dataTables").Include(
                "~/css/jquery.dataTables.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                "~/js/datepicker/bootstrap-datepicker.css"));

            #endregion

            #region General Scripts
            bundles.Add(new ScriptBundle("~/bundles/customize").Include(
                "~/js/activate.js",
                "~/js/demonstration.js",
                "~/js/search.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/js/modernizr.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/js/bootstrap.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/cube").Include(
                "~/js/cube/ie-emulation-modes-warning.js",
                "~/js/cube/ie10-viewport-bug-workaround.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery.validate.*",
            "~/Scripts/jquery.unobtrusive.*"));

            bundles.Add(new ScriptBundle("~/bundles/TreadMarks").Include(
            "~/js/TreadMarks.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-webui-popover").Include(
            "~/js/jquery.webui-popover.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-webui-popover1.2.18").Include(
            "~/js/webui-popover-1.2.18/jquery.webui-popover.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datetimepicker").Include(
            "~/js/bootstrap-datetimepicker.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
               "~/js/jquery-ui.min.js",
               "~/js/underscore-min.js"));

            //jquery datatable
            bundles.Add(new ScriptBundle("~/bundles/jquerydatatable").Include(
                "~/js/jquery.dataTables.js",
                "~/js/dataTables.scroller.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                "~/Scripts/bootstrap-datepicker.js"));

            //Participant Header
            bundles.Add(new ScriptBundle("~/bundles/participantheader").Include(
                "~/js/ParticipantHeader.js"));


            //Claim transaction adjustment
            bundles.Add(new ScriptBundle("~/bundles/claimtransactionadjustment").Include(
                "~/js/Claims/common-transactionadjustment-list.js"));

            //Claim list
            bundles.Add(new ScriptBundle("~/bundles/claimlist").Include(
                "~/js/Claims/claims-list.js"));

            //Staff claim list
            bundles.Add(new ScriptBundle("~/bundles/staffclaimlist").Include(
                "~/js/Claims/staff-claims-list.js"));

            //Application Common
            bundles.Add(new ScriptBundle("~/bundles/applicationcommon").Include(
                "~/js/ApplicationCommon.js",
                "~/js/CommonValidation.js",
                "~/js/search.js",
                "~/js/jquery.timeago.js"));

            bundles.Add(new ScriptBundle("~/bundles/regcommon").Include(
                "~/js/Registration/reg-common-components.js"));

            bundles.Add(new ScriptBundle("~/bundles/applicationjquerycommon").Include(
                "~/js/jquery.timeago.js"));

            //Collector Application 
            bundles.Add(new ScriptBundle("~/bundles/collectorapplicationparticipant").Include(
                "~/Areas/Collector/Scripts/Registration/participant-index.js",
                "~/Areas/Collector/Scripts/Registration/participant-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/collectorapplicationstaff").Include(
                "~/Areas/Collector/Scripts/Registration/staff-index.js",
                "~/Areas/Collector/Scripts/Registration/staff-index.validate.js"));

            //Collector Claims
            bundles.Add(new ScriptBundle("~/bundles/collectorclaimcommon").Include(
                "~/Areas/Collector/Scripts/Claims/collector-claim-app.js",
                "~/Areas/Collector/Scripts/Claims/collector-claim-service.js",
                "~/Areas/Collector/Scripts/Claims/collector-claim-summary-directives.js"));

            bundles.Add(new ScriptBundle("~/bundles/collectorclaimparticipant").Include(
                "~/Areas/Collector/Scripts/Claims/collector-claim-summary-controller.js",
                "~/Areas/Collector/Scripts/Claims/collector-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/collectorclaimstaff").Include(
                 "~/Areas/Collector/Scripts/Claims/collector-staff-claim-summary-controller.js",
                 "~/Areas/Collector/Scripts/Claims/collector-staff-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/collectortransaction").Include(
                "~/Areas/Collector/Scripts/Transaction/transaction-collector-app.js"));

            //Hauler claim
            bundles.Add(new ScriptBundle("~/bundles/haulerclaimcommon").Include(
                 "~/Areas/Hauler/Scripts/Claims/hauler-claim-app.js",
                 "~/Areas/Hauler/Scripts/Claims/hauler-claim-service.js",
                 "~/Areas/Hauler/Scripts/Claims/hauler-claim-summary-directives.js",
                 "~/Areas/Hauler/Scripts/Claims/hauler-transactionadjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/haulerclaimparticipant").Include(
                 "~/Areas/Hauler/Scripts/Claims/hauler-claim-summary-controller.js",
                 "~/Areas/Hauler/Scripts/Claims/hauler-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/haulerclaimstaff").Include(
                 "~/Areas/Hauler/Scripts/Claims/hauler-staff-claim-summary-controller.js",
                 "~/Areas/Hauler/Scripts/Claims/hauler-staff-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/haulerapplicationparticipant").Include(
                 "~/Areas/Hauler/Scripts/Registration/participant-index.js",
                 "~/Areas/Hauler/Scripts/Registration/participant-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/haulerapplicationstaff").Include(
                 "~/Areas/Hauler/Scripts/Registration/staff-index.js",
                 "~/Areas/Hauler/Scripts/Registration/staff-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/haulertransaction").Include(
                "~/Areas/Hauler/Scripts/Transaction/transaction-hauler-app.js"));

            //System Area
            bundles.Add(new ScriptBundle("~/bundles/systemregistration").Include(
                "~/js/invitation.registration.js"));

            bundles.Add(new ScriptBundle("~/bundles/systemclaimlist").Include(
                "~/Areas/System/Scripts/Claims/claim-assignment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/systemapplication").Include(
                "~/Areas/System/Scripts/DashBoard/dashboard-applications.js",
                "~/Areas/System/Scripts/DashBoard/dashboard-app.js"));

            bundles.Add(new ScriptBundle("~/bundles/systemparticipantuser").Include(
                "~/Areas/System/Scripts/Invitation/ParticipantUser.js"));

            bundles.Add(new ScriptBundle("~/bundles/systemstaffuser").Include(
                "~/Areas/System/Scripts/Invitation/StaffUser.js"));

            bundles.Add(new ScriptBundle("~/bundles/reporting").Include(
                "~/Areas/System/Scripts/Reporting/reporting.js"));

            bundles.Add(new ScriptBundle("~/bundles/transaction-generic-list").Include(
                "~/js/Transaction/transaction-generic-list.js"));

            //RPM area
            bundles.Add(new ScriptBundle("~/bundles/rpmclaimcommon").Include(
                 "~/Areas/RPM/Scripts/Claims/rpm-claim-app.js",
                 "~/Areas/RPM/Scripts/Claims/rpm-claim-service.js"));

            bundles.Add(new ScriptBundle("~/bundles/rpmclaimparticipant").Include(
                "~/Areas/RPM/Scripts/Claims/rpm-claim-summary-directives.js",
                 "~/Areas/RPM/Scripts/Claims/rpm-claim-summary-controller.js",
                 "~/Areas/RPM/Scripts/Claims/rpm-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/rpmclaimstaff").Include(
                 "~/Areas/RPM/Scripts/Claims/rpm-staff-claim-summary-controller.js",
                 "~/Areas/RPM/Scripts/Claims/rpm-staff-internaladjustment-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/rpmapplicationparticipant").Include(
                 "~/Areas/RPM/Scripts/Registration/participant-index.js",
                 "~/Areas/RPM/Scripts/Registration/participant-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/rpmapplicationstaff").Include(
                 "~/Areas/RPM/Scripts/Registration/staff-index.js",
                 "~/Areas/RPM/Scripts/Registration/staff-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/rpmtransaction").Include(
                 "~/Areas/RPM/Scripts/Transaction/transaction-rpm-app.js"));

            //Registrant
            bundles.Add(new ScriptBundle("~/bundles/ipad-qrcode-all").Include(
                 "~/Areas/Registrant/Scripts/staffAllIPadGrid.js",
                 "~/Areas/Registrant/Scripts/staffAllQRCodeGrid.js"
                 ));

            //Processor area
            bundles.Add(new ScriptBundle("~/bundles/processor-claim-angular").Include(
                 "~/Areas/Processor/Scripts/Claims/processor-claim-app.js",
                 "~/Areas/Processor/Scripts/Claims/processor-claim-service.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/processor-claim-participant").Include(
                 "~/Areas/Processor/Scripts/Claims/processor-claim-summary-controller.js"));

            bundles.Add(new ScriptBundle("~/bundles/processor-claim-staff").Include(
                 "~/Areas/Processor/Scripts/Claims/processor-staff-claim-summary-controller.js"));

            bundles.Add(new ScriptBundle("~/bundles/processor-claim-list").Include(
                 "~/Areas/Processor/Scripts/Claims/processor-transactionadjustment-list.js",
                 "~/Areas/Processor/Scripts/Claims/processor-internaladjustment-list.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/processor-staff-claim-list").Include(
                 "~/Areas/Processor/Scripts/Claims/processor-transactionadjustment-list.js",
                 "~/Areas/Processor/Scripts/Claims/processor-staff-internaladjustment-list.js"
                ));

            //Processor application
            bundles.Add(new ScriptBundle("~/bundles/processor-application-participant").Include(
                 "~/Areas/Processor/Scripts/Registration/participant-index.js",
                 "~/Areas/Processor/Scripts/Registration/participant-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/processor-application-staff").Include(
                 "~/Areas/Processor/Scripts/Registration/staff-index.js",
                 "~/Areas/Processor/Scripts/Registration/staff-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/transaction-list-common").Include(
                "~/js/jquery.timeago.js",
                "~/js/search.js",
                "~/js/jquery.smoothZoom.js"));

            bundles.Add(new ScriptBundle("~/bundles/processor-angular-app").Include(
                 "~/Areas/Processor/Scripts/Transaction/transaction-processor-app.js"));

            //Steward area
            bundles.Add(new ScriptBundle("~/bundles/steward-application-participant").Include(
                 "~/Areas/Steward/Scripts/Registration/participant-index.js",
                 "~/Areas/Steward/Scripts/Registration/participant-index.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/steward-application-staff").Include(
                 "~/Areas/Steward/Scripts/Registration/staff-index.js",
                 "~/Areas/Steward/Scripts/Registration/staff-index.validate.js"));

            //angular datatable
            bundles.Add(new ScriptBundle("~/bundles/angulardatatable").Include(
                "~/js/angular-datatables.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-datatables-scroller").Include(
                "~/js/angular-datatables.scroller.js"));

            //OTSTM-34
            bundles.Add(new ScriptBundle("~/bundles/global-search-staff-init").Include(
                "~/js/Shared/global-search-staff-init.js"));

            //User and Roles
            //only roles
            bundles.Add(new ScriptBundle("~/bundles/roles-common").Include(
                "~/Areas/System/Scripts/User/common-app.js",
                "~/Areas/System/Scripts/User/role-lib.js"));

            //global search
            bundles.Add(new ScriptBundle("~/bundles/global-search-lib").Include(
                "~/js/AngularModule/global-search-lib.js"));

            //global notification
            bundles.Add(new ScriptBundle("~/bundles/globalNotification").Include(
                "~/js/Shared/globalNotification.js"));


            if (!HttpContext.Current.IsDebuggingEnabled)
                BundleTable.EnableOptimizations = true;

            #endregion
        }
    }
}
