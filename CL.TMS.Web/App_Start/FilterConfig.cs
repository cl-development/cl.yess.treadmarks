﻿using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common.Logging;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RedirectActionAttribute());
            filters.Add(new GlobalExceptionHandlerAttribute());
            filters.Add(new TraceActionRequestAttribute());
        }
    }
}
