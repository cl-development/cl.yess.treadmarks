﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CL.TMS.Web.Message
{
    public static class MessageManagerConfig
    {
        public static void InitializeMessageManager(IEventAggregator eventAggregator)
        {
            MessageManager.Instance.InitializeMessageManager(eventAggregator);
        }
    }
}