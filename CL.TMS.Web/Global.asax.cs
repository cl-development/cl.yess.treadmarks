﻿using CL.TMS.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CL.TMS.ClaimCalculator;
using CL.TMS.DependencyBuilder;
using CL.TMS.UI.Common;
using FluentValidation.Mvc;
using CL.TMS.UI.Common.UserInterface;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Web.Message;
using CL.TMS.Reporting;

namespace CL.TMS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            ExceptionHandlingConfig.BootstrapExceptionHandling();
            AutoMapperConfig.RegisterMappings();
            FluentValidationModelValidatorProvider.Configure();

            ClaimCalculatorConfig.InitializeCalimCalculator(DependencyResolver.Current.GetService<IEventAggregator>());
            MessageManagerConfig.InitializeMessageManager(DependencyResolver.Current.GetService<IEventAggregator>());
            ClaimReportConfig.InitializeClaimReporting(DependencyResolver.Current.GetService<IEventAggregator>());

            DbInterceptor.InitializeDbInterceptor();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomViewEngine());

            //Enable client side validation globally
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
    }
}
