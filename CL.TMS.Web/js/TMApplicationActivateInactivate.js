/*Used by Activate/Deactivate application
    FileName: TMApplicationActivateInactivate.js
*/
function TMActiveInactive() {
  
    //this section is copied from file: \Areas\Hauler\Scripts\Registration\staff-index.js

    /*********************************MODAL ACTIVE INACTIVE BUTTON ***************************************/
    var RegistrantStatus;
    var approvedActiveDate = $('.RegistrantActiveStatusApprovedDate').val();
    var approvedInActiveDate = $('.RegistrantInActiveStatusApprovedDate').val();

    $('#datetimepickerToInactive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedInActiveDate
    });
    $('#datetimepickerToActive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedActiveDate
    });

    //if ($("#activedropdown :selected").text() == "Other") {
    //    $('#OtherActiveStatus').show();
    //}
    //if ($("#inactivedropdown :selected").text() == "Other") {
    //    $('#OtherInActiveStatus').show();
    //}

    if ($('#activedropdown option:selected').index() == 6) {
        $('#OtherActiveStatus').show();
    }

    if ($('#inactivedropdown option:selected').index() == 11) {
        $('#OtherInActiveStatus').show();
    }

    if ($('#inactivedropdown option:selected').index() == 0) {
        $('#modalDeactivatebtn').prop('disabled', true);
    }

    //dropdown list change
    $("#activedropdown").change(function () {
        if (this.selectedIndex == 6) {
            $('#OtherActiveStatus').show();
        }
        else {
            $('#OtherActiveStatus').hide();
            $('#OtherActiveStatus').val('');
        }
    });

    $("#inactivedropdown").change(function () {

        if (this.selectedIndex == 0)
            $('#modalDeactivatebtn').prop('disabled', true);
        else
            $('#modalDeactivatebtn').prop('disabled', false);

        if (this.selectedIndex == 11) {
            $('#OtherInActiveStatus').show();
        }
        else {
            $('#OtherInActiveStatus').hide();
            $('#OtherInActiveStatus').val('');
        }
    });

    if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $('#RegistrantStatusChangeEffectiveDate').val());
    }
    if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $('#InActiveRegistrantStatusChangeEffectiveDate').val());
    }

    $('#RegistrantStatusChangeEffectiveDate').on('change', function () {
        var RegistrantStatusChangeEffectiveDate = $(this);
        $.when(
            RegistrantStatusChangeEffectiveDate.focusout()).then(function () {
                $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $(this).val());
            });
    });

    $('#InActiveRegistrantStatusChangeEffectiveDate').on('change', function () {
        var InActiveEffectiveDate = $(this);
        $.when(
        InActiveEffectiveDate.focusout()).then(function () {
            $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $(this).val());
        });
    });

    $('#modalActivateBtn').on('click', function () {
        RegistrantStatus = "Activate";

        //check if date and reason are filled in
        if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {
            if ($('#activedropdown option:selected').index() != 0) {
                var jsonObj;

                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day;

                if ($('#RegistrantStatusChangeEffectiveDate').val() == output) {
                    $('#activeGreenButton').removeClass('btn btn-xs btn-default').addClass('btn btn-xs btn-success active');
                    $('#inactiveRedButton').removeClass('btn btn-xs btn-danger').addClass('btn btn-xs btn-default');
                }
                else {
                    $('#activeGreenButton').addClass('btn btn-xs btn-default');
                    location.reload();
                }

                jsonObj = processRegistrantStatusJson();
            }
            else {
                RegistrantStatus = "ActivateOnlyDate";

            }
            if (jsonObj != null && typeof jsonObj != 'undefined') {
                $.ajax({
                    url: '/Collector/Registration/VendorActiveHistory',
                    dataType: 'json',
                    type: 'POST',
                    data: jsonObj,
                    failure: function (data) {
                        console.log(data);
                    },
                });
            }
        }
    });

    $('#modalDeactivatebtn').on('click', function () {
        RegistrantStatus = "Deactivate";

        if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0 && $("#inactivedropdown").val().length > 0) {
            if ($('#inactivedropdown option:selected').index() != 0) {
                var jsonObj;
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day;

                if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == output) {
                    $('#inactiveRedButton').removeClass('btn btn-xs btn-default').addClass('btn btn-xs btn-danger');
                    $('#activeGreenButton').removeClass('btn btn-xs btn-success active').addClass('btn btn-xs btn-default');
                }
                else {
                    $('#inactiveRedButton').addClass('btn btn-xs btn-default');
                    location.reload();
                }

                jsonObj = processRegistrantStatusJson();

                if (jsonObj != null && typeof jsonObj != 'undefined') {
                    $.ajax({
                        url: '/Collector/Registration/VendorActiveHistory',
                        dataType: 'json',
                        type: 'POST',
                        data: jsonObj,
                        failure: function (data) {
                            console.log(data);
                        },
                    });
                }
            }
        }
    });

    var processRegistrantStatusJson = function () {

        item = {};
        item['VendorID'] = $('.VendorID').val();

        var activeProp = {
            RegistrantStatusChangeReason: "RegistrantStatusChangeReason",
            RegistrantStatusChangeEffectiveDate: "RegistrantStatusChangeEffectiveDate",
            RegistrantStatusChangeActivityStatus: "RegistrantStatusChangeActivityStatus",
            OtherStatus: "OtherStatus"
        };

        var InActiveProp = {
            RegistrantStatusChangeReason: "RegistrantStatusChangeReason",
            RegistrantStatusChangeEffectiveDate: "RegistrantStatusChangeEffectiveDate",
            RegistrantStatusChangeActivityStatus: "RegistrantStatusChangeActivityStatus",
            NewRegistrationNumber: "NewRegistrationNumber",
            OtherStatus: "OtherStatus"
        };

        switch (RegistrantStatus) {
            case "Activate":
                item[activeProp.RegistrantStatusChangeReason] = $("#activedropdown").val();
                item[activeProp.RegistrantStatusChangeEffectiveDate] = $('#RegistrantStatusChangeEffectiveDate').val();
                item[activeProp.RegistrantStatusChangeActivityStatus] = 1;
                item[activeProp.OtherStatus] = $('#OtherActiveStatus').val();

                break;
            case "Deactivate":
                item[InActiveProp.RegistrantStatusChangeReason] = $("#inactivedropdown").val();
                item[InActiveProp.RegistrantStatusChangeEffectiveDate] = $('#InActiveRegistrantStatusChangeEffectiveDate').val();
                item[InActiveProp.RegistrantStatusChangeActivityStatus] = 2;
                item[InActiveProp.NewRegistrationNumber] = $('#NewRegistrationNumber').val();
                item[InActiveProp.OtherStatus] = $('#OtherInActiveStatus').val();
                break;
            case "ActivateOnlyDate":
                item[activeProp.RegistrantStatusChangeEffectiveDate] = $('#RegistrantStatusChangeEffectiveDate').val();
                item[activeProp.RegistrantStatusChangeActivityStatus] = 1;
                break;
        }
        return item;
    }

    $('#modalCancelInactivatebtn,#modalCancelActivatebtn,.close').click(function () {
        location.reload();
    });
    /********************************END MODAL ACTIVE INACTIVE BUTTON********************************************/

}