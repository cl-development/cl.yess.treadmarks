$(function () {

    // Enable Tooltips
    $("body").tooltip({
        delay: { show: 800, hide: 100 },
        selector: '[data-toggle=tooltip]'
    });

    // Enable Collapsable Divs
    //$('.collapse').collapse();

    // Enable Popovers
    $('[data-toggle="popover"]').popover({ trigger: 'hover' });

});

/*

$(document).ready(function() {
	$( ".panel-heading a" ).click(function() {
		$( this ).toggleClass('panel-arrow-collapse');
	});
});

*/


$(function () {

    $(".panel-heading a").click(function (e) {

        if ($(this).hasClass("collapsed")) {
            $(this).removeClass("panel-arrow-collapse").addClass("panel-arrow-expand");

        } else {
            // if other menus are open remove open class and add closed
            $(this).removeClass("panel-arrow-expand").addClass("panel-arrow-collapse");
            //$(this).siblings().removeClass("open").addClass("closed"); 
            //$(this).removeClass("closed").addClass("open");
        }

    });
});


/*
/* Search Interactivity
*/
$('.subcomp-search .glyphicon-search').each(function () {
    /*
        $(this).mouseover(function () {
            $(this).siblings("input").css("visibility", "visible");
    
            //If the search field is empty, remove the clear icon
            if ($(this).siblings("input").val() != '') {
                $(this).siblings(".remove-icon").show();
                $(this).siblings(".subcomp-search-found").show();
            }
            $(this).siblings("input").focus();
    
        });
        $(this).siblings("input").keyup(function (e) {
            //show clear icon when the the user types the search field
            if ($(this).val() != '') {
                $(this).siblings(".remove-icon").show();
                $(this).siblings(".subcomp-search-found").show();
                $('.remove-icon').click(function (n) {
                    $(this).siblings("input").val("").focus();
                    $(this).hide();
                    $(".subcomp-search-found").hide();
                    n.stopPropagation();
                });
            }
            else {
                $(this).siblings(".remove-icon").hide();
            }
            //to prevent hide the search field when the user clicks the clear icon
            e.stopPropagation();
        });
    */
});

//If the search field is empty, hides seach field when the user clicks outside 
$(document).click(function () {
    //if ($('.subcomp-search input').val() === '') {
    //    $('.subcomp-search input').css("visibility", "hidden");
    //    $('.subcomp-search input').siblings(".remove-icon").hidden();
    //    $('.subcomp-search input').siblings(".subcomp-search-found").hidden();
    //}
});

//If the search field is empty, hides seach field 
$(".subcomp-search").children("input").focusout(function () {
    if ($(this).val() == "") {
        $(this).css("visibility", "");
    }
});

//Clear the search field on clicking the remove icon
$(".remove-icon").on("click", function () {
    $(this).siblings("input").val("");
    $(this).siblings("input").focus();
    $(this).siblings("subcomp-search-found").html("");
    $(this).hide();
});

//Handle the datatable search key-up event
$(".subcomp-search").children("input").on("keyup", function () {
    var s = $(this).val();
    if (s.length >= 1) {
        $(this).css('visibility', 'visible');
        $(this).siblings(".subcomp-search-found").css('display', 'block');
        $(this).siblings(".remove-icon").show();
    }
    else {
        $(this).removeAttr('style');
        $(this).siblings(".subcomp-search-found").css('display', 'none');
        $(this).siblings('.remove-icon').hide();
    }
});

//Numeric input for numbers greater than 0
$(".numberic-input").on("keydown", function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode <= 48 || e.keyCode > 57)) && (e.keyCode <= 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
    else {

        $(this).siblings(".numeric-validation").hide();
    }
});
