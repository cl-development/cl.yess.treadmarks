﻿var statusStr = '';
var errorList = []; // list of invalid Form Fields 
var invalidList = []; //used for auto save feature
var panelLevelCheckErrorList = []; //used for the panel check green tick box
var toolTipSettings = {
    trigger: 'hover',
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};
var errorStatusIcons = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];


var dynamicErrorMsg = function (params, element) {
    var labelName = 'Field';
    if ($(element).prev('label').html()) {
        var labelElementHtml = $($(element).prev('label'));
        labelName = labelElementHtml.html();
    }
    else if ($(element).parent('div.dropdown').prevAll('label').length > 0) {
        labelName = $(element).parent('div.dropdown').prevAll('label').html();
    }

    return 'Invalid ' + labelName;
};

var highlightFields = function () {
    var tmp = errorList.slice();
    for (var i = 0; i < tmp.length; i++) {
        if ($('input[name*=\'' + tmp[i] + '\']').length > 0 || $('select[name*=\'' + tmp[i] + '\']').length > 0) {
            var element = $('input[name*=\'' + tmp[i] + '\']');

            if (element.length == 0) {
                element = $('select[name*=\'' + tmp[i] + '\']');
            }
            var pnl = element.closest('.panel-collapse');
            if (pnl)
                pnl.collapse();

            if (!(element.val().length > 0) || element.is(":checkbox") || element.is(":radio")) {
                element.valid();
            }
            else if (element.attr('type') == 'radio') {
                element.valid();
            }
            var propertyName = $(element).attr('name');
            var index = errorList.indexOf(propertyName);
            if (index > -1) errorList.splice(index, 1);
        }
    }
}

var resetHighlight = function (element, isRequired) {
    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
    $(element).next('span').removeClass('glyphicon-remove');
    $(element).nextAll('.help-block:first').remove();
    if (isRequired) {
        $(element).closest('.form-group').addClass('has-required');
        $(element).next('span').addClass('glyphicon-ok');
    }
}

var disableBackspaceOnCheckBox = function () {
    $('input[type="checkbox"]').keydown(function (e) {
        if (e.keyCode === 8) {
            return false;
        }
    });
}

function _isEmpty(value) {
    return (value == null || value.length === 0);
}

function titleCase() {
    $('.title-case').on('keypress', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}

// BusinessLocation Module
(function (exports) {
    var cbMailingAddressSameAsBusiness = $('#MailingAddressSameAsBusiness');

    var CopyMailingAddress = function () {
        $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val($('#BusinessLocation_BusinessAddress_AddressLine1').val());
        $('input[name="BusinessLocation.MailingAddress.City"]').val($('#BusinessLocation_BusinessAddress_City').val());
        $('input[name="BusinessLocation.MailingAddress.Province"]').val($('#BusinessLocation_BusinessAddress_Province').val());
        $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val($('#BusinessLocation_BusinessAddress_AddressLine2').val());
        $('input[name="BusinessLocation.MailingAddress.Postal"]').val($('#BusinessLocation_BusinessAddress_Postal').val());
        $('select[name="BusinessLocation.MailingAddress.Country"]').val($('#BusinessLocation_BusinessAddress_Country').val());
    };
    var ClearMailingAddress = function () {
        $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val('');
        $('input[name="BusinessLocation.MailingAddress.City"]').val('');
        $('input[name="BusinessLocation.MailingAddress.Province"]').val('');
        $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val('');
        $('input[name="BusinessLocation.MailingAddress.Postal"]').val('');
        $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;
    };
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    var initializeToolTip = function () {
        var businessLocationLBNSettings = { content: $('#businessLocationLBN').html(), width: 300, height: 100 };
        $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationLBNSettings));

        var businessLocationBONSettings = { content: $('#businessLocationBON').html(), width: 300, height: 100 };
        $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationBONSettings));
    }
    var initializeMailingAddressSameAsBusiness = function () {
        $('#MailingAddressSameAsBusiness').click(function () {
            var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

            if (isChecked) CopyMailingAddress();
            else ClearMailingAddress();

            toggleMailingAddress();           
        });
    };
    var initializePanelGreenCheck = function () {
        $('#panelBusinessLocation').removeClass('collapse');
        $('#panelBusinessLocation').addClass('collapse-in');

        var result = $("#fmBusinessLocation").valid();
        if (result) $('#flagBusinessLocation').attr('src', errorStatusIcons[1]);
        else $('#flagBusinessLocation').attr('src', errorStatusIcons[0]);

        $('#fmBusinessLocation').find(':input').each(function () {
            $(this).on("change", function () {
                var businessLoc = $(this);

                $.when(businessLoc.focusout()).then(function () {
                    if ($(this).hasClass("isMailingAddressDifferent") && !$(this).is(":checked")) {
                        $('#flagBusinessLocation').attr('src', errorStatusIcons[0]);
                    }
                    else {
                        if ($("#fmBusinessLocation").valid()) $('#flagBusinessLocation').attr('src', errorStatusIcons[1]);
                        else $('#flagBusinessLocation').attr('src', errorStatusIcons[0]);
                    }

                    ApplicationCommon.SubmitButtonHandler();
                });
            });
        });
    }
    var initializeValidation = function () {
        $('#fmBusinessLocation').validate({
            ignore: '.no-validate, :hidden',
            onfocusout: function (element) {
                this.element(element);
            },
            invalidHandler: function (e, validator) {
            },
            errorPlacement: function (error, element) {
                var propertyName = $(element).attr('name');
                var index = panelLevelCheckErrorList.indexOf(propertyName);

                if ($(element).val() || index > -1) {
                    ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
                }
            },
            errorElement: 'p',
            errorClass: 'help-block',
            rules: {
                'BusinessLocation.BusinessName': {
                    required: true,
                    //regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                    inInvalidList: true
                },
                'BusinessLocation.OperatingName': {
                    required: true,
                    //regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.AddressLine1': {
                    required: true,
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.City': {
                    required: true,
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.Province': {
                    required: true,
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.AddressLine2': {
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.Postal': {
                    required: true,
                    regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                    inInvalidList: true
                },
                'BusinessLocation.BusinessAddress.Country': {
                    required: true,
                    inInvalidList: true
                },
                'BusinessLocation.Phone': {
                    required: true,
                    regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                    inInvalidList: true
                },
                'BusinessLocation.Extension': {
                    required: false,
                    inInvalidList: true
                },
                'BusinessLocation.Email': {
                    required: true,
                    regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                    inInvalidList: true
                },
                'BusinessLocation.MailingAddress.AddressLine1': {
                    inInvalidList: true,
                    required: {
                        depends: function (element) {
                            return !cbMailingAddressSameAsBusiness.is(':checked');
                        }
                    }
                },
                'BusinessLocation.MailingAddress.City': {
                    inInvalidList: true,
                    required: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                },
                'BusinessLocation.MailingAddress.Province': {
                    inInvalidList: true,
                    required: {
                        depends: function (element) {
                            return !cbMailingAddressSameAsBusiness.is(':checked');
                        }
                    }
                },
                'BusinessLocation.MailingAddress.AddressLine2': {
                    required: false,
                    inInvalidList: true
                },
                'BusinessLocation.MailingAddress.Postal': {
                    inInvalidList: true,
                    regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                    required: {
                        depends: function (element) {
                            return !cbMailingAddressSameAsBusiness.is(':checked');
                        }
                    }
                },
                'BusinessLocation.MailingAddress.Country': {
                    inInvalidList: true,
                    required: {
                        depends: function (element) {
                            return !cbMailingAddressSameAsBusiness.is(':checked');
                        }
                    }
                }
            },
            highlight: function (element) {
                var propertyName = $(element).attr('name');
                var index = panelLevelCheckErrorList.indexOf(propertyName);

                if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                    if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                        $(element).closest('.form-group').removeClass('has-required');
                        $(element).closest('.form-group').removeClass('has-success');

                        $(element).closest('.form-group').addClass('has-error');
                        if ($(element).next('span').hasClass('glyphicon-ok'))
                            $(element).next('span').removeClass('glyphicon-ok');
                        $(element).next('span').addClass('glyphicon-remove');
                    }
                }
            },
            unhighlight: function (element) {
                var propertyName = $(element).attr('name');
                var index = panelLevelCheckErrorList.indexOf(propertyName);

                if ($(element).val() || index > -1) {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
                else {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                    $(element).closest('.form-group').addClass('has-required');
                }
            },
            success: function (error) {
                error.remove();
            },
            failure: function () {
            },
            messages: {
                'BusinessLocation.BusinessName': Global.ParticipantIndex.Resources.ValidationMsgInvalidBusinessName,
                'BusinessLocation.OperatingName': Global.ParticipantIndex.Resources.ValidationMsgInvalidOperatingName,
                'BusinessLocation.BusinessAddress.AddressLine1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
                'BusinessLocation.BusinessAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
                'BusinessLocation.BusinessAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
                'BusinessLocation.BusinessAddress.AddressLine2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
                'BusinessLocation.BusinessAddress.Postal': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
                'BusinessLocation.BusinessAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
                'BusinessLocation.Phone': {
                    required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                    regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                    inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                },
                'BusinessLocation.Extension': Global.ParticipantIndex.Resources.ValidationMsgInvalidExt,
                'BusinessLocation.Email': Global.ParticipantIndex.Resources.ValidationMsgInvalidEmail,
                'BusinessLocation.MailingAddress.AddressLine1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
                'BusinessLocation.MailingAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
                'BusinessLocation.MailingAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
                'BusinessLocation.MailingAddress.AddressLine2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
                'BusinessLocation.MailingAddress.Postal': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
                'BusinessLocation.MailingAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
            }
        });
    }

    exports.initialize = function () {
        initializeToolTip();
        initializeMailingAddressSameAsBusiness();
        toggleMailingAddress();
        initializeValidation();
        initializePanelGreenCheck();
    }    
})(this.BusinessLocationModule = {});

function ContactInformation() {
    var contactLimit = 3;
    var k = 0;
    var tempBase = $(".temp").val();
    var tempIncremental;
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            currAddCount = contactTemplateCount++;
            tempIncremental = "<div class='ctemp'><div class='template" + currAddCount + "'>" + tempBase + '</div></div><hr>';
            $(tempIncremental).appendTo('#divContactTemplate');

            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }

    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }
        });
    }

    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }

    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }

    $('.isContactAddressSameAsBusinessAddress').on('click', function () {
        contactAddressSameAs();
    });


    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        contactTemplate = "<div class='template" + i + "'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    $('#btnAddContact').on('click', function () {
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
    });

    primaryContactCheckName();
}

var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
    });
}

var handleBusinessAddressCheckboxForContact = function () {
    $('.isContactAddressSameAsBusinessAddress').each(function () {
        var isChecked = $(this).is(':checked');
        $(this).next('input[type="hidden"]').val(isChecked);
    });

}

function PanelContactInfoGreenCheck() {  
    $('#panelContactInformation').removeClass('collapse');
    $('#panelContactInformation').addClass('collapse-in');

    $("#fmContactInfo").valid();
    if ($("#fmContactInfo").valid()) $('#flagContactInformation').attr('src', errorStatusIcons[1]);
    else $('#flagContactInformation').attr('src', errorStatusIcons[0]);
    
    $('#fmContactInfo').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(businessLoc.focusout()).then(function () {
                if ($(this).hasClass("isContactAddressSameAsBusinessAddress") && !$(this).is(":checked")) {
                    $('#flagContactInformation').attr('src', errorStatusIcons[0]);
                }
                else {
                    if ($("#fmContactInfo").valid()) $('#flagContactInformation').attr('src', errorStatusIcons[1]);
                    else $('#flagContactInformation').attr('src', errorStatusIcons[0]);
                }
                
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
}

function PanelTermsAndConditionsGreenCheck() {
    $('#panelsTermsAndConditions').removeClass('collapse');
    $('#panelsTermsAndConditions').addClass('collapse-in');

    $("#fmTermsAndConditions").valid();
    if ($("#fmTermsAndConditions").valid()) $('#flagTermsAndConditions').attr('src', errorStatusIcons[1]);
    else $('#flagTermsAndConditions').attr('src', errorStatusIcons[0]);
    
    $('#fmTermsAndConditions').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(businessLoc.focusout()).then(function () {
                if ($("#fmTermsAndConditions").valid()) $('#flagTermsAndConditions').attr('src', errorStatusIcons[1]);
                else $('#flagTermsAndConditions').attr('src', errorStatusIcons[0]);
                
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}


function initializeApplicationSubmitButton(serviceURL) {    
    $('#submitApplicationBtn').on('click', function () {
        $.ajax({
            url: '/Hauler/Registration/SubmitApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#ID').val(), tokenId: Global.ParticipantIndex.Resources.TokenId },
            complete: function () {
                window.location.reload();
            }
        });
    });
}


$(function () {
    /* START COMMON VALIDATORS */
    $.validator.addMethod('inInvalidList', function (value, element, param) {
        var propertyName = $(element).attr('name');
        var index = errorList.indexOf(propertyName);
        if (index > -1) {
            errorList.splice(index, 1);
            return false;
        }
        else return true;
    }, 'Invalid');

    $.validator.addMethod('lessThanOrEqualTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam <= today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('greaterThanTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam > today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('dynamic-inInvalidList', function (value, element) {
        var propertyName = $(element).attr('name');
        var index = errorList.indexOf(propertyName);
        if (index > -1) {
            errorList.splice(index, 1);
            return false;
        }
        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-required", function (value, element) {
        if (value) return true;
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-regex-phone", function (value, element) {
        var regex = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$|^$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches) result = (matches[0] == value);
        return result;
    }, 'Expected format ###-###-####');

    $.validator.addMethod("dynamic-regex-postal", function (value, element) {
        var regex = /^[A-Za-z][0-9][A-Za-z]([ ]?)[0-9][A-Za-z][0-9]|([0-9]{5})(?:[- ][0-9]{4})?$/;
        var matches = value.match(regex);
        var result = false;
        if (matches) result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-regex-email", function (value, element) {
        var regex = /^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches) result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-whitespace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });
    /* END COMMON VALIDATORS */   


    /* CONTACT INFORMATION VALIDATION */
    var cbContactAddressSameAsBusinessAddress = $('#ContactAddressSameAsBusinessAddress');

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {

                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {

                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
        rules: {
            '': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                inInvalidList: true
            },
            'ContactInformationList[0].ContactInformation.LastName': {
                required: true,
                inInvalidList: true
            },

            'ContactInformationList[0].ContactInformation.FirstName': {
                required: true,
                inInvalidList: true
            },
            'ContactInformationList[0].ContactInformation.Position': {
                required: true,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                inInvalidList: true
            },
            'ContactInformationList[0].ContactInformation.Ext': {
                required: false,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationAddress.AddressLine1': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': Global.ParticipantIndex.Resources.ValidationMsgInvalidPrimaryContactName,
            'ContactInformation[0].ContactInformationContact.Position': Global.ParticipantIndex.Resources.ValidationMsgInvalidPosition,
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Ext': Global.ParticipantIndex.Resources.ValidationMsgInvalidExt,
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Email': Global.ParticipantIndex.Resources.ValidationMsgInvalidEmail,
            'ContactInformation[0].ContactInformationAddress.Address1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'ContactInformation[0].ContactInformationAddress.Address2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'ContactInformation[0].ContactInformationAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'ContactInformation[0].ContactInformationAddress.PostalCode': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'ContactInformation[0].ContactInformationAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'ContactInformation[0].ContactInformationAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
        }
    });
    /* END CONTACT INFORMATION VALIDATION */


    /* START TERMS AND CONDITIONS VALIDATION */
    var validatorTermsInfo = $('#fmTermsAndConditions').validate({
        ignore: '',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TermsAndConditions.SigningAuthorityFirstName': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.SigningAuthorityLastName': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.SigningAuthorityPosition': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByFirstName': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByLastName': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByPhone': {
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.AgreementAcceptedByFullName': {
                inInvalidList: true,
                required: true,
                regex: '^([^a-z]*)([^a-z ]{2,})([ ]{1,}[^a-z ]+)*([ ]{1,}[^a-z ]{2,})+([^a-z]+)*$'
            },
            'TermsAndConditions.AgreementAcceptedCheck': {
                inInvalidList: true,
                required: true
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).attr('data-signature')) {
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-required');
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-success');
                    $(element).closest('.termsandconditions-signature').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                else {
                    if ($(element).closest('.form-group').hasClass('has-required'))
                        $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).attr('data-signature')) {
                    $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
                else {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
        messages: {
            'TermsAndConditions.SigningAuthorityFirstName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.SigningAuthorityLastName': Global.ParticipantIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.SigningAuthorityPosition': Global.ParticipantIndex.Resources.ValidationMsgInvalidPosition,
            'TermsAndConditions.FormCompletedByFirstName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.FormCompletedByLastName': Global.ParticipantIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.FormCompletedByPhone': {
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFullName,
            'TermsAndConditions.AgreementAcceptedCheck': Global.ParticipantIndex.Resources.ValidationMsgMustBeCheckedForSubmission,
        }
    });
    /* END TERMS AND CONDITIONS */


    var init = function () {
        if (Global.ParticipantIndex.Model.InvalidFormFields && Global.ParticipantIndex.Model.Status.toLowerCase() == 'backtoapplicant') {
            errorList = Global.ParticipantIndex.Model.InvalidFormFields.slice();
            invalidList = Global.ParticipantIndex.Model.InvalidFormFields;
            panelLevelCheckErrorList = Global.ParticipantIndex.Model.InvalidFormFields.slice();
        }
    }


    init();
});