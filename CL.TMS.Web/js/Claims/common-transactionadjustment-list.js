﻿$(function () {
    var pageSize = 5;
    
    //Define data table
    var tableId = '#tblClaimTransactionAdjusList';
    var table = $(tableId).DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.ParticipantTransactionAdjustmentSummary.TransAdjustmentHandleUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY:250, //limit the show rows in the table
        scrollCollapse: false,     
        createdRow: function(row, data, index){
            $(row).attr('transaction-id',data.TransactionId);
            $(row).attr('transaction-adjustment-id', data.TransactionAdjustmentId);
            $(row).addClass('cursor-pointer');
        },
        searching: true,
        ordering: true,
        order: [[4, "desc"]],
        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },     
        columns: [
                 {
                     name: "TransactionType",
                     data: null,
                     render: function (data, type, full, meta) {
                         if ($('.sub-header-registrantinfo').text() && ($('.sub-header-registrantinfo').text().substr(0, 1) === '5')) {
                             var transactionTypename = (data.TransactionType == "SPS" )?"SIR":"SPS"
                             return "<span>" + transactionTypename + "</span>";
                         } else {
                            return "<span>" +data.TransactionType + "</span>";
                         }
                     }
                 },
                 {
                     name: "TransactionNumber",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.TransactionNumber + "</span>";
                     }
                 },
                 {
                     name: "TransactionDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         if (data.TransactionDate != null && data.TransactionDate !== undefined) {
                             return dateTimeConvert(data.TransactionDate);
                         } else {
                             return '';
                         }
                     }
                 },
                 {
                     name: "StartedBy",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.StartedBy + "</span  >";
                     }
                 },
                 {
                     name: "AdjustmentDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         if (data.AdjustmentDate != null && data.AdjustmentDate !== undefined) {
                             return dateTimeConvert(data.AdjustmentDate);
                         } else {
                             return '';
                         }
                     }
                 },
                 {
                     name: "Status",
                     className: "td-center",
                     data: null,
                     render: function (data, type, full, meta) {
                         var span = ""
                         if (data.Status == null) return span;
                         switch (data.Status.toLowerCase()) {
                             case "pending":
                                 span = '<span>' + '<div class="panel-table-status color-tm-orange-bg">' + data.Status + '</div>' + '</span>';
                                 break;
                             case "requested":
                                 span = '<span>' + '<div class="panel-table-status color-tm--gray-light-bg">' + data.Status + '</div>' + '</span>';
                                 break;
                             case "under review":
                             case "recalled":
                                 span = '<span>' + '<div class="panel-table-status color-tm-gray-bg">' + data.Status + '</div>' + '</span>';
                                 break;
                             case "system rejected":
                             case "systemrejected":
                             case "rejected":
                                 span = '<span>' + '<div class="panel-table-status color-tm-red-bg">Rejected</div>' + '</span>';
                                 break;
                             case "accepted":
                                 span = '<span>' + '<div class="panel-table-status color-tm-green-bg">' + data.Status + '</div>' + '</span>';
                                 break;
                             case "recalled":
                                 span = '<span>' + '<div class="panel-table-status color-tm-green-bg">' + data.Status + '</div>' + '</span>';
                                 break;
                             default:
                                 span = '<span>' + '<div class="panel-table-status color-tm-gray-bg">' + data.Status + '</div>' + '</span>';
                                 //default code block 
                         }
                         return span;
                     }
                 },
                 {
                     name: "TransactionAdjustmentId",
                     data: null,
                     className: 'display-none',
                     render: function (data, type, full, meta) {
                         return "<span>" + data.TransactionAdjustmentId + "</span>";
                     }
                 },
                {
                    name: "CreatedVendorId",
                    data: null,
                    className: 'display-none',
                    render: function (data, type, full, meta) {
                        return data.CreatedVendorId;
                    },
                },
                {
                    name: "IsAdjustByStaff",
                    data: null,
                    className: 'display-none',
                    render: function (data, type, full, meta) {
                        return data.IsAdjustByStaff;
                    },
                },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#transactionAdjustFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= pageSize) ? $(Global.ParticipantTransactionAdjustmentSummary.viewMoreId).css('visibility', 'visible') : $(Global.ParticipantTransactionAdjustmentSummary.viewMoreId).css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#transactionAdjustSearch').val();
            var url = Global.ParticipantTransactionAdjustmentSummary.TransAdjustmentExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#exportTransactionAdj').attr('href', url);

            $('body').off().on('click', '#adjustmentBody', function (e) {
                var localScope = angular.element(document.getElementById("tblClaimTransactionAdjusList")).scope();
                localScope.openMe($(e.target).closest("tr").attr("transaction-id"), $(e.target).closest("tr").attr("transaction-adjustment-id"), 'view', function () { table.search($('#transactionAdjustSearch').val()).draw(false); })
            });
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });

    $(Global.ParticipantTransactionAdjustmentSummary.viewMoreId).on('click', function () {
        var scrollBody = $(tableId).parent('.dataTables_scrollBody')
        $(scrollBody).css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('#txtTransactionId').focus(function (e) {
        $('#searchResult').text('');
    });

    $('#openTransactionAdjust').on('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        $.ajax({
            url: Global.ParticipantClaimsSummary.TransactionValidationUrl,
            method: 'GET',
            data: { friendlyId: $('#txtTransactionId').val()},
            dataType: "JSON",
            success: function (result) {
                if (!result.status)
                {
                    $('#searchResult').text('This transaction does not belong to current Claim.');
                    $('#searchResult').show();
                } else {
                    $('#modalTransactionAdjustmentOpen').modal('hide');

                    var localScope = angular.element(document.getElementById("modalTransactionAdjustmentOpen")).scope();
                    //localScope.open(result.data, 'adjust');
                    localScope.open(result.data, 'adjust', function () { table.search($('#transactionAdjustSearch').val()).draw(false); }); //OTSTM2-573
                }
            },
            failure: function (data) {                
            },
        });
    });

    $('#transactionAdjustSearch').on('keyup', function () {
        table.search(this.value).draw(false);
    });

    $('#transactionAdjustRemove').on('click', function () {
        table.search("").draw(false);
        $('#transactionAdjustFound').hide();
    });

    $('#modalTransactionAdjustmentOpen').on('show.bs.modal', function (event) {
        $('#txtTransactionId').val('');
        $('#searchResult').text('');
        $('#searchResult').hide();
    });

    $('#modalTransAdjustmentsRejectConfirm').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        var currentRow = table.row(rowId).data();

        $('#rejectTransType').html(currentRow.TransactionType);
        $('#rejectTransNum').html(currentRow.TransactionNumber);
        $('#rejectTransId').val(currentRow.TransactionId);
    });

    $('#modalTransAdjustmentsRecallConfirm').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        var currentRow = table.row(rowId).data();

        $('#recallTransType').html(currentRow.TransactionType);
        $('#recallTransNum').html(currentRow.TransactionNumber);
        $('#recallTransId').val(currentRow.TransactionId);
    });

    $('#modalTransAdjustmentsAcceptConfirm').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        var currentRow = table.row(rowId).data();

        $('#acceptTransType').html(currentRow.TransactionType);
        $('#acceptTransNum').html(currentRow.TransactionNumber);
        $('#acceptTransId').val(currentRow.TransactionId);
    });

    $('#btnAcceptTransaction').on('click', function () {

        var req = { id: $('#acceptTransId').val(), accepted: false, getUpdatedData: true };

        ajaxHandler(Global.ParticipantTransactionAdjustmentSummary.TransAdjustAcceptUrl, req);
    });

    $('#btnRejectTransaction').on('click', function () {

        var req = { id: $('#rejectTransId').val(), getUpdatedData: true };

        ajaxHandler(Global.ParticipantTransactionAdjustmentSummary.TransAdjustRejectUrl, req);
    });

    $('#btnRecallTransaction').on('click', function () {

        var req = { id: $('#recallTransId').val(), getUpdatedData: true };

        ajaxHandler(Global.ParticipantTransactionAdjustmentSummary.TransAdjustRecallUrl, req);
    });

    var ajaxHandler = function (url, req) {
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                window.location.reload(true);
            },
            failure: function (data) {
                window.location.reload(true);
            },
        });
    }

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    };
});