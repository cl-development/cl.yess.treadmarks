﻿$(document).ready(function () {
    /*********************************MODAL ACTIVE INACTIVE BUTTON ***************************************/
    var RegistrantStatus;
    var approvedActiveDate = $('.RegistrantActiveStatusApprovedDate').val();
    var approvedInActiveDate = $('.RegistrantInActiveStatusApprovedDate').val();

    String.prototype.strongMe = function () {
        if (this) {
            return ' &lt;strong&gt;' + this + '&lt;/strong&gt; ';
        } else {
            return this; 
        }
    };

    $('#datetimepickerToInactive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedInActiveDate,
        endDate: '+0d',
    });
    $('#datetimepickerToActive').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: approvedActiveDate,
        endDate: '+0d',
    });

    if ($('#activedropdown option:selected').index() == 6) {
        $('#OtherActiveStatus').show();
    }

    if ($('#inactivedropdown option:selected').index() == 11) {
        $('#OtherInActiveStatus').show();
    }

    //$('#modalDeactivatebtn').prop('disabled', false);
    changeDeactivateDisableBtn(false);
    if ($('#inactivedropdown option:selected').index() == 0 || $('#InActiveRegistrantStatusChangeEffectiveDate').val() == '') {
        //$('#modalDeactivatebtn').prop('disabled', true);
        changeDeactivateDisableBtn(true);
    }
    else {
        if (($('#inactivedropdown option:selected').index() == 11) && ($('#OtherInActiveStatus').val() == '' &&
            $('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')) {
            //$('#modalDeactivatebtn').prop('disabled', true);
            changeDeactivateDisableBtn(true);
        }
    }

    //dropdown list change
    $("#activedropdown").change(function () {
        if (this.selectedIndex == 6) {
            $('#OtherActiveStatus').show();
        }
        else {
            $('#OtherActiveStatus').hide();
            $('#OtherActiveStatus').val('');
        }
    });

    $("#inactivedropdown").change(function () {

        if (this.selectedIndex == 0)
            //$('#modalDeactivatebtn').prop('disabled', true);
            changeDeactivateDisableBtn(true);
        else if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
            //$('#modalDeactivatebtn').prop('disabled', true);
            changeDeactivateDisableBtn(true);
        else if (this.selectedIndex == 11 && $('#OtherInActiveStatus').val() == '')
            //$('#modalDeactivatebtn').prop('disabled', true);
            changeDeactivateDisableBtn(true);
        else {
            //$('#modalDeactivatebtn').prop('disabled', false);
            changeDeactivateDisableBtn(false);
        }

        if (this.selectedIndex == 11) {
            $('#OtherInActiveStatus').show();
        }
        else {
            $('#OtherInActiveStatus').hide();
            $('#OtherInActiveStatus').val('');
        }
    });

    function changeDeactivateDisableBtn(val) { //OTSTM2-872 change the logic as followed
        if (ActiveInactiveSecurity.IsReadOnly == "False") {
            if (!val) {
                $('#modalDeactivatebtn').removeClass("disabled").prop("disabled", val);
            }
            else {
                $('#modalDeactivatebtn').addClass("disabled").prop("disabled", val);
            }
        }
    }

    $('#OtherInActiveStatus').on('change', function () {
        var OtherInActiveStatus = $(this);
        $.when(
        OtherInActiveStatus.focusout()).then(function () {
            if ($('#OtherInActiveStatus').val() == '')
                changeDeactivateDisableBtn(true);
                //$('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
                changeDeactivateDisableBtn(true);
                //$('#modalDeactivatebtn').prop('disabled', true);
            else
                changeDeactivateDisableBtn(false);
                //$('#modalDeactivatebtn').prop('disabled', false);
        });
    });

    if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $('#RegistrantStatusChangeEffectiveDate').val());
    }
    if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0) {
        $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $('#InActiveRegistrantStatusChangeEffectiveDate').val());
    }

    $('#RegistrantStatusChangeEffectiveDate').on('change', function () {
        var RegistrantStatusChangeEffectiveDate = $(this);
        $.when(
            RegistrantStatusChangeEffectiveDate.focusout()).then(function () {
                $('.ActiveToolTip').attr('data-original-title', 'Activated: ' + $(this).val());
            });
    });

    $('#InActiveRegistrantStatusChangeEffectiveDate').on('change', function () {

        var InActiveEffectiveDate = $(this);
        $.when(
        InActiveEffectiveDate.focusout()).then(function () {
            $('.InActiveToolTip').attr('data-original-title', 'Inactivated: ' + $(this).val());

            if ($('#InActiveRegistrantStatusChangeEffectiveDate').val() == '')
                changeDeactivateDisableBtn(true);
                //$('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#inactivedropdown option:selected').index() == 0)
                changeDeactivateDisableBtn(true);
                //$('#modalDeactivatebtn').prop('disabled', true);
            else if ($('#inactivedropdown option:selected').index() == 0 && $('#OtherInActiveStatus').val() == '')
                changeDeactivateDisableBtn(true);
                //$('#modalDeactivatebtn').prop('disabled', true);
            else
                changeDeactivateDisableBtn(false);
                //$('#modalDeactivatebtn').prop('disabled', false);
        });
    });

    $('#modalActivateBtn').on('click', function () {

        //check if date and reason are filled in
        if ($('#RegistrantStatusChangeEffectiveDate').val().length > 0) {

            var jsonObj;

            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = d.getFullYear() + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                (day < 10 ? '0' : '') + day;

            Global.CommonActivity.Message = GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Application status from" + 'Inactive'.strongMe() + 'to' + 'Active'.strongMe();
            var ObjectId = $('#applicationId').val();
            if (ObjectId == 0) {
                ObjectId = $('#registrationNumber').val(); 
            }
            
            jsonObj = activeInactiveProcess("Activate");

            if (jsonObj != null && typeof jsonObj != 'undefined') {
                $.ajax({
                    url: '/System/Common/ActiveInactiveVendor',
                    dataType: 'json',
                    type: 'POST',
                    data: { vendorActiveHistory: jsonObj, Activity: { ObjectId: ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, ActivityType: Global.CommonActivity.ActivityType, Message: Global.CommonActivity.Message } },
                    success: function (data) {
                        if (data.isValid) {
                            location.reload();
                        }
                        Global.CommonActivity.Message = '';
                    },
                    failure: function (data) {

                    },
                });
            }
        }
    });  

    $('#modalDeactivatebtn').on('click', function () {

        if ($('#InActiveRegistrantStatusChangeEffectiveDate').val().length > 0 && $("#inactivedropdown").val().length > 0) {
            if ($('#inactivedropdown option:selected').index() != 0) {
                var jsonObj;
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var output = d.getFullYear() + '-' +
                    (month < 10 ? '0' : '') + month + '-' +
                    (day < 10 ? '0' : '') + day;

                Global.CommonActivity.Message = GlobalSearch.UserName() + 'changed'.strongMe() + "participant's Application status from" + 'Active'.strongMe() + 'to' + 'Inactive'.strongMe();
                var ObjectId = $('#applicationId').val();
                if (ObjectId == 0) {
                    ObjectId = $('#registrationNumber').val();
                }
                jsonObj = activeInactiveProcess("Deactivate");

                if (jsonObj != null && typeof jsonObj != 'undefined') {
                    $.ajax({
                        url: '/System/Common/ActiveInactiveVendor',
                        dataType: 'json',
                        type: 'POST',
                        data: { vendorActiveHistory: jsonObj, Activity: { ObjectId: ObjectId, ActivityArea: Global.CommonActivity.ActivityArea, ActivityType: Global.CommonActivity.ActivityType, Message: Global.CommonActivity.Message } },
                        success: function (data) {
                            if (data.isValid) {
                                location.reload();
                            }
                            Global.CommonActivity.Message = '';
                        },
                        failure: function (data) {

                        },
                    });
                }
            }
        }
    });

    var activeInactiveProcess = function (activeStatus) {
        item = {};
        item['VendorID'] = $('.VendorID').val();

        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = d.getFullYear() + '-' +
            (month < 10 ? '0' : '') + month + '-' +
            (day < 10 ? '0' : '') + day;

        switch (activeStatus) {
            case "Activate":
                item["Reason"] = $("#activedropdown").val();
                item["ActiveStateChangeDate"] = combineDateAndTime($('#RegistrantStatusChangeEffectiveDate').val());
                item["ActiveState"] = true;
                item["OtherReason"] = $('#OtherActiveStatus').val();
                item["CreateDate"] = combineDateAndTime(output);
                break;
            case "Deactivate":
                item["Reason"] = $("#inactivedropdown").val();
                item["ActiveStateChangeDate"] = combineDateAndTime($('#InActiveRegistrantStatusChangeEffectiveDate').val());
                item["ActiveState"] = false;
                item["Description"] = $('#NewRegistrationNumber').val();
                item["OtherReason"] = $('#OtherInActiveStatus').val();
                item["CreateDate"] = combineDateAndTime(output);
                break;
            case "ActivateOnlyDate":
                item["ActiveStateChangeDate"] = $('#RegistrantStatusChangeEffectiveDate').val();
                item["ActiveState"] = 1;
                break;
        }
        return item;
    }

    var combineDateAndTime = function (dateString) {

        var currDate = new Date();
        var timeString = currDate.getHours() + ':' + currDate.getMinutes() + ':' + currDate.getSeconds();

        var datetime = new Date(dateString + ' ' + timeString);

        // format the output
        var month = datetime.getMonth() + 1;
        var day = datetime.getDate();
        var year = datetime.getFullYear();

        var hour = datetime.getHours();
        if (hour < 10)
            hour = "0" + hour;

        var min = datetime.getMinutes();
        if (min < 10)
            min = "0" + min;

        var sec = datetime.getSeconds();
        if (sec < 10)
            sec = "0" + sec;

        var dateTimeString = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;

        return dateTimeString;
    };

});

//$('#modalCancelInactivatebtn,#modalCancelActivatebtn,.close').click(function () {
//    location.reload();
//});

/********************************END MODAL ACTIVE INACTIVE BUTTON********************************************/