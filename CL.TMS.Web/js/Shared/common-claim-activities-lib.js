﻿"use strict";

var commonActivitiesLib = angular.module("commonActivitiesLib", ['ui.bootstrap', 'datatables', 'datatables.scroller']);
var RegistrationActivitiesApp = angular.module("RegistrationActivitiesApp", ["ui.bootstrap", "commonLib", "commonActivitiesLib"]);//used for all applications activity tracking
//directives section
commonActivitiesLib.directive('claimActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "activityService", function claimActivityPanel($window, DTOptionsBuilder, DTColumnBuilder, $timeout, activityService) {
    return {

        scope: {
            snpUrl: '@',
        },

        restrict: 'E',

        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));                       
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));                       
                    }
                }
                scope.$digest();
            });

            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '300',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }       
            }
        },

        templateUrl: 'claimActivityPanel.html',

        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {
            $scope.dtInstance = {};

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width','15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date +' '+ date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.snpUrl,
                global: false,
                type: "GET",//"POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    //window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order',[0,'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));                          
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    var direction = settings.aLastSort[0].dir;
                    var searchValue = $('#searchActivity').val();
                    var claimId = Global.Transaction.ClaimId;
                    var url = "/System/Claims/GetAllActivitiesExport?searchText=CCC&sortColumn=AAA&sortDirection=BBB&claimId=DDD";
                    url = url.replace('AAA', sortColumn).replace('BBB', direction).replace('CCC', searchValue).replace('DDD',claimId);
                    $('#exportActivities').attr('href', url);
                    $scope.$apply();
                });

            $scope.url = $scope.snpUrl; //keep original snpUrl
            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false
            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;                
                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };              
                var searchValue = $('#searchActivity').val();
                if (searchValue) { //change original snpUrl by appending searchValue, because searchValue cannot be passed to backend by DataGridModel param if "More" button is clicked (maybe a bug of angular datatables)
                    var url = $scope.snpUrl;
                    url = url + "&temp=" + searchValue;
                    $scope.snpUrl = url;
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }            
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 190)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc : "data",
                                    url: $scope.snpUrl,
                                    global:false, // this makes sure ajaxStart is not triggered
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };                   
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);

                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';                   
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //Auto reload datatable after new activity generated
            $rootScope.$on('TOTAL_ACTIVITY', function (e, data) {
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            });

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    $scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.snpUrl,
                                   global:false,
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }                 
            }

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '300',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }

            function dateTimeConvert(data) {
                if (data === null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches === null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                second = second < 10 ? '0' + second : second;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
                //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

                return {
                    date: d,
                    time: curr_time
                }
            }         
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace


//OTSTM2-424
commonActivitiesLib.controller("activityController", ["$scope", "activityService", function ($scope, activityService) {
    $scope.hasNew =false;
    var data = { claimId: Global.Transaction.ClaimId };
    activityService.totalActivity(data).then(function (dataResult) {
        $scope.totalActivity = dataResult;
    });

    $scope.increasingTotalActivity = function (claimId) {
        var data = { claimId: claimId };
        activityService.totalActivity(data).then(function (dataResult) {
            $scope.totalActivity = dataResult;
            $scope.$parent.$emit('TOTAL_ACTIVITY', dataResult);
        });        
        $scope.$apply();
    }
}]);

//service section
commonActivitiesLib.factory('activityService', ["$http", function ($http) {

    var factory = {};
    //OTSTM2-424
    factory.totalActivity = function (data) {
        var req = { claimId: data.claimId };
        var submitVal = {
            url: "/System/Claims/TotalActivity",
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    return factory;
}]);
