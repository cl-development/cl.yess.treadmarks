﻿if (GlobalSearch.IsStaff().toLowerCase()) {
    var GlobalSearchUI = (function () {
        var settings;

        return {
            settings: {
                otsLogo: document.getElementById('ots-logo'),
                otsSpyGlass: document.getElementById('ots-spyglass'),
                logoCss: { position: 'absolute', top: '10px' },
                spyGlassCss: { position: 'absolute', top: '15px' }
            },
            init: function () {
                settings = this.settings;
                this.bindUIActions()
            },
            bindUIActions: function () {
                GlobalSearchUI.initializeLogo();
            },
            initializeLogo: function () {
                var moveLogo = function (options) {
                    if (!(this instanceof moveLogo)) return new moveLogo(options)
                    options = options || {}
                    this.cssObj = options;
                }
                moveLogo.prototype.moveLeft = function (css) {

                    var left = this.cssObj.previousElementSibling.offsetWidth;

                    this.cssObj.style.position = css.position;
                    this.cssObj.style.marginLeft = '5px';
                    this.cssObj.style.cssFloat = 'left'
                    this.cssObj.style.left = (left + 10) + 'px';
                    this.cssObj.style.top = css.top;
                }
                moveLogo.prototype.moveRight = function (css) {

                    var right = this.cssObj.nextElementSibling.offsetWidth;

                    this.cssObj.style.position = css.position;
                    this.cssObj.style.marginRight = '5px';
                    this.cssObj.style.cssFloat = 'right'
                    this.cssObj.style.right = (right + 15) + 'px';
                    this.cssObj.style.top = css.top;
                }

                var otsLogo = moveLogo(settings.otsLogo);
                otsLogo.moveLeft(settings.logoCss)

                var otsSpyGlass = moveLogo(settings.otsSpyGlass);
                otsSpyGlass.moveRight(settings.spyGlassCss)
            }
        }
    })();

    (function () {
        GlobalSearchUI.init();
    })();
}