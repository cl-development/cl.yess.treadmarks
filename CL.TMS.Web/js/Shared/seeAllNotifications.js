﻿'use strict';


var seeAllNotificationApp = angular.module('seeAllNotification', ['commonLib', 'ui.bootstrap', 'datatables', 'datatables.scroller']);

seeAllNotificationApp.directive('seeAllNotificationPanel', ['DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "notificationsService", function seeAllNotificationPanel(DTOptionsBuilder, DTColumnBuilder, $timeout, notificationsService) {
    return {

        scope: {
            snpUrl: '@',       
        },

        restrict: 'E',

        link: function (scope, elem) {},

        templateUrl: 'seeAllNotificatoinPanel.html',

        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {

            $scope.dtInstance = {};

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date +' '+ date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Message", "Details").withOption('name', 'Message')

            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.snpUrl,
                type: "POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('order',[0,'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(15)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('initComplete', function (settings, result) {
                    $scope.viewMore = (result.data.length >= 15 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    var direction = settings.aLastSort[0].dir;
                    var searchValue = $('#search').val();
                    var url = "/System/Common/GetAllNotificationsExport?searchText=CCC&sortColumn=AAA&sortDirection=BBB";
                    url = url.replace('AAA', sortColumn).replace('BBB', direction).replace('CCC', searchValue);
                    $('#export').attr('href', url);
                    $scope.$apply();
                });

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };

                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 250)
                                .withOption('responsive', true);
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search
            $scope.search = function () {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //Auto reload datatable after new notification generated
            $rootScope.$on('TOTAL_NOTIFICATION', function (e, data) {
                $scope.dtInstance.reloadData();
            });

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            function dateTimeConvert(data) {
                if (data == null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches == null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

                return {
                    date: d,
                    time: curr_time
                }
            };

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    $scope.$apply(function () {
                        //var newTabAvoidPopupBlocker = window.open('', '_blank');
                        //newTabAvoidPopupBlocker.document.write('Loading data...');
                        //update unread to read if an unread notification is clicked in "SeeAllNotification" page
                        var data = { notificationId: aData.ID, newStatus: "Read" };
                        notificationsService.updateNotificationStatus(data).success(function (result) {
                            if (result.status) {                               
                                notificationsService.totalUnreadNotification().then(function (data) {
                                    $scope.totalUnreadNotification = data;                                
                                });
                                //newTabAvoidPopupBlocker.document.write('Loading data...');
                                //newTabAvoidPopupBlocker.location.href = '/System/Claims/ClaimsTypeRouting' + '?claimId=' + aData.ClaimId + '&regNumber=' + aData.RegistrationNumber, '_blank';
                                 window.location.href = '/System/Claims/ClaimsTypeRouting' + '?claimId=' + aData.ClaimId + '&regNumber=' + aData.RegistrationNumber;
                            }
                        });
                    });              
                });
                return nRow;
            }
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace
