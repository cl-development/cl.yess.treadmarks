/* Scripts for Interactive Demonstration of Application */


// Include Header Bar
$(function(){
	if (window.location.href.indexOf("participants-") > -1) {
	    $("#hdrbar .navbar-collapse.collapse").load("includes-hdrbar.html");
	}
});
// Include Sidenav: Participant Generic
$(function(){
  if (window.location.href.indexOf("participants-all-") > -1) {
      $(".sidenav").load("includes-sidenav-generic.html");
  }
});
// Include Sidenav: Participant Steward
$(function(){
  if (window.location.href.indexOf("participants-steward-") > -1) {
      $(".sidenav").load("includes-sidenav-steward.html");
  }
});
// Include Sidenav: Participant Collector
$(function(){
	if (window.location.href.indexOf("participants-collector-") > -1) {
	    $(".sidenav").load("includes-sidenav-collector.html");
	}
});
// Include Sidenav: Participant Hauler
$(function(){
	if (window.location.href.indexOf("participants-hauler-") > -1) {
	    $(".sidenav").load("includes-sidenav-hauler.html");
	}
});
// Include Sidenav: Participant Processor
$(function(){
	if (window.location.href.indexOf("participants-processor-") > -1) {
	    $(".sidenav").load("includes-sidenav-processor.html");
	}
});
// Include Sidenav: Participant RPM
$(function(){
	if (window.location.href.indexOf("participants-rpm-") > -1) {
	    $(".sidenav").load("includes-sidenav-rpm.html");
	}
});

// Include Sidenav: Staff
$(function(){
  if (window.location.href.indexOf("staff-dashboard-") > -1) {
      $(".sidenav").load("includes-sidenav-staff-dashboard.html");
  }
});

// Add Transaction button
$( ".comp-claims-addtransaction-btn-add" ).click(function() {
  $( ".comp-claims-addtransaction-success" ).css("display","block");
});

// Adjust Transaction button
$( ".comp-claims-adjtransaction-btn-open" ).click(function() {
  $( ".comp-claims-adjtransaction-open" ).css("display","none");
  $( ".comp-claims-adjtransaction" ).css("display","block");
});


$("div.clickable").click(
function()
{
    window.location = $(this).attr("url");
});

// staff-all-hauler-claims-summary-adj.html
$(function(){
	// Option 1
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'claims-summary-adj-radio1-opt1') {
            $('.claims-summary-adj-radio1-option1-show').show();           
       }

       else {
            $('.claims-summary-adj-radio1-option1-show').hide();   
       }
   });

	// Option 2
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'claims-summary-adj-radio1-opt2') {
            $('.claims-summary-adj-radio1-option2-show').show();           
       }

       else {
            $('.claims-summary-adj-radio1-option2-show').hide();   
       }
   });

	// Option 3
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'claims-summary-adj-radio1-opt3') {
            $('.claims-summary-adj-radio1-option3-show').show();           
       }

       else {
            $('.claims-summary-adj-radio1-option3-show').hide();   
       }
   });

	// Option 4
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'claims-summary-adj-radio1-opt4') {
            $('.claims-summary-adj-radio1-option4-show').show();           
       }

       else {
            $('.claims-summary-adj-radio1-option4-show').hide();   
       }
   });

});


/* Sample map used on Modal Popover when user hovers over GPS Map Icon */

var getMap = function(opts) {
  var src = "http://maps.googleapis.com/maps/api/staticmap?",
      params = $.extend({
        center: 'Etobicoke, ON',
        zoom: 14,
        size: '242x242',
        maptype: 'roadmap',
        sensor: false
      }, opts),
      query = [];

  $.each(params, function(k, v) {
    query.push(k + '=' + encodeURIComponent(v));
  });

  src += query.join('&');
  return '<img src="' + src + '" />';
}

var content = getMap({center: 'Etobicoke, Ontario'});
//$('.sample-popover-map').popover({ html:true, content: content, placement:'bottom' });

