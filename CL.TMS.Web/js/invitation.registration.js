﻿    $(document).ready(function () {

        /*$("#btnRegister").click(function () {
                   //var a = $("#formRegister").valid();
                   //alert(a);
                   alert("Please add the pending student to the department first");
                   });*/
        $("#formRegister").submit(function (event) {
            ShowLoadingPanel();
            var url = '/AppInvitation/CreateApplicationInvitation'

            //var url = 'http://localhost/Communicator/ApplicationInvitationService.svc/rest/CreateApplicationInvitation';
            var resStatus, resMessage;

            $.ajax({
                type: 'GET',
                url: url,
                async: true,
                dataType: 'json',
                data: 'input=' + $("#inputEmail").val() + "," + $("#participantType").val(),
                success: function (msg) {                   
                    $.each(msg, function (key, value) {
                       
                        if (key == "ResponseStatus")
                            resStatus = value;
                        if (key == "ResponseMessages")
                            resMessage = value;
                    });
                    //Success: 1, Error: 2
                    createMessage(resStatus, resMessage);
                },

                error: function (e) {                 
                    createMessage(2, "Error: " + e.statusText + ", ResponseStatus: " + e.status);
                }
                
            });

            //event.preventDefault();

            function createMessage(type, text) {
                if (type == "1")
                    $("#myAlert").attr("class", "alert alert-success fade in");
                else
                    $("#myAlert").attr("class", "alert alert-danger fade in");

                $("#myAlertMessage").html(text); 
                $("#myAlert").show();
                HideLoadingPanel();
            }
            return false;
        });
        $("[data-hide]").on("click", function () {
            $(this).closest("." + $(this).attr("data-hide")).hide();
        });

        var ShowLoadingPanel = function()
        {            
            $('body').append('<div id="spinner"></div>');
            $("div#spinner").fadeIn("fast");
            //disable register button
            $('#btnRegister').prop('disabled', true);
        };
        var HideLoadingPanel = function () {
            //enable register button
            $('#btnRegister').prop('disabled', false);
            $("#spinner").remove();
        };
    });