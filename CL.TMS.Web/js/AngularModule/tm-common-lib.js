﻿"use strict";

var commonLib = angular.module("commonLib", ['globalSearchLib', 'globalSearchApp']);

var tmAngularSettings = tmAngularSettings || {
    loginPageTitle: 'TreadMarks Login',
    loginRedirectUrl: '/Account/Login', 
    landingPagetUrl:'/System/Common/EmptyPage',
    handleAuthErrors: true
};

commonLib.config(function ($httpProvider) {
    if (!$httpProvider.interceptors) return;

    var buildRedirectUrl = function (message) {
        var returnUrl = '/' + window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, "");        

        return tmAngularSettings.loginRedirectUrl + '?ReturnUrl=' + encodeURIComponent(returnUrl);
    };

    var validateAjaxResponse = function (result) {
        if (!result) return true;

        // Redirected to Login page (Session Expired)
        if (typeof result.data === 'string' && result.data.indexOf(tmAngularSettings.loginPageTitle) > -1) {
            window.location.href = buildRedirectUrl("Your session has expired");
        } 

        return true;
    };

    $httpProvider.interceptors.push(function ($q) {
        return {
            'response': function (response) {
                if (tmAngularSettings.handleAuthErrors) validateAjaxResponse(response);
                return response || $q.when(response);
            },
            'responseError': function (rejection) {                
                switch (rejection.status) {                    
                    case 401: // Your session has expired
                        if (tmAngularSettings.handleAuthErrors) window.location.href = buildRedirectUrl("Your session has expired");
                        break;
                    case 403: // Access Denied
                        if (tmAngularSettings.handleAuthErrors) window.location.href = buildRedirectUrl("Access Denied");
                        break;
                }

                return $q.reject(rejection);
            }
        };
    });
});

commonLib.directive('numbersOnly', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''

                var transformedInput = inputValue.replace(/[^0-9]/g, '');                

                if (transformedInput != inputValue) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }

                return transformedInput;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });

            element.bind('keydown', function (event) {            
                if (angular.isDefined(attr.numbersOnly) && attr.numbersOnly != "" && event.keyCode > 47 && event.keyCode < 127 && this.value.length == attr.numbersOnly) return false;
            });
        }
    };
}]);

commonLib.directive('noDecimal', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (viewValue) {
                if (!viewValue)
                    return viewValue;

                ngModelCtrl.$setViewValue(viewValue);
                ngModelCtrl.$render();
                return viewValue;
            })

            element.bind('keypress', function (event) {
                if (event.keyCode === 46) {
                    event.preventDefault();
                }
            });
        }
    }
}]);

commonLib.directive('clShowErrors', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {

            // find the text box element, which has the 'name' attribute
            var inputEl = el[0].querySelector("[name]");

            // convert the native text box element to an angular element
            var inputNgEl = angular.element(inputEl);

            // get the name on the text box so we know the property to check on the form controller
            var inputName = inputNgEl.attr('name');
            
            // only apply the has-error class after the user leaves the text box
            inputNgEl.bind('blur', function () {
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
            });

            scope.$on('show-errors-event', function () {
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
            });

            scope.$on('hide-errors-event', function () {
                $timeout(function () {
                    el.removeClass('has-error');
                }, 0, false);
            });
        }
    }
}]);

commonLib.directive('validateVendorNumber', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validFormat = function (modelValue) {  
                if (attr.validateVendorNumber == "false") return true;                
                
                var validFormat = false;
                var vendorCodeList = attr.vendorCodeList.split(',');
                _.each(vendorCodeList, function (code) {                    
                    if (code == "Collector" && /^[2][0-9]{6}$/.test(modelValue)) validFormat = true;
                    if (code == "Hauler" && /^[3][0-9]{6}$/.test(modelValue)) validFormat = true;
                    if (code == "Processor" && /^40[0-9]{5}$/.test(modelValue)) validFormat = true;
                    if (code == "RPM" && /^5[0-9]{6}$/.test(modelValue)) validFormat = true;
                });                
                
                return validFormat;
            };
        }
    };
}]);

commonLib.directive('validateSelection', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            ctrl.$validators.validSelection = function (modelValue, viewValue) {
                var validSelection = false;
                if (modelValue == 0) {
                    validSelection = false;
                } else {
                    validSelection = true;
                }
                return validSelection;
            };
        }
    }
}]);

commonLib.directive('ngFileSelect', ['$parse', '$http', function ($parse, $http) {
    if ($http.uploadFile === undefined) {        
        $http.uploadFile = function (config) {
            config.method = config.method || 'POST';
            config.headers = config.headers || {};
            config.headers['Content-Type'] = undefined;

            config.transformRequest = function (data) {
                var formData = new FormData();
                formData.append('file', config.file);               
                formData.append('dataObj', angular.toJson(config.data));

                return formData;
            }

            return $http(config);
        };
    }

    return function (scope, elem, attr) {
        var fn = $parse(attr['ngFileSelect']);

        elem.bind('change', function (evt) {
            var files = [];
            var fileList = evt.target.files;            
            for (var i = 0; i < fileList.length; i++) {
                files.push(fileList.item(i));
            }

            elem.val("");

            scope.$apply(function () {
                fn(scope, { $files: files, $event: evt });
            });
        });

        elem.bind('dragover', function (evt) {
            angular.element(elem).closest("div").addClass("dropzone-highlight");
        });

        elem.bind('dragleave', function (evt) {        
            angular.element(elem).closest("div").removeClass("dropzone-highlight");
        });

        elem.bind('drop', function (evt) {
            angular.element(elem).closest("div").removeClass("dropzone-highlight");
        });
    };
}]);

commonLib.directive('dragMe', [function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs, controller) {
            angular.element(el).draggable({
                cursor: "move"
            });
        }
    };
}]);

commonLib.directive("inputLimit", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr, ngModelCtrl) {
            var inputLimit = parseInt(attr.inputLimit);

            angular.element(element).on("keydown", function (event) {
                if (event.keyCode > 47 && event.keyCode < 127) {
                    if (this.value.length == inputLimit)
                        return false;
                }
            });
        }
    }
}]);

commonLib.directive('validDecimal', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 13;
            var scale = (angular.isDefined(attr.scale) && attr.scale != "") ? parseInt(attr.scale) : 4;

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9\.]/g, '');
                var decimalCheck = clean.split('.');

                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                } else clean = decimalCheck[0];

                if (angular.isDefined(attr.maxvalue) && attr.maxvalue != "" && parseFloat(clean) > parseFloat(attr.maxvalue)) clean = attr.maxvalue;

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }

                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
}]);

commonLib.directive('initializeSmoothZoom', ['$document', function ($document) {
    return {
        restrict: 'A',
        scope: {
            imgSrc: "=",
            zoomWidth: "@",
            zoomHeight: "@"
        },
        link: function (scope, el, attrs, formCtrl) { 
            scope.$watch('imgSrc', function () {
                if (angular.isDefined(attrs.imgSrc) && attrs.imgSrc != "") {                                       
                    angular.element(el).attr("src", scope.imgSrc);
                    scope.initialize();
                    angular.element(el).smoothZoom('Reset');
                }
            }, true);
                        
            scope.initialize = function () {
                angular.element(el).smoothZoom({
                    width: scope.zoomWidth,
                    height: scope.zoomHeight,                    
                    zoom_BUTTONS_SHOW: false,
                    pan_BUTTONS_SHOW: false,
                    pan_LIMIT_BOUNDARY: false
                });
            };
        }
    };
}]);

commonLib.directive('scaleTicketSmoothZoom', ['$document', function ($document) {
    return {
        restrict: 'A',
        scope: {
            
        },
        link: function (scope, el, attrs, formCtrl) {
            attrs.$observe('src', function () {
                angular.element(el).smoothZoom({
                    zoom_BUTTONS_SHOW: false,
                    pan_BUTTONS_SHOW: false,
                    pan_LIMIT_BOUNDARY: false
                });
            })            
        }
    };
}]);

commonLib.directive('popover', [function () {
    return {
        restrict: 'A',        
        link: function (scope, el, attrs, controller) {
            var settings = {
                trigger: 'hover',
                multi: true,
                closeable: true,
                style: '',
                delay: { show: 300, hide: 800 },
                padding: true
            };
            var width = angular.isDefined(attrs.popoverWidth) ? attrs.popoverWidth : 270;
            var materialPopoverOneSettings = { content: $(attrs.popover).html(), width: width };
            angular.element(el).webuiPopover('destroy').webuiPopover($.extend({}, settings, materialPopoverOneSettings));           
        }
    };
}]);

commonLib.directive('dynamicGeoMap', function () {
    return {
        restrict: 'E',
        template: '<div id="gmap" style="width: 850px; height: 450px;"></div>',
        replace: true,
        link: function (scope, element, attrs) {

            var postalExpected = attrs.postal;

            var hdnCoordinatesActual = attrs.gps.split(',');

            var map, infoWindow;
            var markers = [];

            (function initializeMap() {
                var mapOptions = {
                    center: new google.maps.LatLng(hdnCoordinatesActual[0], hdnCoordinatesActual[1]),
                    zoom: 6,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: true
                };

                map = new google.maps.Map(element[0], mapOptions);

                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': postalExpected }, function (results, status) {
                    if (status != 'ZERO_RESULTS') {
                        var postalCordActual = (results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng()).split(',');
                        var postalCordAddress = { lat: parseFloat(postalCordActual[0]), lng: parseFloat(postalCordActual[1]) };
                        geocoder.geocode({ 'location': postalCordAddress }, function (results, status) {
                            if (status != 'ZERO_RESULTS') {
                                setMarker(map, new google.maps.LatLng(postalCordActual[0], postalCordActual[1]), results[1].formatted_address, results[1].formatted_address, 'green-dot.png');
                            }
                        });

                        var hdnCoordinatesAddress = { lat: parseFloat(hdnCoordinatesActual[0]), lng: parseFloat(hdnCoordinatesActual[1]) };
                        geocoder.geocode({ 'location': hdnCoordinatesAddress }, function (results, status) {
                            if (status != 'ZERO_RESULTS') {
                                setMarker(map, new google.maps.LatLng(hdnCoordinatesActual[0], hdnCoordinatesActual[1]), results[1].formatted_address, results[1].formatted_address, 'blue-dot.png');
                            }
                        });

                        google.maps.event.trigger(map, "resize");
                        map.setCenter(new google.maps.LatLng(hdnCoordinatesActual[0], hdnCoordinatesActual[1]));
                    }

                });
            })();

            function setMarker(map, position, title, content, icon) {
                var marker;

                var infowindow = new google.maps.InfoWindow({
                    content: content
                });
                var markerOptions = {
                    position: position,
                    map: map,
                    title: title,
                    icon: 'https://maps.google.com/mapfiles/ms/icons/' + icon
                };

                marker = new google.maps.Marker(markerOptions);
                markers.push(marker);

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

                google.maps.event.trigger(map, "resize");
                map.setCenter(new google.maps.LatLng(hdnCoordinatesActual[0], hdnCoordinatesActual[1]));
            }
        }
    }
});

commonLib.directive('dynamicGeoMapMultipin', function () {
    return {
        restrict: 'E',
        template: '<div id="gmap" style="width: 850px; height: 450px;"></div>',
        replace: true,
        link: function (scope, element, attrs) {
            var postalExpected = attrs.postal;

            var locations = JSON.parse(attrs.gps);

            var map, infoWindow;
            var markers = [];

            var filenames = [];
            $.each(locations, function (index, item) {
                filenames.push(this.iconName);
            });

            (function initializeMap() {
                var mapOptions = {
                    center: new google.maps.LatLng(locations[0].latitude, locations[0].longitude),//by default, set "Registered address" as map center
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: true
                };

                map = new google.maps.Map(element[0], mapOptions);

                var geocoder = new google.maps.Geocoder();
                var hdnCoordinatesAddress = { lat: parseFloat(locations[0].latitude), lng: parseFloat(locations[0].longitude) };
                geocoder.geocode({ 'location': hdnCoordinatesAddress }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        for (var i = 0; i < locations.length; i++) {
                            setMarker(map, new google.maps.LatLng(locations[i].latitude, locations[i].longitude), locations[i].addressText, locations[i].addressText, locations[i].iconName);
                        }
                    }
                });

                google.maps.event.trigger(map, "resize");
            })();

            function setMarker(map, position, title, content, icon) {
                var marker;

                var infowindow = new google.maps.InfoWindow({
                    content: content
                });
                var markerOptions = {
                    position: position,
                    map: map,
                    title: title,
                    icon: 'https://maps.google.com/mapfiles/ms/icons/' + icon
                };

                marker = new google.maps.Marker(markerOptions);
                markers.push(marker);

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

                google.maps.event.trigger(map, "resize");
                map.setCenter(new google.maps.LatLng(locations[0].latitude, locations[0].longitude));
            }
        }
    }
});

commonLib.directive('loadingSpinner', ['$http', function ($http) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs, controller) {

            scope.isSpinning = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isSpinning, function (newValue) {
                if (newValue) {
                    el.show();
                } else {
                    el.hide();
                }
            });
        }
    }
}]);

commonLib.controller('GlobalSearchCtrl', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {
    $scope.searchvalue = $("input:hidden[id='registrationNumber']").val();
    $scope.notFound = false;
    if ($scope.searchvalue.length != 0) {
        $scope.isNumberEntered = { 'color': '#535353' };
    }    
    
    $scope.globalSearch = function () {
        $http({
            url: '/Default/GlobalSearch',
            method: "POST",
            data: { searchvalue: $scope.searchvalue }
        }).success(function (result) {
            if (result.status) {
                if (result.redirectUrl != '') {
                    window.location.href = result.redirectUrl;                    
                }
                else {
                    $scope.notFound = true;
                }
            } else {
                window.location.href = tmAngularSettings.loginRedirectUrl;
            }           
        }).error(function () {
            window.location.href = tmAngularSettings.loginRedirectUrl;
        });
    }

    $scope.clearGlobalSearch = function () {
        $scope.notFound = false;
        $scope.searchvalue = "";
        $scope.globalSearch();
    }

 

    $scope.submitGlobalSearch = function ($event) {
        var keyCode = $event.which || $event.keyCode;

        if (keyCode === 8 || keyCode === 46) {
            if ($scope.searchvalue.length == 0) {
                $scope.isNumberEntered = { 'color': 'white' };                
            }
            $scope.notFound = false;
            return
        };

        if (keyCode === 13) {
            if ($scope.searchvalue.length > 0) {
                $scope.globalSearch();
            }
            else
                $scope.clearGlobalSearch();
        } else {
            $scope.isNumberEntered = { 'color': '#535353' };
            $scope.notFound = false;
        }
    };    
}]);

angular.module('DefaultApp', ['ui.bootstrap', 'commonLib']);