﻿"use strict";

var globalSearchLib = angular.module("globalSearchLib", []);

globalSearchLib.directive('globalSearch', [function () {
    return {
        restrict: 'E',        
        templateUrl: 'global-search.html'
    };
}]);