﻿/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

$(function () {
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#fileUploadInput').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $("#addFilesURL").val(),
        filesContainer: $('table#filesToUploadGrid'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        downloadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var disableDropZone = Boolean($('#disableDropZone').val());
                var allowSelection = Boolean($('#allowSelection').val());
                var allowDownload = Boolean($('#allowDownload').val());
                var allowDelete = Boolean($('#allowDelete').val());
                var isValid = Boolean(file.isValid);

                var row = $('<tr class="template-download fade">' +
                    (allowSelection ? '<td style="width:2%"><input type="checkbox" class="upload-file-selection" checked/></td>' : '') +
                    '<td style="width:10%"><p class="upload-file-name"></p>' +
                     (file.error || !isValid ? '<div class="upload-file-error"><span class="label label-danger">Error</span>&nbsp;</div>' : '') +
                    '</td>' +
                    '<td style="width:30%" ><div><input type="text" style="width:80%" class="form-control upload-file-document-name" placeholder="Description" maxlength="512"/></div></td>' +
                    '<td style="width:20%"><span class="upload-file-size"></span></td>' +
                    //'<td style="width:20%"><span class="createdOn"></span></td>' +
                    (allowDownload ? '<td style="width:5%"><a target="_blank"><input type="button" title="Download" class="btn btn-success color-tm-green-bg btn-sm upload-file-download-btn" style="font-weight:bold" value="Download"/></a></td>' : '') +
                    (allowDelete ? '<td style="width:5%"><button type="button" class="btn btn-danger upload-file-delete-btn" data-toggle="modal" data-target="#modalConfirmDeletionOfUploadedFile" data-backdrop="static" data-keyboard="false"  title="Delete"><strong>X</strong></button></td>' : '') +
                    '</tr>');

                var trimmedEncodedFileName = file.encodedFileName.replace('==', '').replace('=', '');
                row.attr('id', 'uploadFileRow_' + trimmedEncodedFileName).attr("data-filename", file.name).attr('data-fileid', file.id).attr('data-fileuniquename', file.encodedFileName);

                if (!isValid) {
                    row.addClass('has-error');
                    row.find('.upload-file-error').attr('id', 'uploadFileError_' + trimmedEncodedFileName).append(file.validationMessage);

                    if (allowSelection) {
                        var selection = row.find('.upload-file-selection');
                        selection.removeAttr('checked');
                        SetSelectionValidationMessage(selection);
                    }
                }

                if (disableDropZone) {
                    row.find('.upload-file-selection').attr('disabled', 'disabled');
                    row.find('.upload-file-delete-btn').attr('disabled', 'disabled');
                }

                if (file.error) {
                    row.find('.upload-file-name').attr('id', 'uploadFileName_' + trimmedEncodedFileName).text(file.name);
                    row.find('.upload-file-error').attr('id', 'uploadFileError_' + trimmedEncodedFileName).append(file.error);
                } else {
                    row.find('.upload-file-selection').attr('id', 'uploadFileSelection_' + trimmedEncodedFileName);
                    row.find('.upload-file-name').attr('id', 'uploadFileName_' + trimmedEncodedFileName).text(file.name);
                    row.find('.upload-file-size').attr('id', 'uploadFileSize_' + trimmedEncodedFileName).text(o.formatFileSize(file.size));
                    //row.find('.createdOn').text(file.createdOn);
                    row.find('.upload-file-document-name').attr('id', 'uploadFileDocumentName_' + trimmedEncodedFileName).val(file.description);
                    //var fileDeleteUrl = $("#deleteFilesURL").val() + "?transactionID=" + file.transactionId + "&fileId=" + file.id + "&encodedFileName=" + file.encodedFileName;
                    //row.find('.upload-file-delete-btn').attr('id', 'uploadFileDeleteBtn' + file.id).attr('data-type', file.delete_type).attr('data-url', fileDeleteUrl).attr('data-filename', file.name);

                    row.find('.upload-file-delete-btn').attr('id', 'uploadFileDeleteBtn_' + trimmedEncodedFileName).attr('data-filename', file.name);
                    row.find('.upload-file-download-btn').attr('id', 'uploadFileDownloadBtn_' + trimmedEncodedFileName).parent("a").attr("href", file.thumbnail_url);
                }
                rows = rows.add(row);
            });
            return rows;
        },
        add: function (e, data) {
            data.formData = [{ name: "TransactionID", value: $("#transactionID").val() },
                { name: "_fileUploadSectionName", value: $("#fileUploadSectionName").val() },
                { name: "bBankInfo", value: $("#BankInfo").val() }]
            data.submit()
                .success(function (result, textStatus, jqXHR) {
                    var currUploadCount = $('#currUploadFiles').val();
                    currUploadCount = parseInt(currUploadCount, 10) + 1;
                    $('#currUploadFiles').val(currUploadCount);
                    PanelGreenCheckSuppDoc();
                })
                .error(function (jqXHR, textStatus, errorThrown) { })
                .complete(function (result, textStatus, jqXHR) { });
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        completed: function (e, data) {

            if (typeof data.result == 'string') {
                alert(data.result);
                console.log(data.result);
            }

        },
        fileInput: $("#fileUploadInput"),
        dropZone: $("#fileUploadDropzone"),
        autoUpload: false,
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        sequentialUploads: true,
        singleFileUploads: false,
        disableImagePreview: true,
        disableAudioPreview: true
    });

    $('#filesToUploadGrid').on("keypress", '.upload-file-document-name', function (e) {
        if (e.keyCode == 13) {
            return false; // prevent the button click from happening
        }
    });

    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    $('#filesToUploadGrid').on('blur', '.upload-file-document-name', function () {
        var fileEncodedName = $(this).parents('tr').attr('data-fileuniquename');
        var description = $(this).val();
        AddUploadedFileDescription(fileEncodedName, description);
    });

    $('#modalConfirmDeletionOfUploadedFile').on('show.bs.modal', function (e) {
        $('#modalConfirmDeletionOfUploadedFileTargetId').val('');
        $('#modalConfirmDeletionOfUploadedFileTargetId').val(e.relatedTarget.id);

        $('#confirmFileNameToDelete').empty();
        var fileName = e.relatedTarget.getAttribute('data-filename');
        $('#confirmFileNameToDelete').html(fileName);
    });

    $('#modalConfirmDeletionOfUploadedFile').on("click", '#modalConfirmDeletionOfUploadedFileYesBtn', function (e) {

        var targetId = $('#modalConfirmDeletionOfUploadedFileTargetId').val();
        var target = $('#' + targetId);
        var targetParent = target.parents('tr');

        var transactionId = $('#transactionID').val();
        var fileUniqueName = targetParent.attr('data-fileuniquename');

        $('#' + targetId).attr('disabled', 'disabled');

        $.ajax({
            url: $('#deleteFilesURL').val(),
            method: "GET",
            data: [{ name: 'transactionId', value: transactionId }, { name: 'encodedFileName', value: fileUniqueName }, { name: "bBankInfo", value: $("#BankInfo").val() }],
            dataType: 'json',
            success: function () {
                var currUploadCount = $('#currUploadFiles').val();
                currUploadCount = parseInt(currUploadCount, 10) - 1;
                $('#currUploadFiles').val(currUploadCount);
                PanelGreenCheckSuppDoc();

                targetParent.remove();
            },
            fail: function (jqXHR, textStatus, error) {
                $('#' + targetId).removeAttr('disabled');
            }
        });
        $('#modalConfirmDeletionOfUploadedFile').modal('hide');
    });

    $('#fileUploadInput').on('change', '.upload-file-selection', function () {
        var validationMessage = "Incorrect file";
        var fileEncodedName = $(this).parents('tr').attr('data-fileuniquename');

        $.ajax({
            url: "/Default/SetUploadedFileValidation",
            method: "POST",
            data: [{ name: "fileEncodedName", value: fileEncodedName }, { name: "isValid", value: $(this).is(':checked') }, { name: "message", value: ($(this).is(':checked') ? '' : validationMessage) }],
            dataType: "json"
        });

        SetSelectionValidationMessage($(this));
    });


    $('#tblSupportingDocuments input[type="radio"]').on('click', function () {
        PanelGreenCheckSuppDoc();
    });

    $('#btnSupDoc').on('click', function () {
        PanelGreenCheckSuppDoc();
    });

    //$('#fileUploadInput').on('change', function () {
    //    console.log('"#fileUploadInput").on("change")');
    //});

    GetUploadedFiles();
    //when page loads
    PanelGreenCheckSuppDoc();

    SubmitButtonSuppDoc();
});

function AddUploadedFileDescription(fileEncodedName, description) {
    if (fileEncodedName != "") {
        $.ajax({
            url: "/Default/UpdateAttachmentDescription",
            method: "POST",
            data: [{ name: "fileEncodedName", value: fileEncodedName }, { name: "description", value: description }],
            dataType: "json"
        });
    }
}

function SetSelectionValidationMessage(element) {
    if (!element.is(':checked')) {
        var fileName = element.parents('tr').attr('data-filename');
        var validationMessage = "Incorrect file";

        element.attr('data-uploadedFile-validation-message', validationMessage + ' (' + fileName + ')');
    }
    else {
        element.removeAttr('data-uploadedFile-validation-message');
    }
}

function GetUploadedFiles() {
    $('#fileUploadInput').addClass('fileupload-processing');
    $.ajax({
        url: $("#getFilesURL").val(),
        method: "GET",
        data: [{ name: "TransactionID", value: $("#transactionID").val() }, { name: "bBankInfo", value: $("#BankInfo").val() }],
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        PanelGreenCheckSuppDoc
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $('#fileUploadInput').fileupload('option', 'done')
            .call('#fileUploadInput', $.Event('done'), { result: result });
    });
}

function SubmitButtonSuppDoc() {

    //$('#disabledsubmitApplicationBtn').hide();
    //$('#submitApplicationBtn').show();

    $(".greenCheckmark").each(function () {
        if ($(this).attr("src") == statuses[1]) {
            $('#disabledsubmitApplicationBtn').hide();
            $('#submitApplicationBtn').show();
            $("#btnApprove").prop("disabled", false);
        }
        else {
            $('#disabledsubmitApplicationBtn').show();
            $('#submitApplicationBtn').hide();
            $("#btnApprove").prop("disabled", true);
            return false;
        }
    });
}

function PanelGreenCheckSuppDoc() {
    $('#panelSortYardDetails').removeClass('collapse');
    $('#panelSortYardDetails').addClass('collapse-in');

    var fileExists = $('#currUploadFiles').val();
    var fileRequired = false;
    $('#flagSupportingDocuments').attr('src', statuses[0]);
    $("#fmSupportingDoc input[type=radio]").each(function () {
        //var fileExists = $('#filesToUploadGrid tr').length;
        var tr = $(this).parents('tr');
        if (tr.css('display') != "none") {
            var radioValue = $(this).val();
            if (radioValue == 'optionupload' && $(this).is(':checked')) {
                fileRequired = true;
            }
        }
    });
    var isValid = (fileRequired && fileExists > 0) || !fileRequired;
    $('#flagSupportingDocuments').attr('src', isValid ? statuses[1] : statuses[0]);
    SubmitButtonSuppDoc();
}
