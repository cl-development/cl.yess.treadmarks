﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.Security.Authorization;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Configuration;

using Newtonsoft.Json;
using CL.TMS.DataContracts.ViewModel.Transaction.Collector;
using CL.TMS.Common.Enum;
using CL.TMS.Framework.DTO;
using CL.TMS.Security;
using CL.TMS.UI.Common.Security;

namespace CL.TMS.Web.Areas.Collector.Controllers
{
    public class TransactionController : BaseController
    {
        #region Properties
        private ITransactionService transactionService;
        private IRegistrantService registrantService;
        private IClaimService claimService;
        #endregion


        #region Constructor
        public TransactionController(ITransactionService transactionService, IRegistrantService registrantService, IClaimService claimService)
        {
            this.transactionService = transactionService;
            this.registrantService = registrantService;
            this.claimService = claimService;
        }
        #endregion


        #region Action Methods
        public ActionResult Index(int id, string type, int direction)
        {
            ViewBag.ngApp = "TransactionApp";
            ViewBag.VendorCode = TreadMarksConstants.Collector;

            var claimStatus = this.claimService.GetClaimStatus(id);

            ViewBag.ClaimId = id;
            ViewBag.TypeId = type;
            ViewBag.Direction = direction;

            if (null != SecurityContextHelper.CurrentVendor)
            {
                ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;

                var vendorReference = claimService.GetVendorReference(id);
                if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 2)
                {
                    if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                    {
                        return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                    }
                    else
                    {
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                    }
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = (SecurityContextHelper.IsStaff() && claimStatus == ClaimStatus.UnderReview);

            //OTSTM2-155
            bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummaryOutbound) == SecurityResultType.EditSave);
            if (SecurityContextHelper.IsStaff()) ViewBag.AllowAddTransaction = this.claimService.IsClaimAllowAddNewTransaction(id) && bWritePermission;
            else ViewBag.AllowAddTransaction = ((bWritePermission && claimStatus == ClaimStatus.Open));

            ViewBag.ClaimPeriod = this.claimService.GetClaimPeriod(id).ShortName;
            ViewBag.AllowEdit = bWritePermission;

            switch (type)
            {
                case TreadMarksConstants.TCR:
                    ViewBag.TransactionType = TreadMarksConstants.TCR;
                    ViewBag.HeaderName = "TCR Transactions";
                    return View("TCRTransactions");

                case TreadMarksConstants.DOT:
                    ViewBag.TransactionType = TreadMarksConstants.DOT;
                    ViewBag.HeaderName = "DOT Transactions";
                    return View("DOTTransactions");

                default:
                    return RedirectToAction("Index", "Claims", new { area = "Collector" });
            };
        }
        #endregion


        #region Service Methods

        #region Paper Form Service Methods
        [HttpPost]
        public NewtonSoftJsonResult initializePaperForm(int id, string type, int direction)
        {
            var claimPeriod = this.claimService.GetClaimPeriod(id);

            ICollectorTransactionViewModel paperFormModel = new CollectorTransactionViewModel();

            switch (type)
            {
                case TreadMarksConstants.TCR:
                    paperFormModel = new CollectorTCRTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.TCR)));
                    break;

                case TreadMarksConstants.DOT:
                    paperFormModel = new CollectorDOTTransactionViewModel();
                    paperFormModel.initialize();
                    paperFormModel.TireTypeList.AddRange(TransactionHelper.GetTransactionTireTypeViewModelList(TransactionHelper.GetAvailableTireTypeListTireTypeList(TreadMarksConstants.DOT)));
                    break;

                default:
                    return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
            };

            paperFormModel.CurrentVendorId = SecurityContextHelper.CurrentVendor.VendorId;
            paperFormModel.ClaimId = id;
            paperFormModel.IsAutoDismantler = string.Equals(SecurityContextHelper.CurrentVendor.PrimaryBusinessActivity, "Auto dismantler", StringComparison.OrdinalIgnoreCase);

            // Form date related setting    
            TransactionHelper.initializeFormDate(paperFormModel.FormDate, claimPeriod, ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummary));
            paperFormModel.FormDate.DisabledDateRangeList = new List<TransactionDisabledDateRangeViewModel>();
            paperFormModel.FormDate.DisabledDateRangeList.AddRange(transactionService.GetVendorInactiveDateRanges(SecurityContextHelper.CurrentVendor.VendorId));


            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = paperFormModel } };
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionTCR(CollectorTCRTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = registrantService.GetVendorByNumber(transactionDataModel.HaulerNumber.ToString());
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                var creationNotification = transactionService.AddCollectorPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {

                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }

        [HttpPost]
        public NewtonSoftJsonResult AddPaperTransactionDOT(CollectorDOTTransactionViewModel transactionDataModel)
        {
            try
            {
                if (!ModelState.IsValid) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-model" } };
                if (transactionDataModel.CurrentVendorId != SecurityContextHelper.CurrentVendor.VendorId) return new NewtonSoftJsonResult { Data = new { status = false, errorType = "invalid-vendor-context" } };

                transactionDataModel.Inbound = registrantService.GetVendorByNumber(transactionDataModel.HaulerNumber.ToString());
                transactionDataModel.Outbound = SecurityContextHelper.CurrentVendor;

                var creationNotification = transactionService.AddCollectorPapeFormTransaction(transactionDataModel);

                if (transactionDataModel.Inbound != null) TransactionCommonHelper.SendAddTransactionEmailNotification(creationNotification);

                return new NewtonSoftJsonResult { Data = new { status = true, errorType = "" } };
            }
            catch (Exception ex)
            {
                return new NewtonSoftJsonResult { Data = new { status = false, errorType = "save-error" } };
            }
        }
        #endregion

        #region List Handler Service Methods
        public ActionResult GetTransactionListHandler(DataGridModelExtend param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    //columns.Add(0, "DeviceName");
                    //columns.Add(1, "Badge");
                    ////columns.Add(2, "TransactionDate");
                    //columns.Add(3, "TransactionFriendlyId");
                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "Badge");
                            columns.Add(2, "TransactionFriendlyId");
                            columns.Add(3, "ReviewStatus");
                            columns.Add(4, "Adjustment");

                            columns.Add(5, "TransactionDate");
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, TreadMarksConstants.PLT);
                            columns.Add(9, TreadMarksConstants.MT);

                            columns.Add(10, TreadMarksConstants.AGLS);
                            columns.Add(11, TreadMarksConstants.IND);
                            columns.Add(12, TreadMarksConstants.SOTR);
                            columns.Add(13, TreadMarksConstants.MOTR);
                            columns.Add(14, TreadMarksConstants.LOTR);

                            columns.Add(15, TreadMarksConstants.GOTR);
                            columns.Add(16, "IsGenerateTires");
                            break;

                        case TreadMarksConstants.DOT:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "Badge");
                            columns.Add(2, "TransactionFriendlyId");
                            columns.Add(3, "ReviewStatus");
                            columns.Add(4, "Adjustment");

                            columns.Add(5, "TransactionDate");
                            columns.Add(6, "VendorNumber");
                            columns.Add(7, "VendorBusinessName");
                            columns.Add(8, TreadMarksConstants.MOTR);
                            columns.Add(9, TreadMarksConstants.LOTR);

                            columns.Add(10, TreadMarksConstants.GOTR);
                            columns.Add(11, "IsGenerateTires");
                            columns.Add(12, "ScaleWeight");
                            columns.Add(13, "EstWeight");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    //var transactions = transactionService.LoadCollectorVendorTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);
                    PaginationDTO<CollectorCommonTransactionListViewModel, int> transactions = new PaginationDTO<CollectorCommonTransactionListViewModel, int>();

                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummaryOutbound) == SecurityResultType.EditSave);
                    switch (transactionType)
                    {
                        case TreadMarksConstants.DOT:
                            transactions = transactionService.LoadCollectorDOTTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.TCR:
                            transactions = transactionService.LoadCollectorTCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }

                    var json = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };
                    return new NewtonSoftJsonResult(json, false);
                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData, error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCollectorStaffTransactionsList(DataGridModelExtend param)
        {
            //var allTransactions = this.transactionService.LoadAllTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, VendorId);

            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    string transactionType = param.DataType;

                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;
                    #region columns
                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    columns = new Dictionary<int, string>();
                    switch (transactionType)
                    {
                        case TreadMarksConstants.TCR:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "TransactionDate");
                            columns.Add(6, "VendorNumber");
                            //columns.Add(7, "VendorGroupName");
                            //columns.Add(8, "VendorRateGroupName");
                            columns.Add(7, TreadMarksConstants.PLT);

                            columns.Add(8, TreadMarksConstants.MT);
                            columns.Add(9, TreadMarksConstants.AGLS);
                            columns.Add(10, TreadMarksConstants.IND);
                            columns.Add(11, TreadMarksConstants.SOTR);
                            columns.Add(12, TreadMarksConstants.MOTR);

                            columns.Add(13, TreadMarksConstants.LOTR);
                            columns.Add(14, TreadMarksConstants.GOTR);
                            columns.Add(15, "IsGenerateTires");
                            break;

                        case TreadMarksConstants.DOT:
                            columns.Add(0, "DeviceName");
                            columns.Add(1, "TransactionFriendlyId");
                            columns.Add(2, "ReviewStatus");
                            columns.Add(3, "Adjustment");
                            columns.Add(4, "Notes");

                            columns.Add(5, "TransactionDate");
                            columns.Add(6, "VendorNumber");
                            //columns.Add(7, "VendorGroupName");
                            //columns.Add(8, "VendorRateGroupName");
                            columns.Add(7, TreadMarksConstants.MOTR);

                            columns.Add(8, TreadMarksConstants.LOTR);
                            columns.Add(9, TreadMarksConstants.GOTR);
                            columns.Add(10, "IsGenerateTires");
                            columns.Add(11, "ScaleWeight");
                            columns.Add(12, "EstWeight");
                            break;

                        default:
                            break;
                    }
                    #endregion

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];
                    string claimStatus = this.claimService.GetClaimStatus(param.ClaimId).ToString();

                    //var transactions = transactionService.LoadCollectorStaffTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, transactionType, param.ClaimId, param.DataSubType);

                    PaginationDTO<CollectorCommonTransactionListViewModel, int> transactions = new PaginationDTO<CollectorCommonTransactionListViewModel, int>();

                    bool bWritePermission = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsCollectorClaimSummaryOutbound) == SecurityResultType.EditSave);
                    switch (transactionType)
                    {
                        case TreadMarksConstants.DOT:
                            transactions = transactionService.LoadCollectorStaffDOTTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                        case TreadMarksConstants.TCR:
                            transactions = transactionService.LoadCollectorStaffTCRTransactions(param.Start, param.Length, searchText, orderBy, sortDirection, param.ClaimId);
                            break;
                    }

                    //OTSTM2-1064
                    var cBTotalTireCount = Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.CBTotalTireCount").Value);
                    var totalTireCountTCR = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountTCR").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountTCR").Value);
                    var totalTireCountDOT = string.IsNullOrEmpty(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOT").Value) ? -1 : Convert.ToInt32(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Threshold.TotalTireCountDOT").Value);

                    transactions.DTOCollection.ForEach(c =>
                    {
                        c.SetFlagQtyOverTCR(cBTotalTireCount, totalTireCountTCR);
                        c.SetFlagQtyOverDOT(cBTotalTireCount, totalTireCountDOT);
                    });

                    var json1 = new
                    {
                        PageReadonly = !bWritePermission,
                        claimStatus = claimStatus,
                        draw = param.Draw,
                        recordsTotal = transactions.TotalRecords,
                        recordsFiltered = transactions.TotalRecords,
                        data = transactions.DTOCollection
                    };

                    return new NewtonSoftJsonResult(json1, false);
                }
            }
            return Json(new { status = "", error = "SecurityContextHelper.CurrentVendor" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export to CSV Service Methods
        public ActionResult ExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            List<Func<CollectorCommonTransactionListViewModel, string>> delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    columns.Add("iPad/Form");
                    columns.Add("Badge");
                    columns.Add("TCR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    columns.Add("Business Name");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");

                    delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),

                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => (null == u.VendorNumber)?string.Empty:u.VendorNumber.ToString(),
                        u => u.VendorBusinessName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),

                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),

                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                    };
                    break;

                case TreadMarksConstants.DOT:
                    columns.Add("iPad/Form");
                    columns.Add("Badge");
                    columns.Add("DOT #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    columns.Add("Business Name");
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("Scale Weight");
                    columns.Add("Est. Weight");

                    delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.Badge.ToString(),
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),

                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => (null == u.VendorNumber)?string.Empty:u.VendorNumber.ToString(),
                        u => u.VendorBusinessName,
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),

                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                    };
                    break;

                default:
                    break;
            };
            #endregion

            //var Transactions = this.transactionService.GetCollectorExportDetailsParticipantTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);
            List<CollectorCommonTransactionListViewModel> transactions = new List<CollectorCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.DOT:
                    transactions = transactionService.LoadCollectorDOTTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.TCR:
                    transactions = transactionService.LoadCollectorTCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }

            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        public ActionResult StaffExportToExcel(string transactionType, int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            int vendorId = (null != vendor) ? vendor.VendorId : 0;

            var sortcolumn = (Request.QueryString.Count > 4) ? Request.QueryString[4] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 5) ? Request.QueryString[5] : string.Empty;
            var dataSubType = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var columns = new List<string>();
            string sHITtype = "";
            List<Func<CollectorCommonTransactionListViewModel, string>> delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>();

            #region column define
            switch (transactionType)
            {
                case TreadMarksConstants.TCR:
                    columns.Add("iPad/Form");
                    columns.Add("TCR #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    //columns.Add("VendorGroupName");
                    //columns.Add("VendorRateGroupName");
                    columns.Add(TreadMarksConstants.PLT);
                    columns.Add(TreadMarksConstants.MT);
                    columns.Add(TreadMarksConstants.AGLS);
                    columns.Add(TreadMarksConstants.IND);
                    columns.Add(TreadMarksConstants.SOTR);
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");

                    delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),
                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        //u => u.VendorGroupName,
                        //u => u.VendorRateGroupName,
                        u => u.PLT.ToString(),
                        u => u.MT.ToString(),
                        u => u.AGLS.ToString(),
                        u => u.IND.ToString(),
                        u => u.SOTR.ToString(),
                        u => u.MOTR.ToString(),
                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                    };
                    break;

                case TreadMarksConstants.DOT:
                    columns.Add("iPad/Form");
                    columns.Add("DOT #");
                    columns.Add("Review Status");
                    columns.Add("Adjusted By");
                    columns.Add("Notes");
                    columns.Add("Transaction Date");
                    columns.Add("Hauler");
                    //columns.Add("VendorGroupName");
                    //columns.Add("VendorRateGroupName");
                    columns.Add(TreadMarksConstants.MOTR);
                    columns.Add(TreadMarksConstants.LOTR);
                    columns.Add(TreadMarksConstants.GOTR);
                    columns.Add("Generate Tires?");
                    columns.Add("Scale Weight(KG)");
                    columns.Add("Est. Weight(KG)");

                    delegates = new List<Func<CollectorCommonTransactionListViewModel, string>>()
                    {
                        u => u.DeviceName,
                        u => u.TransactionFriendlyId.ToString(),
                        u => (null == u.ReviewStatus)?string.Empty : u.ReviewStatus.ToString(),
                        u => u.Adjustment.ToString(),
                        u => u.NotesAllText.ToString().Replace(" <hr>","; "),

                        u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                        u => u.VendorNumber.ToString(),
                        //u => u.VendorGroupName,
                        //u => u.VendorRateGroupName,
                        u => u.MOTR.ToString(),

                        u => u.LOTR.ToString(),
                        u => u.GOTR.ToString(),
                        u => u.IsGenerateTires.ToYesNoString(),
                        u => u.ScaleWeight.ToString(),
                        u => u.EstWeight.ToString(),
                    };
                    break;

                default:
                    break;
            };
            #endregion

            //var Transactions = this.transactionService.GetCollectorExportDetailsStaffTransactions(search, sortcolumn, sortdirection, vendor.VendorId, transactionType, claimId, dataSubType);
            List<CollectorCommonTransactionListViewModel> transactions = new List<CollectorCommonTransactionListViewModel>();
            switch (transactionType)
            {
                case TreadMarksConstants.DOT:
                    transactions = transactionService.LoadCollectorStaffDOTTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
                case TreadMarksConstants.TCR:
                    transactions = transactionService.LoadCollectorStaffTCRTransactions(0, int.MaxValue, searchText, sortcolumn, sortdirection, claimId).DTOCollection;
                    break;
            }

            string csv = transactions.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Transactions{0}{1}-{2}.csv", transactionType.ToString(), sHITtype, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #endregion
    }
}