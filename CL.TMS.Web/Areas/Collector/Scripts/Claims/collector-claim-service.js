﻿'use strict';

collectorClaimApp.factory('collectorClaimService', ["$http", function ($http) {
    var factory = {};

    factory.getCollectorClaimDetails = function () {
        return $http.get(Global.ParticipantClaimsSummary.ClaimDetailsUrl)
            .then(function (result) {
                return result.data;
            });
    }

    factory.addTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND, LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow,
            tireOrigin: data.TireOrigin,
            tireOriginValue: data.TireOriginValue
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.AddTireOriginUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.exportList = function (data, url) {
        var submitVal = {
            url: url,
            method: "POST",
            data: JSON.stringify(data)
        }
        return $http(submitVal);
    }

    factory.removeTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND,LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow,
            tireOrigin: data.TireOrigin,
            tireOriginValue: data.TireOriginValue
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.RemoveTireOriginUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.updateTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND,LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow,
            tireOrigin: data.TireOrigin,
            tireOriginValue: data.TireOriginValue
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateTireOriginUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.addReuseTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND, LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow,
            reuseTires: data.ReuseTires,
            reuseTiresValue: data.ReuseTiresValue
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.AddReuseTireOriginUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.removeReuseTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND, LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow
        }
        $http({
            url: Global.ParticipantClaimsSummary.RemoveReuseTiresUrl,
            method: "POST",
            data: JSON.stringify(req)
        });
    }

    factory.updateReuseTireOrigin = function (data) {
        var itemRow = {
            PLT: data.PLT, AGLS: data.AGLS, GOTR: data.GOTR, IND: data.IND, LOTR: data.LOTR, MOTR: data.MOTR, MT: data.MT, SOTR: data.SOTR
        }
        var req = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            row: itemRow,
            reuseTires: data.ReuseTires,
            reuseTiresValue: data.ReuseTiresValue
        }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateReuseTireOriginUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.submitClaimCheck = function (collectorClaimModel) {

        var submitClaimModel = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            InboundList: collectorClaimModel.InboundList,
            TotalReuse: collectorClaimModel.ReuseTire,
            TCR: factory.getValueFromKeyVal(collectorClaimModel.TCR),
            DOT: factory.getValueFromKeyVal(collectorClaimModel.DOT),            
            TotalEligible: factory.getValueFromKeyVal(collectorClaimModel.TotalEligible),
            TotalIneligible: factory.getValueFromKeyVal(collectorClaimModel.TotalIneligible),
            GrandTotal: collectorClaimModel.CollectorPayment.GrandTotal
        };

        var submitVal = {
            url: Global.ParticipantClaimsSummary.SubmitClaimCheckUrl,
            method: "POST",
            data: JSON.stringify(submitClaimModel)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.FinalSubmitClaim = function (collectorClaimModel) {
        var submitClaimModel = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
            InboundList: collectorClaimModel.InboundList,
            TotalInbound: factory.getValueFromKeyVal(collectorClaimModel.TotalInbound),
            TotalOutbound: factory.getValueFromKeyVal(collectorClaimModel.TotalOutbound),
            TotalReuse: collectorClaimModel.ReuseTire,
            TCR: factory.getValueFromKeyVal(collectorClaimModel.TCR),
            DOT: factory.getValueFromKeyVal(collectorClaimModel.DOT),
            TotalEligible: factory.getValueFromKeyVal(collectorClaimModel.TotalEligible),
            TotalIneligible: factory.getValueFromKeyVal(collectorClaimModel.TotalIneligible),
            GrandTotal: collectorClaimModel.CollectorPayment.GrandTotal
        };
        var submitVal = {
            url: Global.ParticipantClaimsSummary.FinalSubmitClaimUrl,
            method: "POST",
            data: JSON.stringify(submitClaimModel)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    //---------------------------Common services--------------------

    factory.getValueFromKeyVal = function (data) {

        var obj = new Object();
        for (var i = 0; i < data.length; i++) {
            obj[data[i].Key] = data[i].Value;
        }
        return obj;
    }

    factory.objectLength = function (model) {
        var result = 0;
        for (var prop in model) {
            if (model.hasOwnProperty(prop)) {
                result++;
            }
        }
        return result;
    }

    factory.updateModelItem = function (model, item, type) {
        if (type == 1) {
            for (var i in model) {
                if (model[i].TireOrigin === item.TireOrigin && model[i].TireOriginValue === item.TireOriginValue) {
                    model[i] = item;
                }
            }
            return model;
        }

        if (type == 2) {
            for (var i in model) {
                if (model[i].ReuseTires === item.ReuseTires && model[i].ReuseTiresValue === item.ReuseTiresValue) {
                    model[i] = item;
                }
            }
            return model;
        }
    }

    factory.returnTireOriginItemFromlist = function (model, item, type) {
        if (type == 1) {
            for (var i in model) {
                if (model[i].Description === item.TireOrigin && model[i].DefinitionValue === item.TireOriginValue) {
                    return model[i];
                }
            }
        }
        if (type == 2) {
            for (var i in model) {
                if (model[i].Description === item.ReuseTires && model[i].DefinitionValue === item.ReuseTiresValue) {
                    return model[i];
                }
            }
        }
    }

    factory.isInModel = function (item, model, type) {
        if (type == 1) {
            for (var i in model) {
                if (model[i].TireOrigin === item.TireOrigin && model[i].TireOriginValue === item.TireOriginValue) {
                    return true;
                }
            }
            return false;
        }

        if (type == 2) {
            for (var i in model) {
                if (model[i].ReuseTires === item.ReuseTires && model[i].ReuseTiresValue === item.ReuseTiresValue) {
                    return true;
                }
            }
            return false;
        }        
    }

    factory.removeItemFromDropDownlist = function (list, item) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Description === item.Description) {
                list.splice(i, 1);
            }
        }
        return list;
    }

    factory.calculateTotalFromModel = function (model) {
        var totalSum = {
            PLT: 0, MT: 0,AGLS: 0,IND: 0,SOTR: 0,MOTR: 0,LOTR: 0,GOTR: 0
        };
        for (var el in model) {
            if (model.hasOwnProperty(el)) {
                totalSum.PLT += parseFloat(model[el].PLT);
                totalSum.MT += parseFloat(model[el].MT);
                totalSum.AGLS += parseFloat(model[el].AGLS);
                totalSum.GOTR += parseFloat(model[el].GOTR);
                totalSum.IND += parseFloat(model[el].IND);
                totalSum.LOTR += parseFloat(model[el].LOTR);
                totalSum.MOTR += parseFloat(model[el].MOTR);
                totalSum.SOTR += parseFloat(model[el].SOTR);
            }
        }

        return totalSum;
    }

    factory.updateSupportingDocOption = function (data) {
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateSupportingDocOption,
            method: "POST",
            data: JSON.stringify(data)
        }
        return $http(submitVal);
    }

    factory.CheckSupportingFiles = function (data) {
        var returnVal = (($("input:radio[value='optionupload']:checked").length > 0) && ($('#filesToUploadGrid tr').length <= 0));//true == 'has error'
        $('#fileUploadDropzone').toggleClass('has-error', returnVal).toggleClass('dropzone-required', returnVal);

        var localScope = angular.element(document.getElementById("fileupload")).scope();
        localScope.isRequiredErrorFlag = returnVal;//show [Required] error for $('#fileUploadDropzone')
        return returnVal;
    }
    factory.ReloadSupportingDocs = false; //true === "need to refresh supporting doc list"
    factory.isRequiredErrorFlag = false; //true === "missing required support doc"

    factory.updateUI = function (e, msg) {
        var localScope = angular.element(document.getElementById("fileupload")).scope();
        if (msg.data === 'deletefile') {
            //check supporting error
        }
        if ($("input:radio[value='optionupload']:checked").length > 0) {
            localScope.supportDocIsRequired = true;
        } else {
            localScope.supportDocIsRequired = false;
        }

        if (msg.data === 'addfile') {
            $('#fileUploadDropzone').toggleClass('has-error', false).toggleClass('dropzone-required', false).toggleClass('dropzone', true);
            localScope.isRequiredErrorFlag = false;
            $('#isRequiredErrorFlag').toggleClass('ng-hide', true);//since 'isRequiredErrorFlag = false' somehow not hide the validation error msg
        } else {
            localScope.isRequiredErrorFlag = localScope.supportDocIsRequired &&($('#filesToUploadGrid tr').length <= 0); //need support doc && not exists
        }
    }

    factory.getCollectorTopInfoPickupRateGroupDetail = function (data) {
        var submitVal = {
            url: Global.ParticipantClaimsSummary.VendorRateInfoUrl,
            method: "GET",
            params:data,
        }
        return $http(submitVal);

        return $http.get(Global.ParticipantClaimsSummary.VendorRateInfoUrl)
            .then(function (result) {
                return result.data;
            });
    }

    //---------------------------Common services--------------------

    return factory;
}]);