﻿$(function () {
    var pageSize = 7;
    var selectedRow;
    var pageReadonly = function () {
        var isPageReadonly = $('#tblCollectorInternalAdjusList').attr('data-page-readonly');
        //return isPageReadonly == 'true';
        return false;//OTSTM2-87 need always enable Edit/remove for staff user
    }
    var tableId = '#tblCollectorInternalAdjusList';
    //Define data table
    var table = $(tableId).DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.ParticipantInternalAdjustmentSummary.InternalAdjustmentHandleUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY: 300,
        scrollCollapse: false,
        //OTSTM2-645 createdRow 
        createdRow: function (row, data, index) {
            $(row).addClass('cursor-pointer');
            $(row).attr('data-adjusttype', data.InternalAdjustmentType);
            $(row).attr('data-rowId', data.InternalAdjustmentId);
            $('td:has(a)', row).addClass("special-td");//td with action menu item

            $(row).on('click', 'td:not(.special-td)', function () {
                var row = this.parentElement;
                var internalAdjustId = row.attributes['data-rowid'].value;
                var internalAdjustType = row.attributes["data-adjusttype"].value;
                angular.element(document.getElementById('collectorStaffClaimSummary')).scope().viewInternalAdjust(internalAdjustId, internalAdjustType);
            });
        },
        searching: true,
        ordering: true,
        order: [[0, "desc"]],

        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },

        columns: [
                 {
                     name: "AdjustmentDate",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentDate.toString().substring(0, 10); + "</span>";
                     }
                 },
                 {
                     name: "AdjustmentType",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentType + "</span>";
                     }
                 },
                 //OTSTM2-669
                 {
                     name: "Note",
                     data: null,
                     render: function (data, type, full, meta) {
                         var html = '';
                         if (data.NotesAllText) {
                             html = '<label>' +
                                     '<i id="notes' + meta.row + '" class="fa fa-comment color-tm-grey" data-placement="auto" title="" data-text="' + data.NotesAllText + '"></i>' +
                                     '</label>';
                         }
                         return html;
                     }
                 },
                 {
                     name: "AdjustmentBy",
                     data: null,
                     render: function (data, type, full, meta) {
                         return "<span>" + data.AdjustmentBy + "</span>";
                     }
                 },
                 {
                     className: "td-center",
                     orderable: false,
                     data: null,
                     render: function (data, type, full, meta) {
                         if (Global.Settings.Permission.DisableInternalAdjustmentsBtn) {
                             return '';
                         }
                         //fix for safari
                         var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex='" + data.InternalAdjustmentId + "'" : "";
                         if (!pageReadonly() && (Global.ParticipantClaimsSummary.ClaimsStatus != "Approved")) {
                             var content = "<div class='btn-group dropdown-menu-actions'><a href='javascript:void(0)' tabindex='" + data.InternalAdjustmentId + "' data-trigger='focus' data-toggle='popover'" +
                             " id='actions" + meta.row + "' data-original-title='' title=''><i class='glyphicon glyphicon-cog'></i></a><div class='popover-menu'><ul>";

                             content += "<li><a href='javascript:void(0)' id='editadjust" + meta.row + "' data-rowId='" + data.InternalAdjustmentId + "' data-adjusttype='" + data.InternalAdjustmentType + "' class='clickeditopen'><i class='glyphicon fa fa-pencil-square-o'></i>Edit</a></li>" +
                                     " <li><a href='javascript:void(0)' data-trigger='focus' data-toggle='modal' data-target='#modalRemoveInternalAdjustment' data-rowId='" + meta.row + "'><i class='glyphicon fa fa-trash-o'></i>&nbsp;Delete</a></li>" +
                                     " </ul> </div> </div>";
                             return content;
                         }
          
                         return "";
                     }
                 },
                 {
                     name: "InternalAdjustmentId",
                     data: null,
                     className: 'display-none',
                     render: function (data, type, full, meta) {
                         return "<span>" + data.InternalAdjustmentId + "</span>";
                     }
                 }
        ],
        initComplete: function (settings, json) {
            var scrollBody = $(tableId).parent('.dataTables_scrollBody')
            $(scrollBody).css({ 'overflow-y': 'hidden' });
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#internalAdjustFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= pageSize) ? $('#tblCollectorInternalAdjusList_viewmore').css('visibility', 'visible') : $('#tblCollectorInternalAdjusList_viewmore').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#internalAdjustSearch').val();
            var url = Global.ParticipantInternalAdjustmentSummary.InternalAdjustmentExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#exportInternalAdj').attr('href', url);
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });

        //OTSTM2-669
        $("[id^='note']").webuiPopover({
            width: '500',
            height: '300',
            padding: true,
            multi: true,
            closeable: true,
            title: 'Internal Adjustment Notes',
            type: 'html',
            trigger: 'hover',

            content: function () {
                var internalNote = $(this).attr('data-text');
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            },
            delay: { show: 100, hide: 100 },
        });
    });

    $('#tblCollectorInternalAdjusList_viewmore').on('click', function () {
        var scrollBody = $(tableId).parent('.dataTables_scrollBody')
        $(scrollBody).css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('#internalAdjustSearch').on('keyup', function () {
        Global.noLoadSpinner = true;//stop load spinner
        table.search(this.value).draw(false);
    });

    $('#internalAdjustSearchBtn').on('click', function () {
        var searchValue = $('#internalAdjustSearch').val();
        table.search(searchValue).draw(false);
    });

    $('#internalAdjustRemove').on('click', function () {
        table.search("").draw(false);
        $('#internalAdjustFound').hide();
    });

    $('#modalRemoveInternalAdjustment').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        selectedRow = table.row(rowId).data();
    });

    $('#btnRemoveInternalAdjust').on('click', function () {
        angular.element(document.getElementById('collectorStaffClaimSummary')).scope().removeInternalAdjust(selectedRow);
    });

    $(document).on('click', '.clickeditopen', function () {
        var internalAdjustId = this.attributes['data-rowid'].value;
        var internalAdjustType = this.attributes["data-adjusttype"].value;
        angular.element(document.getElementById('collectorStaffClaimSummary')).scope().editInternalAdjust(internalAdjustId, internalAdjustType);
    });

    $(document).on('click', '.clickviewadjust', function () {
        var internalAdjustId = this.attributes['data-rowid'].value;
        var internalAdjustType = this.attributes["data-adjusttype"].value;
        angular.element(document.getElementById('collectorStaffClaimSummary')).scope().viewInternalAdjust(internalAdjustId, internalAdjustType);
    });

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    };
});