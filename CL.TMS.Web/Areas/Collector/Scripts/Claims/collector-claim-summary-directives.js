﻿'use strict';

collectorClaimApp.directive('collectorInboundAndReuseExport', ['collectorClaimService', '$document', function (collectorClaimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            totalModel: '=',
            fileName: '@',
            typeList: '@',
            url: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            }

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                scope.attrVal = attr.attrHeader;

                if (scope.typeList == "Reuse") {
                    scope.model.ReuseTires = '';
                    scope.totalVal = {
                        ReuseTires: scope.attrVal,
                        PLT: scope.model.PLT,
                        AGLS: scope.model.AGLS,
                        GOTR: scope.model.GOTR,
                        IND: scope.model.IND,
                        LOTR: scope.model.LOTR,
                        MOTR: scope.model.MOTR,
                        MT: scope.model.MT,
                        SOTR: scope.model.SOTR
                    };
                    scope.updatedModel = [];
                    scope.updatedModel.push(scope.model);
                    scope.updatedModel.push(scope.totalVal);
                }

                if (scope.typeList == "Inbound") {
                    scope.totalVal = {
                        TireOrigin: scope.attrVal,
                        PLT: scope.totalModel.PLT,
                        AGLS: scope.totalModel.AGLS,
                        GOTR: scope.totalModel.GOTR,
                        IND: scope.totalModel.IND,
                        LOTR: scope.totalModel.LOTR,
                        MOTR: scope.totalModel.MOTR,
                        MT: scope.totalModel.MT,
                        SOTR: scope.totalModel.SOTR
                    };
                    scope.updatedModel = angular.copy(scope.model);
                    scope.updatedModel.push(scope.totalVal);
                }

                collectorClaimService.exportList(scope.updatedModel, scope.url).success(function (result) {
                    var currentDate = new Date();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });
            });
        }
    };
}]);

collectorClaimApp.directive('collectorStaffPaymentAdjustExport', ['collectorClaimService', '$document', function (collectorClaimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            url: '@',
            fileName: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();
                //to do: scope.model is undefined
                var updatedModel = [
                    { RowName: 'PLT', Rate: scope.model.PLTRate.toFixed(2), QTY: scope.model.PLTQty, Payment: scope.model.PLTPayment.toFixed(2) },
                    { RowName: 'MT-GOTR', Rate: scope.model.MTGOTRRate.toFixed(2), QTY: scope.model.MTGOTRQty, Payment: scope.model.MTGOTRPayment.toFixed(2) },
                    { RowName: 'Payment Adjustments', Rate: '', QTY: '', Payment: scope.model.PaymentAdjustment.toFixed(2) },
                    { RowName: 'Subtotal', Rate: '', QTY: '', Payment: scope.model.SubTotal.toFixed(2) },
                    { RowName: 'HST ' + (scope.model.HSTBase * 100).toFixed(2) + '%', Rate: '', QTY: '', Payment: scope.model.HST.toFixed(2) },
                    { RowName: 'Grand Total', Rate: '', QTY: '', Payment: scope.model.GrandTotal.toFixed(2) },
                    { RowName: 'Payment Due', Rate: '', QTY: '', Payment: formatDate(scope.model.PaymentDueDate) },
                    { RowName: 'Cheque/EFT #', Rate: '', QTY: '', Payment: scope.model.ChequeNumber },
                    { RowName: 'Paid', Rate: '', QTY: '', Payment: formatDate(scope.model.PaidDate) },
                    { RowName: 'Amount Paid ($)', Rate: '', QTY: '', Payment: scope.model.AmountPaid },
                    { RowName: 'Mailed', Rate: '', QTY: '', Payment: formatDate(scope.model.MailedDate) },
                ];

                collectorClaimService.exportList(updatedModel, scope.url).success(function (result) {
                    var currentDate = new Date();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });

            });

            function isNumeric(val) {
                if ((val !== undefined) && (val !== null)) {
                    return !isNaN(parseFloat(val)) && isFinite(val);
                }
                return '';
            }
            function isNull(val) {
                if ((val !== undefined) && (val !== null)) {
                    return val;
                }
                return '';
            }
        }
    }

}]);

collectorClaimApp.directive('collectorPaymentAdjustExport', ['collectorClaimService', '$document', function (collectorClaimService, $document) {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            model: '=',
            url: '@',
            fileName: '@'
        },
        link: function (scope, element, attr, ngModel) {
            scope.anchorCss = {
                "background": "none",
                "left": attr.attrAnchor,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                var updatedModel = [
                    { RowName: 'PLT', Rate: scope.model.PLTRate.toFixed(2), QTY: scope.model.PLTQty, Payment: scope.model.PLTPayment !== null ? scope.model.PLTPayment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'MT-GOTR', Rate: scope.model.MTGOTRRate.toFixed(2), QTY: scope.model.MTGOTRQty, Payment: scope.model.MTGOTRPayment !== null ? scope.model.MTGOTRPayment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Adjustments', Rate: '', QTY: '', Payment: scope.model.PaymentAdjustment !== null ? scope.model.PaymentAdjustment.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Subtotal', Rate: '', QTY: '', Payment: scope.model.SubTotal !== null ? scope.model.SubTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'HST ' + (scope.model.HSTBase * 100).toFixed(2) + '%', Rate: '', QTY: '', Payment: scope.model.HST !== null ? scope.model.HST.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Grand Total', Rate: '', QTY: '', Payment: scope.model.GrandTotal !== null ? scope.model.GrandTotal.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Payment Due', Rate: '', QTY: '', Payment: formatDate(scope.model.PaymentDueDate) },
                    { RowName: 'Cheque/EFT #', Rate: '', QTY: '', Payment: scope.model.ChequeNumber },
                    { RowName: 'Paid', Rate: '', QTY: '', Payment: formatDate(scope.model.PaidDate) },
                    { RowName: 'Amount Paid ($)', Rate: '', QTY: '', Payment: (scope.model.AmountPaid !== null) ? scope.model.AmountPaid.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : '' },
                    { RowName: 'Mailed', Rate: '', QTY: '', Payment: formatDate(scope.model.MailedDate) },
                ];

                collectorClaimService.exportList(updatedModel, scope.url).success(function (result) {
                    var currentDate = new Date();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var filename = scope.fileName + "-" + currentDate;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = angular.element('<a/>');
                        anchor.css({ display: 'none' });
                        var body = $document.find('body').eq(0).append(anchor);

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();

                        anchor.remove();
                    }
                });

            });

            function isNumeric(val) {
                if ((val !== undefined) && (val !== null)) {
                    return !isNaN(parseFloat(val)) && isFinite(val);
                }
                return '';
            }
            function isNull(val) {
                if ((val !== undefined) && (val !== null)) {
                    return val;
                }
                return '';
            }
        }
    }

}]);

collectorClaimApp.directive('collectorTireModifications', ['collectorClaimService', '$http', '$window', '$uibModal', function (collectorClaimService, $http, $window, $uibModal) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'collectorTireModifications.html',
        scope: {
            model: '=',
            tireOriginItems: '=',
            completeTireOriginItems: '=',
            totalInbound: '=',
            tireOriginValidations: '=',
            claimCommonModel: '='
        },
        link: function (scope, element, attr, ngModel, uibModal) {

            initTireOriginItems();

            //---------------------Inbound Panel variables--------------
            scope.editTireOrigin = false;
            scope.addTireOrigin = true;

            scope.dropdownHeader = "Select Origin"
            scope.selectedNameTireOriginFirst = scope.dropdownHeader;
            scope.reasonSectionAdd = null;
            scope.reasonSectionEdit = null;
            scope.selectedNameTireOriginSecond = scope.dropdownHeader;
            scope.validationMessage = scope.tireOriginValidations.Message;
            scope.reminderMessage = scope.tireOriginValidations.Reminder; //OTSTM2-543
            //-----------------------------------------------------------

            scope.setTireSelection = function (selectedVal, originVal, display) {

                scope.reasonSectionAdd = 'Reason # ' + display;
                scope.selectedNameTireOriginFirst = selectedVal;
                scope.originVal = originVal;

                var keepGoing = true; //OTSTM2-543
                scope.tireOriginValidations.TireOriginValidations.forEach(function (currVal) {
                    //OTSTM2-543
                    if (keepGoing) {
                        if (currVal.Description === selectedVal) {
                            if (currVal.DefinitionValue == 1 || currVal.DefinitionValue == 2 || currVal.DefinitionValue == 3) {
                                scope.validationSelection = currVal.Description;
                                scope.validationSelectionForSubCollector = undefined;
                                keepGoing = false;
                            }
                            else if (currVal.DefinitionValue == 4) {
                                scope.validationSelectionForSubCollector = currVal.Description;
                                scope.validationSelection = undefined;
                                keepGoing = false;
                            }
                        }
                        else {
                            scope.validationSelectionForSubCollector = undefined;
                            scope.validationSelection = undefined;
                        }
                    }
                });

                scope.setOverFlowDropDownVisibility('none')
            };

            scope.add = function () {
                if (scope.originVal && scope.selectedNameTireOriginFirst && scope.selectedNameTireOriginFirst != scope.dropdownHeader) {

                    scope.addedTireOriginVal = objTireOrigin(scope.originVal, scope.selectedNameTireOriginFirst, scope.firstRowPLT, scope.firstRowAGLS, scope.firstRowGOTR, scope.firstRowIND, scope.firstRowLOTR,
                        scope.firstRowMOTR, scope.firstRowMT, scope.firstRowSOTR)

                    var filter = scope.model.filter(function (element) {
                        return element.TireOrigin === scope.addedTireOriginVal.TireOrigin && element.TireOriginValue === scope.addedTireOriginVal.TireOriginValue;
                    });

                    //check if this already exists in model
                    if (filter.length == 0) {

                        Object.keys(scope.addedTireOriginVal).map(function (value, index) {
                            setEachZero(scope.addedTireOriginVal, value, index);
                        });

                        if (checkZero(scope.addedTireOriginVal)) {
                            scope.warningFunc('Enter a valid value');
                        }
                        else {
                            collectorClaimService.addTireOrigin(scope.addedTireOriginVal).success(function (result) {
                                if (result.status) {
                                    $http({ url: Global.ParticipantClaimsSummary.LoadInboundUrl, method: "POST", data: {} }).then(function (result) {

                                        scope.model = _.sortBy(result.data.InboundList, function (o) { return o.DisplayOrder; })
                                        scope.selectedNameTireOriginFirst = scope.dropdownHeader;
                                        scope.reasonSectionAdd = null;

                                        var addItem = scope.completeTireOriginItems.find(function (element) {
                                            return findAddedTire(element, scope.addedTireOriginVal);
                                        });
                                        scope.tireOriginItems = scope.tireOriginItems.filter(function (element) {
                                            return element.Description !== addItem.Description
                                        });
                                        scope.tireOriginItems = _.sortBy(scope.tireOriginItems, function (o) { return o.DisplayOrder; })

                                        scope.totalInbound = _.object(_.map(result.data.TotalInbound, _.values));

                                        //clear the input
                                        scope.firstRowPLT = '';
                                        scope.firstRowAGLS = '';
                                        scope.firstRowGOTR = '';
                                        scope.firstRowIND = '';
                                        scope.firstRowLOTR = '';
                                        scope.firstRowMOTR = '';
                                        scope.firstRowMT = '';
                                        scope.firstRowSOTR = '';

                                        scope.init.panelScrollToLeft('overflow-add-dropdown');
                                    });

                                    if ((scope.addedTireOriginVal.TireOriginValue == 2) || (scope.addedTireOriginVal.TireOriginValue == 3) || (scope.addedTireOriginVal.TireOriginValue == 4) || (scope.addedTireOriginVal.TireOriginValue == 9)) {
                                        collectorClaimService.ReloadSupportingDocs = true;
                                    }
                                }
                            });
                        }
                    }
                }

                else {
                    scope.warningFunc('Please select an intial item from Dropdown list');
                }
            }

            scope.edit = function (editedItem) {
                scope.editTireOrigin = true;
                scope.addTireOrigin = false;

                scope.currentEditedItem = editedItem;

                scope.selectedNameTireOriginSecond = scope.currentEditedItem.TireOrigin;
                scope.reasonSectionEdit = 'Reason # ' + scope.currentEditedItem.DisplayOrder;
                scope.secondRowPLT = scope.currentEditedItem.PLT;
                scope.secondRowAGLS = scope.currentEditedItem.AGLS;
                scope.secondRowGOTR = scope.currentEditedItem.GOTR;
                scope.secondRowIND = scope.currentEditedItem.IND;
                scope.secondRowLOTR = scope.currentEditedItem.LOTR;
                scope.secondRowMOTR = scope.currentEditedItem.MOTR;
                scope.secondRowMT = scope.currentEditedItem.MT;
                scope.secondRowSOTR = scope.currentEditedItem.SOTR;
            }

            scope.cancelUpdate = function () {
                scope.editTireOrigin = false;
                scope.addTireOrigin = true;
                scope.selectedNameTireOriginFirst = scope.dropdownHeader;
                scope.reasonSectionAdd = null;
            }

            scope.updateItemRow = function () {

                if (scope.model && scope.currentEditedItem) {

                    //check for the current edited item in the model                    
                    scope.updatedEditedItem = objTireOrigin(scope.currentEditedItem.TireOriginValue, scope.currentEditedItem.TireOrigin, scope.secondRowPLT, scope.secondRowAGLS, scope.secondRowGOTR,
                        scope.secondRowIND, scope.secondRowLOTR, scope.secondRowMOTR, scope.secondRowMT, scope.secondRowSOTR);

                    //check if all is zero or there is empty
                    Object.keys(scope.updatedEditedItem).map(function (value, index) {
                        setEachZero(scope.updatedEditedItem, value, index);
                    });

                    if (checkZero(scope.updatedEditedItem)) {
                        scope.warningFunc('Enter a valid value');
                    }
                    else {
                        collectorClaimService.updateTireOrigin(scope.updatedEditedItem).success(function (result) {
                            if (result.status) {
                                $http({ url: Global.ParticipantClaimsSummary.LoadInboundUrl, method: "POST", data: {} }).then(function (result) {
                                    scope.model = _.sortBy(result.data.InboundList, function (o) { return o.DisplayOrder; })
                                    scope.totalInbound = _.object(_.map(result.data.TotalInbound, _.values));

                                    scope.editTireOrigin = false;
                                    scope.addTireOrigin = true;

                                    scope.selectedNameTireOriginFirst = scope.dropdownHeader;

                                    //clear the input
                                    scope.secondRowPLT = '';
                                    scope.secondRowAGLS = '';
                                    scope.secondRowGOTR = '';
                                    scope.secondRowIND = '';
                                    scope.secondRowLOTR = '';
                                    scope.secondRowMOTR = '';
                                    scope.secondRowMT = '';
                                    scope.secondRowSOTR = '';

                                    scope.init.panelScrollToLeft('overflow-edit-dropdown');
                                })
                            }
                        });
                    }
                }
            }

            function initTireOriginItems() {

                scope.model = _.sortBy(scope.model, function (o) { return o.DisplayOrder; });

                //tire origin dropdown should only contain whats not already added in the database.
                if (scope.model.length > 0) {
                    scope.model.forEach(function (mod) {
                        scope.tireOriginItems.forEach(function (entry, index) {
                            if (mod.TireOrigin == entry.Description &&
                                    mod.TireOriginValue == entry.DefinitionValue) {
                                scope.tireOriginItems.splice(index, 1);
                            }
                        });
                    });
                }

                scope.tireOriginItems = _.sortBy(scope.tireOriginItems, function (o) { return o.DisplayOrder; })
            }

            //arg1 = scope.originVal, arg2 = scope.selectedNameTireOriginFirst
            function objTireOrigin(arg1, arg2, plt, agls, gotr, ind, lotr, motr, mt, sotr) {
                return {
                    TireOriginValue: arg1,
                    TireOrigin: arg2,
                    PLT: plt,
                    AGLS: agls,
                    GOTR: gotr,
                    IND: ind,
                    LOTR: lotr,
                    MOTR: motr,
                    MT: mt,
                    SOTR: sotr
                };
            }
        },
        controller: ['$scope', function ($scope) {

            $scope.removeTires = function (item, type) {
                var removeInstance = $uibModal.open({
                    templateUrl: 'RemoveModal.html',
                    controller: 'TireRemoveModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        removeContent: function () {
                            var obj = {
                                item: item,
                                type: type
                            };

                            return obj;
                        }
                    }
                });

                removeInstance.result.then(function (data) {
                    if (data.item && type == 1) {
                        //to-do update the drop downlist as well total inbound to do
                        $scope.removeTireOriginVal = data.item;

                        $scope.model = $scope.model.filter(function (element) {
                            return (element.TireOrigin !== data.item.TireOrigin && element.TireOriginValue !== data.item.TireOriginValue)
                        })

                        $scope.model = _.sortBy($scope.model, function (o) { return o.DisplayOrder; })

                        var removeItem = $scope.completeTireOriginItems.find(function (element) {
                            return (element.Description === $scope.removeTireOriginVal.TireOrigin && element.DefinitionValue === $scope.removeTireOriginVal.TireOriginValue);
                        });

                        $scope.tireOriginItems.push(removeItem);

                        $scope.tireOriginItems = _.sortBy($scope.tireOriginItems, function (o) { return o.DisplayOrder; })

                        $scope.totalInbound = collectorClaimService.calculateTotalFromModel($scope.model, 1);
                    }
                });
            };

            $scope.$watch("validationSelection", function (newVal, oldVal) {
                if (newVal != oldVal && newVal != undefined) {
                    $scope.warningFunc($scope.validationMessage);
                }
            }, true);

            $scope.warningFunc = function (message) {
                var errorInstance = $uibModal.open({
                    templateUrl: 'TireWarning.html',
                    controller: 'TireWarningModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        warningMessage: function () {
                            return message;
                        }
                    }
                });
            }

            //OTSTM2-543
            $scope.$watch("validationSelectionForSubCollector", function (newVal, oldVal) {
                if (newVal != oldVal && newVal != undefined) {
                    $scope.reminderFunc($scope.reminderMessage);
                }
            }, true);

            $scope.reminderFunc = function (message) {
                var errorInstance = $uibModal.open({
                    templateUrl: 'TireReminder.html',
                    controller: 'TireReminderModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        reminderMessage: function () {
                            return message;
                        }
                    }
                });
            }
        }]
    }
}]);

//OTSTM2-386
collectorClaimApp.directive('staffCollectorTireModifications', ['collectorClaimService', '$http', '$window', 'claimService', '$uibModal', function (collectorClaimService, $http, $window, claimService, $uibModal) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: "staffCollectorTireModifications.html",
        scope: {
            model: '=',
            tireOriginItems: '=',
            completeTireOriginItems: '=',
            totalInbound: '=',
            tireOriginValidations: '='
        },
        link: function (scope, element, attr, ngModel) {

            initTireOriginItems();

            //---------------------Inbound Panel variables--------------
            scope.notAllowToEditTireCount = Global.Settings.Permission.DisableAddInboundBtn;
            scope.statusString = attr.statusString.replace(/ /g, '');

            if (scope.statusString === "UnderReview") {  //show adding row for status "UnderReview"   
                scope.addTireOrigin = true;
            }
            else {
                scope.addTireOrigin = false;
            }
            scope.editTireOrigin = false;
            scope.dropdownHeader = "Select Origin"
            scope.selectedNameTireOriginFirst = scope.dropdownHeader; //for adding
            scope.reasonSectionAdd = null;
            scope.reasonSectionEdit = null;
            scope.selectedNameTireOriginSecond = scope.dropdownHeader; //for editing
            scope.validationMessage = scope.tireOriginValidations.Message;
            scope.reminderMessage = scope.tireOriginValidations.Reminder; //OTSTM2-543
            //-----------------------------------------------------------

            //tire origin dropdownlist selection
            scope.setTireSelection = function (selectedVal, originVal, display) {
                scope.reasonSectionAdd = 'Reason # ' + display;
                scope.selectedNameTireOriginFirst = selectedVal;
                scope.originVal = originVal;

                var keepGoing = true; //OTSTM2-543
                scope.tireOriginValidations.TireOriginValidations.forEach(function (currVal) {
                    //OTSTM2-543
                    if (keepGoing) {
                        if (currVal.Description === selectedVal) {
                            if (currVal.DefinitionValue == 1 || currVal.DefinitionValue == 2 || currVal.DefinitionValue == 3) {
                                scope.validationSelection = currVal.Description;
                                scope.validationSelectionForSubCollector = undefined;
                                keepGoing = false;
                            }
                            else if (currVal.DefinitionValue == 4) {
                                scope.validationSelectionForSubCollector = currVal.Description;
                                scope.validationSelection = undefined;
                                keepGoing = false;
                            }
                        }
                        else {
                            scope.validationSelectionForSubCollector = undefined;
                            scope.validationSelection = undefined;
                        }
                    }
                });

            };

            //click "+" button
            scope.add = function () {
                if (scope.originVal && scope.selectedNameTireOriginFirst && scope.selectedNameTireOriginFirst != scope.dropdownHeader) {

                    scope.addedTireOriginVal = objTireOrigin(scope.originVal, scope.selectedNameTireOriginFirst, scope.firstRowPLT, scope.firstRowAGLS, scope.firstRowGOTR, scope.firstRowIND, scope.firstRowLOTR,
                        scope.firstRowMOTR, scope.firstRowMT, scope.firstRowSOTR)

                    var filter = scope.model.filter(function (element) {
                        return element.TireOrigin === scope.addedTireOriginVal.TireOrigin && element.TireOriginValue === scope.addedTireOriginVal.TireOriginValue;
                    });

                    //check if this already exists in model
                    if (filter.length === 0) {

                        Object.keys(scope.addedTireOriginVal).map(function (value, index) {
                            setEachZero(scope.addedTireOriginVal, value, index);
                        });

                        if (checkZero(scope.addedTireOriginVal)) {
                            scope.warningFunc('Enter a valid value');
                        }
                        else {  //open internal note modal                                                 
                            var internalNoteModal = $uibModal.open({
                                templateUrl: 'internal-note-modal.html',
                                scope: scope,
                                controller: 'InternalNoteModalCtrl',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    item: function () {
                                        return {
                                            currentMode: "Add",
                                        }
                                    }
                                }
                            });
                            internalNoteModal.result.then(function (textAreaModel) {
                                collectorClaimService.addTireOrigin(scope.addedTireOriginVal).success(function (result) {
                                    if (result.status) {
                                        $http({ url: Global.ParticipantClaimsSummary.LoadInboundUrl, method: "POST", data: {} }).then(function (result) {

                                            scope.model = _.sortBy(result.data.InboundList, function (o) { return o.DisplayOrder; })
                                            scope.selectedNameTireOriginFirst = scope.dropdownHeader;
                                            scope.reasonSectionAdd = null;

                                            var addItem = scope.completeTireOriginItems.find(function (element) {
                                                return findAddedTire(element, scope.addedTireOriginVal);
                                            });

                                            scope.tireOriginItems = scope.tireOriginItems.filter(function (element) {
                                                return element.Description !== addItem.Description
                                            });
                                            scope.tireOriginItems = _.sortBy(scope.tireOriginItems, function (o) { return o.DisplayOrder; })

                                            scope.totalInbound = _.object(_.map(result.data.TotalInbound, _.values));

                                            //clear the input
                                            scope.firstRowPLT = '';
                                            scope.firstRowAGLS = '';
                                            scope.firstRowGOTR = '';
                                            scope.firstRowIND = '';
                                            scope.firstRowLOTR = '';
                                            scope.firstRowMOTR = '';
                                            scope.firstRowMT = '';
                                            scope.firstRowSOTR = '';

                                            scope.init.panelScrollToLeft('overflow-add-dropdown');
                                        });
                                    }
                                    if ((scope.addedTireOriginVal.TireOriginValue == 2) || (scope.addedTireOriginVal.TireOriginValue == 3) || (scope.addedTireOriginVal.TireOriginValue == 4) || (scope.addedTireOriginVal.TireOriginValue == 9)) {
                                        collectorClaimService.ReloadSupportingDocs = true;
                                    }
                                    claimService.addInternalNote(textAreaModel).success(function (result) {
                                        claimService.loadInternalNote().success(function (result) {
                                            scope.$parent.$emit('TIRE_ORIGIN_INTERNAL_NOTE', result);
                                        });
                                    });
                                });
                            });
                        }
                    }
                }

                else {
                    scope.warningFunc('Please select an intial item from Dropdown list');
                }
            }

            //click "edit" in action gear
            scope.edit = function (editedItem) {
                scope.editTireOrigin = true;
                scope.addTireOrigin = false;

                scope.currentEditedItem = editedItem;

                scope.selectedNameTireOriginSecond = scope.currentEditedItem.TireOrigin;
                scope.reasonSectionEdit = 'Reason # ' + scope.currentEditedItem.DisplayOrder;
                scope.secondRowPLT = scope.currentEditedItem.PLT;
                scope.secondRowAGLS = scope.currentEditedItem.AGLS;
                scope.secondRowGOTR = scope.currentEditedItem.GOTR;
                scope.secondRowIND = scope.currentEditedItem.IND;
                scope.secondRowLOTR = scope.currentEditedItem.LOTR;
                scope.secondRowMOTR = scope.currentEditedItem.MOTR;
                scope.secondRowMT = scope.currentEditedItem.MT;
                scope.secondRowSOTR = scope.currentEditedItem.SOTR;
            }

            //click "X" in editing mode
            scope.cancelUpdate = function () {
                scope.editTireOrigin = false;
                scope.addTireOrigin = true;
                scope.selectedNameTireOriginFirst = scope.dropdownHeader;
                scope.reasonSectionAdd = null;
            }

            //click "V" in editing mode
            scope.updateItemRow = function () {
                //type=1 is inbound
                if (scope.model && scope.currentEditedItem) {

                    scope.updatedEditedItem = objTireOrigin(scope.currentEditedItem.TireOriginValue, scope.currentEditedItem.TireOrigin, scope.secondRowPLT, scope.secondRowAGLS, scope.secondRowGOTR,
                        scope.secondRowIND, scope.secondRowLOTR, scope.secondRowMOTR, scope.secondRowMT, scope.secondRowSOTR);

                    //check if all is zero or there is empty
                    Object.keys(scope.updatedEditedItem).map(function (value, index) {
                        setEachZero(scope.updatedEditedItem, value, index);
                    });

                    if (checkZero(scope.updatedEditedItem)) {
                        scope.warningFunc('Enter a valid value');
                    }
                    else {
                        var internalNoteModal = $uibModal.open({
                            templateUrl: 'internal-note-modal.html',
                            scope: scope,
                            controller: 'InternalNoteModalCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                item: function () {
                                    return {
                                        currentMode: "Edit",
                                    }
                                }
                            }
                        });
                        internalNoteModal.result.then(function (textAreaModel) {
                            collectorClaimService.updateTireOrigin(scope.updatedEditedItem).success(function (result) {
                                if (result.status) {
                                    $http({ url: Global.ParticipantClaimsSummary.LoadInboundUrl, method: "POST", data: {} }).then(function (result) {
                                        scope.model = _.sortBy(result.data.InboundList, function (o) { return o.DisplayOrder; })
                                        scope.totalInbound = _.object(_.map(result.data.TotalInbound, _.values));

                                        scope.editTireOrigin = false;
                                        scope.addTireOrigin = true;

                                        scope.selectedNameTireOriginFirst = scope.dropdownHeader;

                                        //clear the input
                                        scope.secondRowPLT = '';
                                        scope.secondRowAGLS = '';
                                        scope.secondRowGOTR = '';
                                        scope.secondRowIND = '';
                                        scope.secondRowLOTR = '';
                                        scope.secondRowMOTR = '';
                                        scope.secondRowMT = '';
                                        scope.secondRowSOTR = '';

                                        scope.init.panelScrollToLeft('overflow-edit-dropdown');
                                    });

                                }
                                //claimService.addInternalNote(textAreaModel);
                                claimService.addInternalNote(textAreaModel).success(function (result) {
                                    claimService.loadInternalNote().success(function (result) {
                                        scope.$parent.$emit('TIRE_ORIGIN_INTERNAL_NOTE', result);
                                    });
                                });
                            });
                        });
                    }
                }
            }

            function initTireOriginItems() {

                scope.model = _.sortBy(scope.model, function (o) { return o.DisplayOrder; });

                //tire origin dropdown should only contain whats not already added in the database.
                if (scope.model.length > 0) {
                    scope.model.forEach(function (mod) {
                        scope.tireOriginItems.forEach(function (entry, index) {
                            if (mod.TireOrigin == entry.Description &&
                                    mod.TireOriginValue == entry.DefinitionValue) {
                                scope.tireOriginItems.splice(index, 1);
                            }
                        });
                    });
                }
                scope.tireOriginItems = _.sortBy(scope.tireOriginItems, function (o) { return o.DisplayOrder; })
            }

            //arg1 = scope.originVal, arg2 = scope.selectedNameTireOriginFirst
            function objTireOrigin(arg1, arg2, plt, agls, gotr, ind, lotr, motr, mt, sotr) {
                return {
                    TireOriginValue: arg1,
                    TireOrigin: arg2,
                    PLT: plt,
                    AGLS: agls,
                    GOTR: gotr,
                    IND: ind,
                    LOTR: lotr,
                    MOTR: motr,
                    MT: mt,
                    SOTR: sotr
                };
            }

        },
        controller: ['$scope', function ($scope) {
            //open remove confirmation modal
            $scope.removeTires = function (type, item) {
                var removeInstance = $uibModal.open({
                    templateUrl: 'RemoveModal.html',
                    //scope:$scope,
                    controller: 'TireRemoveModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        removeContent: function () {
                            var obj = {
                                item: item,
                                type: type
                            };
                            return obj;
                        }
                    }
                });

                removeInstance.result.then(function (removeContent) {
                    if (removeContent.type == 1) {
                        var internalNoteModal = $uibModal.open({
                            templateUrl: 'internal-note-modal.html',
                            //scope:$scope,
                            controller: 'InternalNoteModalCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                item: function () {
                                    return {
                                        currentMode: "Remove",
                                        removeContent: removeContent
                                    }
                                }
                            }
                        });

                        internalNoteModal.result.then(function (textAreaModel) {
                            collectorClaimService.removeTireOrigin(removeContent.item).then(function (data) {
                                if (data.statusText === "OK") {
                                    //if (removeContent.item && type == 1) {
                                    //to-do update the drop downlist as well total inbound to do
                                    $scope.removeTireOriginVal = removeContent.item;

                                    $scope.model = $scope.model.filter(function (element) {
                                        return (element.TireOrigin !== removeContent.item.TireOrigin && element.TireOriginValue !== removeContent.item.TireOriginValue)
                                    })

                                    $scope.model = _.sortBy($scope.model, function (o) { return o.DisplayOrder; });

                                    var removeItem = $scope.completeTireOriginItems.find(function (element) {
                                        return (element.Description === $scope.removeTireOriginVal.TireOrigin && element.DefinitionValue === $scope.removeTireOriginVal.TireOriginValue);
                                    });

                                    $scope.tireOriginItems.push(removeItem);

                                    $scope.tireOriginItems = _.sortBy($scope.tireOriginItems, function (o) { return o.DisplayOrder; })

                                    $scope.totalInbound = collectorClaimService.calculateTotalFromModel($scope.model, 1);
                                }
                                claimService.addInternalNote(textAreaModel).success(function (result) {
                                    claimService.loadInternalNote().success(function (result) {
                                        $scope.$parent.$emit('TIRE_ORIGIN_INTERNAL_NOTE', result);
                                    });
                                });
                                switch (removeContent.item.TireOriginValue) {
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 9:
                                        collectorClaimService.ReloadSupportingDocs = true;
                                        break;
                                    default:
                                        break;
                                }
                            });
                        });
                    }
                });
            };

            $scope.$watch("validationSelection", function (newVal, oldVal) {
                if (newVal != oldVal && newVal != undefined) {
                    $scope.warningFunc($scope.validationMessage);
                }
            }, true);

            $scope.warningFunc = function (message) {
                var errorInstance = $uibModal.open({
                    templateUrl: 'TireWarning.html',
                    controller: 'TireWarningModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        warningMessage: function () {
                            return message;
                        }
                    }
                });
            }

            //OTSTM2-543
            $scope.$watch("validationSelectionForSubCollector", function (newVal, oldVal) {
                if (newVal != oldVal && newVal != undefined) {
                    $scope.reminderFunc($scope.reminderMessage);
                }
            }, true);

            $scope.reminderFunc = function (message) {
                var errorInstance = $uibModal.open({
                    templateUrl: 'TireReminder.html',
                    controller: 'TireReminderModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        reminderMessage: function () {
                            return message;
                        }
                    }
                });
            }
        }]
    }
}]);

collectorClaimApp.directive('collectorReuse', ['collectorClaimService', '$http', '$uibModal', function (collectorClaimService, $http, $uibModal) {
    return {
        restrict: 'E',
        templateUrl: 'collectorUpdatedReuseTires.html',
        scope: {
            claimCommonModel: '=',
            reuseTireModel: '='
        },
        link: function (scope, element, attr, ngModel) { },
        controller: ['$scope', function ($scope) {

            $scope.addReuseTireOrigin = false;
            $scope.editReuseTireOrigin = false;
            $scope.finalReuseTireOrigin = false;

            var initialize = function () {

                if ($scope.reuseTireModel.IsAllFieldsZero && !$scope.claimCommonModel.ClaimSummarySecurity.PageReadonlyForParticipant) {
                    $scope.addReuseTireOrigin = true;
                    $scope.editReuseTireOrigin = false;
                    $scope.finalReuseTireOrigin = false;
                }
                else {
                    $scope.finalReuseTireOrigin = true;
                }
            }

            $scope.addReuse = function () {

                $scope.addedReuseTireOriginVal = new reuseTiresObj($scope.reuseAddPLT, $scope.reuseAddAGLS, $scope.reuseAddGOTR, $scope.reuseAddIND,
                    $scope.reuseAddLOTR, $scope.reuseAddMOTR, $scope.reuseAddMT, $scope.reuseAddSOTR);

                if ($scope.addedReuseTireOriginVal.allZero())
                    $scope.warningFunc('Enter a valid value');
                else {
                    //Adding
                    var req = {
                        claimId: Global.ParticipantClaimsSummary.ClaimsID,
                        row: $scope.addedReuseTireOriginVal
                    }
                    var submitVal = {
                        url: Global.ParticipantClaimsSummary.AddUpdatedReuseTiresUrl,
                        method: "POST",
                        data: JSON.stringify(req)
                    }
                    $http(submitVal).success(function (result) {
                        if (result.status) {

                            $http.get(Global.ParticipantClaimsSummary.LoadReuseUrl).then(function (result) {
                                $scope.reuseTireModel = result.data;
                                $scope.addReuseTireOrigin = false;

                                loadDataFinalReuse(
                                    $scope.reuseTireModel.PLT,
                                    $scope.reuseTireModel.AGLS,
                                    $scope.reuseTireModel.GOTR,
                                    $scope.reuseTireModel.IND,
                                    $scope.reuseTireModel.LOTR,
                                    $scope.reuseTireModel.MOTR,
                                    $scope.reuseTireModel.MT,
                                    $scope.reuseTireModel.SOTR);

                                $scope.finalReuseTireOrigin = true;
                            });
                        }
                    });
                }
            }

            $scope.editReuse = function () {
                $scope.addReuseTireOrigin = false;
                $scope.editReuseTireOrigin = true;
                $scope.finalReuseTireOrigin = true;

                $scope.reuseEditPLT = $scope.reuseTireModel.PLT;
                $scope.reuseEditAGLS = $scope.reuseTireModel.AGLS;
                $scope.reuseEditGOTR = $scope.reuseTireModel.GOTR;
                $scope.reuseEditIND = $scope.reuseTireModel.IND;
                $scope.reuseEditLOTR = $scope.reuseTireModel.LOTR;
                $scope.reuseEditMOTR = $scope.reuseTireModel.MOTR;
                $scope.reuseEditMT = $scope.reuseTireModel.MT;
                $scope.reuseEditSOTR = $scope.reuseTireModel.SOTR;
            }

            $scope.removeReuse = function () {
                var removeInstance = $uibModal.open({
                    templateUrl: 'RemoveModal.html',
                    controller: 'TireRemoveModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        removeContent: function () {
                            var obj = {
                                item: $scope.reuseTireModel,
                                type: 2
                            };
                            return obj;
                        }
                    }
                });

                removeInstance.result.then(function (data) {

                    $scope.reuseAddPLT = 0,
                    $scope.reuseAddAGLS = 0,
                    $scope.reuseAddGOTR = 0,
                    $scope.reuseAddIND = 0,
                    $scope.reuseAddLOTR = 0,
                    $scope.reuseAddMOTR = 0,
                    $scope.reuseAddMT = 0,
                    $scope.reuseAddSOTR = 0

                    loadDataFinalReuse(0, 0, 0, 0, 0, 0, 0, 0);


                    $scope.addReuseTireOrigin = true;
                    $scope.editReuseTireOrigin = false;
                    $scope.finalReuseTireOrigin = false;
                });
            }

            $scope.cancelUpdate = function () {
                $scope.addReuseTireOrigin = false;
                $scope.editReuseTireOrigin = false;
                $scope.finalReuseTireOrigin = true;
            }

            $scope.updateReuse = function () {
                $scope.editReuseTireOriginVal = new reuseTiresObj($scope.reuseEditPLT, $scope.reuseEditAGLS, $scope.reuseEditGOTR, $scope.reuseEditIND,
                    $scope.reuseEditLOTR, $scope.reuseEditMOTR, $scope.reuseEditMT, $scope.reuseEditSOTR);

                if ($scope.editReuseTireOriginVal.allZero())
                    $scope.warningFunc('Enter a valid value');
                else {
                    //Editing
                    var req = {
                        claimId: Global.ParticipantClaimsSummary.ClaimsID,
                        row: $scope.editReuseTireOriginVal
                    }
                    var submitVal = {
                        url: Global.ParticipantClaimsSummary.EditReuseTiresUrl,
                        method: "POST",
                        data: JSON.stringify(req)
                    }
                    $http(submitVal).success(function (result) {
                        if (result.status) {

                            $http.get(Global.ParticipantClaimsSummary.LoadReuseUrl).then(function (result) {
                                $scope.reuseTireModel = result.data;
                                $scope.addReuseTireOrigin = false;
                                $scope.editReuseTireOrigin = false;
                                $scope.finalReuseTireOrigin = true;

                                loadDataFinalReuse(
                                    $scope.reuseTireModel.PLT,
                                    $scope.reuseTireModel.AGLS,
                                    $scope.reuseTireModel.GOTR,
                                    $scope.reuseTireModel.IND,
                                    $scope.reuseTireModel.LOTR,
                                    $scope.reuseTireModel.MOTR,
                                    $scope.reuseTireModel.MT,
                                    $scope.reuseTireModel.SOTR);

                                $scope.finalReuseTireOrigin = true;
                            });
                        }
                    });
                }
            }

            initialize();

            $scope.warningFunc = function (message) {
                var errorInstance = $uibModal.open({
                    templateUrl: 'TireWarning.html',
                    controller: 'TireWarningModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        warningMessage: function () {
                            return message;
                        }
                    }
                });
            }

            var reuseTiresObj = function (plt, agls, gotr, ind, lotr, motr, mt, sotr) {
                this.PLT = (plt == 'undefined' || !plt) ? 0 : plt;
                this.AGLS = (agls == 'undefined' || !agls) ? 0 : agls;
                this.GOTR = (gotr == 'undefined' || !gotr) ? 0 : gotr;
                this.IND = (ind == 'undefined' || !ind) ? 0 : ind;
                this.LOTR = (lotr == 'undefined' || !lotr) ? 0 : lotr;
                this.MOTR = (motr == 'undefined' || !motr) ? 0 : motr;
                this.MT = (mt == 'undefined' || !mt) ? 0 : mt;
                this.SOTR = (sotr == 'undefined' || !sotr) ? 0 : sotr;

                this.allZero = function () {
                    if (this.PLT == 0 && this.AGLS == 0 && this.GOTR == 0 && this.IND == 0 && this.LOTR == 0
                        && this.MOTR == 0 && this.MT == 0 && this.SOTR == 0)
                        return true;
                    else return false;
                }
            }

            function loadDataFinalReuse(plt, agls, gotr, ind, lotr, motr, mt, sotr) {
                $scope.reuseTireModel = {
                    PLT: plt,
                    AGLS: agls,
                    GOTR: gotr,
                    IND: ind,
                    LOTR: lotr,
                    MOTR: motr,
                    MT: mt,
                    SOTR: sotr
                }
            }
        }]
    }
}]);

collectorClaimApp.directive('submitClaimcheck', ['collectorClaimService', '$uibModal', function (collectorClaimService, $uibModal) {
    return {
        restrict: 'E',
        templateUrl: 'claimSubmit.html',
        scope: {
            model: "=model",
            submitInfo: "=submitInfo",
            submitBtn: "=submitBtn",
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: ['$scope', function ($scope) {
            if ($scope.model.ClaimCommonModel.StatusString == "Open")
                $scope.submitBtn = true;
            $scope.submitClaimCheck = function () {
                //$scope.$broadcast('show-errors-event');
                if (collectorClaimService.CheckSupportingFiles()) {
                    return;
                }
                else {
                    collectorClaimService.submitClaimCheck($scope.model).then(function (data) {

                        if (data.status != "Invalid data") {
                            if (data.Type == "Error") {

                                var errorInstance = $uibModal.open({
                                    templateUrl: 'ErrorModal.html',
                                    controller: 'ErrorModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        errorMessage: function () {
                                            return data.Message;
                                        }
                                    }
                                });
                            }
                            if (data.Type == "Warning") {

                                var warningInstance = $uibModal.open({
                                    templateUrl: 'WarningModal.html',
                                    controller: 'WarningModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        warningMessage: function () {
                                            return data.Warnings;
                                        }
                                    }
                                });

                                warningInstance.result.then(function () {

                                    var submitOneInstance = $uibModal.open({
                                        templateUrl: 'ModalSubmitOne.html',
                                        controller: 'ModalSubmitOneModalCtrl',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            submitClaimModel: function () {
                                                return $scope.model;
                                            }
                                        }
                                    });
                                    submitOneInstance.result.then(function (data) {
                                        $scope.submitBtn = data;

                                        var submitTwoInstance = $uibModal.open({
                                            templateUrl: 'ModalSubmitTwo.html',
                                            controller: 'ModalSubmitTwoModalCtrl',
                                            size: 'lg',
                                            backdrop: 'static',
                                            resolve: {
                                                submitInfo: function () {
                                                    return $scope.submitInfo;
                                                }
                                            }
                                        });
                                        submitOneInstance.result.then(function (data) {

                                        });
                                    });
                                });
                            }

                            if (data.Type == "Success") {
                                var submitOneInstance = $uibModal.open({
                                    templateUrl: 'ModalSubmitOne.html',
                                    controller: 'ModalSubmitOneModalCtrl',
                                    size: 'lg',
                                    backdrop: 'static',
                                    resolve: {
                                        submitClaimModel: function () {
                                            return $scope.model;
                                        }
                                    }
                                });
                                submitOneInstance.result.then(function (data) {
                                    $scope.submitBtn = data;

                                    var submitTwoInstance = $uibModal.open({
                                        templateUrl: 'ModalSubmitTwo.html',
                                        controller: 'ModalSubmitTwoModalCtrl',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            submitInfo: function () {
                                                return $scope.submitInfo;
                                            }
                                        }
                                    });
                                    submitOneInstance.result.then(function (data) {

                                    });
                                });
                            }
                        }
                        else {
                            $scope.submitBtn = true;
                        }
                    });
                }
            }
        }]
    }
}]);

//OTSTM2-431
collectorClaimApp.directive('dropdownResizeTireOrigin', ['$window', function ($window) {
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, el, atts, formCtrl) {
            //---------------------dropdown collector participant Start-----------------------------------------
            scope.init = (function () {

                var panelTireOrigin = document.getElementById('panel-tire-origin');

                var loadDiv = function (css) {
                    var overflowDropdown = document.getElementById(css)
                    var parentDiv = overflowDropdown.parentElement.getBoundingClientRect();
                    var left = parentDiv.left;
                    var top = parentDiv.top;

                    overflowDropdown.style.position = 'fixed';
                    overflowDropdown.style.top = top + 35 + 'px';

                    overflowDropdown.style.left = left + 'px';

                    if (hasXScrollMoved(panelTireOrigin)) {
                        overflowDropdown.style.left = left + ((overflowDropdown.parentElement.offsetWidth / 2) + 60) + 'px';
                    }
                    else {
                        overflowDropdown.style.left = left + 'px';
                    }
                }

                var panelScrollToLeft = function () {
                    panelTireOrigin.scrollLeft = 0;
                }

                var resetDropdownClass = function (css) {
                    var overflowDropdown = document.getElementById(css)
                    var parentDiv = overflowDropdown.parentElement;
                    parentDiv.className = 'dropdown'
                }

                panelTireOrigin.onscroll = function (event) {
                    //scope.setOverFlowDropDownVisibility('hidden')
                    if (scope.addTireOrigin) {
                        resetDropdownClass('overflow-add-dropdown');
                    }
                    else {
                        resetDropdownClass('overflow-edit-dropdown');
                    }
                }

                $window.onscroll = function (e) {
                    if (scope.addTireOrigin) {
                        loadDiv('overflow-add-dropdown');
                    }
                    else {
                        loadDiv('overflow-edit-dropdown');
                    }
                }

                return {
                    loadDiv: loadDiv,
                    panelScrollToLeft: panelScrollToLeft
                }
            })()

            scope.setOverFlowDropDownVisibility = function (visibility) {
                if (scope.addTireOrigin) {
                    document.getElementById('overflow-add-dropdown').style.visibility = visibility
                }
                else if (scope.editTireOrigin) {
                    document.getElementById('overflow-edit-dropdown').style.visibility = visibility
                }
            }

            scope.dropDownClick = function (callback) {
                if (scope.addTireOrigin) {
                    scope.init.loadDiv('overflow-add-dropdown');
                }
                else {
                    scope.init.loadDiv('overflow-edit-dropdown');
                }
                callback('visible')
            }

            function hasXScrollMoved(elem) {
                return elem.scrollLeft !== 0;
            }

            //---------------------dropdown collector participant END-----------------------------------------------
        }
    }
}]);

//helper functions
function checkZero(item) {
    return item.PLT == 0 && item.AGLS == 0 && item.GOTR == 0
            && item.IND == 0 && item.LOTR == 0
            && item.MOTR == 0 && item.MT == 0 && item.SOTR == 0;
}

function findAddedTire(element, tire) {
    return (element.Description === tire.TireOrigin && element.DefinitionValue === tire.TireOriginValue);
}

function setEachZero(curr, value, index) {
    if (typeof (curr[value]) == 'undefined' || !curr[value]) {
        curr[value] = 0;
    }
}


function formatDate(val) {

    if ((val !== undefined) && (val !== null)) {
        var date = new Date(val);

        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        date = yyyy + '-' + mm + '-' + dd;

        return date;
    }
}