﻿'use strict';

collectorClaimApp.controller("collectorStaffClaimSummaryController", ["$scope", "collectorClaimService", "$uibModal", 'claimService', "$http", "$filter", function ($scope, collectorClaimService, $uibModal, claimService, $http, $filter) {
    $scope.dataloaded = false;
    collectorClaimService.getCollectorClaimDetails().then(function (data) {
        $scope.claimPeriod = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimPeriod;
        $scope.vendorId = data.collectorClaimSummaryViewModel.ClaimCommonModel.VendorId;
        $scope.claimId = data.claimId;
        $scope.staffClaimSummaryModel = data.collectorClaimSummaryViewModel;

        $scope.PageIsReadonly = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;

        //Inbound
        $scope.inboundModel = data.collectorClaimSummaryViewModel.InboundList;
        $scope.totalInbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalInbound);
        //OTSTM2-386
        $scope.tireOriginItems = data.TireOriginItems;
        $scope.completeTireOriginItems = angular.copy($scope.tireOriginItems);
        $scope.tireOriginValidations = data.TireOriginValidations;

        $scope.claimSupportingDocVMList = data.collectorClaimSummaryViewModel.ClaimSupportingDocVMList;
        $scope.supportDocIsRequired = false;
        $scope.claimSupportingDocVMList.forEach(function (item) {
            if (item.SelectedOption === 'optionupload') {
                $scope.supportDocIsRequired = true;
            }
        });

        //ReuseTires
        $scope.reuseTireModel = data.collectorClaimSummaryViewModel.ReuseTire;

        //Outbound
        $scope.TCR = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TCR);
        $scope.DOT = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.DOT);
        //OTSTM2-588
        $scope.TotalEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalEligible);
        $scope.TotalIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalIneligible);
        $scope.TotalOutbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalOutbound);

        //OTSTM2-588 
        //Adjustment
        $scope.TotalAdjustmentEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentEligible);
        $scope.TotalAdjustmentIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentIneligible);
        $scope.TotalAdjustments = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustments);
        $scope.isRequiredErrorFlag = false;

        $scope.$watch("staffClaimSummaryModel.CollectorPayment.MailedDate", function () {
            if ($scope.staffClaimSummaryModel.CollectorPayment.MailedDate != null) {
                claimService.updateClaimMailDate($scope.staffClaimSummaryModel.CollectorPayment.MailedDate);
            }
        });

        if ((data.collectorClaimSummaryViewModel.ClaimStatus.StatusString == "Approved") || (data.collectorClaimSummaryViewModel.ClaimStatus.StatusString == "Receive Payment")) {
            $scope.ShowMailDate = true;
        }
        else {
            $scope.ShowMailDate = false;
        }

        $scope.readOnlyRestriction = data.collectorClaimSummaryViewModel.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;
        if (data.collectorClaimSummaryViewModel.ClaimStatus.StatusString == "Approved") {
            $scope.DisableInternalAdjustmentsBtn = true;
        }
        else {
            $scope.DisableInternalAdjustmentsBtn = Global.Settings.Permission.DisableInternalAdjustmentsBtn;
        }

        $scope.rbNewValue = function (val) {
            collectorClaimService.updateSupportingDocOption({ claimID: this.supportingDocItem.ClaimId, defValue: this.supportingDocItem.DefinitionValue, option: this.supportingDocItem.SelectedOption }).then(function (result) {
                if ($("input:radio[value='optionupload']:checked").length > 0) {
                    $scope.supportDocIsRequired = true;
                } else {
                    $scope.supportDocIsRequired = false;
                }
            });
        };

        $scope.$watch(function () { return collectorClaimService.ReloadSupportingDocs; }, function (newVal, oldVal) {
            $http({ url: Global.ParticipantClaimsSummary.LoadSupportingDocsUrl, method: "POST", data: { claimId: $scope.claimId } }).then(function (result) {
                $scope.claimSupportingDocVMList = result.data;
                $scope.supportDocIsRequired = false;
                $scope.claimSupportingDocVMList.forEach(function (item) {
                    if (item.SelectedOption === 'optionupload') {
                        $scope.supportDocIsRequired = true;
                    }
                });
            });
            collectorClaimService.ReloadSupportingDocs = false;
        });

        $scope.$watch(function () { return $scope.supportDocIsRequired; }, function (newVal, oldVal) {
            if (!newVal) {//not require support doc
                $('#fileUploadDropzone').toggleClass('has-error', false).toggleClass('dropzone-required', false).toggleClass('dropzone', true);
                $scope.isRequiredErrorFlag = false;
            }
        });

        $scope.dropZoneStyle = function () {
            if ($scope.isRequiredError())
                return "dropzone-required";
            else
                return "dropzone";
        };

        $('#filesToUploadGrid').on('update', function (e, msg) {//called while add/remove support file
            collectorClaimService.updateUI(e, msg);
        });

        $scope.dataloaded = true;
        $scope.getStatusColor = setStatusColor(data.collectorClaimSummaryViewModel.ClaimStatus.StatusString);
    });

    //Eligible/Ineligible Adjustments Modal
    var refreshPanels = function () {
        collectorClaimService.getCollectorClaimDetails().then(function (data) {
            $scope.staffClaimSummaryModel = data.collectorClaimSummaryViewModel;

            //Inbound
            $scope.inboundModel = data.collectorClaimSummaryViewModel.InboundList;
            $scope.totalInbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalInbound);
            //OTSTM2-386
            $scope.tireOriginItems = data.TireOriginItems;
            $scope.completeTireOriginItems = angular.copy($scope.tireOriginItems);
            $scope.tireOriginValidations = data.TireOriginValidations;

            //ReuseTires
            $scope.reuseTireModel = data.collectorClaimSummaryViewModel.ReuseTire;
            //OTSTM2-386
            $scope.reuseTireOriginItems = angular.copy($scope.tireOriginItems);

            //Outbound
            $scope.TCR = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TCR);
            $scope.DOT = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.DOT);
            //OTSTM2-588
            $scope.TotalEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalEligible);
            $scope.TotalIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalIneligible);
            $scope.TotalOutbound = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalOutbound);

            //OTSTM2-588 
            //Adjustment
            $scope.TotalAdjustmentEligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentEligible);
            $scope.TotalAdjustmentIneligible = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustmentIneligible);
            $scope.TotalAdjustments = collectorClaimService.getValueFromKeyVal(data.collectorClaimSummaryViewModel.TotalAdjustments);

            //refresh internal adjustment datatable
            $('#tblCollectorInternalAdjusList').DataTable().ajax.reload();

            $scope.dataloaded = true;

            console.log('data.collectorClaimSummaryViewModel.CollectorPayment.GrandTotal', data.collectorClaimSummaryViewModel.CollectorPayment.GrandTotal);
            claimService.getClaimWorkflowViewModel(data.collectorClaimSummaryViewModel.CollectorPayment.GrandTotal).then(function (data) {//update workflow UI
                var localScope = angular.element(document.getElementById("claimWorkflowBar")).scope();
                if (localScope) {//if work flow bar exists, update it base on appsetting/$value
                    localScope.claimWorkflowModel = data;//typeof data: ClaimWorkflowViewModel
                }
            });
        });
    }

    $scope.InternalAdjustTypes = [
       {
           name: 'Tire Counts',
           id: '2'
       },
       {
           name: 'Payment',
           id: '4'
       }
    ];

    var initializeModalResult = function () {
        return {
            selectedItem: null,
            direction: '',
            eligibility: 'eligible',
            onroad: 0,
            offroad: 0,
            plt: 0,
            mt: 0,
            agls: 0,
            ind: 0,
            sotr: 0,
            motr: 0,
            lotr: 0,
            gotr: 0,
            paymentType: '',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,
            unitType: 2,
            //OTSTM2-645
            internalNote: null
        };
    };

    $scope.internalAdjustments = function () {
        var modalResult = initializeModalResult();
        var eligibleAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'InternalAdjustAddAdjustment.html',
            controller: 'InternalAdjustAddAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.InternalAdjustTypes;
                },
                modalResult: function () {
                    return modalResult;
                },
                selectedItem: function () {
                    return $scope.InternalAdjustTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });
        eligibleAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'InternalAdjustConfirm.html',
                controller: 'InternalAdjustConfirmCtrl',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    refreshPanels();
                });
            });

        });
    };

    //Remove internal adjustment
    $scope.removeInternalAdjust = function (data) {
        var internalAdjustmentId = data.InternalAdjustmentId;
        var internalAdjustmentType = data.InternalAdjustmentType;

        claimService.removeInventoryAdjustment(internalAdjustmentType, internalAdjustmentId).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    };
    //Edit internal adjustment
    $scope.editInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'InternalAdjustAddAdjustment.html',
                controller: 'InternalAdjustAddAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return false;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'InternalAdjustConfirm.html',
                    controller: 'InternalAdjustConfirmCtrl',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });
        });
    };

    //view internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var viewItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'InternalAdjustAddAdjustment.html',
                controller: 'InternalAdjustAddAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    refreshPanels();
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'InternalAdjustConfirm.html',
                    controller: 'InternalAdjustConfirmCtrl',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });
        });
    };

    //Mail Date Picker
    $scope.datePicker = {
        status: false
    };

    $scope.openDatePicker = function ($event) {
        $scope.datePicker.status = true;
    };

    var category = 1;
    $scope.loadTopInfoBar = function () {
        var isSpecific = true;
        var addNewRateModal = $uibModal.open({
            templateUrl: "collector-modal-load-top-info-bar.html",
            controller: 'loadTopInfoCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                category: function () {
                    return $scope.category;
                },
                vendorId: function () {
                    return $scope.vendorId;
                },
                claimPeriod: function () {
                    return $scope.claimPeriod;
                }
            }
        });
    }


    //OTSTM2-645 InternalNotesSettings for ClaimInternalNotes
    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalNotesUrl;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalNotesUrl;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalNotes;
    var setStatusColor = function (status) {
        switch (status) {
            case "Submitted":
                return 'color-tm-orange-bg';
                break;
            case "Open":
                return 'color-tm-blue-bg';
                break;
            case "Under Review":
                return 'color-tm-yellow-bg';
                break;
            case "Approved":
                return 'color-tm-green-bg';
                break;
            default:
                return '';
        }
    }
}]);

collectorClaimApp.controller('InternalAdjustAddAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, disableForReadOnlyStaff, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; //OTSTM2-155
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";

    $scope.confirm = function (isValid) {
        //OTSTM2-645
        if (isValid === false || !isValid) return;

        $scope.modalResult.selectedItem = $scope.selectedItem;

        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: false });
    }

    $scope.cancel = function () {
        //$uibModalInstance.dismiss('cancel');
        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: true });
    };

    /*
    ---------------------------------------------------------------------------------
    ------------------validations start here OTSTM2-645------------------------------
    */
    //OTSTM2-645 updating loadUrl and addUrl for InternalNotes for 'internalAdjustModal.html'
    //id = 4 is the InternalAdjustTypes for Payment
    if ($scope.isReadOnly) {
        if (selectedItem.id === '4') {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
        }
        else {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
        }
    }

    //OTSTM2-645 get current choosetype
    $scope.returnCurrentChooseType = function (item) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });
        return curr[0].isValid();
    }

    //OTSTM2-645 param1 = item is current choose item & param1 = rule is the type from error rules for each validationtypes
    $scope.getErrorRule = function (item, rule) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });

        if (curr[0].errorRules.hasOwnProperty(rule)) {
            return curr[0].errorRules[rule]($scope.internaladjustForm);
        }
    }

    //OTSTM2-645
    $scope.isTireTypeAllZeros = function () {
        return validation.checkAllTireItemsZero($scope.modalResult)
    }

    //OTSTM2-645
    $scope.allZeroesOrEmpty = function () {
        var args = [].slice.call(arguments);

        return validation.checkAllFieldsZero(args)
    }

    var validation = {
        init: function () {
            var self = this;

            this.validationTypes = [
                   {
                       name: 'Tire Counts',
                       id: '2',
                       isValid: function () {
                           return self.isValid() && !self.checkAllTireItemsZero($scope.modalResult);
                       },
                       errorRules: {
                           _plt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_plt.$error.number && !form.tirecount_plt.$pristine))
                           },
                           _mt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_mt.$error.number && !form.tirecount_mt.$pristine))
                           },
                           _agls: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_agls.$error.number && !form.tirecount_agls.$pristine))
                           },
                           _ind: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_ind.$error.number && !form.tirecount_ind.$pristine))
                           },
                           _sotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_sotr.$error.number && !form.tirecount_sotr.$pristine))
                           },
                           _motr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_motr.$error.number && !form.tirecount_motr.$pristine))
                           },
                           _lotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_lotr.$error.number && !form.tirecount_lotr.$pristine))
                           },
                           _gotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_gotr.$error.number && !form.tirecount_gotr.$pristine))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Payment',
                       id: '4',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.amount])
                       },
                       errorRules: {
                           _amount: function (form) {
                               return (form.amount.$error.isDecimalRequired && !form.amount.$pristine) || (form.amount.$error.number && !form.amount.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.amount]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   }
            ];

            $scope.$watch('selectedItem', function () {
                if ($scope.internaladjustForm) {
                    $scope.internaladjustForm.$setPristine();
                    $scope.internaladjustForm.$setUntouched();
                }
            });
        },
        isValid: function () {
            return ($scope.internaladjustForm.$valid);
        },
        checkAllTireItemsZero: function (item) {
            //return item.plt == 0 && item.agls == 0 && item.gotr == 0 && item.ind == 0 && item.lotr == 0
            //    && item.motr == 0 && item.mt == 0 && item.sotr == 0;
            var tireObj = {
                plt: item.plt,
                agls: item.agls,
                gotr: item.gotr,
                ind: item.ind,
                lotr: item.lotr,
                motr: item.motr,
                mt: item.mt,
                sotr: item.sotr
            };

            return _.values(tireObj).every(function (val) {
                return (val === 0 || val === null);
            });
        },
        //para accepts only an array
        checkAllFieldsZero: function (arr) {
            if (arr instanceof Array) {
                return arr.every(function (val) {
                    return (val === 0 || val === null);
                });
            }
        },
        getAllValidationTypes: function () {
            return this.validationTypes;
        },
        empty: function (item) {
            return item.plt == null || item.agls == null || item.gotr == null || item.ind == null || item.lotr == null
                || item.motr == null || item.mt == null || item.sotr == null;
        }
    }

    validation.init();

    /*
    ------------------validations end here------------------------------
    --------------------------------------------------------------------
    */
}]);

collectorClaimApp.controller('InternalAdjustConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//OTSTM2-386
collectorClaimApp.controller('TireRemoveModalCtrl', ['$rootScope', '$scope', '$http', '$uibModal', '$uibModalInstance', 'removeContent', 'collectorClaimService', function ($rootScope, $scope, $http, $uibModal, $uibModalInstance, removeContent, collectorClaimService) {

    $scope.removeContent = removeContent; //for removing

    if ($scope.removeContent.type == 1) {
        $scope.removeMessage = removeContent.item.TireOrigin;
        $scope.messageType = Global.ParticipantClaimsSummary.TiresOriginRemoveMessage;
    }

    //click confirm to open internal note modal
    $scope.confirm = function () {
        $uibModalInstance.close($scope.removeContent);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('InternalNoteModalCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'item', '$window', 'collectorClaimService', '$http', '$rootScope', function ($scope, $uibModal, $uibModalInstance, item, $window, collectorClaimService, $http, $rootScope) {

    $scope.currentMode = item.currentMode;
    //for internal note modal header
    switch ($scope.currentMode) {
        case "Add":
            $scope.currentModeForModalHeader = "Adding";
            break;
        case "Edit":
            $scope.currentModeForModalHeader = "Editing";
            break;
        case "Remove":
            $scope.currentModeForModalHeader = "Removing";
            break;
        default:
    }

    $scope.removeContent = item.removeContent; //for removing

    $rootScope.$on('CANCEL', function (e, data) {
        $uibModalInstance.dismiss('cancel');
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.disableBtn = true;
    $scope.enableBtn = function () {
        if ($scope.textAreaModel != null && $scope.textAreaModel != '') {
            $scope.disableBtn = false;
        }
        else {
            $scope.disableBtn = true;
        }
    }

    $scope.confirm = function (currentMode) {
        $uibModalInstance.close($scope.textAreaModel);
    }

}]);


collectorClaimApp.controller('TireWarningModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'warningMessage', function ($rootScope, $scope, $http, $uibModalInstance, warningMessage) {
    $scope.warningMessage = warningMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//OTSTM2-543
collectorClaimApp.controller('TireReminderModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'reminderMessage', function ($rootScope, $scope, $http, $uibModalInstance, reminderMessage) {
    $scope.reminderMessage = reminderMessage;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

collectorClaimApp.controller('loadTopInfoCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'vendorId', 'claimPeriod', 'collectorClaimService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, category, vendorId, claimPeriod, collectorClaimService, $rootScope) {
    var params = {
        vendorId: vendorId,
        claimPeriodStart: claimPeriod.StartDate,
        claimPeriodEnd: claimPeriod.EndDate,
        rateGroupCategory: 'PickupGroup',
    };

    collectorClaimService.getCollectorTopInfoPickupRateGroupDetail(params).then(function (result) {
        $scope.vm = result;
        console.log($scope.vm.data);
        $scope.vm.isView = true;
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };
}]);
