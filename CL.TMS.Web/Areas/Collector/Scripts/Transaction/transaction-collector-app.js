﻿"use strict";

// Transaction APP
var app = angular.module('TransactionApp', ['Transaction.directives', 'Transaction.controllers', 'ui.bootstrap', 'commonLib', 'commonTransactionLib']);


// Controllers Section
var controllers = angular.module('Transaction.controllers', []);

controllers.controller('TransactionCtrl', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);


// Directives Section
var directives = angular.module('Transaction.directives', []);