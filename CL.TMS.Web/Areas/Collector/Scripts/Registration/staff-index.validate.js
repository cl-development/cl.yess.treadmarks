﻿//to be done
$(function () {
    var errorList = []; /* list of invalid Form Fields */
    var panel = $('#fmBusinessLocation');
    /** Diable all the checkbox after approve */
    if ($('#status').val() == 'Approved' || $('#status').val() == 'BankInformationSubmitted' || $('#status').val() == 'BankInformationApproved') {
        $('#MainId input[type=checkbox]').not("[name='TireDetails.TireItem'],[class='isMailingAddressDifferent'],[id='ContactAddressSameAsBusinessAddress']").attr('disabled', 'true');
        $('#CollectorDetails_IsTaxExempt').removeAttr('disabled');
    }
    if ($('#IsParticipant').val() == "True") {
        $('#claimHeader').addClass('divDisabled');
        $('#panelSupportingDocuments').addClass('divDisabled');
        $(":input").attr("disabled", "disabled");
        $('#txtGlobalSearch').removeAttr("disabled");
        $('#showGeneratorAction').css("display", "none");
        $('#showApprovedItem').css("display", "none");
    }

    var cbMailingAddressSameAsBusiness = $('#MailingAddressSameAsBusiness');
    var cbContactAddressSameAsBusinessAddress = $('#ContactAddressSameAsBusinessAddress');
    var cbCollectorDetailsCollectorDetailsVendorIsTaxExempt = $('#CollectorDetails_CollectorDetailsVendor_CommercialLiabHSTNumber');

    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    /* START GLOBAL VALIDATION METHODS */

    var stripHtml = function (html) {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    };

    var dynamicErrorMsg = function (params, element) {
        var labelName = 'Field';
        if ($(element).prev('label').html()) {
            labelName = stripHtml($(element).prev('label').html());
        }
        else if ($(element).parent('div.dropdown').prevAll('label')) {
            labelName = stripHtml($(element).parent('div.dropdown').prevAll('label').html());
        }
        return 'Invalid ' + labelName;
    };

    //removes the green/red highlights from element
    var resetHighlight = function (element, isRequired) {
        $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
        $(element).next('span').removeClass('glyphicon-remove');
        $(element).nextAll('.help-block:first').remove();
        if (isRequired) {
            $(element).closest('.form-group').addClass('has-required');
            $(element).next('span').addClass('glyphicon-ok');
        }
    }

    $.validator.addMethod('lessThanOrEqualTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam <= today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('greaterThanTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam > today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-required", function (value, element) {
        if (value)
            return true;
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-regex-phone", function (value, element) {
        var regex = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$|^$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, 'Expected format ###-###-####');

    $.validator.addMethod("dynamic-regex-postal", function (value, element) {
        var regex = /^[A-Za-z][0-9][A-Za-z]([ ]?)[0-9][A-Za-z][0-9]|([0-9]{5})(?:[- ][0-9]{4})?$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-regex-email", function (value, element) {
        var regex = /^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("TireDetailsRequired", function (value, element) {
        var radioStr = $(element).val();
        if (typeof radioStr == 'undefined') {
            return false;
        }
        var radioBool = JSON.parse(radioStr.toLowerCase());
        if (radioBool && $("select[name='TireDetails.HRelatedProcessor'] option:selected").index() == "0") {
            return false;
        }
        //value is no; thus return true
        return !radioBool;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("HRelatedProcessorRequired", function (value, element) {
        if (typeof $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'undefined') {
            return false;
        }
        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    /* END GLOBAL VALIDATION METHODS */

    /* START BUSINESS INFORMATION VALIDATION */

    var validatorBusinessInfo = $('#fmBusinessLocation').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'BusinessLocation.BusinessName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.OperatingName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.BusinessAddress.AddressLine1': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.City': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.Province': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Postal': {
                required: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$'
            },
            'BusinessLocation.BusinessAddress.Country': {
                required: true,
            },
            'BusinessLocation.Phone': {
                required: true,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
            },
            'BusinessLocation.Extension': {
                required: false
            },
            //'BusinessLocation.BusinessLocationAddress.Ext': {
            //    required: false,
            //    //inInvalidList: true
            //},
            'BusinessLocation.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.AddressLine1': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.City': {
                required: function (element) {
                    return !cbMailingAddressSameAsBusiness.is(':checked');
                }
            },
            'BusinessLocation.MailingAddress.Province': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.Postal': {
                //inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.Country': {
                // inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            }
        },
        highlight: function (element) {
            if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                $(element).closest('.form-group').removeClass('has-required');
                $(element).closest('.form-group').removeClass('has-success');
            }
            $(element).closest('.form-group').addClass('has-error');
            if ($(element).next('span').hasClass('glyphicon-ok'))
                $(element).next('span').removeClass('glyphicon-ok');
            $(element).next('span').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
            $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'BusinessLocation.BusinessName': 'Invalid Business Name',
            'BusinessLocation.OperatingName': 'Invalid Operating Name',
            'BusinessLocation.BusinessAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.BusinessAddress.City': 'Invalid City',
            'BusinessLocation.BusinessAddress.Province': 'Invalid Province/State',
            'BusinessLocation.BusinessAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.BusinessAddress.Postal': 'Invalid Postal Code',
            'BusinessLocation.BusinessAddress.Country': 'Invalid Country',
            'BusinessLocation.Phone': {
                regquired: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'BusinessLocation.Extension': 'Invalid Ext',
            'BusinessLocation.Email': 'Invalid Email',
            'BusinessLocation.MailingAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.MailingAddress.City': 'Invalid City',
            'BusinessLocation.MailingAddress.Province': 'Invalid Province/State',
            'BusinessLocation.MailingAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.MailingAddress.Postal': 'Invalid Postal/Zip',
            'BusinessLocation.MailingAddress.Country': 'Invalid Country',
        }
    });

    /* END BUSINESS INFORMATION VALIDATION */

    /* CONTACT INFORMATION VALIDATION */

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                $(element).closest('.form-group').removeClass('has-required');
                $(element).closest('.form-group').removeClass('has-success');
            }
            $(element).closest('.form-group').addClass('has-error');
            if ($(element).next('span').hasClass('glyphicon-ok'))
                $(element).next('span').removeClass('glyphicon-ok');
            $(element).next('span').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
            $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            '': {
                required: true,
                regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'ContactInformation[0].ContactInformationContact.Name': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.Position': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Ext': {
                required: false
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$'
            },
            'ContactInformation[0].ContactInformationAddress.Address1': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': 'Invalid Primary Contact Name',
            'ContactInformation[0].ContactInformationContact.Position': 'Invalid Position',
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'ContactInformation[0].ContactInformationContact.Ext': 'Invalid Ext.',
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: 'Expected format ###-###-####',
            },
            'ContactInformation[0].ContactInformationContact.Email': 'Invalid Email',
            'ContactInformation[0].ContactInformationAddress.Address1': 'Invalid Address Line 1',
            'ContactInformation[0].ContactInformationAddress.Address2': 'Invalid Address Line 2',
            'ContactInformation[0].ContactInformationAddress.City': 'Invalid City',
            'ContactInformation[0].ContactInformationAddress.PostalCode': 'Invalid Postal /Zip',
            'ContactInformation[0].ContactInformationAddress.Province': 'Invalid Province/State',
            'ContactInformation[0].ContactInformationAddress.Country': 'Invalid Country',
        }
    });

    /* END CONTACT INFORMATION VALIDATION */

    var validatorTireDetailsInfo = $('#fmTireDetails').validate({
        ignore: '.no-validate, :hidden',
        onkeyup: function (element) {
            this.element(element);
        },
        // onfocusout: function (element) {
        // this.element(element);
        // },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            if ($(element).val()) {
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#item_checkbox'));
                }
                else
					($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'))
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TireDetails.TireItem': {
                required: true
            },
            'TireDetails.PLT': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.MT': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.AGLS': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.IND': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.SOTR': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.MOTR': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.LOTR': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            'TireDetails.GOTR': {
                required: true,
                regex: '^[0-9]{1,}$'
            },
            //'TireDetails.cbUsedTiresInStorage':{
            //    required: true,

            //},
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            if (isActiveElement(propertyName) || $(element).val()) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-success');
                    $(element).closest('.form-group').addClass('has-error');
                    $(element).next('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            if (isActiveElement(propertyName) || $(element).val()) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-error')) {
                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-error');
                    $(element).closest('.form-group').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failed
        },
        messages: {
            'TireDetails.TireItem': 'Invalid Tire Type',
            'TireDetails.PLT': 'Invalid PLT',
            'TireDetails.MT': 'Invalid MT',
            'TireDetails.AGLS': 'Invalid AGLS',
            'TireDetails.IND': 'Invalid IND',
            'TireDetails.SOTR': 'Invalid SOTR',
            'TireDetails.MOTR': 'Invalid MOTR',
            'TireDetails.LOTR': 'Invalid LOTR',
            'TireDetails.GOTR': 'Invalid GOTR',
        }
    });

    /*END TIRE DETAIL VALIDATION */

    /* TERMS AND CONDITIONS */

    var validatorTermsInfo = $('#fmTermsAndConditions').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TermsAndConditions.SigningAuthorityFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityPosition': {
                required: true,
            },
            'TermsAndConditions.FormCompletedByFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByPhone': {
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                required: true,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': {
                required: true,
                regex: '^([^a-z]*)([^a-z ]{2,})([ ]{1,}[^a-z ]+)*([ ]{1,}[^a-z ]{2,})+([^a-z]+)*$',
                //regex: '^([^a-z]{0,})*[^a-z ]{2,}[ ]{1,}([^a-z]{0,})*[^a-z ]{2,}([^a-z]{0,})*$',
            },
            'TermsAndConditions.AgreementAcceptedCheck': {
                required: true
            }
        },
        highlight: function (element) {
            if ($(element).attr('data-signature')) {
                if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
                    $(element).closest('.termsandconditions-signature').removeClass('has-required');
                if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
                    $(element).closest('.termsandconditions-signature').removeClass('has-success');
                $(element).closest('.termsandconditions-signature').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
            else {
                if ($(element).closest('.form-group').hasClass('has-required'))
                    $(element).closest('.form-group').removeClass('has-required');
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if ($(element).attr('data-signature')) {
                $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function (e) {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TermsAndConditions.SigningAuthorityFirstName': 'Invalid First Name',
            'TermsAndConditions.SigningAuthorityLastName': 'Invalid Last Name',
            'TermsAndConditions.SigningAuthorityPosition': 'Invalid Position',
            'TermsAndConditions.FormCompletedByFirstName': 'Invalid First Name',
            'TermsAndConditions.FormCompletedByLastName': 'Invalid Last Name',
            'TermsAndConditions.FormCompletedByPhone': {
                required: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone'
            },
            'TermsAndConditions.AgreementAcceptedByFullName': 'Invalid Full Name',
            'TermsAndConditions.AgreementAcceptedCheck': 'Must be checked for submission'
        }
    });

    /* END TERMS AND CONDITIONS */

    /* Banking Information */
    var validatorBankingInfo = $('#frmBankingInformation').validate({
        ignore: ".ignore",
        onkeyup: function (element) {
            this.element(element);
        },

        onfocusout: function (element) {
            this.element(element);
        },
        rules: {
            'BankName': {
                //regex: '/^[\w\-\s]+$/',
                required: true,
                minlength: 2,
                maxlength: 50,
            },
            'TransitNumber': {
                //regex:'[0-9]',
                required: true,
                digits: true,
                minlength: 5,
                maxlength: 5,
            },
            'BankNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 3,
            },
            'AccountNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 15,
            },
            'Email': {
                required: ($('.isBankTransferNotficationPrimaryContact').is(':checked') ? false : true),
                email: true,
            },
            'CCEmail': {
                email: true,
            },
            'uploadFiles': {
                data: 'supportingDocumentsOption1'
            }
        },

        highlight: function (element) {
            if ($(element).val() || $(element).val() == '') {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            if ($(element).val()) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ($(element).val()) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
    });
    /*End Banking Information */
    /* COLLECTOR DETAILS VALIDATION*/

    var validatorCollectorInfo = $('#fmCollectorDetails').validate({
        ignore: '.no-validate, :hidden',

        submitHandler: function (form) {
            return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'CollectorDetails.BusinessStartDate': {
                required: true,
                lessThanOrEqualTodayDate: true
            },
            'CollectorDetails.BusinessNumber': {
                required: true,
            },
            'CollectorDetails.CommercialLiabHstNumber': {
                required: true,
                regex: '^[0-9]{9}$',
                required: function (element) {
                    return !$('#CollectorDetails_IsTaxExempt').is(':checked');
                }
            },
            'CollectorDetails.CommercialLiabInsurerName': {
                required: true,
            },
            'CollectorDetails.CommercialLiabInsurerExpDate': {
                required: true,
                greaterThanTodayDate: true,
            },
            'CollectorDetails.BusinessActivity': {
                required: true,
            },

            'CollectorDetails.WsibNumber': {
                regex: '^[0-9]{7}$',
                required: function (element) {
                    var rbtnCollectorDetailsHaulerDetailsVendorHasMoreThanOneEmp = $('#CollectorDetails_HasMoreThanOneEmp:checked');
                    return rbtnCollectorDetailsHaulerDetailsVendorHasMoreThanOneEmp.val() == 'True';
                }
            },
        },
        highlight: function (element) {
            if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                $(element).closest('.form-group').removeClass('has-required');
                $(element).closest('.form-group').removeClass('has-success');
            }
            $(element).closest('.form-group').addClass('has-error');
            if ($(element).next('span').hasClass('glyphicon-ok'))
                $(element).next('span').removeClass('glyphicon-ok');
            $(element).next('span').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
            $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        success: function (e) {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'CollectorDetails.BusinessStartDate': '', //Global.ParticipantIndex.Resources.ValidationMsgInvalidBusinessStartDate,
            'CollectorDetails.BusinessNumber': 'Invalid Business Number',
            'CollectorDetails.CommercialLiabHstNumber': 'Invalid HST Registration Number',
            'CollectorDetails.CommercialLiabInsurerName': 'Invalid Commercial Liability Insurance',
            'CollectorDetails.WsibNumber': 'Invalid WSIB Number',
            'CollectorDetails.HIsGVWR': '',
            'CollectorDetails.HasMoreThanOneEmp': '',
            'CollectorDetails.BusinessActivity': 'Invalid Activity type',
        }
    });

    /* END COLLECTOR DETAILS VALIDATION */

    //triggers validations
    var highlightFields = function () {
        var tmp = errorList.slice();
        for (var i = 0; i < tmp.length; i++) {
            if ($('input[name*=\'' + tmp[i] + '\']').length > 0 || $('select[name*=\'' + tmp[i] + '\']').length > 0) {
                var element = $('input[name*=\'' + tmp[i] + '\']');

                if (element.length == 0) {
                    element = $('select[name*=\'' + tmp[i] + '\']');
                }
                var pnl = element.closest('.panel-collapse');
                if (pnl)
                    pnl.collapse();

                if (!(element.val().length > 0) || element.is(":checkbox") || element.is(":radio")) {
                    element.valid();
                }
                else if (element.attr('type') == 'radio') {
                    element.valid();
                }
                //remove item from list
                var propertyName = $(element).attr('name');
                var index = errorList.indexOf(propertyName);
                if (index > -1)
                    errorList.splice(index, 1);
            }
        }
    }

    /* END HIGHLIGHTING OF FIELDS */
        
    $('#panelTireDetails > .panel-body > .form-group.has-feedback input[type=checkbox]').each(function (i, obj) {
        var attr = $(obj).attr('name');
        if (attr == 'TireDetails.TireItem') {
            $(obj).closest('.form-group').css('border', 'none');
            //$(obj).closest('.form-group').removeClass('has-error').addClass('has-success');
        }

        $('.termsandconditions-signature > input[type=text]').each(function (i, obj) {
            if ($(obj).val() == '') {
                $(obj).parent('div').removeClass('has-error');
            }
        });
    });
});

//OTSTM2-83
$(function () {
    StaffCommonValidations.BusinessLocation();
    StaffCommonValidations.ContactInfo();
    StaffCommonValidations.SupportingDoc();
    StaffCommonValidations.TermsAndConditions();

    //reviewCheckBox validation OTSTM2-83    
    $('#fmTireDetails').panelReviewCheckBoxValidate({
        flagId: '#flagTireDetails',
        rules: {
            'TireDetails.TireItem': {
                required: true,
                isChecked: $('#TireDetails_cbTypesHandled').is(':checked')
            },
            'TireDetails.cbUsedTiresInStorage': {
                required: true,
                isChecked: $('#TireDetails_cbUsedTiresInStorage').is(':checked')
            },
        }
    });

    //reviewCheckBox validation OTSTM2-83  
    $('#fmCollectorDetails').panelReviewCheckBoxValidate({
        flagId: '#flagCollectorDetails',
        rules: {
            'CollectorDetails.BusinessStartDate': {
                required: true,
                isChecked: $('#CollectorDetails_cbBusinessStartDate').is(':checked')
            },
            'CollectorDetails.BusinessNumber': {
                required: true,
                isChecked: $('#CollectorDetails_cbBusinessNumber').is(':checked')
            },
            'CollectorDetails.CommercialLiabHstNumber': {
                required: true,
                isChecked: $('#CollectorDetails_cbCommercialLiabHSTNumber').is(':checked')
            },

            'CollectorDetails.BusinessActivity': {
                required: true,
                isChecked: $('#CollectorDetails_cbBusinessActivity').is(':checked')
            },
            'CollectorDetails.CommercialLiabInsurerName': {
                required: true,
                isChecked: $('#CollectorDetails_cbCommercialLiabInsurerName').is(':checked')
            },
            'CollectorDetails.CommercialLiabInsurerExpDate': {
                required: true,
                isChecked: $('#CollectorDetails_cbCommercialLiabInsurerExpDate').is(':checked')
            },
            'CollectorDetails.HasMoreThanOneEmp': {
                required: true,
                isChecked: $('#CollectorDetails_cbHasMoreThanOneEmp').is(':checked')
            },
            'CollectorDetails.WsibNumber': {
                required: function () {
                    return ($('#CollectorDetails_HasMoreThanOneEmp:checked').val() == 'True' || $('#pnlWSIB').css('display') !== 'none');
                },
                isChecked: $('#CollectorDetails_cbWsibNumber').is(':checked')
            }
        }
    });


});


