﻿"use strict";

// Transaction APP
var app = angular.module('TransactionApp', ['Transaction.directives', 'Transaction.controllers', 'ui.bootstrap', 'commonLib', 'commonTransactionLib']);


// Controllers Section
var controllers = angular.module('Transaction.controllers', []);

controllers.controller('TransactionCtrl', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {
}]);


// Directives Section
var directives = angular.module('Transaction.directives', []);

directives.directive('spsSoldToFromHandler', function () {
    return {
        restrict: 'A',
        controller: function ($rootScope, $scope) {
            $scope.$watch('trans.soldTo', function () {
                if (angular.isDefined($scope.trans.soldTo)) {
                    $rootScope.$emit('set-unregistered-company-requirement', { required: $scope.trans.soldTo == 'unregistered' });
                    $rootScope.$emit('set-vendor-information-requirement', { required: $scope.trans.soldTo == 'registered' });
                }
            }, true);
        }
    };
});

directives.directive('spsProductDetailCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            productDetail: "=",
            productList: "=",
            scaleTicket: "=",
            adjust: "@",
            mobile: "@"
        },
        templateUrl: 'sps-product-detail.html',
        controller: function ($scope) {
            $scope.currentProduct = null;
            $scope.adjustmentInitialized = false;

            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.isMobile = function () {
                return (angular.isDefined($scope.mobile) && $scope.mobile == "true");
            };

            $scope.$watch('productDetail', function () {
                if (angular.isDefined($scope.productDetail) && $scope.isAdjustment() && !$scope.adjustmentInitialized) {
                    var product = _.find($scope.productList, function (i) { return i.productId == $scope.productDetail.productTypeId; });
                    if (!_.isUndefined(product)) $scope.currentProduct = product;
                    $scope.adjustmentInitialized = true;
                }
            }, true);
            
            $scope.productChange = function () {
                if ($scope.currentProduct == null) {
                    $scope.productDetail.productTypeId = null;                    
                    $scope.productDetail.productRate = null;
                }
                else {
                    $scope.productDetail.productTypeId = $scope.currentProduct.productId;                    
                    $scope.productDetail.productRate = $scope.currentProduct.rate;
                }

                if (!$scope.showMeshRange()) {
                    $scope.productDetail.meshRangeStart = null;
                    $scope.productDetail.meshRangeEnd = null;
                }

                if (!$scope.showIsFreeOfSteel()) {
                    $scope.productDetail.isFreeOfSteel = null;
                }

                if (!$scope.showProductDescription()) {
                    $scope.productDetail.productDescription = null;
                }
            };

            $scope.getScaleTicketWeight = function () {
                var weight = 0.0000;
                if ($scope.isMobile()) {
                    _.each($scope.scaleTicket.tickets, function (i) {
                        weight = weight + (((_.isNull(i.inboundWeight) ? 0.0000 : i.inboundWeight) - (_.isNull(i.outboundWeight) ? 0.0000 : i.outboundWeight)));
                    });
                } else {
                    weight = _.isNull($scope.scaleTicket.weight) ? 0.0000 : $scope.scaleTicket.weight;
                }

                return weight;
            };

            $scope.showAmount = function () {
                return ($scope.productDetail.productRate != null && ($scope.getScaleTicketWeight() * 1) > 0.00);
            };

            $scope.getAmount = function () {
                if ($scope.scaleTicket.weightType == 1) return $scope.productDetail.productRate * GlobalSettingService.ConvertLbToTon($scope.getScaleTicketWeight()).toFixed(4);
                if ($scope.scaleTicket.weightType == 2) return $scope.productDetail.productRate * GlobalSettingService.ConvertKgToTon($scope.getScaleTicketWeight()).toFixed(4);
                if ($scope.scaleTicket.weightType == 3) return ($scope.productDetail.productRate * $scope.getScaleTicketWeight());
            };

            $scope.showMeshRange = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP1' || $scope.currentProduct.productCode == 'TDP2' || $scope.currentProduct.productCode == 'TDP3'));
            };

            $scope.showIsFreeOfSteel = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP1' || $scope.currentProduct.productCode == 'TDP2'));
            };

            $scope.showProductDescription = function () {
                return ($scope.currentProduct != null && ($scope.currentProduct.productCode == 'TDP4'));
            };
        }
    };
}]);

directives.directive('pitProductDetailCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            productDetail: "=",
            productList: "=",
            scaleTicket: "=",
            isInbound: "=",
            adjust: "@",
            mobile: "@"
        },
        templateUrl: 'pit-product-detail.html',
        controller: function ($scope) {
            $scope.currentProduct = null;
            $scope.adjustmentInitialized = false;

            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.isMobile = function () {
                return (angular.isDefined($scope.mobile) && $scope.mobile == "true");
            };

            $scope.$watch('productDetail', function () {
                if (angular.isDefined($scope.productDetail) && $scope.isAdjustment() && !$scope.adjustmentInitialized) {                    
                    var product = _.find($scope.productList, function (i) { return i.productId == $scope.productDetail.productTypeId; });
                    if (!_.isUndefined(product)) $scope.currentProduct = product;
                    $scope.adjustmentInitialized = true;
                } 
            }, true);

            $scope.productChange = function () {
                if ($scope.currentProduct == null) {
                    $scope.productDetail.productTypeId = null;
                    $scope.productDetail.productRate = null;
                }
                else {
                    $scope.productDetail.productTypeId = $scope.currentProduct.productId;
                    $scope.productDetail.productRate = $scope.currentProduct.rate;
                }                
            };

            $scope.getScaleTicketWeight = function () {
                var weight = 0.0000;
                if ($scope.isMobile()) {
                    _.each($scope.scaleTicket.tickets, function (i) {
                        weight = weight + (((_.isNull(i.inboundWeight) ? 0.0000 : i.inboundWeight) - (_.isNull(i.outboundWeight) ? 0.0000 : i.outboundWeight)));
                    });
                } else {
                    weight = _.isNull($scope.scaleTicket.weight) ? 0.0000 : $scope.scaleTicket.weight;
                }
                
                return weight;
            };

            $scope.showAmount = function () {
                return ($scope.productDetail.productRate != null && ($scope.getScaleTicketWeight() * 1) > 0.00);
            };

            $scope.getAmount = function () {
                if ($scope.scaleTicket.weightType == 1) return $scope.productDetail.productRate * GlobalSettingService.ConvertLbToTon($scope.getScaleTicketWeight());
                if ($scope.scaleTicket.weightType == 2) return $scope.productDetail.productRate * GlobalSettingService.ConvertKgToTon($scope.getScaleTicketWeight());
                if ($scope.scaleTicket.weightType == 3) return ($scope.productDetail.productRate * $scope.getScaleTicketWeight());
            };
        }
    };
}]);

directives.directive('dorSoldToFromHandler', function () {
    return {
        restrict: 'A',
        controller: function ($rootScope, $scope) {
            $scope.$watch('trans.soldTo', function () {
                if (angular.isDefined($scope.trans.soldTo)) {
                    $rootScope.$emit('set-unregistered-company-requirement', { required: $scope.trans.soldTo == 'unregistered' });
                    $rootScope.$emit('set-vendor-information-requirement', { required: $scope.trans.soldTo == 'registered' });
                }
            }, true);
        }
    };
});

directives.directive('dorProductDetailCollector', ['GlobalSettingService', function (GlobalSettingService) {
    return {
        restrict: 'E',
        scope: {
            form: "=",
            productDetail: "=",
            weightTypeList: "=",
            scaleTicket: "=",
            tireTypeList: "=",
            adjust: "@",
            vm: "="
        },
        templateUrl: 'dor-product-detail.html',
        controller: function ($rootScope, $scope, $http) {
            $scope.isAdjustment = function () {
                return (angular.isDefined($scope.adjust) && $scope.adjust == "true");
            };

            $scope.tireRimsRequired = function () {
                return $scope.productDetail.materialTypeId == 4;
            };

            $scope.usedTireSaleRequired = function () {
                return $scope.productDetail.materialTypeId == 5;
            };

            $scope.weighTypeAvailable = function (type) {
                return _.contains($scope.weightTypeList, type);
            };

            $scope.weighTypeStyle = function (id, selectionId) {
                if (id == selectionId) return "btn btn-xs btn-success";
                else return "btn btn-xs btn-default";
            };

            $scope.weighTypeLabel = function (selectionId) {
                if (selectionId == 1) return "LBS";
                else if (selectionId == 2) return "KG";
                else if (selectionId == 3) return "TON";
            };

            $scope.$watch('productDetail.materialTypeId', function () {
                if (angular.isDefined($scope.productDetail) && angular.isDefined($scope.productDetail.materialTypeId)) {
                    //if ($scope.isAdjustment()) {
                    //} else {
                        if ($scope.productDetail.materialTypeId != 4) {
                            $scope.productDetail.onRoadScaleWeight.weightType = 3;
                            $scope.productDetail.onRoadScaleWeight.weight = null;
                            $scope.productDetail.offRoadScaleWeight.weightType = 3;
                            $scope.productDetail.offRoadScaleWeight.weight = null;

                            $scope.productDetail.ptrNumber = null;
                            $scope.form.ptrNumber.$setValidity("validFormat", true);
                            $scope.form.ptrNumber.$setValidity("notFound", true);

                            $scope.form.onRoadScaleWeight.$setValidity("required", true);
                            $scope.form.onRoadScaleWeight.$setValidity("invalidWeight", true);
                            $scope.form.offRoadScaleWeight.$setValidity("required", true);
                            $scope.form.offRoadScaleWeight.$setValidity("invalidWeight", true);
                        }

                        if ($scope.productDetail.materialTypeId == 4) {
                            $scope.scaleTicket.ticketNumber = null;
                            $scope.scaleTicket.weight = null;
                        }

                        if ($scope.productDetail.materialTypeId == 5) {
                            _.each($scope.tireTypeList, function (itm) { itm.count = null; });
                        } else {
                            _.each($scope.tireTypeList, function (itm) { itm.count = 1; });
                        }
                    //}                                        
                }
            }, true);            

            $scope.getTireRimsAmount = function () {
                var onRoadWeight = $scope.productDetail.onRoadScaleWeight.weight;
                if ($scope.productDetail.onRoadScaleWeight.weightType == 1) onRoadWeight = GlobalSettingService.ConvertLbToTon($scope.productDetail.onRoadScaleWeight.weight);
                if ($scope.productDetail.onRoadScaleWeight.weightType == 2) onRoadWeight = GlobalSettingService.ConvertKgToTon($scope.productDetail.onRoadScaleWeight.weight);

                var offRoadWeight = $scope.productDetail.offRoadScaleWeight.weight;
                if ($scope.productDetail.offRoadScaleWeight.weightType == 1) offRoadWeight = GlobalSettingService.ConvertLbToTon($scope.productDetail.offRoadScaleWeight.weight);
                if ($scope.productDetail.offRoadScaleWeight.weightType == 2) offRoadWeight = GlobalSettingService.ConvertKgToTon($scope.productDetail.offRoadScaleWeight.weight);

                return (onRoadWeight * $scope.productDetail.onRoadRate) + (offRoadWeight * $scope.productDetail.offRoadRate);
            };

            $scope.getOnRoadScaleWeightInKg = function () {
                if ($scope.productDetail.onRoadScaleWeight.weight == null) return null;

                var onRoadWeight = $scope.productDetail.onRoadScaleWeight.weight;
                if ($scope.productDetail.onRoadScaleWeight.weightType == 1) onRoadWeight = GlobalSettingService.ConvertLbToKg($scope.productDetail.onRoadScaleWeight.weight);
                if ($scope.productDetail.onRoadScaleWeight.weightType == 3) onRoadWeight = GlobalSettingService.ConvertTonToKg($scope.productDetail.onRoadScaleWeight.weight);

                return onRoadWeight;
            }

            $scope.getOffRoadScaleWeightInKg = function () {
                if ($scope.productDetail.offRoadScaleWeight.weight == null) return null;

                var offRoadWeight = $scope.productDetail.offRoadScaleWeight.weight;
                if ($scope.productDetail.offRoadScaleWeight.weightType == 1) offRoadWeight = GlobalSettingService.ConvertLbToKg($scope.productDetail.offRoadScaleWeight.weight);
                if ($scope.productDetail.offRoadScaleWeight.weightType == 3) offRoadWeight = GlobalSettingService.ConvertTonToKg($scope.productDetail.offRoadScaleWeight.weight);

                return offRoadWeight;
            }

            $scope.isPTRNumberValid = function () {
                return $scope.form.ptrNumber.$valid;
            };

            $scope.resetPTRNumberValidation = function () {
                $scope.productDetail.ptrWeightDetail = null;

                if ($scope.form.ptrNumber != null) {
                    $scope.form.ptrNumber.$setValidity("notFound", true);
                }                

                $scope.$broadcast('show-errors-event');                
            };

            $scope.validatePTRNumber = function () {
                if ($scope.isPTRNumberValid()) {                    
                    $http({
                        url: '/System/Transactions/validatePTRNumber',
                        method: "POST",
                        data: { ptrNumber: $scope.productDetail.ptrNumber.trim() }
                    }).success(function (result) {
                        if (result.data.isFound) {
                            $scope.form.ptrNumber.$setValidity("notFound", true);
                            $scope.productDetail.ptrWeightDetail = result.data.detail;                            
                        } else {                            
                            $scope.form.ptrNumber.$setValidity("notFound", false);
                            $scope.productDetail.ptrWeightDetail = null;                            
                        }

                        $scope.$broadcast('show-errors-event');
                    }).error(function () {
                    });
                }                 
            };

            $scope.$watch('productDetail', function () {
                if ($scope.tireRimsRequired()) {
                    if ($scope.isPTRNumberValid() && $scope.productDetail.ptrWeightDetail != null) {                        
                        if ($scope.getOnRoadScaleWeightInKg() != null) {
                            $scope.form.onRoadScaleWeight.$setValidity("invalidWeight", $scope.getOnRoadScaleWeightInKg() <= Math.round($scope.productDetail.ptrWeightDetail.onRoadWeight * 10) / 10);//to fix accuracy losing during KG TON convert
                        } else $scope.form.onRoadScaleWeight.$setValidity("invalidWeight", true);

                        if ($scope.getOffRoadScaleWeightInKg() != null) {
                            $scope.form.offRoadScaleWeight.$setValidity("invalidWeight", $scope.getOffRoadScaleWeightInKg() <= Math.round($scope.productDetail.ptrWeightDetail.offRoadWeight * 10) / 10);
                        } else $scope.form.offRoadScaleWeight.$setValidity("invalidWeight", true);

                        $scope.$broadcast('show-errors-event');
                    }
                }
            }, true);
        }
    };
}]);