﻿'use strict';
processorClaimApp.controller("processorStaffClaimSummaryController", ["$scope", "$filter", "processorClaimService", '$uibModal', 'claimService', function ($scope, $filter, processorClaimService, $uibModal, claimService) {
    $scope.dataloaded = false;
    $scope.taxDisabled = false;
    processorClaimService.getClaimDetails().then(function (data) {
        $scope.claimPeriod = data.ClaimCommonModel.ClaimPeriod;
        $scope.vendorId = data.ClaimCommonModel.VendorId;
        //populating claim details
        $scope.ProcessorClaimSummary = data;

        $scope.alert = function (arg) {
            alert(arg);
        }

        $scope.rateTransactionID = data.AssociatedRateTransactionId;

        //open specific rate modal
        $scope.openRateDetailsModal = function () {
            $scope.rateTransactionID = data.AssociatedRateTransactionId;
            var rateDetailModal = $uibModal.open({
                templateUrl: 'modelSpecificRate.html',
                controller: 'loadRateCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    rateTransactionID: function () { return $scope.rateTransactionID; }
                }
            });
        }
        
        $scope.HasSpecificRate = data.HasSpecificRate;

        $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;

        $scope.ClaimCommonModel = data.ClaimCommonModel;

        if ((data.ClaimStatus.StatusString == "Approved") || (data.ClaimStatus.StatusString == "Receive Payment")) {
            $scope.ShowMailDate = true;
        }
        else {
            $scope.ShowMailDate = false;
        }
        $scope.taxDisabled = Global.Settings.Permission.IsPaymentReadOnly == "True" || $scope.ShowMailDate;
        $scope.$watch("ProcessorClaimSummary.ProcessorPayment.MailedDate", function () {
            if ($scope.ProcessorClaimSummary.ProcessorPayment.MailedDate != null) {
                claimService.updateClaimMailDate($scope.ProcessorClaimSummary.ProcessorPayment.MailedDate);
            }
        });
        $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;

        if (data.ClaimStatus.StatusString == "Approved") {
            $scope.DisableInternalAdjustmentsBtn = true;
        }
        else {
            $scope.DisableInternalAdjustmentsBtn = Global.Settings.Permission.DisableInternalAdjustmentsBtn;
        }

        $scope.dataloaded = true;
        $scope.getStatusColor = setStatusColor(data.ClaimStatus.StatusString);
    });

    var refreshPanels = function () {
        //call claim service to refresh panel
        processorClaimService.getClaimDetails().then(function (data) {
            //populating claim details
            $scope.ProcessorClaimSummary = data;
            $scope.ClaimCommonModel = data.ClaimCommonModel;
            //refresh internal adjustment datatable
            $('#internalAdjustSearchBtn').click();

           // console.log('$scope.ProcessorClaimSummary.ProcessorPayment.GroundTotal', $scope.ProcessorClaimSummary.ProcessorPayment.GroundTotal);
            claimService.getClaimWorkflowViewModel($scope.ProcessorClaimSummary.ProcessorPayment.GroundTotal).then(function (data) {//update workflow UI
                var localScope = angular.element(document.getElementById("claimWorkflowBar")).scope();
                if (localScope) {//if work flow bar exists, update it base on appsetting/$value
                    localScope.claimWorkflowModel = data;//typeof data: ClaimWorkflowViewModel
                }
            });

        });
    }
    var initializeModalResult = function () {
        return {
            selectedItem: null,
            direction: 'overall',
            eligibility: 'eligible',
            onroad: 0,
            offroad: 0,
            plt: 0,
            mt: 0,
            agls: 0,
            ind: 0,
            sotr: 0,
            motr: 0,
            lotr: 0,
            gotr: 0,
            paymentType: 'Overall',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,
            unitType: 3,
            //OTSTM2-648
            internalNote: null
        };
    }
    $scope.InternalAdjustTypes = [
       {
           name: 'Weight',
           id: '1'
       },
       {
           name: 'Tire Counts',
           id: '2'
       },
       {
           name: 'Payment',
           id: '4'
       }
    ];

    //Internal adjustment modal
    $scope.processorInternalAdjustments = function () {
        var modalResult = initializeModalResult();
        var internalAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'EligibleAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.InternalAdjustTypes;
                },
                modalResult: function () {
                    return modalResult;
                },
                selectedItem: function () {
                    return $scope.InternalAdjustTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        internalAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var internalAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'EligibleAdjustmentConfirmCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            internalAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {
                        refreshPanels();
                    }
                });
            });
        });
    }

    //Edit internal adjustment
    $scope.editInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return false;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {
                if (result.isCancel) {
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

    //view internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    refreshPanels();
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

    //Remove internal adjustment
    $scope.removeInternalAdjust = function (data) {
        var internalAdjustmentId = data.InternalAdjustmentId;
        var internalAdjustmentType = data.InternalAdjustmentType;
        claimService.removeInventoryAdjustment(internalAdjustmentType, internalAdjustmentId).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    }

    //Mail Date Picker
    $scope.datePicker = {
        status: false
    };

    $scope.openDatePicker = function ($event) {
        $scope.datePicker.status = true;
    };

    var category = 1;
    $scope.loadTopInfoBar = function () {
        var isSpecific = true;
        var addNewRateModal = $uibModal.open({
            templateUrl: "processor-modal-load-top-info-bar.html",
            controller: 'loadTopInfoCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                category: function () {
                    return $scope.category;
                },
                vendorId: function () {
                    return $scope.vendorId;
                },
                claimPeriod: function () {
                    return $scope.claimPeriod;
                }
            }
        });
    }

    //InternalNotesSettings for ClaimInternalNotes
    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalNotesUrl;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalNotesUrl;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalNotes;
    var setStatusColor = function (status) {
        switch (status) {
            case "Submitted":
                return 'color-tm-orange-bg';                
            case "Open":
                return 'color-tm-blue-bg';                
            case "Under Review":
                return 'color-tm-yellow-bg';                
            case "Approved":
                return 'color-tm-green-bg';                
            default:
                return '';
        }
    }

    $scope.taxApplicable = function () {
        $scope.ProcessorClaimSummary.ProcessorPayment.IsTaxApplicable = !$scope.ProcessorClaimSummary.ProcessorPayment.IsTaxApplicable;
        claimService.UpdateHST($scope.ProcessorClaimSummary.ProcessorPayment.HST, $scope.ProcessorClaimSummary.ProcessorPayment.IsTaxApplicable).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    }
   
}]);

//View Rate Controller
processorClaimApp.controller('loadRateCtrl', ['$scope', '$uibModalInstance', 'rateTransactionID', 'processorClaimService', function ($scope, $uibModalInstance, rateTransactionID, processorClaimService) {

    $scope.isView = true;
    $scope.loadUrl = Global.ParticipantClaimsSummary.LoadNotesListUrl;
    $scope.exportUrl = Global.ParticipantClaimsSummary.InternalNotesExportUrl;
    $scope.rateTransactionID = rateTransactionID;
    $scope.disableNoteBtn = true;

    processorClaimService.loadPIRatesByTransactionID(rateTransactionID).then(function (data) {
        $scope.rateVM = rateViewModel(data.data);
        $scope.rateVM.startDate = $scope.rateVM.effectiveStartDate.substring(0, 7);
        $scope.rateVM.endDate = $scope.rateVM.effectiveEndDate.substring(0, 7);
        $scope.allInternalNotes = $scope.rateVM.notes;
        $scope.viewMore = ($scope.rateVM.notes.length > 5 && !$scope.scroller);
        $('#rate-viewdetail-notes > div > div.form-group > textarea').prop("disabled", $scope.disableNoteBtn);
        $('#rate-viewdetail-notes > div > button').prop("disabled", $scope.disableNoteBtn);
    });

    $scope.sortingOrder = false;
    $scope.noteSorting = function (e) {
        $scope.sortingOrder = !$scope.sortingOrder;
        $(e.target).removeClass('sorting');
        $(e.target).toggleClass('sorting_asc', $scope.sortingOrder);
        $(e.target).toggleClass('sorting_desc', !$scope.sortingOrder);
    };

    function rateViewModel(data) {
        var result = {};
        var prefex = '$';
        if (typeof (data) === 'object') {//session time out handling
            $.each(data, function (name, value) {
                if (value instanceof Object && name != "notes") {
                    var copy = {};
                    $.each(value, function (itemname, itemvalue) {
                        if (typeof itemvalue !== 'object') {
                            if (typeof itemvalue === 'number') {
                                copy[itemname] = prefex + itemvalue.toFixed(data.decimalsize).toString();
                            } else if (typeof itemvalue === 'string') {
                                copy[itemname] = itemvalue;
                            }
                        }
                    })
                    result[name] = copy;
                } else {
                    result[name] = value;
                }
            });
            return result;
        } else {
            console.log('session time out?');
            return false;//return Boolean instead of object[null]
        }
    };

    $scope.cancel = function () {
        if (typeof ($scope.rateVM) === 'object') {//session time out handling

        }
        $uibModalInstance.dismiss('cancel');
    };
    //view more
    $scope.viewMoreNotesClick = function ($event) {
        $scope.quantity = $scope.allInternalNotes.length;
        $scope.viewMore = false;
        $scope.scroller = true;
        $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
    }
}]);

processorClaimApp.controller('EligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, disableForReadOnlyStaff, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; //OTSTM2-155
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";
    $scope.confirm = function (isValid) {
        //OTSTM2-648
        if (isValid === false || !isValid) return;

        $scope.modalResult.selectedItem = $scope.selectedItem;

        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: false });
    }

    $scope.cancel = function () {
        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: true });
    };

    /*
    ---------------------------------------------------------------------------------
    ------------------validations start here OTSTM2-270------------------------------
    */
    //OTSTM2-648 updating loadUrl and addUrl for InternalNotes for 'internalAdjustModal.html'
    //id = 4 is the InternalAdjustTypes for Payment
    if ($scope.isReadOnly) {
        if (selectedItem.id === '4') {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
        }
        else {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
        }
    }

    //OTSTM2-648 get current choosetype
    $scope.returnCurrentChooseType = function (item) {

        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });
        return curr[0].isValid();
    }

    //OTSTM2-648 param1 = item is current choose item & param1 = rule is the type from error rules for each validationtypes
    $scope.getErrorRule = function (item, rule) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });

        if (curr[0].errorRules.hasOwnProperty(rule)) {
            return curr[0].errorRules[rule]($scope.internaladjustForm);
        }
    }

    //OTSTM2-648
    $scope.isTireTypeAllZeros = function () {
        return validation.checkAllTireItemsZero($scope.modalResult)
    }

    //OTSTM2-270
    $scope.allZeroesOrEmpty = function () {
        var args = [].slice.call(arguments);

        return validation.checkAllFieldsZero(args)
    }

    var validation = {
        init: function () {
            var self = this;

            this.validationTypes = [
                   {
                       name: 'Weight',
                       id: '1',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.onroad])
                       },
                       errorRules: {
                           _onRoad: function (form) {
                               return (form.onroad.$error.isDecimalRequired && !form.onroad.$pristine)
                                   || (form.onroad.$error.number && !form.onroad.$pristine)
                                   || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.onroad]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Tire Counts',
                       id: '2',
                       isValid: function () {
                           return self.isValid() && !self.checkAllTireItemsZero($scope.modalResult);
                       },
                       errorRules: {
                           _plt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_plt.$error.number && !form.tirecount_plt.$pristine))
                           },
                           _mt: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_mt.$error.number && !form.tirecount_mt.$pristine))
                           },
                           _agls: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_agls.$error.number && !form.tirecount_agls.$pristine))
                           },
                           _ind: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_ind.$error.number && !form.tirecount_ind.$pristine))
                           },
                           _sotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_sotr.$error.number && !form.tirecount_sotr.$pristine))
                           },
                           _motr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_motr.$error.number && !form.tirecount_motr.$pristine))
                           },
                           _lotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_lotr.$error.number && !form.tirecount_lotr.$pristine))
                           },
                           _gotr: function (form) {
                               return (form.$submitted && self.checkAllTireItemsZero($scope.modalResult) || (form.tirecount_gotr.$error.number && !form.tirecount_gotr.$pristine))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Payment',
                       id: '4',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.amount])
                       },
                       errorRules: {
                           _amount: function (form) {
                               return (form.amount.$error.isDecimalRequired && !form.amount.$pristine) || (form.amount.$error.number && !form.amount.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.amount]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   }
            ];

            $scope.$watch('selectedItem', function () {
                if ($scope.internaladjustForm) {
                    $scope.internaladjustForm.$setPristine();
                    $scope.internaladjustForm.$setUntouched();
                }
            });
        },
        isValid: function () {
            return ($scope.internaladjustForm.$valid);
        },
        checkAllTireItemsZero: function (item) {
            var tireObj = {
                plt: item.plt,
                agls: item.agls,
                gotr: item.gotr,
                ind: item.ind,
                lotr: item.lotr,
                motr: item.motr,
                mt: item.mt,
                sotr: item.sotr
            };

            return _.values(tireObj).every(function (val) {
                return (val === 0 || val === null);
            });
        },
        //para accepts only an array
        checkAllFieldsZero: function (arr) {
            if (arr instanceof Array) {
                return arr.every(function (val) {
                    return (val === 0 || val === null);
                });
            }
        },
        getAllValidationTypes: function () {
            return this.validationTypes;
        },
        empty: function (item) {
            return item.plt == null || item.agls == null || item.gotr == null || item.ind == null || item.lotr == null
                || item.motr == null || item.mt == null || item.sotr == null;
        }
    }

    validation.init();
    /*
    ------------------validations end here------------------------------
    --------------------------------------------------------------------
    */
}]);

processorClaimApp.controller('EligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


processorClaimApp.controller('loadTopInfoCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'vendorId', 'claimPeriod', 'processorClaimService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, category, vendorId, claimPeriod, processorClaimService, $rootScope) {
    var params = {
        vendorId: vendorId,
        claimPeriodStart: claimPeriod.StartDate,
        claimPeriodEnd: claimPeriod.EndDate,
        rateGroupCategory: 'DeliveryGroup',
    };

    processorClaimService.getProcessorTopInfoPickupRateGroupDetail(params).then(function (result) {
        $scope.vm = result;
        console.log($scope.vm.data);
        $scope.vm.isView = true;
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };
}]);
