﻿//ApplicationProcessor jS

$(function () {
    CommonValidation.initializeStandardFormValidations();

    var errorList = []; /* list of invalid Form Fields */
    /** Diable all the checkbox after approve */
    if ($('#status').val() == 'Approved' || $('#status').val() == 'BankInformationSubmitted' || $('#status').val() == 'BankInformationApproved') {
        $('#AppProcessorFormID input[type=checkbox]').not("[name='TireDetails.TireItem'],[name='TireDetails.TireItemProduct'],[class='isMailingAddressDifferent'],[id='ContactAddressSameAsBusinessAddress']").attr('disabled', 'true');
        $('#ProcessorDetails_IsTaxExempt').removeAttr('disabled');
    }

    var cbMailingAddressSameAsBusiness = $('#MailingAddressSameAsBusiness');
    var cbContactAddressSameAsBusinessAddress = $('#ContactAddressSameAsBusinessAddress');
    var cbProcessorDetailsProcessorDetailsVendorIsTaxExempt = $('#ProcessorDetails_CommercialLiabHstNumber');

    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];
    var doHighlight = true;

    /* START GLOBAL VALIDATION METHODS */

    var stripHtml = function (html) {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    };

    var dynamicErrorMsg = function (params, element) {

        var labelName = 'Field';
        
        //OTSTM2-643 add logic to generate capacity validation message
        if ($(element).prev('label').is("label")) {          
            labelName = stripHtml($(element).prev('label').html());
        }
        else if ($(element).prev().prev('label').is("label")) { 
            labelName = stripHtml($(element).prev().prev('label').html());
        }
        else if ($(element).parent('div.dropdown').prevAll('label')) {
            labelName = stripHtml($(element).parent('div.dropdown').prevAll('label').html());
        }
        return 'Invalid ' + labelName;
    };

    //removes the green/red highlights from element
    var resetHighlight = function (element, isRequired) {
        $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
        $(element).next('span').removeClass('glyphicon-remove');
        $(element).nextAll('.help-block:first').remove();
        if (isRequired) {
            $(element).closest('.form-group').addClass('has-required');
            $(element).next('span').addClass('glyphicon-ok');
        }
    }

    $.validator.addMethod('lessThanOrEqualTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam <= today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('greaterThanTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam > today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-required", function (value, element) {
        if (value)
            return true;
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-regex-phone", function (value, element) {
        var regex = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$|^$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, 'Expected format ###-###-####');

    $.validator.addMethod("dynamic-regex-postal", function (value, element) {
        var regex = /^[A-Za-z][0-9][A-Za-z]([ ]?)[0-9][A-Za-z][0-9]|([0-9]{5})(?:[- ][0-9]{4})?$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-regex-email", function (value, element) {
        var regex = /^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("TireDetailsRequired", function (value, element) {
        var radioStr = $(element).val();
        if (typeof radioStr == 'undefined') {
            return false;
        }
        var radioBool = JSON.parse(radioStr.toLowerCase());
        if (radioBool && $("select[name='TireDetails.HRelatedProcessor'] option:selected").index() == "0") {
            return false;
        }
        //value is no; thus return true
        return !radioBool;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("HRelatedProcessorRequired", function (value, element) {
        if (typeof $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'undefined') {
            return false;
        }
        else if ($("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'True' && $('#HRelatedProcessor')[0].selectedIndex == 0) {
            return false;
        }

        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    /* END GLOBAL VALIDATION METHODS */

    /* START BUSINESS INFORMATION VALIDATION */

    var validatorBusinessInfo = $('#fmBusinessLocation').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'BusinessLocation.BusinessName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.OperatingName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.BusinessAddress.AddressLine1': {
                required: true
            },
            'BusinessLocation.BusinessAddress.City': {
                required: true
            },
            'BusinessLocation.BusinessAddress.Province': {
                required: true
            },
            'BusinessLocation.BusinessAddress.AddressLine2': {
                required: false
                //inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Postal': {
                required: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$'
            },
            'BusinessLocation.BusinessAddress.Country': {
                required: true
            },
            'BusinessLocation.Phone': {
                required: true,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
            },
            'BusinessLocation.Extension': {
                required: false
            },
            //'BusinessLocation.BusinessLocationAddress.Ext': {
            //    required: false,
            //    //inInvalidList: true
            //},
            'BusinessLocation.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.AddressLine1': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.City': {
                required: function (element) {
                    return !cbMailingAddressSameAsBusiness.is(':checked');
                }
            },
            'BusinessLocation.MailingAddress.Province': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.AddressLine2': {
                required: false
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.Postal': {
                //inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.Country': {
                // inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'BusinessLocation.BusinessName': Global.StaffIndex.Resources.ValidationMsgInvalidBusinessName,
            'BusinessLocation.OperatingName': Global.StaffIndex.Resources.ValidationMsgInvalidOperatingName,
            'BusinessLocation.BusinessAddress.AddressLine1': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine1,
            'BusinessLocation.BusinessAddress.City': Global.StaffIndex.Resources.ValidationMsgInvalidCity,
            'BusinessLocation.BusinessAddress.Province': Global.StaffIndex.Resources.ValidationMsgInvalidProvinceState,
            'BusinessLocation.BusinessAddress.AddressLine2': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine2,
            'BusinessLocation.BusinessAddress.Postal': Global.StaffIndex.Resources.ValidationMsgInvalidPostalCode,
            'BusinessLocation.BusinessAddress.Country': Global.StaffIndex.Resources.ValidationMsgInvalidCountry,
            'BusinessLocation.Phone': {
                required: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.StaffIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
            },
            'BusinessLocation.Extension': Global.StaffIndex.Resources.ValidationMsgInvalidExt,
            'BusinessLocation.Email': Global.StaffIndex.Resources.ValidationMsgInvalidEmail,
            'BusinessLocation.MailingAddress.AddressLine1': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine1,
            'BusinessLocation.MailingAddress.City': Global.StaffIndex.Resources.ValidationMsgInvalidCity,
            'BusinessLocation.MailingAddress.Province': Global.StaffIndex.Resources.ValidationMsgInvalidProvinceState,
            'BusinessLocation.MailingAddress.AddressLine2': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine2,
            'BusinessLocation.MailingAddress.Postal': Global.StaffIndex.Resources.ValidationMsgInvalidPostalCode,
            'BusinessLocation.MailingAddress.Country': Global.StaffIndex.Resources.ValidationMsgInvalidCountry,
        }
    });

    var validateBusinessInfo = function (highlight) {
        //disabling green checkbox for now
        //disabling green checkbox for now
        //disabling green checkbox for now
        //disabling green checkbox for now
        //doHighlight = highlight ? highlight : false;
        //var isValid = panel.valid();
        //(isValid) ? panel.find('.status-flag').attr('src', statuses[1]) : panel.find('.status-flag').attr('src', statuses[0]);
        //doHighlight = true;
    };

    $('body').on('click', function (e) {
        //validateBusinessInfo(false);
    });

    var initBusinessInfo = function () {
        //validateBusinessInfo(false);
    };

    initBusinessInfo();

    /* END BUSINESS INFORMATION VALIDATION */

    /* CONTACT INFORMATION VALIDATION */

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            '': {
                required: true,
                regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'ContactInformation[0].ContactInformationContact.Name': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.Position': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Ext': {
                required: false
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$'
            },
            'ContactInformation[0].ContactInformationAddress.Address1': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': Global.StaffIndex.Resources.ValidationMsgInvalidPrimaryContactName,
            'ContactInformation[0].ContactInformationContact.Position': Global.StaffIndex.Resources.ValidationMsgInvalidPosition,
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.StaffIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
                required: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Ext': Global.StaffIndex.Resources.ValidationMsgInvalidExt,
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: Global.StaffIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Email': Global.StaffIndex.Resources.ValidationMsgInvalidEmail,
            'ContactInformation[0].ContactInformationAddress.Address1': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine1,
            'ContactInformation[0].ContactInformationAddress.Address2': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine2,
            'ContactInformation[0].ContactInformationAddress.City': Global.StaffIndex.Resources.ValidationMsgInvalidCity,
            'ContactInformation[0].ContactInformationAddress.PostalCode': Global.StaffIndex.Resources.ValidationMsgInvalidPostalCode,
            'ContactInformation[0].ContactInformationAddress.Province': Global.StaffIndex.Resources.ValidationMsgInvalidProvinceState,
            'ContactInformation[0].ContactInformationAddress.Country': Global.StaffIndex.Resources.ValidationMsgInvalidCountry,
        }
    });

    /* END CONTACT INFORMATION VALIDATION */

    /* SORT YARD DETAILS */

    var validatorSortYardInfo = $('#fmSortYardDetails').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            //return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.City': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Province': {
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Address2': {
                required: false
            },
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsAddress.Country': {
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': {
                regex: '^[0-9]{1,}$',
                required: true
            }
            ////'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': {
            ////    inInvalidList: true,
            ////    required: function (element) {
            ////        var txtMaxStorageCap = $('#SortYardDetails_0__SortYardDetailsVendorStorageSiteModel_MaxStorageCapacity');
            ////        var val = parseFloat(txtMaxStorageCap.val());
            ////        if (val)
            ////            return val >= 50;
            ////        else
            ////            return false;
            ////    }
            ////}
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine1,
            'SortYardDetails[0].SortYardDetailsAddress.City': Global.StaffIndex.Resources.ValidationMsgInvalidCity,
            'SortYardDetails[0].SortYardDetailsAddress.Province': Global.StaffIndex.Resources.ValidationMsgInvalidProvinceState,
            'SortYardDetails[0].SortYardDetailsAddress.Address2': Global.StaffIndex.Resources.ValidationMsgInvalidAddressLine2,
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': Global.StaffIndex.Resources.ValidationMsgInvalidPostalCode,
            'SortYardDetails[0].SortYardDetailsAddress.Country': Global.StaffIndex.Resources.ValidationMsgInvalidCountry,
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': Global.StaffIndex.Resources.ValidationMsgInvalidCapacity
            ////'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': Global.StaffIndex.Resources.ValidationMsgInvalidCertificateOfApproval,
        }
    });

    /* END SORT YARD DETAILS */

    /* Processor DETAILS */

    var validatorProcessorDetailsInfo = $('#fmProcessorDetails').validate({
        ignore: '.no-validate, :hidden',

        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                if (!$(element).next('span').hasClass('input-group-addon'))
                    $(element).next('span').addClass('glyphicon-remove');
                else {
                    if ($(element).parent('.input-group').next('span').hasClass('glyphicon-ok')) {
                        $(element).parent('.input-group').next('span').removeClass('glyphicon-ok');
                        $(element).parent('.input-group').next('span').addClass('glyphicon-remove');
                    }
                }
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                if (!$(element).next('span').hasClass('input-group-addon'))
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                else {
                    if ($(element).parent('.input-group').next('span').hasClass('glyphicon-remove')) {
                        $(element).parent('.input-group').next('span').removeClass('glyphicon-remove');
                        $(element).parent('.input-group').next('span').addClass('glyphicon-ok');
                    }
                }
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            'ProcessorDetails.BusinessStartDate': {
                required: true,
                lessThanOrEqualTodayDate: true
            },
            'ProcessorDetails.BusinessNumber': {
                required: true
            },
            'ProcessorDetails.CommercialLiabHstNumber': {
                regex: '^[0-9]{9}$',
                required: function (element) {
                    return !cbProcessorDetailsProcessorDetailsVendorIsTaxExempt.is(':checked');
                }
            },
            'ProcessorDetails.IsTaxExempt': {
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#ProcessorDetails_CommercialLiabHstNumber'), true);
                    }
                    return false;
                }
            },
            //'ProcessorDetails.HasMoreThanOneEmp': {
            //    required: function (element) {
            //        if ($(element).is(':checked')) {
            //            resetHighlight($('#ProcessorDetails_WsibNumber'), true);
            //        }
            //        return true;
            //    }
            //},
            'ProcessorDetails.HIsGVWR': {
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#ProcessorDetails_ProcessorDetailsVendor_CVORNumber'), true);
                        resetHighlight($('#ProcessorDetails_ProcessorDetailsVendor_CVORExpiryDate'), true);
                    }
                    return true;
                }
            },

            'ProcessorDetails.CertificateOfApproval': {
                required: true,
            },
            'ProcessorDetails.CommercialLiabInsurerName': {
                required: true
            },
            'ProcessorDetails.CommercialLiabInsurerExpDate': {
                required: true,
                greaterThanTodayDate: true
            },
            'ProcessorDetails.CvorNumber': {
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR = $('#ProcessorDetails_ProcessorDetailsVendor_HIsGVWR:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'ProcessorDetails.CVORExpiryDate': {
                greaterThanTodayDate: true,
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR = $('#ProcessorDetails_ProcessorDetailsVendor_HIsGVWR:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'ProcessorDetails.WsibNumber': {
                regex: '^[0-9]{7}$',
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHasMoreThanOneEmp = $('#ProcessorDetails_WsibNumber:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHasMoreThanOneEmp.val() == 'True';
                }
            },
        },
        messages: {
            'ProcessorDetails.BusinessStartDate': '',//Global.StaffIndex.Resources.ValidationMsgInvalidBusinessStartDate,
            'ProcessorDetails.BusinessNumber': Global.StaffIndex.Resources.ValidationMsgInvalidOntarioBusinessNumber,
            'ProcessorDetails.CommercialLiabHstNumber': Global.StaffIndex.Resources.ValidationMsgInvalidHSTRegistrationNumber,
            'ProcessorDetails.CommercialLiabInsurerName': Global.StaffIndex.Resources.ValidationMsgInvalidComercialLiabilityInsurance,
            'ProcessorDetails.CommercialLiabInsurerExpDate': '',//Global.StaffIndex.Resources.ValidationMsgInvalidExpiryDate,
            'ProcessorDetails.CvorExpiryDate': '',//Global.StaffIndex.Resources.ValidationMsgInvalidExpiryDate,
            'ProcessorDetails.CvorNumber': Global.StaffIndex.Resources.ValidationMsgInvalidCVORNumber,
            'ProcessorDetails.WsibNumber': Global.StaffIndex.Resources.ValidationMsgInvalidWSIPNumber,
            'ProcessorDetails.HIsGVWR': '',
            'ProcessorDetails.HasMoreThanOneEmp': '',
            'ProcessorDetails.CertificateOfApproval': Global.StaffIndex.Resources.ValidationMsgInvalidCertificateOfApproval
        }
    });

    /* END Processor DETAILS */

    /* TERMS AND CONDITIONS */

    var validatorTermsInfo = $('#fmTermsAndConditions').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TermsAndConditions.SigningAuthorityFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.SigningAuthorityPosition': {
                required: true,
            },
            'TermsAndConditions.FormCompletedByFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
            },
            'TermsAndConditions.FormCompletedByPhone': {
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                required: true,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': {
                required: true,
                regex: '^([^a-z]*)([^a-z ]{2,})([ ]{1,}[^a-z ]+)*([ ]{1,}[^a-z ]{2,})+([^a-z]+)*$',
                //regex: '^([^a-z]{0,})*[^a-z ]{2,}[ ]{1,}([^a-z]{0,})*[^a-z ]{2,}([^a-z]{0,})*$',
            },
            'TermsAndConditions.AgreementAcceptedCheck': {
                required: true
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-signature')) {
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-required');
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-success');
                    $(element).closest('.termsandconditions-signature').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                else {
                    if ($(element).closest('.form-group').hasClass('has-required'))
                        $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('data-signature')) {
                    $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
                else {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
            }
        },
        success: function (e) {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TermsAndConditions.SigningAuthorityFirstName': Global.StaffIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.SigningAuthorityLastName': Global.StaffIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.SigningAuthorityPosition': Global.StaffIndex.Resources.ValidationMsgInvalidPosition,
            'TermsAndConditions.FormCompletedByFirstName': Global.StaffIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.FormCompletedByLastName': Global.StaffIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.FormCompletedByPhone': {
                required: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.StaffIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.StaffIndex.Resources.ValidationMsgInvalidPhone,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': Global.StaffIndex.Resources.ValidationMsgInvalidFullName,
            'TermsAndConditions.AgreementAcceptedCheck': Global.StaffIndex.Resources.ValidationMsgMustBeCheckedForSubmission,
        }
    });

    /* END TERMS AND CONDITIONS */

    /************************************************************************************************************
     ******************************* TIRE DETAILS START STAFF ********************************************************
     ************************************************************************************************************/
    var validatorTireDetailsInfo = $('#fmTireDetails').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight) {
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#item_checkbox'));
                }
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#product'));
                }
                else {
                    ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'))
                }
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TireDetails.TireItem': {
                required: true
            },

            'TireDetails.TireItemProduct': {
                required: true
            },
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('name') == 'TireDetails.TireItem') {
                    //$(element).closest('.form-group').css('border', '1px solid #a94442');
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                }
                if ($(element).attr('name') == 'TireDetails.TireItemProduct') {
                    if ($("input[name='TireDetails.TireItemProduct']:checked").length == 0) {
                        //$(element).closest('.form-group').css('border', '1px solid #a94442');
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }
                }

                if ($(element).attr('name') == 'TireDetails.RegistrantSubTypeID') {
                    if ($("input[name='TireDetails.RegistrantSubTypeID']:checked").length == 0) {
                        //REMOVE
                        //$(element).closest('.form-group').css('border', '1px solid #a94442');
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }
                }

                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');

                //$('form #fmTireDetails label').addClass();
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                if ($(element).attr('name') == 'TireDetails.TireItem') {
                    $(element).closest('.form-group').css('border', 'none');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }
                if ($("input[name='TireDetails.TireItemProduct']:checked").length > 0) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    $(element).closest('.form-group').css('border', 'none');
                }
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TireDetails.TireItem': 'Invalid Tire Type',
            'TireDetails.TireItemProduct': 'Invalid Product Produced Type',
            'TireDetails.HRelatedProcessor': 'Invalid Processor',
        }
    });

    /* Banking Information */
    var validatorBankingInfo = $('#frmBankingInformation').validate({
        ignore: ".ignore",
        onkeyup: function (element) {
            this.element(element);
        },

        onfocusout: function (element) {
            this.element(element);
        },
        rules: {
            'BankName': {
                //regex: '/^[\w\-\s]+$/',
                required: true,
                minlength: 2,
                maxlength: 50,
            },
            'TransitNumber': {
                //regex:'[0-9]',
                required: true,
                digits: true,
                minlength: 5,
                maxlength: 5,
            },
            'BankNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 3,
            },
            'AccountNumber': {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 15,
            },
            'Email': {
                required: ($('.isBankTransferNotficationPrimaryContact').is(':checked') ? false : true),
                email: true,
            },
            'CCEmail': {
                email: true,
            },
            'uploadFiles': {
                data: 'supportingDocumentsOption1'
            }
        },

        highlight: function (element) {
            if ($(element).val() || $(element).val() == '') {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            if ($(element).val()) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ($(element).val()) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
    });
    /*End Banking Information */

    /************************************************************************************************************
     ******************************* TIRE DETAILS ENDS  ********************************************************
     ************************************************************************************************************/

    /* HIGHLIGHTING OF FIELDS */

    //triggers validations
    var highlightFields = function () {
        var tmp = errorList.slice();
        for (var i = 0; i < tmp.length; i++) {
            if ($('input[name*=\'' + tmp[i] + '\']').length > 0 || $('select[name*=\'' + tmp[i] + '\']').length > 0) {
                var element = $('input[name*=\'' + tmp[i] + '\']');

                if (element.length == 0) {
                    element = $('select[name*=\'' + tmp[i] + '\']');
                }
                var pnl = element.closest('.panel-collapse');
                if (pnl)
                    pnl.collapse();

                if (!(element.val().length > 0) || element.is(":checkbox") || element.is(":radio")) {
                    element.valid();
                }
                else if (element.attr('type') == 'radio') {
                    element.valid();
                }
                //remove item from list
                var propertyName = $(element).attr('name');
                var index = errorList.indexOf(propertyName);
                if (index > -1)
                    errorList.splice(index, 1);
            }
        }
    }

    /* END HIGHLIGHTING OF FIELDS */

    /*Green Check Mark Validation Start */

    /*Green Check Mark Validation End */

    /* INITIALIZATION */

    var init = function () {
        var statusStr = String(Global.StaffIndex.Model.Status);
        if (Global.StaffIndex.Model.InvalidFormFields && statusStr.toLowerCase() == 'backtoapplicant') {
            errorList = Global.StaffIndex.Model.InvalidFormFields.slice();//obj.InvalidFormFields.toString().split(",");
            //invalidList = Global.StaffIndex.Model.InvalidFormFields;//obj.InvalidFormFields.toString().split(",");

            //this error list to check green panel level checks
            panelLevelCheckErrorList = Global.StaffIndex.Model.InvalidFormFields.slice();

            highlightFields();
        }

        //PanelBusinessLocationGreenCheck();
        ///PanelContactInfoGreenCheck();
        //AddButtonHandlerForGreenCheck();
        // PanelSortYardGreenCheck();
        // PanelTireDetailsGreenCheck();
        // PanelProcessorDetailsGreenCheck();
        // PanelProcessorTermsAndConditions();
        //SubmitButtonHandler();
    }
    init();

    /* END HIGHLIGHTING OF FIELDS */
});

//OTSTM2-83
$(function () {
    StaffCommonValidations.BusinessLocation();
    StaffCommonValidations.ContactInfo();
    StaffCommonValidations.SupportingDoc();
    StaffCommonValidations.TermsAndConditions();

    $('#fmSortYardDetails').dynamicReviewChkboxValidate({
        flagId: '#flagSortYard',
        formId: '#fmSortYardDetails',
        submitBtn: '#btnAddSort',
        isIgnore: false
    });

    //reviewCheckBox validation OTSTM2-83    
    $('#fmTireDetails').panelReviewCheckBoxValidate({
        flagId: '#flagTireDetails',
        rules: {
            'TireDetails.TireItem': {
                required: true,
                isChecked: $('#TireDetails_cbHRelatedProcessor').is(':checked')
            },
            'TireDetails.RegistrantSubTypeID': {
                required: true,
                isChecked: $('#TireDetails_cbProcessorType').is(':checked')
            },
            'TireDetails.TireItemProduct': {
                required: true,
                isChecked: $('#TireDetails_cbProcessorProductType').is(':checked')
            }
        }
    });


    //reviewCheckBox validation OTSTM2-83    
    $('#fmProcessorDetails').panelReviewCheckBoxValidate({
        flagId: '#flagProcessorDetails',
        rules: {
            'ProcessorDetails.BusinessStartDate': {
                required: true,
                isChecked: $('#ProcessorDetails_cbBusinessStartDate').is(':checked')
            },
            'ProcessorDetails.BusinessNumber': {
                required: true,
                isChecked: $('#ProcessorDetails_cbBusinessNumber').is(':checked')
            },
            'ProcessorDetails.CommercialLiabHstNumber': {
                required: true,
                isChecked: $('#ProcessorDetails_cbCommercialLiabHSTNumber').is(':checked')
            },
            'ProcessorDetails.CertificateOfApproval': {
                required: true,
                isChecked: $('#ProcessorDetails_cbCertificateOfApproval').is(':checked')
            },
            'ProcessorDetails.CommercialLiabInsurerName': {
                required: true,
                isChecked: $('#ProcessorDetails_cbCommercialLiabInsurerName').is(':checked')
            },
            'ProcessorDetails.CommercialLiabInsurerExpDate': {
                required: true,
                isChecked: $('#ProcessorDetails_cbCommercialLiabInsurerExpDate').is(':checked')
            },
            'ProcessorDetails.HasMoreThanOneEmp': {
                required: true,
                isChecked: $('#ProcessorDetails_cbHasMoreThanOneEmp').is(':checked')
            },
            'ProcessorDetails.WsibNumber': {
                required: function () {
                    return ($('#ProcessorDetails_HasMoreThanOneEmp:checked').val() == 'True' || $('#pnlWSIB').css('display') !== 'none');
                },
                isChecked: $('#ProcessorDetails_cbWsibNumber').is(':checked')
            }
        }
    });


});

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

function SubmitButtonHandler() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#disabledsubmitApplicationBtn').hide();
    //$('#submitApplicationBtn').show();

    $(".greenCheckmark").each(function () {
        if ($(this).attr("src") == statuses[1]) {
            $('#disabledsubmitApplicationBtn').hide();
            $('#submitApplicationBtn').show();
        }
        else {
            $('#disabledsubmitApplicationBtn').show();
            $('#submitApplicationBtn').hide();
            return false;
        }
    });
}

function PanelBusinessLocationGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $("#fmBusinessLocation").valid();

    if ($("#fmBusinessLocation").valid()) {
        $('#flagBusinessLocation').attr('src', statuses[1]);
    }
    else {
        $('#flagBusinessLocation').attr('src', statuses[0]);
    }

    $('#fmBusinessLocation').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($(this).hasClass("isMailingAddressDifferent") && !$(this).is(":checked")) {
                    $('#flagBusinessLocation').attr('src', statuses[0]);
                }
                else {
                    if ($("#fmBusinessLocation").valid()) {
                        $('#flagBusinessLocation').attr('src', statuses[1]);
                    }
                    else {
                        $('#flagBusinessLocation').attr('src', statuses[0]);
                    }
                }
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelContactInfoGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#panelContactInformation').removeClass('collapse');
    //$('#panelContactInformation').addClass('collapse-in');

    $("#fmContactInfo").valid();

    if ($("#fmContactInfo").valid()) {
        $('#flagContactInformation').attr('src', statuses[1]);
    }
    else {
        $('#flagContactInformation').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmContactInfo').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($(this).hasClass("isContactAddressSameAsBusinessAddress") && !$(this).is(":checked")) {
                    $('#flagContactInformation').attr('src', statuses[0]);
                }
                else {
                    if ($("#fmContactInfo").valid()) {
                        $('#flagContactInformation').attr('src', statuses[1]);
                    }
                    else {
                        $('#flagContactInformation').attr('src', statuses[0]);
                    }
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();

                $("#fmContactInfo").valid();
            });
        });
    })
}

function PanelSortYardGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#panelSortYardDetails').removeClass('collapse');
    //$('#panelSortYardDetails').addClass('collapse-in');

    $("#fmSortYardDetails").valid();

    if ($("#fmSortYardDetails").valid()) {
        $('#flagSortYard').attr('src', statuses[1]);
    }
    else {
        $('#flagSortYard').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmSortYardDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmSortYardDetails").valid()) {
                    $('#flagSortYard').attr('src', statuses[1]);
                }
                else {
                    $('#flagSortYard').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelTireDetailsGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#panelTireDetails').removeClass('collapse');
    //$('#panelTireDetails').addClass('collapse-in');

    $("#fmTireDetails").valid();

    if ($("#fmTireDetails").valid()) {
        $('#flagTireDetails').attr('src', statuses[1]);
    }
    else {
        $('#flagTireDetails').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmTireDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmTireDetails").valid()) {
                    $('#flagTireDetails').attr('src', statuses[1]);
                }
                else {
                    $('#flagTireDetails').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelProcessorDetailsGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#panelProcessorDetails').removeClass('collapse');
    //$('#panelProcessorDetails').addClass('collapse-in');

    ProcessorDetailEnableReadOnly(false);

    var isValid = $("#fmProcessorDetails").valid();
    $('#flagProcessorDetails').attr('src', isValid ? statuses[1] : statuses[0]);

    ProcessorDetailEnableReadOnly(true);

    ///any changes to the Processor info form is handled here
    $('#fmProcessorDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                ProcessorDetailEnableReadOnly(false);

                if ($('#ProcessorDetails_IsTaxExempt').is(':checked')) {
                    $('#flagProcessorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#ProcessorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#ProcessorDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
                }
                else {
                    $('#flagProcessorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#ProcessorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#ProcessorDetails_CommercialLiabHstNumber').attr('readonly', false);
                }

                isValid = $("#fmProcessorDetails").valid();
                $('#flagProcessorDetails').attr('src', isValid ? statuses[1] : statuses[0]);
                ProcessorDetailEnableReadOnly(true);
                $("#fmProcessorDetails").valid();

                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelProcessorTermsAndConditions() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    //$('#panelsTermsAndConditions').removeClass('collapse');
    //$('#panelsTermsAndConditions').addClass('collapse-in');

    $("#fmTermsAndConditions").valid();

    if ($("#fmTermsAndConditions").valid()) {
        $('#flagTermsAndConditions').attr('src', statuses[1]);
    }
    else {
        $('#flagTermsAndConditions').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmTermsAndConditions').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmTermsAndConditions").valid()) {
                    $('#flagTermsAndConditions').attr('src', statuses[1]);
                }
                else {
                    $('#flagTermsAndConditions').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function ProcessorDetailEnableReadOnly(flag) {
    //remove readonly methods in Processor details
    $('#ProcessorDetails_BusinessStartDate').attr("readonly", flag);
    $('#ProcessorDetails_CommercialLiabInsurerExpDate').attr("readonly", flag);
    $('#ProcessorDetails_CvorExpiryDate').attr("readonly", flag);
}

function AddButtonHandlerForGreenCheck() {
    $('#btnAddContact').on('click', function () {
        PanelContactInfoGreenCheck();
    });

    $('#btnAddSort').on('click', function () {
        PanelSortYardGreenCheck();
    });
}

