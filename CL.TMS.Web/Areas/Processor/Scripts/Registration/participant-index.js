﻿/* GLOBAL FUNCTIONS */
var toolTipSettings = {
    trigger: 'hover',
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};
/* END GLOBAL FUNCTIONS */

$(document).ready(function () {
    BusinessLocation();
    ContactInformation();
    TireDetails();
    ProcessorDetails();
    SortyardDetails();
    SupportingDocuments();
    TermsAndConditions();
    ProductDescCheck();
    /*Shared between all participant panels */

    //fix backspace error for checkboxes
    $('input[type="checkbox"]').keydown(function (e) {
        if (e.keyCode === 8) {
            return false;
        }
    });

    //input only allow numbers
    var restrictOnlyNumbers = function () {
        $(".only-numbers").keydown(function (event) {
            // Allow only backspace and delete
            if (event.keyCode == 46 || event.keyCode == 8) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
                }
            }
        });
    }

    var initialize = function () {
        titleCase();
        restrictOnlyNumbers();
        DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        InitAutoSave();
    }
    initialize();

    $('#submitApplicationBtn').on('click', function () {
        $.ajax({
            url: '/Processor/Registration/SubmitApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#ID').val(), tokenId: Global.ParticipantIndex.Resources.TokenId },
            complete: function () {
                window.location.reload();
            }
        });
    });
});

function getSortYardDetailsTSYCContent() {
    return $('#sortYardDetailsTSYC').html();
}

function getSortYardDetailsTSYCSettings() {
    return {
        content: getSortYardDetailsTSYCContent(),
        width: 300,
        height: 90,
    };
}

function ProductDescCheck() {
    $(".checkbox input[type='checkbox']").each(function () {
        if ($(this).data('myattri') == 'Other~') {
            var show = $(this).prop('checked');
            $('#pnlOtherProd').toggle(show);
            if (!show) {
                $('#pnlOtherProd').find('input').val('');
            }
        }
    });

    $(".radio input[type='radio']").each(function () {
        if ($(this).attr('id') == 'TireDetails_RegistrantSubTypeID') {
            var showProcessorTypeOther = false;

            if ($(this).val() == 4 && $(this).attr('checked')) {
                showProcessorTypeOther = true;
            }

            $('#pnlOtherProcType').toggle(showProcessorTypeOther);

            if (!showProcessorTypeOther) {
                $('#pnlOtherProcType').find('input').val('');
            }
        }
    });
};

function BusinessLocation() {
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };

    $('#MailingAddressSameAsBusiness').click(function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {
            CopyMailingAddress();
        }
        else {
            ClearMailingAddress();
        }
        toggleMailingAddress();

        $("#fmBusinessLocation").valid();
    });

    toggleMailingAddress();
}

function CopyMailingAddress() {
    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())

    $('input[name="BusinessLocation.MailingAddress.City"]').val($('#BusinessLocation_BusinessAddress_City').val())
    $('input[name="BusinessLocation.MailingAddress.Province"]').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('select[name="BusinessLocation.MailingAddress.Country"]').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearMailingAddress() {
    $('input[name="BusinessLocation.MailingAddress.AddressLine1"]').val('')
    $('input[name="BusinessLocation.MailingAddress.City"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Province"]').val('')
    $('input[name="BusinessLocation.MailingAddress.AddressLine2"]').val('')
    $('input[name="BusinessLocation.MailingAddress.Postal"]').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function ContactInformation() {
    $('.isContactAddressSameAsBusinessAddress').on('click', function () {
        contactAddressSameAs();
    });

    //#region
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        contactTemplate = "<div class='template" + i + "'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');

        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }
    $('#btnAddContact').on('click', function () {
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
    });

    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            currAddCount = contactTemplateCount++;
            temp = "<div class='ctemp'><div class='template" + currAddCount + "'>" + temp + '</div></div><hr>';
            $(temp).appendTo('#divContactTemplate');

            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }

    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            /*
                         if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                             if ($(this).is(":checked")) {
                                 $(this).closest('.row').next('.contactAddressDifferent').hide();
                             }
                             else {
                                 $(this).closest('.row').next('.contactAddressDifferent').show();
                             }
                         }
            */
        });
    }

    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }
    primaryContactCheckName();
}

function ProcessorDetails() {
    //applies to radiobutton for ProcessorDetails_CVORNumber
    $('.cvor').on('click', function () {
        DisplayCVORDocumentOption();
    });

    //applies to radiobutton for ProcessorDetails.ProcessorDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    $('#ProcessorDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });

    $('#dpBusinessStartDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpInsuranceExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpCVORExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    //var disabledHSTTextBox = function () {
    //    if (!($('#ProcessorDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
    //        if ($('#ProcessorDetails_IsTaxExempt').is(':checked'))
    //        {
    //            $('#ProcessorDetails_CommercialLiabHstNumber').val('').attr('disabled', 'disabled');
    //        }
    //        else {
    //            $('#ProcessorDetails_CommercialLiabHstNumber').removeAttr('disabled');
    //        }
    //    }
    //}

    var disabledHSTTextBox = function () {
        if (!($('#ProcessorDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#ProcessorDetails_IsTaxExempt').is(':checked')) {
                $('#ProcessorDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
            }
            else {
                $('#ProcessorDetails_CommercialLiabHstNumber').attr('readonly', false);
            }
        }
    }

    $('#ProcessorDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });

    $('.radio input.cvor').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlCVOR').toggle(show);
        if (!show) {
            $('#pnlCVOR').find('input').val('');
        }
    });

    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    $('.checkbox input[type=checkbox]').on('change', function () {
        if ($(this).data('myattri') == 'Other~') {
            var show = $(this).prop('checked');

            $('#pnlOtherProd').toggle(show);

            if (!show) {
                $('#pnlOtherProd').find('textarea').val('');
            }
        }
    });

    $('.radio input[type=radio]').on('change', function () {
        if ($(this).attr('id') == 'TireDetails_RegistrantSubTypeID') {
            var show = false;

            if ($(this).val() == 4) {
                show = true;
            }

            $('#pnlOtherProcType').toggle(show);

            if (!show) {
                $('#pnlOtherProcType').find('textarea').val('')
            }
        }
    });

    //disable checkboxes for CVORAndExpiryDate
    var disableCVORAndExpiryDateCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#ProcessorDetails_cbCVORNumber').removeAttr('disabled');
            $('#ProcessorDetails_cbCVORExpiryDate').removeAttr('disabled');
        }
        else {
            $('#ProcessorDetails_cbCVORNumber').attr('disabled', 'disabled').removeAttr('checked');
            $('#ProcessorDetails_cbCVORExpiryDate').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    //disable checkboxes for WSIBNumber
    var disableWSIBNumberCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#ProcessorDetails_cbWSIBNumber').removeAttr('disabled');
        }
        else {
            $('#ProcessorDetails_cbWSIBNumber').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    //$('#ProcessorDetails_cbHIsGVWR').on('change', function () {
    //    disableCVORAndExpiryDateCheckbox(this);
    //});

    $('#ProcessorDetails_cbHasMoreThanOneEmp').on('change', function () {
        disableWSIBNumberCheckbox(this);
    });

    disabledHSTTextBox();
    $('#pnlCVOR').toggle($('.radio input.cvor:checked').val() == 'True');
    $('#pnlWSIB').toggle($('.radio input.wsib:checked').val() == 'True');
}

function TireDetails() {
    $('#cb_tiredetails').change(function () {
        if ($(this).is(":checked")) $('#cb_tiredetails').attr('checked', true);
        else $('#cb_tiredetails').attr('checked', false);
    });

    /*Needed for tiredetails validation */
    var OTSProcessorRadio;
    var OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();

    $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
        if (this.checked)
            OTSProcessorRadio = $(this).val();
    });

    $('input[type=radio][class=HHasRelatioshipWithProcessor]').change(function () {
        OTSProcessorRadio = $(this).val();

        if (OTSProcessorRadio == "False") {
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').closest('.form-group').removeClass('has-error');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').removeClass('has-success');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').addClass('has-required');

            var cnfrm = confirm(Global.ParticipantIndex.Resources.TireDetailsDialog);
            if (cnfrm != true) {
                $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
                $('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
                OTSProcessorRadio = "True";
                $("#fmTireDetails").valid();

                $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
                    if ($(this).val() == "True") {
                        this.checked = true;
                    }
                    else this.checked = false;
                });
            }
            else {
                $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", true);
                $('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
            }
        }
        else {
            $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').removeClass('has-success');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').removeClass('has-required');
        }
        OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();
        $("#fmTireDetails").valid();
    });

    $('select[name="TireDetails.HRelatedProcessor"]').change(function () {
        $("#fmTireDetails").valid();
    });

    /*End tire details validation*/
}

function SortyardDetails() {
    var sortYardtempBase = $(".sortyardtemp").val();
    var sortYardtempIncremental;
    var sortYardTemplateCount = $('#sorYardCount').val();
    var maxStorageCap = 0;
    var currAddCount = 0;
    $('#SortYardCount').text("0");

    for (var i = 0; i < sortYardTemplateCount; i++) {
        var sortYardTemplate = $("#sortYardTemplate" + i).val();
        sortYardTemplate = "<div class='sortYardtemplate" + i + "'>" + sortYardTemplate + '</div><hr>';

        $(sortYardTemplate).appendTo('#fmSortYardDetails');

        if (i == 0)
            $('.sort-yard-address-disabled').attr('disabled', 'disabled');
    }

    $('#SortYardCount').text(sortYardTemplateCount);

    $('#btnAddSort').on('click', function () {
        addSortRow();
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
        titleCase();
        $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, getSortYardDetailsTSYCSettings()));
    });

    var addSortRow = function () {
        currAddCount = sortYardTemplateCount++;
        $('#SortYardCount').text(sortYardTemplateCount);
        sortYardtempIncremental = "<div class='ctemp'><div class='sortYardtemplate" + currAddCount + "'>" + sortYardtempBase + '</div></div><hr>';
        $(sortYardtempIncremental).appendTo('#fmSortYardDetails');

        //set the datamaxstorage of the new added sortyard to 0 so its not nan
        $('.sortYardtemplate' + currAddCount + ' input[data-maxstorage]').attr('data-maxstorage', 0);
        $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');

        for (var i = 0; i < sortYardTemplateCount; i++) {
            $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {
                if ((typeof $(this).attr('name') != 'undefined')) {
                    $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                }
                if ((typeof $(this).attr('id') != 'undefined')) {
                    $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
                }
            });

            if (i == currAddCount) {
                $('.sortYardtemplate' + i + ' input[type=text]').removeClass('sort-yard-address-disabled');
                $('.sortYardtemplate' + i + ' select').removeClass('sort-yard-address-disabled');
            }
        }
    }
    var totalStorage = function () {
        var inputs = $("input[data-maxstorage]");
        maxStorageCap = 0;
        inputs.each(function () {
            if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
                maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
            }
        });
        if (!isNaN(parseFloat(maxStorageCap))) {
            $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
        }
    }

    var bindSortYardSelect = function () {
        $(".sortYardtemplate" + currAddCount + " select").each(function () {
            if (currAddCount > 0)
                $(this).children().removeAttr('selected');
        });
    }

    var renameSortYardLabel = function () {
        var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
        for (var j = 0; j < label.length; j++) {
            var num = 1;
            $(label[j]).each(function () {
                var newLabel = $(this).text().replace(/\d+/, num++);
                $(this).text(newLabel);
            });
        }
    }

    //copies code from business location to sort yard
    $('.sortyard-address').on('focusout', function () {
        var attr = $(this).attr('data-sortyard');
        if (typeof attr !== typeof undefined && attr) {
            if ($("input[name*='" + attr + "']").length > 0)
                $("input[name*='" + attr + "']").val($(this).val());
            if ($("select[name*='" + attr + "']").length > 0) {
                $("select[name*='" + attr + "']").val($(this).val());
            }
        }
    });

    //Set the Max Storage on value change
    $("#panelSortYardDetails").on('focusout', "input[data-maxstorage]", function () {
        $(this).attr('data-maxstorage', $(this).val());
    });

    var initSortYard = function () {
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
    }
    initSortYard();

    $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, getSortYardDetailsTSYCSettings()));
}

function SupportingDocuments() {
    $('input[name="supportingDocumentsOptionMBL"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCLI"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCVOR"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionPRL"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionHST"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionWSIB"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCHQ"]').on('change', function () {
    });
}

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
}

/* COMMON FUNCTIONS */
function _isEmpty(value) {
    return (value == null || value.length === 0);
}

function titleCase() {
    $('.title-case').on('keypress', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}

function DisplayCVORDocumentOption() {
    var show = ($('#ProcessorDetails_HIsGvwr:checked').val() == 'True');
    $('#cVORDocument').toggle(show);
    //found in CL.TMS.Web\js\FileUpload\main.js
    //should not be doing this but need to validate supporting docs panel, needs to be refactored
    PanelGreenCheckSuppDoc();
}

function DisplayWSIBDocumentOption() {
    var show = ($('#ProcessorDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
    //found in CL.TMS.Web\js\FileUpload\main.js
    //should not be doing this but need to validate supporting docs panel, needs to be refactored
    PanelGreenCheckSuppDoc();
}

function DisplayHSTDocumentOption() {
    var show = !$('#ProcessorDetails_IsTaxExempt').is(':checked');
    $('#hSTDocument').toggle(show);
    //found in CL.TMS.Web\js\FileUpload\main.js
    //should not be doing this but need to validate supporting docs panel, needs to be refactored
    PanelGreenCheckSuppDoc();
}

var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
    });
}

var totalStorage = function () {
    var maxStorageCap = 0;
    var inputs = $("input[data-maxstorage]");
    inputs.each(function () {
        if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
            maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        }
    });
    if (!isNaN(parseFloat(maxStorageCap))) {
        $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
    }
}

/* END COMMON FUNCTIONS */

function InitAutoSave() {
    $('#AppProcessorFormID').on('change', ':input', function () {
        var appProcessor = $(this);
        $.when(
        appProcessor.focusout()).then(function () {
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });

    $('#AppProcessorFormID').on('change', '.ctemp', function () {
        var ctemp = $(this);
        $.when(
        ctemp.focusout()).then(function () {
            processJson();
            totalStorage();
            contactAddressSameAs();
        });
    });
}

function processJson() {
    var myform = $('#AppProcessorFormID');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var data = $('form').serializeArray();
    data = data.concat(
        $('#AppProcessorFormID input[type=checkbox]:not(:checked)').map(
        function () {
            return { "name": this.name, "value": this.value }
        }).get());

    data = data.concat(
    $('#AppProcessorFormID input[type=radio]:checked').map(
        function () {
            return { "name": this.name, "value": this.value }
        }).get());

    $('#item_checkbox input[type="checkbox"]').each(function () {
        if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
            data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
        }
    });

    $('#product input[type="checkbox"]').each(function () {
        if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
            data.push({ name: 'ProductsProduceTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
        }
    });

    if ($('#MailingAddressSameAsBusiness').is(':checked')) {
        data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
    }
    disabled.attr('disabled', 'disabled');

    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant') {
        //insert current invalid forms back into model
        var tmpList = Global.ParticipantIndex.Model.InvalidFormFields;
        for (var i = 0; i < tmpList.length; i++) {
            data.push({ name: 'InvalidFormFields[]', value: tmpList[i] });
        }
    }

    if (Global.ParticipantIndex.Model.Status == 'Open') {
        var len = data.length;
        for (var j = 0; j < len; j++) {
            data.push({ name: 'InvalidFormFields[]', value: data[j].name });
        }
    }
    //remove leading and trailing space
    var len = data.length;
    for (var i = 0; i < len; i++) {
        data[i].value = $.trim(data[i].value);
    }

    $.ajax({
        url: '/Processor/Registration/SaveModel',
        dataType: 'json',
        type: 'POST',
        data: $.param(data),
        success: function (result) {
        },
        error: function (result) {
        }
    });
};

/* GABE STYLES */

(function () {
    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
        businessLocationLBNSettings = {
            content: businessLocationLBNContent,
            width: 300,
            height: 100,
        };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
        businessLocationBONSettings = {
            content: businessLocationBONContent,
            width: 300,
            height: 100,
        };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationBONSettings));

    // Sort Yard Details: All Sort Yards Capacity
    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
        sortYardDetailsASYCSettings = {
            content: sortYardDetailsASYCContent,
            width: 300,
            height: 90,
        };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, sortYardDetailsASYCSettings));

    var sortYardDetailsTSContent = $('#sortYardDetailsTS').html(),
       sortYardDetailsTSSettings = {
           content: sortYardDetailsTSContent,
           width: 300,
           height: 100,
       };
    var popLargeTS = $('.sortYardDetailsTS').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, sortYardDetailsTSSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
        tiredetailsPopoverPLTSettings = {
            content: tiredetailsPopoverPLTContent,
            width: 500,
            height: 600,
        };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
        tiredetailsPopoverMTSettings = {
            content: tiredetailsPopoverMTContent,
            width: 380,
            height: 160,
        };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverAGLSSettings));

    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
        tiredetailsPopoverINDSettings = {
            content: tiredetailsPopoverINDContent,
            width: 360,
            height: 120,
        };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverINDSettings));

    // Tire Details: Small Off the Road Tire (SOTR)
    var tiredetailsPopoverSOTRContent = $('#tiredetailsPopoverSOTR').html(),
        tiredetailsPopoverSOTRSettings = {
            content: tiredetailsPopoverSOTRContent,
            width: 430,
            height: 120,
        };
    var popLargeSOTR = $('.tiredetailsPopoverSOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverSOTRSettings));

    // Tire Details: Medium Off the Road Tire (MOTR)
    var tiredetailsPopoverMOTRContent = $('#tiredetailsPopoverMOTR').html(),
        tiredetailsPopoverMOTRSettings = {
            content: tiredetailsPopoverMOTRContent,
            width: 430,
            height: 120,
        };
    var popLargeMOTR = $('.tiredetailsPopoverMOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverMOTRSettings));

    // Tire Details: Large Off the Road Tire (LOTR)
    var tiredetailsPopoverLOTRContent = $('#tiredetailsPopoverLOTR').html(),
        tiredetailsPopoverLOTRSettings = {
            content: tiredetailsPopoverLOTRContent,
            width: 420,
            height: 120,
        };
    var popLargeLOTR = $('.tiredetailsPopoverLOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverLOTRSettings));

    // Tire Details: Giant Off the Road Tire (GOTR)
    var tiredetailsPopoverGOTRContent = $('#tiredetailsPopoverGOTR').html(),
        tiredetailsPopoverGOTRSettings = {
            content: tiredetailsPopoverGOTRContent,
            width: 420,
            height: 100,
        };
    var popLargeGOTR = $('.tiredetailsPopoverGOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverGOTRSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBL').html(),
        supportingDocumentsAIMBLSettings = {
            content: supportingDocumentsAIMBLContent,
            width: 500,
            height: 140,
        };
    var popLargeAIMBL = $('.supportingDocumentsAIMBL').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLI').html(),
        supportingDocumentsCLISettings = {
            content: supportingDocumentsCLIContent,
            width: 500,
            height: 260,
        };
    var popLargeCLI = $('.supportingDocumentsCLI').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCLISettings));

    // Supporitng Documents: supportingDocumentsCFP
    var supportingDocumentsCFPContent = $('#supportingDocumentsCFP').html(),
    supportingDocumentsCFPSettings = {
        content: supportingDocumentsCFPContent,
        width: 500,
        height: 70,
    };
    var popLargeCLI = $('.supportingDocumentsCFP').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCFPSettings));

    // Supporitng Documents: supportingDocumentWSIB
    var supportingDocumentWSIBContent = $('#supportingDocumentWSIB').html(),
    supportingDocumentWSIBSettings = {
        content: supportingDocumentWSIBContent,
        width: 500,
        height: 86,
    };
    var popLargeCLI = $('.supportingDocumentWSIB').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentWSIBSettings));
    // Supporitng Documents: supportingDocumentsHST
    var supportingDocumentsHSTContent = $('#supportingDocumentsHST').html(),
    supportingDocumentsHSTSettings = {
        content: supportingDocumentsHSTContent,
        width: 500,
        height: 160,
    };
    var popLargeCLI = $('.supportingDocumentsHST').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsHSTSettings));

    // Supporitng Documents: CVOR Abstract Level II
    var supportingDocumentsCVORContent = $('#supportingDocumentsCVOR').html(),
        supportingDocumentsCVORSettings = {
            content: supportingDocumentsCVORContent,
            width: 460,
            height: 170,
        };
    var popLargeCVOR = $('.supportingDocumentsCVOR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCVORSettings));

    // Supporitng Documents: Processor Relationship Letter
    var supportingDocumentsPRLContent = $('#supportingDocumentsPRL').html(),
        supportingDocumentsPRLSettings = {
            content: supportingDocumentsPRLContent,
            width: 460,
            height: 105,
        };
    var popLargePRL = $('.supportingDocumentsPRL').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsPRLSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCH').html(),
        supportingDocumentsVOCHSettings = {
            content: supportingDocumentsVOCHContent,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.supportingDocumentsVOCH').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsVOCHSettings));

    // Tire Details & Service Area: Zone 1
    var serviceAreaZone1Content = $('#serviceAreaZone1').html(),
        serviceAreaZone1Settings = {
            content: serviceAreaZone1Content,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.serviceAreaZone1').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, serviceAreaZone1Settings));
})();

/* END GABE STYLES */