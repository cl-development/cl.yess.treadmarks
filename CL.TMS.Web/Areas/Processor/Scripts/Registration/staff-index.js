﻿/* GLOBAL FUNCTIONS */
var toolTipSettings = {
    trigger: 'hover',
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};
/* END GLOBAL FUNCTIONS */

$(function () {
    "use strict";
    BusinessLocation();
    ContactInformation();
    SortYardDetails();
    TireDetails();
    ProcessorDetails();
    BankingInformation();
    SupportingDocuments();
    TermsAndConditions();
    ProductDescCheck();
    var initialize = function () {
        DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        contactAddressSameAs();
        ApplicationCommon.setContactAddressCountryDropDown();
        restrictOnlyNumbers();
        InitAutoSave();
        //REMOVE
        //certificateOfApproval();        
        titleCase();
        //fix backspace error for checkboxes
        $('input[type="checkbox"]').keydown(function (e) {
            if (e.keyCode === 8) {
                return false;
            }
        });

        ////OTSTM2-155
        //if (Global.StaffIndex.Settings.AllowEdit == "False") {
        //    $("#staffWorkflowBtns").find(".btn").addClass("disabled").prop("disabled", true);
        //    $(".activeInactive").find(".btn").addClass("disabled").prop("disabled", true);
        //    $(".panel-group").find("input").addClass("disabled").prop("disabled", true);
        //    $(".panel-group").find("textarea").addClass("disabled").prop("disabled", true);
        //    $(".panel-group").find("label").css("cursor", "not-allowed");
        //    $(".panel-group").find("button").addClass("disabled").prop("disabled", true);
        //    $(".panel-group").find("select").addClass("disabled").prop("disabled", true);
        //    //OTSTM2-532
        //    if (Global.StaffIndex.Settings.AllowUploadFile == "True") {
        //        $("#fileUploadInput").removeClass("disabled").prop("disabled", false);
        //    }
        //}
    }
    initialize();
});

window.onload = function () {
    disableInputIsNotStaff();
};

function getSortYardDetailsTSYCContent() {
    return $('#sortYardDetailsTSYC').html();
}

function getSortYardDetailsTSYCSettings() {
    return {
        content: getSortYardDetailsTSYCContent(),
        width: 300,
        height: 90,
    };
}

function ProductDescCheck() {
    $(".checkbox input[type='checkbox']").each(function () {
        if ($(this).data('myattri') == 'Other~') {
            var show = $(this).prop('checked');
            $('#pnlOtherProd').toggle(show);
            if (!show) {
                $('#pnlOtherProd').find('input').val('');
            }
        }
    });

    var show = false;
    $(".radio input[type='radio']").each(function () {
        if ($(this).attr('id') == 'TireDetails_RegistrantSubTypeID') {

            if ($(this).val() == 4 && $(this).attr('checked')) {
                show = true;
            }
        }
    });

    $('#pnlOtherProcType').toggle(show);
    if (!show) {
        $('#pnlOtherProcType').find('textarea').val('');
    }
};
function BusinessLocation() {
    var toggleMailingAddress = function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');
        $(".mailingAddressPanel").toggle(!isChecked);
        if (isChecked) {
            $('.mailingAddressPanel').find('input, select').val('');
        }
    };
    $('#MailingAddressSameAsBusiness').click(function () {
        var isChecked = $('#MailingAddressSameAsBusiness').is(':checked');

        if (isChecked) {
            CopyMailingAddress();
        }
        else {
            ClearMailingAddress();
        }
        toggleMailingAddress();
    });
    toggleMailingAddress();
};

function CopyMailingAddress() {
    $('#BusinessLocation_MailingAddress_AddressLine1').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('#BusinessLocation_MailingAddress_City').val($('#BusinessLocation_BusinessAddress_City').val())
    $('#BusinessLocation_MailingAddress_Province').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('#BusinessLocation_MailingAddress_AddressLine2').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('#BusinessLocation_MailingAddress_Postal').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('#BusinessLocation_MailingAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearMailingAddress() {
    $('#BusinessLocation_MailingAddress_AddressLine1').val('')
    $('#BusinessLocation_MailingAddress_City').val('')
    $('#BusinessLocation_MailingAddress_Province').val('')
    $('#BusinessLocation_MailingAddress_AddressLine2').val('')
    $('#BusinessLocation_MailingAddress_Postal').val('')
    $('#BusinessLocation_MailingAddress_Country').val('')
    $('select[name="BusinessLocation.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function CopyContactAddress() {
    $('#ContactInformationList_0__ContactAddress_AddressLine1').val($('#BusinessLocation_BusinessAddress_AddressLine1').val())
    $('#ContactInformationList_0__ContactAddress_City').val($('#BusinessLocation_BusinessAddress_City').val())
    $('#ContactInformationList_0__ContactAddress_Province').val($('#BusinessLocation_BusinessAddress_Province').val())
    $('#ContactInformationList_0__ContactAddress_AddressLine2').val($('#BusinessLocation_BusinessAddress_AddressLine2').val())
    $('#ContactInformationList_0__ContactAddress_Postal').val($('#BusinessLocation_BusinessAddress_Postal').val())
    $('#ContactInformationList_0__ContactAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val())
}

function ClearContactAddress() {
    $('#ContactInformationList_0__ContactAddress_AddressLine1').val('')
    $('#ContactInformationList_0__ContactAddress_City').val('')
    $('#ContactInformationList_0__ContactAddress_Province').val('')
    $('#ContactInformationList_0__ContactAddress_AddressLine2').val('')
    $('#ContactInformationList_0__ContactAddress_Postal').val('')

    $('select[name="ContactInformationList_0__ContactAddress_Country.MailingAddress.Country"]')[0].selectedIndex = 0;
}

function ContactInformation() {
    var contactLimit = 3;
    var k = 0;
    var temp = $(".temp").val();

    if (temp != undefined) {
        temp = temp.replace(/value=\".+\"/g, "value=''"); //OTSTM2-50 remove all default value of input
    }
    
    var contactTemplateCount = $('#contactCount').val();
    var currAddCount = 0;

    for (var i = 0; i < contactTemplateCount; i++) {
        var contactTemplate = $("#contactTemplate" + i).val();
        contactTemplate = "<div class='template" + i + " parent'>" + contactTemplate + '</div><hr>';
        $(contactTemplate).appendTo('#divContactTemplate');
        $(".template" + i + " input[type='checkbox']").each(function () {
            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    // CopyContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    //   ClearContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    if (contactTemplateCount == contactLimit) {
        $('#btnAddContact').closest('div.row').hide();
    }

    var initCheckboxClearField = function () {
        $('input[type="checkbox"]').on('click', function () {
            var checkBox = $(this);
            clearField(checkBox);
        });
    }

    //$('#btnAddContact').on('click', function () {
    //    initCheckboxClearField();
    //});

    $('#btnAddContact').on('click', function () {
        initCheckboxClearField();
        addContactRow();
        bindContactCheckbox();
        bindContactSelect();
        primaryContactCheckName();
    });
    $('.isContactAddressDifferent').click(function () {
        $(".contactAddressDifferent").toggle(this.unchecked);
    });
    var addContactRow = function () {
        if (contactTemplateCount < contactLimit) {
            currAddCount = contactTemplateCount++;
            tempDiv = "<div class='ctemp'><div class='template" + currAddCount + " parent'>" + temp + '</div></div><hr>';  //OTSTM2-50 change temp to tempDiv to avoid multiple <hr> after removing and adding
            $(tempDiv).appendTo('#divContactTemplate');
            $('.template' + currAddCount + ' input[type="text"]').val('');

            for (var i = 0; i < contactTemplateCount; i++) {
                $(".template" + i + " input[type='text']," + ".template" + i + " input[type='checkbox']," + ".template" + i + " input[type='hidden']," + ".template" + i + " select").each(function () {
                    if ((typeof $(this).attr('name') != 'undefined')) {
                        $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                    }
                });
            }
        }
        if (contactTemplateCount == contactLimit) {
            $('#btnAddContact').closest('div.row').hide();
        }
    }
    var bindContactCheckbox = function () {
        $(".template" + currAddCount + " input[type='checkbox']").each(function () {
            if ($(this).hasClass('no-validate'))
                $(this).removeAttr('checked');

            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, currAddCount));
            }
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                $(this).attr('data-att-chkbox', $(this).attr('data-att-chkbox').replace(/\d+/, currAddCount));
            }

            if ($(this).attr('id') == "ContactAddressSameAsBusinessAddress") {
                if ($(this).is(":checked")) {
                    //  CopyContactAddress();

                    $(this).closest('.row').next('.contactAddressDifferent').hide();
                }
                else {
                    //  ClearContactAddress();
                    $(this).closest('.row').next('.contactAddressDifferent').show();
                }
            }
        });
    }

    var bindContactSelect = function () {
        $(".template" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }
    var primaryContactCheckName = function () {
        var contactNames = ['.contactFName', '.contactLName'];

        for (var j = 0; j < contactNames.length; j++) {
            var primaryContactCount = $(contactNames[j]).size();
            if (primaryContactCount > 1) {
                var count = 0;
                $(contactNames[j]).each(function () {
                    if (count > 0) {
                        $(this).text($(this).text().replace('Primary Contact', 'Contact ' + count + ' '));
                    }
                    count++;
                });
            }
        }
    }

    $(".dropdownCountry").change(function () {
        var optionVal = $('option:selected', this).text();
        $(this).prev('input').val(optionVal);
    });

    primaryContactCheckName();

    //OTSTM2-50 
    $(document).on("click", ".btnRemoveContact", function (evt) {

        evt.stopImmediatePropagation();
        var contactSection = $(this).parent().parent();
        if (Global.StaffIndex.Settings.IsApprovedApplication) { //for registered application
            clearAllInput(contactSection);

            contactTemplateCount--;
            if (contactSection.parent().hasClass("ctemp")) { //for new added contact without refresh page
                if (contactSection.hasClass("template1") && $(".template2").length > 0) { //remove contact1, switch contact2 to contact1
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");

                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                }
                else { //remove contact2 or remove contact1 (no contact2)
                    contactSection.parent().next().remove();
                    contactSection.parent().remove();
                }
            }
            else { //for existing contact
                if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                    contactSection.next().remove();
                    contactSection.remove();
                    $(".template2").removeClass("template2").addClass("template1");
                    $(".template1").find(".contactFName").text("Contact 1 First Name");
                    $(".template1").find(".contactLName").text("Contact 1 Last Name");

                    var originalHtmlString = $(".template1").html();
                    newHtmlString = replaceHtml(originalHtmlString);
                    $(".template1").html(newHtmlString);

                    //to remember selected country when switching from contact2 to contact1
                    var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                    switch (selectedValue) {
                        case "Canada":
                            $(".template1").find("select option:contains('Canada')").prop('selected', true);
                            break;
                        case "USA":
                            $(".template1").find("select option:contains('USA')").prop('selected', true);
                            break;
                        case "Other":
                            $(".template1").find("select option:contains('Other')").prop('selected', true);
                            break;
                        case "":
                            $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                            break;
                    }
                }
                else {
                    contactSection.next().remove();
                    contactSection.remove();
                }
            }
            $('#btnAddContact').closest('div.row').show();
        }
        else { //for unregistered application
            contactSection.addClass("contactToBeDeleted"); //add a temporary class to remember the contact that to be removed
            $("#modalRemoveContactConfirmation").modal("show");

            var currentContactClassName = contactSection.attr("class");
            var index = currentContactClassName.indexOf("template");
            var value = currentContactClassName.charAt(index + 8);
            var msg = "Are you sure you want to remove Contact " + value + "?";
            $('#modalRemoveContactConfirmation').on('shown.bs.modal', function () {
                $("#contactRemovingMsg").text(msg);
            });
        }
    });

    //remove above temporarily added class when click "Cancel" button in Modal
    $('#modalRemoveContactConfirmation').on('hidden.bs.modal', function () {
        $(".contactToBeDeleted").removeClass("contactToBeDeleted");
    });

    $(document).on("click", "#modalRemoveContactYesBtn", function (evt) {

        var contactSection = $(".contactToBeDeleted");
        clearAllInput(contactSection);

        contactTemplateCount--;
        if (contactSection.parent().hasClass("ctemp")) {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");

                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

            }
            else {
                contactSection.parent().next().remove();
                contactSection.parent().remove();
            }
        }
        else {
            if (contactSection.hasClass("template1") && $(".template2").length > 0) {
                contactSection.next().remove();
                contactSection.remove();
                $(".template2").removeClass("template2").addClass("template1");
                $(".template1").find(".contactFName").text("Contact 1 First Name");
                $(".template1").find(".contactLName").text("Contact 1 Last Name");

                var originalHtmlString = $(".template1").html();
                newHtmlString = replaceHtml(originalHtmlString);
                $(".template1").html(newHtmlString);

                var selectedValue = $(".template1").find(".dropdown>input[type=hidden]").val();
                switch (selectedValue) {
                    case "Canada":
                        $(".template1").find("select option:contains('Canada')").prop('selected', true);
                        break;
                    case "USA":
                        $(".template1").find("select option:contains('USA')").prop('selected', true);
                        break;
                    case "Other":
                        $(".template1").find("select option:contains('Other')").prop('selected', true);
                        break;
                    case "":
                        $(".template1").find("select option:contains('Select Country')").prop('selected', true);
                        break;
                }

            }
            else {
                contactSection.next().remove();
                contactSection.remove();
            }
        }
        $('#btnAddContact').closest('div.row').show();
        processJson(); //to autosave
    });

    var clearAllInput = function (element) {
        element.find("input[type=text]").val("");
        element.find("input[type=checkbox]").prop("checked", false);
        element.find("select").find('option').removeAttr('selected');
        element.find("input[type=hidden]").val("0");
    }

    var replaceHtml = function (html) {
        var index = html.indexOf("ContactInformationList");
        var value = html.charAt(index + 23);
        var replaceValue = value - 1;
        var rp1 = "ContactInformationList_" + replaceValue;
        var rp2 = "ContactInformationList[" + replaceValue;
        var html = html.replace(/ContactInformationList_\d/g, rp1).replace(/ContactInformationList\[\d/g, rp2);
        return html;
    }
};

function SortYardDetails() {
    /* SORT YARD DETAILS */

    //disabled and uncheck COA checkbox
    $('.sort-yard-capacity-cb').on('click', function () {
        if (!$(this).is(':checked'))
            $('.sort-yard-COA-cb').attr('disabled', 'disabled').removeAttr('checked');
        else
            $('.sort-yard-COA-cb').removeAttr('disabled');
    });
    /* END SORT YARD DETAILS */

    var sortYardtemp = $(".sortyardtemp").val();

    if (sortYardtemp != undefined) {
        sortYardtemp = sortYardtemp.replace(/value=\".+\"/g, "value=''"); //OTSTM2-643 remove all default value of input
    }

    var sortYardTemplateCount = $('#sorYardCount').val();
    var maxStorageCap = 0;
    var currAddCount = 0;

    $('#SortYardCount').text("0");

    for (var i = 0; i < sortYardTemplateCount; i++) {
        var sortYardTemplate = $("#sortYardTemplate" + i).val();
        sortYardTemplate = "<div class='sortYardtemplate" + i + " parent'>" + sortYardTemplate + '</div><hr>';

        $(sortYardTemplate).appendTo('#fmSortYardDetails');

        if (i == 0)
            $('.sort-yard-address-disabled').attr('disabled', 'disabled');
    }
    $('#SortYardCount').text(sortYardTemplateCount);

    //$('body').on('click', '#btnAddSort', function () {
    $('#btnAddSort').on('click', function () {
        addSortRow();
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
        titleCase();
        $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, getSortYardDetailsTSYCSettings()));
    });

    var addSortRow = function () {
        currAddCount = sortYardTemplateCount++;
        sortYardtempDiv = ("<div class='ctemp'><div class='sortYardtemplate" + currAddCount + " parent'>" + sortYardtemp + '</div></div><hr>'); //OTSTM2-643 rename from sortYardtemp to sortYardtempDiv to make sure <hr> added and removed correctly
        //Replace the default values
        sortYardtempDiv = sortYardtempDiv.replace(/\[0]/g, '[' + currAddCount + ']').replace(/\_0__/g, '_' + currAddCount + '__');
        $(sortYardtempDiv).appendTo('#fmSortYardDetails');

        $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');
        $('#SortYardCount').text(sortYardTemplateCount); 

        //OTSTM2-643 add "required" validation for unregistered application (only for new added processing location), at the mean time, leave no validation for registered application as existing (0 can be added by system automatically for empty input after saving) 
        if (!Global.StaffIndex.Settings.IsApprovedApplication) { //for unregistered application
            $(".maxStorageCapacity").not(".dynamic-required").addClass("dynamic-required"); //add "dynamic-required" class if no "dynamic-required" class
        }

        for (var i = 0; i < sortYardTemplateCount; i++) {
            $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {
                if ((typeof $(this).attr('name') != 'undefined')) {
                    $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                }
                if ((typeof $(this).attr('id') != 'undefined')) {
                    $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
                }
            });
        }
    }

    var bindSortYardSelect = function () {
        $(".sortYardtemplate" + currAddCount + " select").each(function () {
            $(this).children().removeAttr('selected');
        });
    }
    var renameSortYardLabel = function () {
        var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
        for (var j = 0; j < label.length; j++) {
            var num = 1;
            $(label[j]).each(function () {
                var newLabel = $(this).text().replace(/\d+/, num++);
                $(this).text(newLabel);
            });
        }
    }

    //copies code from business location to sort yard
    $('.sortyard-address').on('focusout', function () {
        var attr = $(this).attr('data-sortyard');
        if (typeof attr !== typeof undefined && attr) {
            if ($("input[name*='" + attr + "']").length > 0)
                $("input[name*='" + attr + "']").val($(this).val());
            if ($("select[name*='" + attr + "']").length > 0) {
                $("select[name*='" + attr + "']").val($(this).val());
            }
        }
    }).on();

    $("#panelSortYardDetails").on('focusout', "input[data-maxstorage]", function () {
        $(this).attr('data-maxstorage', $(this).val());
    });

    var initSortYard = function () {
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
    }
    initSortYard();

    $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, getSortYardDetailsTSYCSettings()));

    $('#SortYardDetails_0__SortYardAddress_Country').val($('#BusinessLocation_BusinessAddress_Country').val());

    //OTSTM2-643 --start--
    $(document).on("click", ".btnRemoveSortYard", function (evt) {

        evt.stopImmediatePropagation();
        var sortYardSection = $(this).parent().parent();
        if (Global.StaffIndex.Settings.IsApprovedApplication) { //for registered application
            clearAllInput(sortYardSection);

            sortYardTemplateCount--;
            if (sortYardSection.parent().hasClass("ctemp")) { //if the removed sortyard is new added (without refresh page)
                if (sortYardSection.parent().nextAll("div").length > 0) { //if the removed sortyard is not the last one
                    //get current sort yard class name, for example 'sortYardtemplate3 parent'
                    var currentSortYardSectionClassName = sortYardSection.attr("class");
                    //get all followed sortyard
                    var allFollowdSiblings = sortYardSection.parent().nextAll("div");

                    //remove the current sortyard
                    sortYardSection.parent().next().remove(); //remove <hr>
                    sortYardSection.parent().remove();

                    //get the value followed by class name 'sortYardtemplate' of current removed sortyard
                    var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                    var value = parseInt(num);

                    //do id and name replacement(decrement) for the following sortyard, 
                    //for example, replace id, "SortYardDetails_3__SortYardAddress_AddressLine1" as "SortYardDetails_2__SortYardAddress_AddressLine1"
                    // or replace name, "SortYardDetails[3].SortYardAddress.AddressLine1" as "SortYardDetails[2].SortYardAddress.AddressLine1"
                    for (var i = 0; i < allFollowdSiblings.length; i++) {
                        var originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                        doTheReplacement(originalSortYardSection, value + i);
                    }
                }
                else { //if the removed sortyard is the last one, remove directly
                    sortYardSection.parent().next().remove();
                    sortYardSection.parent().remove();
                }
            }
            else { //for existing sortyard (not new added)
                if (sortYardSection.nextAll("div").length > 0) {//if the removed sortyard is not the last one

                    //get current sort yard class name, for example 'sortYardtemplate3 parent'
                    var currentSortYardSectionClassName = sortYardSection.attr("class");
                    var allFollowdSiblings = sortYardSection.nextAll("div");

                    //remove the current sortyard
                    sortYardSection.next().remove();
                    sortYardSection.remove();

                    //get the value followed by class name 'sortYardtemplate' of current removed sortyard
                    var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                    var value = parseInt(num);

                    for (var i = 0; i < allFollowdSiblings.length; i++) {
                        var originalSortYardSection = allFollowdSiblings[i];
                        if ($(originalSortYardSection).hasClass("ctemp")) { //if the followed sortyard is new added
                            originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                        }
                        doTheReplacement(originalSortYardSection, value + i);
                    }
                }
                else { //if the removed sortyard is the last one, remove directly
                    sortYardSection.next().remove();
                    sortYardSection.remove();
                }
            }
            totalStorage(); //update Maximum Capacity of Sort Yards            
            $('#SortYardCount').text(sortYardTemplateCount); //update Total # of Sort Yards
        }
        else { //for unregistered application
            sortYardSection.addClass("sortYardToBeDeleted"); //add a temporary class to remember the sortyard that to be removed
            $("#modalRemoveSortYardConfirmation").modal("show");

            var currentSortYardSectionClassName = sortYardSection.attr("class");
            var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
            var value = parseInt(num) + 1;

            var msg = "Are you sure you want to remove Processing Location " + value + "?";
            $('#modalRemoveSortYardConfirmation').on('shown.bs.modal', function () {
                $("#sortYardRemovingMsg").text(msg);
            });
        }
    });

    //remove above temporarily added class when click "Cancel" button in Modal
    $('#modalRemoveSortYardConfirmation').on('hidden.bs.modal', function () {
        $(".sortYardToBeDeleted").removeClass("sortYardToBeDeleted");
    });

    $(document).on("click", "#modalRemoveSortYardYesBtn", function (evt) {

        var sortYardSection = $(".sortYardToBeDeleted"); //get the removed sortyard
        clearAllInput(sortYardSection);

        sortYardTemplateCount--;
        if (sortYardSection.parent().hasClass("ctemp")) {
            if (sortYardSection.parent().nextAll("div").length > 0) {

                var currentSortYardSectionClassName = sortYardSection.attr("class");
                var allFollowdSiblings = sortYardSection.parent().nextAll("div");

                sortYardSection.parent().next().remove();
                sortYardSection.parent().remove();

                var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                var value = parseInt(num);

                for (var i = 0; i < allFollowdSiblings.length; i++) {
                    var originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                    doTheReplacement(originalSortYardSection, value + i);
                }
            }
            else {
                sortYardSection.parent().next().remove();
                sortYardSection.parent().remove();
            }
        }
        else {
            if (sortYardSection.nextAll("div").length > 0) {

                var currentSortYardSectionClassName = sortYardSection.attr("class");
                var allFollowdSiblings = sortYardSection.nextAll("div");

                sortYardSection.next().remove();
                sortYardSection.remove();

                var num = currentSortYardSectionClassName.match(/sortYardtemplate\d+/)[0].match(/\d+/)[0];
                var value = parseInt(num);

                for (var i = 0; i < allFollowdSiblings.length; i++) {
                    var originalSortYardSection = allFollowdSiblings[i];
                    if ($(originalSortYardSection).hasClass("ctemp")) {
                        originalSortYardSection = $(allFollowdSiblings[i]).children().first();
                    }
                    doTheReplacement(originalSortYardSection, value + i);
                }
            }
            else {
                sortYardSection.next().remove();
                sortYardSection.remove();
            }
        }
        processJson(); //to autosave
        totalStorage(); //update Maximum Capacity of Sort Yards         
        $('#SortYardCount').text(sortYardTemplateCount); //update Total # of Sort Yards
    });

    var clearAllInput = function (element) {
        element.find("input[type=text]").val("");
        element.find("input[type=checkbox]").prop("checked", false);
        element.find("select").find('option').removeAttr('selected');
        element.find("input[type=hidden]").val("0");
    }

    var doTheReplacement = function (originalSortYardSection, replaceValue) {

        //there is a difference of 1 between UI and corresponding id and name, for example, UI is "Processing Location 3 Address Line 1", but the id is "SortYardDetails_2__SortYardAddress_AddressLine1"
        var replaceValuePlusOne = parseInt(replaceValue) + 1;

        //replace id and name (name should be correct and in )
        var htmlString = $(originalSortYardSection).html();
        var rp1 = "SortYardDetails_" + replaceValue;
        var rp2 = "SortYardDetails[" + replaceValue;
        var htmlString = htmlString.replace(/SortYardDetails_\d+/g, rp1).replace(/SortYardDetails\[\d+/g, rp2);
        $(originalSortYardSection).html(htmlString);
        //replace class name
        $(originalSortYardSection).removeClass("sortYardtemplate" + replaceValuePlusOne).addClass("sortYardtemplate" + replaceValue);
        //replace UI
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-address1-label").text("Processing Location " + replaceValuePlusOne + " Address Line 1");
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-address2-label").text("Processing Location " + replaceValuePlusOne + " Address Line 2");
        $(".sortYardtemplate" + replaceValue).find(".sort-yard-capacity").text("Processing Location " + replaceValuePlusOne + " Capacity (tonnes)");
    }
    //OTSTM2-643 --end--
};

function TireDetails() {
    //code if necessary
    $('#cb_tiredetails').change(function () {
        if ($(this).is(":checked")) $('#cb_tiredetails').attr('checked', true);
        else $('#cb_tiredetails').attr('checked', false);
    });
};
function ProcessorDetails() {
    //applies to radiobutton for ProcessorDetails_CVORNumber
    $('.cvor').on('change', function () {
        DisplayCVORDocumentOption();
    });

    //applies to radiobutton for ProcessorDetails.ProcessorDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    $('#ProcessorDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });

    var readOnlyStr = String(typeof Global.StaffIndex.Model.IsReadOnly !== 'undefined' ? Global.StaffIndex.Model.IsReadOnly : "true");
    var readOnlyBool = readOnlyStr.toLowerCase() == "true";

    //security
    var calendarReadOnlyBool = Global.StaffIndex.Security.ProcessorDetailsCalendarReadonly.toLowerCase() == 'true';

    if (!calendarReadOnlyBool) {
        $('#dpBusinessStartDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#dpInsuranceExpiryDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#dpCVORExpiryDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    }

    var disabledHSTTextBox = function () {
        if (!($('#ProcessorDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#ProcessorDetails_IsTaxExempt').is(':checked'))
                $('#ProcessorDetails_CommercialLiabHstNumber').val('').attr('disabled', 'disabled');
            else
                $('#ProcessorDetails_CommercialLiabHstNumber').removeAttr('disabled');
        }
    }
    $('#ProcessorDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });

    $('.radio input.cvor').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlCVOR').toggle(show);
        if (!show) {
            $('#pnlCVOR').find('input').val('');
        }
    });

    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    //REFACTOR
    $('.checkbox input[type=checkbox]').on('change', function () {
        if ($(this).data('myattri') == 'Other~') {
            var show = $(this).prop('checked');
            $('#pnlOtherProd').toggle(show);
            if (!show) {
                $('#pnlOtherProd').find('textarea').val('');
            }
        }
    });

    $('.radio input[type=radio]').on('change', function () {
        if ($(this).attr('id') == 'TireDetails_RegistrantSubTypeID') {
            var show = false;

            if ($(this).val() == 4) {
                show = true;
            }
            $('#pnlOtherProcType').toggle(show);
            if (!show) {
                $('#pnlOtherProcType').find('textarea').val('');
            }
        }
    });
    //disable checkboxes for CVORAndExpiryDate
    var disableCVORAndExpiryDateCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#ProcessorDetails_cbCVORNumber').removeAttr('disabled');
            $('#ProcessorDetails_cbCVORExpiryDate').removeAttr('disabled');
        }
        else {
            $('#ProcessorDetails_cbCvorNumber').attr('disabled', 'disabled').removeAttr('checked');
            $('#ProcessorDetails_cbCvorExpiryDate').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    //disable checkboxes for WSIBNumber
    var disableWSIBNumberCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#ProcessorDetails_cbWsibNumber').removeAttr('disabled');
        }
        else {
            $('#ProcessorDetails_cbWsibNumber').attr('disabled', 'disabled');
        }
    }

    $('#ProcessorDetails_cbHIsGvwr').on('change', function () {
        disableCVORAndExpiryDateCheckbox(this);
    });

    $('#ProcessorDetails_cbHasMoreThanOneEmp').on('change', function () {
        disableWSIBNumberCheckbox(this);
    });

    disabledHSTTextBox();
    $('#pnlCVOR').toggle($('.radio input.cvor:checked').val() == 'True');
    $('#pnlWSIB').toggle($('.radio input.wsib:checked').val() == 'True');
};
function BankingInformation() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });

    $('#listOfStaff').empty().append('<option value="0" selected="selected"></option>');
    $("#modalAssignAssignBtn").attr("disabled", "disabled");

    $.ajax({
        url: '/System/Common/GetUsersInAuditGroup',
        method: 'GET',
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, data) {
                $('#listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')
            });
        },
        failure: function (data) {
        },
    });

    //Check whether all Bank Info validation check boxes are checked, if so, then change the application
    //status to completed
    $('.bank-info').on('click', function () {
        var allBankInfoChecked = true;

        $('.bank-info').each(function () {
            if (!$(this).is(":checked")) {
                allBankInfoChecked = false;
            }
        });

        if (allBankInfoChecked) {
            $.ajax({
                url: '/Processor/Registration/ChangeStatus',
                method: 'POST',
                dataType: "JSON",
                data: { applicationId: $('#applicationId').val(), status: 'Completed' },
                success: function (data) {
                    if (data == 'Success') {
                        window.location.reload();
                    }
                },
            });
        }
    });
};
function SupportingDocuments() {
    $("div #panelSupportingDocuments [type=checkbox]").each(function () {
        $(this).attr('name', $(this).attr('id'));
    });
};

function TermsAndConditions() {
    $('.isBankTransferNotficationPrimaryContact').click(function () {
        $(".bankTransferNotficationPrimaryContactDifferent").toggle(this.unchecked);
    });
};

function InitAutoSave() {
    //used to explicitly trigger save feature
    $('#hdnExplicitSave').on('click', function () {
        $('#AppProcessorFormID :input').eq(0).trigger('change');
    });

    //OTSTM2-50/643 adding for new added contact autosave
    //$(document).on("change", ".ctemp :input", function (evt) {
    $(document).on("change", "#AppProcessorFormID :input:not(#inactivedropdown):not(#activedropdown)", function (evt) {

        var IsProcessorAutoSaveDisabled = Global.StaffIndex.Security.IsProcessorAutoSaveDisabled.toLowerCase() == 'true';

        evt.stopImmediatePropagation(); //avoid being fired twice

        //ET-79
        var appProcessor = $(this);
        if (appProcessor.is(':checkbox')) {
            clearField(appProcessor);
        }

        //OTSTM2-50/643 save textbox, checkbox or select option value to the template, keep value for removing
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            if ((ctemp[0].name != "TireDetails.TireItem") && (ctemp[0].name!="TireDetails.TireItemProduct")) {
                var value = ctemp.is(':checked');
            }
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;
            }
            if ((ctemp[0].name != "TireDetails.TireItem") && (ctemp[0].name != "TireDetails.TireItemProduct")) {
                this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
                this.defaultChecked = value;
            }
        }
        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country") >= 0) {
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }

        $.when(
        ctemp.focusout()).then(function () {          
            switch (Global.StaffIndex.Settings.status) {
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
                case Global.StaffIndex.Settings.ApplicationStatus.Approved:
                case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                    break;
                default:
                    if (!IsProcessorAutoSaveDisabled) {
                        processJson();
                    }
                    else {
                        $('#modalSaveError').modal('toggle')
                    }
                    break;

            }
            contactAddressSameAs();
            totalStorage();  //update Maximum Capacity of Sort Yards
        });
    });

    //$('#AppProcessorFormID').on('change', ':input', function () {
    $(document).on("change", "#AppProcessorFormID :input:not(#inactivedropdown):not(#activedropdown)", function (evt) { //OTSTM2-50/643 change the "Change" event 

        evt.stopImmediatePropagation(); //OTSTM2-50 avoid firing twice (bubble event)

        var IsProcessorAutoSaveDisabled = Global.StaffIndex.Security.IsProcessorAutoSaveDisabled.toLowerCase() == 'true';

        var appProcessor = $(this);
        if (appProcessor.is(':checkbox')) {
            clearField(appProcessor);
        }

        //OTSTM2-50/643 save textbox, checkbox or select option value to the template, keep value for removing
        var ctemp = $(this);
        if (ctemp.attr('type') == "text") {
            var value = ctemp.val();
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
        }
        else if (ctemp.attr('type') == "checkbox") {
            var value = ctemp.is(':checked');
            if (ctemp.is("#ContactAddressSameAsBusinessAddress")) {
                //clear the following value when checking above checkbox
                var contactText = $(this).closest(".row").next().find("input[type=text]");
                for (var i = 0; i < contactText.length; i++) {
                    contactText[i].defaultValue = '';
                }
                var contactCheckbox = $(this).closest(".row").next().find("input[type=checkbox]");
                for (var i = 0; i < contactCheckbox.length; i++) {
                    contactCheckbox[i].defaultChecked = false;
                    contactCheckbox[i].checked = false;
                }
                var contactSelect = $(this).closest(".row").next().find("select").find("option");
                contactSelect[0].defaultSelected = true;
                contactSelect[1].defaultSelected = false;
                contactSelect[2].defaultSelected = false;
                contactSelect[3].defaultSelected = false;
            }
            this.defaultValue = value; //Use Javascript to change default value, jquery cannot do it 
            this.defaultChecked = value;
        }
        if (ctemp.is("select") && ctemp.attr("name").indexOf("Country") >= 0) {
            var selectedIndex = ctemp[0].selectedIndex;
            switch (selectedIndex) {
                case 0:
                    ctemp[0][0].value = "";
                    ctemp.find("option:contains('Select Country')")[0].defaultSelected = true;
                    break;
                case 1:
                    ctemp[0][1].value = "Canada";
                    ctemp.find("option:contains('Canada')")[0].defaultSelected = true;
                    break;
                case 2:
                    ctemp[0][2].value = "USA";
                    ctemp.find("option:contains('USA')")[0].defaultSelected = true;
                    break;
                case 3:
                    ctemp[0][3].value = "Other";
                    ctemp.find("option:contains('Other')")[0].defaultSelected = true;
                    break;
            }
        }

        $.when(
        appProcessor.focusout()).then(function () {
            switch (Global.StaffIndex.Settings.status) {
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
                case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
                case Global.StaffIndex.Settings.ApplicationStatus.Approved:
                case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                    break;
                default:
                    if (!IsProcessorAutoSaveDisabled) {
                        processJson();
                    }
                    else {
                        $('#modalSaveError').modal('toggle')
                    }
                    break;
            }
            totalStorage();
            contactAddressSameAs();
        });
    });

    //OTSTM2-50/643 comment out
    //$('#AppProcessorFormID').on('change', '.ctemp', function () {

    //    var IsProcessorAutoSaveDisabled = Global.StaffIndex.Security.IsProcessorAutoSaveDisabled.toLowerCase() == 'true';

    //    var ctemp = $(this);
    //    $.when(
    //    ctemp.focusout()).then(function () {
    //        switch (Global.StaffIndex.Settings.status) {
    //            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
    //            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
    //            case Global.StaffIndex.Settings.ApplicationStatus.Approved:
    //            case Global.StaffIndex.Settings.ApplicationStatus.Completed:
    //                break;
    //            default:
    //                if (!IsProcessorAutoSaveDisabled) {
    //                    processJson();
    //                }
    //                else {
    //                    $('#modalSaveError').modal('toggle')
    //                }
    //                break;

    //        }
    //        totalStorage();
    //        contactAddressSameAs();
    //    });
    //});
};

var processJson = function () {
    if ($('#status').val() != 'Approved') {
        var form = $('form');
        //Select the first Processor Location input elements, and remove their disable attribute
        var tmpEnabled = $('.sortYardtemplate0').find('input.sort-yard-address-disabled,select.sort-yard-address-disabled').removeAttr('disabled');
        var disabled = form.find(':input:disabled').removeAttr('disabled');

        var data = form.serializeArray();

        //Re-enable the first Processor Location input element disable attribute
        tmpEnabled = tmpEnabled.attr('disabled', 'disabled');
        disabled.attr('disabled', 'disabled');

        data = data.concat(
            $('#AppProcessorFormID input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": this.value }
                    }).get());

        data = data.concat(
            $('#AppProcessorFormID input[type=radio]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": null }
                    }).get());

        $.each($('input[type=checkbox]'), function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }
                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });
        $('#product input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'ProductsProduceTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });


        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }

        //remove leading and trailing space

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        var IsProcessorAutoSaveDisabled = Global.StaffIndex.Security.IsProcessorAutoSaveDisabled.toLowerCase() == 'true';

        if (!IsProcessorAutoSaveDisabled) {
            $.ajax({
                url: '/Processor/Registration/SaveModel',
                dataType: 'json',
                type: 'POST',
                data: $.param(data),
                success: function (result) {
                    if (result != 'Success')
                        console.log('Failuer in Auto Save: ' + result);
                },
                error: function (result) {
                }
            });
        }
    }
}

var totalStorage = function () {
    var inputs = $("input[data-maxstorage]");
    maxStorageCap = 0;
    inputs.each(function () {
        //if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
        //    maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        //}
        //OTSTM2-643 comment out above and apply the followed code
        if (!isNaN(parseFloat($(this).val()))) {
            maxStorageCap += parseFloat($(this).val());
        }
    });

    if (!isNaN(parseFloat(maxStorageCap))) {
        $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
    }
}

var contactAddressSameAs = function () {
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:checked').each(function () {
        //   CopyContactAddress();
        $(this).closest('.row').next('.contactAddressDifferent').hide();
        $(this).closest('.row').next('.contactAddressDifferent').find('input, select').val('');
    });
    $('input:checkbox[id^="ContactAddressSameAsBusinessAddress"]:not(:checked)').each(function () {
        $(this).closest('.row').next('.contactAddressDifferent').show();
    });
}

var handleBusinessAddressCheckboxForContact = function () {

    $('.ContactAddressSameAsBusinessAddress').each(function () {
        var isChecked = $(this).is(':checked');
        $(this).next('input[type="hidden"]').val(isChecked);
    });

}

/* COMMON FUNCTIONS */
function _isEmpty(value) {
    return (value == null || value.length === 0);
}
var restrictOnlyNumbers = function () {
    $(".only-numbers").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    });
}
function DisplayCVORDocumentOption() {
    var show = ($('#ProcessorDetails_HIsGvwr:checked').val() == 'True');
    $('#cVORDocument').toggle(show);
}
function DisplayWSIBDocumentOption() {
    var show = ($('#ProcessorDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
}
function DisplayHSTDocumentOption() {
    var show = !$('#ProcessorDetails_IsTaxExempt').is(':checked');
    $('#hSTDocument').toggle(show);
}

function titleCase() {
    $('.title-case').on('keypress', function () {
        var str = $(this).val();
        var r = str.match(/[A-Za-z]/gi);
        if (r) {
            var i = str.indexOf(r[0]);
            if (i > -1) {
                var titleCaseStr = str.substring(0, i) + str.substring(i, i + 1).toUpperCase() + str.substring(i + 1);
                $(this).val(titleCaseStr);
            }
        }
    });
}
var clearField = function (element) {
    if (!$(element).is(':checked')) {
        var nameAttr = $(element).attr('data-att-chkbox');
        if (nameAttr) {
            if ($('input[name="' + nameAttr + '"]').length > 0) {
                var control = $('input[name="' + nameAttr + '"]');

                if (control.attr('type') == 'radio') {
                    control.prop('checked', false);
                }
                else if (control.attr('type') == 'checkbox') {
                    control.prop('checked', false);
                }
                else {
                    control.val('');
                }
            }
            else if ($('select[name="' + nameAttr + '"]').length > 0) {
                $('select[name="' + nameAttr + '"]').find('option').removeAttr('selected');
            }
        }
    }
}
var bindSortYardSelect = function () {
    $(".sortYardtemplate" + currAddCount + " select").each(function () {
        $(this).children().removeAttr('selected');
    });
}
var renameSortYardLabel = function () {
    var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
    for (var j = 0; j < label.length; j++) {
        var num = 1;
        $(label[j]).each(function () {
            var newLabel = $(this).text().replace(/\d+/, num++);
            $(this).text(newLabel);
        });
    }
}
var addSortRow = function () {
    currAddCount = sortYardTemplateCount++;
    sortYardtemp = "<div class='ctemp'><div class='sortYardtemplate" + currAddCount + "'>" + sortYardtemp + '</div></div><hr>';
    //Replace the default values
    sortYardtemp = sortYardtemp.replace(/\[0]/g, '[' + currAddCount + ']').replace(/\_0__/g, '_' + currAddCount + '__');
    $(sortYardtemp).appendTo('#fmSortYardDetails');

    $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');

    for (var i = 0; i < sortYardTemplateCount; i++) {
        $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {
            if ((typeof $(this).attr('name') != 'undefined')) {
                $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
            }
            if ((typeof $(this).attr('id') != 'undefined')) {
                $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
            }
        });
    }
}
var disableInputIsNotStaff = function () {
    var isStaff = $('#isStaff').val();
    if (isStaff == 'False') {
        $.each($(':INPUT'), function () {
            if (!$(this).attr('disabled')) {
                $(this).attr('disabled', 'disabled');
            }
        });
        $('#txtGlobalSearch').removeAttr("disabled");
    }
};

function loadGroups() {
    $('#listOfVendorGroup').html("");
    var nowDate = new Date();
    var date = nowDate.getFullYear() + '/' + (nowDate.getMonth() + 1) + '/' + nowDate.getDate();

    $.ajax({
        url: '/System/Rates/LoadEffectiveVendorGroupList',
        method: 'GET',
        dataType: "JSON",
        data: { category: 'ProcessorGroup', effectiveDate: date },
        success: function (result) {
            if (result.data && result.data.length > 0) {
                $.each(result.data, function (index, data) {
                    var displayTxt = data.Name + ' ' + data.Description;
                    if (displayTxt.length > 60) {
                        displayTxt = displayTxt.substring(0, 55) + '...';
                    }
                    $('#listOfVendorGroup').append('<option value="' + data.ID + '">' + displayTxt + '</option>')
                });
                $('#listOfVendorGroup').prop('selectedIndex', 0);
                $('#modalApproveVendorGroupBtnYes').removeAttr('disabled');
                $('#listOfVendorGroup').attr('data-EffectiveRateGroupId', result.data[0].EffectiveRateGroupId);
            } else {//no effective group available
                $('#listOfVendorGroup').append($('<option/>', { value: 0, html: '<strong>cannot approve application, there is no payment group</strong>' }));
                $('#modalApproveVendorGroupBtnYes').attr('disabled', 'disabled');
            }
        },
        failure: function (data) {
        },
    });
}

/* END COMMON FUNCTIONS */

/* WORKFLOW CODE */
$(function () {
    $(window).load(function () {
        $(document).ready(function () {
            $('#modalDeny').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApplicant').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOnhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalOffhold').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalAssign').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalApproveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#approveWithSelectedVendorGroup').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalResend').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });
            $('#modalSaveApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#modalCancelApplication').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            //ActiveInactive

            $('#switchInactiveToActive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });

            $('#switchActiveToInactive').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });
        });
    });

    $('#modalSaveDBYesBtn').on('click', function () {

        handleBusinessAddressCheckboxForContact();

        var form = $('form');
        var disabled = form.find(':input:disabled').removeAttr('disabled'); //for saving when all fields are disabled

        var data = form.serializeArray();
        disabled.attr('disabled', 'disabled');

        data = data.concat(
            $('#AppProcessorFormID  input[type=checkbox]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": this.value }
                    }).get());

        data = data.concat(
            $('#ApplicationHaulerID input[type=radio]:not(:checked)').map(
                    function () {
                        return { "name": this.name, "value": null }
                    }).get());

        $('input[type="checkbox"]').each(function () {
            if ((typeof $(this).attr('data-att-chkbox') != 'undefined')) {
                if (!this.checked) {
                    if ($(this).attr('data-att-chkbox') == "TireDetails.HHasRelatioshipWithProcessor") {
                        data.push({ name: 'InvalidFormFields[]', value: 'TireDetails.HRelatedProcessor' });
                    }

                    data.push({ name: 'InvalidFormFields[]', value: $(this).attr('data-att-chkbox') });
                }
            }
        });

        $('#item_checkbox input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        $('#product input[type="checkbox"]').each(function () {
            if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
                data.push({ name: 'ProductsProduceTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
            }
        });

        if ($('#MailingAddressSameAsBusiness').is(':checked')) {
            data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
        }
        if ($('#status').val() != 'Approved') {
            data.push({ name: 'BankingInformation.BankName', value: $('#BankName').val() });
            data.push({ name: 'BankingInformation.BankNumber', value: $('#BankNumber').val() });
            data.push({ name: 'BankingInformation.AccountNumber', value: $('#AccountNumber').val() });
            data.push({ name: 'BankingInformation.TransitNumber', value: $('#TransitNumber').val() });
        }

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        $.ajax({
            url: '/Processor/Registration/SaveModelDB',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (data) {
                if (data.isValid) {
                    window.location.reload(true);
                }
                else {
                    $('#save-error-text').html(data);
                    $('#modalSaveError').modal('toggle');
                }
            },
            failure: function (data) {
            }
        });
    });

    var checkReviewCheckBoxForApproveAndDenyList = function () {
        //check if any same as mailing address for business location
        if ($('input.isMailingAddressDifferent').is(':checked')) {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.mailingAddressPanel input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //check if any same as mailing address for contact information
        if ($('input.isContactAddressSameAsBusinessAddress').is(':checked')) {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        } else {
            $('.contactAddressDifferent input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for each sort yard details COA Capacity tonnes whether certificate of approval appears
        $('.COA input[type=checkbox]').each(function () {
            if ($(this).closest('.COA').css('display') == "none") {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            } else {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            }
        });

        //for Processor details GVWR
        //if display is none and add that attribute
        if ($('#pnlCVOR').css('display') == "none") {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlCVOR input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }

        //for Processor details WSIB
        //if display is none and add that attribute
        if ($('#pnlWSIB').css('display') == "none") {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).attr('approve-validation', 'check');
                $(this).removeClass('validation-checkbox');
            });
        }
        else {
            $('#pnlWSIB input[type=checkbox]').each(function () {
                $(this).removeAttr('approve-validation');
                $(this).addClass('validation-checkbox');
            });
        }
    }

    var GetValidationErrorMessages = function () {
        var validationErrorMessages = [];
        checkReviewCheckBoxForApproveAndDenyList();
        var validationCheckBoxes = $('INPUT.validation-checkbox:checkbox:not(:checked)');
        $.each(validationCheckBoxes, function () {
            var validationMessage = $(this).data('validate-message');
            validationErrorMessages.push(validationMessage);
        });
        var additionalReasons = $('#modelDenyAdditionalReasonInput').val();
        if (additionalReasons.length != '' && additionalReasons.length > 0) {
            validationErrorMessages.push(additionalReasons);
        }

        var uploadedFilesValidationMessage = $('.upload-file-selection');
        //console.log(uploadedFilesValidationMessage);
        $.each(uploadedFilesValidationMessage, function () {
            var validationMessage = $(this).attr('data-uploadedFile-validation-message');
            if (validationMessage != undefined && validationMessage != '') {
                validationErrorMessages.push(validationMessage);
            }
        });

        return validationErrorMessages;
    }

    var reasonList;

    $('#modalDeny').on('shown.bs.modal', function () {
        $('#denyReasonsList').empty();
        $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
        reasonList = GetValidationErrorMessages();
        $.each(reasonList, function (index) {
            $('#denyReasonsList').append('<li>' + reasonList[index] + '</li>');
        });
        if (reasonList.length > 0) {
            $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
        } else {
            var comment = $.trim($('#modelDenyAdditionalReasonInput').val());
            if (comment.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            };
        };
    });

    $('#modelDenyAdditionalReasonInput').on('change', function () {
        if (reasonList.length <= 0) {
            if (this.value.length > 0) {
                $('#modalDenyDenyApplicationBtn').removeAttr('disabled');
            } else {
                $('#modalDenyDenyApplicationBtn').attr('disabled', 'disabled');
            };
        };
    });

    $('#listOfStaff').on('change', function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#modalAssignAssignBtn").removeAttr("disabled");
            $("#modalAssignAssignBtn").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#modalAssignAssignBtn").attr("disabled", "disabled");
        }
    });

    $('.workflow').on('click', function () {
        var status = $(this).attr('data-status');
        var userID = 0;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }
        if (status != "") {
            if (status.toLowerCase() == 'backtoapplicant') {
                //as per OTSTM-864 clear all fields that have their checkbox unchecked

                $('input[type="checkbox"]:not(:checked)').each(function () {
                    var checkBox = $(this);
                    clearField(checkBox);
                });

                $('#hdnExplicitSave').trigger('click');//saves data
            }
            //let model save first then change status
            setTimeout(function () {
                $.ajax({
                    url: '/Processor/Registration/ChangeStatus',
                    method: 'POST',
                    dataType: "JSON",
                    data: { applicationId: $('#applicationId').val(), status: status, userID: userID },
                    success: function (data) {
                        window.location.reload(true);
                    },
                });
            }, 1000);
        }
    });

    $('#modalApproveApproveBtn').on('click', function () {
        $.ajax({
            url: '/Processor/Registration/ApproveApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val() },
            success: function (data) {
                if (data.isValid) {
                    var url = Global.StaffIndex.Settings.afterApproveRedirectUrl + '?vId=' + data.vendorID;
                    window.location = url;
                }
            },
            failure: function (data) {
            }
        });
    });

    $('#modalApproveVendorGroupBtnYes').on('click', function () {//approve application with selected groupId
        $.ajax({
            url: '/Processor/Registration/ApproveApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val(), SelectedVendorGroupId: $('#listOfVendorGroup').val(), EffectiveRateGroupId: $('#listOfVendorGroup').attr('data-EffectiveRateGroupId') },
            success: function (data) {
                if (data.isValid) {
                    var url = Global.StaffIndex.Settings.afterApproveRedirectUrl + '?vId=' + data.vendorID
                    window.location = url;
                }
            },
            failure: function (data) {
            }
        });
    });

    //approve button when field checkbox not selected
    $('#approveBtn').on('click', function () {
        //check if any fields are checked
        $('#approveBtn').removeAttr("data-target");

        checkReviewCheckBoxForApproveAndDenyList();
        var isAllValidationChecksCompleted = true;

        $('input:checkbox.validation-checkbox.no-validate').each(function () {
            if (!this.checked && !this.hasAttribute('approve-validation')) {
                isAllValidationChecksCompleted = false;
            }
        });

        if (isAllValidationChecksCompleted &&
            $('#fmBusinessLocation').valid() &&
            $('#fmContactInfo').valid() &&
            $('#fmSortYardDetails').valid() &&
            $('#fmProcessorDetails').valid() &&
            $('#fmTermsAndConditions').valid() &&
            $('#fmTireDetails').valid()) {
            $(this).attr("data-target", "#approveWithSelectedVendorGroup");//modalApproveApplication
        }
        else {
            // alert('Validation error. One or more fields are not valid.');
            $('#modalApproveValidateWarning').modal('show');
        }
    });

    $('#saveBtn').on('click', function () {
        //check if any fields are checked
        $('#saveBtn').removeAttr("data-target");

        var IsProcessorAutoSaveDisabled = Global.StaffIndex.Security.IsProcessorAutoSaveDisabled.toLowerCase() == 'true';

        if (!IsProcessorAutoSaveDisabled) {
            if ($('#fmBusinessLocation').valid() &&
                $('#fmContactInfo').valid() &&
                $('#fmSortYardDetails').valid() &&
                $('#fmProcessorDetails').valid() &&
                $('#fmTermsAndConditions').valid() &&
                $('#fmTireDetails').valid()) {
                $(this).attr("data-target", "#modalSaveApplication");
            }
            else {
                //alert('Validation error. One or more fields are not valid.');
                $('#modalApproveValidateWarning').modal('show');
            }
        }
        else {
            $('#modalSaveError').modal('toggle')
        }
    });

    $('#modalDenyDenyApplicationBtn').on('click', function () {
        $.ajax({
            url: '/Processor/Registration/DenyApplication',
            method: 'POST',
            dataType: "JSON",
            data: { applicationId: $('#applicationId').val(), validationErrorMessages: GetValidationErrorMessages() },
            failure: function (data) {
            },
        });
    });

    $('#modalResendYesBtn').on('click', function () {

        //added
        switch (Global.StaffIndex.Settings.status) {
            case Global.StaffIndex.Settings.ApplicationStatus.Approved:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationSubmitted:
            case Global.StaffIndex.Settings.ApplicationStatus.BankInformationApproved:
            case Global.StaffIndex.Settings.ApplicationStatus.Completed:
                var url = '/Processor/Registration/ResendApproveRegistrant'
                var data = { vendorId: Global.StaffIndex.Settings.vendorID };

                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: "JSON",
                    data: data,
                    failure: function (data) {
                        if (data.isValid) {
                            window.location.reload(true);
                        }
                    },
                    success: function (data) {
                        if (data.isValid) {
                            window.location.reload(true);
                        }
                    },
                });

                break;
            default:
                break;
        }
    });
});

$('#approveWithSelectedVendorGroup').on('show.bs.modal', function (event) {
    loadGroups();
    var strUser = $("#listOfStaffRe-Assign option:selected").text() + ' ' + $("#listOfStaffRe-Assign option:selected").val();
    $("#Btn-Re-Assign").attr("data-assignuser", $("#listOfStaffRe-Assign option:selected").val());
});


/* END WORKFLOW CODE */

/* GABE STYLES */

(function () {
    // Business Location: Legal Business Name
    var businessLocationLBNContent = $('#businessLocationLBN').html(),
      businessLocationLBNSettings = {
          content: businessLocationLBNContent,
          width: 300,
          height: 100,
      };
    var popLargeLBN = $('.businessLocationLBN').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationLBNSettings));

    // Business Location: Business Operating Name
    var businessLocationBONContent = $('#businessLocationBON').html(),
      businessLocationBONSettings = {
          content: businessLocationBONContent,
          width: 300,
          height: 100,
      };
    var popLargeBON = $('.businessLocationBON').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, businessLocationBONSettings));

    // Sort Yard Details: All Sort Yards Capacity

    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
      sortYardDetailsASYCSettings = {
          content: sortYardDetailsASYCContent,
          width: 300,
          height: 90,
      };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, sortYardDetailsASYCSettings));

    // Sort Yard Details: This Sort Yard Capacity

    var sortYardDetailsTSYCContent = $('#sortYardDetailsTSYC').html(),

      sortYardDetailsTSYCSettings = {
          content: sortYardDetailsTSYCContent,
          width: 300,
          height: 90,
      };
    var popLargeTSYC = $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, sortYardDetailsTSYCSettings));

    var sortYardDetailsTSContent = $('#sortYardDetailsTS').html(),
     sortYardDetailsTSSettings = {
         content: sortYardDetailsTSContent,
         width: 300,
         height: 100,
     };
    var popLargeTSYC = $('.sortYardDetailsTS').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, sortYardDetailsTSSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
      tiredetailsPopoverPLTSettings = {
          content: tiredetailsPopoverPLTContent,
          width: 500,
          height: 600,
      };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
      tiredetailsPopoverMTSettings = {
          content: tiredetailsPopoverMTContent,
          width: 380,
          height: 160,
      };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
      tiredetailsPopoverAGLSSettings = {
          content: tiredetailsPopoverAGLSContent,
          width: 560,
          height: 230,
      };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverAGLSSettings));

    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
      tiredetailsPopoverINDSettings = {
          content: tiredetailsPopoverINDContent,
          width: 360,
          height: 120,
      };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverINDSettings));

    // Tire Details: Small Off the Road Tire (SOTR)
    var tiredetailsPopoverSOTRContent = $('#tiredetailsPopoverSOTR').html(),
      tiredetailsPopoverSOTRSettings = {
          content: tiredetailsPopoverSOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeSOTR = $('.tiredetailsPopoverSOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverSOTRSettings));

    // Tire Details: Medium Off the Road Tire (MOTR)
    var tiredetailsPopoverMOTRContent = $('#tiredetailsPopoverMOTR').html(),
      tiredetailsPopoverMOTRSettings = {
          content: tiredetailsPopoverMOTRContent,
          width: 430,
          height: 120,
      };
    var popLargeMOTR = $('.tiredetailsPopoverMOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverMOTRSettings));

    // Tire Details: Large Off the Road Tire (LOTR)
    var tiredetailsPopoverLOTRContent = $('#tiredetailsPopoverLOTR').html(),
      tiredetailsPopoverLOTRSettings = {
          content: tiredetailsPopoverLOTRContent,
          width: 420,
          height: 120,
      };
    var popLargeLOTR = $('.tiredetailsPopoverLOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverLOTRSettings));

    // Tire Details: Giant Off the Road Tire (GOTR)
    var tiredetailsPopoverGOTRContent = $('#tiredetailsPopoverGOTR').html(),
      tiredetailsPopoverGOTRSettings = {
          content: tiredetailsPopoverGOTRContent,
          width: 420,
          height: 100,
      };
    var popLargeGOTR = $('.tiredetailsPopoverGOTR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, tiredetailsPopoverGOTRSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBL').html(),
      supportingDocumentsAIMBLSettings = {
          content: supportingDocumentsAIMBLContent,
          width: 500,
          height: 140,
      };
    var popLargeAIMBL = $('.supportingDocumentsAIMBL').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLI').html(),
      supportingDocumentsCLISettings = {
          content: supportingDocumentsCLIContent,
          width: 500,
          height: 260,
      };
    var popLargeCLI = $('.supportingDocumentsCLI').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCLISettings));

    // Supporitng Documents: supportingDocumentsCFP
    var supportingDocumentsCFPContent = $('#supportingDocumentsCFP').html(),
    supportingDocumentsCFPSettings = {
        content: supportingDocumentsCFPContent,
        width: 500,
        height: 70,
    };
    var popLargeCLI = $('.supportingDocumentsCFP').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCFPSettings));

    // Supporitng Documents: supportingDocumentWSIB
    var supportingDocumentWSIBContent = $('#supportingDocumentWSIB').html(),
    supportingDocumentWSIBSettings = {
        content: supportingDocumentWSIBContent,
        width: 500,
        height: 86,
    };
    var popLargeCLI = $('.supportingDocumentWSIB').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentWSIBSettings));
    // Supporitng Documents: supportingDocumentsHST
    var supportingDocumentsHSTContent = $('#supportingDocumentsHST').html(),
    supportingDocumentsHSTSettings = {
        content: supportingDocumentsHSTContent,
        width: 500,
        height: 160,
    };
    var popLargeCLI = $('.supportingDocumentsHST').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsHSTSettings));

    // Supporitng Documents: CVOR Abstract Level II
    var supportingDocumentsCVORContent = $('#supportingDocumentsCVOR').html(),
        supportingDocumentsCVORSettings = {
            content: supportingDocumentsCVORContent,
            width: 460,
            height: 170,
        };
    var popLargeCVOR = $('.supportingDocumentsCVOR').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsCVORSettings));

    // Supporitng Documents: Processor Relationship Letter
    var supportingDocumentsPRLContent = $('#supportingDocumentsPRL').html(),
        supportingDocumentsPRLSettings = {
            content: supportingDocumentsPRLContent,
            width: 460,
            height: 105,
        };
    var popLargePRL = $('.supportingDocumentsPRL').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsPRLSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCH').html(),
        supportingDocumentsVOCHSettings = {
            content: supportingDocumentsVOCHContent,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.supportingDocumentsVOCH').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, supportingDocumentsVOCHSettings));

    // Popover: Participant Information
    var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
        popoverParticipantInfoSettings = {
            content: popoverParticipantInfoContent,
            width: 270,
        };
    var popLargeLBN = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, toolTipSettings, popoverParticipantInfoSettings));
})();

//OTSTM2-643 add "required" validation for unregistered application (only for existing procession location), at the mean time, leave no validation for registered application as existing (0 can be added by system automatically for empty input after saving)
$(document).ready(function () {
    $(window).load(function () {
        if (!Global.StaffIndex.Settings.IsApprovedApplication) { //for unregistered application
            $(".maxStorageCapacity").addClass("dynamic-required");
        }
    });
});
