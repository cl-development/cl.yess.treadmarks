﻿//AppFormProcessor js
$(function () {
    CommonValidation.initializeStandardFormValidations();

    var errorList = []; /* list of invalid Form Fields */
    var invalidList = []; //used for auto save feature
    var panelLevelCheckErrorList = []; //used for the panel check green tick box
    //var panel = $('#fmBusinessLocation');
    //var panelBusinessInfo = $('#fmBusinessLocation');
    //var panelTermsInfo = $('#fmTermsAndConditions');

    var status = $('#Status');
    var cbMailingAddressSameAsBusiness = $('#MailingAddressSameAsBusiness');
    var cbContactAddressSameAsBusinessAddress = $('#ContactAddressSameAsBusinessAddress');
    var cbProcessorDetailsProcessorDetailsVendorIsTaxExempt = $('#ProcessorDetails_CommercialLiabHSTNumber');
    var doHighlight = true;

    /* START GLOBAL VALIDATION METHODS */

    var dynamicErrorMsg = function (params, element) {
        var labelName = 'Field';
        if ($(element).prev('label').html()) {
            labelName = $(element).prev('label').html();
        }
        else if ($(element).parent('div.dropdown').prevAll('label').length > 0) {
            labelName = $(element).parent('div.dropdown').prevAll('label').html();
        }
        return 'Invalid ' + labelName;
    };

    //removes the green/red highlights from element
    var resetHighlight = function (element, isRequired) {
        $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
        $(element).next('span').removeClass('glyphicon-remove');
        $(element).nextAll('.help-block:first').remove();
        if (isRequired) {
            $(element).closest('.form-group').addClass('has-required');
            $(element).next('span').addClass('glyphicon-ok');
        }
    }

    //inInvalidList - if field is found in list, method highlights field and removes it from the errorList
    $.validator.addMethod('inInvalidList', function (value, element, param) {
        var propertyName = $(element).attr('name');
        var index = errorList.indexOf(propertyName);
        if (index > -1) {
            errorList.splice(index, 1);
            return false;
        }
        else
            return true;
    }, 'Invalid');

    $.validator.addMethod('lessThanOrEqualTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam <= today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('greaterThanTodayDate', function (value, element, param) {
        var today = new Date();
        var dateArray = value.split('-');
        if (dateArray) {
            var dateParam = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
            return dateParam > today;
        }
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod('dynamic-inInvalidList', function (value, element) {
        var propertyName = $(element).attr('name');
        var index = errorList.indexOf(propertyName);
        if (index > -1) {
            errorList.splice(index, 1);
            return false;
        }
        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-required", function (value, element) {
        if (value)
            return true;
        return false;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    $.validator.addMethod("dynamic-regex-phone", function (value, element) {
        var regex = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$|^$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, 'Expected format ###-###-####');

    $.validator.addMethod("dynamic-regex-postal", function (value, element) {
        var regex = /^[A-Za-z][0-9][A-Za-z]([ ]?)[0-9][A-Za-z][0-9]|([0-9]{5})(?:[- ][0-9]{4})?$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("dynamic-regex-email", function (value, element) {
        var regex = /^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$|^$/;
        var matches = value.match(regex);
        var result = false;
        if (matches)
            result = (matches[0] == value);
        return result;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });
    $.validator.addMethod("dynamic-whitespace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });
    $.validator.addMethod("TireDetailsRequired", function (value, element) {
        var radioStr = $(element).val();
        if (typeof radioStr == 'undefined') {
            return false;
        }
        var radioBool = JSON.parse(radioStr.toLowerCase());
        if (radioBool && $("select[name='TireDetails.HRelatedProcessor'] option:selected").index() == "0") {
            return false;
        }
        //value is no; thus return true
        return !radioBool;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("HRelatedProcessorRequired", function (value, element) {
        if (typeof $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'undefined') {
            return false;
        }
        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    /* END GLOBAL VALIDATION METHODS */

    /* START BUSINESS INFORMATION VALIDATION */

    var validatorBusinessInfo = $('#fmBusinessLocation').validate({
        ignore: '.no-validate, :hidden',
        onkeyup: function (element) {
            this.element(element);
        },
        // onfocusout: function (element) {
        // this.element(element);
        // },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'BusinessLocation.BusinessName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                inInvalidList: true
            },
            'BusinessLocation.OperatingName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.AddressLine1': {
                required: true,
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.City': {
                required: true,
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Province': {
                required: true,
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.AddressLine2': {
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Postal': {
                required: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Country': {
                required: true,
                inInvalidList: true
            },
            'BusinessLocation.Phone': {
                required: true,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                inInvalidList: true
            },
            'BusinessLocation.Extension': {
                required: false,
                inInvalidList: true
            },
            'BusinessLocation.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                inInvalidList: true
            },
            'BusinessLocation.MailingAddress.AddressLine1': {
                inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.City': {
                inInvalidList: true,
                required: function (element) {
                    return !cbMailingAddressSameAsBusiness.is(':checked');
                }
            },
            'BusinessLocation.MailingAddress.Province': {
                inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.AddressLine2': {
                required: false,
                inInvalidList: true
            },
            'BusinessLocation.MailingAddress.Postal': {
                inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.Country': {
                inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        messages: {
            'BusinessLocation.BusinessName': Global.ParticipantIndex.Resources.ValidationMsgInvalidBusinessName,
            'BusinessLocation.OperatingName': Global.ParticipantIndex.Resources.ValidationMsgInvalidOperatingName,
            'BusinessLocation.BusinessAddress.AddressLine1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'BusinessLocation.BusinessAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'BusinessLocation.BusinessAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'BusinessLocation.BusinessAddress.AddressLine2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'BusinessLocation.BusinessAddress.Postal': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'BusinessLocation.BusinessAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
            'BusinessLocation.Phone': {
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'BusinessLocation.Extension': Global.ParticipantIndex.Resources.ValidationMsgInvalidExt,
            'BusinessLocation.Email': Global.ParticipantIndex.Resources.ValidationMsgInvalidEmail,
            'BusinessLocation.MailingAddress.AddressLine1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'BusinessLocation.MailingAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'BusinessLocation.MailingAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'BusinessLocation.MailingAddress.AddressLine2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'BusinessLocation.MailingAddress.Postal': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'BusinessLocation.MailingAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
        }
    });

    /* END BUSINESS INFORMATION VALIDATION */

    /* CONTACT INFORMATION VALIDATION */

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onkeyup: function (element) {
            this.element(element);
        },
        // onfocusout: function (element) {
        // this.element(element);
        // },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        rules: {
            '': {
                required: true,
                regex: '^[A-Za-z0-9&-.,_ ]{1,}$',
                inInvalidList: true
            },

            'ContactInformation[0].ContactInformationContact.Name': {
                required: true,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.Position': {
                required: true,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.Ext': {
                required: false,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationAddress.AddressLine1': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false,
                inInvalidList: true
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                inInvalidList: true,
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': Global.ParticipantIndex.Resources.ValidationMsgInvalidPrimaryContactName,
            'ContactInformation[0].ContactInformationContact.Position': Global.ParticipantIndex.Resources.ValidationMsgInvalidPosition,
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Ext': Global.ParticipantIndex.Resources.ValidationMsgInvalidExt,
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'ContactInformation[0].ContactInformationContact.Email': Global.ParticipantIndex.Resources.ValidationMsgInvalidEmail,
            'ContactInformation[0].ContactInformationAddress.Address1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'ContactInformation[0].ContactInformationAddress.Address2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'ContactInformation[0].ContactInformationAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'ContactInformation[0].ContactInformationAddress.PostalCode': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'ContactInformation[0].ContactInformationAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'ContactInformation[0].ContactInformationAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
        }
    });

    /* END CONTACT INFORMATION VALIDATION */

    /* SORT YARD DETAILS */

    var validatorSortYardInfo = $('#fmSortYardDetails').validate({
        ignore: '.no-validate, :hidden',
        onkeyup: function (element) {
            this.element(element);
        },
        // onfocusout: function (element) {
        // this.element(element);
        // },
        submitHandler: function (form) {
            //return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.City': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Province': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Address2': {
                inInvalidList: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': {
                inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsAddress.Country': {
                inInvalidList: true,
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': {
                inInvalidList: true,
                regex: '^[0-9]{1,}$',
                required: true
            }
            ////'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': {
            ////    inInvalidList: true,
            ////    required: function (element) {
            ////        var txtMaxStorageCap = $('#SortYardDetails_0__SortYardDetailsVendorStorageSiteModel_MaxStorageCapacity');
            ////        var val = parseFloat(txtMaxStorageCap.val());
            ////        if (val)
            ////            return val >= 50;
            ////        else
            ////            return false;
            ////    }
            ////}
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                /*
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
                */
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'SortYardDetails[0].SortYardDetailsAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'SortYardDetails[0].SortYardDetailsAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'SortYardDetails[0].SortYardDetailsAddress.Address2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'SortYardDetails[0].SortYardDetailsAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': Global.ParticipantIndex.Resources.ValidationMsgInvalidCapacity

            ////'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidCertificateOfApproval,
        }
    });

    /* END SORT YARD DETAILS */

    /* Processor DETAILS */

    var validatorProcessorDetailsInfo = $('#fmProcessorDetails').validate({
        ignore: '.no-validate, :hidden, [readonly=readonly]',

        onkeyup: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('ProcessorDetails.CommercialLiabInsurerExpDate') > -1) {
                    $(element).closest('#dpInsuranceExpiryDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpInsuranceExpiryDate').next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('ProcessorDetails.BusinessStartDate') > -1) {
                    $(element).closest('#dpBusinessStartDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpBusinessStartDate').next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('ProcessorDetails.CvorExpiryDate') > -1) {
                    $(element).closest('#dpCVORExpiryDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpCVORExpiryDate').next('span').addClass('glyphicon-remove');
                }
            }
            else {
                //  $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                /// $(element).closest('.form-group').addClass('has-required');
            }

            //if radio buttons have nothing checked dont highlight
            if ($(element).attr('type') == "radio") {
                var val = "input:radio[name='" + $(element).attr('name') + "']";

                if (!$(val).is(":checked")) {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                    $(element).closest('.form-group').addClass('has-required');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');

                if (propertyName.indexOf('ProcessorDetails.CommercialLiabInsurerExpDate') > -1) {
                    $(element).closest('#dpInsuranceExpiryDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpInsuranceExpiryDate').next('span').addClass('glyphicon-ok');
                }
                if (propertyName.indexOf('ProcessorDetails.BusinessStartDate') > -1) {
                    $(element).closest('#dpBusinessStartDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpBusinessStartDate').next('span').addClass('glyphicon-ok');
                }
                if (propertyName.indexOf('ProcessorDetails.CvorExpiryDate') > -1) {
                    $(element).closest('#dpCVORExpiryDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpCVORExpiryDate').next('span').addClass('glyphicon-ok');
                }
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        rules: {
            'ProcessorDetails.BusinessStartDate': {
                required: true,
                lessThanOrEqualTodayDate: true,
                inInvalidList: true,
            },
            'ProcessorDetails.BusinessNumber': {
                required: true,
                inInvalidList: true,
            },
            'ProcessorDetails.CommercialLiabHstNumber': {
                inInvalidList: true,
                regex: '^[0-9]{9}$',
                required: function (element) {
                    //return !cbProcessorDetailsProcessorDetailsVendorIsTaxExempt.is(':checked');
                    return !$('#ProcessorDetails_IsTaxExempt').is(':checked');
                }
            },
            'ProcessorDetails.IsTaxExempt': {
                required: false
            },
            'ProcessorDetails.HasMoreThanOneEmp': {
                inInvalidList: true,
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#ProcessorDetails_WsibNumber'), true);
                    }
                    return true;
                }
            },
            'ProcessorDetails.HIsGVWR': {
                inInvalidList: true,
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#ProcessorDetails_CvorNumber'), true);
                        resetHighlight($('#ProcessorDetails_CvorExpiryDate'), true);
                    }
                    return true;
                }
            },
            'ProcessorDetails.CommercialLiabInsurerName': {
                required: true,
                inInvalidList: true,
            },
            'ProcessorDetails.CertificateOfApproval': {
                required: true,
                inInvalidList: true,
            },
            'ProcessorDetails.CommercialLiabInsurerExpDate': {
                required: true,
                greaterThanTodayDate: true,
                inInvalidList: true,
            },
            'ProcessorDetails.CvorNumber': {
                inInvalidList: true,
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR = $('#ProcessorDetails_HIsGvwr:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'ProcessorDetails.CvorExpiryDate': {
                greaterThanTodayDate: true,
                inInvalidList: true,
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR = $('#ProcessorDetails_HIsGvwr:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'ProcessorDetails.WsibNumber': {
                inInvalidList: true,
                regex: '^[0-9]{7}$',
                required: function (element) {
                    var rbtnProcessorDetailsProcessorDetailsVendorHasMoreThanOneEmp = $('#ProcessorDetails_HasMoreThanOneEmp:checked');
                    return rbtnProcessorDetailsProcessorDetailsVendorHasMoreThanOneEmp.val() == 'True';
                }
            },
        },
        messages: {
            'ProcessorDetails.BusinessStartDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidBusinessStartDate,
            'ProcessorDetails.BusinessNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidOntarioBusinessNumber,
            'ProcessorDetails.CommercialLiabHstNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidHSTRegistrationNumber,
            'ProcessorDetails.CommercialLiabInsurerName': Global.ParticipantIndex.Resources.ValidationMsgInvalidComercialLiabilityInsurance,
            'ProcessorDetails.CommercialLiabInsurerExpDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidExpiryDate,
            'ProcessorDetails.CvorExpiryDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidExpiryDate,
            'ProcessorDetails.CvorNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidCVORNumber,
            'ProcessorDetails.WsibNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidWSIPNumber,
            'ProcessorDetails.HIsGVWR': Global.ParticipantIndex.Resources.ValidationMsgHIsGVWR,
            'ProcessorDetails.HasMoreThanOneEmp': Global.ParticipantIndex.Resources.ValidationMsgHasMoreThanOneEmp,
            'ProcessorDetails.CertificateOfApproval': Global.ParticipantIndex.Resources.ValidationMsgInvalidCertificateOfApproval
        }
    });

    /* END Processor DETAILS */

    /* TERMS AND CONDITIONS */

    var validatorTermsInfo = $('#fmTermsAndConditions').validate({
        ignore: '',
        onkeyup: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
            //return false;
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TermsAndConditions.SigningAuthorityFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.SigningAuthorityLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.SigningAuthorityPosition': {
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByFirstName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByLastName': {
                //regex: '^[A-Za-z0-9_ ]{1,}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.FormCompletedByPhone': {
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
                required: true,
                inInvalidList: true
            },
            'TermsAndConditions.AgreementAcceptedByFullName': {
                inInvalidList: true,
                required: true,
                regex: '^([^a-z]*)([^a-z ]{2,})([ ]{1,}[^a-z ]+)*([ ]{1,}[^a-z ]{2,})+([^a-z]+)*$',
                //regex: '^([^a-z]{0,})*[^a-z ]{2,}[ ]{1,}([^a-z]{0,})*[^a-z ]{2,}([^a-z]{0,})*$'
            },
            'TermsAndConditions.AgreementAcceptedCheck': {
                inInvalidList: true,
                required: true
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).attr('data-signature')) {
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-required');
                    if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
                        $(element).closest('.termsandconditions-signature').removeClass('has-success');
                    $(element).closest('.termsandconditions-signature').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                else {
                    if ($(element).closest('.form-group').hasClass('has-required'))
                        $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
            else {
                //$(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                // $(element).closest('.form-group').addClass('has-required');
            }

            //if (doHighlight) {
            //    if ($(element).attr('data-signature')) {
            //        if ($(element).closest('.termsandconditions-signature').hasClass('has-required'))
            //            $(element).closest('.termsandconditions-signature').removeClass('has-required');
            //        if ($(element).closest('.termsandconditions-signature').hasClass('has-success'))
            //            $(element).closest('.termsandconditions-signature').removeClass('has-success');
            //        $(element).closest('.termsandconditions-signature').addClass('has-error');
            //        if ($(element).next('span').hasClass('glyphicon-ok'))
            //            $(element).next('span').removeClass('glyphicon-ok');
            //        $(element).next('span').addClass('glyphicon-remove');
            //    }
            //    else {
            //        if ($(element).closest('.form-group').hasClass('has-required'))
            //            $(element).closest('.form-group').removeClass('has-required');
            //        $(element).closest('.form-group').addClass('has-error');
            //        if ($(element).next('span').hasClass('glyphicon-ok'))
            //            $(element).next('span').removeClass('glyphicon-ok');
            //        $(element).next('span').addClass('glyphicon-remove');
            //    }
            //}
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).attr('data-signature')) {
                    $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
                else {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                    $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
                }
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }

            //if (doHighlight) {
            //    if ($(element).attr('data-signature')) {
            //        $(element).closest('.termsandconditions-signature').removeClass('has-error').removeClass('has-required').addClass('has-success');
            //        $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            //    }
            //    else {
            //        $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
            //        $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            //    }
            //}
        },
        success: function (error) {
            error.remove();
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'TermsAndConditions.SigningAuthorityFirstName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.SigningAuthorityLastName': Global.ParticipantIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.SigningAuthorityPosition': Global.ParticipantIndex.Resources.ValidationMsgInvalidPosition,
            'TermsAndConditions.FormCompletedByFirstName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFirstName,
            'TermsAndConditions.FormCompletedByLastName': Global.ParticipantIndex.Resources.ValidationMsgInvalidLastName,
            'TermsAndConditions.FormCompletedByPhone': {
                required: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
                regex: Global.ParticipantIndex.Resources.ValidationMsgExpectedFormat,
                inInvalidList: Global.ParticipantIndex.Resources.ValidationMsgInvalidPhone,
            },
            'TermsAndConditions.AgreementAcceptedByFullName': Global.ParticipantIndex.Resources.ValidationMsgInvalidFullName,
            'TermsAndConditions.AgreementAcceptedCheck': Global.ParticipantIndex.Resources.ValidationMsgMustBeCheckedForSubmission,
        }
    });

    /* END TERMS AND CONDITIONS */

    /* AUTOSAVE FEATURE */

    /************************************************************************************************************
    ******************************* TIRE DETAILS Participant START  ********************************************
    ************************************************************************************************************/

    var validatorTireDetailsInfo = $('#fmTireDetails').validate({
        ignore: '.no-validate, :hidden',
        onkeyup: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#item_checkbox'));
                }
                else if (element.attr("type") == "radio") {
                    return false;
                }
                else {
                    ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'))
                }
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TireDetails.TireItem': {
                inInvalidList: true,
                required: true
            },
            'TireDetails.TireItemProduct': {
                inInvalidList: true,
                required: true
            },
            'TireDetails.OtherProcessProduct': {
                required: {
                    depends: function (element) {
                        return ($("input[id=chb_30]").prop('checked'));
                    }
                },
            },
            'TireDetails.RegistrantSubTypeID': {
                inInvalidList: true,
                required: true
            },
            'TireDetails.OtherRegistrantSubType': {
                required: {
                    depends: function (element) {
                        return ($("input[id=TireDetails_RegistrantSubTypeID]:checked").val() == 4);
                    }
                },
            },
            'TireDetails.HHasRelatioshipWithProcessor': {
                inInvalidList: true,
                required: true,
                TireDetailsRequired: true
            },
            'TireDetails.HRelatedProcessor': {
                inInvalidList: true,
                required: {
                    depends: function (element) {
                        return $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == "True";
                    }
                },
                HRelatedProcessorRequired: true
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if (index > -1) {
                    if ($(element).attr('name') == 'TireDetails.TireItem') {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }
                    if ($(element).attr('name') == 'TireDetails.TireItemProduct') {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }
                    if ($(element).attr('name') == 'TireDetails.RegistrantSubTypeID') {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }
                }
                else {
                    //only highlight if any is not empty

                    $('#item_checkbox :input').each(function () {
                        if (!$(this).is(':checked')) {
                            if ($(element).attr('data-tiredetails')) {
                                if ($(element).closest('.form-group').hasClass('has-success') || $(element).closest('.form-group').hasClass('has-error'))
                                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                            }
                        }
                    });
                    $('#Processortype :input').each(function () {
                        if (!$(this).is(':checked')) {
                            if ($(element).attr('data-tiredetails')) {
                                if ($(element).closest('.form-group').hasClass('has-success') || $(element).closest('.form-group').hasClass('has-error'))
                                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                            }
                        }
                    });

                    $('#product :input').each(function () {
                        if (!$(this).is(':checked')) {
                            if ($(element).attr('data-tiredetails')) {
                                if ($(element).closest('.form-group').hasClass('has-success') || $(element).closest('.form-group').hasClass('has-error'))
                                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                            }
                        }
                    });
                }
            }

            if (($(element).attr('name') == 'TireDetails.RegistrantSubTypeID' && $("input[id=TireDetails_RegistrantSubTypeID]:checked").val() == '') || ($(element).attr('name') == 'TireDetails.OtherRegistrantSubType' && $("input[id=TireDetails_RegistrantSubTypeID]:checked").val() == 4 && $(element).val() == '')) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            }

            if ($(element).attr('name') == 'TireDetails.OtherProcessProduct' && $("input[id='chb_30']").prop('checked') && $(element).val() == '') {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (index > -1) {
                if ($(element).attr('name') == 'TireDetails.TireItem') {
                    $(element).closest('.form-group').css('border', 'none');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }

                if ($(element).attr('name') != 'TireDetails.RegistrantSubTypeID' && $(element).attr('name') != 'TireDetails.OtherProcessProduct' && $(element).attr('type') == "radio") {
                    var val = "input:radio[name='" + $(element).attr('name') + "']";

                    if ($(element).closest('.form-group').hasClass('has-error'))
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            }
            else {
                //unhighlight if any is not empty
                $('#item_checkbox :input').each(function () {
                    if (!$(this).is(':checked')) {
                        if ($(element).attr('data-tiredetails')) {
                            $(element).closest('.form-group').css('border', 'none');

                            if ($(element).closest('.form-group').hasClass('has-error') || $(element).closest('.form-group').hasClass('has-required')) {
                                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                            }
                        }
                    }
                });
                $('#product :input').each(function () {
                    if (!$(this).is(':checked')) {
                        if ($(element).attr('data-tiredetails')) {
                            $(element).closest('.form-group').css('border', 'none');

                            if ($(element).closest('.form-group').hasClass('has-error') || $(element).closest('.form-group').hasClass('has-required')) {
                                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                            }
                        }
                    }
                });
            }

            if ($(element).attr('name') == 'TireDetails.RegistrantSubTypeID' && $("input[id=TireDetails_RegistrantSubTypeID]:checked").val() != '' && $("input[id=TireDetails_RegistrantSubTypeID]:checked").val() != 4) {
                $(element).closest('.form-group').css('border', 'none');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }

            if ($(element).attr('name') == 'TireDetails.OtherRegistrantSubType' && $("input[id=TireDetails_RegistrantSubTypeID]:checked").val() == 4 && $(element).val() != '') {
                $(element).closest('.form-group').css('border', 'none');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }

            if ($("input[name='TireDetails.TireItemProduct']").is(":checked")) {
                if ($("input[id='chb_30']").prop('checked')) {
                    if ($(element).attr('name') == 'TireDetails.OtherProcessProduct' && $(element).val() != '') {
                        $(element).closest('.form-group').css('border', 'none');
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                } else {
                    $(element).closest('.form-group').css('border', 'none');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failed
        },
        messages: {
            'TireDetails.TireItem': Global.ParticipantIndex.Resources.ValidationMsgInvalidTireType,
            'TireDetails.HRelatedProcessor': Global.ParticipantIndex.Resources.ValidationMsgInvalidProcessor,
            'TireDetails.HHasRelatioshipWithProcessor': Global.ParticipantIndex.Resources.ValidationMsgHasRelatioshipWithProcessor
        }
    });

    /*END TIRE DETAIL VALIDATION */

    /* SUPPORTING DOCUMENT VALIDATION */

    var validatorSupportingDocInfo = $('#fmSupportingDoc').validate({
        ignore: '.no-validate, [readonly=readonly]',
        /*
        onfocusout: function (element) {
            this.element(element);
        },
		*/
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if ($(element).attr('name') == 'SupportingDocuments.RequiredDocuments') {
                    $(element).closest('.form-group').removeClass('has-success').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if ($(element).attr('name') == 'SupportingDocuments.RequiredDocuments') {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-success');
                }
            }
        },

        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        rules: {
            'SupportingDocuments.RequiredDocuments': {
                inInvalidList: true
            }
        },
        messages: {
            'SupportingDocuments.RequiredDocuments': "Invalid Required Documents"
        }
    });

    /* END SUPPORTING DOCUMENT VALIDATION */

    /* HIGHLIGHTING OF FIELDS */

    //triggers validations
    var highlightFields = function () {
        var tmp = errorList.slice();
        for (var i = 0; i < tmp.length; i++) {
            if ($('input[name*=\'' + tmp[i] + '\']').length > 0 || $('select[name*=\'' + tmp[i] + '\']').length > 0) {
                var element = $('input[name*=\'' + tmp[i] + '\']');

                if (element.length == 0) {
                    element = $('select[name*=\'' + tmp[i] + '\']');
                }
                var pnl = element.closest('.panel-collapse');
                if (pnl)
                    pnl.collapse();

                if (!(element.val().length > 0) || element.is(":checkbox") || element.is(":radio")) {
                    element.valid();
                }
                else if (element.attr('type') == 'radio') {
                    element.valid();
                }
                //remove item from list
                var propertyName = $(element).attr('name');
                var index = errorList.indexOf(propertyName);
                if (index > -1)
                    errorList.splice(index, 1);
            }
        }
    }
    /* END HIGHLIGHTING OF FIELDS */

    /*Green Check Mark Validation Start */

    /*Green Check Mark Validation End */

    /* INITIALIZATION */

    var init = function () {
        var statusStr = String(Global.ParticipantIndex.Model.Status);
        if (Global.ParticipantIndex.Model.InvalidFormFields && statusStr.toLowerCase() == 'backtoapplicant') {
            errorList = Global.ParticipantIndex.Model.InvalidFormFields.slice();//obj.InvalidFormFields.toString().split(",");
            //invalidList = Global.ParticipantIndex.Model.InvalidFormFields;//obj.InvalidFormFields.toString().split(",");

            //this error list to check green panel level checks
            panelLevelCheckErrorList = Global.ParticipantIndex.Model.InvalidFormFields.slice();

            highlightFields();
        }
        PanelBusinessLocationGreenCheck();
        PanelContactInfoGreenCheck();
        AddButtonHandlerForGreenCheck();
        PanelSortYardGreenCheck();
        PanelTireDetailsGreenCheck();
        PanelProcessorDetailsGreenCheck();
        PanelSupportingDocumentGreenCheck();
        PanelProcessorTermsAndConditions();
        ApplicationCommon.SubmitButtonHandler();
        DisableAddingNewDynamicPanel();
    }
    init();

    /* END HIGHLIGHTING OF FIELDS */
});

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}


function PanelBusinessLocationGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelBusinessLocation').removeClass('collapse');
    $('#panelBusinessLocation').addClass('collapse-in');

    $("#fmBusinessLocation").valid();

    if ($("#fmBusinessLocation").valid()) {
        $('#flagBusinessLocation').attr('src', statuses[1]);
    }
    else {
        $('#flagBusinessLocation').attr('src', statuses[0]);
    }

    $('#fmBusinessLocation').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($(this).hasClass("isMailingAddressDifferent") && !$(this).is(":checked")) {
                    $('#flagBusinessLocation').attr('src', statuses[0]);
                }
                else {
                    if ($("#fmBusinessLocation").valid()) {
                        $('#flagBusinessLocation').attr('src', statuses[1]);
                    }
                    else {
                        $('#flagBusinessLocation').attr('src', statuses[0]);
                    }
                }
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelContactInfoGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelContactInformation').removeClass('collapse');
    $('#panelContactInformation').addClass('collapse-in');

    var isValid = $("#fmContactInfo").valid();

    if (isValid) {
        $('#flagContactInformation').attr('src', statuses[1]);
    }
    else {
        $('#flagContactInformation').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmContactInfo').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($(this).hasClass("isContactAddressSameAsBusinessAddress") && !$(this).is(":checked")) {
                    $('#flagContactInformation').attr('src', statuses[0]);
                }
                else {
                    if ($("#fmContactInfo").valid()) {
                        $('#flagContactInformation').attr('src', statuses[1]);
                    }
                    else {
                        $('#flagContactInformation').attr('src', statuses[0]);
                    }
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();

                $("#fmContactInfo").valid();
            });
        });
    })
}

function PanelSortYardGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelSortYardDetails').removeClass('collapse');
    $('#panelSortYardDetails').addClass('collapse-in');

    var isValid = $("#fmSortYardDetails").valid();

    if (isValid) {
        $('#flagSortYard').attr('src', statuses[1]);
    }
    else {
        $('#flagSortYard').attr('src', statuses[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmSortYardDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmSortYardDetails").valid()) {
                    $('#flagSortYard').attr('src', statuses[1]);
                }
                else {
                    $('#flagSortYard').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelTireDetailsGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelTireDetails').removeClass('collapse');
    $('#panelTireDetails').addClass('collapse-in');

    var isValid = $("#fmTireDetails").valid();

    if (isValid) {
        $('#flagTireDetails').attr('src', statuses[1]);
    }
    else {
        $('#flagTireDetails').attr('src', statuses[0]);
    }

    $('#fmTireDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                isValid = $("#fmTireDetails").valid();
                if (isValid) {
                    $('#flagTireDetails').attr('src', statuses[1]);
                }
                else {
                    $('#flagTireDetails').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelProcessorDetailsGreenCheck() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelProcessorDetails').removeClass('collapse');
    $('#panelProcessorDetails').addClass('collapse-in');

    ProcessorDetailEnableReadOnly(false);

    var isValid = $("#fmProcessorDetails").valid();
    $('#flagProcessorDetails').attr('src', isValid ? statuses[1] : statuses[0]);

    ProcessorDetailEnableReadOnly(true);

    ///any changes to the Processor info form is handled here
    $('#fmProcessorDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                ProcessorDetailEnableReadOnly(false);

                if ($('#ProcessorDetails_IsTaxExempt').is(':checked')) {
                    $('#flagProcessorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#ProcessorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#ProcessorDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
                }
                else {
                    $('#flagProcessorDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#ProcessorDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#ProcessorDetails_CommercialLiabHstNumber').attr('readonly', false);
                }

                isValid = $("#fmProcessorDetails").valid();
                $('#flagProcessorDetails').attr('src', isValid ? statuses[1] : statuses[0]);
                ProcessorDetailEnableReadOnly(true);
                $("#fmProcessorDetails").valid();

                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelSupportingDocumentGreenCheck() {
    $('#panelSupportingDocuments').removeClass('collapse');
    $('#panelSupportingDocuments').addClass('collapse-in');
}

function PanelProcessorTermsAndConditions() {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

    $('#panelsTermsAndConditions').removeClass('collapse');
    $('#panelsTermsAndConditions').addClass('collapse-in');

    $("#fmTermsAndConditions").valid();

    if ($("#fmTermsAndConditions").valid()) {
        $('#flagTermsAndConditions').attr('src', statuses[1]);
    }
    else {
        $('#flagTermsAndConditions').attr('src', statuses[0]);
    }

    //any changes to the TermsAndConditions info form is handled here
    $('#fmTermsAndConditions').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmTermsAndConditions").valid()) {
                    $('#flagTermsAndConditions').attr('src', statuses[1]);
                }
                else {
                    $('#flagTermsAndConditions').attr('src', statuses[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function ProcessorDetailEnableReadOnly(flag) {
    //remove readonly methods in Processor details
    $('#ProcessorDetails_BusinessStartDate').attr("readonly", flag);
    $('#ProcessorDetails_CommercialLiabInsurerExpDate').attr("readonly", flag);
    $('#ProcessorDetails_CvorExpiryDate').attr("readonly", flag);
}

function AddButtonHandlerForGreenCheck() {
    $('#btnAddContact').on('click', function () {
        PanelContactInfoGreenCheck();
        ApplicationCommon.SubmitButtonHandler();
    });

    $('#btnAddSort').on('click', function () {
        PanelSortYardGreenCheck();
        ApplicationCommon.SubmitButtonHandler();
    });
}

function DisableAddingNewDynamicPanel() {
    //Contact
    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant' && !$('#btnAddContact').attr('disabled')) {
        $('#btnAddContact').attr('disabled', 'disabled');
    }

    //Yard
    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant' && !$('#btnAddSort').attr('disabled')) {
        $('#btnAddSort').attr('disabled', 'disabled');
    }
}