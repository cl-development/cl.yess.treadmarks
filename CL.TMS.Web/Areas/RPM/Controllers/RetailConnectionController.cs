﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RPMServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Helper;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MicrosoftIO = System.IO;

namespace CL.TMS.Web.Areas.RPM.Controllers
{
    public class RetailConnectionController : BaseController //Test
    {
        private IRetailConnectionService retailConnectionService;
        private string uploadRepositoryPath;

        public RetailConnectionController(IRetailConnectionService retailConnectionService)
        {
            this.retailConnectionService = retailConnectionService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.RetailConnection)]
        public ActionResult Index()
        {
            ViewBag.ngApp = "retailConnectionApp";
            ViewBag.IsEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RetailConnectionAllRetailConnection) == Security.SecurityResultType.EditSave);

            ViewBag.IsAllRetailersEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RetailConnectionAllRetailConnectionAllRetailers) == Security.SecurityResultType.EditSave);
            ViewBag.IsAllProductsEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RetailConnectionAllRetailConnectionAllProducts) == Security.SecurityResultType.EditSave);
            ViewBag.IsProductCategoriesEditable = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.RetailConnectionAllRetailConnectionProductCategories) == Security.SecurityResultType.EditSave);

            ViewBag.IsCategoryVisible = true;
            ViewBag.SelectedMenu = "RetailConnection";
            if (SecurityContextHelper.IsStaff())
            {
                if (SecurityContextHelper.CurrentVendor != null)
                {
                    //after global search
                    ViewBag.IsEditable = false;
                    ViewBag.IsCategoryVisible = false;
                }

                return View("StaffIndex");
            }
            else
            {
                //Participant side
                var currentVendor = SecurityContextHelper.CurrentVendor;
                if (currentVendor != null)
                {
                    if (currentVendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
                    {
                        ViewBag.NoActiveRetailerExists = retailConnectionService.LoadParticipantRetailer().Count == 0;
                        return View("ParticipantIndex");
                    }
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }
        }

        #region Participant Retailers
        [HttpPost]
        public ActionResult EditParticipantRetailerForm(RetailersFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            if (string.IsNullOrWhiteSpace(model.City))
                model.City = "";

            //call service method TO-DO
            retailConnectionService.EditRetailer(model);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult CheckRetailerName(string retailerName)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var find = retailConnectionService.CheckRetailerName(retailerName);
            if (find)
            {
                return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
            }
            return Json(new { status = false, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult AddParticipantRetailerForm(RetailersFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            retailConnectionService.AddRetailer(model, true);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }
        [HttpPost]
        public JsonResult LoadAllParticipantRetailers(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
                        {
                            {0, "ApprovedDate"},
                            {1, "Status"},
                            {2, "RetailerName"},
                            {3, "AddedBy"}
                        };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = retailConnectionService.LoadParticipantRetailer(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult InitializeRetailerFormData()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            RetailersFormViewModel formModel = new RetailersFormViewModel();
            formModel.CountryList.AddRange(DataLoader.GetCountriesByIso3166());

            return new NewtonSoftJsonResult(new { status = true, result = formModel, statusMsg = MessageResource.ValidData }, false);
        }

        [HttpPost]
        public ActionResult GetRetailerEditFormData(int retailerId)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var formModel = retailConnectionService.LoadRetailerById(retailerId);

            formModel.RetailerNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            formModel.CountryList.AddRange(DataLoader.GetCountriesByIso3166());

            return Json(new { status = true, result = formModel, statusMsg = MessageResource.ValidData });
        }

        #endregion

        #region Products
        [HttpPost]
        public ActionResult EditProductForm(ProductsFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            retailConnectionService.EditProduct(model);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult EditStaffProductForm(ProductsFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            retailConnectionService.EditStaffProduct(model);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult AddProductForm(ProductsFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            //call service method TO-DO
            if (SecurityContextHelper.CurrentVendor != null)
                model.VendorId = SecurityContextHelper.CurrentVendor.VendorId;

            retailConnectionService.AddProduct(model);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpGet]
        public ActionResult InitializeProductFormData()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            ProductsFormViewModel formModel = new ProductsFormViewModel();

            return new NewtonSoftJsonResult(new { status = true, result = formModel, statusMsg = MessageResource.ValidData }, false);
        }

        [HttpPost]
        public ActionResult GetProductEditFormData(int productId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var formModel = retailConnectionService.GetProduct(productId);

            if (formModel.UploadDocument.Url == null)
            {
                formModel.UploadDocument = new RcProductFileUploadModel()
                {
                    Url = "/Images/blank-image.png",
                    IsOTSImage = true,
                    IsDefaultImage = true,
                    Name = "Default photo",
                    Description = "",
                    Size = 31280,
                    Type = "image/jpeg"

                };
            }

            return new NewtonSoftJsonResult(new { status = true, result = formModel, statusMsg = MessageResource.ValidData }, false);
        }

        [HttpPost]
        public JsonResult LoadAllParticipantProducts(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ApprovedDate"},
                {1, "Status"},
                {2, "ProductName"},
                {3, "BrandName"},
                {4, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = retailConnectionService.LoadParticipantProducts(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection,
                //existsActiveRetailer = retailConnectionService.LoadParticipantRetailer(0, 1, null, null, "asc").DTOCollection.Count > 0,//exists any Active Retailer?
            };

            return Json(json, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult LoadProductCategories()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var result = retailConnectionService.LoadProductCategory();

            return new NewtonSoftJsonResult(new { status = true, result = result, statusMsg = MessageResource.ValidData }, false);

        }
        [HttpGet]
        public ActionResult LoadRetailersForProduct()
        {
            var result = retailConnectionService.LoadParticipantRetailer();
            return new NewtonSoftJsonResult(new { status = true, result = result, statusMsg = MessageResource.ValidData }, false);
        }
        [HttpPost]
        public JsonResult ApproveProduct(int productId)
        {
            retailConnectionService.ChangeProductStatus(productId, TreadMarksConstants.UnderReviewStatus, TreadMarksConstants.ActiveStatus);
            var emailInfor = retailConnectionService.GetEmailInforByProductId(productId);
            if (emailInfor != null && EmailContentHelper.IsEmailEnabled("RetailConnectionProductApproved"))
            {
                Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
                MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
                MailAddress to = new MailAddress(emailInfor.EmailAddress);
                MailMessage message = new MailMessage(from, to) { };

                var siteUrl = SiteSettings.Instance.GetSettingValue("Settings.DomainName");

                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.RCProductApproveEmailTemplate"));
                //string emailBody = MicrosoftIO.File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("RetailConnectionProductApproved");
                string subject = EmailContentHelper.GetSubjectByName("RetailConnectionProductApproved");
                
                emailBody = emailBody
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.ProductName, emailInfor.Name)
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.Product, "Product")
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.SiteUrl, siteUrl)
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = CollectorApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = "Product Approved";

                Task.Factory.StartNew(() =>
                {

                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailInfor.EmailAddress, null, null, subject, emailBody, null, null, alternateViewHTML);
                });
            }

            return Json(new { status = "OK" });
        }

        [HttpPost]
        public JsonResult RejectProduct(int productId)
        {
            retailConnectionService.ChangeProductStatus(productId, TreadMarksConstants.UnderReviewStatus, TreadMarksConstants.RejectedStatus);
            return Json(new { status = "OK" });
        }
        [HttpPost]
        public JsonResult ActiveProduct(int productId)
        {
            retailConnectionService.ChangeProductStatus(productId, TreadMarksConstants.InactiveStatus, TreadMarksConstants.ActiveStatus);
            return Json(new { status = "OK" });
        }
        [HttpPost]
        public JsonResult InactiveProduct(int productId)
        {
            retailConnectionService.ChangeProductStatus(productId, TreadMarksConstants.ActiveStatus, TreadMarksConstants.InactiveStatus);
            return Json(new { status = "OK" });
        }

        #endregion

        #region Staff Retailer

        public ActionResult LoadAllStaffRetailers(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ApprovedDate"},
                {1, "Status"},
                {2, "RetailerName"},
                {3, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = retailConnectionService.LoadStaffRetailers(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };

            return new NewtonSoftJsonResult(json, false);
        }

        [HttpPost]
        public ActionResult AddStaffRetailerFormData(RetailersFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            retailConnectionService.AddRetailer(model, false);
            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult EditStaffRetailerForm(RetailersFormViewModel model)
        {

            if (string.IsNullOrWhiteSpace(model.City))
                model.City = "";

            retailConnectionService.EditRetailer(model);


            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public JsonResult ApproveRetailer(int retailerId)
        {
            retailConnectionService.ChangeRetailerStatus(retailerId, TreadMarksConstants.UnderReviewStatus, TreadMarksConstants.ActiveStatus);
            var emailInfor = retailConnectionService.GetEmailInforByRetailerId(retailerId);

            if (emailInfor != null && EmailContentHelper.IsEmailEnabled("RetailConnectionRetailerApproved"))
            {
                Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
                MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
                MailAddress to = new MailAddress(emailInfor.EmailAddress);
                MailMessage message = new MailMessage(from, to) { };
                var siteUrl = SiteSettings.Instance.GetSettingValue("Settings.DomainName");

                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.RCRetailerApproveEmailTemplate"));
                //string emailBody = MicrosoftIO.File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("RetailConnectionRetailerApproved");
                string subject = EmailContentHelper.GetSubjectByName("RetailConnectionRetailerApproved");

                emailBody = emailBody
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.RetailerName, emailInfor.Name)
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.Retailer, "Retailer")
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.SiteUrl, siteUrl)
                     .Replace(RetailerConnectionProducrRetailerApproveEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = CollectorApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //string subject = "Retailer Approved";

                Task.Factory.StartNew(() =>
                {

                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), emailInfor.EmailAddress, null, null, subject, emailBody, null, null, alternateViewHTML);
                });
            }

            return Json(new { status = "OK" });
        }

        [HttpPost]
        public JsonResult RejectRetailer(int retailerId)
        {
            retailConnectionService.ChangeRetailerStatus(retailerId, TreadMarksConstants.UnderReviewStatus, TreadMarksConstants.RejectedStatus);
            return Json(new { status = "OK" });
        }
        [HttpPost]
        public JsonResult ActiveRetailer(int retailerId)
        {
            retailConnectionService.ChangeRetailerStatus(retailerId, TreadMarksConstants.InactiveStatus, TreadMarksConstants.ActiveStatus);
            var addProductButtonDisable = (retailConnectionService.LoadParticipantRetailer().Count() == 0);
            return Json(new { status = "OK", addProductButtonDisable = addProductButtonDisable });
        }
        [HttpPost]
        public JsonResult InactiveRetailer(int retailerId)
        {
            retailConnectionService.ChangeRetailerStatus(retailerId, TreadMarksConstants.ActiveStatus, TreadMarksConstants.InactiveStatus);
            var addProductButtonDisable = (retailConnectionService.LoadParticipantRetailer().Count() == 0);
            return Json(new { status = "OK", addProductButtonDisable = addProductButtonDisable });
        }


        #endregion

        #region Staff Product

        public JsonResult LoadAllStaffProducts(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ApprovedDate"},
                {1, "Status"},
                {2, "ProductName"},
                {3, "BrandName"},
                {4, "Category"},
                {5, "BusinessName"},
                {6, "RegistrationNumber"},
                {7, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = retailConnectionService.LoadStaffProducts(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Product Category

        [HttpPost]
        public ActionResult AddCategoryFormData(StaffCategoryFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            //string formModel = null;
            if (SecurityContextHelper.CurrentUser != null)
                model.UserId = SecurityContextHelper.CurrentUser.Id;

            retailConnectionService.AddCategory(model);

            return Json(new { status = true, result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult InitializeCategoryFormData(string type)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            StaffCategoryFormViewModel formModel = new StaffCategoryFormViewModel()
            {

            };

            return Json(new { status = true, result = formModel, statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult LoadAllProductCategories(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "AddedDate"},
                {1, "Status"},
                {2, "CategoryName"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = retailConnectionService.LoadProductCategory(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            //return Json(json, JsonRequestBehavior.AllowGet);
            return new NewtonSoftJsonResult(json, false);
        }

        [HttpPost]
        public ActionResult ChangeCategoryStatus(int categoryId, string fromStatus, string toStatus)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
            retailConnectionService.ChangeCategoryStatus(categoryId, fromStatus, toStatus);
            return Json(new { status = "OK", result = "", statusMsg = MessageResource.ValidData });
        }

        [HttpPost]
        public ActionResult GetRetailerInternalNotes(int id)
        {
            var result = retailConnectionService.LoadRetailerInternalNotes(id);
            result.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddRetailerNotesHandler(int id, string notes)
        {
            var internalNote = retailConnectionService.AddRetailerNote(id, notes);
            return new NewtonSoftJsonResult(internalNote, false);
        }

        #endregion

        #region Export

        [HttpGet]
        [Authorize]
        public ActionResult GetParticipantRetailerExport()
        {
            var result = retailConnectionService.GetParticipantRetailerExport();

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "MY_RETAILERS", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Participant Retailer Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetStaffRetailerExport()
        {
            var result = retailConnectionService.GetStaffRetailerExport();

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "ALL_RETAILERS", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Staff Retailer Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetParticipantProductExport()
        {
            var result = retailConnectionService.GetParticipantProductExport();

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "MY_PRODUCTS", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Participant Product Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetStaffProductExport()
        {
            var result = retailConnectionService.GetStaffProductExport();

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "ALL_PRODUCTS", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Staff Product Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetProductCategoryExport()
        {
            var result = retailConnectionService.GetProductCategoryExport();

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "ALL_CATEGORIES", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Staff Product Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        [HttpPost]
        public ActionResult GetCategoryEditFormData(int categoryId)
        {

            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var formModel = retailConnectionService.LoadCategoryById(categoryId);

            formModel.InternalNoteAddedOn = ConvertTimeFromUtc(formModel.InternalNoteAddedOn);

            return new NewtonSoftJsonResult(new { status = true, result = formModel, statusMsg = MessageResource.ValidData }, false);
        }

        #endregion
    }
}