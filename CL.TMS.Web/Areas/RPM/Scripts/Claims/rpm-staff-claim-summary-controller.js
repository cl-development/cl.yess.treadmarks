﻿'use strict';
rpmClaimApp.controller("rpmStaffClaimSummaryController", ["$scope", "$filter", "rpmClaimService", '$uibModal', 'claimService', function ($scope, $filter, rpmClaimService, $uibModal, claimService) {
    rpmClaimService.getClaimDetails().then(function (data) {
        $scope.RPMClaimSummary = data;
        $scope.ClaimCommonModel = data.ClaimCommonModel;

        $scope.PageIsReadonly = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;
        
        if ((data.ClaimStatus.StatusString == "Approved") || (data.ClaimStatus.StatusString == "Receive Payment")) {
            $scope.ShowMailDate = true;
        }
        else {
            $scope.ShowMailDate = false;
        }
        $scope.taxDisabled = $scope.ShowMailDate || Global.Settings.Permission.PaymentsAndAdjustmentsPermission == Global.Settings.Permission.ReadOnly;
        $scope.$watch("RPMClaimSummary.RPMPayment.MailedDate", function () {
            if ($scope.RPMClaimSummary.RPMPayment.MailedDate != null) {
                claimService.updateClaimMailDate($scope.RPMClaimSummary.RPMPayment.MailedDate);
            }
        });
        $scope.readOnlyRestriction = data.ClaimCommonModel.ClaimSummarySecurity.PageReadonlyForStaff;
        
        if (data.ClaimStatus.StatusString == "Approved") {
            $scope.DisableInternalAdjustmentsBtn = true;
        }
        else {
            $scope.DisableInternalAdjustmentsBtn = Global.Settings.Permission.DisableInternalAdjustmentsBtn
        }

        $scope.claimDetailsLoaded = true;
        $scope.getStatusColor = setStatusColor(data.ClaimStatus.StatusString);
    });

    var refreshPanels = function () {
        //call rpm claim service to refresh panel
        rpmClaimService.getClaimDetails().then(function (data) {
            $scope.RPMClaimSummary = data;
            $scope.ClaimCommonModel = data.ClaimCommonModel;
            $scope.claimDetailsLoaded = true;

            //refresh internal adjustment datatable
            $('#internalAdjustSearchBtn').click();

            console.log('$scope.RPMClaimSummary.RPMPayment.GroundTotal', $scope.RPMClaimSummary.RPMPayment.GroundTotal);
            claimService.getClaimWorkflowViewModel($scope.RPMClaimSummary.RPMPayment.GroundTotal).then(function (data) {//update workflow UI
                var localScope = angular.element(document.getElementById("claimWorkflowBar")).scope();
                if (localScope) {
                    localScope.claimWorkflowModel = data;//typeof data: ClaimWorkflowViewModel
                }
            });
        });
    }


    var initializeModalResult = function () {
        return {
            selectedItem: null,
            direction: 'overall',
            eligibility: '',
            onroad: 0,
            offroad: 0,
            paymentType: 'Overall',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,
            unitType: 3,
            //OTSTM2-649
            internalNote: null
        };
    }
    $scope.InternalAdjustTypes = [
       {
           name: 'Weight',
           id: '1'
       },
       {
           name: 'Payment',
           id: '4'
       }
    ];

    //Internal adjustment modal
    $scope.rpmInternalAdjustments = function () {
        var modalResult = initializeModalResult();
        var internalAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'EligibleAdjustmentCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.InternalAdjustTypes;
                },
                modalResult: function () {
                    return modalResult;
                },
                selectedItem: function () {
                    return $scope.InternalAdjustTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        internalAdjustmentModalInstance.result.then(function (result) {
            if (result.isCancel) {
                return
            }
            var internalAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'EligibleAdjustmentConfirmCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    modalResult: function () {
                        return result.modalResult;
                    }
                }
            });
            internalAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                claimService.submitInventoryAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {
                        refreshPanels();
                    }
                });
            });
        });
    }

    //Edit internal adjustment
    $scope.editInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    }, 
                    isReadOnly: function () {
                        return false;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }                  
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {
                if (result.isCancel) {
                    return
                }
                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        claimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var eligibleAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'EligibleAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    }, 
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            eligibleAdjustmentModalInstance.result.then(function (result) {

                if (result.isCancel) {
                    refreshPanels();
                    return
                }

                var eligibleAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'EligibleAdjustmentConfirmCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        modalResult: function () {
                            return result.modalResult;
                        }
                    }
                });
                eligibleAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    claimService.editInventoryAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            refreshPanels();
                        }
                    });
                });
            });

        });
    }

    //Remove internal adjustment
    $scope.removeInternalAdjust = function (data) {
        var internalAdjustmentId = data.InternalAdjustmentId;
        var internalAdjustmentType = data.InternalAdjustmentType;
        claimService.removeInventoryAdjustment(internalAdjustmentType, internalAdjustmentId).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    }

    //Mail Date Picker
    $scope.datePicker = {
        status: false
    };

    $scope.openDatePicker = function ($event) {
        $scope.datePicker.status = true;
    };

    //InternalNotesSettings for ClaimInternalNotes
    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalNotesUrl;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalNotesUrl;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalNotes;
    var setStatusColor = function (status) {
        switch (status) {
            case "Submitted":
                return 'color-tm-orange-bg';
                break;
            case "Open":
                return 'color-tm-blue-bg';
                break;
            case "Under Review":
                return 'color-tm-yellow-bg';
                break;
            case "Approved":
                return 'color-tm-green-bg';
                break;
            default:
                return '';
        }
    }
    $scope.taxApplicable = function () {
        $scope.RPMClaimSummary.RPMPayment.IsTaxApplicable = !$scope.RPMClaimSummary.RPMPayment.IsTaxApplicable;
        claimService.UpdateHST($scope.RPMClaimSummary.RPMPayment.HST, $scope.RPMClaimSummary.RPMPayment.IsTaxApplicable).then(function (data) {
            if (data.status == "refresh") {
                refreshPanels();
            }
        });
    }
}]);

rpmClaimApp.controller('EligibleAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable,disableForReadOnlyStaff, isReadOnly) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; //OTSTM2-155
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";

    $scope.confirm = function (isValid) {
        //OTSTM2-649
        if (isValid === false || !isValid) return;

        $scope.modalResult.selectedItem = $scope.selectedItem;

        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: false });
    }

    $scope.cancel = function () {
        $uibModalInstance.close({ modalResult: $scope.modalResult, isCancel: true });
    };

    /*
    ---------------------------------------------------------------------------------
    ------------------validations start here OTSTM2-270------------------------------
    */
    //OTSTM2-270 updating loadUrl and addUrl for InternalNotes for 'internalAdjustModal.html'
    //id = 4 is the InternalAdjustTypes for Payment
    if ($scope.isReadOnly) {
        if (selectedItem.id === '4') {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
        }
        else {
            $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
            $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalAdjustmentId=' + $scope.modalResult.adjustmentId;
        }
    }

    //OTSTM2-649 get current choosetype
    $scope.returnCurrentChooseType = function (item) {

        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });
        return curr[0].isValid();
    }

    //OTSTM2-649 param1 = item is current choose item & param1 = rule is the type from error rules for each validationtypes
    $scope.getErrorRule = function (item, rule) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });

        if (curr[0].errorRules.hasOwnProperty(rule)) {
            return curr[0].errorRules[rule]($scope.internaladjustForm);
        }
    }

    //OTSTM2-649
    $scope.isTireTypeAllZeros = function () {
        return validation.checkAllTireItemsZero($scope.modalResult)
    }

    //OTSTM2-649
    $scope.allZeroesOrEmpty = function () {
        var args = [].slice.call(arguments);

        return validation.checkAllFieldsZero(args)
    }

    var validation = {
        init: function () {
            var self = this;

            this.validationTypes = [
                   {
                       name: 'Weight',
                       id: '1',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.offroad, $scope.modalResult.onroad])
                       },
                       errorRules: {
                           _onRoad: function (form) {
                               return (form.onroad.$error.isDecimalRequired && !form.onroad.$pristine) || (form.onroad.$error.number && !form.onroad.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.onroad, $scope.modalResult.offroad]))
                           },
                           _offRoad: function (form) {
                               return (form.offroad.$error.isDecimalRequired && !form.offroad.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.onroad, $scope.modalResult.offroad]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   },
                   {
                       name: 'Payment',
                       id: '4',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.amount])
                       },
                       errorRules: {
                           _amount: function (form) {
                               return (form.amount.$error.isDecimalRequired && !form.amount.$pristine) || (form.amount.$error.number && !form.amount.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.amount]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   }
            ];

            $scope.$watch('selectedItem', function () {
                if ($scope.internaladjustForm) {
                    $scope.internaladjustForm.$setPristine();
                    $scope.internaladjustForm.$setUntouched();
                }
            });
        },
        isValid: function () {
            return ($scope.internaladjustForm.$valid);
        },
        checkAllTireItemsZero: function (item) {
            var tireObj = {
                plt: item.plt,
                agls: item.agls,
                gotr: item.gotr,
                ind: item.ind,
                lotr: item.lotr,
                motr: item.motr,
                mt: item.mt,
                sotr: item.sotr
            };

            return _.values(tireObj).every(function (val) {
                return (val === 0 || val === null);
            });
        },
        //para accepts only an array
        checkAllFieldsZero: function (arr) {
            if (arr instanceof Array) {
                return arr.every(function (val) {
                    return (val === 0 || val === null);
                });
            }
        },
        getAllValidationTypes: function () {
            return this.validationTypes;
        },
        empty: function (item) {
            return item.plt == null || item.agls == null || item.gotr == null || item.ind == null || item.lotr == null
                || item.motr == null || item.mt == null || item.sotr == null;
        }
    }

    validation.init();
    /*
    ------------------validations end here------------------------------
    --------------------------------------------------------------------
    */
}]);

rpmClaimApp.controller('EligibleAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);

