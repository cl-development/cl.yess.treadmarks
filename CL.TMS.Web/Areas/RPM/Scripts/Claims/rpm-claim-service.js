﻿'use strict';
rpmClaimApp.factory('rpmClaimService', ["$http", function ($http) {

    var factory = {};

    factory.getClaimDetails = function () {
        return $http.get(Global.ParticipantClaimsSummary.ClaimDetailsUrl)
            .then(function (result) {
                return result.data;
            });
    }

    factory.UpdateHST = function (HST, isTaxApplicable) {
        var req = { claimId: Global.ParticipantClaimsSummary.ClaimsID, HST: HST, IsTaxApplicable: isTaxApplicable }
        var submitVal = {
            url: Global.ParticipantClaimsSummary.UpdateHSTUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.submitClaimCheck = function () {

        var submitClaimModel = {
            claimId: Global.ParticipantClaimsSummary.ClaimsID,
        };
        
        var submitVal = {
            url: Global.ParticipantClaimsSummary.SubmitClaimCheckUrl,
            method: "POST",
            data: JSON.stringify(submitClaimModel)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.FinalSubmitClaim = function () {
        var req = { claimId: Global.ParticipantClaimsSummary.ClaimsID }
        var submitVal = {
                url: Global.ParticipantClaimsSummary.FinalSubmitClaimUrl,
                method: "POST",
                data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.exportList = function (data, url) {
        var submitVal = {
            url: url,
            method: "POST",
            data: JSON.stringify(data)
        }
        return $http(submitVal);
    }

    return factory;
}]);