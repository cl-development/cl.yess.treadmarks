﻿'use strict';

//RetailConnection App
var retailConnectionApp = angular.module("retailConnectionApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller", "retailConnection.directives", "retailConnection.controllers", "retailConnection.services", "retailConnection.filters"]);

//Controllers Section
var controllers = angular.module('retailConnection.controllers', []);
controllers.controller('retailConnectionController', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) { }]);
//Form Controller Section
controllers.controller('formCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'item', '$rootScope', function ($scope, $uibModal, $uibModalInstance, item, $rootScope) {
    var rcTypeArr = item.rcType.split(' ');
    $scope.rcType = item.rcType;
    $scope.panelName = rcTypeArr.length > 0 ? rcTypeArr[rcTypeArr.length - 1] : '';
    $scope.emptyOrMy = rcTypeArr[0] === "Participant" ? 'My' : '';
    
    $scope.currentMode = item.currentMode;
    if ($scope.currentMode === "Edit" || $scope.currentMode === "View") {
        $scope.id = item.id;
    }
    $rootScope.$on('CANCEL', function (e, data) {
        $uibModalInstance.dismiss('cancel');
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
}]);
controllers.controller('RemoveProductRtlCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', 'removeContent', 'retailConnServices', function ($rootScope, $scope, $http, $uibModalInstance, removeContent, retailConnServices) {
    $scope.removeContent = removeContent;

    $scope.confirm = function () {
        $uibModalInstance.close($scope.removeContent.retailer);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('StatusChangeCtrl', ['$scope', '$http', '$window', '$uibModal', '$uibModalInstance', 'statusContent', '$rootScope', function ($scope, $http, $window, $uibModal, $uibModalInstance, statusContent, $rootScope) {

    $scope.statusMessage = statusContent.statusMessage;
    $scope.rcType = statusContent.rcType;
    $scope.Id = statusContent.id;
    $scope.fromStatus = statusContent.fromStatus;
    $scope.toStatus = statusContent.toStatus;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.confirm = function () {
        $uibModalInstance.close(statusContent);
    }
}]);

//Services Section
var services = angular.module('retailConnection.services', []);
services.factory('retailConnServices', ["$http", function ($http) {
    var factory = {};

    factory.getProductEditFormData = function (productId) {
        var submitVal = {
            url: Global.RetailConnnection.GetProductEditFormDataUrl,
            method: "POST",
            data: { productId: productId }
        };
        return $http(submitVal);
    };

    factory.getRetailerEditFormData = function (retailerId) {
        var submitVal = {
            url: Global.RetailConnnection.GetRetailerEditFormDataUrl,
            method: "POST",
            data: { retailerId: retailerId }
        };
        return $http(submitVal);
    };

    factory.getCategoryEditFormData = function (categoryId) {
        var submitVal = {
            url: Global.RetailConnnection.GetCategoryEditFormDataUrl,
            method: "POST",
            data: { categoryId: categoryId }
        };
        return $http(submitVal);
    };

    factory.initializeCategoryFormData = function (type) {
        var submitVal = {
            url: Global.RetailConnnection.InitializeCategoryFormDataUrl,
            method: "POST",
            data: { type: type }
        };
        return $http(submitVal);
    };

    factory.deleteTemporaryProductFile = function (attachmentId, productId) {
        var submitVal = {
            url: Global.RetailConnnection.DeleteTemporaryProductFileURL,
            method: "POST",
            data: { attachmentId: attachmentId, productId: productId }
        };
        return $http(submitVal);
    };

    factory.addProductForm = function (productFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.AddProductFormUrl,
            method: "POST",
            data: { model: productFormModel }
        };
        return $http(submitVal);
    };

    factory.editProductForm = function (productFormModel, isStaff) {

        if (isStaff) {
            var url = Global.RetailConnnection.EditStaffProductFormUrl;
        }
        else {
            var url = Global.RetailConnnection.EditProductFormUrl;
        }

        var submitVal = {
            url: url,
            method: "POST",
            data: { model: productFormModel }
        };
        return $http(submitVal);
    };

    factory.dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return '1/1/1950';
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();

        var hours = c.getHours();
        var minutes = c.getMinutes();
        var second = c.getSeconds();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

        return d + ' ' + curr_time;
    };

    factory.getCurrentDateTime = function () {
        var currentdate = new Date();
        var datetime = currentdate.getFullYear() + "-/"
                        + (currentdate.getMonth() + 1) + "-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
        return currentdate;
    };

    factory.addCategoryForm = function (categoryFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.AddCategoryFormDataUrl,
            method: "POST",
            data: { model: categoryFormModel }
        };
        return $http(submitVal);
    };

    factory.checkRetailerName = function (retailerName) {
        var submitVal = {
            url: Global.RetailConnnection.CheckRetailerNameUrl,
            method: "POST",
            data: { retailerName: retailerName }
        };
        return $http(submitVal);
    };

    factory.addStaffRetailerForm = function (retailerFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.AddStaffRetailerFormDataUrl,
            method: "POST",
            data: { model: retailerFormModel }
        };
        return $http(submitVal);
    };

    factory.addParticipantRetailerForm = function (participantRetailerFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.AddParticipantRetailerUrl,
            method: "POST",
            data: { model: participantRetailerFormModel }
        };
        return $http(submitVal);
    };

    factory.editStaffRetailerForm = function (retailerFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.EditStaffRetailerFormDataUrl,
            method: "POST",
            data: { model: retailerFormModel }
        };
        return $http(submitVal);
    };

    factory.editParticipantRetailerForm = function (participantRetailerFormModel) {
        var submitVal = {
            url: Global.RetailConnnection.EditParticipantRetailerUrl,
            method: "POST",
            data: { model: participantRetailerFormModel }
        };
        return $http(submitVal);
    };

    factory.approveProduct = function (productId) {
        var submitVal = {
            url: Global.RetailConnnection.ApproveProductUrl,
            method: "POST",
            data: { productId: productId }
        };
        return $http(submitVal);
    };

    factory.rejectProduct = function (productId) {
        var submitVal = {
            url: Global.RetailConnnection.RejectProductUrl,
            method: "POST",
            data: { productId: productId }
        };
        return $http(submitVal);
    };

    factory.approveRetailer = function (retailerId) {
        var submitVal = {
            url: Global.RetailConnnection.ApproveRetailerUrl,
            method: "POST",
            data: { retailerId: retailerId }
        };
        return $http(submitVal);
    };

    factory.rejectRetailer = function (retailerId) {
        var submitVal = {
            url: Global.RetailConnnection.RejectRetailerUrl,
            method: "POST",
            data: { retailerId: retailerId }
        };
        return $http(submitVal);
    };

    factory.checkExport = function (batchId) {
        var submitVal = {
            url: Global.GP.CheckExportUrl,
            method: "POST",
            data: { batchId: batchId }
        }
        return $http(submitVal);
    }

    factory.getExport = function (batchId, panelName) {
        return $http({
            method: 'GET',
            url: Global.GP.ExportUrl,
            params: {
                batchId: batchId,
                panelName: panelName
            }
        });
    }

    factory.NoActiveRetailerExists = Global.RetailConnnection.NoActiveRetailerExists;

    return factory;
}]);

//Filter Section
var filters = angular.module('retailConnection.filters', []);
filters.filter('dateFilter', ['$filter', function ($filter) {
    return function (input) {
        return $filter('date')(new Date(input), 'yyyy-MM-dd HH:mm a');
    }
}])

//Directives Section
var directives = angular.module('retailConnection.directives', []);
directives.directive('rcPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', function (DTOptionsBuilder, DTColumnBuilder) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'rc-panel-template.html',
        scope: {
            rcUrl: '@',
            type: '@',
            header: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {

            $scope.dtInstance = {};

            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");

            //PageEditable;            
            if (Global.RetailConnnection.PageEditable === 'false') {
                $scope.PageEditable = false;
            }
            else {
                $scope.PageEditable = true;
            }

            if (Global.RetailConnnection.IsAllRetailersEditable === 'false') 
                $scope.IsAllRetailersEditable = false;            
            else 
                $scope.IsAllRetailersEditable = true;
            
            if (Global.RetailConnnection.IsAllProductsEditable === 'false')
                $scope.IsAllProductsEditable = false;
            else
                $scope.IsAllProductsEditable = true;

            if (Global.RetailConnnection.IsProductCategoriesEditable === 'false')
                $scope.IsProductCategoriesEditable = false;
            else
                $scope.IsProductCategoriesEditable = true;


            $scope.chevronId = $scope.header.replace(/\s/g, '');

            $scope.viewMore = true;

            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, data) {
                $scope.dtInstance.DataTable.draw();
            });

            switch ($scope.type) {
                case "Participant Retailer":
                    participantRetailerColumns();
                    break;
                case "Participant Product":
                    participantProductColumns();
                    break;
                case "Staff Retailer":
                    staffRetailerColumns();
                    break;
                case "Staff Product":
                    staffProductColumns();
                    break;
                case "Product Category":
                    productCategoryColumns();
                    break;
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax',
                {
                    dataSrc: "data",
                    url: $scope.rcUrl,
                    type: "POST"
                })
                .withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [0, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                    //OTSTM2-843 clicking more, make sure alignment
                    if ($scope.viewMore != undefined && $scope.viewMore == false) {
                        if (angular.element(row)["0"].children.length == 5) { //Product Categories
                            $(angular.element(row)["0"].children[2]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[3]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-25px');
                        }
                        if (angular.element(row)["0"].children.length == 6) { //All Retailers
                            $(angular.element(row)["0"].children[2]).addClass('move-right-10px');
                            $(angular.element(row)["0"].children[3]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[5]).addClass('move-right-25px');
                        }
                        if (angular.element(row)["0"].children.length == 9) { //All Products
                            $(angular.element(row)["0"].children[2]).addClass('move-right-10px');
                            $(angular.element(row)["0"].children[3]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[5]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[6]).addClass('move-right-25px');
                            $(angular.element(row)["0"].children[7]).addClass('move-right-25px');
                            $(angular.element(row)["0"].children[8]).addClass('move-right-25px');
                        }
                    }
                })
                .withOption('initComplete', function (settings, result) {
                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.$apply();
                });

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250)
                                .withOption('scrollX', "100%");
            }

            //search
            $scope.search = function (dtInstance) {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            //participant retailer columns
            function participantRetailerColumns() {
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ApprovedDate", "Date Added").withOption('width', '10%').withOption('name', 'ApprovedDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div title="' + date.time + '">' + date.date + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Status", "Status").withOption('width', '10%').withOption('name', 'Status').withClass('td-center').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Inactive":
                                return '<div class="panel-table-status color-tm-red-bg" title="Inactive">Inactive</div>';
                            case "UnderReview":
                                return '<div class="panel-table-status color-tm-yellow-bg" title="UnderReview">Under Review</div>';
                            case "Active":
                                return '<div class="panel-table-status color-tm-green-bg" title="Active">Active</div>';
                            case "Rejected":
                                return '<div class="panel-table-status color-tm-red-bg" title="Rejected">Rejected</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn('RetailerName', "Retailer").withOption('width', '30%').withOption('name', 'RetailerName'),
                    DTColumnBuilder.newColumn('AddedBy', "Added By").withOption('width', '40%').withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withOption('width', '10%').notSortable()
                        .renderWith(participantRetailerActionsHtml)
                ];
            }

            //participant product columns
            function participantProductColumns() {
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ApprovedDate", "Date Added").withOption('width', '10%').withOption('name', 'ApprovedDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div title="' + date.time + '">' + date.date + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Status", "Status").withOption('width', '10%').withOption('name', 'Status').withClass('td-center').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Inactive":
                                return '<div class="panel-table-status color-tm-red-bg" title="Inactive">Inactive</div>';
                            case "UnderReview":
                                return '<div class="panel-table-status color-tm-yellow-bg" title="UnderReview">Under Review</div>';
                            case "Active":
                                return '<div class="panel-table-status color-tm-green-bg" title="Active">Active</div>';
                            case "Rejected":
                                return '<div class="panel-table-status color-tm-red-bg" title="Rejected">Rejected</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn('ProductName', "Product Name").withOption('width', '10%').withOption('name', 'ProductName'),
                    DTColumnBuilder.newColumn('BrandName', "Brand Name").withOption('width', '20%').withOption('name', 'BrandName'),
                    DTColumnBuilder.newColumn('AddedBy', "Added By").withOption('width', '40%').withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withOption('width', '10%').notSortable()
                        .renderWith(participantProductActionsHtml)
                ];
            }

            //staff retailer columns
            function staffRetailerColumns() {
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ApprovedDate", "Date Added").withOption('width', '10%').withOption('name', 'ApprovedDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return data.toString().substring(0, 10);
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Status", "Status").withOption('width', '10%').withOption('name', 'Status').withClass('td-center').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Inactive":
                                return '<div class="panel-table-status color-tm-red-bg" title="Inactive">Inactive</div>';
                            case "UnderReview":
                                return '<div class="panel-table-status color-tm-yellow-bg" title="UnderReview">Under Review</div>';
                            case "Active":
                                return '<div class="panel-table-status color-tm-green-bg" title="Active">Active</div>';
                            case "Rejected":
                                return '<div class="panel-table-status color-tm-red-bg" title="Rejected">Rejected</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn('RetailerName', "Retailer").withOption('width', '30%').withOption('name', 'RetailerName'),
                    DTColumnBuilder.newColumn('AddedBy', "Added By").withOption('width', '30%').withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn("StaffNotesAllText", "Notes").withOption('name', 'StaffNotesAllText').withOption('width', '10%').notSortable().renderWith(function (data, type, full, meta) {
                        if (data && data != '') {
                            var str = '';
                            for (var i = 0; i < data.length; i++) {
                                str += '<ul>' + data[i] + '</ul>';
                            }

                            var html = $compile($templateCache.get('messagePopover.html').replace('Message', str))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withOption('width', '10%').notSortable()
                        .renderWith(staffRetailerActionsHtml)
                ];
            }

            //staff product columns
            function staffProductColumns() {
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ApprovedDate", "Date Added").withOption('width', '10px').withOption('name', 'ApprovedDate').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div title="' + date.time + '">' + date.date + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Status", "Status").withOption('width', '10px').withOption('name', 'Status').withClass('td-center').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Inactive":
                                return '<div class="panel-table-status color-tm-red-bg" title="Inactive">Inactive</div>';
                            case "UnderReview":
                                return '<div class="panel-table-status color-tm-yellow-bg" title="UnderReview">Under Review</div>';
                            case "Active":
                                return '<div class="panel-table-status color-tm-green-bg" title="Active">Active</div>';
                            case "Rejected":
                                return '<div class="panel-table-status color-tm-red-bg" title="Rejected">Rejected</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn('ProductName', "Name").withOption('width', '10px').withOption('name', 'ProductName'),
                    DTColumnBuilder.newColumn('BrandName', "Brand Name").withOption('width', '10px').withOption('name', 'BrandName'),
                    DTColumnBuilder.newColumn('Category', "Category").withOption('width', '10px').withOption('name', 'Category'),
                    DTColumnBuilder.newColumn('BusinessName', "Business Name").withOption('width', '30px').withOption('name', 'BusinessName'),
                    DTColumnBuilder.newColumn('RegistrationNumber', "RPM Registration#").withOption('width', '20px').withOption('name', 'RegistrationNumber'),
                    DTColumnBuilder.newColumn('AddedBy', "Added By").withOption('width', '30px').withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withOption('width', '5px').notSortable()
                        .renderWith(staffProductActionsHtml)
                ];
            }

            //product category columns
            function productCategoryColumns() {
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("AddedDate", "Added Date").withOption('width', '20%').withOption('name', 'AddedDate').renderWith(function (data, type, full, meta) {                      
                        //var date = dateTimeConvert(data);
                        //return '<div title="' + date.time + '">' + date.date + '</div>';
                        if (data !== null) {
                            return data.toString().substring(0, 10);
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Status", "Status").withOption('width', '10%').withOption('name', 'Status').withClass('td-center').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Inactive":
                                return '<div class="panel-table-status color-tm-red-bg" title="Inactive">Inactive</div>';
                            case "Active":
                                return '<div class="panel-table-status color-tm-green-bg" title="Active">Active</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn('CategoryName', "Category Name").withOption('width', '40%').withOption('name', 'CategoryName'),
                    DTColumnBuilder.newColumn("StaffCategoryNotesAllText", "Notes").withOption('width', '20%').withOption('name', 'StaffCategoryNotesAllText').notSortable().renderWith(function (data, type, full, meta) {
                        if (data && data != '') {
                            var str = '';
                            for (var i = 0; i < data.length; i++) {
                                str += '<ul>' + data[i] + '</ul>';
                            }

                            var html = $compile($templateCache.get('messagePopover.html').replace('Message', str))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withOption('width', '10%').notSortable()
                        .renderWith(productCategoryActionsHtml)
                ];
            }

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function (ev) {
                    var el = ev.toElement;
                    $scope.$apply(function () {
                        if (!(el && ($(el).attr('data-name') == 'notes' || $(el).closest('.popover').length > 0))) {
                            if (aData.Status==="Active") {
                                //open view modal
                                $scope.formViewModal(aData.ID);
                            }                 
                        }
                    });
                });
                return nRow;
            }

            //open second modal
            $scope.formViewModal = function (id) {
                var formViewModal = $uibModal.open({
                    templateUrl: 'rc-form-modal.html',
                    controller: 'formCtrl',
                    //size: 'lg',
                    size: function () {
                        switch ($scope.type.replace(/\s/g, '')) {
                            case "ParticipantRetailer":
                                return 'lg';
                                break;
                            case "ParticipantProduct":
                                return 'lg';
                                break;
                            case "StaffRetailer":
                                return 'lg';
                                break;
                            case "StaffProduct":
                                return 'lg';
                                break;
                            case "ProductCategory":
                                return '';
                                break;
                        }
                    },
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return {
                                rcType: $scope.type,
                                id: id,
                                currentMode: "View"
                            }
                        }
                    }
                });
            }

            function staffRetailerActionsHtml(data, type, full, meta) {
                if ($scope.IsAllRetailersEditable) {
                    switch (data.Status.replace(" ", "")) {
                        case "Active":
                            var html = $compile($templateCache.get('active-action.html').replace(/rcType/g, 'Staff Retailer').replace(/rcId/g, data.ID).replace(/rcName/g, data.RetailerName))($scope);
                            return html[0].outerHTML;

                        case "Inactive":
                            var html = $compile($templateCache.get('inactive-action.html').replace(/rcType/g, 'Staff Retailer').replace(/rcId/g, data.ID).replace(/rcName/g, data.RetailerName))($scope);
                            return html[0].outerHTML;

                        case "UnderReview":
                            var html = $compile($templateCache.get('staff-under-review-action.html').replace(/rcType/g, 'Staff Retailer').replace(/rcId/g, data.ID).replace(/rcName/g, data.RetailerName))($scope);
                        return html[0].outerHTML;
                        default:
                            return "";
                    }
                }
                else {
                    return "";
                }
            }

            function staffProductActionsHtml(data, type, full, meta) {

                if ($scope.IsAllProductsEditable) {
                    switch (data.Status.replace(" ", "")) {
                        case "UnderReview":
                            var html = $compile($templateCache.get('staff-under-review-action.html').replace(/rcType/g, 'Staff Product').replace(/rcId/g, data.ID).replace(/rcName/g, data.ProductName))($scope);
                            return html[0].outerHTML;

                        case "Active":
                            var html = $compile($templateCache.get('active-action.html').replace(/rcType/g, 'Staff Product').replace(/rcId/g, data.ID).replace(/rcName/g, data.ProductName))($scope);
                            return html[0].outerHTML;

                        case "Inactive":
                            var html = $compile($templateCache.get('inactive-action.html').replace(/rcType/g, 'Staff Product').replace(/rcId/g, data.ID).replace(/rcName/g, data.ProductName))($scope);
                            return html[0].outerHTML;

                        default:
                            return "";
                    }
                }
                else {
                    return "";
                }
            }

            function productCategoryActionsHtml(data, type, full, meta) {
                if ($scope.IsProductCategoriesEditable) {
                    switch (data.Status.replace(" ", "")) {
                        case "Active":
                            var html = $compile($templateCache.get('active-action-only.html').replace(/rcType/g, 'Product Category').replace(/rcId/g, data.ID).replace(/rcName/g, data.CategoryName))($scope);
                            return html[0].outerHTML;

                        case "Inactive":
                            var html = $compile($templateCache.get('inactive-action.html').replace(/rcType/g, 'Product Category').replace(/rcId/g, data.ID).replace(/rcName/g, data.CategoryName))($scope);
                            return html[0].outerHTML;

                        default:
                            return "";
                    }
                }
                else {
                    return "";
                }
            }

            function participantRetailerActionsHtml(data, type, full, meta) {
                if ($scope.PageEditable) {
                    switch (data.Status.replace(" ", "")) {
                        case "Active":
                            if (data.IsAddedByParticipant) {
                                var html = $compile($templateCache.get('active-action.html').replace(/rcType/g, 'Participant Retailer').replace(/rcId/g, data.ID).replace(/rcName/g, data.RetailerName))($scope);
                                return html[0].outerHTML;
                            }
                            else
                                return "";

                        case "Inactive":
                            if (data.IsAddedByParticipant) {
                                var html = $compile($templateCache.get('inactive-action.html').replace(/rcType/g, 'Participant Retailer').replace(/rcId/g, data.ID).replace(/rcName/g, data.RetailerName))($scope);
                                return html[0].outerHTML;
                            }
                            else
                                return "";
                        default:
                            return "";
                    }
                } else {
                    return "";
                }
            }

            function participantProductActionsHtml(data, type, full, meta) {
                if ($scope.PageEditable) {
                    switch (data.Status.replace(" ", "")) {
                        case "Active":
                            var html = $compile($templateCache.get('active-action.html').replace(/rcType/g, 'Participant Product').replace(/rcId/g, data.ID).replace(/rcName/g, data.ProductName))($scope);
                            return html[0].outerHTML;

                        case "Inactive":
                            var html = $compile($templateCache.get('inactive-action.html').replace(/rcType/g, 'Participant Product').replace(/rcId/g, data.ID).replace(/rcName/g, data.ProductName))($scope);
                            return html[0].outerHTML;

                        default:
                            return "";
                    }
                }
                else {
                    return "";
                }
            }

            function dateTimeConvert(data) {
                if (data == null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches == null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

                return {
                    date: d,
                    time: curr_time
                }
            };
        }
    }
}]);
//Add Button handler
directives.directive('rcAddBtn', ['retailConnServices', function (retailConnServices) {
    return {
        restrict: 'E',
        templateUrl: 'rc-add-btn.html',
        scope: {
            rcType: '=',
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal) {

            if (Global.RetailConnnection.PageEditable === 'false') {
                $scope.PageEditable = false;
            }
            else {
                $scope.PageEditable = true;
            }
            $scope.$watch(function() {return retailConnServices.NoActiveRetailerExists;}, function (val) {
                if ('Participant Product' == $scope.rcType) {
                    console.log(val,' = val; $scope.$watch - retailConnServices.NoActiveRetailerExists:', retailConnServices.NoActiveRetailerExists);
                    $scope.NoActiveRetailerExists = retailConnServices.NoActiveRetailerExists;
                }
            });

            if ('Participant Product' == $scope.rcType) {
                $scope.NoActiveRetailerExists = Global.RetailConnnection.NoActiveRetailerExists;
            }

            $scope.add = function () {
                var formModal = $uibModal.open({
                    templateUrl: 'rc-form-modal.html',
                    controller: 'formCtrl',
                    //size: 'lg',
                    size: function () {
                        switch ($scope.rcType.replace(/\s/g, '')) {
                            case "ParticipantRetailer":
                                return 'lg';
                                break;
                            case "ParticipantProduct":
                                return 'lg';
                                break;
                            case "StaffRetailer":
                                return 'lg';
                                break;
                            case "StaffProduct":
                                return 'lg';
                                break;
                            case "ProductCategory":
                                return '';
                                break;
                        }
                    },
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return {
                                rcType: $scope.rcType,
                                currentMode: "Add",
                                panelName: $scope.panelName
                            }
                        }
                    }
                });
            }
        }
    };
}]);
//Edit Button handler
directives.directive('rcEditForm', [function () {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            rcType: '@',
            id: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal) {
            $scope.openFormForEdit = function () {
                var formModal = $uibModal.open({
                    templateUrl: 'rc-form-modal.html',
                    controller: 'formCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return {
                                rcType: $scope.rcType,
                                id: $scope.id,
                                currentMode: "Edit"
                            }
                        }
                    }
                });
            }
        }
    };
}]);
//Approve Button handler
directives.directive('rcApproveForm', ['retailConnServices', function (retailConnServices) {
    return {
        restrict: 'E',
        templateUrl: 'staff-approve-btn.html',
        scope: {
            rcType: '@',
            id: '@',
            approveName: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal, $rootScope) {
            $scope.approve = function () {

                var approveInstance = $uibModal.open({
                    templateUrl: 'rc-status-change-modal.html',
                    controller: 'StatusChangeCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        statusContent: function () {
                            return {
                                statusMessage: Global.RetailConnnection.approveConfirmMessage + $scope.approveName,
                                rcType: $scope.rcType,
                                id: $scope.id
                            };
                        }
                    }
                });
                approveInstance.result.then(function (response) {
                    switch ($scope.rcType.replace(/\s/g, '')) {
                        case "StaffRetailer":
                            retailConnServices.approveRetailer($scope.id).then(function (response) {
                                if (response.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcApproveForm");
                                }
                            });
                            break;
                        case "StaffProduct":
                            retailConnServices.approveProduct($scope.id).then(function (response) {
                                if (response.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcApproveForm");
                                }
                            });
                            break;
                    }
                });
            }
        }
    };
}]);
//Reject Button handler
directives.directive('rcRejectForm', ['retailConnServices', function (retailConnServices) {
    return {
        restrict: 'E',
        templateUrl: 'staff-reject-btn.html',
        scope: {
            rcType: '@',
            id: '@',
            rejectName: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal, $rootScope) {
            $scope.reject = function () {
                var rejectInstance = $uibModal.open({
                    templateUrl: 'rc-status-change-modal.html',
                    controller: 'StatusChangeCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        statusContent: function () {
                            return {
                                statusMessage: Global.RetailConnnection.rejectConfirmMessage + $scope.rejectName,
                                rcType: $scope.rcType,
                                id: $scope.id
                            };
                        }
                    }
                });
                rejectInstance.result.then(function (response) {
                    switch ($scope.rcType.replace(/\s/g, '')) {
                        case "StaffRetailer":
                            retailConnServices.rejectRetailer($scope.id).then(function (response) {
                                if (response.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRejectForm");
                                }
                            });
                            break;
                        case "StaffProduct":
                            retailConnServices.rejectProduct($scope.id).then(function (response) {
                                if (response.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRejectForm");
                                }
                            });
                            break;
                    }
                });
            }
        }
    };
}]);
//Inactive Button handler
directives.directive('rcInactiveForm', ['retailConnServices', function (retailConnServices) {

    return {
        restrict: 'E',
        templateUrl: 'inactive-btn.html',
        scope: {
            rcType: '@',
            id: '@',
            inactiveName: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal, $http) {
            $scope.inactive = function () {
                var inactiveInstance = $uibModal.open({
                    templateUrl: 'rc-status-change-modal.html',
                    controller: 'StatusChangeCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        statusContent: function () {
                            return {
                                statusMessage: Global.RetailConnnection.inactiveConfirmMessage + $scope.inactiveName + '?',
                                rcType: $scope.rcType,
                                id: $scope.id,
                                fromStatus: "Active",
                                toStatus: "Inactive"
                            };
                        }
                    }
                });
                inactiveInstance.result.then(function (response) {
                    $scope.Id = response.id;
                    $scope.fromStatus = response.fromStatus;
                    $scope.toStatus = response.toStatus;

                    switch ($scope.rcType.replace(/\s/g, '')) {
                        case "ParticipantRetailer":
                            var submitVal = {
                                url: Global.RetailConnnection.InactiveRetailerUrl,
                                method: "POST",
                                data: { retailerId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    retailConnServices.NoActiveRetailerExists = result.data.addProductButtonDisable;
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcInactiveForm");
                                }
                            });
                            break;
                        case "ParticipantProduct":
                            var submitVal = {
                                url: Global.RetailConnnection.InactiveProductUrl,
                                method: "POST",
                                data: { productId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcInactiveForm");
                                }
                            });
                            break;
                        case "StaffRetailer":
                            var submitVal = {
                                url: Global.RetailConnnection.InactiveRetailerUrl,
                                method: "POST",
                                data: { retailerId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcInactiveForm");
                                }
                            });
                            break;
                        case "StaffProduct":
                            var submitVal = {
                                url: Global.RetailConnnection.InactiveProductUrl,
                                method: "POST",
                                data: { productId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcInactiveForm");
                                }
                            });
                            break;
                        case "ProductCategory":
                            var submitVal = {
                                url: Global.RetailConnnection.ChangeCategoryStatusUrl,
                                method: "POST",
                                data: { categoryId: $scope.Id, fromStatus: $scope.fromStatus, toStatus: $scope.toStatus }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcInactiveForm");
                                }
                            });
                            break;
                    }

                });
            }
        }
    };
}]);
//Active Button handler
directives.directive('rcActiveForm', ['retailConnServices', function (retailConnServices) {
    return {
        restrict: 'E',
        templateUrl: 'active-btn.html',
        scope: {
            rcType: '@',
            id: '@',
            activeName: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal, $http) {
            $scope.active = function () {
                var activeInstance = $uibModal.open({
                    templateUrl: 'rc-status-change-modal.html',
                    controller: 'StatusChangeCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        statusContent: function () {
                            return {
                                statusMessage: Global.RetailConnnection.activeConfirmMessage + $scope.activeName,
                                rcType: $scope.rcType,
                                id: $scope.id,
                                fromStatus: "Inactive",
                                toStatus: "Active"
                            };
                        }
                    }
                });
                activeInstance.result.then(function (response) {
                    $scope.Id = response.id;
                    $scope.fromStatus = response.fromStatus;
                    $scope.toStatus = response.toStatus;

                    switch ($scope.rcType.replace(/\s/g, '')) {
                        case "ParticipantRetailer":
                            var submitVal = {
                                url: Global.RetailConnnection.ActiveRetailerUrl,
                                method: "POST",
                                data: { retailerId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                retailConnServices.NoActiveRetailerExists = result.data.addProductButtonDisable;
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcActiveForm");
                                }
                            });
                            break;
                        case "ParticipantProduct":
                            var submitVal = {
                                url: Global.RetailConnnection.ActiveProductUrl,
                                method: "POST",
                                data: { productId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcActiveForm");
                                }
                            });
                            break;
                        case "StaffRetailer":
                            var submitVal = {
                                url: Global.RetailConnnection.ActiveRetailerUrl,
                                method: "POST",
                                data: { retailerId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcActiveForm");
                                }
                            });
                            break;
                        case "StaffProduct":
                            var submitVal = {
                                url: Global.RetailConnnection.ActiveProductUrl,
                                method: "POST",
                                data: { productId: $scope.Id }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcActiveForm");
                                }
                            });
                            break;
                        case "ProductCategory":
                            var submitVal = {
                                url: Global.RetailConnnection.ChangeCategoryStatusUrl,
                                method: "POST",
                                data: { categoryId: $scope.Id, fromStatus: $scope.fromStatus, toStatus: $scope.toStatus }
                            }
                            return $http(submitVal).then(function (result) {
                                if (result.data.status == "OK") {
                                    $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcActiveForm");
                                }
                            });
                            break;
                    }
                });
            }
        }
    };
}]);
//common modal for status change

//product category form
directives.directive('rcCategoryForm', ['retailConnServices', '$rootScope', function (retailConnServices, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'rc-category-form.html',
        scope: {
            currentMode: '=',
            categoryId: '='
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope) {

            $scope.loadCategoryModel = false;

            $scope.cancel = function () {
                $rootScope.$emit('CANCEL', "cancel");
            }

            $scope.submitForm = function (currentMode) {
                switch ($scope.currentMode) {
                    case "Add":
                        addCategoryForm();
                        break;
                    case "Edit":
                        break;
                    case "View":
                        break;
                }
            }

            function addCategoryForm() {
                if (angular.isDefined($scope.categoryFormModel)) {
                    retailConnServices.addCategoryForm($scope.categoryFormModel).then(function (response) {
                        if (response.data.status) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED', "CategoryForm");
                            $rootScope.$emit('CANCEL', "cancel");
                        }
                    });
                }
            }

            function initializeFormData() {
                retailConnServices.initializeCategoryFormData('Product Category').then(function (response) {
                    if (response.data.status) {
                        $scope.CategoryFormModel = response.data.result;
                    }
                });
            }

            function getCategoryEditFormData() {
                retailConnServices.getCategoryEditFormData($scope.categoryId).then(function (response) {
                    if (response.data.status) {
                        $scope.categoryFormModel = response.data.result;
                        if ($scope.categoryFormModel.InternalNoteAddedOn.indexOf("0001-01-01") !== -1) {//"0001-01-01T00:00:00" == 
                            $scope.categoryFormModel.InternalNoteAddedOn = null;
                        }
                        $scope.loadCategoryModel = true;
                    }
                });
            }

            switch ($scope.currentMode) {
                case "Add":
                    initializeFormData();
                    break;
                case "Edit":
                    break;
                case "View":
                    getCategoryEditFormData();
                    break;
            }
        }
    };
}]);

//participant retailer form
directives.directive('rcRetailerForm', ['retailConnServices', '$rootScope', function (retailConnServices, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'rc-retailer-form.html',
        scope: {
            currentMode: '=',
            retailerId: '=',
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $http) {

            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");
            $scope.loadRetailModel = false;
            $scope.retailerNameEditFlag = null;

            $scope.cancel = function () {
                $rootScope.$emit('CANCEL', "cancel");
            }

            $scope.submitForm = function (mode) {
                switch ($scope.currentMode) {
                    case "Add":                      
                        $scope.checkRetailerNameFlag = false;
                        checkRetailerNameForAdd();
                        break;
                    case "Edit":
                        if ($scope.retailerFormModel.RetailerName==$scope.retailerNameEditFlag) {
                            if ($scope.isStaff) {
                                editStaffRetailerForm();
                            } else {
                                editParticipantRetailerForm();
                            } 
                        }
                        else {
                            $scope.checkRetailerNameFlag = false;
                            checkRetailerNameForEdit();
                        }                   
                        break;
                    case "View":
                        break;
                }
            }

            function checkRetailerNameForAdd() {
                retailConnServices.checkRetailerName($scope.retailerFormModel.RetailerName).then(function (response) {
                    if (response.data.status) {
                        $scope.checkRetailerNameFlag = true;
                    }
                    else {                      
                        if ($scope.isStaff) {
                            addStaffRetailerForm();
                        } else {
                            addParticipantRetailerForm();
                        }
                    }
                });
            }

            function checkRetailerNameForEdit() {
                retailConnServices.checkRetailerName($scope.retailerFormModel.RetailerName).then(function (response) {
                    if (response.data.status) {
                        $scope.checkRetailerNameFlag = true;
                    }
                    else {
                        if ($scope.isStaff) {
                            editStaffRetailerForm();
                            $scope.retailerNameEditFlag = null;
                        } else {
                            editParticipantRetailerForm();
                            $scope.retailerNameEditFlag = null;
                        }
                    }
                });
            }

            function initializeFormData() {
                $http.get(Global.RetailConnnection.InitializeRetailerFormDataUrl).then(function (response) {
                    if (response.data.status) {
                        $scope.retailerFormModel = response.data.result;
                        $scope.retailerFormModel.ChainOnline = false;
                        $scope.loadRetailModel = true;
                    }
                });
            }

            switch ($scope.currentMode) {
                case "Add":
                    initializeFormData();
                    break;
                case "Edit":
                case "View":
                    getRetailerEditFormData();
                    break;
            }

            $scope.$watch('retailerFormModel.ChainOnline', function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    if (newVal && $scope.currentMode === 'Add') {
                        resetRetailerAddressForm();
                    }
                    else if (newVal && $scope.currentMode === 'Edit' && $scope.isStaff) {
                        resetRetailerAddressForm();
                    }
                }
            });

            function addParticipantRetailerForm() {

                formValidityCheck();
                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.participantRetailerForm.$invalid) return;

                if (angular.isDefined($scope.retailerFormModel)) {
                    retailConnServices.addParticipantRetailerForm($scope.retailerFormModel).then(function (response) {
                        if (response.data.status) {
                            $rootScope.$emit('CANCEL', "cancel");
                            $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRetailerForm");
                        }
                    });
                }
            }

            function addStaffRetailerForm() {

                formValidityCheck();

                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.participantRetailerForm.$invalid) return;

                if (angular.isDefined($scope.retailerFormModel)) {
                    retailConnServices.addStaffRetailerForm($scope.retailerFormModel).then(function (response) {
                        if (response.data.status) {
                            $rootScope.$emit('CANCEL', "cancel");
                            $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRetailerForm");
                        }
                    });
                }
            }

            function resetRetailerAddressForm() {
                $scope.retailerFormModel.AddressLine1 = null;
                $scope.retailerFormModel.AddressLine2 = null;
                $scope.retailerFormModel.City = null;
                $scope.retailerFormModel.Province = null;
                $scope.retailerFormModel.PostalCode = null;
                $scope.retailerFormModel.Country = null;
                $scope.retailerFormModel.PhoneNumber = null;
            }

            function loadFormModel() {
                $scope.textAreaModel = '';
                $scope.allInternalNotes = $scope.retailerFormModel.RetailerNotes;
                $scope.quantity = $scope.allInternalNotes.length;

                if ($scope.quantity > 2) {
                    $scope.showMore = false;
                }
            }

            function getRetailerEditFormData() {
                retailConnServices.getRetailerEditFormData($scope.retailerId).then(function (response) {
                    if (response.data.status) {
                        $scope.retailerFormModel = response.data.result;
                        loadFormModel();
                        changeDbDateTimeforRetailerProd();
                        $scope.loadRetailModel = true;
                        $scope.retailerNameEditFlag = $scope.retailerFormModel.RetailerName;
                    }
                });
            }

            function editParticipantRetailerForm() {
                formValidityCheck();
                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.participantRetailerForm.$invalid) return;

                if (angular.isDefined($scope.retailerFormModel)) {
                    retailConnServices.editParticipantRetailerForm($scope.retailerFormModel).then(function (response) {
                        if (response.data.status) {
                            $scope.retailerNameEditFlag = null;
                            $rootScope.$emit('CANCEL', "cancel");
                            $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRetailerForm");
                        }
                    });
                }
            }

            function editStaffRetailerForm() {

                formValidityCheck();
                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.participantRetailerForm.$invalid) return;

                if (angular.isDefined($scope.retailerFormModel)) {
                    retailConnServices.editStaffRetailerForm($scope.retailerFormModel).then(function (response) {
                        if (response.data.status) {
                            $scope.retailerNameEditFlag = null;
                            $rootScope.$emit('CANCEL', "cancel");
                            $scope.$parent.$emit('PANEL_VIEW_UPDATED', "rcRetailerForm");
                        }
                    });
                }
            }

            function changeDbDateTimeforRetailerProd() {
                //change all the current to datetime
                _.each($scope.retailerFormModel.RetailerNotes, function (item) {
                    if (_.has(item, 'AddedOn')) {
                        item['AddedOn'] = retailConnServices.dateTimeConvert(item.AddedOn);
                    }
                })
            }

            function formValidityCheck() {

                if (!$scope.retailerFormModel.ChainOnline) {
                    if (!$scope.retailerFormModel.Country) {
                        if (!$scope.retailerFormModel.AddressLine1) {
                            $scope.participantRetailerForm.addressLine1.$setValidity("passRequired", false);
                        }
                        if (!$scope.retailerFormModel.City) {
                            $scope.participantRetailerForm.city.$setValidity("passRequired", false);
                        }
                        if (!$scope.retailerFormModel.Province) {
                            $scope.participantRetailerForm.province.$setValidity("passRequired", false);
                        }
                    }
                    else {
                        //canada or US
                        if ($scope.retailerFormModel.Country === 'Canada' || $scope.retailerFormModel.Country === 'United States') {
                            if (!$scope.retailerFormModel.AddressLine1) {
                                $scope.participantRetailerForm.addressLine1.$setValidity("passRequired", false);
                            }
                            if (!$scope.retailerFormModel.City) {
                                $scope.participantRetailerForm.city.$setValidity("passRequired", false);
                            }
                            if (!$scope.retailerFormModel.Province) {
                                $scope.participantRetailerForm.province.$setValidity("passRequired", false);
                            }
                            if (!$scope.retailerFormModel.PostalCode) {
                                $scope.participantRetailerForm.postal.$setValidity("validPostalFormat", false);
                            }
                            if (!$scope.retailerFormModel.PhoneNumber) {
                                $scope.participantRetailerForm.phone.$setValidity("validPhoneFormat", false);
                            }
                        }
                            //other countries
                        else {
                            if (!$scope.retailerFormModel.AddressLine1) {
                                $scope.participantRetailerForm.addressLine1.$setValidity("passRequired", false);
                            }
                            $scope.participantRetailerForm.city.$setValidity("passRequired", true);
                            $scope.participantRetailerForm.province.$setValidity("passRequired", true);

                            $scope.participantRetailerForm.postal.$setValidity("validPostalFormat", true);
                            $scope.participantRetailerForm.phone.$setValidity("validPhoneFormat", true);
                        }
                    }
                }
                angular.forEach($scope.participantRetailerForm, function (formElement, fieldName) {
                    if (fieldName[0] === '$') return;
                    formElement.$setDirty();
                    formElement.$setTouched();
                });
                
            }
        }
    };
}]);
directives.directive('rcRetailerFormAddress', [function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            var isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            var isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");
            if (isStaff) {
                return 'rc-staff-retailer-address.html';
            }
            if (isParticipant) {
                return 'rc-retailer-address.html';
            }
        },
        scope: {
            form: '=',
            currentMode: '=',
            formModel: '='
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $uibModal) {

            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");
            $scope.isAddressFieldRequired = ($scope.formModel.Country == 'Canada' || $scope.formModel.Country == 'United States');
            $scope.countryChange = function () {
                if ($scope.formModel.Country.replace(/\s/g, '') === 'Canada' || $scope.formModel.Country.replace(/\s/g, '') === 'UnitedStates') {
                    $scope.isUSCanada = true;
                    $scope.isOther = false;
                }
                else {
                    $scope.isUSCanada = false;
                    $scope.isOther = true;
                }
                $scope.isAddressFieldRequired = ($scope.formModel.Country == 'Canada' || $scope.formModel.Country == 'United States');
            }
        }
    };
}]);

//participant product form
directives.directive('rcProductForm', ['retailConnServices', '$rootScope', '$http', function (retailConnServices, $rootScope, $http) {
    return {
        restrict: 'E',
        templateUrl: 'rc-product-form.html',
        scope: {
            currentMode: '=',
            productId: '='
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope) {

            $scope.tempUploadStaff = false;

            $scope.editLoad = false;
            $scope.addLoad = false;
            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");
            
            $scope.data = {
                availableOptions: [],
                selectedOption: { id: '0', name: 'Select Category' },
                defaultOption: { id: '0', name: 'Select Category' }
            };

            $rootScope.$on('TEMP_UPLOAD_STAFF', function (e, data) {
                $scope.tempUploadStaff = true;
            });

            $scope.cancel = function () {

                //if staff
                if ($scope.isStaff && $scope.tempUploadStaff && $scope.productFormModel.UploadDocument !== null) {
                    deleteTemporaryFileUpload();
                }
                //if participant and its add mode
                else if ($scope.productFormModel.UploadDocument !== null && $scope.currentMode === "Add" && $scope.isParticipant) {
                    deleteTemporaryFileUpload()
                }
                else {
                    $rootScope.$emit('CANCEL', "cancel");
                }
            };

            //Category dropdown change
            $scope.$watch('data.selectedOption', function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    if ($scope.currentMode || $scope.isStaff) {
                        if (newVal.name !== $scope.data.defaultOption.name) {
                            $scope.productFormModel.CategoryName = newVal.name;
                            $scope.productFormModel.CategoryId = newVal.id;
                        }
                    }
                }
            }, true);

            //submit form
            $scope.submitForm = function (currentMode) {
                switch ($scope.currentMode) {
                    case "Add":
                        addProductForm();
                        break;
                    case "Edit":
                        editProductForm();
                        break;
                    case "View":
                        break;
                }
            }

            switch ($scope.currentMode) {
                case "Add":
                    initializeFormData();
                    break;
                case "Edit":
                case "View":
                    getProductEditFormData($scope.productId);
                    break;
            }

            function initializeFormData() {

                $http.get(Global.RetailConnnection.InitializeProductFormDataUrl).then(function (response) {
                    if (response.data.status) {
                        $scope.productFormModel = response.data.result;

                        getProductCategory().then(function (response) {
                            if (response.data.status) {
                                //populating dropdown category
                                $scope.data.availableOptions = _.map(response.data.result, function (category) { return { id: category.ID, name: category.CategoryName }; });
                                $scope.data.availableOptions.unshift({ id: '0', name: 'Select Category' });

                                changeDbDateTimeforRetailerProd();

                                $scope.addLoad = true;
                            }
                        });
                    }
                });
            }

            function getProductEditFormData(productId) {
                retailConnServices.getProductEditFormData(productId).then(function (response) {
                    if (response.data.status) {
                        $scope.productFormModel = response.data.result;

                        $scope.defaultAttachment = {
                            AttachmentId: $scope.productFormModel.UploadDocument.AttachmentId,
                            ProductId: $scope.productFormModel.UploadDocument.ProductId,
                            IsOTSImage: $scope.productFormModel.UploadDocument.IsOTSImage,
                            IsDefaultImage: $scope.productFormModel.UploadDocument.IsDefaultImage,
                        }
                        
                        getProductCategory().then(function (response) {
                            if (response.data.status) {
                                //populating dropdown category
                                $scope.data.availableOptions = _.map(response.data.result, function (category) { return { id: category.ID, name: category.CategoryName }; });
                                $scope.data.selectedOption.id = $scope.productFormModel.CategoryId;
                                $scope.data.selectedOption.name = $scope.productFormModel.CategoryName;

                                changeDbDateTimeforRetailerProd();

                                $scope.editLoad = true;
                            }
                        });
                    }
                });
            }

            function addProductForm() {

                formValidityCheck();
                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.productForm.$invalid) return;

                if ($scope.productFormModel !== null) {
                    retailConnServices.addProductForm($scope.productFormModel).then(function (response) {
                        if (response.data.status) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED', "ProductForm");
                            $rootScope.$emit('CANCEL', "cancel");
                        }
                    });
                }                               
            }

            function editProductForm() {

                formValidityCheck();
                //fire validation form
                $scope.$broadcast('show-errors-event');
                if ($scope.productForm.$invalid) return;

                if ($scope.isStaff) {
                    deleteStaffDefaultFileUpload();
                }
                else if ($scope.isParticipant) {
                    $scope.productFormModel.UploadDocument = null;
                    retailConnServices.editProductForm($scope.productFormModel, false).then(function (response) {
                        if (response.data.status) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED', "ProductForm");
                            $rootScope.$emit('CANCEL', "cancel");
                        }
                    });
                }                
            }

            function getProductCategory() {
                return $http.get(Global.RetailConnnection.LoadCategoryProductFormUrl);
            }

            function changeDbDateTimeforRetailerProd() {
                //change all the current to datetime
                _.each($scope.productFormModel.RetailerProducts, function (item) {
                    if (_.has(item, 'DateAdded')) {
                        item['DateAdded'] = retailConnServices.dateTimeConvert(item.DateAdded);
                    }
                })
            }

            function deleteTemporaryFileUpload() {
                if ($scope.productFormModel.UploadDocument.AttachmentId && $scope.productFormModel.UploadDocument.ProductId) {

                    retailConnServices.deleteTemporaryProductFile($scope.productFormModel.UploadDocument.AttachmentId, $scope.productFormModel.UploadDocument.ProductId)
                        .then(function (response) {
                            if (response.data.status) {
                                $scope.productFormModel.UploadDocument = null;
                                $rootScope.$emit('CANCEL', "cancel");
                            }
                        });
                } else {
                    $rootScope.$emit('CANCEL', "cancel");
                }
            }

            function deleteStaffDefaultFileUpload() {

                if ($scope.defaultAttachment.AttachmentId && $scope.defaultAttachment.ProductId) {

                    retailConnServices.deleteTemporaryProductFile($scope.defaultAttachment.AttachmentId, $scope.defaultAttachment.ProductId)
                        .then(function (response) {
                            if (response.data.status) {
                                if ($scope.productFormModel !== null) {
                                    retailConnServices.editProductForm($scope.productFormModel, $scope.isStaff).then(function (response) {
                                        if (response.data.status) {
                                            $rootScope.$emit('PANEL_VIEW_UPDATED', "ProductForm");
                                            $rootScope.$emit('CANCEL', "cancel");
                                        }
                                    });
                                }
                            }
                        });
                }
                else {
                    //simple edit
                    if ($scope.productFormModel !== null) {
                        retailConnServices.editProductForm($scope.productFormModel, $scope.isStaff).then(function (response) {
                            if (response.data.status) {
                                $rootScope.$emit('PANEL_VIEW_UPDATED', "ProductForm");
                                $rootScope.$emit('CANCEL', "cancel");
                            }
                        });
                    }
                    $rootScope.$emit('CANCEL', "cancel");
                }
            }
            
            function formValidityCheck() {
                $scope.productForm.category.$setValidity("validCategory", $scope.productFormModel.CategoryId !== 0);

                angular.forEach($scope.productForm, function (formElement, fieldName) {
                    if (fieldName[0] === '$') return;
                    formElement.$setDirty();
                    formElement.$setTouched();
                });
            }
        }
    };
}]);
directives.directive('participantDocumentUploader', ['retailConnServices', '$rootScope', function (retailConnServices, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'documentUploader.html',
        scope: {
            form: '=',
            currentMode: '=',
            formUploadModel: '=',
            productId: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $http) {

            $scope.loadUploadedFile = false;
            $scope.currentSupportingDocument = { url: '', name: '' };

            $scope.$watch('formUploadModel', function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    if ($scope.currentMode === "Edit" || $scope.currentMode === "View") {
                        initializePhotos($scope.formUploadModel.Url, $scope.formUploadModel.Name);
                        $scope.loadUploadedFile = true;
                    }
                }
            }, true);
            $scope.invalidFileFormat = false;

            $scope.onFileSelect = function ($files) {
                //check if file is an image
                if ($files[0] && $files[0].type.indexOf('image') <= -1) {
                    $scope.invalidFileFormat = true;
                    return;
                }
                else {
                    $scope.invalidFileFormat = false;
                }
                if ($scope.formUploadModel === null) {
                    uploadFile($files);
                }
                else {
                    if ($scope.formUploadModel.Name !== $files[0].name) {
                        if ($scope.formUploadModel.AttachmentId && $scope.formUploadModel.ProductId) {
                            retailConnServices.deleteTemporaryProductFile($scope.formUploadModel.AttachmentId, $scope.formUploadModel.ProductId).then(function (response) {
                                if (response.data.status) {
                                    $scope.formUploadModel = null;
                                    uploadFile($files);
                                }
                            });
                        }
                        else {
                            uploadFile($files);
                        }
                    }
                }
            };

            $scope.delete = function ($files) {
                if ($scope.formUploadModel) {
                    retailConnServices.deleteTemporaryProductFile($scope.formUploadModel.AttachmentId, $scope.formUploadModel.ProductId).then(function (response) {
                        if (response.data.status) {
                            $scope.formUploadModel = null;
                            $scope.currentSupportingDocument['url'] = '';
                            $scope.currentSupportingDocument['name'] = '';
                            $scope.loadUploadedFile = false;
                        }
                    });
                }
            }

            function uploadFile(files) {
                $http.uploadFile({
                    url: Global.RetailConnnection.UploadProductFileURL,
                    file: files[0],
                    data: { productId: $scope.productId }
                }).success(function (result) {
                    if (result.status) {
                        $scope.formUploadModel = result.data;
                        initializePhotos($scope.formUploadModel.Url, $scope.formUploadModel.Name);
                        $scope.loadUploadedFile = true;
                    }
                }).error(function (error) {
                });
            }

            function initializePhotos(url, name) {
                $scope.currentSupportingDocument.url = url;
                $scope.currentSupportingDocument.name = name;
            }
        }
    };
}]);
directives.directive('staffDocumentUploader', ['retailConnServices', '$rootScope', function (retailConnServices, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'documentUploader.html',
        scope: {
            form: '=',
            currentMode: '=',
            formUploadModel: '=',
            productId: '@'
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $http) {

            $scope.loadUploadedFile = false;
            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");
            $scope.currentSupportingDocument = { url: '', name: '' };

            $scope.onFileSelect = function ($files) {
                if ($scope.formUploadModel === null) {
                    uploadFile($files);
                }

                else {
                    if ($scope.formUploadModel.Name !== $files[0].name) {

                        if ($scope.formUploadModel.IsDefaultImage) {
                            //if default image..DONT DELETE from the attachment table
                            uploadFile($files);
                        }
                        else if (!$scope.formUploadModel.IsDefaultImage && $scope.formUploadModel.AttachmentId && $scope.formUploadModel.ProductId) {
                            //if not default image..delete from the attachment table
                            retailConnServices.deleteTemporaryProductFile($scope.formUploadModel.AttachmentId, $scope.formUploadModel.ProductId).then(function (response) {
                                if (response.data.status) {
                                    $scope.formUploadModel = null;
                                    uploadFile($files);
                                }
                            });
                        }
                        else {
                            uploadFile($files);
                        }
                    }
                }
            };

            $scope.delete = function ($files) {
                if ($scope.formUploadModel) {

                    //if removing default image
                    if ($scope.formUploadModel.IsDefaultImage) {
                        resetPhotos();
                        $scope.formUploadModel.IsDefaultImage = false;
                    }
                    //if removing OTS image
                    else if ($scope.formUploadModel.IsOTSImage) {
                        resetPhotos();
                        $scope.formUploadModel.IsDefaultImage = false;
                    }
                    //if removing regular images delete
                    else if (angular.isDefined($scope.formUploadModel.AttachmentId) && $scope.formUploadModel.AttachmentId) {
                        retailConnServices.deleteTemporaryProductFile($scope.formUploadModel.AttachmentId, $scope.formUploadModel.ProductId).then(function (response) {
                            if (response.data.status) {
                                resetPhotos();
                            }
                        });
                    }
                }
            }

            initialize();

            function initialize() {
                initializePhotos($scope.formUploadModel.Url, $scope.formUploadModel.Name);
                $scope.loadUploadedFile = true;
            }

            function uploadFile(files) {
                $http.uploadFile({
                    url: Global.RetailConnnection.UploadProductFileURL,
                    file: files[0],
                    data: { productId: $scope.productId }
                }).success(function (result) {
                    if (result.status) {
                        $scope.formUploadModel = result.data;
                        initializePhotos($scope.formUploadModel.Url, $scope.formUploadModel.Name);
                        $scope.loadUploadedFile = true;
                        $scope.formUploadModel.IsDefaultImage = false;
                        $rootScope.$emit('TEMP_UPLOAD_STAFF', "tempupload");
                    }
                }).error(function (error) {
                });
            }

            function initializePhotos(url, name) {
                $scope.currentSupportingDocument.url = url;
                $scope.currentSupportingDocument.name = name;
            }

            function resetPhotos() {
                $scope.formUploadModel = null;
                $scope.currentSupportingDocument['url'] = '';
                $scope.currentSupportingDocument['name'] = '';
                $scope.loadUploadedFile = false;
                $rootScope.$emit('TEMP_UPLOAD_STAFF', "tempupload");
            }
        }
    };
}]);


directives.directive('rcProductRetailerSection', ['retailConnServices', '$rootScope', function (retailConnServices, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'rc-product-retailer-section.html',
        scope: {
            form: '=',
            currentMode: '=',
            formProductRetailerModel: '='
        },
        link: function (scope, el, attrs, formCtrl) { },
        controller: function ($scope, $http, $uibModal) {

            $scope.message = "";
            if ($scope.currentMode=="View") {
                $scope.message = "tableForProductRetailerSection";
            }

            $scope.isStaff = (Global.RetailConnnection.Settings.isStaff == "True");
            $scope.isParticipant = (Global.RetailConnnection.Settings.isParticipant == "True");

            $scope.retailerDropDownChange = function (description, id) {
                $scope.selectedRetailerName = description;
                $scope.selectedRetailer.RetailerId = id;
                $scope.selectedRetailer.RetailerName = description;
            }

            //to update url model value for form validation
            $scope.$watch('form.selectedUrl.$modelValue', function (newVal, oldVal) {
                if (newVal != oldVal) {
                    $scope.selectedUrl = newVal;
                }
            }, true);

            //Add Retailer
            $scope.addRetailer = function () {
                $scope.selectedRetailer.ProductUrl = $scope.selectedUrl;
                $scope.selectedRetailer.DateAdded = retailConnServices.getCurrentDateTime();

                $scope.formProductRetailerModel.push($scope.selectedRetailer);
                $scope.retailerItems = _.without($scope.retailerItems, _.findWhere($scope.retailerItems, { Id: $scope.selectedRetailer.RetailerId }));
                initializeAdd();
            }

            //Remove Retailer
            $scope.removeRetailer = function (retailerItem) {
                var removeInstance = $uibModal.open({
                    templateUrl: 'rc-product-rtl-modal.html',
                    controller: 'RemoveProductRtlCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        removeContent: function () {
                            return {
                                retailer: retailerItem
                            };
                        }
                    }
                });
                removeInstance.result.then(function (response) {
                    $scope.formProductRetailerModel = _.without($scope.formProductRetailerModel, _.findWhere($scope.formProductRetailerModel, { RetailerId: response.RetailerId }));
                    $scope.retailerItems.push({ Id: response.RetailerId, Name: response.RetailerName });
                    $scope.retailerItems = _.sortBy($scope.retailerItems, function (i) { return i.Name.toLowerCase(); });
                });
            }

            //Edit Retailer
            $scope.editRetailer = function (retailerItem) {
                initializeEdit(retailerItem);
            }

            $scope.cancelUpdate = function () {
                initializeAdd();
            }

            $scope.updateRetailer = function () {
                $scope.selectedRetailer.ProductUrl = $scope.selectedUrl;
                $scope.selectedRetailer.DateAdded = retailConnServices.getCurrentDateTime();

                _.map($scope.formProductRetailerModel, function (item) {
                    if (item.RetailerId === $scope.selectedRetailer.RetailerId) {
                        item.DateAdded = $scope.selectedRetailer.DateAdded;
                    }
                });
                initializeAdd();
            }

            $scope.linkClick = function (url) {
                if (!/^(f|ht)tps?:\/\//i.test(url)) {
                    url = "http://" + url;
                }                
                window.open(url, '_blank');
            }

            //first time initialization
            switch ($scope.currentMode) {
                case "Add":
                    Initialize();
                    break;
                case "Edit":
                case "View":
                    EditInitialize();
                    break;
            }

            function Initialize() {
                initializeAdd();
                getRetailersForProduct().then(function (response) {
                    if (response.data.status) {
                        $scope.retailerItems = _.sortBy(response.data.result, function (i) { return i.Name.toLowerCase(); });
                    }
                });
            }

            function EditInitialize() {
                initializeAdd();
                getRetailersForProduct().then(function (response) {
                    if (response.data.status) {
                        $scope.retailerItems = _.reject(response.data.result, function (item) {
                            return _.find($scope.formProductRetailerModel, function (r) { return r.RetailerId === item.Id });
                        });
                        $scope.retailerItems = _.sortBy(response.data.result, function (i) { return i.Name.toLowerCase(); });
                    }
                });
            }

            function initializeAdd() {
                $scope.editSection = false;
                $scope.addSection = true;
                resetHeader();
            }

            function initializeEdit(editRetailerItem) {
                $scope.editSection = true;
                $scope.addSection = false;
                editHeader(editRetailerItem);
            }

            function resetHeader() {
                $scope.selectedRetailerName = "Select Retailer";
                $scope.selectedUrl = null;
                $scope.selectedRetailer = null;
                $scope.selectedRetailer = { RetailerId: '', ProductUrl: '', DateAdded: '', AddedBy: Global.RetailConnnection.Settings.AddedBy, RetailerName: '' };
            }

            function editHeader(editRetailerItem) {
                $scope.selectedRetailerName = editRetailerItem.RetailerName;
                $scope.selectedUrl = editRetailerItem.ProductUrl;
                $scope.selectedRetailer = editRetailerItem;
            }

            function getRetailersForProduct() {
                return $http.get(Global.RetailConnnection.GetRetailersForProductUrl);
            }

        }
    }
}]);

directives.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {

            var content = el[0].nextElementSibling.innerHTML;

            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

//validation directives
directives.directive('atLeastOneCharacter', [function () {

    var REGEX = /^\d*[a-zA-Z][a-zA-Z0-9]*$/;

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {
                if (REGEX.test(viewValue)) {
                    ngModelCtrl.$setValidity('isOneChar', true);
                    return viewValue;
                } else {
                    ngModelCtrl.$setValidity('isOneChar', false);
                    return undefined;
                }
            });
        }
    };
}]);

directives.directive('isCustomRequired', [function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {

                if (scope.isUSCanada) {
                    if (viewValue) {
                        ngModelCtrl.$setValidity('passRequired', true);
                        return viewValue;
                    } else {
                        ngModelCtrl.$setValidity('passRequired', false);
                        return undefined;
                    }
                }
                else {
                    ngModelCtrl.$setValidity('passRequired', true);
                    return viewValue;
                }

            });
        }
    };
}]);

directives.directive('validatePhoneNumber', [function () {

    var REGEX = /^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$/;

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$validators.validPhoneFormat = function (modelValue, viewValue) {
                if (REGEX.test(viewValue)) {
                    return true
                }
                return false;
            };
        }
    }
}]);

directives.directive('validatePostal', [function () {

    var REGEX = /^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$/;

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$validators.validPostalFormat = function (modelValue, viewValue) {
                if (REGEX.test(viewValue)) {
                    return true
                }
                return false;
            };
        }
    }
}]);

directives.directive('validateCountry', [function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {

                if (viewValue.name.replace(/\s/g, '') !== 'SelectCountry') {
                    ngModelCtrl.$setValidity('validCountry', true);
                    return viewValue;
                } else {
                    ngModelCtrl.$setValidity('validCountry', false);
                    return viewValue;
                }
            });
        }
    }
}]);

directives.directive('validateCategory', [function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {

                if (viewValue.name.replace(/\s/g, '') !== 'SelectCategory') {
                    ngModelCtrl.$setValidity('validCategory', true);
                    return viewValue;
                } else {
                    ngModelCtrl.$setValidity('validCategory', false);
                    return viewValue;
                }
            });
        }
    }
}]);

directives.directive('dynamicRetailerValidation', [function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {

                ngModelCtrl.$setValidity('urlrequired', false);
                return viewValue;
            });
        }
    }
}]);

directives.directive('retailerInternalNotes', ['$http', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'retail-connection-internal-notes.html',
        scope: true,
        link: function (scope, elem, attr, ngModel) {
            scope.focus = function () {
                scope.focusVal = true;
                scope.searchStyle = 'visibility: visible;';
            };

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = 'visibility: hidden;';
                    scope.searchNote = '';
                }
                else {
                    scope.focusVal = true;
                }
            };

            scope.search = function () {
                scope.internalNoteLength = scope.filtered.length;
            };

            scope.add = function () {
                if (scope.textAreaModel) {
                    var req = {
                        id: scope.retailerId,
                        notes: scope.textAreaModel
                    };
                    $http({
                        url: Global.InternalNoteSettings.AddInternalNotesUrl,
                        method: "POST",
                        data: JSON.stringify(req)
                    }).success(function (result) {
                        $http({
                            url: Global.InternalNoteSettings.LoadInternalNotesUrl,
                            method: "POST",
                            data: JSON.stringify({ id: scope.retailerId })
                        }).success(function (result) {
                            scope.textAreaModel = '';
                            scope.allInternalNotes = result;
                            scope.quantity = scope.allInternalNotes.length;

                            if (scope.quantity > 2) {
                                scope.showMore = false;
                            }
                        });
                    });
                }
            };

            scope.stripNotes = function (note) {
                if (note.length > 72) {
                    return note.substr(0, 72) + '...';
                }
                else {
                    return note;
                }
            };
        },
        controller: function ($scope) {

        }
    };
}]);

directives.directive('messageHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {

    var header = '<span>Retailer Notes</span>' +
        '<button type="button" class="close" ng-click="closeMe()")><span aria-hidden="true">×</span></button>';

    var getHeader = function (contentType) {
        return $templateCache.get('retailerNotePopoverHeader.html');
    };

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;
            var note = toHtml(content);
            
            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    if(res[i]){
                        result = result + '<p>' + res[i] + '</p>';
                    }                    
                }
                return result;
            }

            $(element).webuiPopover({
                width: '500',
                height: '300',
                padding: true,
                multi: true,
                closeable: true,
                title: 'Retailer Notes',
                type: 'html',
                trigger: 'hover',
                content: function () {
                    return $compile(note)(scope);
                },
                delay: { show: 100, hide: 100 },
            });
        },
        controller: function ($scope, $element) { }
    };
});

directives.directive('rcCommonExport', [function () {
    return {
        restrict: 'E',
        templateUrl: 'anchorExport.html',
        scope: {
            rcType: '='
        },
        link: function (scope, element, attr, ngModel) {
            scope.left = attr.attrAnchor;
            if (scope.rcType.replace(/\s/g, '') == "ProductCategory") {
                scope.left = "180px"
            }
            scope.anchorCss = {
                "background": "none",
                "left": scope.left,
                "position": "absolute",
                "top": "11px",
                "width": "25px"
            };

            element.bind('click', function (event) {
                event.stopPropagation();
                event.preventDefault();

                switch (scope.rcType.replace(/\s/g, '')) {
                    case "ParticipantRetailer":
                        window.location = Global.RetailConnnection.ExportParticipantRetailerUrl;
                        break;
                    case "ParticipantProduct":
                        window.location = Global.RetailConnnection.ExportParticipantProductUrl;
                        break;
                    case "StaffRetailer":
                        window.location = Global.RetailConnnection.ExportStaffRetailerUrl;
                        break;
                    case "StaffProduct":
                        window.location = Global.RetailConnnection.ExportStaffProductUrl;
                        break;
                    case "ProductCategory":
                        window.location = Global.RetailConnnection.ExportProductCategoryUrl;
                        break;
                }
            });
        },
        controller: function ($scope, $element) { }
    };
}]);

directives.directive('internalNoteHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {

    var getTemplate = function (contentType) {
        return $templateCache.get('internalNotePopoverTemplate.html');
    };

    var header = '<span>Notes</span>' +
        '<button type="button" class="close" ng-click="closeMe()")><span aria-hidden="true">×</span></button>';

    var getHeader = function (contentType) {
        return $templateCache.get('internalNotePopoverHeader.html');
    };

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var note = toHtml(attrs.note);
            $templateCache.put('internalNotePopoverTemplate.html', '<div style="overflow: auto; height: 300px; padding:true;">' + note + '</div>');
            $templateCache.put('internalNotePopoverHeader.html', header);


            var content = getTemplate();
            var popoverheader = getHeader();

            $rootScope.insidePopover = false;
            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'top',
                title: function () {
                    return $compile(popoverheader)(scope);
                },
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });

            scope.closeMe = function () {
                $(element).popover('hide');
            }

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    result = result + '<p>' + res[i] + '</p>';
                }
                return result;
            }

        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});

directives.directive('columnHoverPopover', function ($compile, $timeout, $rootScope) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});


