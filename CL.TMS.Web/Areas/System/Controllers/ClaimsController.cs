﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.UserInterface;
using Microsoft.AspNet.Identity;
using CL.TMS.DataContracts.ViewModel.Claims;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CL.TMS.Web.Infrastructure;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataContracts.ViewModel.Claims.Export;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Framework.DTO;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;
using System.Globalization;
using CL.TMS.UI.Common.Security;
using System.Threading.Tasks;
using CL.TMS.UI.Common.Helper;
using System.Net.Mail;
using System.Net.Mime;
using CL.Framework.Logging;
using System.IO;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class ClaimsController : BaseController
    {

        private IClaimService claimService;
        private IUserService userService;
        private IMessageService messageService;

        public ClaimsController(IClaimService claimService, IUserService userService, IMessageService messageService)
        {
            this.claimService = claimService;
            this.userService = userService;
            this.messageService = messageService;
        }

        public ActionResult Index()
        {
            ViewBag.SelectedMenu = "Claims";
            return ApplyRoutingLogic();
        }

        private ActionResult ApplyRoutingLogic()
        {
            ViewBag.RegistrationNumber = string.Empty;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;
                ViewBag.RegistrationNumber = vendor.RegistrationNumber;

                if (vendor.IsVendor)
                {
                    if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
                    {
                        return RedirectToAction("Index", "Claims", new { area = "Hauler" });
                    }
                    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
                    {
                        return RedirectToAction("Index", "Claims", new { area = "Collector" });
                    }
                    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
                    {
                        return RedirectToAction("Index", "Claims", new { area = "RPM" });
                    }
                    else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
                    {
                        return RedirectToAction("Index", "Claims", new { area = "Processor" });
                    }
                }
                //By default
                return RedirectToAction("Index", "Claims", new { area = "Hauler" });
            }

            return View("StaffAllClaims");
        }

        public ActionResult ClaimsTypeRouting(int claimId, string regNumber)
        {
            var vendorReference = claimService.GetVendorRefernce(regNumber);

            if (vendorReference != null)
            {
                //TODO: redirect to error if vendorreference is null
                if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                {
                    Session["SelectedVendor"] = vendorReference;
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            switch (vendorReference.VendorType)
            {
                case (int)ClaimType.Steward: // 1: //TreadMarksConstants.Steward
                    return RedirectToAction("StaffIndex", "TSFRemittances", new { area = "Steward", id = claimId });
                case (int)ClaimType.Collector: // 2: //TreadMarksConstants.Collector
                    return RedirectToAction("ClaimSummary", "Claims", new { area = "Collector", claimId = claimId });
                case (int)ClaimType.Hauler: // 3: //TreadMarksConstants.Hauler
                    return RedirectToAction("ClaimSummary", "Claims", new { area = "Hauler", claimId = claimId });
                case (int)ClaimType.Processor: // 4: //TreadMarksConstants.Processor
                    return RedirectToAction("ClaimSummary", "Claims", new { area = "Processor", claimId = claimId });
                case (int)ClaimType.RPM: // 5: //TreadMarksConstants.RPM
                    return RedirectToAction("ClaimSummary", "Claims", new { area = "RPM", claimId = claimId });
                default:
                    return RedirectToAction("ClaimSummary", "Claims", new { area = "Hauler", claimId = claimId });
            }
        }

        public ActionResult UpdateClaimOnhold(int claimId, bool isOnhold)
        {
            var claimReviewDueDate = claimService.UpdateClaimOnhold(claimId, isOnhold);
            var updatedDate = DateTime.UtcNow;
            var result = new
            {
                updatedDate = updatedDate,
                claimReviewDueDate = claimReviewDueDate
            };
            return new NewtonSoftJsonResult(result);
        }

        public ActionResult UpdateAuditOnhold(int claimId, bool isOnhold)
        {
            var auditReviewDueDate = claimService.UpdateAuditOnhold(claimId, isOnhold);
            var updatedDate = DateTime.UtcNow;
            var result = new
            {
                updatedDate = updatedDate,
                claimReviewDueDate = auditReviewDueDate
            };
            return new NewtonSoftJsonResult(result);
        }

        public ActionResult LoadClaimWorkflowUsers(string Workflow, string AccountName)
        {
            var result = claimService.LoadClaimWorkflowUsers(Workflow, AccountName);
            return new NewtonSoftJsonResult(result);
        }

        public ActionResult LoadClaimWorkflowViewModel(int claimId, decimal? claimsAmountTotal = null)
        {
            var result = claimService.LoadClaimWorkflowViewModel(claimId, claimsAmountTotal);
            return new NewtonSoftJsonResult(result);
        }

        public ActionResult UpdateClaimWorkflowStatus(int claimId, long reviewedBy, int fromStatus, int toStatus)
        {
            claimService.UpdateClaimWorkflowStatus(claimId, reviewedBy, fromStatus, toStatus);
            //Add notification
            var model = claimService.LoadClaimWorkflowViewModel(claimId);
            var assigner = SecurityContextHelper.CurrentUser;
            var assignee = userService.FindUserByUserId(reviewedBy);
            var assigneeUser = new UserReference
            {
                Id = assignee.ID,
                Email = assignee.Email,
                FirstName = assignee.FirstName,
                LastName = assignee.LastName,
                UserName = assignee.UserName
            };

            var claim = claimService.FindClaimById(claimId);
            if (model.IsApprover2BtnVisible)
            {
                if (toStatus == (int)ClaimStatus.AuditHold) // 5)
                {
                    AddNotification(assigner, assigneeUser, claim, false);
                }
                else
                {
                    AddNotification(assigner, assigneeUser, claim, true);
                }
            }
            else if (model.IsApprover1BtnVisible)
            {
                if (toStatus == (int)ClaimStatus.AuditReview) //4)
                {
                    AddNotification(assigner, assigneeUser, claim, false);
                }
                else
                {
                    AddNotification(assigner, assigneeUser, claim, true);
                }
            }
            else
            {
                if (toStatus == (int)ClaimStatus.Onhold) // 3)
                {
                    AddNotification(assigner, assigneeUser, claim, false);
                }
                else
                {
                    AddNotification(assigner, assigneeUser, claim, true);
                }
            }

            //OTSTM2-551 Add Activity
            AddActivity(assigner, assigneeUser, claim, fromStatus, toStatus, false, null);

            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateHST(int claimID,decimal HST, bool IsTaxApplicable = true)
        {           
            claimService.UpdateHST(claimID, HST, IsTaxApplicable);

            return Json(new { status = "refresh" });
        }

        public ActionResult ApproveClaim(int claimId, int fromStatus, int toStatus)
        {
            string result=claimService.ApproveClaim(claimId, fromStatus, toStatus);
            if (!string.IsNullOrEmpty(result)) { return Json(new { status = result }, JsonRequestBehavior.AllowGet); }
            //OTSTM2-551 Add Activity
            var model = claimService.LoadClaimWorkflowViewModel(claimId);
            var initiator = SecurityContextHelper.CurrentUser;
            var claim = claimService.FindClaimById(claimId);
            AddActivity(initiator, null, claim, fromStatus, toStatus, false, null);

            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        //OTSTM2-1084 validate claim amount, threshold and assignee before approving
        public ActionResult ConcurrentUsersValidationBeforeApproving(int claimId, int fromStatus)
        {
            var result = claimService.ConcurrentUsersValidationBeforeApproving(claimId, fromStatus);           

            return Json(new { isWarningModalPopup = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClaimBackToParticipant(int claimId)
        {
            var model = claimService.LoadClaimWorkflowViewModel(claimId);  //OTSTM2-551
            claimService.ClaimBackToParticipant(claimId);

            //OTSTM2-551 Add Activity           
            var initiator = SecurityContextHelper.CurrentUser;
            var claim = claimService.FindClaimById(claimId);
            if (model.ClaimWorkflowStatus == ClaimWorkflowStatus.Representative)
            {
                AddActivity(initiator, null, claim, 1, 0, false, null);
            }
            else if (model.ClaimWorkflowStatus == ClaimWorkflowStatus.Lead)
            {
                AddActivity(initiator, null, claim, 2, 0, false, null);
            }

            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateWorkflowProcess(int claimId)
        {
            var result = claimService.ValidateWorkflowProcess(claimId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateClaimMailDate(int claimId, DateTime mailDate)
        {
            claimService.UpdatedClaimMailDate(claimId, mailDate);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        #region Internal Adjustments
        public JsonResult InventoryAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            var result = claimService.AddInventoryAdjustment(claimId, modalResult);
            if (result)
            {
                Thread.Sleep(600); // waiting for the claim recalculation
                return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RemoveInternalAdjustment(int claimId, int adjustmentType, int internalAdjustmentId)
        {
            claimService.RemoveInternalAdjustment(claimId, adjustmentType, internalAdjustmentId);
            Thread.Sleep(600); // waiting for the claim recalculation
            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInternalAdjustment(int adjustmentType, int internalAdjustmentId)
        {
            var result = claimService.GetClaimAdjustmentModalResult(adjustmentType, internalAdjustmentId);
            return new NewtonSoftJsonResult(result);
        }

        public JsonResult EditInternalAdjustment(int claimId, ClaimAdjustmentModalResult modalResult)
        {
            claimService.EditInternalAdjustment(claimId, modalResult);
            //Thread.Sleep(600); // waiting for the claim recalculation
            return Json(new { status = "refresh" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetListHandlerInternalAdjustment(DataGridModel param, int claimId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "AdjustmentDate"},
                {1, "AdjustmentType"},
                {2, "Note"},
                {3, "AdjustmentBy" }
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var internalAdjustments = claimService.LoadClaimInternalAdjustments(param.Start, param.Length, searchText, orderBy, sortDirection, claimId, SecurityContextHelper.IsStaff());
            internalAdjustments.DTOCollection.ForEach(i =>
            {
                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
            });

            var json = new
            {
                draw = param.Draw,
                recordsTotal = internalAdjustments.TotalRecords,
                recordsFiltered = internalAdjustments.TotalRecords,
                data = internalAdjustments.DTOCollection
            };

            return new NewtonSoftJsonResult(json, false);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcelInternalAdjustment(int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            string sVendorType = string.Empty;

            if (vendor.IsVendor)
            {
                if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
                {
                    sVendorType = "Hauler";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
                {
                    sVendorType = "Collector";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
                {
                    sVendorType = "RPM";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
                {
                    sVendorType = "Processor";
                }
            }

            var sortcolumn = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 1) ? Request.QueryString[1] : string.Empty;

            var columns = new List<string>();
            columns.Add("Date");
            columns.Add("Type");
            if (SecurityContextHelper.IsStaff())
            {
                columns.Add("Notes");
            }
            columns.Add("Added By");

            List<Func<ClaimInternalAdjustment, string>> delegates;
            if (SecurityContextHelper.IsStaff())
            {
                delegates = new List<Func<ClaimInternalAdjustment, string>>() { u => u.AdjustmentDate.ToString(TreadMarksConstants.DateFormat), u => u.AdjustmentType, u => u.Note, u => u.AdjustmentBy };
            }
            else
            {
                delegates = new List<Func<ClaimInternalAdjustment, string>>() { u => u.AdjustmentDate.ToString(TreadMarksConstants.DateFormat), u => u.AdjustmentType, u => u.AdjustmentBy };
            }

            var internalAdjustments = claimService.LoadClaimInternalAdjustments(0, int.MaxValue, searchText, sortcolumn, "1", claimId, SecurityContextHelper.IsStaff());

            internalAdjustments.DTOCollection.ForEach(i =>
            {
                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
                i.Note = i.NotesAllText.ToString().Replace(" <hr>", "; ");
            });

            string csv = internalAdjustments.DTOCollection.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format(sVendorType + "Claims-InternalAdjustment-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        //OTSTM2-270 Add Internal Notes for View Only for Internal Adjustment
        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandlerInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, string notes)
        {
            var note = claimService.AddNotesHandlerInternalAdjustment(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId, notes);
            return new NewtonSoftJsonResult(note, false);
        }

        [HttpGet]
        public ActionResult GetInternalNotesInternalAdjustment(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId)
        {
            var result = claimService.GetInternalNotesInternalAdjustment(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId);
            result.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelInternalAdjustmentNotes(int? ClaimInternalAdjustmentId, int? ClaimInternalPaymentAdjustId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            var claimNotes = claimService.ExportToExcelInternalAdjustmentNotes(ClaimInternalAdjustmentId, ClaimInternalPaymentAdjustId, searchText, sortcolumn, sortReverse);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }


        #endregion

        #region TransactionAdjustment
        [HttpGet]
        public JsonResult GetListHandlerTransactionAdjustment(DataGridModel param, int claimId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "TransactionType"},
                {1, "TransactionNumber"},
                {2, "TransactionDate"},
                {3, "StartedBy"},
                {4, "AdjustmentDate"},
                {5, "Status"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var transactionAdjustments = claimService.LoadClaimTransactionAdjustment(param.Start, param.Length, searchText, orderBy, sortDirection, claimId);

            transactionAdjustments.DTOCollection.ForEach(i =>
            {
                ///For a paper form transaciton, during creation, only date information is saved to Transaction.TransactionDate, time value for Transaction.TransactionDate is 
                ///set to 12:00:00 midnight (00:00:00), this means Transaction.TransactionDate doesn't consider time zone info, may be another bug. -Y
                ///Transaction.CreatedDate has full Datetime info.
                //i.TransactionDate = ConvertTimeFromUtc(i.TransactionDate);

                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
            });

            //Populate action menu
            //PopulateGearActionMenu(transactionAdjustments.DTOCollection);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = transactionAdjustments.TotalRecords,
                recordsFiltered = transactionAdjustments.TotalRecords,
                data = transactionAdjustments.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private void PopulateGearActionMenu(List<TransactionAdjustmentViewModel> transactionAdjustments)
        {
            // 0 - No menu
            // 1 - Recall
            // 2 - Accept, Reject
            // 3 - Reject
            // 4 - Accept
            // 5 - View
            var currentVendor = SecurityContextHelper.CurrentVendor;
            if (SecurityContextHelper.IsStaff())
            {
                transactionAdjustments.ForEach(c =>
                {
                    c.ActionMenu = 0;
                    if (c.IsAdjustByStaff)
                    {
                        switch (c.Status.ToLower())
                        {
                            case "pending":
                                if (currentVendor.VendorId != c.CreatedVendorId)
                                    c.ActionMenu = 2;
                                else
                                    c.ActionMenu = 1;
                                break;
                            case "accepted":
                                c.ActionMenu = 3;
                                break;
                            case "rejected":
                            case "systemrejected":
                                c.ActionMenu = 5;
                                break;
                        }
                    }
                    else
                    {
                        switch (c.Status.ToLower())
                        {
                            case "pending":
                                if (currentVendor.VendorId != c.CreatedVendorId)
                                    c.ActionMenu = 2;
                                else
                                    c.ActionMenu = 1;
                                break;
                            case "accepted":
                                c.ActionMenu = 3;
                                break;
                            case "rejected":
                            case "systemrejected":
                                c.ActionMenu = 5;
                                break;
                        }
                    }
                });
            }
            else
            {
                transactionAdjustments.ForEach(c =>
                {
                    c.ActionMenu = 0;
                    if (c.IsAdjustByStaff)
                    {
                        if (c.Status.ToLower() == "pending")
                        {
                            if (currentVendor.VendorId != c.CreatedVendorId)
                                c.ActionMenu = 2;
                            else
                                c.ActionMenu = 1;
                        }
                    }
                    else
                    {
                        if (c.Status.ToLower() == "pending")
                        {
                            if (currentVendor.VendorId != c.CreatedVendorId)
                                c.ActionMenu = 2;
                            else
                                c.ActionMenu = 1;
                        }
                    }
                });
            }
        }

        [HttpGet]
        public NewtonSoftJsonResult IsUnderCurrentClaim(long friendlyId, int claimId)
        {
            //#1172 filter UCR transactions for collector.
            var result = claimService.GetTransactionIdByFriendlyId(friendlyId, claimId);

            if (result != null) return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = result } };

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }
        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcelTransactionAdjustment(int claimId, string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            string sVendorType = string.Empty;

            if (vendor.IsVendor)
            {
                if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Hauler").DefinitionValue)
                {
                    sVendorType = "Hauler";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Collector").DefinitionValue)
                {
                    sVendorType = "Collector";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "RPM").DefinitionValue)
                {
                    sVendorType = "RPM";
                }
                else if (vendor.VendorType == AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.VendorType, "Processor").DefinitionValue)
                {
                    sVendorType = "Processor";
                }
            }

            var sortcolumn = (Request.QueryString.Count > 2) ? Request.QueryString[2] : string.Empty;
            var sortdirection = (Request.QueryString.Count > 3) ? Request.QueryString[3] : string.Empty;
            var search = (Request.QueryString.Count > 1) ? Request.QueryString[1] : string.Empty;

            var columns = new List<string>();
            columns.Add("Type");
            columns.Add("Trans.#");
            columns.Add("Date");
            columns.Add("Started By");
            columns.Add("Adj. Date");
            columns.Add("Status");
            List<Func<TransactionAdjustmentViewModel, string>> delegates = new List<Func<TransactionAdjustmentViewModel, string>>() {
                u => u.TransactionType.ToString(),
                u => u.TransactionNumber,
                u => u.TransactionDate.ToString(TreadMarksConstants.DateFormat),
                u => u.StartedBy,
                u => u.AdjustmentDate.ToString(TreadMarksConstants.DateFormat),
                u => u.Status };

            var transactionAdjustments = claimService.LoadClaimTransactionAdjustment(0, int.MaxValue, searchText, sortcolumn, "1", claimId);

            transactionAdjustments.DTOCollection.ForEach(i =>
            {
                i.AdjustmentDate = ConvertTimeFromUtc(i.AdjustmentDate);
            });

            string csv = transactionAdjustments.DTOCollection.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format(sVendorType + "Claims-TransactionAdjustment-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Staff All Claims Assignment
        [HttpGet]
        public JsonResult GetListHandler(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                 : string.Empty;

            //OTSTM2-904           
            var columnSearchText = new Dictionary<string, string>
            {
                {"RegNumber", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                {"Type", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                {"Company", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                {"Period", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                {"Submitted",Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                {"ChequeDueDate", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                {"TotalClaimAmount", Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
                {"Status", Request.QueryString.GetValues("columns[7][search][value]").FirstOrDefault()},
                {"HoldStatus", Request.QueryString.GetValues("columns[8][search][value]").FirstOrDefault()},
                {"AssignedTo",Request.QueryString.GetValues("columns[9][search][value]").FirstOrDefault()},
            };

            var columns = new Dictionary<int, string>
            {
                {0, "RegNumber"},
                {1, "Type"},
                {2, "Company"},
                {3, "Period"},
                {4, "Submitted"},
                {5, "ChequeDueDate"},
                {6, "TotalClaimAmount"},
                {7, "Status"},
                {8, "HoldStatus"},
                {9, "AssignedTo"},
                {10, "ClaimsActions"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var claims = claimService.LoadAllClaims(param.Start, param.Length, searchText, orderBy, sortDirection, columnSearchText);

            //claims.DTOCollection.ForEach(i =>
            //        {
            //            i.PaymentDue = (null == i.PaymentDue) ? null : ConvertTimeFromUtc(i.PaymentDue);
            //        });

            var json = new
            {
                draw = param.Draw,
                recordsTotal = claims.TotalRecords,
                recordsFiltered = claims.TotalRecords,
                data = claims.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAssignReassignHandler(string userid, string claimid)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int uid, cid;
            if (Int32.TryParse(userid, out uid) && Int32.TryParse(claimid, out cid))
            {
                this.claimService.UpdateAssignToUserIdForClaim(uid, cid);

                //Add notification
                var assigner = SecurityContextHelper.CurrentUser;
                var assignee = userService.FindUserByUserId(uid);
                var assigneeUser = new UserReference
                {
                    Id = assignee.ID,
                    Email = assignee.Email,
                    FirstName = assignee.FirstName,
                    LastName = assignee.LastName,
                    UserName = assignee.UserName
                };

                var claim = claimService.FindClaimById(cid);
                AddNotification(assigner, assigneeUser, claim, true);

                //OTSTM2-551 Add Activity
                var model = claimService.LoadClaimWorkflowViewModel(cid);
                switch (model.ClaimWorkflowStatus)
                {
                    case ClaimWorkflowStatus.Representative:
                        AddActivity(assigner, assigneeUser, claim, 1, 1, true, true);
                        break;
                    case ClaimWorkflowStatus.Lead:
                        AddActivity(assigner, assigneeUser, claim, 2, 2, true, true);
                        break;
                    case ClaimWorkflowStatus.Supervisor:
                        AddActivity(assigner, assigneeUser, claim, 3, 3, true, true);
                        break;
                    case ClaimWorkflowStatus.Approver1:
                        AddActivity(assigner, assigneeUser, claim, 4, 4, true, true);
                        break;
                    case ClaimWorkflowStatus.Approver2:
                        AddActivity(assigner, assigneeUser, claim, 5, 5, true, true);
                        break;
                }
                return Json(new { status = "" }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUserList(string claimType, string workflowName)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            switch (claimType)
            {
                case "1":
                    claimType = TreadMarksConstants.StewardAccount;
                    break;
                case "2":
                    claimType = TreadMarksConstants.CollectorAccount;
                    break;
                case "3":
                    claimType = TreadMarksConstants.HaulerAccount;
                    break;
                case "4":
                    claimType = TreadMarksConstants.ProcessorAccount;
                    break;
                case "5":
                    claimType = TreadMarksConstants.RPMAccount;
                    break;
                default:
                    break;
            }
            int result;
            if (Int32.TryParse(claimType, out result))
            {
                var allUsers = userService.GetClaimWorkflowUsers(claimType, workflowName).Select(a => new { Name = a.FirstName + " " + a.LastName, ID = a.ID });
                return Json(allUsers, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUnassignHandler(string claimId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            int cid;
            if (Int32.TryParse(claimId, out cid))
            {
                //OTSTM2-551 Add Activity
                var assigner = SecurityContextHelper.CurrentUser;
                var model = claimService.LoadClaimWorkflowViewModel(cid);
                var claimModel = claimService.FindClaimById(cid);
                var claim = claimService.FindClaimByClaimId(cid);
                var assignedToUserId = (long)claim.AssignToUserId;
                var assignee = userService.FindUserByUserId(assignedToUserId);
                var assigneeUser = new UserReference
                {
                    Id = assignee.ID,
                    Email = assignee.Email,
                    FirstName = assignee.FirstName,
                    LastName = assignee.LastName,
                    UserName = assignee.UserName
                };
                this.claimService.UpdateAssignToUserIdForClaim(null, cid);

                switch (model.ClaimWorkflowStatus)
                {
                    case ClaimWorkflowStatus.Representative:
                        AddActivity(assigner, assigneeUser, claimModel, 1, 0, true, true);
                        break;
                    case ClaimWorkflowStatus.Lead:
                        AddActivity(assigner, assigneeUser, claimModel, 2, 0, true, true);
                        break;
                    case ClaimWorkflowStatus.Supervisor:
                        AddActivity(assigner, assigneeUser, claimModel, 3, 0, true, true);
                        break;
                    case ClaimWorkflowStatus.Approver1:
                        AddActivity(assigner, assigneeUser, claimModel, 4, 0, true, true);
                        break;
                    case ClaimWorkflowStatus.Approver2:
                        AddActivity(assigner, assigneeUser, claimModel, 5, 0, true, true);
                        break;
                }
            }

            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AssignToMeHandler()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
            int currUserId;
            currUserId = Convert.ToInt32(SecurityContextHelper.CurrentUser.Id);

            var claim = this.claimService.FilterClaimByType(currUserId);
            if (claim != null)
            {
                this.claimService.UpdateAssignToUserIdForClaim(currUserId, claim.ClaimId);

                //OTSTM2-551 Add Activity
                AddActivity(SecurityContextHelper.CurrentUser, SecurityContextHelper.CurrentUser, claim, 0, 1, true, false);
                return Json(new { status = "", claim = claim }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
        }

        //OTSTM2-551
        private void AddActivity(UserReference assignerUser, UserReference assigneeUser, StaffAllClaimsModel claim, int fromStatus, int toStatus, bool isAssign, bool? isGetAssignReassignUnassign)
        {
            var activity = new Activity();
            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName) : string.Empty;
            activity.Assignee = assigneeUser != null ? assigneeUser.UserName : string.Empty;
            activity.AssigneeName = assigneeUser != null ? string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName) : string.Empty;
            activity.CreatedTime = DateTime.Now;
            activity.ActivityType = TreadMarksConstants.ClaimActivity;
            switch (claim.Type)
            {
                case ClaimType.Collector:
                    activity.ActivityArea = TreadMarksConstants.Collector;
                    break;
                case ClaimType.Hauler:
                    activity.ActivityArea = TreadMarksConstants.Hauler;
                    break;
                case ClaimType.Processor:
                    activity.ActivityArea = TreadMarksConstants.Processor;
                    break;
                case ClaimType.RPM:
                    activity.ActivityArea = TreadMarksConstants.RPM;
                    break;
            }
            activity.ObjectId = claim.ClaimId;
            string forwardOrBack = string.Empty;
            if (isAssign)
            {
                if ((bool)isGetAssignReassignUnassign)
                {
                    switch (toStatus)
                    {
                        case (int)ClaimStatus.AuditHold: // 5:
                            activity.Message = string.Format("{0} <strong>assigned</strong> this claim to <strong>{1}</strong> (Approver 2).", activity.InitiatorName, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.AuditReview:  //4:
                            activity.Message = string.Format("{0} <strong>assigned</strong> this claim to <strong>{1}</strong> (Approver 1).", activity.InitiatorName, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.Onhold: // 3:
                            activity.Message = string.Format("{0} <strong>assigned</strong> this claim to <strong>{1}</strong> (Supervisor).", activity.InitiatorName, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.UnderReview: //2:
                            activity.Message = string.Format("{0} <strong>assigned</strong> this claim to <strong>{1}</strong> (Lead).", activity.InitiatorName, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.Submitted: // 1:
                            activity.Message = string.Format("{0} <strong>assigned</strong> this claim to <strong>{1}</strong> (Representative).", activity.InitiatorName, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.Open: // 0:
                            switch (fromStatus)
                            {
                                case (int)ClaimStatus.AuditHold: // 5:
                                    activity.Message = string.Format("{0} <strong>unassigned</strong> this claim from <strong>{1}</strong> (Approver 2).", activity.InitiatorName, activity.AssigneeName);
                                    break;
                                case (int)ClaimStatus.AuditReview:  //4:
                                    activity.Message = string.Format("{0} <strong>unassigned</strong> this claim from <strong>{1}</strong> (Approver 1).", activity.InitiatorName, activity.AssigneeName);
                                    break;
                                case (int)ClaimStatus.Onhold: // 3:
                                    activity.Message = string.Format("{0} <strong>unassigned</strong> this claim from <strong>{1}</strong> (Supervisor).", activity.InitiatorName, activity.AssigneeName);
                                    break;
                                case (int)ClaimStatus.UnderReview: //2:
                                    activity.Message = string.Format("{0} <strong>unassigned</strong> this claim from <strong>{1}</strong> (Lead).", activity.InitiatorName, activity.AssigneeName);
                                    break;
                                case (int)ClaimStatus.Submitted: // 1:
                                    activity.Message = string.Format("{0} <strong>unassigned</strong> this claim from <strong>{1}</strong> (Representative).", activity.InitiatorName, activity.AssigneeName);
                                    break;
                            }
                            break;
                    }
                }
                else
                {
                    activity.Message = string.Format("Claim <strong>assigned</strong> to <strong>{0}</strong> (Representative).", activity.AssigneeName);
                }
            }
            else
            {
                if (toStatus > fromStatus)
                {
                    forwardOrBack = "forward";
                    switch (toStatus)
                    {
                        case (int)ClaimStatus.Approved: // 6:
                            if (fromStatus == (int)ClaimStatus.Onhold) // 3:3)
                            {
                                activity.Message = string.Format("{0} (Supervisor) <strong>approved</strong> this claim.", activity.InitiatorName);
                            }
                            else if (fromStatus == (int)ClaimStatus.AuditReview)  //4:)
                            {
                                activity.Message = string.Format("{0} (Approver 1) <strong>approved</strong> this claim.", activity.InitiatorName);
                            }
                            else if (fromStatus == (int)ClaimStatus.AuditHold) // 5)
                            {
                                activity.Message = string.Format("{0} (Approver 2) <strong>approved</strong> this claim.", activity.InitiatorName);
                            }
                            break;
                        case (int)ClaimStatus.AuditHold: // 5:
                            activity.Message = string.Format("{0} (Approver 1) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Approver 2).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.AuditReview: // 4:
                            activity.Message = string.Format("{0} (Supervisor) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Approver 1).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.Onhold: // 3:
                            activity.Message = string.Format("{0} (Lead) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Supervisor).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.UnderReview: // 2:
                            activity.Message = string.Format("{0} (Representative) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Lead).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                    }
                }
                else
                {
                    forwardOrBack = "back";
                    switch (toStatus)
                    {
                        case (int)ClaimStatus.Open: // 0:
                            if (fromStatus == (int)ClaimStatus.Submitted)
                            {
                                activity.Message = string.Format("{0} (Representative) sent this claim <strong>{1}</strong> to <strong>Participant</strong>.", activity.InitiatorName, forwardOrBack);
                            }
                            else if (fromStatus == (int)ClaimStatus.UnderReview) //2)
                            {
                                activity.Message = string.Format("{0} (Lead) sent this claim <strong>{1}</strong> to <strong>Participant</strong>.", activity.InitiatorName, forwardOrBack);
                            }
                            break;
                        case (int)ClaimStatus.Submitted: //1:
                            activity.Message = string.Format("{0} (Lead) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Representative).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.UnderReview: // 2:
                            activity.Message = string.Format("{0} (Supervisor) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Lead).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.Onhold: // 3:
                            activity.Message = string.Format("{0} (Approver 1) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Supervisor).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                        case (int)ClaimStatus.AuditReview: // 4:
                            activity.Message = string.Format("{0} (Approver 2) sent this claim <strong>{1}</strong> to <strong>{2}</strong> (Approver 1).", activity.InitiatorName, forwardOrBack, activity.AssigneeName);
                            break;
                    }
                }
            }
            messageService.AddActivity(activity);

            //Publish activity event
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(claim.ClaimId);
        }

        private void AddNotification(UserReference assignerUser, UserReference assigneeUser, StaffAllClaimsModel claim, bool isReviewNotApprove)
        {
            var notification = new Notification();
            notification.Assigner = assignerUser.UserName;
            notification.AssignerName = string.Format("{0} {1}", assignerUser.FirstName, assignerUser.LastName);
            notification.AssignerInitial = string.Format("{0}{1}", assignerUser.FirstName[0], assignerUser.LastName[0]);
            notification.Assignee = assigneeUser.UserName;
            notification.AssigneeName = string.Format("{0} {1}", assigneeUser.FirstName, assigneeUser.LastName);
            notification.CreatedTime = DateTime.Now;
            notification.NotificationType = TreadMarksConstants.ClaimNotification;
            notification.Status = "Unread";  //Add by Frank
            switch (claim.Type)
            {
                case ClaimType.Collector:
                    notification.NotificationArea = TreadMarksConstants.Collector;
                    break;
                case ClaimType.Hauler:
                    notification.NotificationArea = TreadMarksConstants.Hauler;
                    break;
                case ClaimType.Processor:
                    notification.NotificationArea = TreadMarksConstants.Processor;
                    break;
                case ClaimType.RPM:
                    notification.NotificationArea = TreadMarksConstants.RPM;
                    break;
            }
            notification.ObjectId = claim.ClaimId;
            if (isReviewNotApprove)
            {
                notification.Message = string.Format("<strong>{0}</strong> assigned <strong>{1}</strong> - <strong>{2} Claim</strong> to you for <strong>review</strong>.", notification.AssignerName, claim.RegNumber, claim.Period);
            }
            else
            {
                notification.Message = string.Format("<strong>{0}</strong> assigned <strong>{1}</strong> - <strong>{2} Claim</strong> to you for <strong>approval</strong>.", notification.AssignerName, claim.RegNumber, claim.Period);
            }

            messageService.AddNotification(notification);

            //Publish notification event
            var notificationPayload = new NotificationPayload
            {
                NotificationId = notification.ID,
                Receiver = notification.Assignee
            };
            DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<NotificationEvent>().Publish(notificationPayload);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcel(string searchText = "")
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            //OTSTM2-904           
            var columnSearchText = new Dictionary<string, string>
            {
                {"RegNumber", Request.QueryString[3]},
                {"Type", Request.QueryString[4]},
                {"Company", Request.QueryString[5]},
                {"Period", Request.QueryString[6]},
                {"Submitted",Request.QueryString[7]},
                {"ChequeDueDate", Request.QueryString[8]},
                {"TotalClaimAmount", Request.QueryString[9]},
                {"Status", Request.QueryString[10]},
                {"HoldStatus", Request.QueryString[11]},
                {"AssignedTo",Request.QueryString[12]},
            };

            var claims = this.claimService.GetExportDetails(searchText, sortcolumn, sortdirection, columnSearchText);

            var columnNames = new List<string>(){
                "RegNumber", "Type", "Company", "Period","Submitted","Payment Due Date","Total Claim Amount $","Status","Hold Status","AssignedTo"
            };

            var delegates = new List<Func<StaffAllClaimsModel, string>>()
            {
                u => u.RegNumber,
                u => u.Type.ToString(),
                u => u.Company,
                u => u.Period,
                u => u.Submitted != null ? u.Submitted.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.PaymentDue != null ? u.PaymentDue.Value.Date.ToString(TreadMarksConstants.DateFormat, CultureInfo.InvariantCulture) : "",
                u => u.TotalClaimAmount != null ? String.Format( "{0:#,##0.##}", u.TotalClaimAmount) : "", //OTSTM2-423
                u => u.Status,
                u => u.HoldStatus,
                u => u.AssignedTo
            };

            string csv = claims.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Claims-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #region Participant/Staff claims list when participant and staff logs in but not assignment

        #region Participant
        [HttpGet]
        public JsonResult GetAllClaimsListHandler(DataGridModel param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;

                    //OTSTM2-988           
                    var columnSearchText = new Dictionary<string, string>
                    {
                        {"PeriodName", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                        {"Status", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                        {"Amount", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                        {"SubmittedDate", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                        {"ChequeDueDate", Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                        {"EftNumber", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                        {"PaymentDate", Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
                        {"AssignedDate", Request.QueryString.GetValues("columns[7][search][value]").FirstOrDefault()},
                        {"SubmittedBy",Request.QueryString.GetValues("columns[8][search][value]").FirstOrDefault()},
                    };

                    var columns = new Dictionary<int, string>
                    {
                        {0, "PeriodName"},
                        {1, "Status"},
                        {2, "Amount"},
                        {3, "SubmittedDate"},
                        {4, "ChequeDueDate"},
                        {5, "EftNumber"},
                        {6, "PaymentDate"},
                        {7, "AssignedDate"},
                        {8, "SubmittedBy"}
                    };

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];

                    var claims = claimService.LoadVendorClaims(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, columnSearchText);

                    //OTSTM2-988 comment out, because SubmittedDate and AssignedDate are converted to local time in Repository layer
                    //claims.DTOCollection.ForEach(i =>
                    //{
                    //    i.SubmittedDate = (null == i.SubmittedDate) ? null : ConvertTimeFromUtc(i.SubmittedDate);
                    //    //i.ChequeDueDate = (null == i.ChequeDueDate) ? null : ConvertTimeFromUtc(i.ChequeDueDate);
                    //    i.AssignedDate = (null == i.AssignedDate) ? null : ConvertTimeFromUtc(i.AssignedDate);
                    //});

                    var json = new
                    {
                        draw = param.Draw,
                        recordsTotal = claims.TotalRecords,
                        recordsFiltered = claims.TotalRecords,
                        data = claims.DTOCollection
                    };
                    return Json(json, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllClaimsExportToExcel(string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            //OTSTM2-988           
            var columnSearchText = new Dictionary<string, string>
            {
                {"PeriodName", Request.QueryString[3]},
                {"Status", Request.QueryString[4]},
                {"Amount", Request.QueryString[5]},
                {"SubmittedDate", Request.QueryString[6]},
                {"ChequeDueDate", Request.QueryString[7]},
                {"EftNumber", Request.QueryString[8]},
                {"PaymentDate", Request.QueryString[9]},
                {"AssignedDate", Request.QueryString[10]},
                {"SubmittedBy",Request.QueryString[11]},
            };

            var claims = this.claimService.GetExportDetailsParticipantClaims(searchText, sortcolumn, sortdirection, vendor.VendorId, columnSearchText);

            var columnNames = new List<string>(){
                "PeriodName", "Status", "Amount", "SubmittedDate","ChequeDueDate","EftNumber","PaymentDate","AssignedDate","SubmittedBy"
            };

            var delegates = new List<Func<ClaimViewModel, string>>()
            {
                u => u.PeriodName,
                u => u.Status,
                u => u.Amount != null ? String.Format( "{0:#,##0.##}", u.Amount)  : "", //OTSTM2-423
                u => u.SubmittedDate != null ? u.SubmittedDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.ChequeDueDate != null ? u.ChequeDueDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.EftNumber,
                u => u.PaymentDate != null ? u.PaymentDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.AssignedDate != null ? u.AssignedDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.SubmittedBy
            };

            //OTSTM2-988 comment out, because SubmittedDate and AssignedDate are converted to local time in Repository layer
            //claims.ForEach(i =>
            //{
            //    i.SubmittedDate = (null == i.SubmittedDate) ? null : ConvertTimeFromUtc(i.SubmittedDate);
            //    i.AssignedDate = (null == i.AssignedDate) ? null : ConvertTimeFromUtc(i.AssignedDate);
            //});

            string csv = claims.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Claims-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Staff
        [HttpGet]
        public JsonResult StaffGetAllClaimsListHandler(DataGridModel param)
        {
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var vendor = SecurityContextHelper.CurrentVendor;

                if (vendor.IsVendor)
                {
                    var searchText = param.Search != null && param.Search.ContainsKey("value")
                         ? param.Search["value"].Trim()
                         : string.Empty;

                    //OTSTM2-986           
                    var columnSearchText = new Dictionary<string, string>
                    {
                        {"PeriodName", Request.QueryString.GetValues("columns[0][search][value]").FirstOrDefault()},
                        {"Status", Request.QueryString.GetValues("columns[1][search][value]").FirstOrDefault()},
                        {"Amount", Request.QueryString.GetValues("columns[2][search][value]").FirstOrDefault()},
                        {"SubmittedDate", Request.QueryString.GetValues("columns[3][search][value]").FirstOrDefault()},
                        {"ReviewDate",Request.QueryString.GetValues("columns[4][search][value]").FirstOrDefault()},
                        {"ChequeDueDate", Request.QueryString.GetValues("columns[5][search][value]").FirstOrDefault()},
                        {"EftNumber", Request.QueryString.GetValues("columns[6][search][value]").FirstOrDefault()},
                        {"PaymentDate", Request.QueryString.GetValues("columns[7][search][value]").FirstOrDefault()},
                        {"AssignedDate", Request.QueryString.GetValues("columns[8][search][value]").FirstOrDefault()},
                        {"AssignedTo", Request.QueryString.GetValues("columns[9][search][value]").FirstOrDefault()},
                        {"SubmittedBy",Request.QueryString.GetValues("columns[10][search][value]").FirstOrDefault()},
                    };

                    var columns = new Dictionary<int, string>
                    {
                        {0, "PeriodName"},
                        {1, "Status"},
                        {2, "Amount"},
                        {3, "SubmittedDate"},
                        {4, "ReviewDate"},
                        {5, "ChequeDueDate"},
                        {6, "EftNumber"},
                        {7, "PaymentDate"},
                        {8, "AssignedDate"},
                        {9, "AssignedTo"},
                        {10, "SubmittedBy"}
                    };

                    int orderColumnIndex;
                    int.TryParse(param.Order[0]["column"], out orderColumnIndex);
                    var orderBy = columns[orderColumnIndex];
                    var sortDirection = param.Order[0]["dir"];

                    var claims = claimService.LoadStaffVendorClaims(param.Start, param.Length, searchText, orderBy, sortDirection, vendor.VendorId, columnSearchText);

                    //OTSTM2-986 comment out, because SubmittedDate and AssignedDate are converted to local time in Repository layer
                    //claims.DTOCollection.ForEach(i =>
                    //{
                    //    i.SubmittedDate = (null == i.SubmittedDate) ? null : ConvertTimeFromUtc(i.SubmittedDate);
                    //    //i.ReviewDate = (null == i.ReviewDate) ? null : ConvertTimeFromUtc(i.ReviewDate);
                    //    //i.ChequeDueDate = (null == i.ChequeDueDate) ? null : ConvertTimeFromUtc(i.ChequeDueDate);
                    //    i.AssignedDate = (null == i.AssignedDate) ? null : ConvertTimeFromUtc(i.AssignedDate);
                    //});

                    var json = new
                    {
                        draw = param.Draw,
                        recordsTotal = claims.TotalRecords,
                        recordsFiltered = claims.TotalRecords,
                        data = claims.DTOCollection
                    };

                    return Json(json, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = CL.TMS.Resources.MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StaffAllClaimsExportToExcel(string searchText = "")
        {
            var vendor = SecurityContextHelper.CurrentVendor;

            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            //OTSTM2-986           
            var columnSearchText = new Dictionary<string, string>
                    {
                        {"PeriodName", Request.QueryString[3]},
                        {"Status", Request.QueryString[4]},
                        {"Amount", Request.QueryString[5]},
                        {"SubmittedDate", Request.QueryString[6]},
                        {"ReviewDate",Request.QueryString[7]},
                        {"ChequeDueDate", Request.QueryString[8]},
                        {"EftNumber", Request.QueryString[9]},
                        {"PaymentDate", Request.QueryString[10]},
                        {"AssignedDate", Request.QueryString[11]},
                        {"AssignedTo", Request.QueryString[12]},
                        {"SubmittedBy",Request.QueryString[13]},
                    };

            var claims = this.claimService.GetExportDetailsStaffClaims(searchText, sortcolumn, sortdirection, vendor.VendorId, columnSearchText);

            var columnNames = new List<string>(){
                "PeriodName", "Status", "Amount", "SubmittedDate", "ReviewDate", "ChequeDate","EftNumber","PaymentDate","AssignedDate","AssignedTo","SubmittedBy"
            };

            var delegates = new List<Func<ClaimViewModel, string>>()
            {
                u => u.PeriodName,
                u=> u.Status,
                u => u.Amount != null ? String.Format( "{0:#,##0.##}", u.Amount)  : "", //OTSTM2-423
                u => u.SubmittedDate != null ? u.SubmittedDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.ReviewDate != null ? u.ReviewDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.ChequeDueDate != null ? u.ChequeDueDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.EftNumber,
                u => u.PaymentDate != null ? u.PaymentDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.AssignedDate != null ? u.AssignedDate.Value.Date.ToString(TreadMarksConstants.DateFormat,CultureInfo.InvariantCulture) : "",
                u => u.AssignedTo,
                u => u.SubmittedBy
            };

            //OTSTM2-986 comment out, because SubmittedDate and AssignedDate are converted to local time in Repository layer
            //claims.ForEach(i =>
            //{
            //    i.SubmittedDate = (null == i.SubmittedDate) ? null : ConvertTimeFromUtc(i.SubmittedDate);
            //    //i.ReviewDate = (null == i.ReviewDate) ? null : ConvertTimeFromUtc(i.ReviewDate);
            //    i.AssignedDate = (null == i.AssignedDate) ? null : ConvertTimeFromUtc(i.AssignedDate);
            //});

            string csv = claims.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("Claims-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #endregion

        #region Claims Internal Notes

        [HttpGet]
        public ActionResult GetClaimInternalNotes(int id)
        {
            var result = claimService.LoadClaimInternalNotes(id);
            result.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotesHandler(int id, string notes)
        {
            var claimNote = claimService.AddClaimNote(id, notes);
            return new NewtonSoftJsonResult(claimNote, false);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelInternalNotes(int id, bool sortReverse, string sortcolumn, string searchText = "")
        {
            var claimNotes = claimService.LoadClaimNotesForExport(id, searchText, sortcolumn, sortReverse);

            claimNotes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = claimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion

        #region Breadcrumb
        [HttpGet]
        public JsonResult LoadClaimPeriodBreadCrumb()
        {
            var result = claimService.GetClaimPeriodForBreadCrumb(SecurityContextHelper.CurrentVendor.VendorId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Common
        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelCommon(List<CommonExportViewModel> list)
        {
            var columnNames = new List<string>(){
                "", "Actual(t)"
            };

            var delegates = new List<Func<CommonExportViewModel, string>>()
            {
                u => u.RowName,
                u => u.Actual
            };

            string csv = list.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("file-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelPaymentAdjustment(IList<CommonPaymentAdjustExportViewModel> PaymentAdjList)
        {
            var columnNames = new List<string>(){
                "","Payment ($)"
            };

            var delegates = new List<Func<CommonPaymentAdjustExportViewModel, string>>()
            {
                u => u.RowName,
                u => u.Payment
            };

            string csv = PaymentAdjList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("file-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ExportToExcelStatusPanel(IList<CommonStatusPanelExportViewModel> StatusPanelList)
        {
            string csv = StatusPanelExportTypeOne(StatusPanelList) + Environment.NewLine;
            csv = csv + StatusPanelExportTypeTwo(StatusPanelList) + Environment.NewLine;
            csv = csv + StatusPanelExportTypeThree(StatusPanelList);

            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("file-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        private string StatusPanelExportTypeOne(IList<CommonStatusPanelExportViewModel> StatusPanelList)
        {
            var columnNames = new List<string>(){
                "Submitted","Assigned","Started","Review Due","Reviewed","Approved"
            };

            var delegates = new List<Func<CommonStatusPanelExportViewModel, string>>()
            {
                u => u.firstCol,
                u => u.secondCol,
                u => u.thirdCol,
                u => u.fourthCol,
                u => u.fifthCol,
                u => u.sixthCol
            };

            StatusPanelList = StatusPanelList.Where(s => s.Type == "1").ToList();
            string csv = StatusPanelList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            return csv;
        }
        private string StatusPanelExportTypeTwo(IList<CommonStatusPanelExportViewModel> StatusPanelList)
        {
            var columnNames = new List<string>(){
                "","Hold","On-hold","Off-hold","On-hold (Days)"
            };

            var delegates = new List<Func<CommonStatusPanelExportViewModel, string>>()
            {
                u => u.firstCol,
                u => u.secondCol,
                u => u.thirdCol,
                u => u.fourthCol,
                u => u.fifthCol
            };

            StatusPanelList = StatusPanelList.Where(s => s.Type == "2").ToList();
            string csv = StatusPanelList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());

            return csv;
        }
        private string StatusPanelExportTypeThree(IList<CommonStatusPanelExportViewModel> StatusPanelList)
        {
            var columnNames = new List<string>(){
                "","Transactions","","","","","",""
            };

            var delegates = new List<Func<CommonStatusPanelExportViewModel, string>>()
            {
                u => u.firstCol,
                u => u.secondCol,
                u => u.thirdCol,
                u => u.fourthCol,
                u => u.fifthCol,
                u => u.sixthCol,
                u => u.seventhCol,
                u => u.eighthCol
            };

            StatusPanelList = StatusPanelList.Where(s => s.Type == "3").ToList();
            string csv = StatusPanelList.ToCSV(",", columnNames.ToArray(), delegates.ToArray());

            return csv;
        }

        #endregion

        #region Activity
        public JsonResult LoadAllActivities(DataGridModel param, int claimId, string temp)
        {
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }
            else
            {
                if (temp != null)
                {
                    searchText = temp.Trim();
                }
            }

            //searchText = param.Search != null && param.Search.ContainsKey("value")
            //    ? param.Search["value"].Trim()
            //    : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadAllActivities(param.Start, param.Length, searchText, orderBy, sortDirection, claimId);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetAllActivitiesExport(string searchText)
        {
            var sortColumn = Request.QueryString[1];
            var sortDirection = Request.QueryString[2];
            var claimId = Request.QueryString[3];
            int cid = 0;
            int.TryParse(claimId, out cid);
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var result = messageService.GetAllActivitiesExport(cid, searchText, sortColumn, sortDirection);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All Activity Excel-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult TotalActivity(int claimId)
        {
            var count = messageService.TotalActivity(claimId);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}