﻿using CL.TMS.DataContracts.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CL.TMS.ServiceContracts;
using CL.TMS.Common.Enum;
using CL.TMS.UI.Common.Helper;
using System.Net.Mail;
using System.Net.Mime;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.ServiceContracts.SystemServices;
using System.IO;
using SystemIO = System.IO;
using System.Threading.Tasks;
using CL.TMS.Resources;
using CL.TMS.ExceptionHandling;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.Configuration;
using CL.TMS.Common.Helper;
using CL.Framework.Logging;
using System.Configuration;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [AllowAnonymous]
    public class AppInvitationController : Controller
    {
        private IApplicationInvitationService applicationInvitation;
        private IApplicationService application;
        private ICompanyBrandingServices companyBrandingService;
        private string uploadRepositoryPath;

        #region Constructors
        public AppInvitationController(IApplicationService application, IApplicationInvitationService applicationInvitation, ICompanyBrandingServices companyBrandingService)
        {
            this.application = application;
            this.applicationInvitation = applicationInvitation;
            this.companyBrandingService = companyBrandingService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }
        #endregion

        //
        // GET: /System/AppInvitation/
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Registration()
        {
            ViewBag.versionid = ConfigurationManager.AppSettings["AppVersion"];
            return View();
        }

        [HttpGet]
        public JsonResult CreateApplicationInvitation(string input)
        {
            ResponseMessage response = new ResponseMessage();
            string[] inputString = input.Split(',');
            string emailID = inputString[0];
            string participantType = inputString[1];
            //response = ValidateAndCreateApplicationInvitation(emailID, participantType);

            //OTSTM2-36 create application invitation and send new token as new requests come in
            CreateApplicationInvitation(emailID, participantType, response);

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        //OTSTM2-36 create application invitation and send new token as new requests come in
        public void CreateApplicationInvitation(string emailID, string participantType, ResponseMessage message)
        {
            ApplicationInvitation appInvitation = CreateAndSendToken(emailID, participantType, ref message);
        }

        public ResponseMessage ValidateAndCreateApplicationInvitation(string emailID, string participantType)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            try
            {
                ApplicationInvitation appInvitation = applicationInvitation.GetApplicationInvitationByEmailID(emailID, participantType);

                //Create New record ApplicationInvitation
                if (appInvitation == null)
                {
                    appInvitation = CreateAndSendToken(emailID, participantType, ref responseMessage);
                }
                // Use existing ApplicationInvitation, Check Status 
                else
                {
                    //Already clicked the email link
                    if (appInvitation.ApplicationID != null)
                    {

                        string message = "";
                        switch ((ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), application.GetSingleApplication(appInvitation.ApplicationID.Value).Status))
                        {
                            case ApplicationStatusEnum.Approved:
                            case ApplicationStatusEnum.BankInformationSubmitted:
                            case ApplicationStatusEnum.BankInformationApproved:
                            case ApplicationStatusEnum.Completed:
                                {
                                    responseMessage.ResponseMessages.Add(string.Format(MessageResource.ApplicationInvitationAlreadyExists, emailID, participantType));
                                    responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Open:
                                {
                                    ResendInvite(appInvitation, ref responseMessage);
                                    break;
                                }
                            case ApplicationStatusEnum.Submitted:
                                {
                                    responseMessage.ResponseMessages.Add(string.Format(MessageResource.ApplicationInvitationAlreadySubmitted, emailID, participantType));
                                    responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.BackToApplicant:
                                {
                                    responseMessage.ResponseMessages.Add(string.Format(MessageResource.ApplicationInvitationInProgress, emailID, participantType));
                                    responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.OnHold:
                                {
                                    responseMessage.ResponseMessages.Add(string.Format(MessageResource.ApplicationInvitationOnHold, emailID, participantType));
                                    responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Assigned:
                                {
                                    responseMessage.ResponseMessages.Add(string.Format(MessageResource.ApplicationInvitationInProgress, emailID, participantType));
                                    responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
                                    break;
                                }
                            case ApplicationStatusEnum.Denied:
                                {
                                    appInvitation = CreateAndSendToken(emailID, participantType, ref responseMessage);
                                    break;
                                }
                        }
                    }
                    //Never clicked the email link
                    else
                    {
                        ResendInvite(appInvitation, ref responseMessage);
                    }


                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                responseMessage.ResponseMessages.Add(ex.Message.ToString());
                responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;

            }
            return responseMessage;
        }

        private void ResendInvite(ApplicationInvitation appInvitaion, ref ResponseMessage responseMessage)
        {

            if (EmailContentHelper.IsEmailEnabled("NewApplicationRequest") && ComposeAndSendEmail(appInvitaion))
            {
                responseMessage.ResponseMessages.Add(string.Format(string.Format(MessageResource.ApplicationInvitationEmailResent,
                    appInvitaion.Email)));
                responseMessage.ResponseStatus = (int)WebServiceResponseStatus.OK;
            }
            else
            {
                responseMessage.ResponseMessages.Add("This email is disabled. Please contact us for assistance.");
                responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
            }
        }

        private ApplicationInvitation CreateAndSendToken(string emailID, string participantType, ref ResponseMessage responseMessage)
        {
            int participantTypeID = applicationInvitation.GetParticipantTypeID(participantType);
            ApplicationInvitation appInvitaion = new ApplicationInvitation();
            Guid guid = Guid.NewGuid();
            appInvitaion.Email = emailID;
            appInvitaion.ParticipantTypeID = participantTypeID;
            appInvitaion.TokenID = guid.ToString();
            appInvitaion.ParticipantTypeName = participantType;
            appInvitaion.InvitationDate = DateTime.UtcNow;

            if (EmailContentHelper.IsEmailEnabled("NewApplicationRequest") && ComposeAndSendEmail(appInvitaion))
            {
                applicationInvitation.AddApplicationInvitation(appInvitaion);
                responseMessage.ResponseMessages.Add(string.Format(string.Format(MessageResource.ApplicationInvitationEmailSent,
                    participantType, emailID)));
                responseMessage.ResponseStatus = (int)WebServiceResponseStatus.OK;
            }
            else
            {
                responseMessage.ResponseMessages.Add("This email is disabled. Please contact us for assistance.");
                responseMessage.ResponseStatus = (int)WebServiceResponseStatus.ERROR;
            }
            return appInvitaion;
        }

        //OTSTM2-346
        protected DateTime? ConvertTimeFromUtc(DateTime? dt)
        {
            if (null == dt)
            {
                return null;
            }
            DateTime parseResult = DateTime.MinValue;
            DateTime.TryParse(dt.ToString(), out parseResult);
            var destinationTimeZone = ViewBag.TimeZoneInfo as TimeZoneInfo;
            if (destinationTimeZone == null)
            {
                destinationTimeZone = TimeZoneInfo.Local;
            }

            return TimeZoneInfo.ConvertTimeFromUtc(parseResult, destinationTimeZone);
        }

        private bool ComposeAndSendEmail(ApplicationInvitation appInvitation)
        {
            bool isEmailed = false;
            try
            {
                Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));


                //string invitationSubject = SiteSettings.Instance.GetSettingValue("Application.InvitationEmailSubject").Replace("*ParticipantType*", appInvitation.ParticipantTypeName);
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\ApplicationInviteEmailTemplate.html");
                //string body = SystemIO.File.ReadAllText(htmlbody);

                string body = EmailContentHelper.GetCompleteEmailByName("NewApplicationRequest");
                string invitationSubject = EmailContentHelper.GetSubjectByName("NewApplicationRequest");

                body = body.Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString());

                var siteURl = Path.Combine(SiteSettings.Instance.GetSettingValue("Settings.DomainName"),
                    SiteSettings.Instance.GetSettingValue("Settings.ApplicationInvitationPath")
                    );


                string combinedPathUrl = string.Format("{0}{1}/{2}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"),
                    SiteSettings.Instance.GetSettingValue("Settings.ApplicationInvitationPath"),
                    appInvitation.TokenID);
                body = body.Replace("@siteUrl", combinedPathUrl).Replace("@participantType", appInvitation.ParticipantTypeName);

                //OTSTM2-346
                var expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Application.InvitationExpiryDays"))).ToString();
                body = body.Replace("@expireDate", expireDate);

                //OTSTM2-979
                body = body.Replace("@datetimeNowYear", DateTime.Now.Year.ToString());

                AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\RethinkTires.jpg"), MediaTypeNames.Image.Jpeg);
                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\follow_us_on_twitter.jpg"), MediaTypeNames.Image.Jpeg);
                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //rethinkTiresLog.ContentId = "RethinkTires";
                //followUsOnTwitter.ContentId = "follow_us_on_twitter";
                treadMarksLogo.ContentId = "@ApplicationLogo";

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), appInvitation.Email, null, null, invitationSubject, body, appInvitation, null, alternateViewHTML);
                });
                isEmailed = true;
                return isEmailed;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return false;
            }
        }
        [AllowAnonymous]
        public ActionResult InvitationExpired()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult AcceptApplicationInvitation(Guid guid)
        {
            int days = Convert.ToInt16(CL.TMS.Configuration.AppSettings.Instance.GetSettingValue("Application.InvitationExpiryDays"));

            ApplicationInvitation appInvitation = applicationInvitation.GetByTokenID(guid);
            if (guid == null || appInvitation == null)
            {
                return RedirectToAction("UnauthorizedPage", "Error");
            }
            else if ((appInvitation.InvitationDate != null) && (appInvitation.ApplicationID == null) && (appInvitation.InvitationDate < DateTime.UtcNow.AddDays(-1 * days)))//compare between UTC time (now - 7days) vs. (InvitationDate)
            {
                ViewBag.Title = "Invitation Expired";
                return RedirectToAction("InvitationExpired", "System/AppInvitation");
            }
            else if (appInvitation.ApplicationID == null)
            {
                //Insert a new Application record if not already existing and update the AppId in ApplicationInvitation table
                Application app = new Application();
                app.Status = ApplicationStatusEnum.Open.ToString();
                app.StatusIndex = 3;
                app.ExpireDate = DateTime.UtcNow.AddDays(days); //days start counting from UtcNow. (click "Accept" from Email)
                app.TermsAndConditionsID = companyBrandingService.GetCurrentTermsAndConditionID(appInvitation.ParticipantTypeName);
                app = application.AddApplication(app);
                appInvitation.ApplicationID = app.ID;
                applicationInvitation.Update(appInvitation);
            }

            if (appInvitation.ParticipantTypeName == ApplicationTypes.Collector.ToString())
            {
                return Redirect("/Collector/Registration/Index/" + guid);
            }
            else if (appInvitation.ParticipantTypeName == ApplicationTypes.Hauler.ToString())
            {
                return Redirect("/Hauler/Registration/Index/" + guid);
            }
            else if (appInvitation.ParticipantTypeName == ApplicationTypes.Processor.ToString())
            {
                return Redirect("/Processor/Registration/Index/" + guid);
            }
            else if (appInvitation.ParticipantTypeName == ApplicationTypes.RPM.ToString())
            {
                return Redirect("/RPM/Registration/Index/" + guid);
            }
            else if (appInvitation.ParticipantTypeName == ApplicationTypes.Steward.ToString())
            {
                return Redirect("/Steward/Registration/Index/" + guid);
            }
            return null;
        }

        [Authorize]
        public JsonResult ResendAppInvitationEmail(int applicationId)
        {
            ApplicationInvitation appinv = applicationInvitation.UpdateInvitationDatetime(applicationId);

            if (EmailContentHelper.IsEmailEnabled("NewApplicationRequest"))
                ComposeAndSendEmail(appinv);

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }
    }
}