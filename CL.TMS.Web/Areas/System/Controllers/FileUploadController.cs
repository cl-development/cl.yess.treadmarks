﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.RetailConnection;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.CollectorServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Helper;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace CL.TMS.Web.Areas.System.Controllers
{
    [RedirectAction(false)]
    public class FileUploadController : BaseController
    {
        #region Properties
        private IFileUploadService fileUploadServiceService;
        private string uploadRepositoryPath;
        private string fileUploadPreviewDomainName;
      
        #endregion

        #region Constructor
        public FileUploadController(IFileUploadService fileUploadServiceService)
        {
            this.fileUploadServiceService = fileUploadServiceService;

            //for QA/UAT/Prod
            this.fileUploadPreviewDomainName = SiteSettings.Instance.GetSettingValue("Settings.FileUploadPreviewDomainName");
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
//#if DEBUG
//            this.uploadRepositoryPath = "C:\\Project\\Upload";
//#endif

            //for localhost
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo";
            //this.fileUploadPreviewDomainName = "http://localhost:8081/logos/";
        }
        #endregion

        #region Service Methods
        #region FileUpload for staff/participant Application
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadFiles(FileUploadServiceModel fileUploadServiceModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            IEnumerable<AttachmentModel> attachments;      
            attachments = fileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, "Online User");

            int applicationId = fileUploadServiceModel.TransactionID;
            this.fileUploadServiceService.AddApplicationAttachments(applicationId, attachments);

            var jQueryFileUploadResponseModel = FileUploadServiceModel.ComposeJQueryFileUploadResponseModelFromAttachments(attachments, fileUploadServiceModel.TransactionID, fileUploadPreviewDomainName, uploadRepositoryPath);

            return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);

            //return Json(ex.Message, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetUploadedFiles(string transactionID, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int applicationId;

            int.TryParse(transactionID.Trim(), out applicationId);

            if (applicationId > 0)
            {
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.GetUploadedFiles(GetAttachments, applicationId, fileUploadPreviewDomainName, uploadRepositoryPath, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult DeleteUploadedFile(string transactionID, string encodedFileName, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int applicationId;
            int.TryParse(transactionID.Trim(), out applicationId);

            if (applicationId > 0 && !string.IsNullOrEmpty(encodedFileName.Trim()))
            {
                string fileNameString = Token.DecodeFrom64(encodedFileName.Trim());
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.DeleteUploadedFile(DeleteAttachment, GetAttachments, applicationId, fileNameString, fileUploadPreviewDomainName, fileUploadPreviewDomainName, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        private void DeleteAttachment(int applicationId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadServiceService.GetApplicationAttachment(applicationId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                //FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadServiceService.RemoveApplicationAttachment(applicationId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }

        private IEnumerable<AttachmentModel> GetAttachments(int applicationId, bool bBankInfo)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (applicationId > 0)
            {
                attachments = this.fileUploadServiceService.GetApplicationAttachments(applicationId, bBankInfo);
            }
            return attachments;
        }

        #endregion

        #region FileUpload for Vendor
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadVendorFiles(FileUploadServiceModel fileUploadServiceModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            IEnumerable<AttachmentModel> attachments;
            attachments = fileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, "Online User");

            int vendorId = fileUploadServiceModel.TransactionID;
            this.fileUploadServiceService.AddVendorAttachments(vendorId, attachments);

            var jQueryFileUploadResponseModel = FileUploadServiceModel.ComposeJQueryFileUploadResponseModelFromAttachments(attachments, fileUploadServiceModel.TransactionID, fileUploadPreviewDomainName, uploadRepositoryPath);

            return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetUploadedVendorFiles(string transactionID, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int vendorId;

            int.TryParse(transactionID.Trim(), out vendorId);

            if (vendorId > 0)
            {
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.GetUploadedFiles(GetVendorAttachments, vendorId, fileUploadPreviewDomainName, uploadRepositoryPath, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult DeleteUploadedVendorFile(string transactionID, string encodedFileName, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int vendorId;
            int.TryParse(transactionID.Trim(), out vendorId);

            if (vendorId > 0 && !string.IsNullOrEmpty(encodedFileName.Trim()))
            {
                string fileNameString = Token.DecodeFrom64(encodedFileName.Trim());
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.DeleteUploadedFile(DeleteVendorAttachment, GetVendorAttachments, vendorId, fileNameString, fileUploadPreviewDomainName, fileUploadPreviewDomainName, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        private void DeleteVendorAttachment(int vendorId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadServiceService.GetVendorAttachment(vendorId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                this.fileUploadServiceService.RemoveVendorAttachment(vendorId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
            }

        }

        private IEnumerable<AttachmentModel> GetVendorAttachments(int vendorId, bool bBankInfo)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (vendorId > 0)
            {
                attachments = this.fileUploadServiceService.GetVendorAttachments(vendorId, bBankInfo);
            }
            return attachments;
        }

        #endregion

        #region FileUpload for Customer

        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadCustomerFiles(FileUploadServiceModel fileUploadServiceModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            IEnumerable<AttachmentModel> attachments;
            attachments = fileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, "Online User");

            int customerId = fileUploadServiceModel.TransactionID;
            this.fileUploadServiceService.AddCustomerAttachments(customerId, attachments);

            var jQueryFileUploadResponseModel = FileUploadServiceModel.ComposeJQueryFileUploadResponseModelFromAttachments(attachments, fileUploadServiceModel.TransactionID, fileUploadPreviewDomainName, uploadRepositoryPath);

            return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetUploadedCustomerFiles(string transactionID, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            int customerId;

            int.TryParse(transactionID.Trim(), out customerId);

            if (customerId > 0)
            {
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.GetUploadedFiles(GetCustomerAttachments, customerId, fileUploadPreviewDomainName, uploadRepositoryPath, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult DeleteUploadedCustomerFile(string transactionID, string encodedFileName, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int customerId;
            int.TryParse(transactionID.Trim(), out customerId);

            if (customerId > 0 && !string.IsNullOrEmpty(encodedFileName.Trim()))
            {
                string fileNameString = Token.DecodeFrom64(encodedFileName.Trim());
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.DeleteUploadedFile(DeleteCustomerAttachment, GetCustomerAttachments, customerId, fileNameString, fileUploadPreviewDomainName, fileUploadPreviewDomainName, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        private void DeleteCustomerAttachment(int customerId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadServiceService.GetCustomerAttachment(customerId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                this.fileUploadServiceService.RemoveCustomerAttachment(customerId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
            }

        }

        private IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId, bool bBankInfo)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (customerId > 0)
            {
                attachments = this.fileUploadServiceService.GetCustomerAttachments(customerId, bBankInfo);
            }
            return attachments;
        }

        #endregion

        #region FileUpload for Claims
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadClaimFiles(FileUploadServiceModel fileUploadServiceModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            IEnumerable<AttachmentModel> attachments;
          
            attachments = fileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, "Online User");

            int claimId = fileUploadServiceModel.TransactionID;
            this.fileUploadServiceService.AddClaimAttachments(claimId, attachments);

            var jQueryFileUploadResponseModel = FileUploadServiceModel.ComposeJQueryFileUploadResponseModelFromAttachments(attachments, fileUploadServiceModel.TransactionID, fileUploadPreviewDomainName, uploadRepositoryPath);

            return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);

            //return Json(ex.Message, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetClaimUploadedFiles(string transactionID, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int claimId;

            int.TryParse(transactionID.Trim(), out claimId);

            if (claimId > 0)
            {
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.GetUploadedFiles(GetClaimAttachments, claimId, fileUploadPreviewDomainName, uploadRepositoryPath, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult DeleteClaimUploadedFile(string transactionID, string encodedFileName, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int claimId;
            int.TryParse(transactionID.Trim(), out claimId);

            if (claimId > 0 && !string.IsNullOrEmpty(encodedFileName.Trim()))
            {
                string fileNameString = Token.DecodeFrom64(encodedFileName.Trim());
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.DeleteUploadedFile(DeleteClaimAttachment, GetClaimAttachments, claimId, fileNameString, fileUploadPreviewDomainName, fileUploadPreviewDomainName, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        private void DeleteClaimAttachment(int claimId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadServiceService.GetClaimAttachment(claimId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                //FileUploadHandler.DeleteUploadedFile(fileFullPath); //this is business requirement change not to delete physical file
                this.fileUploadServiceService.RemoveClaimAttachment(claimId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }

        private IEnumerable<AttachmentModel> GetClaimAttachments(int claimId, bool bBankInfo)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (claimId > 0)
            {
                attachments = this.fileUploadServiceService.GetClaimAttachments(claimId, bBankInfo);
            }
            return attachments;
        }
        #endregion
        
        #region FileUpload for TSFClaims
        [HttpPost]
        [AllowAnonymous]
        public JsonResult UploadTSFClaimFiles(FileUploadServiceModel fileUploadServiceModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            IEnumerable<AttachmentModel> attachments;
            attachments = fileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, "Online User");

            int claimId = fileUploadServiceModel.TransactionID;
            this.fileUploadServiceService.AddTSFClaimAttachments(claimId, attachments);

            var jQueryFileUploadResponseModel = FileUploadServiceModel.ComposeJQueryFileUploadResponseModelFromAttachments(attachments, fileUploadServiceModel.TransactionID, fileUploadPreviewDomainName, uploadRepositoryPath);

            return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);

            //return Json(ex.Message, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetTSFClaimUploadedFiles(string transactionID, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int claimId;

            int.TryParse(transactionID.Trim(), out claimId);

            if (claimId > 0)
            {
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.GetUploadedFiles(GetTSFClaimAttachments, claimId, fileUploadPreviewDomainName, uploadRepositoryPath, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult DeleteTSFClaimUploadedFile(string transactionID, string encodedFileName, bool bBankInfo)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int claimId;
            int.TryParse(transactionID.Trim(), out claimId);

            if (claimId > 0 && !string.IsNullOrEmpty(encodedFileName.Trim()))
            {
                string fileNameString = Token.DecodeFrom64(encodedFileName.Trim());
                JQueryFileUploadModel jQueryFileUploadResponseModel = FileUploadServiceModel.DeleteUploadedFile(DeleteTSFClaimAttachment, GetTSFClaimAttachments, claimId, fileNameString, fileUploadPreviewDomainName, fileUploadPreviewDomainName, bBankInfo);

                return Json(jQueryFileUploadResponseModel, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.InvalidApplicationID }, JsonRequestBehavior.AllowGet);
        }

        private void DeleteTSFClaimAttachment(int claimId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadServiceService.GetTSFClaimAttachment(claimId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                //FileUploadHandler.DeleteUploadedFile(fileFullPath); //this is business requirement change not to delete physical file
                this.fileUploadServiceService.RemoveTSFClaimAttachment(claimId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
                //throw new InvalidOperationException("The File ID does not match the specified File Name");
            }

        }

        private IEnumerable<AttachmentModel> GetTSFClaimAttachments(int claimId, bool bBankInfo)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (claimId > 0)
            {
                attachments = this.fileUploadServiceService.GetTSFClaimAttachments(claimId, bBankInfo);
            }
            return attachments;
        }
        #endregion

        #region FileUpload for Transaction
        [HttpPost]        
        public NewtonSoftJsonResult UploadTransactionFile(HttpPostedFileBase file, string dataObj)
        {
            if (file == null || string.IsNullOrEmpty(dataObj)) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

            var transactionFileUploadModel = JsonConvert.DeserializeObject<TransactionFileUploadModel>(dataObj);

            var transactionFileUploadServiceModel = new TransactionFileUploadServiceModel() { TransactionID = transactionFileUploadModel.TransactionId.ToString(), File = file, FileUploadSectionName = FileUploadSectionName.Transactions, IsBankingRelated = false };
                        
            var attachment = transactionFileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, SecurityContextHelper.CurrentUser.UserName);

            if (attachment != null)
            {
                this.fileUploadServiceService.AddTransactionAttachments(new List<AttachmentModel>() { attachment });

                TransactionFileUploadServiceModel.FillTransactionFileUploadModelFromAttachments(transactionFileUploadModel, attachment, this.fileUploadPreviewDomainName);

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = transactionFileUploadModel } };
            }

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult DeleteTemporaryTransactionFile(int attachmentId, string transactionId)
        {
            AttachmentModel attachment = this.fileUploadServiceService.GetTransactionAttachment(attachmentId);
            if (attachment != null)
            {                
                string fileFullPath = Path.Combine(this.uploadRepositoryPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadServiceService.RemoveTemporaryTransactionAttachment(attachmentId);

                string directoryPath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath(uploadRepositoryPath, FileUploadSectionName.Transactions, transactionId);
                FileUploadHandler.RemoveCurrentUploadDirectoryIfEmpty(directoryPath);

                return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
            }

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult CleanUpTemporaryTransactionFiles(List<int> attachmentIdList, string transactionId)
        {            
            this.fileUploadServiceService.RemoveTemporaryTransactionAttachments(attachmentIdList);

            string directoryPath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath(uploadRepositoryPath, FileUploadSectionName.Transactions, transactionId);
            FileUploadHandler.RemoveCurrentUploadDirectoryAndFiles(directoryPath);

            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData } };
        }        
        #endregion

        #region FileUpload for Product
        public NewtonSoftJsonResult UploadProductFile(HttpPostedFileBase file, string dataObj)
        {
            if (file == null || string.IsNullOrEmpty(dataObj)) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

            var productFileUploadModel = JsonConvert.DeserializeObject<RcProductFileUploadModel>(dataObj);

            var productFileUploadServiceModel = new ProductFileUploadServiceModel()
            { 
                ProductID = productFileUploadModel.ProductId,
                File = file, 
                FileUploadSectionName = FileUploadSectionName.Product, 
                IsBankingRelated = false 
            };

            var attachment = productFileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, SecurityContextHelper.CurrentUser.UserName);

            if (attachment != null)
            {
                this.fileUploadServiceService.AddProductAttachment(attachment);

                ProductFileUploadServiceModel.FillProductionFileUploadModelFromAttachments(productFileUploadModel, attachment, this.fileUploadPreviewDomainName);

                return new NewtonSoftJsonResult(new { status = true, statusMsg = MessageResource.ValidData, data = productFileUploadModel }, false);
            }

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }

        [HttpPost]
        public NewtonSoftJsonResult DeleteTemporaryProductFile(int attachmentId, string productId)
        {
            AttachmentModel attachment = this.fileUploadServiceService.GetProductAttachment(attachmentId);
            if (attachment != null)
            {
                string fileFullPath = Path.Combine(this.uploadRepositoryPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                try
                {
                    var fileNameOrig = Path.GetFileName(fileFullPath);
                    var filePath = fileFullPath.Replace(fileNameOrig, string.Empty);
                    var thumbnail = Path.Combine(filePath, string.Format("{0}sm{1}", Path.GetFileNameWithoutExtension(fileFullPath), Path.GetExtension(fileFullPath)));
                    FileUploadHandler.DeleteUploadedFile(thumbnail);
                }
                catch (Exception e)
                {

                    //most likely file does not exist
                }



                this.fileUploadServiceService.RemoveTemporaryProductAttachment(attachmentId);

                string directoryPath = CL.TMS.Common.Helper.FileUploadHandler.ComposeUploadAbsolutePath_Product(uploadRepositoryPath, FileUploadSectionName.Product, productId);
                FileUploadHandler.RemoveCurrentUploadDirectoryIfEmpty(directoryPath);

                return new NewtonSoftJsonResult(new { status = true, statusMsg = MessageResource.ValidData }, false);
            }

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }

        #endregion

        [AllowAnonymous]
        [HttpPost]
        public JsonResult UpdateAttachmentDescription(string fileEncodedName, string description)
        {
            string result = string.Empty;
            byte[] decodedFileNameBytes = Convert.FromBase64String(fileEncodedName);
            string decodedFileName = Encoding.UTF8.GetString(decodedFileNameBytes);

            try
            {
                fileUploadServiceService.UpdateAttachmentDescription(decodedFileName, description);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region FileUpload for Logo
        public NewtonSoftJsonResult UploadCompanyLogoFile(HttpPostedFileBase file, string dataObj)
        {
            if (file == null || string.IsNullOrEmpty(dataObj)) return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };

            var logoFileUploadModel = JsonConvert.DeserializeObject<CompanyLogoFileUploadModel>(dataObj);

            var logoFileUploadServiceModel = new LogoFileUploadServiceModel()
            {
                //LogoID = logoFileUploadModel.LogoId,
                File = file,
                FileUploadSectionName = FileUploadSectionName.CompanyLogo,
                IsBankingRelated = false
            };
            
            var attachment = logoFileUploadServiceModel.SaveUploadedFilesAndCreateAttachmentModels(uploadRepositoryPath, SecurityContextHelper.CurrentUser.UserName);

            if (attachment != null)
            {
                this.fileUploadServiceService.AddProductAttachment(attachment);
                LogoFileUploadServiceModel.FillLogoFileUploadModelFromAttachments(logoFileUploadModel, attachment, this.fileUploadPreviewDomainName);
                return new NewtonSoftJsonResult(new { status = true, statusMsg = MessageResource.ValidData, data = logoFileUploadModel }, false);
            }

            return new NewtonSoftJsonResult { Data = new { status = false, statusMsg = MessageResource.InvalidData } };
        }
        #endregion
    }
}