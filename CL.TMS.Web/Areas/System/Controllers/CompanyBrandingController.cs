﻿using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.CompanyBranding;
using CL.TMS.DataContracts.ViewModel.Transaction;
using CL.TMS.Resources;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.ServiceContracts.ConfigurationsServices;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Helper;
using CL.TMS.UI.Common.Security;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class CompanyBrandingController : BaseController
    {
        #region Services

        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        private ICompanyBrandingServices companyBrandingServices;
        private IConfigurationsServices configurationService;

        #endregion Services

        public CompanyBrandingController(IUserService userService, IRegistrantService registrantService, IMessageService messageService, ICompanyBrandingServices companyBrandingServices, IConfigurationsServices configurationService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
            this.companyBrandingServices = companyBrandingServices;
            this.configurationService = configurationService;
        }
        [ClaimsAuthorization(TreadMarksConstants.AdminCompanyBrandingTermsAndConditions)]
        public ActionResult TermConditionIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "companyBrandingApp";
                ViewBag.SelectedMenu = "CompanyBranding";
                ViewBag.SelectedSubMenu = "TermCondition";
                ViewBag.TermConditonAccess = ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminCompanyBrandingTermsAndConditions).ToString();
                var applicationTypes = new List<BaseItemModel>() {
                        new BaseItemModel() { ItemValue = 0, ItemName = "Select Application ..."}
                    };
                AppDefinitions.Instance.TypeDefinitions[DefinitionCategory.ApplicationType.ToString()].ForEach(x =>
                    applicationTypes.Add(new BaseItemModel()
                    {
                        ItemValue = x.DefinitionValue,
                        ItemName = x.Name
                    })
                );
                ViewBag.ApplicationList = applicationTypes.OrderBy(x => x.ItemValue);
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [HttpPost]
        public ActionResult SaveTermCondition(TermAndConditionVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminCompanyBrandingTermsAndConditions, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized", data = vm });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData, data = vm });
            }
            else
            {
                var textValue = stripHtml(HttpUtility.HtmlDecode(vm.content));
                if (string.IsNullOrWhiteSpace(textValue))
                {
                    return Json(new { status = false, statusMsg = "Terms and Conditions contents are required.", data = vm });
                }
            }
            bool result = companyBrandingServices.SaveTermCondition(vm, SecurityContextHelper.CurrentUser.Id);

            if (result)
            {
                LogManager.LogInfo(string.Format("User: {0} (ID:{1}) added Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            }

            return Json(new { status = result, data = vm });
        }


        private string stripHtml(string input)
        {
            // \n=newline \t=tab \r= carriage return.
            string HTML_TAG_PATTERN = "\n*\t*\r*<.*?>\n*\t*\r*";
            return Regex.Replace(input, HTML_TAG_PATTERN, string.Empty);
        }


        public ActionResult LoadTermConditionByID(int applicationTypeID)
        {
            var result = companyBrandingServices.LoadDetailsByID(applicationTypeID);

            return new NewtonSoftJsonResult(result, false);
        }

        #region Company Information
        [ClaimsAuthorization(TreadMarksConstants.AdminCompanyBrandingCompanyInformation)]
        public ActionResult CompanyInformationIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "companyBrandingApp";
                ViewBag.SelectedMenu = "CompanyBranding";
                ViewBag.SelectedSubMenu = "CompanyInformation";

                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        public ActionResult LoadCompanyInformationVM()
        {
            var result = companyBrandingServices.LoadCompanyInformation();
            result.CountryList.AddRange(DataLoader.GetCountriesByIso3166());
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public ActionResult UpdateCompanyInformation(CompanyInformationVM companyInformationVM, string logoFilePath)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminCompanyBrandingCompanyInformation, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized"});
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            var length = logoFilePath.Length;

            //for localhost
            //var index = logoFilePath.IndexOf("logos");
            //var fileName = logoFilePath.Substring(index + 6, length - 6 - index);
            //logoFilePath = Path.Combine("CompanyLogo", fileName);

            //for QA/UAT/Prod
            var index = logoFilePath.IndexOf("upload");
            logoFilePath = logoFilePath.Substring(index + 7, length - 7 - index);

            companyBrandingServices.UpdateCompanyInformation(companyInformationVM, logoFilePath);

            return Json(new { status = true });
        }

        public ActionResult LoadCompanyLogoUrl()
        {
            var result = companyBrandingServices.LoadCompanyLogoUrl();

            //for localhost
            //var array = result.Split('\\');
            //var fileName = array[1];
            //var url = Path.Combine("http://localhost:8081/logos/", fileName);

            //for QA/UAT/Prod
            var fileUploadPreviewDomainName = SiteSettings.Instance.GetSettingValue("Settings.FileUploadPreviewDomainName");
            var url = Path.Combine(fileUploadPreviewDomainName, result);

            return new NewtonSoftJsonResult(url, false);
        }

        #endregion Company Information

        #region Company Information Internal Note

        public ActionResult LoadCompanyInformationInternalNotes(string parentId)
        {
            List<InternalNoteViewModel> result = configurationService.LoadAppSettingNotesByType(parentId);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddCompanyInformationInternalNote(string parentId, string notes)
        {
            var note = configurationService.AddTransactionNote(parentId, notes);
            note.AddedOn = ConvertTimeFromUtc(note.AddedOn);
            return new NewtonSoftJsonResult(note, false);
        }

        public ActionResult ExportCompanyInformationInternalNotes(string parentId, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> notes = configurationService.ExportInternalNotesToExcel(parentId, sortReverse, sortcolumn, searchText);

            notes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = notes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("CompanyInformationInternalNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"companyInformationInternalNotes/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion Company Information Internal Note

        #region //Notes

        public ActionResult LoadNotes(int applicationTypeID)
        {
            List<InternalNoteViewModel> result = companyBrandingServices.LoadNoteByID(applicationTypeID);
            result.ForEach(i =>
            {
                i.AddedOn = ConvertTimeFromUtc(i.AddedOn);
            });
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public NewtonSoftJsonResult AddNotes(int applicationTypeID, string notes)
        {
            var note = companyBrandingServices.AddNote(applicationTypeID, notes);
            note.AddedOn = ConvertTimeFromUtc(note.AddedOn);
            return new NewtonSoftJsonResult(note, false);
        }

        public ActionResult ExportNotes(int applicationTypeID, bool sortReverse, string sortcolumn, string searchText = "")
        {
            List<InternalNoteViewModel> notes = companyBrandingServices.ExportNotesToExcel(applicationTypeID, sortReverse, sortcolumn, searchText);

            notes.ForEach(c =>
            {
                c.AddedOn = ConvertTimeFromUtc(c.AddedOn);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<InternalNoteViewModel, string>>()
            {
                u => u.AddedOn.ToString("yyyy-MM-dd hh:mm tt"),
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = notes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        #endregion //Notes
    }
}