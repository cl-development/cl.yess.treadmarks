﻿using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Announcement;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Configurations;
using CL.TMS.Resources;
using CL.TMS.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [ClaimsAuthorization(TreadMarksConstants.AdminAccess)]
    public class AnnouncementController : BaseController
    {
        #region Services

        private IUserService userService;
        private IMessageService messageService;

        #endregion Services

        public AnnouncementController(IUserService userService, IMessageService messageService)
        {
            this.userService = userService;
            this.messageService = messageService;
        }

        #region //Fees and Incentives

        [ClaimsAuthorization(TreadMarksConstants.AdminAnnouncements)]
        public ActionResult Index()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.ngApp = "announcementApp";
                ViewBag.SelectedMenu = "AdminAnnouncement";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }


        public ActionResult LoadList(DataGridModel param)
        {
            if (Session["IsAdminLogin"] == null)
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "ID"},
                {1, "Subject"},
                {2, "StartDate"},
                {3, "EndDate"},
                {4, "DateAdded"},
                {5, "AddedBy"},
                {6, "ModifiedDate"},
                {7, "ModifiedBy"},
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = messageService.LoadAnnouncementList(param.Start, param.Length, searchText, orderBy, sortDirection);

            var json = new
            {
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return new NewtonSoftJsonResult(json, false);
        }

        [HttpPost]
        public ActionResult AddNew(AnnouncementVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminAnnouncements, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            messageService.AddAnnouncement(vm);

            return Json(new
            {
                status = true, 
                startDate = vm.StartDate,
                endDate=vm.EndDate
            });
        }

        [HttpPost]
        public ActionResult Update(AnnouncementVM vm)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminAnnouncements, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }
          
            messageService.UpdateAnnouncement(vm);

            return Json(new
            {
                status = true,                
            });
        }

        [HttpPost]
        public ActionResult Remove(int announcementID)
        {
            //check user write authority #OTSTM2-961
            if (!ResourceActionAuthorizationHandler.CheckResourcePermission(TreadMarksConstants.AdminAnnouncements, (int)SecurityResultType.EditSave))
            {
                return Json(new { status = false, statusMsg = "Unauthorized" });
            };
            if (!ModelState.IsValid)
            {
                return Json(new { status = false, statusMsg = MessageResource.InvalidData });
            }

            messageService.RemoveAnnouncement(announcementID);
            //if (result)
            //{
            //    LogManager.LogInfo(string.Format("User: {0} (ID:{1}) deleted Rates setting. UTC time:{2}", SecurityContextHelper.CurrentUser.FirstName + " " + SecurityContextHelper.CurrentUser.LastName, SecurityContextHelper.CurrentUser.Id, DateTime.UtcNow.ToString()));
            //}

            var json = new
            {
                status = true,               
                statusMsg = MessageResource.ValidData              
            };
            return new NewtonSoftJsonResult(json, false);
        }

        public ActionResult LoadAnnouncementByID(int announcementID)
        {
            var result = messageService.LoadAnnouncementByID(announcementID);
            return new NewtonSoftJsonResult(result, false);
        }

        #endregion //Fees and Incentives

    }
}