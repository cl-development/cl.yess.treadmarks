﻿using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel.Reporting;
using OfficeOpenXml;
using CL.TMS.UI.Common.Helper;
using CL.TMS.Web.Infrastructure;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.Reporting;
using CL.TMS.Common.Enum;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security.Authorization;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class ReportsController : BaseController
    {
        #region Services

        private readonly IReportingService reportingService;
        private readonly IRegistrantService registrantService;
        #endregion

        public ReportsController(IReportingService reportingService, IRegistrantService registrantService)
        {
            this.reportingService = reportingService;
            this.registrantService = registrantService;
        }
        // GET: System/Reports
        [ClaimsAuthorization(TreadMarksConstants.Reports)]
        public ActionResult Index()
        {
            //ViewBag.ngApp = "ReportsApp";
            ViewBag.SelectedMenu = "Reports";
            var reportCategories = reportingService.GetPermittedReportCategories();
            return View("ReportListing", reportCategories);
        }

        #region Admin_UserReport
        [HttpPost]
        public JsonResult GetUserNameBySearchText(string searchText)
        {
            var userNameList = reportingService.GetUserNameBySearchText(searchText);
            return Json(userNameList, JsonRequestBehavior.AllowGet);
        }
        [ClaimsAuthorization(TreadMarksConstants.AdminReports)]
        public ActionResult AdminIndex()
        {
            if (Session["IsAdminLogin"] != null)
            {
                ViewBag.SelectedMenu = "AdminReport";
                return View();
            }
            else
            {
                return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
            }
        }

        [HttpPost]
        public ActionResult StaffUserRolePermissionMatrixReportExport(string selectedUserName, string selectedUserEmail)
        {
            using (var package = new ExcelPackage())
            {
                var userPermissionList = reportingService.GetStaffUserRolePermissionMatrix(selectedUserEmail);
                ExcelFileHandler.ExportListWithPackage(userPermissionList, "Sheet1", package);
                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("TreadMarks {0}-{1:yyyy-MM-dd-HH-mm-ss}.xlsx", "User Permission Matrix Report", ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
                return RedirectToAction("PageNotFound", "Error");
            }
        }

        #endregion



        public ActionResult ReportDetails(int Id)
        {
            //ViewBag.ngApp = "reportingApp";
            var report = reportingService.GetReport(Id);
            var model = new ReportDetailsViewModel() { Id = Id, ReportName = (report != null ? report.ReportName : string.Empty) };
            return View("ReportDetails", model);
        }

        [HttpPost]
        public ActionResult GetReportExport(ReportDetailsViewModel model)
        {
            using (var package = new ExcelPackage())
            {
                switch (model.Id)
                {
                    case 2:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetRegistrantWeeklyReport(), "Sheet1", package, new List<string> { "DateReceived", "CreatedDate", "ActivateDate", "ConfirmationMailDate" });
                        break;
                    case 4:
                        var tireCountData = reportingService.GetStewardRevenueSupplyNewTireTypes(model);
                        tireCountData.ToList().ForEach(i =>
                        {
                            i.webSubmissionDate = ConvertTimeFromUtc(i.webSubmissionDate);
                        });
                        ExcelFileHandler.ExportListWithPackage(tireCountData, "Tire Counts", package, new List<string>() { "webSubmissionDate", });
                        if (model.EndDate >= Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value))
                        {
                            var data = reportingService.GetStewardRevenueSupplyNewTireCreditTypes_SP(model, Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value));
                            data.ToList().ForEach(i =>
                            {
                                i.webSubmissionDate = ConvertTimeFromUtc(i.webSubmissionDate);
                                i.receiptDate = ConvertTimeFromUtc(i.receiptDate);
                                i.depositDate = ConvertTimeFromUtc(i.depositDate);
                            });

                            ExcelFileHandler.ExportListWithPackage(data, "Credit - New Tire types", package, new List<string>() { "webSubmissionDate", });

                            var dataOld = reportingService.GetStewardRevenueSupplyOldTireCreditTypes_SP(model, Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value));
                            dataOld.ToList().ForEach(i =>
                            {
                                i.webSubmissionDate = ConvertTimeFromUtc(i.webSubmissionDate);
                                i.receiptDate = ConvertTimeFromUtc(i.receiptDate);
                                i.depositDate = ConvertTimeFromUtc(i.depositDate);
                            });
                            ExcelFileHandler.ExportListWithPackage(dataOld, "Credit - Old Tire types", package, new List<string>() { "webSubmissionDate", });

                        }
                        break;
                    case 5:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetProcessorTIPIReport(model.StartDate, model.EndDate), "Sheet1", package);
                        break;
                    case 8:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetTsfExtrtactInBatchOnlyReportGp(), "Sheet1", package);
                        break;
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetWebListingCollectorReportPostal(model.Id), "Sheet1", package);
                        break;
                    case 15:
                        var regNumberStr = model.RegNumber > 0 ? model.RegNumber.ToString() : string.Empty;
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetHaulerCollectorComparisonReport(model.StartDate, model.EndDate, regNumberStr), "Sheet1", package);
                        break;
                    case 16:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetWebListingHaulerReport(), "Sheet1", package);
                        break;
                    case 17:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetHaulerVolumeReport(model.StartDate, model.EndDate), "Sheet1", package);
                        break;
                    case 18:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetDetailHaulerVolumeReportBasedOnScaleWeight(model.StartDate, model.EndDate), "Sheet1", package);
                        break;
                    case 19:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetProcessorVolumeReport(model.StartDate, model.EndDate), "Sheet1", package);
                        break;
                    case 20:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetRpmData(model), "Sheet1", package);
                        break;
                    case 21:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetProcessorDispositionOfResidual(), "Sheet1", package);
                        break;
                    case 22:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetStewardNonFilers(model.StartDate, model.EndDate), "Sheet1", package);
                        break;
                    case 23:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetTSFOnlinePaymentsOutstanding(model, Convert.ToDateTime(CL.TMS.Configuration.AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Settings.TSFNegAdjSwitchDate").Value)), "Sheet1", package);
                        break;
                    case 24:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetCollectorCumulativeReport(model), "Sheet1", package, new List<string>() { "DateFinalized", });
                        break;
                    case 25:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetWebListingStewardReportPostal(model.Id), "Sheet1", package);
                        break;
                    case 26:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetWebListingProcessorReportPostal(model.Id), "Sheet1", package);
                        break;
                    case 27:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetWebListingRPMReportPostal(model.Id), "Sheet1", package);
                        break;
                    case 28:
                        //To recalculate report data if it is missing
                        ClaimReportManager.Instance.PopulateReportData(ClaimType.Hauler, model.RegNumber.ToString());
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetHaulerTireMovementReport(model), "Sheet1", package);
                        break;
                    case 29:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetRpmCumulative(model), "Sheet1", package);
                        break;
                    case 30:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetCollectorTireOriginReport(model), "Sheet1", package);
                        break;
                    case 32:
                        ExcelFileHandler.ExportListWithPackage(reportingService.GetHaulerTireCollectionReport(model), "Sheet1", package);
                        break;
                    default:
                        throw new KeyNotFoundException();
                }
                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("TreadMarks {0}-{1:yyyy-MM-dd-HH-mm-ss}.xlsx", model.ReportName, ConvertTimeFromUtc(DateTime.UtcNow));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
                return RedirectToAction("PageNotFound", "Error");
            }
        }

        public NewtonSoftJsonResult GetVendorInfo(string vendorNumber)
        {
            var vendor = registrantService.GetVendorByNumber(vendorNumber);
            return new NewtonSoftJsonResult { Data = new { status = true, statusMsg = MessageResource.ValidData, data = (null == vendor ? null : "FoundVendor") } };
        }

        #region OTSTM2-215       
        public ActionResult CollectionGenerationOfTiresBasedOnFSAReportDetails(string pickupRateGroup, string collectorGroup, DateTime fromDate, DateTime toDate)
        {
            ViewBag.PickupRateGroup = pickupRateGroup;
            ViewBag.CollectorGroup = collectorGroup;
            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;
            return View();
        }

        [HttpGet]
        public ActionResult GetReportDetailHandler(DataGridModel param, string region)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var zone = Request.QueryString[1];
            var fromDate = Request.QueryString[2];
            var toDate = Request.QueryString[3];

            var columns = new Dictionary<int, string>
            {
                {0, "HaulerRegistrationNumber"},
                {1, "HaulerName"},
                {2, "PickupDate"},
                {3, "TransactionType" },
                {4, "FormNumber" },
                {5, "RegistrationNumber" },
                {6, "CollectorName" },
                {7, "PostalCode" },
                {8, "PLT" },
                {9, "MT" },
                {10, "AGLG" },
                {11, "IND" },
                {12, "SOTR" },
                {13, "MOTR" },
                {14, "LOTR" },
                {15, "GOTR" }
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var reportDetails = reportingService.GetReportDetailHandler(param.Start, param.Length, searchText, orderBy, sortDirection, region, zone, fromDate, toDate);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = reportDetails.TotalRecords,
                recordsFiltered = reportDetails.TotalRecords,
                data = reportDetails.DTOCollection
            };

            return new NewtonSoftJsonResult(json, false);
        }

        #endregion

        #region OTSTM2-1226
        [HttpPost]
        public ActionResult GetCollectionGenerationOfTiresBasedOnRateGroup(CollectionGenerationOfTiresBasedOnRateGroupSelectionVM selectionModel)
        {
            var list = reportingService.GetCollectionGenerationOfTiresBasedOnRateGroup(selectionModel);
            return new NewtonSoftJsonResult(list, false);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcelCollectionGenerationOfTiresBasedOnRateGroupReport(CollectionGenerationOfTiresBasedOnRateGroupSelectionVM selectionModel, string sortColumn, string sortDirection)
        {

            var list = reportingService.GetCollectionGenerationOfTiresBasedOnRateGroup(selectionModel);
            if (sortColumn == "Pickup Rate Group")
            {
                list = sortDirection == "asc" ? list.OrderBy(c => c.PickupRateGroup).ThenBy(c => c.CollectorGroup).ToList() : list.OrderByDescending(c => c.PickupRateGroup).ToList();
            }
            if (sortColumn == "Collector Group")
            {
                list = sortDirection == "asc" ? list.OrderBy(c => c.CollectorGroup).ThenBy(c => c.PickupRateGroup).ToList() : list.OrderByDescending(c => c.CollectorGroup).ToList();
            }

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(list, "Sheet1", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Collection/Generation of tires based on Groups Report-{0}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }

        public ActionResult CollectionGenerationOfTiresBasedOnRateGroupDetails(string pickupRateGroup, string collectorGroup, DateTime fromDate, DateTime toDate)
        {
            //ViewBag.Region = region;
            //ViewBag.Zone = zone;
            ViewBag.PickupRateGroup = pickupRateGroup;
            ViewBag.CollectorGroup = collectorGroup;
            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;
            return View();
        }

        [HttpGet]
        public ActionResult GetReportDetailByRateGroupHandler(DataGridModel param, string pickupRateGroup)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;

            var collectorGroup = Request.QueryString[1];
            var fromDate = Request.QueryString[2];
            var toDate = Request.QueryString[3];

            var columns = new Dictionary<int, string>
            {
                {0, "HaulerRegistrationNumber"},
                {1, "HaulerName"},
                {2, "PickupDate"},
                {3, "TransactionType" },
                {4, "FormNumber" },
                {5, "RegistrationNumber" },
                {6, "CollectorName" },
                {7, "PostalCode" },
                {8, "PLT" },
                {9, "MT" },
                {10, "AGLG" },
                {11, "IND" },
                {12, "SOTR" },
                {13, "MOTR" },
                {14, "LOTR" },
                {15, "GOTR" }
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var reportDetails = reportingService.GetReportDetailByRateGroupHandler(param.Start, param.Length, searchText, orderBy, sortDirection, pickupRateGroup, collectorGroup, fromDate, toDate);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = reportDetails.TotalRecords,
                recordsFiltered = reportDetails.TotalRecords,
                data = reportDetails.DTOCollection
            };

            return new NewtonSoftJsonResult(json, false);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcelCollectionGenerationOfTiresBasedOnRateGroupReportDetails()
        {
            var sortColunm = Request.QueryString[0];
            var sortDirection = Request.QueryString[1];
            var pickupRateGroup = Request.QueryString[2];
            var collectorGroup = Request.QueryString[3];
            var fromDate = Request.QueryString[4];
            var toDate = Request.QueryString[5];
            var reportDetails = reportingService.GetReportDetailByRateGroupHandler(0, int.MaxValue, string.Empty, sortColunm, sortDirection, pickupRateGroup, collectorGroup, fromDate, toDate);

            var result = reportDetails.DTOCollection;

            using (var package = new ExcelPackage())
            {
                ExcelFileHandler.ExportListWithPackage(result, "Sheet1", package);

                if (package.Workbook.Worksheets.Count > 0)
                {
                    var fileName = string.Format("Collection/Generation of tires based on Groups Report-Details-{0}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
                    return File(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
            return RedirectToAction("PageNotFound", "Error");
        }
        #endregion

    }
}