﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.Framework.Logging;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.UI.Common;
using CL.TMS.UI.Common.Security;
using CL.TMS.DataContracts.ViewModel.System;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.Helper;
using SystemIO = System.IO;
using CL.Framework.Common;
using CL.TMS.Common;
using CL.TMS.Common.Enum;
using CL.TMS.Configuration;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.Security;
using CL.TMS.Security.Implementations;
using CL.TMS.Security.Interfaces;
using CL.TMS.UI.Common.ExceptionHelper;
using CL.TMS.UI.Common.UserInterface;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using OfficeOpenXml.ConditionalFormatting;
using Claim = System.Security.Claims.Claim;
using CL.TMS.ExceptionHandling;
using CL.TMS.UI.Common.Extensions;
using CL.TMS.DataContracts.Adapter;
using CL.TMS.DataContracts.ViewModel.Roles;
using CL.TMS.Common.Helper;

namespace CL.TMS.Web.Areas.System.Controllers
{
    [Authorize]
    public class InvitationController : BaseController
    {
        #region Fields
        private IUserService userService;
        private IAuthentication authentication;
        private IApplicationService applicationService;
        private IApplicationInvitationService applicationInvitation;
        private IRegistrantService registrantService;
        private string uploadRepositoryPath;
        #endregion

        #region Constructors
        public InvitationController(IUserService userService, IAuthentication authentication, IApplicationService applicationService, IApplicationInvitationService applicationInvitation, IRegistrantService registrantService)
        {
            this.authentication = authentication;
            this.userService = userService;
            this.applicationService = applicationService;
            this.applicationInvitation = applicationInvitation;
            this.registrantService = registrantService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
        }
        #endregion

        #region Public Methods

        [ClaimsAuthorization(TreadMarksConstants.AdminUsersStaffUsers)]
        public ActionResult StaffIndex()
        {
            var invitationsViewModel = new InvitationsViewModel();
            invitationsViewModel.CreateInvitationViewModel = new CreateInvitationViewModel();

            //Send localized messages to client side
            InitializeClientMessages();

            //Apply Page Security
            ApplyPageSecurity(invitationsViewModel);

            //Apply Routing logic
            //return ApplyRoutingLogicForStaff(invitationsViewModel);
            ViewBag.RegistrationNumber = string.Empty;
            ViewBag.ngApp = "adminStaffUserApp";
            ViewBag.SelectedMenu = "Users";
            if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminUsersStaffUsers) >= CL.TMS.Security.SecurityResultType.ReadOnly)
            {
                //return View("StaffUser", invitationsViewModel);
                if (Session["IsAdminLogin"] != null)
                {
                    return View("StaffUser", invitationsViewModel);
                }
                else
                {
                    return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
                }
            }
            else
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
            }
        }

        public ActionResult ParticipantIndex(int? vendorId = null, string searchText = "")
        {
            var invitationsViewModel = new InvitationsViewModel();
            invitationsViewModel.CreateInvitationViewModel = new CreateInvitationViewModel();

            ViewBag.Resource = "Users"; //OTSTM2-902 separate Users page with others (for header popup info)

            //Send localized messages to client side
            InitializeClientMessages();

            //Apply Page Security
            ApplyPageSecurity(invitationsViewModel);

            //Apply Routing logic
            return ApplyRoutingLogicForParticipant(invitationsViewModel, vendorId, searchText);
        }

        private void ApplyPageSecurity(InvitationsViewModel invitationsViewModel)
        {
            //User page - view and edit          
            if (SecurityContextHelper.IsStaff())
            {
                //OTSTM2-882
                invitationsViewModel.StaffParticipantUserPageReadonly = !(ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.UsersRegistrantUsers) == CL.TMS.Security.SecurityResultType.EditSave);
                invitationsViewModel.AdminUserPageReadonly = !(ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminUsersStaffUsers) == CL.TMS.Security.SecurityResultType.EditSave);
            }
            else
            {
                invitationsViewModel.StaffParticipantUserPageReadonly = !SecurityContextHelper.IsParticipantAdmin();
            }
        }

        [HttpGet]
        public ActionResult ValidateUniqueEmail()
        {
            var email = Request.QueryString[0];
            var emailIsExisting = userService.EmailIsExisting(email);

            return Json(!emailIsExisting, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckEmail(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return Json(new { status = "Email is empty" }, JsonRequestBehavior.AllowGet);
            }

            var emailIsExisting = userService.EmailIsExisting(emailAddress.Trim());
            if (emailIsExisting)
            {
                return Json(new { status = string.Format(MessageResource.InvitationAlreadyExists, emailAddress) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckEmailForParticipant(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return Json(new { status = "Email is empty" }, JsonRequestBehavior.AllowGet);
            }
            var vendor = SecurityContextHelper.CurrentVendor;
            if (vendor != null)
            {
                var emailIsExisting = userService.EmailIsExistingForParticipant(emailAddress.Trim(), vendor.VendorId, vendor.IsVendor);
                if (emailIsExisting)
                {
                    return Json(new { status = string.Format(MessageResource.InvitationAlreadyExists, emailAddress) }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LoadParticipantUserDetails(string emailAddress)
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            if (vendor != null)
            {
                var result = userService.GetParticipantUserDetails(emailAddress, vendor.VendorId, vendor.IsVendor);
                result.Status = "Ok";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult LoadParticipantUserDetailsView(string emailAddress)
        {
            var vendor = SecurityContextHelper.CurrentVendor;
            if (vendor != null)
            {
                var result = userService.GetParticipantUserDetailsView(emailAddress, vendor.VendorId, vendor.IsVendor);
                result.Status = "Ok";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult CreateInvitation(CreateInvitationViewModel createInvitationViewModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var userIp = Request.UserHostAddress;            
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            //OTSTM2-42
            string registrationNumber = string.Empty;

            var invitation = ModelAdapter.ToInvitation(createInvitationViewModel, userIp, expireDate, siteUrl);


            //if it is particiapnt user page, assing vender id to invitation
            if (SecurityContextHelper.CurrentVendor != null)
            {
                if (SecurityContextHelper.CurrentVendor.IsVendor)
                {
                    invitation.VendorId = SecurityContextHelper.CurrentVendor.VendorId;
                }
                else
                {
                    invitation.CustomerId = SecurityContextHelper.CurrentVendor.VendorId;
                }
                //OTSTM2-42
                registrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            }

            //Adding Roles
            invitation = AddingRoles(invitation, createInvitationViewModel);

            //OTSTM2-42
            if (SecurityContextHelper.CurrentVendor != null)
            {
                if (EmailContentHelper.IsEmailEnabled("NewUserAccountInvitation") && EmailInvitationWithRegistrationNumber(invitation, registrationNumber))
                {
                    invitation.IsEmailed = true;
                }
            }
            else
            {
                if (EmailContentHelper.IsEmailEnabled("NewUserAccountInvitation") && EmailInvitation(invitation))
                {
                    invitation.IsEmailed = true;
                }
            }

            userService.CreateUserInvitation(invitation);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult AcceptInvitation(Guid id)
        {
            var userAcceptInvitationModel = new UserAcceptInvitationModel();
            if (id != Guid.Empty)
            {
                var invitation = userService.FindInvitation(id);
                if (invitation == null)
                {
                    userAcceptInvitationModel = new UserAcceptInvitationModel();
                    ModelState.AddModelError("", MessageResource.InvalidInvitationUrl);
                    return View("AcceptInvitation", userAcceptInvitationModel);
                }

                var user = userService.FindUserByEmailAddress(invitation.EmailAddress);

                #region when user already exists and coming through email link
                if (user != null)//user account exists only for participant user
                {
                    user.RedirectUserAfterLogin = true;
                    user.RedirectUserAfterLoginUrl = invitation.RedirectUrl;
                    userService.ResetRedirectUrlForSecondInvitation(invitation, user);

                    var isVendor = invitation.VendorId != null;
                    var vendorId = invitation.VendorId ?? invitation.CustomerId;
                    if (vendorId != null)
                    {
                        //Add addional user before redirect to login page
                        userService.AddAdditonalUser(user.ID, (int)vendorId, isVendor, invitation);
                    }

                    // sign out current user and loged in with new user
                    HttpContext.GetOwinContext().Authentication.SignOut();
                    //redirect to login new user
                    return RedirectToAction("LogIn", "Account", new { area = "", username = user.Email, vendorid = vendorId });
                }
                #endregion


                //new user without user account
                if (invitation.Status.Trim() == EnumHelper.GetEnumDescription(UserInvitationStatus.Accepted))
                {
                    userAcceptInvitationModel = new UserAcceptInvitationModel();
                    ModelState.AddModelError("", MessageResource.InvitationAlreadyAccepted);
                    return View("AcceptInvitation", userAcceptInvitationModel);
                }
                if (invitation.ExpireDate < DateTime.UtcNow)
                {
                    userAcceptInvitationModel = new UserAcceptInvitationModel();
                    ModelState.AddModelError("", MessageResource.InvitationExpired);
                    return View("AcceptInvitation", userAcceptInvitationModel);
                }
                userAcceptInvitationModel.UserName = invitation.UserName;
                userAcceptInvitationModel.InviteGUID = invitation.InviteGUID;
                userAcceptInvitationModel.ExpireDate = invitation.ExpireDate;
                return View("AcceptInvitation", userAcceptInvitationModel);
            }

            ModelState.AddModelError("", MessageResource.InvalidInvitationUrl);
            return View("AcceptInvitation", userAcceptInvitationModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AcceptInvitation(UserAcceptInvitationModel model, string returnUrl = "/")
        {
            Invitation invitation = userService.FindInvitation(model.InviteGUID);
            if (invitation != null && ModelState.IsValid)
            {
                // sign out current user and loged in with new user
                HttpContext.GetOwinContext().Authentication.SignOut();
                var passwordExpirationInDays = DateTime.UtcNow.AddDays(Convert.ToInt16(SiteSettings.Instance.GetSettingValue("UserPolicy.PasswordExpiresInDays")));
                var password = PBKDF2PasswordHasher.Create(model.Password);

                //create USER here for the first time
                if (userService.ConfirmInvitaion(password, model.UserName, passwordExpirationInDays, invitation.RedirectUrl, model.InviteGUID))
                {
                    //Sign in user
                    var authenticationUser = authentication.VerifyUser(model.UserName, model.Password, false, Convert.ToInt16(SiteSettings.Instance.GetSettingValue("UserPolicy.MaxLoginAttemptBeforeLock")));
                    if (authenticationUser.Status == SignInStatus.Success)
                    {
                        authenticationUser.StewardType = SiteSettings.Instance.SiteStewardType;

                        var id = authentication.CreateIdentity(authenticationUser, DefaultAuthenticationTypes.ApplicationCookie);
                        HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true,
                            ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(UserPolicy.NonPersistentAuthTicketExpiresInMinutes)
                        }, id);

                        //Clear Global search session
                        Session["SelectedVendor"] = null;
                        if (authenticationUser.UserVendorClaims.Count == 1)
                        {
                            var claim = authenticationUser.UserVendorClaims.First();
                            var vendorId = Convert.ToInt32(claim.Value);
                            ParticipantPageHelper.SetCurrentVendor(registrantService, vendorId);
                        }

                        return Redirect(returnUrl);
                    }
                }
            }

            return View("AcceptInvitation", model);
        }

        [HttpPost]
        public ActionResult Resend(string email)
        {
            Invitation invitation = null;

            if (SecurityContextHelper.CurrentVendor != null)
            {
                //Participant user page
                invitation = userService.FindInvitationByUserName(email, SecurityContextHelper.CurrentVendor.VendorId, SecurityContextHelper.CurrentVendor.IsVendor);
            }
            else
            {
                //Staff user
                invitation = userService.FindInvitationByUserName(email);
            }

            if (invitation == null)
                return Json(new { status = MessageResource.InvitationNotFound }, JsonRequestBehavior.AllowGet);

            if (invitation.ApplicationId != null)
            {
                if ((invitation.Status == "Invite Resent") || (invitation.Status == "Invite Sent"))
                {
                    ResendWelcomToApplicant(invitation);
                }
                else
                {
                    ResendUserInvitation(invitation);
                }
            }
            else
            {
                ResendUserInvitation(invitation);
            }

            invitation.Status = EnumHelper.GetEnumDescription(UserInvitationStatus.InviteResent);
            
            invitation.InviteSendDate = DateTime.UtcNow;
            userService.UpdateInvitation(invitation);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        private void ResendWelcomToApplicant(Invitation invitation)
        {
            var approvedApplicationModel = registrantService.GetApprovedApplicantInformation((int)invitation.ApplicationId);
            approvedApplicationModel.InvitationToken = invitation.InviteGUID;

            if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationHauler"))
                ComposeAndSendApplicationApprovalEmail(approvedApplicationModel);
        }

        private bool ComposeAndSendApplicationApprovalEmail(ApplicationEmailModel model)
        {
            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(model.Email);
            MailMessage message = new MailMessage(from, to) { };

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.ApprovedHaulerApplicationEmailTemplLocation"));
            //string emailBody = SystemIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("ApprovedApplicationHauler");
            string subject = EmailContentHelper.GetSubjectByName("ApprovedApplicationHauler");

            var inviteUrl = string.Format("{0}{1}{2}", SiteSettings.Instance.DomainName, "/System/Invitation/AcceptInvitation/", model.InvitationToken);

            DateTime expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));

            emailBody = emailBody
                .Replace(HaulerApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.AcceptInvitationUrl, inviteUrl)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(HaulerApplicationEmailTemplPlaceHolders.InvitationExpirationDateTime, expireDate.ToString(TreadMarksConstants.DateFormat) + " at ") // + invitation.InviteDate.ToString("hh:mm tt"));
                .Replace(HaulerApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }

            //string subject = MessageResource.HaulerApplicationApprovedEmailSubject;

            SiteSettings.Instance.GetSettingValue("Invitation.WelcomeSubject");
            Task.Factory.StartNew(() =>
            {
                emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, null, null, subject, emailBody, model, null, alternateViewHTML);
            });
            return true;
        }

        private void ResendUserInvitation(Invitation invitation)
        {
            invitation.ExpireDate =
                DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"),
                SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            invitation.SiteUrl = siteUrl;

            //OTSTM2-42
            string registrationNumber = string.Empty;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                registrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
                if (EmailContentHelper.IsEmailEnabled("NewUserAccountInvitation"))
                    EmailInvitationWithRegistrationNumber(invitation, registrationNumber);
            }
            else
            {
                if (EmailContentHelper.IsEmailEnabled("NewUserAccountInvitation"))
                    EmailInvitation(invitation);
            }
        }

        [HttpPost]
        public JsonResult Inactive(string email)
        {
            userService.ActiveUserByEmail(email, false);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult InactiveParticipantUser(long userId)
        {
            var currentVendor = SecurityContextHelper.CurrentVendor;
            userService.ActiveParticipantUser(userId, currentVendor.VendorId, currentVendor.IsVendor, false);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActiveParticipantUser(long userId)
        {
            var currentVendor = SecurityContextHelper.CurrentVendor;
            userService.ActiveParticipantUser(userId, currentVendor.VendorId, currentVendor.IsVendor, true);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Active(string email)
        {
            userService.ActiveUserByEmail(email, true);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        #region OTSTM2-1013 unlock user      
        [HttpPost]
        public JsonResult UnlockStaffUser(string email)
        {
            userService.UnlockStaffUserByEmail(email);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UnlockParticipantUser(long userId)
        {
            userService.UnlockParticipantUserById(userId);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult EditParticipantUser(EditUserViewModel editUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var invitation = new Invitation();
                invitation.FirstName = editUserViewModel.FirstName;
                invitation.LastName = editUserViewModel.LastName;
                invitation.EmailAddress = editUserViewModel.EmailAddress;
                invitation.Rowversion = Convert.FromBase64String(editUserViewModel.RowversionString);

                //Adding Roles
                invitation = AddingRolesForParticipantUser(invitation, editUserViewModel);
                //Convert ID to roles
                invitation.InvitationRoleIds.ForEach(c =>
                {
                    var invitationRole = new InvitationRole
                    {
                        Role = c
                    };
                    invitation.InvitationRoles.Add(invitationRole);
                });

                if (SecurityContextHelper.CurrentVendor.IsVendor)
                {
                    invitation.VendorId = SecurityContextHelper.CurrentVendor.VendorId;
                }
                else
                {
                    invitation.CustomerId = SecurityContextHelper.CurrentVendor.VendorId;
                }
                userService.EditParticipantUserAndInvitation(invitation);
                return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }

        private Invitation AddingRolesForParticipantUser(Invitation invitation, EditUserViewModel editUserViewModel)
        {
            //For participant user role
            if (editUserViewModel.IsRead)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Read").DefinitionValue);
            }
            if (editUserViewModel.IsWrite)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Write").DefinitionValue);
            }
            if (editUserViewModel.IsSubmit)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Submit").DefinitionValue);
            }
            if (editUserViewModel.IsParticipantAdmin)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "ParticipantAdmin").DefinitionValue);
            }
            return invitation;
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ExportToExcel(string searchtext)
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            //useless, comment out for OTSTM2-1013 
            //if (string.IsNullOrWhiteSpace(sortcolumn))
            //{
            //    sortcolumn = "FirstName";
            //    sortdirection = "asc";
            //}
            //if (sortcolumn == "InviteSendDate")
            //{
            //    sortcolumn = "ExpireDate";
            //}

            var userVendorClaims = new List<Claim>();
            if (SecurityContextHelper.CurrentVendor != null)
            {
                var claim = new Claim("UserVendor", SecurityContextHelper.CurrentVendor.VendorId.ToString());
                userVendorClaims.Add(claim);
            }
            else
            {
                userVendorClaims = SecurityContextHelper.UserVendorClaims;
            }

            var vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            }

            var userInvitations = userService.LoadInvitations(searchtext.Trim(), vendorId, sortcolumn, sortdirection);

            var columnNames = new List<string>(){
                "FirstName", "LastName", "Email", "InvitationDate", "Status"
            };

            var delegates = new List<Func<UserInviteModel, string>>()
            {
                u => u.FirstName,
                u => u.LastName,
                u => u.InviteEmail,
                u => u.InviteSendDate.ToShortDateString(),
                u => u.DisplayStatus
            };

            var filename = string.Format("UsersAndInvitations-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return new CSVResult<UserInviteModel>(columnNames, delegates, userInvitations, filename);
        }

        public JsonResult GetListHandler(DataGridModel param)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            //Order by
            var columns = new Dictionary<int, string>
            {
                {0, "FirstName"},
                {1, "LastName"},
                {2, "InviteEmail"},
                {3, "InviteSendDate"},
                {4, "DisplayStatus"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            string orderBy = columns[orderColumnIndex];
            //if (orderBy == "InviteSendDate")
            //{
            //    orderBy = "ExpireDate";
            //}
            var sortDirection = param.Order[0]["dir"];

            var vendorId = 0;
            if (SecurityContextHelper.CurrentVendor != null)
            {
                if (Session["IsAdminLogin"] == null)
                {
                    vendorId = SecurityContextHelper.CurrentVendor.VendorId;
                }
            }

            var result = userService.LoadInvitations(param.Start, param.Length, searchText, orderBy, sortDirection, vendorId);
            var json = new
            {
                draw = param.Draw,
                iTotalRecords = result.TotalRecords,
                iTotalDisplayRecords = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region New User Security Changes
        public ActionResult NewStaffUser()
        {
            var staffUserViewModel = new StaffUserViewModel();
            var allUserRoles = userService.GetStaffUserRoles();
            staffUserViewModel.AvailableRoles = allUserRoles;
            staffUserViewModel.WorkflowList = new List<string>
            {
                TreadMarksConstants.NoneWorkflow,
                TreadMarksConstants.RepresentativeWorkflow,
                TreadMarksConstants.LeadWorkflow,
                TreadMarksConstants.SupervisorWorkflow,
                TreadMarksConstants.Approver1Workflow,
                TreadMarksConstants.Approver2Workflow
            };

            staffUserViewModel.StewardWorkflowList = new List<string>
            {
                TreadMarksConstants.NoneWorkflow,
                TreadMarksConstants.TSFClerkWorkflow
            };

            staffUserViewModel.AssignedRoles = new List<RoleSelectionViewModel>();
            staffUserViewModel.StewardClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
            staffUserViewModel.CollectorClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
            staffUserViewModel.HaulerClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
            staffUserViewModel.ProcessorClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;
            staffUserViewModel.RPMClaimsWorkflow.Workflow = TreadMarksConstants.NoneWorkflow;

            return Json(staffUserViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create Staff User Invitation
        /// </summary>
        /// <param name="staffUserViewModel"></param>
        /// <returns></returns>
        public ActionResult SaveNewStaffUser(StaffUserViewModel staffUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var userIp = Request.UserHostAddress;
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));

            var invitation = ModelAdapter.ToStaffUserInvitation(staffUserViewModel.FirstName, staffUserViewModel.LastName, staffUserViewModel.Email, userIp, expireDate, siteUrl);

            if (EmailContentHelper.IsEmailEnabled("NewUserAccountInvitation") && EmailInvitation(invitation))
            {
                invitation.IsEmailed = true;
            }

            userService.CreateStaffUserInvitation(invitation, staffUserViewModel);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public JsonResult LoadStaffUserDetails(string emailAddress)
        {
            var result = userService.LoadStaffUserDetails(emailAddress);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <returns></returns>
        [HttpPost]
        public ActionResult EditStaffUserDetail(StaffUserViewModel staffUserViewModel)
        {
            if (ModelState.IsValid)
            {
                userService.EditStaffUserDetail(staffUserViewModel);
                return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private Methods

        //private ActionResult ApplyRoutingLogicForStaff(InvitationsViewModel invitationsViewModel)
        //{
        //    ViewBag.RegistrationNumber = string.Empty;
        //    ViewBag.SelectedMenu = "Users";
        //    if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.AdminAccess) >= CL.TMS.Security.SecurityResultType.ReadOnly)
        //    {
        //        //return View("StaffUser", invitationsViewModel);
        //        if (Session["IsAdminLogin"] != null)
        //        {
        //            return View("StaffUser", invitationsViewModel);
        //        }
        //        else
        //        {
        //            return RedirectToAction("AdminLoginPage", "Account", new { area = "" });
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
        //    }
        //}

        private ActionResult ApplyRoutingLogicForParticipant(InvitationsViewModel invitationsViewModel, int? vendorId, string searchText)
        {
            ViewBag.RegistrationNumber = string.Empty;
            ViewBag.SelectedMenu = "Users";
            if (SecurityContextHelper.IsStaff())
            {
                if (Session["IsAdminLogin"] != null)
                {
                    return View("StaffUser", invitationsViewModel);
                }
                else
                {
                    if (vendorId != null && (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.Users) >= CL.TMS.Security.SecurityResultType.ReadOnly))
                    {
                        //Keep it in session
                        var vendor = registrantService.GetVendorById(vendorId.Value);
                        if (vendor != null)
                        {
                            Session["SelectedVendor"] = vendor;
                            ViewBag.ParticipantInfo = SecurityContextHelper.CurrentVendor;
                            ViewBag.SearchText = searchText;
                            return View("ParticipantUser", invitationsViewModel);
                        }
                        else
                            return RedirectToAction("EmptyPage", "Common", new { area = "System" });
                    }
                    else if (SecurityContextHelper.CurrentVendor != null)
                    {
                        ViewBag.ParticipantInfo = SecurityContextHelper.CurrentVendor;
                        return View("ParticipantUser", invitationsViewModel);
                    }
                    else
                    {
                        //return RedirectToAction("NoAccess", "Common", new { area = "System" });
                        return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                    }
                }
            }
            else
            {
                ParticipantPageHelper.SetGlobalRegistrationNumber(registrantService);
                if (SecurityContextHelper.CurrentVendor != null)
                {
                    if (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.Users) >= CL.TMS.Security.SecurityResultType.ReadOnly)
                    {
                        if (string.IsNullOrWhiteSpace(SecurityContextHelper.CurrentVendor.RegistrationNumber))
                            ParticipantPageHelper.SetCurrentVendor(registrantService, SecurityContextHelper.CurrentVendor.VendorId);
                        ViewBag.ParticipantInfo = SecurityContextHelper.CurrentVendor;
                        return View("ParticipantUser", invitationsViewModel);
                    }
                    else
                    {
                        return RedirectToAction("NoAccess", "Common", new { area = "System" });
                    }
                }
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }
        }
        private Invitation AddingRoles(Invitation invitation, CreateInvitationViewModel createInvitationViewModel)
        {
            //For participant user role
            if (createInvitationViewModel.IsRead)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Read").DefinitionValue);
            }
            if (createInvitationViewModel.IsWrite)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Write").DefinitionValue);
            }
            if (createInvitationViewModel.IsSubmit)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "Submit").DefinitionValue);
            }
            if (createInvitationViewModel.IsParticipantAdmin)
            {
                invitation.InvitationRoleIds.Add(AppDefinitions.Instance.GetDefinitionByCode(DefinitionCategory.UserRole, "ParticipantAdmin").DefinitionValue);
            }
            return invitation;
        }


        private void InitializeClientMessages()
        {
            ViewBag.FirstNameRequired = MessageResource.FirstNameIsRequired;
            ViewBag.LastNameRequired = MessageResource.LastNameIsRequired;
        }

        private bool EmailInvitation(Invitation invitation)
        {
            try
            {
                Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                //string invitationSubject = SiteSettings.Instance.GetSettingValue("Invitation.InviteSubject");
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\EmailTemplate.html");
                //string body = SystemIO.File.ReadAllText(htmlbody);

                string body = EmailContentHelper.GetCompleteEmailByName("NewUserAccountInvitation");
                string invitationSubject = EmailContentHelper.GetSubjectByName("NewUserAccountInvitation");

                body = body.Replace("@siteUrl", invitation.SiteUrl)
                        .Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString());

                body = body.Replace("@InviteGUID", invitation.InviteGUID.ToString());
                body = body.Replace("@ExpireDate", ConvertTimeFromUtc(invitation.ExpireDate).ToString());
                body = body.Replace("@DatetimeNowYear", DateTime.Now.Year.ToString()); //OTSTM2-979

                AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\RethinkTires.jpg"), MediaTypeNames.Image.Jpeg);
                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\follow_us_on_twitter.jpg"), MediaTypeNames.Image.Jpeg);
                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //rethinkTiresLog.ContentId = "RethinkTires";
                //followUsOnTwitter.ContentId = "follow_us_on_twitter";
                treadMarksLogo.ContentId = "@ApplicationLogo";

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), invitation.EmailAddress, null, null, invitationSubject, body, invitation,
                        null, alternateViewHTML);

                });

                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    //throw;
                }
                LogManager.LogExceptionWithMessage("Failed to email invitation", ex);
                return false;
            }
        }

        //OTSTM2-42
        private bool EmailInvitationWithRegistrationNumber(Invitation invitation, string registrationNumber)
        {
            try
            {
                Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                //string invitationSubject = SiteSettings.Instance.GetSettingValue("Invitation.InviteSubject") + "  Reg # " + registrationNumber;
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\EmailTemplate.html");
                //string body = SystemIO.File.ReadAllText(htmlbody);

                string body = EmailContentHelper.GetCompleteEmailByName("NewUserAccountInvitation");
                string invitationSubject = EmailContentHelper.GetSubjectByName("NewUserAccountInvitation");

                body = body.Replace("@siteUrl", invitation.SiteUrl);

                body = body.Replace("@InviteGUID", invitation.InviteGUID.ToString());
                body = body.Replace("@ExpireDate", ConvertTimeFromUtc(invitation.ExpireDate).ToString());
                body = body.Replace(CollectorApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                AlternateView alternateViewHTML = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLog = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\RethinkTires.jpg"), MediaTypeNames.Image.Jpeg);
                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\follow_us_on_twitter.jpg"), MediaTypeNames.Image.Jpeg);
                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Images\TreadMarksLogo.jpg"), MediaTypeNames.Image.Jpeg);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                //rethinkTiresLog.ContentId = "RethinkTires";
                //followUsOnTwitter.ContentId = "follow_us_on_twitter";
                treadMarksLogo.ContentId = "@ApplicationLogo";

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLog);
                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);
                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                Task.Factory.StartNew(() =>
                {
                    emailer.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), invitation.EmailAddress, null, null, invitationSubject, body, invitation,
                        null, alternateViewHTML);

                });

                return true;
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    //throw;
                }
                LogManager.LogExceptionWithMessage("Failed to email invitation", ex);
                return false;
            }
        }
        #endregion
    }
}