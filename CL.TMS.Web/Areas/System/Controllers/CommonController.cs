﻿using CL.TMS.Common.Enum;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.UI.Common.Security;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.Common;
using OfficeOpenXml;
using CL.TMS.UI.Common.Helper;
using CL.TMS.DataContracts.ViewModel.System;
using System.Globalization;
using CL.TMS.ExceptionHandling;
using System.Diagnostics;
using Microsoft.Practices.Prism.PubSubEvents;
using CL.TMS.Communication.Events;

namespace CL.TMS.Web.Areas.System.Controllers
{
    public class CommonController : BaseController
    {
        #region Services
        private IUserService userService;
        private IRegistrantService registrantService;
        private IMessageService messageService;
        #endregion

        public CommonController(IUserService userService, IRegistrantService registrantService, IMessageService messageService)
        {
            this.userService = userService;
            this.registrantService = registrantService;
            this.messageService = messageService;
        }

        public JsonResult GetUsersInAuditGroup()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            string[] groupName = { UsersInAuditGroupEnum.Audit.ToString(), UsersInAuditGroupEnum.CallCentre.ToString() };

            var textValuePair = this.userService.GetUsersByGroup(groupName)
                .Select(r => new TextValuePair() { Text = r.Value, Value = r.Key.ToString() })
                .OrderBy(r => r.Text);

            return Json(textValuePair, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersByApplicationsWorkflow(string accountName)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var textValuePair = this.userService.GetUsersByApplicationsWorkflow(accountName)
                .Select(r => new TextValuePair() { Text = r.Value, Value = r.Key.ToString() })
                .OrderBy(r => r.Text);

            return Json(textValuePair, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersByClaimsWorkflow(int claimId, string accountName, string claimType)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var textValuePair = this.userService.GetUsersByClaimsWorkflow(claimId, accountName, claimType)
                .Select(r => new TextValuePair() { Text = r.Value, Value = r.Key.ToString() })
                .OrderBy(r => r.Text);

            return Json(textValuePair, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersByGroup(string sGroupName)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            UsersInAuditGroupEnum status = UsersInAuditGroupEnum.TSFClerk;//default role
            string[] groupName = { status.ToString() };

            UsersInAuditGroupEnum roleValue;
            if (Enum.TryParse<UsersInAuditGroupEnum>(sGroupName, true, out roleValue))
            {
                //if sGroupName is valid, use it
                groupName[0] = roleValue.ToString();
            }

            var textValuePair = this.userService.GetUsersByGroup(groupName)
                .Select(r => new TextValuePair() { Text = r.Value, Value = r.Key.ToString() })
                .OrderBy(r => r.Text);

            return Json(textValuePair, JsonRequestBehavior.AllowGet);
        }
        [RedirectAction(false)]
        public ActionResult UnauthorizedPage()
        {
            if (Session["IsAdminLogin"] != null)
            {
                Session["IsAdminLogin"] = null;
            }
            return View();
        }

        [RedirectAction(false)]
        public ActionResult EmptyPage()
        {
            var queryValuesQs = Request.QueryString;
            string queryStr = Request.QueryString["navigationAction"];
            if (queryStr == "loadTopMenu")
            {
                ViewBag.loadTopMenu = true;
            }
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            return View();
        }

        [RedirectAction(false)]
        public ActionResult NoAccess(string reason)
        {
            ViewBag.Reason = reason;
            return View();
        }
        #region Active/Inactive

        [HttpPost]
        public ActionResult ActiveInactiveVendor(VendorActiveHistory vendorActiveHistory, ActivityVM Activity = null)
        {
            vendorActiveHistory.ActiveStateChangeDate = vendorActiveHistory.ActiveStateChangeDate.Date;
            vendorActiveHistory.CreateDate = ConvertTimeToUtc(vendorActiveHistory.CreateDate);

            registrantService.ActiveInactiveVendor(vendorActiveHistory);

            if ((null != Activity) && Activity.ObjectId > 0 && !string.IsNullOrEmpty(Activity.ActivityArea) && !string.IsNullOrEmpty(Activity.Message))
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, Activity.ActivityArea, Activity.Message, Activity.ObjectId, null, Activity.ActivityType);
            }

            return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActiveInactiveCustomer(CustomerActiveHistory customerActiveHistory, ActivityVM Activity = null)
        {
            customerActiveHistory.ActiveStateChangeDate = customerActiveHistory.ActiveStateChangeDate.Date;
            customerActiveHistory.CreateDate = ConvertTimeToUtc(customerActiveHistory.CreateDate);

            registrantService.ActiveInactiveCustomer(customerActiveHistory);

            if ((null != Activity) && Activity.ObjectId > 0 && !string.IsNullOrEmpty(Activity.ActivityArea) && !string.IsNullOrEmpty(Activity.Message))
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, Activity.ActivityArea, Activity.Message, Activity.ObjectId, null, Activity.ActivityType);
            }

            return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Internal Notes
        public JsonResult GetListHandler(DataGridModel param, string applicationID)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var appId = Convert.ToInt32(applicationID);

            //Order by
            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Note"},
                {2, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = registrantService.LoadApplicationNotes(appId, param.Start, param.Length, searchText, orderBy,
                sortDirection);

            result.DTOCollection.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var json = new
            {
                draw = param.Draw,
                iTotalRecords = result.TotalRecords,
                iTotalDisplayRecords = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListHandlerForVendor(DataGridModel param, int vendorId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            //Order by
            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Note"},
                {2, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = registrantService.LoadApplicationNotesForVendor(vendorId, param.Start, param.Length, searchText, orderBy,
                sortDirection);

            result.DTOCollection.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var json = new
            {
                draw = param.Draw,
                iTotalRecords = result.TotalRecords,
                iTotalDisplayRecords = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListHandlerForCustomer(DataGridModel param, int customerId)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;

            //Order by
            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Note"},
                {2, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = registrantService.LoadApplicationNotesForCustomer(customerId, param.Start, param.Length, searchText, orderBy,
                sortDirection);

            result.DTOCollection.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var json = new
            {
                draw = param.Draw,
                iTotalRecords = result.TotalRecords,
                iTotalDisplayRecords = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ExportToExcel(string searchText = "")
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            if (sortcolumn.Equals("CreatedDateString"))
            {
                sortcolumn = "CreatedDate";
            }
            var appId = Convert.ToInt32(Request.QueryString[3]);
            var applicationNotes = registrantService.LoadApplicationNotesForExport(appId, searchText, sortcolumn,
                sortdirection);

            applicationNotes.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<ApplicationNoteViewMode, string>>()
            {
                u => u.CreatedDateString,
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = applicationNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public virtual ActionResult ExportToExcelForVendor(string searchText = "")
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            if (sortcolumn.Equals("CreatedDateString"))
            {
                sortcolumn = "CreatedDate";
            }
            var vendorId = Convert.ToInt32(Request.QueryString[3]);
            var vendorNotes = registrantService.LoadVendorNotesForExport(vendorId, searchText, sortcolumn,
                sortdirection);

            vendorNotes.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<ApplicationNoteViewMode, string>>()
            {
                u => u.CreatedDateString,
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = vendorNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        public virtual ActionResult ExportToExcelForCustomer(string searchText = "")
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            if (sortcolumn.Equals("CreatedDateString"))
            {
                sortcolumn = "CreatedDate";
            }
            var customerId = Convert.ToInt32(Request.QueryString[3]);
            var customerNotes = registrantService.LoadCustomerNotesForExport(customerId, searchText, sortcolumn,
                sortdirection);

            customerNotes.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<ApplicationNoteViewMode, string>>()
            {
                u => u.CreatedDateString,
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = customerNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("ApplicationNotes-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult AddNotesHandler(string applicationID, string notes)
        {
            var applicationNote = new ApplicationNote
            {
                ApplicationId = Convert.ToInt32(applicationID),
                UserId = SecurityContextHelper.CurrentUser.Id,
                Note = notes,
                CreatedDate = DateTime.UtcNow
            };
            registrantService.AddApplicationNote(applicationNote);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddNotesHandlerForCustomer(int customerId, string notes)
        {
            var applicationNote = new ApplicationNote
            {
                CustomerId = customerId,
                UserId = SecurityContextHelper.CurrentUser.Id,
                Note = notes,
                CreatedDate = DateTime.UtcNow
            };
            registrantService.AddApplicationNote(applicationNote);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddNotesHandlerForVendor(int vendorId, string notes)
        {
            var applicationNote = new ApplicationNote
            {
                VendorId = vendorId,
                UserId = SecurityContextHelper.CurrentUser.Id,
                Note = notes,
                CreatedDate = DateTime.UtcNow
            };
            registrantService.AddApplicationNote(applicationNote);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region TSF Remittance Internal Notes
        public JsonResult GetTSFListHandler(DataGridModel param, string claimID)
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                ? param.Search["value"].Trim()
                : string.Empty;
            var appId = Convert.ToInt32(claimID);

            //Order by
            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Note"},
                {2, "AddedBy"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];
            var result = registrantService.LoadTSFRemittanceNotes(appId, param.Start, param.Length, searchText, orderBy,
                sortDirection);

            result.DTOCollection.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });
            var json = new
            {
                draw = param.Draw,
                iTotalRecords = result.TotalRecords,
                iTotalDisplayRecords = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult TSFExportToExcel(string searchText = "")
        {
            var sortcolumn = Request.QueryString[1];
            var sortdirection = Request.QueryString[2];

            if (sortcolumn.Equals("CreatedDateString"))
            {
                sortcolumn = "CreatedDate";
            }
            var appId = Convert.ToInt32(Request.QueryString[3]);
            var tsfclaimNotes = registrantService.LoadTSFRemittanceNotesForExport(appId, searchText, sortcolumn,
                sortdirection);

            tsfclaimNotes.ToList().ForEach(s =>
            {
                s.CreatedDate = ConvertTimeFromUtc(s.CreatedDate);
            });

            var columnNames = new List<string>(){
                "CreatedDate", "Note", "AddedBy"
            };

            var delegates = new List<Func<TSFRemittanceNoteViewMode, string>>()
            {
                u => u.CreatedDateString,
                u=> u.Note,
                u => u.AddedBy,
            };

            string csv = tsfclaimNotes.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("TSFRemittanceNotes-{0}.csv", DateTime.Now.ToString(TreadMarksConstants.DateFormat));
            return File(bytes, @"tsfclaim/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        [HttpPost]
        public JsonResult AddTSFNotesHandler(string claimID, string notes)
        {
            var tsfclaimNote = new TSFRemittanceNote
            {
                TSFClaimId = Convert.ToInt32(claimID),
                UserId = SecurityContextHelper.CurrentUser.Id,
                Note = notes,
                CreatedDate = DateTime.UtcNow
            };
            registrantService.AddTSFRemittanceNote(tsfclaimNote);
            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Spotlight Search      

        // Default/SpotlightSearchResults/my+keywords+example
        public ActionResult SpotLightSearchResults(string keyWords)
        {
            var model = keyWords ?? string.Empty;
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.ngApp = "spotLightSearchResultApp";
                return View("SpotLightSearchResults", (object)model);
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }

        //Default/SpotlightSearch/my+example+words
        public JsonResult SpotLightSearch(string keyWords, string searchFilters, int? pageLimit = null)
        {
            var searchText = Server.UrlDecode(keyWords);
            var filters = Server.UrlDecode(searchFilters);

            Session.Remove("PreviousSearchWord");
            Session["PreviousSearchWord"] = searchText;

            Session.Remove("PreviousSearchFilters");
            Session["PreviousSearchFilters"] = filters;


            var result = Enumerable.Empty<RegistrantSpotlightModel>();
            if (!string.IsNullOrEmpty(searchText) && searchText.Trim().Length >= 2)
            {
                result = registrantService.GetRegistrantSpotlights(searchText, searchFilters, pageLimit);
                return Json(new { status = true, data = result }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SpotLightSearchPaginate(string keyWords, string section, int start = 0, int pageLimit = 50)
        {
            var d = Server.UrlDecode(keyWords);
            var result = Enumerable.Empty<RegistrantSpotlightModel>();
            if (!string.IsNullOrEmpty(d) && d.Trim().Length >= 2)
            {
                result = registrantService.GetRegistrantSpotlightsPaginate(keyWords, section, start, pageLimit);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadSpotlightViewMoreResult(DataGridModel param, string keyWords, string section, int pageLimit = 50, string temp = "")
        {
            var searchText = param.Search != null && param.Search.ContainsKey("value")
                 ? param.Search["value"].Trim()
                : string.Empty;
            if ((string.IsNullOrEmpty(searchText)) && (!string.IsNullOrEmpty(temp)))
            {
                searchText = temp.Trim();
            }
            Dictionary<int, string> columns = GetGridColumnsForSorting(section);

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];

            var sortDirection = param.Order[0]["dir"];

            //int orderColumnIndex;
            //int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            //var orderBy = orderColumnIndex.ToString();

            var result = registrantService.LoadSpotlightViewMoreResult(param.Start, param.Length, searchText, orderBy, sortDirection, keyWords, pageLimit, section);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private static Dictionary<int, string> GetGridColumnsForSorting(string section)
        {
            switch (section)
            {
                case "Applications":
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "ModuleType"},
                                                    {2, "PlaceHolder1"},
                                                    {3, "PlaceHolder3"},
                                                    {4, "PlaceHolder2"}
                                                   };
                    }
                case "Claims":
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "ModuleType"},
                                                    {2, "PlaceHolder1"},
                                                    {3, "PeriodStartDate"},
                                                    {4, "PlaceHolder3"}
                                                   };
                    }
                case "Remittances":
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "PlaceHolder1"},
                                                    {2, "PeriodStartDate"},
                                                    {3, "PlaceHolder3"}
                                                   };
                    }
                case "Users":
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "PlaceHolder1"},
                                                    {2, "PlaceHolder2"},
                                                    {3, "PlaceHolder3"},
                                                    {4, "PlaceHolder4"}
                                                   };
                    }
                case "Transactions":
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "PlaceHolder1"},
                                                    {2, "PlaceHolder2"},
                                                    {3, "PeriodStartDate"},
                                                    {4, "PlaceHolder4"},
                                                    {5, "PlaceHolder5"}
                                                   };
                    }
                default:
                    {
                        return new Dictionary<int, string> {
                                                    {0, "Status"},
                                                    {1, "ModuleType"},
                                                    {2, "PlaceHolder1"},
                                                    {3, "PlaceHolder2"},
                                                    {4, "PlaceHolder3"}
                                                   };
                    }
            }
        }

        [HttpPost]
        public JsonResult PreviousSearchWord()
        {
            var previousSearchWord = Session["PreviousSearchWord"] != null ? Session["PreviousSearchWord"].ToString() : string.Empty;
            var previousSearchFilters = Session["PreviousSearchFilters"] != null ? Session["PreviousSearchFilters"].ToString() : string.Empty;


            return Json(new { status = true, previousSearchWord = previousSearchWord, previousSearchFilters = previousSearchFilters }, JsonRequestBehavior.AllowGet);
        }

        #region common export - OTSTM2-960/963/964/965/966
        public ActionResult GetGlobalSearchCommonExport(string keyWords, string section)
        {
            var searchText = Request.QueryString[2];
            var sortColumn = Request.QueryString[3];
            var sortDirection = Request.QueryString[4];

            Dictionary<int, string> columns = GetGridColumnsForSorting(section);

            int orderColumnIndex;
            int.TryParse(sortColumn, out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];

            var result = registrantService.LoadSpotlightResultExport(searchText, orderBy, sortDirection, keyWords, section);

            string csv = GetCSVString(section, result);
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("{0}-{1}.csv", section, DateTime.Now.ToString("yyyy-MM-dd-hh-mm"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        private static string GetCSVString(string section, List<RegistrantSpotlightModel> result)
        {
            switch (section)
            {
                case "Applications":
                    {
                        var columnNames = new List<string>() { "Status", "Type", "Reg. #", "Operating Name", "Legal Name" };
                        var delegates = new List<Func<RegistrantSpotlightModel, string>>()
                        {
                            u => u.Status == "0" ? "Inactive" : "Active",
                            u => u.ModuleType,
                            u => u.PlaceHolder1,
                            u => u.PlaceHolder3,
                            u => u.PlaceHolder2
                        };
                        return result.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
                    }
                case "Claims":
                    {
                        var columnNames = new List<string>() { "Status", "Type", "Reg. #", "Period", "Amount ($)" };
                        var delegates = new List<Func<RegistrantSpotlightModel, string>>()
                        {
                            u => u.Status,
                            u => u.ModuleType,
                            u => u.PlaceHolder1,
                            u => u.PlaceHolder2,
                            u => u.PlaceHolder3
                        };
                        return result.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
                    }
                case "Remittances":
                    {
                        var columnNames = new List<string>() { "Status", "Reg. #", "Period", "Total ($)" };
                        var delegates = new List<Func<RegistrantSpotlightModel, string>>()
                        {
                            u => u.Status,
                            u => u.PlaceHolder1,
                            u => u.PlaceHolder2,
                            u => u.PlaceHolder3
                        };
                        return result.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
                    }
                case "Users":
                    {
                        var columnNames = new List<string>() { "Status", "Name", "Email", "Reg. #", "Business Name" };
                        var delegates = new List<Func<RegistrantSpotlightModel, string>>()
                        {
                            u => u.Status,
                            u => u.PlaceHolder1,
                            u => u.PlaceHolder2,
                            u => u.PlaceHolder3,
                            u => u.PlaceHolder4
                        };
                        return result.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
                    }
                case "Transactions":
                    {
                        var columnNames = new List<string>() { "Status", "Trans. #", "Type", "Trans. Date", "INC. Badge / Reg #", "Out. Reg. #" };
                        var delegates = new List<Func<RegistrantSpotlightModel, string>>()
                        {
                            u => u.Status,
                            u => u.PlaceHolder1,
                            u => u.PlaceHolder2,
                            u => u.PlaceHolder3,
                            u => u.PlaceHolder4,
                            u => u.PlaceHolder5
                        };
                        return result.ToCSV(",", columnNames.ToArray(), delegates.ToArray());
                    }
                default:
                    {
                        return string.Empty;
                    }
            }
        }
        #endregion

        #endregion

        #region Notifications

        public ActionResult SeeAllNotifications()
        {
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.ngApp = "seeAllNotification";
                ViewBag.SelectedMenu = "SeeAllNotification";
                return View();
            }
            else
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
            }
        }

        [HttpPost]
        public JsonResult LoadAllNotifications(DataGridModel param)
        {
            var userName = SecurityContextHelper.CurrentUser.UserName;

            var searchText = param.Search != null && param.Search.ContainsKey("value")
                  ? param.Search["value"].Trim()
                 : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadAllNotifications(param.Start, param.Length, searchText, orderBy, sortDirection, userName);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadUnreadNotifications()
        {
            var userName = SecurityContextHelper.CurrentUser.UserName;
            var notificationList = messageService.LoadUnreadNotifications(userName);
            return Json(notificationList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MarkAllAsRead()
        {
            var userName = SecurityContextHelper.CurrentUser.UserName;
            messageService.MarkAllAsRead(userName);
            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TotalUnreadNotification()
        {
            var userName = SecurityContextHelper.CurrentUser.UserName;
            var count = messageService.TotalUnreadNotification(userName);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TotalNotification()
        {
            var userName = SecurityContextHelper.CurrentUser.UserName;
            var count = messageService.TotalNotification(userName);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateNotificationStatus(int notificationId, string newStatus)
        {
            messageService.UpdateNotificationStatus(notificationId, newStatus);
            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RedirectToClaimSummaryPage(int notificationId)
        {
            var redirectionResult = messageService.RedirectToClaimSummaryPage(notificationId);
            return Json(redirectionResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllNotificationsExport(string searchText)
        {
            var sortColumn = Request.QueryString[1];
            var sortDirection = Request.QueryString[2];
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var userName = SecurityContextHelper.CurrentUser.UserName;
            var result = messageService.GetAllNotificationsExport(userName, searchText, sortColumn, sortDirection);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Details");
            List<Func<AllNotificationsExportModel, string>> delegates = new List<Func<AllNotificationsExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All Notifications Excel-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }
        #endregion

        #region Registration Activities
        [AllowAnonymous]
        [HttpPost]
        public ActionResult AddCommonActivity(int ObjectId, string ActivityArea, string logContent, int ActivityType = TreadMarksConstants.VendorRegistrationActivity)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                if ((ObjectId > 0 || TreadMarksConstants.AdminMenuActivity == ActivityType) && !string.IsNullOrEmpty(ActivityArea) && !string.IsNullOrEmpty(logContent))
                {
                    this.AddActivity(SecurityContextHelper.CurrentUser, ActivityArea, logContent, ObjectId, null, ActivityType);
                    //Publish activity event
                    DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(ObjectId);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult AddCommonActivityBatch(int ObjectId, string ActivityArea, string logContent, List<Dictionary<string, string>> logBatch, int ActivityType = TreadMarksConstants.VendorRegistrationActivity)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                if ((ObjectId > 0 || TreadMarksConstants.AdminMenuActivity == ActivityType) && !string.IsNullOrEmpty(ActivityArea) && !(string.IsNullOrEmpty(logContent) && (logBatch == null)))
                {
                    DateTime d = DateTime.Now;
                    logBatch.ForEach(s =>
                    {
                        this.AddActivity(SecurityContextHelper.CurrentUser, ActivityArea, s["value"], ObjectId, d, ActivityType);                        
                    });
                    //Publish activity event
                    DependencyResolver.Current.GetService<IEventAggregator>().GetEvent<ActivityEvent>().Publish(ObjectId);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
        }
        private void AddActivity(UserReference assignerUser, string ActivityArea, string logContent, int ObjectId, DateTime? logtime = null, int ActivityType = TreadMarksConstants.VendorRegistrationActivity)// default TreadMarksConstants.VendorRegistrationActivity, 6: application activity
        {
            var activity = new Activity();
            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant";
            string tmp = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.';
            this.messageService.AddActivity(new Activity()
            {
                ObjectId = ObjectId,
                InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant",
                CreatedTime = logtime ?? DateTime.Now,
                ActivityType = ActivityType,
                ActivityArea = ActivityArea,
                Message = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.',
                Initiator = assignerUser != null ? assignerUser.UserName : "Applicant",
                Assignee = assignerUser != null ? assignerUser.UserName : "Applicant@test.com",
                AssigneeName = assignerUser != null ? assignerUser.UserName : "Applicant",
            });
        }
        #endregion

        #region Activity
        public JsonResult LoadCommonActivities(DataGridModel param, int objectId = 0, string activityArea = "", int activityType = 0, string temp = "")
        {
            if ((activityType == TreadMarksConstants.VendorRegistrationActivity) && (0 == objectId))//to load application activity log, has to provide objectId
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var searchText = string.Empty;
            if (param.Search != null && param.Search.ContainsKey("value") && !string.IsNullOrEmpty(param.Search["value"].Trim()))
            {
                searchText = param.Search["value"].Trim();
            }
            else
            {
                if (!string.IsNullOrEmpty(temp))
                {
                    searchText = temp.Trim();
                }
            }

            //searchText = param.Search != null && param.Search.ContainsKey("value")
            //    ? param.Search["value"].Trim()
            //    : string.Empty;

            var columns = new Dictionary<int, string>
            {
                {0, "CreatedDate"},
                {1, "Initiator" },
                {2, "Message"}
            };

            int orderColumnIndex;
            int.TryParse(param.Order[0]["column"], out orderColumnIndex);
            var orderBy = columns[orderColumnIndex];
            var sortDirection = param.Order[0]["dir"];

            var result = messageService.LoadAllCommonActivities(param.Start, param.Length, searchText, orderBy, sortDirection, objectId, activityArea, activityType);

            var json = new
            {
                draw = param.Draw,
                recordsTotal = result.TotalRecords,
                recordsFiltered = result.TotalRecords,
                data = result.DTOCollection
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllActivitiesExport(string searchText, string activityArea = "", int activityType = 0)
        {
            var sortColumn = Request.QueryString["sortcolumn"];
            var sortDirection = Request.QueryString["sortDirection"];
            var searchTxt = Request.QueryString["searchText"];
            var claimId = Request.QueryString["claimId"];
            int cid = 0;
            int.TryParse(claimId, out cid);
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "CreatedDate";
                sortDirection = "desc";
            }
            var result = messageService.GetAllCommonActivitiesExport(cid, searchText, sortColumn, sortDirection, activityArea, activityType);

            var columns = new List<string>();
            columns.Add("Date/Time");
            columns.Add("Initiator");
            columns.Add("Activity");
            List<Func<AllActivitiesExportModel, string>> delegates = new List<Func<AllActivitiesExportModel, string>>() { u => u.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss tt"), u => u.Initiator, u => u.Message };

            string csv = result.ToCSV(",", columns.ToArray(), delegates.ToArray());
            var bytes = Encoding.UTF8.GetBytes(csv);
            var filename = string.Format("All Activity Excel-{0:yyyy-MM-dd-HH-mm-ss}.csv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
            return File(bytes, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", filename);
        }

        [HttpPost]
        public JsonResult TotalActivity(int claimId)
        {
            var count = messageService.TotalActivity(claimId);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}