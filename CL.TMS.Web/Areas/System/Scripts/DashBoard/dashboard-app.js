﻿"use strict";

// Dashboard APP

var dashboardApp = angular.module('DashboardApp', ['ui.bootstrap', 'commonLib']);

// Controllers Section

dashboardApp.controller('DashboardCtrl', ['$rootScope', '$scope', '$http', '$uibModal', '$log', function ($rootScope, $scope, $http, $uibModal, $log) {
    $scope.openBankDetail = function (Id) {
        $http({
            url: Global.Settings.LoadBankinfoDetaillURL,// '@Url.Action("GetBankinfoDetail", "FinancialIntegration", new { area = "System" })'
            method: "GET",
            params: { bankId: Id },
        }).success(function (result) {
            if (result.status) {
                var paperFormModal = $uibModal.open({
                    templateUrl: 'bank-detail-form.html',
                    controller: 'BankFormModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        bankDetailData: function () { return result.data; },
                        rowData: $scope.rowData,
                        vendorId: $scope.rowData.VendorID,
                        table: $scope.table,                      
                    }
                });
            } else {

            }
        }).error(function () {

        });
    };
    $scope.viewBankDetail = function (Id) {
        $http({
            url: Global.Settings.LoadBankinfoDetaillURL,//'@Url.Action("GetBankinfoDetail", "FinancialIntegration", new { area = "System" })'
            method: "GET",
            params: { bankId: Id },
        }).success(function (result) {
            if (result.status) {
                var paperFormModal = $uibModal.open({
                    templateUrl: 'view-bank-detail-form.html',
                    controller: 'BankFormModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        bankDetailData: function () { return result.data; },
                        rowData: $scope.rowData,
                        vendorId: $scope.rowData.VendorID,
                        table: $scope.table
                    }
                });
            } else {

            }
        }).error(function () {

        });
    };
    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
}]);

dashboardApp.controller('BankFormModalCtrl', ['$rootScope', '$scope', '$http', '$uibModal', '$uibModalInstance', '$window', 'bankDetailData', 'rowData', 'vendorId', 'table',
    function ($rootScope, $scope, $http, $uibModal, $uibModalInstance, $window, bankDetailData, rowData, vendorId, table) {//'$uibModalInstance', 

        //OTSTM2-573
        $scope.registrantTable = table.registrantTable;
        $scope.searchVal = table.searchVal;

        $scope.bankinfoData = bankDetailData;
        $scope.rowData = rowData;
        if ($scope.bankinfoData.submittedOnDate != null) {
            $scope.bankinfoData.submittedOnDate = $scope.bankinfoData.submittedOnDate.substring(0, 10);//short date format;
        }
        switch ($scope.bankinfoData.supportDocOption) {
            case 'optionmail':
                $scope.bankinfoData.supportDocOption = 'Mail';
                break;
            case 'optionupload':
                $scope.bankinfoData.supportDocOption = 'Upload';
                break;
            case 'optionfax':
                $scope.bankinfoData.supportDocOption = 'Fax';
                break;
            default:
        }
        $scope.showRejectBtn = $scope.bankinfoData.reviewStatus == 'Submitted';

        if ($scope.bankinfoData.reviewStatus == 'Approved') {
            $scope.displayActionText = 'Save';
        } else {
            $scope.displayActionText = 'Approve';
        };

        $scope.cancelEdit = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.approveBankInfo = function () {

            if ($scope.bankinfoData.reviewStatus == 'Approved') {
                $scope.bankinfoData.reviewStatus = '';//save bank info doesn't need status
            }

            $scope.updatedBankInfo = $scope.bankinfoData;

            $http({
                url: Global.Settings.SaveBankinfoDetaillURL,//'@Url.Action("Savebankinfo", "FinancialIntegration", new { area = "System" })'
                method: "POST",
                data: JSON.stringify({
                    bankinfoData: {
                        BankName: $scope.bankinfoData.bankName,
                        TransitNumber: $scope.bankinfoData.transitNumber,
                        BankNumber: $scope.bankinfoData.bankNumber,
                        AccountNumber: $scope.bankinfoData.accountNumber,
                        ID: $scope.bankinfoData.bankinfoId,
                        ReviewStatus: $scope.bankinfoData.reviewStatus,
                        CCEmail: $scope.bankinfoData.ccEmail,
                        Email: $scope.bankinfoData.email,
                    }
                }),
            }).success(function (result) {
                console.log('success' + result.statusMsg);
                //reload table, call table.search()
                // $('#glyphicon-search-Reg').click();
                //$('#tblRegistrantlist').DataTable().ajax.reload(); // this cause popover menu open failure after reload table, using $window.location.reload() for now -Y

                $scope.registrantTable.search($scope.searchVal).draw(false);
                //$window.location.reload();
            }).error(function () {
            });

            $uibModalInstance.close();
        };

        $scope.openConfirmation = function (eventName, templateName) {
            $uibModalInstance.dismiss('cancel');
            var confirmModal = $uibModal.open({
                templateUrl: templateName,
                controller: 'ConfirmModalCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    eventName: function () { return eventName; },
                    bankinfoId: $scope.bankinfoData.bankinfoId,
                    vendorId: vendorId,
                    table: table
                }
            });

            confirmModal.result.then(function (data) {
                if (data == 'submit-paper-form') {
                    $http({
                        url: Global.Settings.AddPaperFormServiceURL,
                        method: "POST",
                        data: { transactionDataModel: $scope.bankinfoData }
                    }).success(function (result) {
                        if (result.status) {
                            $uibModalInstance.close();
                        } else {
                        }
                    }).error(function () {
                    });
                }
            });
        };

    }]);

dashboardApp.controller('ConfirmModalCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'eventName', 'bankinfoId', 'vendorId', 'table',
    function ($rootScope, $scope, $http, $uibModalInstance, $window, eventName, bankinfoId, vendorId, table) {

        //OTSTM2-573
        $scope.registrantTable = table.registrantTable;
        $scope.searchVal = table.searchVal;

        $scope.eventName = eventName;
        $scope.confirm = function () {
            $uibModalInstance.close($scope.eventName);
        }

        $scope.rejectBankInfo = function () {
            $uibModalInstance.dismiss('cancel');
            ///delete bank info and reload
            $http({
                url: Global.Settings.RejectBankinfoDetaillURL,//'@Url.Action("RejectBankinfo", "FinancialIntegration", new { area = "System" })'
                method: "POST",
                params: { bankId: bankinfoId, vendorId: vendorId },
            }).success(function (result) {
                if (result.status) {
                    //$window.location.reload();
                    //OTSTM2-573
                    $scope.registrantTable.search($scope.searchVal).draw(false);
                } else {

                }
            }).error(function () {

            });

        };

        $scope.cancelReject = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);
