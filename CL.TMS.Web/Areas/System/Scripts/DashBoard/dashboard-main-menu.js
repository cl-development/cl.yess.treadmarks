﻿"use strict";

/*************** App ********
*Dashboard APP (inject with metricApp, announcementApp and stcApp should be injected later)
******************************/
var dashboardApp = angular.module('dashboardMainMenu', ['ui.bootstrap', 'datatables', 'datatables.scroller', 'commonLib', "dashboard.services", "dashboard.directives", "dashboard.controllers", 'metricApp']);

//*********service section *******************
var services = angular.module("dashboard.services", []);
services.factory("dashboardService", ["$http", function ($http) {
    var factory = {};

    factory.loadListByID = function (data) {
        var submitVal = {
            url: Global.Dashboard.loadListUrl,
            method: "POST",
            data: { paramModel: data }
        }
        return $http(submitVal);
    }
    factory.loadAnnouncementList = function (url) {
        var submitVal = {
            url: url,
            method: "GET",
            params: {}
        }
        return $http(submitVal);
    }
    factory.loadTCRServiceThresholdList = function () {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.LoadTCRServiceThresholdListUrl,
            method: "GET",
            data: {}
        }
        return $http(submitVal);
    }
    factory.addTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.AddNewTCRServiceThresholdUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.removeTCR = function (id) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.RemoveTCRServiceThresholdUrl,
            method: "POST",
            data: { id: id }
        }
        return $http(submitVal);
    }
    factory.updateTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.UpdateTCRServiceThresholdUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.exportTCR = function (sortColumnName,direction) {
        var submitVal = {
            url: Global.DashBoard.ExportToExcelForTCR,
            method: "POST",
            data: { column: sortColumnName, direction: direction }
        }
        return $http(submitVal);
    }
    // constants
    factory.Security = {
        isReadonly: function (resource) {
            return resource === Global.Dashboard.Security.Constants.ReadOnly
        },
        isEditSave: function (resource) {
            return resource === Global.Dashboard.Security.Constants.EditSave
        },
        isNoAccess: function (resource) {
            return resource === Global.Dashboard.Security.Constants.NoAccess
        },
    };

    factory.loadSTCEventsCount = function () {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.LoadSTCEventsCountUrl,
            method: "POST",
            data: { }
        }
        return $http(submitVal);
    } 
    factory.getSTCEventDetailsByEventNumber = function (eventNumber) {
        return $http({
            method: 'GET',
            url: Global.DashBoard.LoadSTCEventDetailsByEventNumberUrl,
            params: {
                eventNumber: eventNumber
            }
        });
    }

    factory.AddNewSTCEvent = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.AddNewSTCEventUrl,
            method: "POST",
            data: { newEvent: data }
        }
        return $http(submitVal);
    }
    factory.UpdateSTCEvent = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.UpdateSTCEventUrl,
            method: "POST",
            data: { updatedEvent: data }
        }
        return $http(submitVal);
    }
    factory.RemoveSTCEvent = function (eventNumber, id) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.RemoveSTCEventUrl,
            method: "POST",
            data: { eventNumber: eventNumber, id: id }
        }
        return $http(submitVal);
    }


    factory.loadTCRServiceThresholdList = function () {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.LoadTCRServiceThresholdListUrl,
            method: "GET",
            data: {}
        }
        return $http(submitVal);
    }
    factory.addTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.AddNewTCRServiceThresholdUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.removeTCR = function (id) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.RemoveTCRServiceThresholdUrl,
            method: "POST",
            data: { id: id }
        }
        return $http(submitVal);
    }
    factory.updateTCR = function (data) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DashBoard.UpdateTCRServiceThresholdUrl,
            method: "POST",
            data: { vm:data }
        }
        return $http(submitVal);
    }

    return factory;
}]);


//*****************************************************************
//                 directives section_Start
//*****************************************************************
var directives = angular.module("dashboard.directives", []);
directives.directive("dashboardAnnouncementListPanel",
    ["$window", "$timeout", "dashboardService", "$http", "$compile", "$templateCache", "$rootScope", "$sce", "$uibModal",
    function ($window, $timeout, dashboardService, $http, $compile, $templateCache, $rootScope, $sce, $uibModal) {
        return {
            scope: {
                url: "@",
            },
            restrict: "E",
            templateUrl: "dashboard-announcement-list-panel.html",
            link: function (scope, elem, attr, ctrl) {
            },
            controller: ["$scope", "$sce", function ($scope, $sce) {
                $scope.vm = {};
                $scope.isView = false;
                $rootScope.$on("PANEL_VIEW_UPDATED", function (e, tableId) {
                    window.location.reload(true);
                });

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;
                    $scope.dtOptions.withScroller()
                                    .withOption("deferRender", true)
                                    .withOption("scrollY", 220)
                                    .withOption("scrollX", "100%");
                }
                dashboardService.loadAnnouncementList($scope.url).then(function (data) {
                    if (data && data.data) {
                        $("#announcementList").show();
                        $scope.vm = $.each(data.data.result, function (index, item) {
                            var trimedStr = item.Description.replace(/['"]+/g, '').replace(/ {1,}/g, " ");
                            item.isOver1000 = false;
                            if (trimedStr.length > 1000) {
                                item.shortDesc = item.Description.substr(0, 1000);
                                item.isOver1000 = true;
                            } else {
                                item.shortDesc = item.Description;
                            }
                        });
                    } else {
                        $("#announcementList").hide();
                    }
                });
                $scope.more = function (subject, description) {
                    var rateDetailModal = $uibModal.open({
                        templateUrl: 'modal-show-full-announcement.html',
                        controller: 'showFullAnnouncementCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            subject: function () { return subject; },
                            description: function () { return description; },
                        }
                    });
                }
            }]
        }
    }]);

//***************STC Premium_start******************
directives.directive('stcPremiumListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'dashboardService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, dashboardService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {                
            },
            templateUrl: 'stc-premium-list-panel.html',
            controller: ['$scope', function ($scope) {
                $scope.ShowActionGear = Global.DashBoard.Security.STCAuthorize === Global.DashBoard.Security.Constants.EditSave;
                $scope.isView = Global.DashBoard.Security.STCAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
                $scope.dtInstance = {};

                dashboardService.loadSTCEventsCount().then(function (result) {
                    if (result.data) {
                        $scope.countVM = result.data.result;
                    }
                });

                $rootScope.$on('PANEL_VIEW_UPDATED_STC_PREMIUM', function (e) {
                    $scope.dtInstance.DataTable.draw();
                    dashboardService.loadSTCEventsCount().then(function (result) {
                        if (result.data) {
                            $scope.countVM = result.data.result;
                        }
                    });
                });

                $scope.dtColumns = [
                        DTColumnBuilder.newColumn("ID", "ID").withOption('name', 'ID').withClass('hidden'),
                        DTColumnBuilder.newColumn("EventNumber", "Event Number").withOption('name', 'EventNumber').notSortable().withClass("td-medium").renderWith(function (data, type, full, meta) {
                            return '<div style="margin-left: -20px">' + data + '</div>';
                        }), ,
                        DTColumnBuilder.newColumn("Description", "Description").withOption('name', 'Description').notSortable().withClass("td-large"),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().renderWith(actionHtml).withClass("td-small"),                       
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.url,
                    type: "GET",
                    error: function (xhr, error, thrown) { }
                }).withOption('processing', false)
                    .withOption('responsive', true).withOption('bAutoWidth', false)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', [3, 'desc'])
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);                      
                    })
                    .withOption('initComplete', function (settings, result) {
                        
                    })
                    .withOption('drawCallback', function (settings, result) {
                        
                    });

                $scope.dtOptions.withScroller()
                                    .withOption('deferRender', true)
                                    .withOption('scrollY', 291)
                                    .withOption('scrollX', "100%");

                //view more
                //$scope.viewMoreClick = function ($event) {
                //    $scope.viewMore = false;
                //    $scope.scroller = true;
                       
                      
               
                function actionHtml(data, type, full, meta) {
                    var html = $compile($templateCache.get('item-action.html')
                                .replace(/itemNumber/g, data.EventNumber)
                                .replace(/itemID/g, data.ID)
                                .replace(/itemDescription/g, data.Description)
                                )($scope);
                    return html[0].outerHTML;
                }

                //click to view
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).addClass('rowClickable');
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function (ev) {
                        $scope.$apply(function () {
                            $scope.openFormForView(aData);
                        });
                    });
                    return nRow;
                }

                $scope.openFormForView = function (aData) {
                    var viewModal = $uibModal.open({
                        templateUrl: 'modal-stc-premium-rate-detail-template.html',
                        controller: 'editStcPremiumController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            eventNumber: function () {
                                return aData.EventNumber;
                            },
                            isView: function () {
                                return true;
                            }
                        }
                    });
                    // after modal is rendered       
                    viewModal.rendered.then(function () {
                        if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
                            $("#responsiveDiv1").removeClass("margin-left-neg-15");
                            $("#responsiveDiv2").removeClass("margin-right-neg-25");
                            $("#startDateViewMode").find("br").remove();
                            $("#endDateViewMode").find("br").remove();
                        }
                        else if (matchMedia('(max-width: 767px)').matches) {
                            $("#responsiveDiv1").removeClass("margin-left-neg-15");
                            $("#responsiveDiv2").removeClass("margin-right-neg-25");
                            $("#startDateViewMode").find("br").remove();
                            $("#endDateViewMode").find("br").remove();
                        }
                        else {
                            $("#responsiveDiv1").addClass("margin-left-neg-15");
                            $("#responsiveDiv2").addClass("margin-right-neg-25");
                            if ($("#startDateViewMode").find("br").length == 0) {
                                $("#startDateViewMode label").first().after("<br />");
                            }
                            if ($("#endDateViewMode").find("br").length == 0) {
                                $("#endDateViewMode label").first().after("<br />");
                            }
                        }
                    });
                }
            }]
        }
    }]);

//for show action gear pop-up
directives.directive('tablerowpopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);
//Add STC Premium Button handler
directives.directive('addStcPremiumButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName: '@',
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.DashBoard.Security.STCAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.openModal = function () {
                var addModal = $uibModal.open({
                    templateUrl: 'modal-stc-premium-rate-detail-template.html',
                    controller: 'addStcPremiumController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        panelName: function () {
                            return $scope.panelName;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
                // after modal is rendered       
                addModal.rendered.then(function () {
                    if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
                        $("#responsiveDiv1").removeClass("margin-left-neg-15");
                        $("#responsiveDiv2").removeClass("margin-right-neg-25");                       
                    }
                    else if (matchMedia('(max-width: 767px)').matches) {
                        $("#responsiveDiv1").removeClass("margin-left-neg-15");
                        $("#responsiveDiv2").removeClass("margin-right-neg-25");                      
                    }
                    else {
                        $("#responsiveDiv1").addClass("margin-left-neg-15");
                        $("#responsiveDiv2").addClass("margin-right-neg-25");                       
                    }
                });
            };
        }]
    };
}]); 
 
//Edit STC Premium Button handler
directives.directive('editStcPremiumButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            itemNumber: '@',
            itemId: '@',
            itemDescription: '@',
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.STCAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                var editModal = $uibModal.open({
                    templateUrl: 'modal-stc-premium-rate-detail-template.html',
                    controller: 'editStcPremiumController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eventNumber: function () {
                            return $scope.itemNumber;
                        },                                             
                        isView: function () {
                            return false;
                        }
                    }
                });
                // after modal is rendered       
                editModal.rendered.then(function () {
                    if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
                        $("#responsiveDiv1").removeClass("margin-left-neg-15");
                        $("#responsiveDiv2").removeClass("margin-right-neg-25");
                    }
                    else if (matchMedia('(max-width: 767px)').matches) {
                        $("#responsiveDiv1").removeClass("margin-left-neg-15");
                        $("#responsiveDiv2").removeClass("margin-right-neg-25");
                    }
                    else {
                        $("#responsiveDiv1").addClass("margin-left-neg-15");
                        $("#responsiveDiv2").addClass("margin-right-neg-25");
                    }
                });
            }
        }]
    }
}]);    

//Delete STC Premium Button handler
directives.directive('removeStcPremiumButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            itemNumber: '@',
            itemId: '@',           
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.STCAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeStcPremiumController',
                    size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            EventNumber: function () {
                                return $scope.itemNumber;
                            },
                            id: function () {
                                return $scope.itemId;
                            },
                            isView: function () {
                                return false;
                            }
                      }
                });
            }
        }]
    };
}]);

directives.directive('validNumberStc', function () {
    return {
        require: '?ngModel',
        scope: {
            theprefex: '@',
            decimalsize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {
            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 12;
            var zero = "00000";
            var prefex = scope.theprefex == "%" || scope.theprefex == "non-dollar" ? '' : '$';
            var scale;


            ngModelCtrl.$parsers.push(function (val) {

                if (angular.isUndefined(val)) {
                    var val = '0';
                }


                var clean = val.replace(/[^0-9\.]/g, '');
                var decimalCheck = clean.split('.');
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(prefex + clean);
                    ngModelCtrl.$render();
                }
                return clean != "" ? prefex + clean : clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            }).bind('blur', function () {
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                var elementValue = element.val() ? element.val().split('$').length > 1 ? element.val().split('$')[1] : element.val().split('$')[0] : '';
                if (elementValue == '' || elementValue == '-' || elementValue.indexOf('-') > -1) {
                    ngModelCtrl.$setViewValue(prefex + (scale > 0 ? "0." + zero.substr(0, scale) : "0"));
                } else {
                    var splitValue = elementValue.split('.');
                    if (splitValue.length > 1 && splitValue[1].length > scale) {
                        elementValue = splitValue[0] + '.' + splitValue[1].substr(0, scale);
                    }
                    $(".valid-number-error").hide()
                    var fixedValue = parseFloat(elementValue).toFixed(scale).toString();
                    if (scope.theprefex) {
                        if (scope.theprefex == "%" && parseFloat(elementValue) > 100) {
                            $(".valid-number-error").show();
                            fixedValue = "0";
                        }
                    } else {
                        fixedValue = prefex + fixedValue;
                    }
                    ngModelCtrl.$setViewValue(fixedValue);
                }
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});
//***************STC Premium_end******************



//***************TCR Service Threshold start******************
directives.directive('tcrServiceThresholdListPanel', ['$window','DTOptionsBuilder', 'DTColumnBuilder', 'dashboardService', 
    function ($window,DTOptionsBuilder,DTColumnBuilder, dashboardService) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {
                angular.element($window).bind('resize', function () {
                    scope.dtInstance.DataTable.draw();
                    $(".dataTables_scrollHead table").css("width", "100%");                   
                    $(".dataTables_scrollHead").css("height", "70px");
                    $(".dataTables_scrollHead table>thead>tr>th").css("border-bottom", 0);
                    $(".dataTables_scrollHead").css("margin-top", "-20px");
                    scope.$digest();                    
                });      
            },
            templateUrl: 'tcr-service-threshold-list-panel.html',
            controller:function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
                $scope.ShowActionGear = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.EditSave;
                $scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
                $scope.dtInstance = {};
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "ID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("RegNumber", "Reg#").withOption('name', 'Reg#').withClass('text-left').withOption("width","80px"), 
                    DTColumnBuilder.newColumn("BusinessName", "Company").withOption('name', 'BusinessName').withClass('text-left'),
                    DTColumnBuilder.newColumn("LastServicedDate", "Last Serviced Date").withOption('name', 'LastServicedDate').withClass("text-center")
                        .renderWith(function (data, type, full, meta) {
                        return  data != null ? data.substring(0, data.indexOf("T")): "";                      
                    }),
                    DTColumnBuilder.newColumn("DaysSinceLastServiced", "Days since Last Service").withOption('name', 'DaysSinceLastServiced').withClass("text-center"),
                    DTColumnBuilder.newColumn("ThresholdDays", "Threshold Days").withOption('name', 'ThresholdDays').withClass("text-center"),
                    DTColumnBuilder.newColumn(null).withTitle("Days Over").withOption('name', "DaysOver").withClass("text-center")
                        .renderWith(function (data, type, full, meta) {
                        return data.DaysLeft == 0 && data.DaysSinceLastServiced >0 ? (data.DaysSinceLastServiced - data.ThresholdDays) : "";
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withClass('text-center').withOption("width","110px")
                                        .renderWith(actionsHtml),
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.url,
                    type: "GET",
                    error: function (xhr, error, thrown) {
                        window.location.reload(true);
                    }
                }).withOption('processing', true)
                    .withOption('serverSide', true) 
                    .withOption('aaSorting', [1, 'desc'])
                    .withOption('responsive', true)                   
                    .withOption('autoWidth', true)                    
                    .withDOM('tr')
                    .withScroller()
                    .withOption('deferRender', true)
                    .withOption('scrollY', 240) 
                    .withOption('lengthChange', true)  
                    .withOption('scrollCollpase',true)  
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('initComplete', function (settings, result) {           
                        resetTableStyle();    
                    })
                    .withOption('drawCallback', function (settings, result) {          
                        resetTableStyle();                        
                        var direction = settings.aaSorting[0][1];                      
                        var sortColumnName = settings.aoColumns[settings.aLastSort[0].col].sName;
                        var url = Global.DashBoard.ExportToExcelForTCR.replace('AAA', sortColumnName).replace('BBB', direction);
                        $("#tcrExport").attr("href", url);
                    });          

                dashboardService.loadTCRServiceThresholdList().then(function (result) {
                    if (result.data) {
                        $scope.vm = result.data;                       
                    }
                });

                $rootScope.$on('PANEL_VIEW_UPDATED_TCR', function (e) {
                    $scope.dtInstance.DataTable.draw();
                    resetTableStyle();
                    dashboardService.loadTCRServiceThresholdList().then(function (result) {
                        if (result.data) {
                            $scope.vm = result.data;                            
                        }
                    });
                });

                $scope.openViewModal = function (aData) {
                    var viewModal = $uibModal.open({
                        templateUrl: 'modal-tcr-detail-template.html',
                        controller: 'editTcrController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            adata: function () {
                                return aData;
                            },
                            isView: function () {
                                return true;
                            }
                        }
                    });
                }
                $scope.filterThresholds = function (isFilter) {
                    if (isFilter) {
                        $scope.dtInstance.DataTable.search(isFilter).draw(false);
                    } else {                        
                        $scope.dtInstance.DataTable.search(isFilter).draw(false);
                    }
                }
                function resetTableStyle() { 
                    $(".dataTables_scrollHead table").css("width", "100%");
                    $(".dataTables_scrollHead").css("height", "70px");
                    $(".dataTables_scrollHead").css("margin-top", "-20px");
                    $(".dataTables_scrollHead table>thead>tr>th").css("border-bottom", 0);
                };
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function () {
                        if ($scope._cancelMouseClick) {
                            $scope._cancelMouseClick = false;
                            return;
                        }
                        $scope.$apply(function () {
                            $scope.openViewModal(aData);
                        });
                    });
                    return nRow;
                }
                function actionsHtml(data, type, full, meta) {
                    if ($scope.ShowActionGear){
                        var temp = $compile($templateCache.get('tcr-list-panel-action.html')
                            .replace(/ID/g, data.ID)
                            .replace(/regNumber/g, data.RegNumber)
                            .replace(/thresholdDays/g, data.ThresholdDays)
                            .replace(/businessName/g, data.BusinessName)
                            .replace(/description/g, data.Description)
                            )($scope);
                        return temp[0].outerHTML;
                    } else {
                        return "";
                    }
                }
         
            }
        }
    }]);
directives.directive('addTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName: '@',
            adata: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.openModal = function () {
                var addModal = $uibModal.open({
                    templateUrl: 'modal-tcr-detail-template.html',
                    controller: 'addTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        panelName: function () {
                            return $scope.panelName;
                        },
                        adata: function () {
                            return $scope.adata;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            };
        }]
    };
}]);
directives.directive('editTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            //adata: '@',
            tcrId:'@',
            regNumber: '@',
            thresholdDays: '@',
            businessName: '@',
            descript: '@'
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                var editModal = $uibModal.open({
                    templateUrl: 'modal-tcr-detail-template.html',
                    controller: 'editTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        adata: function () {
                            var adata=
                            {   
                                ID:parseInt($scope.tcrId),
                                RegNumber:$scope.regNumber,
                                ThresholdDays:parseInt($scope.thresholdDays),
                                BusinessName:$scope.businessName,
                                Description:$scope.descript                                    
                            };
                            return adata;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    }
}]);
directives.directive('removeTcrButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            itemNumber: '@',
            businessName: '@',
            itemId: '@',
        },
        link: function (scope, el, attrs, roleFormController) {
            scope.isView = Global.DashBoard.Security.TCRAuthorize === Global.DashBoard.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeTcrController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        regNumber: function () {
                            return $scope.itemNumber;
                        },
                        businessName: function () {
                            return $scope.businessName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    };
}]);
//***************TCR end******************

//*****************************************************************
//                 directives section_End
//*****************************************************************



//*****************************************************************
//                 controllers section_Start
//*****************************************************************
var controllers = angular.module('dashboard.controllers', []);
controllers.controller('dashboardControl', ['$rootScope', '$scope', '$http', '$uibModal', "dashboardService", function ($rootScope, $scope, $http, $uibModal, dashboardService) {
    
}]);



//***************STC Premium_start******************
controllers.controller('addStcPremiumController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'panelName', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, panelName, isView) {
    $scope.isValid = {       
        effectiveStartDate: true,
        effectiveEndDate: true,
        endDateOverStartDate: true,
        onRoadRate: true,
        offRoadRate: true,
        description: true,
        checkValid: function () {
            return this.effectiveStartDate && this.effectiveEndDate && this.endDateOverStartDate && this.onRoadRate && this.offRoadRate && this.description
        },
    }
    $scope.isEdit = false;
    $scope.isView = isView;

    dashboardService.getSTCEventDetailsByEventNumber(0).then(function (data) {//0:initial a new event
        $scope.vm = eventViewModel(data.data);
        $scope.vm.decimalsize = 3;
        $scope.vm.StartDate = $scope.vm.EffectiveStartDate.substring(0, 10);
        $scope.vm.EndDate = $scope.vm.EffectiveEndDate.substring(0, 10);
        triggerSTCDateTimePicker();
    });
    
    $scope.firstLoad = true;
    $scope.$watch('vm.Description', function (newVal, oldVal) {
        if (newVal == null || newVal == undefined) {
            $scope.isValid.description = true;
        }
        else {
            if ($scope.firstLoad) {
                $scope.isValid.description = true;
                $scope.firstLoad = false;
            }
            else {
                if (newVal == "") {
                    $scope.isValid.description = false;
                }
                else {
                    $scope.isValid.description = true;
                }          
            }
        }
        
    });

    $scope.$watch('vm.EndDate', function (newVal, oldVal) {
        if (newVal) {
            $scope.isValid.endDateOverStartDate = $scope.vm.StartDate <= newVal;
        }
    });

    $scope.$watch('vm.StartDate', function (newVal, oldVal) {
        if (newVal) {
            $scope.isValid.endDateOverStartDate = newVal <= $scope.vm.EndDate;
        }
    });

    $scope.submitContent = function () {
        //validate description, onRoadRate and offRoadRate 
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description != "";
        $scope.isValid.onRoadRate = $scope.vm.OnRoadRate != null;
        $scope.isValid.offRoadRate = $scope.vm.OffRoadRate != null;
        //validate endDateOverStartDate
        $scope.isValid.endDateOverStartDate = $scope.vm.StartDate <= $scope.vm.EndDate;
        

        if (!$scope.isValid.checkValid()) {           
            return;
        }
        else {
            //update the datetime values (EffectiveStartDate and EffectiveEndDate)
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var startDT = Date.parse(moment($scope.vm.StartDate));
            $scope.vm.EffectiveStartDate = (new Date(startDT - tzoffset)).toISOString().slice(0, -1); //local time
            //$scope.vm.EffectiveStartDate = new Date(startDT).toISOString().split('Z')[0]; //UTC time
            var endDT = Date.parse(moment($scope.vm.EndDate));
            $scope.vm.EffectiveEndDate = (new Date(endDT - tzoffset)).toISOString().slice(0, - 1); //local time
            //$scope.vm.EffectiveEndDate = new Date(endDT).toISOString().split('Z')[0]; //UTC time
        }
        
        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmAddSTCEventHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm     
            var data = eventPostModel($scope.vm);
            dashboardService.AddNewSTCEvent(data).then(function (response) {
                $rootScope.$emit('PANEL_VIEW_UPDATED_STC_PREMIUM', null);
                $uibModalInstance.close(true);
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });       
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('editStcPremiumController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'eventNumber', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, eventNumber, isView) {
    $scope.isValid = {
        effectiveStartDate: true,
        effectiveEndDate: true,
        endDateOverStartDate: true,
        onRoadRate: true,
        offRoadRate: true,
        description: true,
        checkValid: function () {
            return this.effectiveStartDate && this.effectiveEndDate && this.endDateOverStartDate && this.onRoadRate && this.offRoadRate && this.description
        },
    };

    $scope.isEdit = true;
    $scope.isView = isView;

    dashboardService.getSTCEventDetailsByEventNumber(eventNumber).then(function (data) {//0:initial a new event
        $scope.vm = eventViewModel(data.data);
        $scope.vm.decimalsize = 3;
        $scope.vm.StartDate = $scope.vm.EffectiveStartDate.substring(0, 10);
        $scope.vm.EndDate = $scope.vm.EffectiveEndDate.substring(0, 10);
        triggerSTCDateTimePicker();
    });

    $scope.$watch('vm.Description', function (newVal, oldVal) {
        if (newVal == null || newVal == undefined) {
            $scope.isValid.description = true;
        }
        else { 
            if (newVal == "") {
                $scope.isValid.description = false;
            }
            else {
                $scope.isValid.description = true;
            }                 
        }
        
    });

    $scope.$watch('vm.EndDate', function (newVal, oldVal) {
        if (newVal) {
            $scope.isValid.endDateOverStartDate = $scope.vm.StartDate <= newVal;
        }
    });

    $scope.$watch('vm.StartDate', function (newVal, oldVal) {
        if (newVal) {
            $scope.isValid.endDateOverStartDate = newVal <= $scope.vm.EndDate;
        }
    });

    $scope.editSaveEvent = function () {
        //validate description, onRoadRate and offRoadRate 
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description != "";
        $scope.isValid.onRoadRate = $scope.vm.OnRoadRate != null;
        $scope.isValid.offRoadRate = $scope.vm.OffRoadRate != null;
        //validate endDateOverStartDate
        $scope.isValid.endDateOverStartDate = $scope.vm.StartDate <= $scope.vm.EndDate;


        if (!$scope.isValid.checkValid()) {
            return;
        }
        else {
            //update the datetime values (EffectiveStartDate and EffectiveEndDate)
            var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
            var startDT = Date.parse(moment($scope.vm.StartDate));
            $scope.vm.EffectiveStartDate = (new Date(startDT - tzoffset)).toISOString().slice(0, -1); //local time
            //$scope.vm.EffectiveStartDate = new Date(startDT).toISOString().split('Z')[0]; //UTC time
            var endDT = Date.parse(moment($scope.vm.EndDate));
            $scope.vm.EffectiveEndDate = (new Date(endDT - tzoffset)).toISOString().slice(0, - 1); //local time
            //$scope.vm.EffectiveEndDate = new Date(endDT).toISOString().split('Z')[0]; //UTC time           
        }

        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmEditSTCEventHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm     
            var data = eventPostModel($scope.vm);
            dashboardService.UpdateSTCEvent(data).then(function (response) {
                if (response.data.status) {                
                    $rootScope.$emit('PANEL_VIEW_UPDATED_STC_PREMIUM', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventEditFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });                 
                }
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('removeStcPremiumController', ['$scope', '$rootScope',  '$uibModal','$uibModalInstance', '$http', 'EventNumber', 'id', 'isView', 'dashboardService', function ($scope, $rootScope,$uibModal, $uibModalInstance,  $http, EventNumber, id, isView, dashboardService) {
        $scope.vm = {          
            ID: id,
            EventNumber: EventNumber,
            isView: isView
        }
        $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
        $scope.confirmMessage = Global.DashBoard.ConfirmRemoveSTCEventHtml.replace("Event#", $scope.vm.EventNumber);

        $scope.confirm = function () {
            dashboardService.RemoveSTCEvent($scope.vm.EventNumber, $scope.vm.ID).then(function (response) {
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_STC_PREMIUM', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventRemoveFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });
                }
            });
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

controllers.controller('stcEventFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', '$rootScope', function ($scope, $uibModal, $uibModalInstance, failResult, header, $rootScope) {

    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
        $rootScope.$emit('PANEL_VIEW_UPDATED_STC_PREMIUM', null);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $rootScope.$emit('PANEL_VIEW_UPDATED_STC_PREMIUM', null);
    };
}]);

//***************STC Premium_end******************










//*****************TCR controlls******************
controllers.controller('addTcrController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'panelName', 'adata', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, panelName, adata, isView) {
    $scope.isValid = {      
        regNumberAvailable: true,    
        thresholdDaysInvalid: true,
        description: true,
        checkValid: function () {
            return this.regNumberAvailable && this.thresholdDaysInvalid && this.description;
        }
    };
    $scope.vm = {
        RegNumber: null,
        BusinessName: "",
        ThresholdDays: 0,
        Description: null,
        VendorID: 0,
        vendors: JSON.parse(adata)
    }
    $scope.isView = isView;  
    $scope.isNew = true;
   
    $scope.thresholdDays = function () {
        $scope.isValid.thresholdDaysInvalid = true;
        $scope.thresholdDaysMsg = "";
        if ($scope.vm.ThresholdDays == null || $scope.vm.ThresholdDays ==undefined || $scope.vm.ThresholdDays === "") {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Required";
        } else if (!isNaN($scope.vm.ThresholdDays)) {
            if ($scope.vm.ThresholdDays > 0 && $scope.vm.ThresholdDays < 366) {
                $scope.isValid.thresholdDaysInvalid = true;
            } else {
                $scope.isValid.thresholdDaysInvalid = false;
                $scope.thresholdDaysMsg = "Invalid input.";
            }
        } else {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Invalid input.";
        }
    }
    $scope.internalNote = function () {
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description !== "";
    }

    $scope.regNumberAvailable = function (e) {
        $scope.isValid.regNumberAvailable = true;
        $scope.regNumberMsg = "";
        $scope.vm.BusinessName = "";
        if ($scope.vm.RegNumber == null || $scope.vm.RegNumber == undefined || $scope.vm.RegNumber == "") {
            $scope.isValid.regNumberAvailable = false;  
            $scope.regNumberMsg = "Required";           
        } else if (isNaN($scope.vm.RegNumber)) {
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "Invalid Input.";           
        } else if ($scope.vm.RegNumber.charAt(0) !="2") {
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "The Registration Number should be at least seven digits starting with 2";
        } else if ($scope.vm.RegNumber.length<7){
            $scope.isValid.regNumberAvailable = false;
            $scope.regNumberMsg = "The Registration Number should be at least seven digits starting with 2";
        } else {
            $scope.isNew = true;
            var temp = $scope.vm.vendors.find(function (item) {
                return item.RegNumber == $scope.vm.RegNumber;
            });
            if (temp) {
                if (temp.IsActive) {
                    $scope.vm.BusinessName = temp.BusinessName;
                    $scope.vm.VendorID = temp.VendorID;     
                    if (temp.ID > 0) {
                        $scope.vm.ID = temp.ID;
                        $scope.vm.ThresholdDays = temp.ThresholdDays;
                        $scope.isNew = false;
                    }
                } else {
                    $scope.isValid.regNumberAvailable = false;
                    $scope.regNumberMsg = "Collector is inactive.";                    
                }
            } else {
                $scope.isValid.regNumberAvailable = false;
                $scope.regNumberMsg = "This Collector Reg# does not exist.";               
            }
        }
    }

    $scope.saveTCR = function () {
        $scope.thresholdDays();
        $scope.regNumberAvailable();
        $scope.internalNote();
        if (!$scope.isValid.checkValid()) {
            return;
        }


        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmTCRServiceThresholdHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm  
            delete $scope.vm.vendors;
            dashboardService.addTCR($scope.vm).then(function (response) {
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventEditFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });
                }
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('editTcrController', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'dashboardService', '$rootScope', 'adata', 'isView', function ($scope, $http, $uibModal, $uibModalInstance, dashboardService, $rootScope, adata, isView) {
    $scope.isValid = {  
        thresholdDaysInvalid: true,
        description: true,
        checkValid: function () {
            return this.thresholdDaysInvalid && this.description;
        }
    };
    $scope.vm = adata;
    $scope.isView = isView;
    $scope.isNew = false;  
    $scope.thresholdDaysMsg = "";
    $scope.thresholdDays = function () {
        $scope.isValid.thresholdDaysInvalid = true;
        if ($scope.vm.ThresholdDays == null || $scope.vm.ThresholdDays === "") {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Required";
        } else if (!isNaN($scope.vm.ThresholdDays)) {
            if ($scope.vm.ThresholdDays > 0 && $scope.vm.ThresholdDays < 366) {
                $scope.isValid.thresholdDaysInvalid = true;
            } else {
                $scope.isValid.thresholdDaysInvalid = false;
                $scope.thresholdDaysMsg = "Invalid input.";
            }
        } else {
            $scope.isValid.thresholdDaysInvalid = false;
            $scope.thresholdDaysMsg = "Invalid input.";
        }
    }
    $scope.internalNote = function () {
        $scope.isValid.description = $scope.vm.Description != null && $scope.vm.Description !== "";
    }
    $scope.saveTCR = function () {
        $scope.thresholdDays();
        $scope.internalNote();
        if (!$scope.isValid.checkValid()) {
            return;
        }

        //close existing modal
        $uibModalInstance.dismiss('cancel');
        //confirm 
        var confirmModal = $uibModal.open({
            templateUrl: 'confirm-modal.html',
            size: 'lg',
            backdrop: 'static',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
                $scope.confirmMessage = Global.DashBoard.ConfirmTCRServiceThresholdHtml;
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            }
        });
        confirmModal.result.then(function (confirm) { //confirm  
            dashboardService.updateTCR($scope.vm).then(function (response) {
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                    $uibModalInstance.close(true);
                }
                else {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'stcEventFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DashBoard.WarningHeader; },
                            failResult: function () {
                                return Global.DashBoard.StcEventEditFailHtml.replace("Event#", response.data.eventNumber);
                            }
                        }
                    });
                }
            });
            $uibModalInstance.dismiss('close');
        }, function (cancel) {
            console.log('non-save');
        });
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('removeTcrController', ['$scope', '$rootScope', '$uibModal', '$uibModalInstance', '$http', 'regNumber', 'businessName', 'id', 'isView', 'dashboardService', function ($scope, $rootScope, $uibModal, $uibModalInstance, $http, regNumber, businessName, id, isView, dashboardService) {
    $scope.vm = {
        id: id,
        regNumber: regNumber,
        businessName: businessName
    };
    $scope.confirmHeader = Global.DashBoard.ConfirmHeader;
    $scope.confirmMessage = Global.DashBoard.ConfirmRemoveTCRHtml.replace("RegNumber", $scope.vm.regNumber).replace("BusinessName", $scope.vm.businessName);

    $scope.confirm = function () {
        dashboardService.removeTCR($scope.vm.id).then(function (response) {
            if (response.data.status) {
                $rootScope.$emit('PANEL_VIEW_UPDATED_TCR', null);
                $uibModalInstance.close(true);
            }
            else {
                $uibModalInstance.dismiss('cancel');
                var failRemoveModal = $uibModal.open({
                    templateUrl: 'fail-modal.html',
                    controller: 'stcEventFailCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        header: function () { return Global.DashBoard.WarningHeader; },
                        failResult: function () {
                            return Global.DashBoard.TcrRemoveFailHtml.replace("RegNumber", $scope.vm.regNumber);
                        }
                    }
                });
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


dashboardApp.controller('showFullAnnouncementCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', '$window', 'subject', 'description',
function ($rootScope, $scope, $http, $uibModalInstance, $window, subject, description) {
    $scope.subject = subject;
    $scope.description = description;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//*****************************************************************
//                 controllers section_End
//*****************************************************************

function triggerSTCDateTimePicker() {
    
    $('#stcEffectiveStartDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm-dd",        
        startDate: new Date(),
        minDate: new Date()
    });

    
    $('#stcEffectiveEndDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm-dd",      
        startDate: new Date(),
        minDate: new Date()
    });
    
}

function eventPostModel(data) {
    var result = {};
    $.each(data, function (name, value) {
        if (name == "OnRoadRate" || name == "OffRoadRate") {                 
            result[name] = value.split('$')[1];
        } else {
            result[name] = value;
        }
    });
    return result;
};

function eventViewModel(data) {
    var result = {};
    var prefex = '$';
    $.each(data, function (name, value) {
        if (name == "OnRoadRate" || name == "OffRoadRate") {
            if (value != null) {
                result[name] = prefex + value.toFixed(3).toString();
            }
            else {
                value = 0;
                result[name] = prefex +value.toFixed(3).toString();
                //result[name] = value;
            }       
        } else {
            result[name] = value;
        }
    });
    return result;   
};

$(function ($) {
    $(window).resize(function () {
        if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
            $("#responsiveDiv1").removeClass("margin-left-neg-15");
            $("#responsiveDiv2").removeClass("margin-right-neg-25");
            $("#startDateViewMode").find("br").remove();
            $("#endDateViewMode").find("br").remove();
        }
        else if (matchMedia('(max-width: 767px)').matches) {
            $("#responsiveDiv1").removeClass("margin-left-neg-15");
            $("#responsiveDiv2").removeClass("margin-right-neg-25");
            $("#startDateViewMode").find("br").remove();
            $("#endDateViewMode").find("br").remove();
        }
        else {
            $("#responsiveDiv1").addClass("margin-left-neg-15");
            $("#responsiveDiv2").addClass("margin-right-neg-25");
            if ($("#startDateViewMode").find("br").length == 0) {
                $("#startDateViewMode label").first().after("<br />");
            }
            if ($("#endDateViewMode").find("br").length == 0) {
                $("#endDateViewMode label").first().after("<br />");
            }           
        }
    });  
})