﻿"use strict";
// constants
var validMsg = {
    clean: "clean",
    isRequired: "Required",
    invalidInput: "Invalid Input",
    uniqueName: "Unique name is required",
    sameSelection: "You cannot select same Processor Group in both Dropdown list",
    noneMsg: ""
};
var deliveryGroupsApp = angular.module("deliveryGroupsApp", ['ui.bootstrap', 'commonLib', 'datatables', 'datatables.scroller'
    , "deliveryGroups.services", "deliveryGroups.directives", "deliveryGroups.controllers", "commonClaimInternalNotesLib"
]);

//OTSTM2-1042 for both supply and recovery
var Security = {
    isReadonly: function (resource) {
        return resource === Global.DeliveryGroups.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.DeliveryGroups.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.DeliveryGroups.Security.Constants.NoAccess
    },
    checkUserSecurity: function (type) {
        switch (type) {
            case "1":
            case 1:
                return Global.DeliveryGroups.Security.Authorize;
                break;
            case "2":
            case 2:
                return Global.DeliveryGroups.Security.Authorize;
                break;
            default:
                return Global.DeliveryGroups.Security.Authorize;
                break;
        }
    }
}

//service section
var services = angular.module("deliveryGroups.services", []);
services.factory('productService', ["$http", function ($http) {
    var factory = {};
    
    factory.AddNewProcessorGroup = function (name, description) {
        var submitVal = {
            url: Global.DeliveryGroups.AddNewProcessorGroupUrl,
            method: "POST",
            data: { Name: name, Description: description }
        }
        return $http(submitVal);
    }

    factory.UpdateProcessorGroup = function (name, description, id) {
        var submitVal = {
            url: Global.DeliveryGroups.UpdateProcessorGroupUrl,
            method: "POST",
            data: { Name: name, Description: description, GroupId: id }
        }
        return $http(submitVal);
    }

    factory.RemoveProcessorGroup = function (groupId) {
        var submitVal = {
            url: Global.DeliveryGroups.RemoveProcessorGroupUrl,
            method: "POST",
            data: { groupId: groupId }
        }
        return $http(submitVal);
    }

    factory.RemoveVendorRateGroupMapping = function (RateGroupId) {
        var submitVal = {
            url: Global.DeliveryGroups.RemoveDeliveryRateGroupUrl,
            method: "POST",
            data: { RateGroupId: RateGroupId }
        }
        return $http(submitVal);
    }

    
    factory.ExcelActivity = function (data) {
        var submitVal = {
            url: Global.DeliveryGroups.ExcelUrl,
            method: "GET",
            params: data
        }
        return $http(submitVal);
    }
    factory.VendorGroupUniqueNameCheck = function (groupName) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.VendorGroupUniqueNameCheckUrl,
            method: "POST",
            data: { name: groupName }
        }
        return $http(submitVal);
    }

    factory.RateGroupUniqueNameCheck = function (groupName) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.RateGroupUniqueNameCheckUrl,
            method: "POST",
            data: { name: groupName }
        }
        return $http(submitVal);
    }



    factory.IsGroupMappingExistingCheck = function (groupId) {
        var submitVal = {
            headers: {
                'Content-Type': 'application/json'
            },
            url: Global.DeliveryGroups.IsGroupMappingExistingCheckUrl,
            method: "POST",
            data: {
                id: groupId
            }
        }
        return $http(submitVal);
    }

    
    factory.getVedorGroups = function (category) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.getVendorGroupsUrl,
            method: "GET",
            data: { category: category }
        }
        return $http(submitVal);
    }
    
    factory.getAssociatedVedorGroups = function (id) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.getAssociatedVendorGroupsUrl,
            method: "POST",
            data: { rateGroupId: id }
        }
        return $http(submitVal);
    }
    
    factory.getVendors = function (selectedValue, rateGroupId) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.getVendorsUrl,
            method: "POST",
            data: { vendorGroupId: selectedValue, rateGroupId: rateGroupId }
        }
        return $http(submitVal);
    }

    factory.getDefaultVendors = function (selectedValue) {
        var submitVal = {
            headers: {
                'Content-Type': 'application/json'
            },
            url: Global.DeliveryGroups.getDefaultVendorsUrl,
            method: "POST",
            data: {
                vendorGroupId: selectedValue
            }
        }
        return $http(submitVal);
    }

    factory.AddNewDeliveryRateGroup = function (name, category, sourceGroup, destinationGroup, vendorsMovedFromSourceToDestination, vendorsMovedFromDestinationToSource, logContent) {
        var submitVal = {
            url: Global.DeliveryGroups.AddNewDeliveryRateGroupUrl,
            method: "POST",
            data: {
                RateGroupName: name, Category: category, SourceGroup: sourceGroup, DestinationGroup: destinationGroup,
                VendorsMovedFromSourceToDestination: vendorsMovedFromSourceToDestination,
                VendorsMovedFromDestinationToSource: vendorsMovedFromDestinationToSource,
                logContent: logContent,
    }
        }
        return $http(submitVal);
    }

    factory.UpdateDeliveryRateGroup = function (name, id, category, sourceGroup, destinationGroup, vendorsMovedFromSourceToDestination, vendorsMovedFromDestinationToSource, logContent) {
        var submitVal = {
            url: Global.DeliveryGroups.UpdateDeliveryRateGroupUrl,
            method: "POST",
            data: {
                RateGroupName: name, Id: id,
                Category: category,
                SourceGroup: sourceGroup,
                DestinationGroup: destinationGroup,
                VendorsMovedFromSourceToDestination: vendorsMovedFromSourceToDestination,
                VendorsMovedFromDestinationToSource: vendorsMovedFromDestinationToSource,
                logContent: logContent,
            }
        }
        return $http(submitVal);
    }

    factory.getVendorRateGroups = function (rateGroupId) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.DeliveryGroups.GetVendorRateGroupsUrl,
            method: "POST",
            data: { id: rateGroupId }
        }
        return $http(submitVal);
    }

    return factory;
}]);

//directives section
var directives = angular.module('deliveryGroups.directives', []);
directives.directive('processorGroupListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'productService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) { },
            templateUrl: 'processor-group-list-panel.html',
            controller: ['$scope', function ($scope) {

                $scope.ShowActionGear = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.EditSave;
                $scope.isView = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.ReadOnly;
                $scope.dtInstance = {};
                $scope.count = 0;

                $rootScope.$on('PANEL_VIEW_UPDATED_VENDOR_GROUP', function (e) {
                    $scope.dtInstance.DataTable.draw();
                });

                $scope.dtColumns = [
                        DTColumnBuilder.newColumn("ID", "ID").withOption('name', 'ID').withClass('hidden'),
                        DTColumnBuilder.newColumn("Name", "Processor Group").withOption('name', 'Name').renderWith(function (data, type, full, meta) {
                            if (full != null) {
                                return '<strong>' + full.Name + '</strong><br/>' + full.Description;
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                        DTColumnBuilder.newColumn("DateModified", "Date Modified").withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withOption('width', '13%').renderWith(actionsHtml)
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.url,
                    type: "GET",
                    error: function (xhr, error, thrown) { }
                }).withOption('processing', true)
                    .withOption('responsive', true).withOption('bAutoWidth', false)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', [1, 'asc'])
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                        //clicking more, make sure alignment
                        if ($scope.viewMore != undefined && $scope.viewMore == false) {
                            $(angular.element(row)["0"].children[3]).addClass('move-right-12px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[5]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[6]).addClass('move-right-25px');
                        }
                    })
                    .withOption('initComplete', function (settings, result) {
                        $scope.viewMore = (result.recordsTotal > 5 && !$scope.scroller);
                        $scope.count = result.recordsTotal;
                        $scope.$apply();
                    })
                    .withOption('drawCallback', function (settings, result) {
                        $scope.viewMore = (settings.json.recordsTotal > 5 && !$scope.scroller);
                        $scope.count = settings.json.recordsTotal;
                    });

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    $scope.dtOptions.withScroller()
                                    .withOption('deferRender', true)
                                    .withOption('scrollY', 310)
                                    .withOption('scrollX', "100%");
                }

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }

                function actionsHtml(data, type, full, meta) {
                    var html = $compile($templateCache.get('item-action.html')
                                .replace(/itemName/g, data.Name)
                                .replace(/itemID/g, data.ID)
                                .replace(/itemDescription/g, data.Description)
                                )($scope);
                    return html[0].outerHTML;
                }

                //click to view
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).addClass('rowClickable');
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function (ev) {
                        $scope.$apply(function () {
                            $scope.openFormForView(aData);
                        });
                    });
                    return nRow;
                }

                $scope.openFormForView = function (aData) {
                    var editModal = $uibModal.open({
                        templateUrl: 'modal-processor-group.html',
                        controller: 'editProcessorGroupController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            ProcessorGroupName: function () {
                                return aData.Name;
                            },
                            OldProcessorGroupName: function () {
                                return aData.Name;
                            },
                            id: function () {
                                return aData.ID;
                            },
                            ProcessorGroupDescription: function () {
                                return aData.Description;
                            },                          
                            isView: function () {
                                return true;
                            }
                        }
                    });
                }
            }]
        }
    }]);

directives.directive('deliveryGroupsListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'productService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {

            },
            templateUrl: 'delivery-rate-groups-list-panel.html',
            controller: ['$scope', function ($scope) {
                $scope.ShowActionGear = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.EditSave;
                $scope.isView = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.ReadOnly;
                $scope.dtInstance = {};

                $rootScope.$on('PANEL_VIEW_UPDATED_RATE_GROUP', function (e, tableId) {
                    $scope.dtInstance.DataTable.draw();
                });

                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "ID").withOption('name', 'ID').withClass('hidden'),
                        DTColumnBuilder.newColumn("Name", "Delivery Rate Group").withOption('name', 'Name').renderWith(function (data, type, full, meta) {
                            if (full != null) {
                                var html = '';
                                html = "<a href='javascript:void(0)' id='associatedGroup" + meta.row + "' data-placement='auto' title='' data-text='" + full.AllAssociatedGroupsText + "'>" + data + "</a>";
                                return html;
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                        DTColumnBuilder.newColumn("DateModified", "Date Modified").withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withOption('width', '13%').renderWith(actionsHtml)
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('ajax',
                    {
                        dataSrc: "data",
                        url: $scope.url,
                        type: "POST"
                    })
                    .withOption('processing', false)
                    .withOption('responsive', true)
                    .withOption('bAutoWidth', false)
                    .withOption('aaSorting', [1, 'asc'])
                    .withOption('serverSide', true)
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                        //clicking more, make sure alignment
                        if ($scope.viewMore != undefined && $scope.viewMore == false) {
                            $(angular.element(row)["0"].children[3]).addClass('move-right-12px');
                            $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[5]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[6]).addClass('move-right-25px');
                        }
                    })
                    .withOption('initComplete', function (settings, result) {
                        $scope.viewMore = (result.recordsTotal > 5 && !$scope.scroller);
                        $scope.count = result.recordsTotal;
                        $scope.$apply();
                    })
                    .withOption('drawCallback', function (settings, result) {
                        $scope.viewMore = (settings.json.recordsTotal > 5 && !$scope.scroller);
                        $scope.count = settings.json.recordsTotal;
                        $("[id^='associatedGroup']").webuiPopover({
                            width: '500',
                            height: '300',
                            padding: true,
                            multi: true,
                            closeable: true,
                            title: 'Processor Group List',
                            type: 'html',
                            trigger: 'hover',
                            content: function () {
                                var internalNote = $(this).attr('data-text');
                                var res = internalNote.split('\n');
                                var result = "";
                                var arrayLength = res.length;
                                for (var i = 0; i < arrayLength; i++) {
                                    result = result + '<p>' + res[i] + '</p>';
                                }
                                return result;
                            },
                            delay: {
                                show: 100, hide: 100
                            },
                        });
                    });

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    $scope.dtOptions.withScroller()
                                   .withOption('deferRender', true)
                                   .withOption('scrollY', 210)
                                    .withOption('scrollX', "100%");
                }

                $scope.delay = (function () {
                    var promise = null;
                    return {
                        calculateDelay: function (callback, ms) {
                            $timeout.cancel(promise);
                            promise = $timeout(callback, ms)
                        }
                    }
                })();

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }

                
                function actionsHtml(data, type, full, meta) {
                    //hide the action gear when rate group is associated with rate
                    if (data.IsAssociatedWithRate) {
                        return '';
                    }
                    var html = $compile($templateCache.get('delivery-rate-group-action.html')
                                .replace(/deliveryRateGroupName/g, data.Name)
                                .replace(/deliveryRateGroupID/g, data.ID)
                                )($scope);
                    return html[0].outerHTML;
                }

                //click to view
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).addClass('rowClickable');
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function () {
                        $scope.$apply(function () {
                            $scope.openFormForView(aData);
                        });
                    });                    
                    return nRow;
                }
                

                $scope.openFormForView = function (aData) {
                    if (aData.IsAssociatedWithRate) {
                        productService.getAssociatedVedorGroups(aData.ID).then(function (result) {
                            var viewModal = $uibModal.open({
                                templateUrl: 'modal-delivery-rate-group.html',
                                controller: 'editDeliveryRateGroupsController',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    DeliveryRateGroupName: function () {
                                        return aData.Name;
                                    },
                                    OldDeliveryRateGroupName: function () {
                                        return aData.Name;
                                    },
                                    id: function () {
                                        return aData.ID;
                                    },
                                    isView: function () {
                                        return true;
                                    },
                                    vendorGroupList: function () {
                                        return result.data;
                                    }
                                }
                            });

                            viewModal.rendered.then(function (e) {
                                //1. Dropdown list data feeding
                                $('#processorGroupList_From').empty();
                                $('#processorGroupList_To').empty();
                                //1.1 from dropdown list
                                var listProcessorGroups_From = $('#processorGroupList_From');
                                var fromDropdown = fillData(listProcessorGroups_From, "NoGroup", result.data);
                                $('#processorGroupList_From').select2({
                                    placeholder: 'Select Processor Group...'
                                });
                                $('#processorGroupList_From').val(fromDropdown).trigger('change');
                                var options_from = {};
                                options_from.ui = {
                                    showPopover: true,
                                    showErrors: true,
                                    showProgressBar: false
                                };
                                options_from.rules = {
                                    activated: {
                                        wordTwoCharacterClasses: true,
                                        wordRepetitions: true
                                    }
                                };
                                options_from.common = {
                                    debug: true,
                                    //  usernameField: "#popoverName"
                                };
                                //1.2 to dropdown list
                                var listProcessorGroups_To = $('#processorGroupList_To');
                                // select second record for dropdownlist "To"
                                var toDropdown = fillData(listProcessorGroups_To, "NoGroup", result.data);
                                $('#processorGroupList_To').select2({
                                    placeholder: 'Select Processor Group...'
                                });
                                $('#processorGroupList_To').val(toDropdown).trigger('change');
                                var options_to = {};
                                options_to.ui = {
                                    showPopover: true,
                                    showErrors: true,
                                    showProgressBar: false
                                };
                                options_to.rules = {
                                    activated: {
                                        wordTwoCharacterClasses: true,
                                        wordRepetitions: true
                                    }
                                };
                                options_to.common = {
                                    debug: true,
                                    //  usernameField: "#popoverName"
                                };

                                // 2. Actions of dropdown list
                                // 2.1 initialize multiselect
                                $('#multiselect').multiselect();
                                var listVendors_From = $('#multiselect');
                                var listVendors_To = $('#multiselect_to');

                                // 2.2 from dropdown list                       
                                $('#processorGroupList_From').on("change", function (e) {
                                    var selectedValue = $(this).find("option:selected").val();
                                    listVendors_From.empty();
                                    productService.getVendors(selectedValue, aData.ID).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_From, selectedValue, result.data);
                                        }
                                    });
                                })
                                // 2.3 to dropdown list                       
                                $('#processorGroupList_To').on("change", function (e) {
                                    var selectedValue = $(this).find("option:selected").val();
                                    listVendors_To.empty();
                                    productService.getVendors(selectedValue, aData.ID).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_To, selectedValue, result.data);
                                        }
                                    });
                                })
                            });
                        });
                    }
                    else {
                        productService.getVedorGroups(Global.DeliveryGroups.DeliveryGroupCategory).then(function (result) {
                            var viewModal = $uibModal.open({
                                templateUrl: 'modal-delivery-rate-group.html',
                                controller: 'editDeliveryRateGroupsController',
                                size: 'lg',
                                backdrop: 'static',
                                resolve: {
                                    DeliveryRateGroupName: function () {
                                        return aData.Name;
                                    },
                                    OldDeliveryRateGroupName: function () {
                                        return aData.Name;
                                    },
                                    id: function () {
                                        return aData.ID;
                                    },
                                    isView: function () {
                                        return true;
                                    },
                                    vendorGroupList: function () {
                                        return result.data;
                                    }
                                }
                            });

                            viewModal.rendered.then(function (e) {
                                //1. Dropdown list data feeding
                                $('#processorGroupList_From').empty();
                                $('#processorGroupList_To').empty();
                                //1.1 from dropdown list
                                var listProcessorGroups_From = $('#processorGroupList_From');
                                var fromDropdown = fillData(listProcessorGroups_From, "NoGroup", result.data);
                                $('#processorGroupList_From').select2({
                                    placeholder: 'Select Processor Group...'
                                });
                                $('#processorGroupList_From').val(fromDropdown).trigger('change');
                                var options_from = {};
                                options_from.ui = {
                                    showPopover: true,
                                    showErrors: true,
                                    showProgressBar: false
                                };
                                options_from.rules = {
                                    activated: {
                                        wordTwoCharacterClasses: true,
                                        wordRepetitions: true
                                    }
                                };
                                options_from.common = {
                                    debug: true,
                                    //  usernameField: "#popoverName"
                                };
                                //1.2 to dropdown list
                                var listProcessorGroups_To = $('#processorGroupList_To');
                                // select second record for dropdownlist "To"
                                var toDropdown = fillData(listProcessorGroups_To, "NoGroup", result.data);
                                $('#processorGroupList_To').select2({
                                    placeholder: 'Select Processor Group...'
                                });
                                $('#processorGroupList_To').val(toDropdown).trigger('change');
                                var options_to = {};
                                options_to.ui = {
                                    showPopover: true,
                                    showErrors: true,
                                    showProgressBar: false
                                };
                                options_to.rules = {
                                    activated: {
                                        wordTwoCharacterClasses: true,
                                        wordRepetitions: true
                                    }
                                };
                                options_to.common = {
                                    debug: true,
                                    //  usernameField: "#popoverName"
                                };

                                // 2. Actions of dropdown list
                                // 2.1 initialize multiselect
                                $('#multiselect').multiselect();
                                var listVendors_From = $('#multiselect');
                                var listVendors_To = $('#multiselect_to');

                                // 2.2 from dropdown list                       
                                $('#processorGroupList_From').on("change", function (e) {
                                    var selectedValue = $(this).find("option:selected").val();
                                    listVendors_From.empty();
                                    productService.getVendors(selectedValue, aData.ID).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_From, selectedValue, result.data);
                                        }
                                    });
                                })
                                // 2.3 to dropdown list                       
                                $('#processorGroupList_To').on("change", function (e) {
                                    var selectedValue = $(this).find("option:selected").val();
                                    listVendors_To.empty();
                                    productService.getVendors(selectedValue, aData.ID).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_To, selectedValue, result.data);
                                        }
                                    });
                                })
                            });
                        });
                    }
                    
                }

            }]
        }
    }]);

directives.directive('deliveryGroupsActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "productService", function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService) {
    return {
        scope: {
            url: '@',
        },
        restrict: 'E',
        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));
                    }
                }
                scope.$digest();
            });
            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '300',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }
            }
        },
        templateUrl: 'delivery-group-activity-panel.html',
        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {
            //Auto reload datatable after new activity generated
            $rootScope.$on('TOTAL_ACTIVITY', function (e, data) {
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            });

            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                $scope.dtInstance.DataTable.draw();
            });
            $rootScope.$on('PANEL_VIEW_UPDATED_VENDOR_GROUP', function (e) {
                $scope.dtInstance.DataTable.draw();
            });
            $rootScope.$on('PANEL_VIEW_UPDATED_RATE_GROUP', function (e, tableId) {
                $scope.dtInstance.DataTable.draw();
            });

            $scope.dtInstance = {};
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width', '15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return date.date + ' ' + date.time;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];
            $scope.sortColumn = ''
            $scope.direction = '';
            $scope.searchValue = '';
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.url,
                global: false,
                type: "POST",
                error: function (xhr, error, thrown) {
                }
            }).withOption('processing', true)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order', [0, 'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    $scope.sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    $scope.direction = settings.aLastSort[0].dir;
                    $scope.searchValue = $('#searchActivity').val();
                    $scope.$apply();
                });

            //$scope.url = $scope.snpUrl; //keep original snpUrl
            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false
            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };
                $scope.searchValue = $('#searchActivity').val();
                if ($scope.searchValue) {
                    $scope.url = $scope.url + "&temp=" + $scope.searchValue;
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 220)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc: "data",
                                    url: $scope.url,
                                    global: false, // this makes sure ajaxStart is not triggered
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    //$scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.url,
                                   global: false,
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            $("#exportActivities").on("click", function (event) {
                event.stopPropagation();
                event.preventDefault();
                var data = {
                    sortcolumn: $scope.sortColumn,
                    sortDirection: $scope.direction,
                    searchText: $scope.searchValue,
                }
                productService.ExcelActivity(data).then(function (result) {
                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = "Delivery-Groups-Activity-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = '<a title="Export to CSV" href="javascript:void(0);" style="display: none;">';
                        var body = $(document.body).append(anchor);
                        $(anchor).attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();
                        $(anchor).remove();
                    }
                });
            });

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '300',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }
        } //controller closing brace
    } // literal object closing brace
}]);

//for show action gear pop-up
directives.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);
//Add Processor Group Button handler
directives.directive('addProcessorGroupButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName: '@',
            count: '='
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.openModal = function () {
                var addModal = $uibModal.open({
                    templateUrl: 'modal-processor-group.html',
                    controller: 'addProcessorGroupController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        panelName: function () {
                            return $scope.panelName;
                        },
                        count: function () {
                            return $scope.count + 1;
                        }
                    }
                });
                addModal.rendered.then(function () {
                    $(".require-input").keyup(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).siblings(".required-validity").html(validMsg.isRequired);
                        } else {
                            $(this).siblings(".required-validity").html(validMsg.noneMsg);
                        }
                    });
                    $(".require-input.require-description").focusout(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).siblings(".required-validity").html(validMsg.isRequired);
                        }
                    });
                });
            };
        }]
    };
}]);

//Add Delivery Rate Group Button handler
directives.directive('addDeliveryRateButton', ['$uibModal', 'productService', function ($uibModal, productService) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName: '@',
            count: '='
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.DeliveryGroups.Security.Authorize === Global.DeliveryGroups.Security.Constants.ReadOnly;
            //scope.vendors = {};           
        },
        controller: ['$scope', function ($scope) {
            productService.getVedorGroups(Global.DeliveryGroups.DeliveryGroupCategory).then(function (result) {
                $scope.openModal = function () {
                    var addModal = $uibModal.open({
                        templateUrl: 'modal-delivery-rate-group.html',
                        controller: 'addDeliveryRateGroupController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            panelName: function () {
                                return $scope.panelName;
                            },
                            count: function () {
                                return $scope.count + 1;
                            },
                            isView: function () {
                                return false;
                            },
                        }
                    });
                    // after modal is rendered       
                    addModal.rendered.then(function () {

                    });
                };
            });
        }]
    };
}]);


//Edit Processor Group Button handler
directives.directive('editProcessorGroupButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            itemName: '@',
            itemId: '@',
            itemDescription: '@',
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                var editModal = $uibModal.open({
                    templateUrl: 'modal-processor-group.html',
                    controller: 'editProcessorGroupController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        ProcessorGroupName: function () {
                            return $scope.itemName;
                        },
                        OldProcessorGroupName: function () {
                            return $scope.itemName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        ProcessorGroupDescription: function () {
                            return $scope.itemDescription;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });

                editModal.rendered.then(function () {
                    $(".require-input").keyup(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).siblings(".required-validity").html(validMsg.isRequired);
                        } else {
                            $(this).siblings(".required-validity").html(validMsg.noneMsg);
                        }
                    });
                });
            }
        }]
    };
}]);

//Delete Processor Group Button handler
directives.directive('removeProcessorGroupButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            itemName: '@',
            itemId: '@',
            itemDescription: '@',
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeProcessorGroupController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        ProcessorGroupName: function () {
                            return $scope.itemName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        ProcessorGroupDescription: function () {
                            return $scope.itemDescription;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    };
}]);

//Edit Delivery Rate Group Button handler
directives.directive('deliveryRateGroupEditButton', ['$uibModal', '$http', 'productService', function ($uibModal, $http, productService) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {
            itemName: '@',
            itemId: '@',
        },
        link: function (scope, el, attrs, roleFormController) {
        },
        controller: ['$scope', function ($scope) {
            productService.getVedorGroups(Global.DeliveryGroups.DeliveryGroupCategory).then(function (result) {
                $scope.edit = function () {
                    var editModal = $uibModal.open({
                        templateUrl: 'modal-delivery-rate-group.html',
                        controller: 'editDeliveryRateGroupsController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            DeliveryRateGroupName: function () {
                                return $scope.itemName;
                            },
                            OldDeliveryRateGroupName: function () {
                                return $scope.itemName;
                            },
                            id: function () {
                                return $scope.itemId;
                            },
                            isView: function () {
                                return false;
                            },
                            vendorGroupList: function () {
                                return result.data;
                            }
                        }
                    });

                    // after modal is rendered       
                    editModal.rendered.then(function () {
                        $(".require-input").keyup(function (e) {
                            if (!$(this).val() || $(this).val() == "") {
                                $(this).siblings(".required-validity").html(validMsg.isRequired);
                            } else {
                                $(this).siblings(".required-validity").html(validMsg.noneMsg);
                            }
                        });

                        //1. Dropdown list data feeding
                        $('#processorGroupList_From').empty();
                        $('#processorGroupList_To').empty();
                        //1.1 from dropdown list
                        var listProcessorGroups_From = $('#processorGroupList_From');
                        var fromDropdown = fillData(listProcessorGroups_From, "NoGroup", result.data);
                        $('#processorGroupList_From').select2({
                            placeholder: 'Select Processor Group...'
                        });
                        $('#processorGroupList_From').val(fromDropdown).trigger('change');
                        var options_from = {};
                        options_from.ui = {
                            showPopover: true,
                            showErrors: true,
                            showProgressBar: false
                        };
                        options_from.rules = {
                            activated: {
                                wordTwoCharacterClasses: true,
                                wordRepetitions: true
                            }
                        };
                        options_from.common = {
                            debug: true,
                            //  usernameField: "#popoverName"
                        };
                        //1.2 to dropdown list
                        var listProcessorGroups_To = $('#processorGroupList_To');
                        // select second record for dropdownlist "To"
                        var toDropdown = fillData(listProcessorGroups_To, "NoGroup", result.data);
                        $('#processorGroupList_To').select2({
                            placeholder: 'Select Processor Group...'
                        });
                        $('#processorGroupList_To').val(toDropdown).trigger('change');
                        var options_to = {};
                        options_to.ui = {
                            showPopover: true,
                            showErrors: true,
                            showProgressBar: false
                        };
                        options_to.rules = {
                            activated: {
                                wordTwoCharacterClasses: true,
                                wordRepetitions: true
                            }
                        };
                        options_to.common = {
                            debug: true,
                            //  usernameField: "#popoverName"
                        };


                        // 2. initialize multiselect
                        $('#multiselect').multiselect({
                            afterMoveToRight: function ($left, $right, $options) {
                                //lock dropdown
                                listProcessorGroups_From.addClass("disabled").prop("disabled", true);
                                listProcessorGroups_To.addClass("disabled").prop("disabled", true);
                                //validation message
                                $('#SaveChangeBeforeSwitching').css("display", "block");
                                $('#SaveChangeBeforeSwitching').addClass("required-validity");
                            },
                            afterMoveToLeft: function ($left, $right, $options) {
                                //lock dropdown
                                listProcessorGroups_From.addClass("disabled").prop("disabled", true);
                                listProcessorGroups_To.addClass("disabled").prop("disabled", true);
                                //validation message
                                $('#SaveChangeBeforeSwitching').css("display", "block");
                                $('#SaveChangeBeforeSwitching').addClass("required-validity");
                            }
                        });
                        var listVendors_From = $('#multiselect');
                        var listVendors_To = $('#multiselect_to');
                        listVendors_From.addClass("disabled").prop("disabled", true);
                        listVendors_To.addClass("disabled").prop("disabled", true);
                        $('#multiselect_rightSelected').addClass('arrow-disabled');
                        $('#multiselect_leftSelected').addClass('arrow-disabled');


                        //3. dropdown list behaviour
                        $('#processorGroupList_From').on("change", function (e) {
                            var selectedValue = $(this).find("option:selected").val();
                            if ($('#processorGroupList_To').find("option:selected").val()) {
                                if ($('#processorGroupList_To').find("option:selected").val() == selectedValue) {
                                    //validation message
                                    $('#SameSelectionOfDropdownList').css("display", "block");
                                    $('#SameSelectionOfDropdownList').addClass("required-validity");
                                    //disable multiselect and move buttons
                                    listVendors_From.addClass("disabled").prop("disabled", true);
                                    listVendors_To.addClass("disabled").prop("disabled", true);
                                    $('#multiselect_rightSelected').addClass('arrow-disabled');
                                    $('#multiselect_leftSelected').addClass('arrow-disabled');
                                    //feed data
                                    listVendors_From.empty();
                                    productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_From, selectedValue, result.data);
                                        }
                                    });
                                }
                                else {
                                    listVendors_From.empty();
                                    productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_From, selectedValue, result.data);
                                        }
                                    });
                                    $('#multiselect_rightSelected').removeClass('arrow-disabled');
                                    $('#multiselect_leftSelected').removeClass('arrow-disabled');
                                    $('#SameSelectionOfDropdownList').css("display", "none");
                                    $('#SameSelectionOfDropdownList').removeClass("required-validity");
                                    listVendors_From.removeClass("disabled").prop("disabled", false);
                                    listVendors_To.removeClass("disabled").prop("disabled", false);
                                }
                            }
                            else {
                                listVendors_From.empty();
                                productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                    if (result.data) {
                                        fillData(listVendors_From, selectedValue, result.data);
                                    }
                                });
                            }

                        })

                        $('#processorGroupList_To').on("change", function (e) {
                            var selectedValue = $(this).find("option:selected").val();
                            if ($('#processorGroupList_From').find("option:selected").val()) {
                                if ($('#processorGroupList_From').find("option:selected").val() == selectedValue) {
                                    //validation message
                                    $('#SameSelectionOfDropdownList').css("display", "block");
                                    $('#SameSelectionOfDropdownList').addClass("required-validity");
                                    //disable multiselect and move buttons
                                    listVendors_From.addClass("disabled").prop("disabled", true);
                                    listVendors_To.addClass("disabled").prop("disabled", true);
                                    $('#multiselect_rightSelected').addClass('arrow-disabled');
                                    $('#multiselect_leftSelected').addClass('arrow-disabled');
                                    //feed data
                                    listVendors_To.empty();
                                    productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_To, selectedValue, result.data);
                                        }
                                    });
                                }
                                else {
                                    listVendors_To.empty();
                                    productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                        if (result.data) {
                                            fillData(listVendors_To, selectedValue, result.data);
                                        }
                                    });
                                    $('#multiselect_rightSelected').removeClass('arrow-disabled');
                                    $('#multiselect_leftSelected').removeClass('arrow-disabled');
                                    $('#SameSelectionOfDropdownList').css("display", "none");
                                    $('#SameSelectionOfDropdownList').removeClass("required-validity");
                                    listVendors_From.removeClass("disabled").prop("disabled", false);
                                    listVendors_To.removeClass("disabled").prop("disabled", false);
                                }
                            }
                            else {
                                listVendors_To.empty();
                                productService.getVendors(selectedValue, $scope.itemId).then(function (result) {
                                    if (result.data) {
                                        fillData(listVendors_To, selectedValue, result.data);
                                    }
                                });
                            }

                        })
                    });
                }
            });

        }]
    };
}]);

//Delete Delivery Rate Group Button handler
directives.directive('deliveryRateGroupRemoveButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'remove-btn.html',
        scope: {
            itemName: '@',
            itemId: '@',
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.remove = function () {
                $scope.result = {};

                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    controller: 'removeDeliveryRateGroupController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        DeliveryRateGroupName: function () {
                            return $scope.itemName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
            }
        }]
    };
}]);

directives.directive('validNumber', function () {
    return {
        require: '?ngModel',
        scope: {
            max: '@',
            min: '@',
            maxlength: '@',
            theprefex: '@',
            decimalSize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {

            ngModelCtrl.$parsers.push(function (val) {

                val = '' + val;
                var clean = val.replace(/[^0-9]/g, '');
                var decimalCheck = clean.split('.');
                var scale = scope.decimalSize ? parseInt(scope.decimalSize) : 0;
                decimalCheck[0] = decimalCheck[0].slice(0, (scope.maxlength - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }
                if (parseInt(clean) > parseInt(scope.max)) {
                    clean = "0";
                }
                if (val !== clean && clean != "" || val === "0") {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }

                return clean;
            });

            element.bind('keydown', function (e) {
                if (!((e.key >= 0 && e.key <= 9) || [8, 37, 39, 46].indexOf(e.keyCode) > -1)) {
                    e.preventDefault();
                }
            }).bind('blur', function (event) {
                event.preventDefault();
                var temp = element.val() == "" ? "0" : element.val();
                temp = +temp;
                ngModelCtrl.$setViewValue("" + temp);
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});


//controller section
var controllers = angular.module('deliveryGroups.controllers', []);
controllers.controller("deliveryGroupController", ['$rootScope', '$scope', '$http', '$uibModal', "productService", function ($rootScope, $scope, $http, $uibModal, productService) { }]);

controllers.controller('addProcessorGroupController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http', 'panelName', 'productService', 'count', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, panelName, productService, count) {
    $scope.vm = {
        title: panelName,
        buttonTitle: "Save",
        ID: 0,
        ProcessorGroupName: "PG" + count,
        ProcessorGroupDescription: "",
        isView: false
    }
    $scope.submitContent = function () {
        //valid check   
        var isValid = validCheck($scope.vm);
        if (!isValid) {
            return;
        };
        var uniqueName = productService.VendorGroupUniqueNameCheck($scope.vm.ProcessorGroupName);
        //$scope.vm.Color = $("#result").css("color");
        uniqueName.success(function (response) {
            if (response && response.result) {
                $("input.require-input.require-name").siblings(".required-validity").html(validMsg.uniqueName);
                return;
            } else {
                //close existing modal
                $uibModalInstance.dismiss('cancel');
                //confirm 
                var confirmModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    size: 'lg',
                    backdrop: 'static',
                    controller: function ($scope, $uibModalInstance, $uibModal) {
                        $scope.confirmHeader = Global.DeliveryGroups.ConfirmHeader;
                        $scope.confirmMessage = Global.DeliveryGroups.ConfirmAddVendorGroupHtml
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        }
                        $scope.confirm = function () {
                            $uibModalInstance.close();
                        };
                    }
                });

                confirmModal.result.then(function (confirm) { //confirm            
                    productService.AddNewProcessorGroup($scope.vm.ProcessorGroupName, $scope.vm.ProcessorGroupDescription).then(function (response) {
                        $rootScope.$emit('PANEL_VIEW_UPDATED_VENDOR_GROUP', null);
                        $uibModalInstance.close(true);
                    });
                    $uibModalInstance.dismiss('close');
                }, function (cancel) {
                    console.log('non-save');
                });
            }
        });
        uniqueName.error(function (data) {
            $uibModalInstance.close({ closeParentRoleModal: false });
            return;
        })
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('addDeliveryRateGroupController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http', 'panelName', 'productService', 'count', 'isView', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, panelName, productService, count, isView) {
    productService.getVedorGroups(Global.DeliveryGroups.DeliveryGroupCategory).then(function (result) {

        $scope.vendorGroupList = result.data;
        $scope.vm = {
            title: panelName,
            buttonTitle: "Save",
            ID: 0,
            DeliveryRateGroupName: "DRG" + count,
            isView: isView
        }


        //load all the mapping of VendorGroups and Vendors
        $scope.oldVendors = [];
        if ($scope.vendorGroupList) {
            for (var i = 0; i < $scope.vendorGroupList.length; i++) {
                productService.getDefaultVendors($scope.vendorGroupList[i].ItemID).then(function (result) {
                    if (result.data) {
                        $scope.oldVendors.push(result.data);
                    }
                });
            }
        }

        $scope.oldSourceVendors = [];
        $scope.oldDestinationVendors = [];
        $scope.newSourceVendors = [];
        $scope.newDestinationVendors = [];

        initialProcessorGroupDropDownList($scope, productService, result);

        $scope.submitContent = function () {

            //get latest selection
            var listProcessorGroups_From = $('#processorGroupList_From');
            var listProcessorGroups_To = $('#processorGroupList_To');
            var fromVendorGroupId = listProcessorGroups_From.find("option:selected").val();
            var toVendorGroupId = listProcessorGroups_To.find("option:selected").val();
            var fromVendorGroupName = listProcessorGroups_From.find("option:selected").text();
            var toVendorGroupName = listProcessorGroups_To.find("option:selected").text();
            var fromVendorList = [];
            var toVendorList = [];

            //get old Vendors based on latest Vendor Group selection
            for (var i = 0; i < $scope.oldVendors.length; i++) {
                if ($scope.oldVendors[i].find(x => x.ItemValue == fromVendorGroupId)) {
                    $scope.oldSourceVendors = $scope.oldVendors[i];
                }
                if ($scope.oldVendors[i].find(x => x.ItemValue == toVendorGroupId)) {
                    $scope.oldDestinationVendors = $scope.oldVendors[i];
                }
            }

            //get new vendors
            var sourceVendorsOptions = $("#multiselect option");
            var destinationVendorsOptions = $("#multiselect_to option");
            for (var i = 0; i < sourceVendorsOptions.length; i++) {
                var VendorItem = { ItemID: parseInt(sourceVendorsOptions[i].value), ItemName: sourceVendorsOptions[i].text, ItemValue: parseInt(fromVendorGroupId) }
                $scope.newSourceVendors.push(VendorItem);
                fromVendorList.push(parseInt(sourceVendorsOptions[i].value));
            }
            for (var i = 0; i < destinationVendorsOptions.length; i++) {
                var VendorItem = { ItemID: parseInt(destinationVendorsOptions[i].value), ItemName: destinationVendorsOptions[i].text, ItemValue: parseInt(toVendorGroupId) }
                $scope.newDestinationVendors.push(VendorItem);
                toVendorList.push(parseInt(destinationVendorsOptions[i].value));
            }

            //create sourceGroup and destinationGroup for View Model to backend
            var sourceGroup = { GroupId: fromVendorGroupId, GroupName: fromVendorGroupName, VendorIds: fromVendorList };
            var destinationGroup = { GroupId: toVendorGroupId, GroupName: toVendorGroupName, VendorIds: toVendorList };

            //find vendors moved
            var VendorsMovedFromSourceToDestination = _.differenceWith($scope.oldSourceVendors, $scope.newSourceVendors, _.isEqual);
            var VendorsMovedFromDestinationToSource = _.differenceWith($scope.newSourceVendors, $scope.oldSourceVendors, _.isEqual);


            //valid check   
            var isValid = validCheckDeliveryRateGroup($scope.vm);
            if (!isValid) {
                return;
            };
            var uniqueName = productService.RateGroupUniqueNameCheck($scope.vm.DeliveryRateGroupName);
            //$scope.vm.Color = $("#result").css("color");
            uniqueName.success(function (response) {
                if (response && response.result) {
                    $("input.require-input").siblings(".required-validity").html(validMsg.uniqueName);
                    return;
                } else {
                    //close existing modal
                    $uibModalInstance.dismiss('cancel');
                    //confirm 
                    var confirmModal = $uibModal.open({
                        templateUrl: 'confirm-modal.html',
                        size: 'lg',
                        backdrop: 'static',
                        controller: 'RateGroupConfirmCtrl',
                        resolve: {
                            header: function () { return Global.DeliveryGroups.ConfirmHeader; },
                            confirmResult: function () {
                                var messageWithoutMoving = "<h4>Are you sure you want to add this Delivery Rate Group?</h4>";
                                var messageWithMoving = "<h4>Are you sure you want to Move ";
                                if (VendorsMovedFromSourceToDestination.length <= 0 && VendorsMovedFromDestinationToSource.length <= 0) {
                                    return messageWithoutMoving;
                                }
                                else {
                                    if (VendorsMovedFromSourceToDestination.length > 0) {
                                        for (var i = 0; i < VendorsMovedFromSourceToDestination.length; i++) {
                                            var ItemArray = VendorsMovedFromSourceToDestination[i].ItemName.split(" ");
                                            var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                            messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", fromVendorGroupName).replace("Destination Processor Group", toVendorGroupName);
                                        }
                                    }
                                    messageWithMoving += '<br/>';//one more break line
                                    if (VendorsMovedFromDestinationToSource.length > 0) {
                                        for (var i = 0; i < VendorsMovedFromDestinationToSource.length; i++) {
                                            var ItemArray = VendorsMovedFromDestinationToSource[i].ItemName.split(" ");
                                            var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                            messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", toVendorGroupName).replace("Destination Processor Group", fromVendorGroupName);
                                        }
                                    }
                                    messageWithMoving += "?</h4>";
                                    messageWithMoving = messageWithMoving.replace(", ?", " ?").replace(", <br/>?", " ?");
                                    return messageWithMoving;
                                }

                            }
                        }

                    });

                    confirmModal.result.then(function (confirm) { //confirm            
                        productService.AddNewDeliveryRateGroup($scope.vm.DeliveryRateGroupName, "DeliveryGroup", sourceGroup, destinationGroup).then(function (response) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED_RATE_GROUP', null);
                            $uibModalInstance.close(true);
                        });
                        $uibModalInstance.dismiss('close');
                    }, function (cancel) {
                        console.log('non-save');
                    });
                }
            });
            uniqueName.error(function (data) {
                $uibModalInstance.close({ closeParentRoleModal: false });
                return;
            })
        }
        // close add modal 
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });
}]);

controllers.controller('editProcessorGroupController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'ProcessorGroupName', 'OldProcessorGroupName', 'id', 'ProcessorGroupDescription', 'isView', 'productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, ProcessorGroupName, OldProcessorGroupName, id, ProcessorGroupDescription, isView, productService) {
        $scope.vm = {
            title: Global.DeliveryGroups.ProcessorGroupPanelHeader,
            buttonTitle: "Save",
            ID: id,
            ProcessorGroupName: ProcessorGroupName,
            OldProcessorGroupName: OldProcessorGroupName,
            ProcessorGroupDescription: ProcessorGroupDescription,
            isView: isView
        }

        $scope.submitContent = function () {
            //valid check       

            var isValid = validCheck($scope.vm);
            if (!isValid) {
                return;
            };


            if ($scope.vm.OldProcessorGroupName.trim().toLowerCase() != $scope.vm.ProcessorGroupName.trim().toLowerCase()) {
                var uniqueName = productService.VendorGroupUniqueNameCheck($scope.vm.ProcessorGroupName);
                uniqueName.success(function (response) {
                    if (response && response.result) {
                        $("input.require-input.require-name").siblings(".required-validity").html(validMsg.uniqueName);
                        return;
                    } else {
                        //close existing modal
                        $uibModalInstance.dismiss('cancel');
                        //confirm 
                        var confirmModal = $uibModal.open({
                            templateUrl: 'confirm-modal.html',
                            size: 'lg',
                            backdrop: 'static',
                            controller: function ($scope, $uibModalInstance, $uibModal) {
                                $scope.confirmHeader = Global.DeliveryGroups.ConfirmHeader;
                                $scope.confirmMessage = Global.DeliveryGroups.ConfirmEditVendorGroupHtml;
                                $scope.cancel = function () {
                                    $uibModalInstance.dismiss('cancel');
                                }
                                $scope.confirm = function () {
                                    $uibModalInstance.close();
                                };
                            }
                        });

                        confirmModal.result.then(function (confirm) { //confirm            
                            productService.UpdateProcessorGroup($scope.vm.ProcessorGroupName, $scope.vm.ProcessorGroupDescription, $scope.vm.ID).then(function (response) {
                                $rootScope.$emit('PANEL_VIEW_UPDATED_VENDOR_GROUP', null);
                                $uibModalInstance.close(true);
                            });
                            $uibModalInstance.dismiss('close');
                        }, function (cancel) {
                            console.log('non-save');
                        });
                    }
                });
            } else {
                //close existing modal
                $uibModalInstance.dismiss('cancel');
                //confirm 
                var confirmModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    size: 'lg',
                    backdrop: 'static',
                    controller: function ($scope, $uibModalInstance, $uibModal) {
                        $scope.confirmHeader = Global.DeliveryGroups.ConfirmHeader;
                        $scope.confirmMessage = Global.DeliveryGroups.ConfirmEditVendorGroupHtml;
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        }
                        $scope.confirm = function () {
                            $uibModalInstance.close();
                        };
                    }
                });
                confirmModal.result.then(function (confirm) { //confirm            
                    productService.UpdateProcessorGroup($scope.vm.ProcessorGroupName, $scope.vm.ProcessorGroupDescription, $scope.vm.ID).then(function (response) {
                        $rootScope.$emit('PANEL_VIEW_UPDATED_VENDOR_GROUP', null);
                        $uibModalInstance.close(true);
                    });
                    $uibModalInstance.dismiss('close');
                }, function (cancel) {
                    console.log('non-save');
                });
            }
        }
        // close add modal 
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

controllers.controller('removeProcessorGroupController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'ProcessorGroupName', 'id', 'ProcessorGroupDescription', 'isView', 'productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, ProcessorGroupName, id, ProcessorGroupDescription, isView, productService) {
        $scope.vm = {
            title: Global.DeliveryGroups.ProcessorGroupPanelHeader,
            buttonTitle: "OK",
            ID: id,
            ProcessorGroupName: ProcessorGroupName,
            ProcessorGroupDescription: ProcessorGroupDescription,
            isView: isView
        }
        $scope.confirmHeader = Global.DeliveryGroups.ConfirmHeader;
        $scope.confirmMessage = Global.DeliveryGroups.ConfirmRemoveVendorGroupHtml.replace("Name", $scope.vm.ProcessorGroupName).replace("Description", $scope.vm.ProcessorGroupDescription);

        $scope.confirm = function () {
            productService.IsGroupMappingExistingCheck($scope.vm.ID).then(function (response) {
                if (response.data.result) {
                    $uibModalInstance.dismiss('cancel');
                    var failRemoveModal = $uibModal.open({
                        templateUrl: 'fail-modal.html',
                        controller: 'vendorGroupFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.DeliveryGroups.WarningHeader; },
                            failResult: function () {
                                return Global.DeliveryGroups.RemoveVendorGroupFailHtml.replace("Name", $scope.vm.ProcessorGroupName).replace("Description", $scope.vm.ProcessorGroupDescription);
                            }
                        }
                    });
                }
                else {
                    productService.RemoveProcessorGroup($scope.vm.ID).then(function (response) {
                        if (response.data.result) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED_VENDOR_GROUP', null);
                            $uibModalInstance.close(true);
                        }
                    });
                }
            });

        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

controllers.controller('editDeliveryRateGroupsController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'DeliveryRateGroupName', 'OldDeliveryRateGroupName', 'id', 'isView', 'vendorGroupList', 'productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http,
    DeliveryRateGroupName, OldDeliveryRateGroupName, id, isView, vendorGroupList, productService) {

        $scope.vm = {
            title: Global.DeliveryGroups.DeliveryRateGroupPanelHeader,
            buttonTitle: "Save",
            ID: id,
            DeliveryRateGroupName: DeliveryRateGroupName,
            OldDeliveryRateGroupName: OldDeliveryRateGroupName,
            isView: isView
        }

        //load all the mapping of VendorGroups and Vendors
        $scope.oldVendors = [];
        $scope.vendorGroupList = vendorGroupList;
        for (var i = 0; i < $scope.vendorGroupList.length; i++) {
            productService.getVendors($scope.vendorGroupList[i].ItemID, $scope.vm.ID).then(function (result) {
                if (result.data) {
                    $scope.oldVendors.push(result.data);
                }
            });
        }

        $scope.oldSourceVendors = [];
        $scope.oldDestinationVendors = [];
        $scope.newSourceVendors = [];
        $scope.newDestinationVendors = [];

        $scope.submitContent = function () {

            //get latest selection
            var listProcessorGroups_From = $('#processorGroupList_From');
            var listProcessorGroups_To = $('#processorGroupList_To');
            var fromVendorGroupId = listProcessorGroups_From.find("option:selected").val();
            var toVendorGroupId = listProcessorGroups_To.find("option:selected").val();
            var fromVendorGroupName = listProcessorGroups_From.find("option:selected").text();
            var toVendorGroupName = listProcessorGroups_To.find("option:selected").text();
            var fromVendorList = [];
            var toVendorList = [];

            //get old Vendors based on latest Vendor Group selection
            for (var i = 0; i < $scope.oldVendors.length; i++) {
                if ($scope.oldVendors[i].find(x => x.ItemValue == fromVendorGroupId)) {
                    $scope.oldSourceVendors = $scope.oldVendors[i];
                }
                if ($scope.oldVendors[i].find(x => x.ItemValue == toVendorGroupId)) {
                    $scope.oldDestinationVendors = $scope.oldVendors[i];
                }
            }

            //get new vendors
            var sourceVendorsOptions = $("#multiselect option");
            var destinationVendorsOptions = $("#multiselect_to option");
            for (var i = 0; i < sourceVendorsOptions.length; i++) {
                var VendorItem = { ItemID: parseInt(sourceVendorsOptions[i].value), ItemName: sourceVendorsOptions[i].text, ItemValue: parseInt(fromVendorGroupId) }
                $scope.newSourceVendors.push(VendorItem);
                fromVendorList.push(parseInt(sourceVendorsOptions[i].value));
            }
            for (var i = 0; i < destinationVendorsOptions.length; i++) {
                var VendorItem = { ItemID: parseInt(destinationVendorsOptions[i].value), ItemName: destinationVendorsOptions[i].text, ItemValue: parseInt(toVendorGroupId) }
                $scope.newDestinationVendors.push(VendorItem);
                toVendorList.push(parseInt(destinationVendorsOptions[i].value));
            }

            //create sourceGroup and destinationGroup for View Model to backend
            var sourceGroup = { GroupId: fromVendorGroupId, GroupName: fromVendorGroupName, VendorIds: fromVendorList };
            var destinationGroup = { GroupId: toVendorGroupId, GroupName: toVendorGroupName, VendorIds: toVendorList };

            //find vendors moved
            var VendorsMovedFromSourceToDestination = _.differenceWith($scope.oldSourceVendors, $scope.newSourceVendors, _.isEqual);
            var VendorsMovedFromDestinationToSource = _.differenceWith($scope.newSourceVendors, $scope.oldSourceVendors, _.isEqual);

            //valid check       
            var isValid = validCheckDeliveryRateGroup($scope.vm);
            if (!isValid) {
                return;
            };

            if ($scope.vm.OldDeliveryRateGroupName.trim().toLowerCase() != $scope.vm.DeliveryRateGroupName.trim().toLowerCase()) {
                var uniqueName = productService.RateGroupUniqueNameCheck($scope.vm.DeliveryRateGroupName);
                uniqueName.success(function (response) {
                    if (response && response.result) {
                        $("input.require-input").siblings(".required-validity").html(validMsg.uniqueName);
                        return;
                    } else {
                        //close existing modal
                        $uibModalInstance.dismiss('cancel');
                        //confirm 
                        var confirmModal = $uibModal.open({
                            templateUrl: 'confirm-modal.html',
                            size: 'lg',
                            backdrop: 'static',
                            controller: 'RateGroupConfirmCtrl',
                            resolve: {
                                header: function () { return Global.DeliveryGroups.ConfirmHeader; },
                                confirmResult: function () {
                                    var messageWithoutMoving = "<h4>Are you sure you want to save the changes?</h4>";
                                    var messageWithMoving = "<h4>Are you sure you want to Move ";
                                    if (VendorsMovedFromSourceToDestination.length <= 0 && VendorsMovedFromDestinationToSource.length <= 0) {
                                        return messageWithoutMoving;
                                    }
                                    else {
                                        if (VendorsMovedFromSourceToDestination.length > 0) {
                                            for (var i = 0; i < VendorsMovedFromSourceToDestination.length; i++) {
                                                var ItemArray = VendorsMovedFromSourceToDestination[i].ItemName.split(" ");
                                                var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                                messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", fromVendorGroupName).replace("Destination Processor Group", toVendorGroupName);
                                            }
                                        }
                                        messageWithMoving += '<br/>';//one more break line
                                        if (VendorsMovedFromDestinationToSource.length > 0) {
                                            for (var i = 0; i < VendorsMovedFromDestinationToSource.length; i++) {
                                                var ItemArray = VendorsMovedFromDestinationToSource[i].ItemName.split(" ");
                                                var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                                messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", toVendorGroupName).replace("Destination Processor Group", fromVendorGroupName);
                                            }
                                        }
                                        messageWithMoving += "?</h4>";
                                        messageWithMoving = messageWithMoving.replace(", ?", " ?").replace(", <br/>?", " ?");
                                        $scope.logContent = messageWithMoving;
                                        return messageWithMoving;
                                    }

                                }
                            }

                        });

                        confirmModal.result.then(function (confirm) { //confirm            
                            productService.UpdateDeliveryRateGroup($scope.vm.DeliveryRateGroupName, $scope.vm.ID, "DeliveryGroup", sourceGroup, destinationGroup, VendorsMovedFromSourceToDestination, VendorsMovedFromDestinationToSource, $scope.logContent).then(function (response) {
                                $rootScope.$emit('PANEL_VIEW_UPDATED_RATE_GROUP', null);
                                $uibModalInstance.close(true);
                            });
                            $uibModalInstance.dismiss('close');
                        }, function (cancel) {
                            console.log('non-save');
                        });
                    }
                });
            } else {
                //close existing modal
                $uibModalInstance.dismiss('cancel');
                //confirm 
                var confirmModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',
                    size: 'lg',
                    backdrop: 'static',
                    controller: 'RateGroupConfirmCtrl',
                    resolve: {
                        header: function () { return Global.DeliveryGroups.ConfirmHeader; },
                        confirmResult: function () {
                            var messageWithoutMoving = "<h4>Are you sure you want to save the changes?</h4>";
                            var messageWithMoving = "<h4>Are you sure you want to Move ";
                            if (VendorsMovedFromSourceToDestination.length <= 0 && VendorsMovedFromDestinationToSource.length <= 0) {
                                return messageWithoutMoving;
                            }
                            else {
                                if (VendorsMovedFromSourceToDestination.length > 0) {
                                    for (var i = 0; i < VendorsMovedFromSourceToDestination.length; i++) {
                                        var ItemArray = VendorsMovedFromSourceToDestination[i].ItemName.split(" ");
                                        var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                        messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", fromVendorGroupName).replace("Destination Processor Group", toVendorGroupName);
                                    }
                                }
                                messageWithMoving += '<br/>';//one more break line
                                if (VendorsMovedFromDestinationToSource.length > 0) {
                                    for (var i = 0; i < VendorsMovedFromDestinationToSource.length; i++) {
                                        var ItemArray = VendorsMovedFromDestinationToSource[i].ItemName.split(" ");
                                        var legalName = ItemArray.slice(1, ItemArray.length).join(" ");
                                        messageWithMoving += '<br/>' + Global.DeliveryGroups.ConfirmAddRateGroupHtml.replace("Reg#", ItemArray[0]).replace("Legal name", legalName).replace("Source Processor Group", toVendorGroupName).replace("Destination Processor Group", fromVendorGroupName);
                                    }
                                }
                                messageWithMoving += "?</h4>";
                                messageWithMoving = messageWithMoving.replace(", ?", " ?").replace(", <br/>?", " ?");
                                $scope.logContent = messageWithMoving;
                                return messageWithMoving;
                            }

                        }
                    }

                });

                confirmModal.result.then(function (confirm) { //confirm            
                    productService.UpdateDeliveryRateGroup($scope.vm.DeliveryRateGroupName, $scope.vm.ID, "DeliveryGroup", sourceGroup, destinationGroup, VendorsMovedFromSourceToDestination, VendorsMovedFromDestinationToSource, $scope.logContent).then(function (response) {
                        $rootScope.$emit('PANEL_VIEW_UPDATED_RATE_GROUP', null);
                        $uibModalInstance.close(true);
                    });
                    $uibModalInstance.dismiss('close');
                }, function (cancel) {
                    console.log('non-save');
                });
            }
        }
        // close add modal 
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

controllers.controller('removeDeliveryRateGroupController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'DeliveryRateGroupName', 'id', 'isView', 'productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, DeliveryRateGroupName, id, isView, productService) {
        $scope.vm = {
            title: Global.DeliveryGroups.PanelHeader,
            buttonTitle: "OK",
            ID: id,
            RateGroupId: id,
            DeliveryRateGroupName: DeliveryRateGroupName,
            isView: isView
        }
        $scope.confirmHeader = Global.DeliveryGroups.ConfirmHeader;
        $scope.confirmMessage = Global.DeliveryGroups.ConfirmRemoveRateGroupHtml.replace("Name", $scope.vm.DeliveryRateGroupName);

        $scope.confirm = function () {
            productService.RemoveVendorRateGroupMapping($scope.vm.ID).then(function (response) {
                if (response.data.result) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED_RATE_GROUP', null);
                    $uibModalInstance.close(true);
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

controllers.controller('vendorGroupFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', function ($scope, $uibModal, $uibModalInstance, failResult, header) {

    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('RateGroupConfirmCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'confirmResult', 'header', function ($scope, $uibModal, $uibModalInstance, confirmResult, header) {

    $scope.confirmMessage = confirmResult;
    $scope.confirmHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

function validCheck(vm) {
    var isValid = true;
    if (!vm.ProcessorGroupName || vm.ProcessorGroupName == "") {
        $("input.require-input.require-name").siblings(".required-validity").html(validMsg.isRequired);
        isValid = false;
    }
    if (!vm.ProcessorGroupDescription || vm.ProcessorGroupDescription == "") {
        $("input.require-input.require-description").siblings(".required-validity").html(validMsg.isRequired);
        isValid = false;
    }
    return isValid;
}

function validCheckDeliveryRateGroup(vm) {
    var isValid = true;
    if (!vm.DeliveryRateGroupName || vm.DeliveryRateGroupName == "") {
        $("input.require-input.require-name").siblings(".required-validity").html(validMsg.isRequired);
        isValid = false;
    }
    return isValid;
}


function dateTimeConvert(data) {
    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
    //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

    return {
        date: d,
        time: curr_time
    }
}

function fillData(listContents, vendorGroup, data) {
    if (data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].ItemValue == vendorGroup || vendorGroup == "NoGroup") {
                if (data[i].ItemID && data[i].ItemName) {
                    listContents.append(
                        $('<option/>', {
                            value: data[i].ItemID,
                            html: data[i].ItemName,
                        })
                    );
                }
            }
        }
    }
}

function initialProcessorGroupDropDownList($scope, productService, result) {
    if (!result || !result.data) {
        return;
    }
    $(".require-input").keyup(function (e) {
        if (!$(this).val() || $(this).val() == "") {
            $(this).siblings(".required-validity").html(validMsg.isRequired);
        } else {
            $(this).siblings(".required-validity").html(validMsg.noneMsg);
        }
    });

    //1. Dropdown list data feeding
    $('#processorGroupList_From').empty();
    $('#processorGroupList_To').empty();
    //1.1 from dropdown list
    var listProcessorGroups_From = $('#processorGroupList_From');
    var fromDropdown = fillData(listProcessorGroups_From, "NoGroup", result.data);
    $('#processorGroupList_From').select2({
        placeholder: 'Select Processor Group...'
    });
    $('#processorGroupList_From').val(fromDropdown).trigger('change');
    var options_from = {};
    options_from.ui = {
        showPopover: true,
        showErrors: true,
        showProgressBar: false
    };
    options_from.rules = {
        activated: {
            wordTwoCharacterClasses: true,
            wordRepetitions: true
        }
    };
    options_from.common = {
        debug: true,
        //  usernameField: "#popoverName"
    };

    //1.2 to dropdown list
    var listProcessorGroups_To = $('#processorGroupList_To');
    // select second record for dropdownlist "To"
    var toDropdown = fillData(listProcessorGroups_To, "NoGroup", result.data);
    $('#processorGroupList_To').select2({
        placeholder: 'Select Processor Group...'
    });
    $('#processorGroupList_To').val(toDropdown).trigger('change');
    var options_to = {};
    options_to.ui = {
        showPopover: true,
        showErrors: true,
        showProgressBar: false
    };
    options_to.rules = {
        activated: {
            wordTwoCharacterClasses: true,
            wordRepetitions: true
        }
    };
    options_to.common = {
        debug: true,
        //  usernameField: "#popoverName"
    };

    // 2. Actions of dropdown list
    // 2.1 initialize multiselect
    $('#multiselect').multiselect({
        afterMoveToRight: function ($left, $right, $options) {
            //lock dropdown
            listProcessorGroups_From.addClass("disabled").prop("disabled", true);
            listProcessorGroups_To.addClass("disabled").prop("disabled", true);
            //validation message
            $('#SaveChangeBeforeSwitching').css("display", "block");
            $('#SaveChangeBeforeSwitching').addClass("required-validity");
        },
        afterMoveToLeft: function ($left, $right, $options) {
            //lock dropdown
            listProcessorGroups_From.addClass("disabled").prop("disabled", true);
            listProcessorGroups_To.addClass("disabled").prop("disabled", true);
            //validation message
            $('#SaveChangeBeforeSwitching').css("display", "block");
            $('#SaveChangeBeforeSwitching').addClass("required-validity");
        }
    });
    var listVendors_From = $('#multiselect');
    var listVendors_To = $('#multiselect_to');
    listVendors_From.addClass("disabled").prop("disabled", true);
    listVendors_To.addClass("disabled").prop("disabled", true);
    $('#multiselect_rightSelected').addClass('arrow-disabled');
    $('#multiselect_leftSelected').addClass('arrow-disabled');

    // 2.2 from dropdown list

    $('#processorGroupList_From').on("change", function (e) {
        var selectedValue = $(this).find("option:selected").val();
        if ($('#processorGroupList_To').find("option:selected").val()) {
            if ($('#processorGroupList_To').find("option:selected").val() == selectedValue) {
                //validation message
                $('#SameSelectionOfDropdownList').css("display", "block");
                $('#SameSelectionOfDropdownList').addClass("required-validity");
                //disable multiselect and move buttons
                listVendors_From.addClass("disabled").prop("disabled", true);
                listVendors_To.addClass("disabled").prop("disabled", true);
                $('#multiselect_rightSelected').addClass('arrow-disabled');
                $('#multiselect_leftSelected').addClass('arrow-disabled');
                //feed data
                listVendors_From.empty();
                productService.getDefaultVendors(selectedValue).then(function (result) {
                    if (result.data) {
                        fillData(listVendors_From, selectedValue, result.data);
                    }
                });
            }
            else {
                listVendors_From.empty();
                productService.getDefaultVendors(selectedValue).then(function (result) {
                    if (result.data) {
                        fillData(listVendors_From, selectedValue, result.data);
                    }
                });
                $('#multiselect_rightSelected').removeClass('arrow-disabled');
                $('#multiselect_leftSelected').removeClass('arrow-disabled');
                $('#SameSelectionOfDropdownList').css("display", "none");
                $('#SameSelectionOfDropdownList').removeClass("required-validity");
                listVendors_From.removeClass("disabled").prop("disabled", false);
                listVendors_To.removeClass("disabled").prop("disabled", false);
            }
        }
        else {
            listVendors_From.empty();
            productService.getDefaultVendors(selectedValue).then(function (result) {
                if (result.data) {
                    fillData(listVendors_From, selectedValue, result.data);
                }
            });
        }

    })
    // 2.3 to dropdown list

    $('#processorGroupList_To').on("change", function (e) {
        var selectedValue = $(this).find("option:selected").val();
        if ($('#processorGroupList_From').find("option:selected").val()) {
            if ($('#processorGroupList_From').find("option:selected").val() == selectedValue) {
                //to do: validation message
                $('#SameSelectionOfDropdownList').css("display", "block");
                $('#SameSelectionOfDropdownList').addClass("required-validity");
                //disable multiselect and move buttons
                listVendors_From.addClass("disabled").prop("disabled", true);
                listVendors_To.addClass("disabled").prop("disabled", true);
                $('#multiselect_rightSelected').addClass('arrow-disabled');
                $('#multiselect_leftSelected').addClass('arrow-disabled');
                //feed data
                listVendors_To.empty();
                productService.getDefaultVendors(selectedValue).then(function (result) {
                    if (result.data) {
                        fillData(listVendors_To, selectedValue, result.data);
                    }
                });
            }
            else {
                listVendors_To.empty();
                productService.getDefaultVendors(selectedValue).then(function (result) {
                    if (result.data) {
                        fillData(listVendors_To, selectedValue, result.data);
                    }
                });
                $('#multiselect_rightSelected').removeClass('arrow-disabled');
                $('#multiselect_leftSelected').removeClass('arrow-disabled');
                $('#SameSelectionOfDropdownList').css("display", "none");
                $('#SameSelectionOfDropdownList').removeClass("required-validity");
                listVendors_From.removeClass("disabled").prop("disabled", false);
                listVendors_To.removeClass("disabled").prop("disabled", false);
            }
        }
        else {
            listVendors_To.empty();
            productService.getDefaultVendors(selectedValue).then(function (result) {
                if (result.data) {
                    fillData(listVendors_To, selectedValue, result.data);
                }
            });
        }

    })
}

//OTSTM2-1142 Modal responsive when resizing
$(function ($) {
    $(window).resize(function () {
        if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
            $(".required-validity.product-name").css("left", "-5px");
            $(".required-validity.product-description").css("left", "-5px");
            $("#rangeMin").css("right", "185px");
            $("#rangeMax").css("left", "190px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
            }
            else {
                $("#shortName").css("font-size", "45px").css("margin-left", "");
            }
        }
        else if (matchMedia('(max-width: 767px)').matches) {
            $("#rangeMin").css("right", "225px");
            $("#rangeMax").css("left", "230px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
            }
            else {
                $("#shortName").css("font-size", "45px").css("margin-left", "");
            }
        }
        else {
            $(".required-validity.product-name").css("left", "");
            $(".required-validity.product-description").css("left", "");
            $("#rangeMin").css("right", "275px");
            $("#rangeMax").css("left", "280px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "").css("font-size", "50px");
            }
            else {
                $("#shortName").css("font-size", "50px").css("margin-left", "20px");
            }
        }
    });
})


