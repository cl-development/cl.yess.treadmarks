﻿'use strict';

//Reporting App
var reportingApp = angular.module("reportingApp",
    ["commonLib", "ui.bootstrap", "datatables", "datatables.scroller", "reporting.directives", "reporting.controllers", "reporting.services"]);

//Controllers Section
var controllers = angular.module('reporting.controllers', []);
controllers.controller('reportingController', ['$rootScope', '$scope', '$http', '$uibModal', function ($rootScope, $scope, $http, $uibModal) {

}]);
controllers.controller('GPSecondModalCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'batchNumber', 'type', function ($scope, $http, $uibModal, $uibModalInstance, batchNumber, type) {
    $scope.batchNumber = batchNumber;
    $scope.type = type;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Common Controllers
controllers.controller('GpSuccessCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'successResult', 'header', 'body', function ($scope, $uibModal, $uibModalInstance, successResult, header, body) {

    $scope.sucessMessage = body;
    $scope.successHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
controllers.controller('GpFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', 'isExport', function ($scope, $uibModal, $uibModalInstance, failResult, header, isExport) {

    if (!isExport) {
        $scope.failResult = failResult;
        $scope.failHeader = header;

        if ($scope.failResult.Errors.length > 0) {
            $scope.message = $scope.failResult.Errors[0];
        }
        else {
            $scope.message = $scope.failResult.Warnings[0];
        }
    }
    else {
        $scope.message = failResult;
        $scope.failHeader = header;
    }


    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//Services Section
var services = angular.module('reporting.services', []);
services.factory('financialIntegrationServices', ["$http", function ($http) {
    var factory = {};

    factory.createBatch = function (type) {
        var submitVal = {
            url: Global.ReportDetail.CreateBatchUrl,
            method: "POST",
            data: { type: type }
        }
        return $http(submitVal);
    }

    factory.postBatch = function (batchNum) {
        var submitVal = {
            url: Global.ReportDetail.PostBatchUrl,
            method: "POST",
            data: { batchId: batchNum }
        }
        return $http(submitVal);
    }

    factory.putOnHold = function (gpTransNum) {
        var submitVal = {
            url: Global.ReportDetail.OnHoldTransactionUrl,
            method: "POST",
            data: { gpiBatchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.putOffHold = function (gpTransNum) {
        var submitVal = {
            url: Global.ReportDetail.OffHoldTransactionUrl,
            method: "POST",
            data: { gpiBatchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.removeTransaction = function (gpTransNum) {
        var submitVal = {
            url: Global.ReportDetail.RemoveTransactionUrl,
            method: "POST",
            data: { batchEntryId: gpTransNum }
        }
        return $http(submitVal);
    }

    factory.checkExport = function (batchId) {
        var submitVal = {
            url: Global.ReportDetail.CheckExportUrl,
            method: "POST",
            data: { batchId: batchId }
        }
        return $http(submitVal);
    }

    factory.getExport = function (batchId, panelName) {
        return $http({
            method: 'GET',
            url: Global.ReportDetail.ExportUrl,
            params: {
                batchId: batchId,
                panelName: panelName
            }
        });
    }

    return factory;
}]);

//Directives Section
var directives = angular.module('reporting.directives', []);
directives.directive('reportStewardRevenueSupplyDesign', ['DTOptionsBuilder', 'DTColumnBuilder', function (DTOptionsBuilder, DTColumnBuilder) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'steward-Revenue-suppy-design.html',
        scope: {
            reportUrl: '@',
            type: '@',
            header: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            debugger;
            $scope.dtInstance = {};

            $scope.chevronId = $scope.header.replace(/\s/g, '');

            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, data) {
                $scope.dtInstance.DataTable.draw();
            });
            //return;
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("Period", "Batch #").withOption('name', 'Period'),
                    DTColumnBuilder.newColumn("legalName", "legalName").withOption('name', 'legalName'),
                    DTColumnBuilder.newColumn("class1", "class1").withOption('name', 'class1').renderWith(function (data, type, full, meta) {
                        var date = dateTimeConvert(data);
                        return '<div title="' + date.time + '">' + date.date + '</div>';
                    }),
                    DTColumnBuilder.newColumn("registrantStatus", "registrantStatus").withOption('name', 'registrantStatus').renderWith(function (data, type, full, meta) {
                        switch (data.replace(" ", "")) {
                            case "Extracted":
                                return '<div class="panel-table-status color-tm-yellow-bg" title="Extracted">Extracted</div>';
                            case "Queued":
                                return '<div class="panel-table-status color-tm-orange-bg" title="Posted (Queued)">Queued</div>';
                            case "FailedStaging":
                            case "Failed":
                                return '<div class="panel-table-status color-tm-red-bg" title="Failed (Import)">Failed</div>';
                            case "Posted":
                                return '<div class="panel-table-status color-tm-green-bg" title="Posted (Imported)">Posted</div>';
                            case "OnHold":
                                return '<div class="panel-table-status color-tm-gray-bg" title="On-Hold">On-Hold</div>';
                            case "Unknown":
                                return '<div class="panel-table-status color-tm-red-bg" title="Unknown">Unknown</div>';
                            default:
                                return data;
                        }
                    }),
                    DTColumnBuilder.newColumn("statusChangeDate", "statusChangeDate").withOption('name', 'statusChangeDate').renderWith(function (data, type, full, meta) {
                        if (data) {
                            var html = $compile($templateCache.get('messagePopover.html').replace('statusChangeDate', data))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml)
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.reportUrl,
                type: "POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [0, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('initComplete', function (settings, result) {
                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.$apply();
                });

            //open second modal
            $scope.openSecondModal = function (data) {
                var secondGPModal = $uibModal.open({
                    templateUrl: 'gp-second-modal-template.html',
                    controller: 'GPSecondModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        batchNumber: function () { return data; },
                        type: function () { return $scope.type; }
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    $scope.$apply(function () {
                        $scope.openSecondModal(aData.Period);
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {
                switch (data.registrantStatus.replace(" ", "")) {
                    case "Extracted":
                        var html = $compile($templateCache.get('panel-extracted.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    case "Queued":
                        var html = $compile($templateCache.get('panel-queued.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    case "Failed":
                    case "FailedStaging":
                        var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    case "Posted":
                        var html = $compile($templateCache.get('panel-posted.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    case "OnHold":
                        var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    case "Unknown":
                        var html = $compile($templateCache.get('panel-failed.html').replace(/batchNumber/g, data.Period).replace(/panelName/g, $scope.panelName))($scope);
                        return html[0].outerHTML;

                    default:
                        return "";
                }
            }

            function dateTimeConvert(data) {
                if (data == null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches == null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;

                return {
                    date: d,
                    time: curr_time
                }
            };
        }
    }
}]);
