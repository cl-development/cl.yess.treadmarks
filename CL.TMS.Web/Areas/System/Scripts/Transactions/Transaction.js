﻿$(function () {
    _txtSearchId = 'Search';//id of search textbox
    _exportId = 'export'; //id of export
    _modalId = 'myModal';
    var _smoothzoompanMainPhotoId = 'smoothzoompanMainPhoto';
    var _zoomCssClass = 'zoomPhoto';
    _PageSize = 5;
    _counter = 0;
    var transactionList = $('#myDataTable').DataTable({
        serverSide: true,
        ajax: "Transactions/GetTransactionListHandler",
        scrollY: 300,
        scrollX: true,
        createdRow: function (row, data, index) {
            $(row).addClass('cursor-pointer');
            var id = data[11];
            $(row).on('click', function (e) {
                var data = transactionList.row(this).data();
                var nodeName = e.target.nodeName;
                //debugger;
                //$('.confirmActionNumber').val(data[2]);
                $('.confirmActionNumber').html(data[2]);
                $('.confirmActionID').val(data[11]);
                console.log('.confirmActionID:'+$('.confirmActionID').val());
                //var url = '/System/Transactions/GetTransactionById/' + id;
                //$.get(url, function (data) {
                //    console.log('data:' + data);
                //    if (data != '') {
                //        $('#' + _modalId).html('');
                //        $('#' + _modalId).html(data).show();
                //        debugger;

                //        //do the same zooming thing for PTR scale tickets
                //        $('.' + _zoomCssClass).smoothZoom({
                //            width: 270,
                //            height: 270,
                //            zoom_MIN: '',
                //            initial_ZOOM: '',
                //            zoom_BUTTONS_SHOW: 'NO',
                //            pan_BUTTONS_SHOW: 'NO',
                //            pan_LIMIT_BOUNDARY: 'YES'
                //        });

                //        //now need to rebind jquery stuff
                //        $('.view-thumbnail').on('click', function () {
                //            var index = $('.view-thumbnail').index(this);
                //            var smoothzoompanMainUrl = $('#smoothzoompanMainUrl');
                //            var photoDesc = $('.smoothzoompanPhotoDesc').eq(index).html();
                //            var src = $(this).attr('src');
                //            $('#' + _smoothzoompanMainPhotoId).attr('src', src);
                //            $('#' + _smoothzoompanMainPhotoId).show().smoothZoom({
                //                width: 270,
                //                height: 270,
                //                zoom_MIN: '',
                //                reset_TO_ZOOM_MIN: true,
                //                initial_ZOOM: '',
                //                zoom_BUTTONS_SHOW: 'NO',
                //                pan_BUTTONS_SHOW: 'NO',
                //                pan_LIMIT_BOUNDARY: 'YES'
                //            });
                //            smoothzoompanMainUrl.attr('href', src);
                //            //hack to reset to initial zoom for smoothZoom
                //            $('#previewPhoto').find('#_zonorm').trigger('mousedown');
                //            $('#photo-comment').find('p').html(photoDesc);
                //        }).eq(0).trigger('click');

                //        var postalExpected = $('#hdnPostalExpected').val();
                //        var hdnCoordinatesActual = $('#hdnCoordinatesActual').val();
                //        var geocoder = new google.maps.Geocoder();
                //        geocoder.geocode({ 'address': postalExpected }, function (results, status) {
                //            var coordinatesA = '';
                //            var coordinatesStr = '';
                //            if (status != 'ZERO_RESULTS') {
                //                var coordinatesA = 'color:blue|label:A|' + results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();
                //                var coordinatesB = '';
                //                if (hdnCoordinatesActual)
                //                    coordinatesB = '&markers=color:red|label:B|' + hdnCoordinatesActual;
                //                coordinatesStr = coordinatesA + coordinatesB;
                //            }
                //            else {
                //                if (hdnCoordinatesActual)
                //                    coordinatesStr = 'color:red|label:B|' + hdnCoordinatesActual;
                //            }
                //            var content = getMap({ markers: coordinatesStr });
                //            $('#' + _modalId).find('.sample-popover-map').popover({ html: true, content: content, placement: 'bottom' });

                //        });
                //        $('#' + _modalId).modal('show');
                //    }
                //});
            });
        },
        processing: false,
        scrollCollapse: false,
        //"paging" : false,
        order: [[6, "desc"]],
        searching: true,
        "dom": "rtiS",
        info: false,
        deferRender: true,
        "scroller": {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },
        "aoColumns": [
                        {
                            "sName": "TransactionStatusTypeNameKey",
                            "bSearchable": true,
                            "render": function (data) {
                                //console.log('status data:' + data);
                                switch (data) {
                                    case 'Incomplete Transaction':
                                        //return '<img src="/Images/icon-trans-status-incomplete.png" class="icon-trans-status">';
                                        return '<div class="panel-table-status color-tm-yellow-bg">Incomplete</div>';
                                    case 'Complete Transaction':
                                        //return '<img src="/Images/icon-trans-status-complete.png" class="icon-trans-status" />';
                                        return '<div class="panel-table-status color-tm-green-bg">Completed</div>';
                                    case 'Error In Transaction':
                                        //return '<img src="/Images/icon-trans-status-completeerror.png" class="icon-trans-status">';
                                        return '<div class="panel-table-status color-tm-red-bg">Error</div>';
                                    case 'Deleted Transaction':
                                        //return '<img src="/Images/icon-trans-status-voided.png" class="icon-trans-status">';
                                        return '<div class="panel-table-status color-tm-gray-light-bg">Deleted</div>';
                                    case 'Voided':
                                        return '<div class="panel-table-status color-tm-gray-light-bg">Voided</div>';
                                    case 'pending':
                                        return '<div class="panel-table-status color-tm-orange-bg">Pending</div>';
                                    case 'rejected':
                                        return '<div class="panel-table-status color-tm-red-bg">Rejected</div>';
                                    //case 'error':
                                    //    return '<div class="panel-table-status color-tm-red-bg">Error</div>';
                                }
                                return '';
                            },
                            'fnCreatedCell': function (nTd, sData, oData, iRow, iCol) {
                                _counter++;
                                //if (_counter < 20) {
                                    //console.log('nTd, sData, oData, iRow, iCol:' + nTd + "; " + sData + "; " + oData + "; " + iRow + "; " + iCol);
                                //}
                                //nTd.title = sData;
                                switch (sData.toLowerCase()) {
                                    case 'incomplete transaction':
                                        nTd.title = sData;
                                        break;
                                    case 'complete transaction':
                                        nTd.title = sData;
                                        break;
                                    case 'deleted transaction':
                                        nTd.title = "Pending Approval by <<Reg #>>";
                                        break;
                                };
                            }
                        },

                        {
                            "sName": "deviceName",
                            render: function (data) {
                                return '<div class="truncate-100">' + data + '</div>';
                            }
                        },

                        {
                            "sName": "friendlyId",
                            "bSortable": true

                        },

                        { "sName": "transactionTypeId" },
                        { "sName": "createdDate" },
                        { "sName": "transactionDate" },
                        { "sName": "syncDate" },
                        {
                            "sName": "incomingRegistrationNumber",
                            "render": function (data) {
                                var incomingReg = parseInt(data);
                                return incomingReg > 0 ? incomingReg : '';
                            }
                        },
                        {
                            "sName": "outgoingRegistrationNumber",
                            "render": function (data) {
                                var outgoingReg = parseInt(data);
                                return outgoingReg > 0 ? outgoingReg : '';
                            }
                        },

                        {
                            "sName": "badgeId",
                            "className": "padRight"
                        },

                        {
                            className: "td-center",
                            data: null,
                            orderable: false,
                            render: function (data) {
                                //console.log('data:' + data);
                                return ApplicationActionRender(data[0]);
                            }
                        },

                        {
                            "sName": "transactionId",
                            "visible": false
                        },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
            $('#totalRecords').val('Total ' + settings.fnRecordsTotal());

            var api = this.api();

        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');
            $('#txtFound').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= _PageSize) ? $('#viewmore').css('visibility', 'visible') : $('#viewmore').css('visibility', 'hidden');
        },
        footerCallback: function (row, data, start, end, display) { }
    });

    transactionList.$('td').tooltip({
        "delay": 0,
        "track": true,
        "fade": 100
    });

    $("#Search").on('keyup', function () {
        var str = $(this).val();
        if (str.length >= 1) {
            transactionList.search(str).draw(false);
            $(this).css('visibility', 'visible');
            $('#txtFound').css('display', 'block');
            var url = Global.InvitationIndex.ExportUrl.replace('----', $('#' + _txtSearchId).val());
            $('#' + _exportId).attr('href', url);
            $(this).siblings('.remove-icon').show();
        }
        else {
            $(this).siblings('.remove-icon').hide();
            $('#' + _txtSearchId).removeAttr('style');
            $('#txtFound').css('display', 'none');
            var url = Global.InvitationIndex.ExportUrl.replace('----', $('#' + _txtSearchId).val());
            $('#' + _exportId).attr('href', url);
            transactionList.search(str).draw(false);
        }
    });

    $('#viewmore').on('click', function () {
        $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $(this).hide();
    });

    $('.remove-icon').click(function (n) {
        $(this).siblings("input").val("");
        $(this).hide();
        $("#Search").trigger('keyup');
    });

    $('.workflow').on('click', function () {
        var action = $(this).attr('data-action');
        //var clickedTransNumber = 0;
        //var clickedTransToken = 0;
        //debugger;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }
        if (action != "") {
            if (status.toLowerCase() == 'backtoapplicant') {
                //as per OTSTM-864 clear all fields that have their checkbox unchecked 

                $('input[type="checkbox"]:not(:checked)').each(function () {
                    clearField(this);
                });

                $('#hdnExplicitSave').trigger('click');//saves data                
            }
            //let model save first then change status
            setTimeout(function () {
                $.ajax({
                    url: '/System/Transactions/TransactionAction/',
                    method: 'POST',
                    dataType: "JSON",
                    data: { Token: $('.confirmActionID').val(), transId: $('.confirmActionNumber').html(), action: action },
                    success: function (data) {
                        //window.location.reload(true);
                        //transactionList.draw();
                        //transactionList.fnReloadAjax(transactionList.fnSettings());
                        transactionList.ajax.reload();
                    },
                });
            }, 1000);
        }
    });

    $('#export').on('click', function (e) {
        console.log('export csv');
        e.stopPropagation();
    });

    $('#btnNewFound').on('click', function (e) {
        console.log('btnNewFound');
        e.stopPropagation();
    });

    var getMap = function (opts) {
        var src = "http://maps.googleapis.com/maps/api/staticmap?",
            params = $.extend({
                //zoom: 14,
                size: '242x242',
                maptype: 'roadmap',
                sensor: false
            }, opts),
            query = [];

        $.each(params, function (k, v) {
            query.push(k + '=' + v);//encodeURIComponent(v));
        });

        src += query.join('&');
        return '<img class="popover-map" src="' + src + '" />';
    }

    function ApplicationActionRender(StatusName) {
        var content = "";
        if (StatusName == null) return content;
        switch (StatusName.toLowerCase()) {
            case 'complete transaction'://"Void":
                content = "<div class=\"btn-group dropdown-menu-actions\">" +
                "<a href=\"#\" data-toggle=\"dropdown\"><i class=\"glyphicon glyphicon-cog\"></i></a>" +
                "<ul class=\"dropdown-menu\">" +
                "<li><a data-target=\"#modalVoid\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-times\"></i>" +
                "Void" +
                "</a></li></ul></div>";
                break;
            case "accept":
            case 'incomplete transaction'://"Reject":
                content = "<div class=\"btn-group dropdown-menu-actions\"><a href=\"#\" data-toggle=\"dropdown\"><i class=\"glyphicon glyphicon-cog\"></i></a>" +
                "<ul class=\"dropdown-menu\"><li><a data-target=\"#modalAccept\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-check\"></i>Accept</a></li>" +
                "<li><a data-target=\"#modalReject\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-times\"></i>Reject</a></li>" +
                "</ul></div>";
                break;
        }
        return content;
    }

});//end