﻿"use strict"; 
// constants
var   validMsg = {
    clean: "clean",
    isRequired: "Required",  
    invalidInput: "Invalid Input",
    uniqueName:"The material name exists",
    noneMsg: ""
};
var productCompositionsApp = angular.module("productCompositionsApp", ['ui.bootstrap', 'commonLib', 'datatables', 'datatables.scroller'
    , "productCompositions.services", "productCompositions.directives", "productCompositions.controllers", "commonClaimInternalNotesLib"
]);

//OTSTM2-1042 for both supply and recovery
var Security = {
    isReadonly: function (resource) {
        return resource === Global.EstimatedWeights.Security.Constants.ReadOnly
    },
    isEditSave: function (resource) {
        return resource === Global.EstimatedWeights.Security.Constants.EditSave
    },
    isNoAccess: function (resource) {
        return resource === Global.EstimatedWeights.Security.Constants.NoAccess
    },
    checkUserSecurity: function (type) {
        switch (type) {
            case "1":
            case 1:
                return Global.EstimatedWeights.Security.Authorize;
                break;
            case "2":
            case 2:
                return Global.EstimatedWeights.Security.Authorize;
                break;          
            default:
                return Global.EstimatedWeights.Security.Authorize;
                break;
        }
    }
}

//service section
var services = angular.module("productCompositions.services", []);
services.factory('productService', ["$http", function ($http) {
    var factory = {};
    factory.SaveRM = function (data) {
        var submitVal = {
            url: Global.ProductCompositions.SaveRMUrl,
            method: "POST",
            data: { rvm: data }
        }
        return $http(submitVal);
    }
    factory.SaveMC = function (data) {
        var submitVal = {
            url: Global.ProductCompositions.SaveMCUrl,
            method: "POST",
            data: { vm: data }
        }
        return $http(submitVal);
    }
    factory.ExcelActivity = function (data) {
        var submitVal = {
            url: Global.ProductCompositions.ExcelUrl,
            method: "GET",
            params: data
        }
        return $http(submitVal);
    }
    factory.UniqueName = function (material) {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.ProductCompositions.UniqueNameUrl,
            method: "POST",
            data: { name: material }
        }
        return $http(submitVal);
    }
    factory.getData = function () {
        var submitVal = {
            headers: { 'Content-Type': 'application/json' },
            url: Global.ProductCompositions.getDataUrl,
            method: "GET",
            data: { }
        }
        return $http(submitVal);
    }

    //OTSTM2-1042 recovery estimated weights ===========================================
    factory.getWeightTransactionByID = function (weightTransactionID, category) {
        return $http({
            method: 'GET',
            url: Global.EstimatedWeights.LoadRecoveryEstimatedWeightDetailsByTransactionIDUrl,
            params: {
                weightTransactionID: weightTransactionID,
                category: category
            }
        });
    }
    factory.AddNewWeight = function (data, category) {
        var submitVal = {
            url: Global.EstimatedWeights.AddNewRecoveryEstimatedWeightUrl,
            method: "POST",
            data: { newWeight: data, category: category }
        }
        return $http(submitVal);
    }

    factory.EditWeight = function (data) {
        var submitVal = {
            url: Global.EstimatedWeights.EditRecoveryEstimatedWeightUrl,
            method: "POST",
            data: { weightVM: data }
        }
        return $http(submitVal);
    }

    factory.removeTransaction = function (weightTransactionID, category) {
        var submitVal = {
            url: Global.EstimatedWeights.RemoveTransactionUrl,
            method: "POST",
            data: { weightTransactionID: weightTransactionID, category: category }
        }
        return $http(submitVal);
    }

    factory.anchorCss = function () {
        return {
            "display": "block",
            "padding": "3px 20px",
            "clear": "both",
            "font-weight": "400",
            "line-height": "1.42857143",
            "color": "#000",
            "white-space": "nowrap"
        }
    }
    //OTSTM2-1042 recovery estimated weights ====================end=======================


    factory.getSupplyEstimatedWeightByID = function (itemID, category) {
        return $http({
            method: 'GET',
            url: Global.EstimatedWeights.LoadSupplyEstimatedWeightDetailsByItemIDUrl,
            params: {
                itemID: itemID,
                category: category
            }
        });
    }

    factory.EditSupplyEstimatedWeight = function (data) {
        var submitVal = {
            url: Global.EstimatedWeights.EditSupplyEstimatedWeightUrl,
            method: "POST",
            data: { supplyEstimatedWeightVM: data }
        }
        return $http(submitVal);
    }

    return factory;
}]);

//directives section
var directives = angular.module('productCompositions.directives', []);
directives.directive('recoverableMaterialListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'productService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {},
            templateUrl: 'list-panel-recoverable-material.html',
            controller: ['$scope', function ($scope) {

                $scope.ShowActionGear = Global.ProductCompositions.Security.Authorize === Global.ProductCompositions.Security.Constants.EditSave;
                $scope.isView = Global.ProductCompositions.Security.Authorize === Global.ProductCompositions.Security.Constants.ReadOnly;
                $scope.dtInstance = {};
 
                $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                    //after recoverable materials change, should refresh all the list panel.
                    window.location.reload(true);
                });

                $scope.dtColumns = [
                        DTColumnBuilder.newColumn("ID", "ID").withOption('name', 'ID').withClass('hidden'),
                        DTColumnBuilder.newColumn("MaterialName", "Material").withOption('name', 'MaterialName'),   
                        DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                        DTColumnBuilder.newColumn("DateModified","Date Modified").withOption('name','DateModified').renderWith(function (data, type, full, meta) {
                            if (data !== null) {
                                var date = dateTimeConvert(data);
                                return '<div>' + date.date + '</div>';
                            }
                            else {
                                return "";
                            }
                        }),
                        DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                        DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withOption('width', '13%').renderWith(actionsHtml)
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                    dataSrc: "data",
                    url: $scope.url,
                    type: "GET",
                    error: function (xhr, error, thrown) {}
                }).withOption('processing', true)
                    .withOption('responsive', true).withOption('bAutoWidth', false)
                    .withOption('serverSide', true)
                    .withOption('aaSorting', [1, 'asc'])
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('initComplete', function (settings, result) {                        
                        $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                        $scope.$apply();   
                    })
                    .withOption('drawCallback', function (settings, result) { 
                    });

                //view more
                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    $scope.dtOptions.withScroller()
                                    .withOption('deferRender', true)
                                    .withOption('scrollY', 220)                                                  
                                    .withOption('scrollX', "100%");
                }   

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }               

                function actionsHtml(data, type, full, meta) {
                    var html = $compile($templateCache.get('item-action.html')                               
                                .replace(/itemName/g, data.MaterialName)
                                .replace(/itemID/g, data.ID)
                                .replace(/itemDescription/g, data.MaterialDescription)
                                .replace(/itemColor/g, data.Color)
                                )($scope);
                    return html[0].outerHTML;
                }

                //click to view
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).addClass('rowClickable');
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function (ev) {                       
                        $scope.$apply(function () {
                            $scope.openFormForView(aData);
                        });
                    });
                    return nRow;
                }

                $scope.openFormForView = function (aData) {
                    var editModal = $uibModal.open({
                        templateUrl: 'modal-recoverable-material.html',
                        controller: 'editController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            materialName: function () {
                                return aData.MaterialName;
                            },
                            oldmaterialName: function(){
                                return aData.MaterialName;
                            },
                            id: function () {
                                return aData.ID;
                            },
                            materialDescription: function () {
                                return aData.MaterialDescription;
                            },
                            color: function () {
                                return aData.Color;
                            },
                            isView: function () {
                                return true;
                            }
                        }
                    });
                    editModal.rendered.then(function () {
                        setupSlider(aData.Color,true);
                        $(".require-input").focusout(function (e) {
                            if (!$(this).val() || $(this).val() == "") {
                                $(this).siblings(".required-validity").html(validMsg.isRequired);
                            } else if ($(this).data("name") == "material-name" && $(this).val().length > 15) {
                                $(this).siblings(".required-validity").html(validMsg.invalidInput);
                            } else {
                                $(this).siblings(".required-validity").html(validMsg.noneMsg);
                            }
                        });
                    });
                }
            }]
        }
    }]);

directives.directive('materialCompositionListPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', 'productService', '$http', '$compile', '$templateCache', '$rootScope', '$uibModal',
    function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService, $http, $compile, $templateCache, $rootScope, $uibModal) {
        return {
            scope: {
                url: '@',
            },
            restrict: 'E',
            link: function (scope, elem) {

            },
            templateUrl: 'list-panel-material-composition.html',
            controller: ['$scope', function ($scope) {
                $scope.ShowActionGear = Global.ProductCompositions.Security.Authorize === Global.ProductCompositions.Security.Constants.EditSave;
                $scope.isView = Global.ProductCompositions.Security.Authorize === Global.ProductCompositions.Security.Constants.ReadOnly;
                $scope.dtInstance = {};               
   
                $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                    $scope.dtInstance.DataTable.draw();
                });
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ItemID").withOption('width', '7%').notSortable().renderWith(function (data, type, full, meta) {
                        var html = "<div style='text-align:center;'><img src='/images/shared/Material-Composition-Tire-icon.png' height='80%' width='80%'/></div>";
                        return html;
                    }),
                    DTColumnBuilder.newColumn("ItemName", "Recovered Product").notSortable().withOption('width', '16%').withOption('name', 'ItemName').renderWith(function (data, type, full, meta) {
                        return '<strong>'+data + '<br/>' + full.ItemShortName+'</strong>';
                    }), 
                    DTColumnBuilder.newColumn("DateModified", "Date Modified").notSortable().withOption('width', '10%').withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div  style="text-overflow: ellipsis; overflow:hidden;">' + date.date + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("ModifiedBy", "Modified By").notSortable().withOption('width', '10%').withOption('name', 'ModifiedBy'),
                    DTColumnBuilder.newColumn(null).notSortable().withOption('width', '7%').withOption('name', 'ItemMaterialList').renderWith(function (data, type, full, meta) {
                        var html = '<div class="donut-chart" id="donut-"' + data.ItemID + ' data-row="' + meta.row + '"></div>';
                        return html;
                    }),
                    DTColumnBuilder.newColumn("ItemMaterialList", "Product Composition").notSortable().withOption('width', '37%').renderWith(function (data, type, full, meta) {
                        var htmlStart = '<div class="donut-chart-text details col-md-3" data-row="' + meta.row + '">';
                        var count = 1;
                        var html=""; 
                        $.each(data, function (index, item) {
                            if (item.Quantity && item.Quantity > 0) {                               
                                if (count == 1) {
                                    html = html +htmlStart;
                                }
                                var color = item.Color ? item.Color : "white";
                                var frontColor = item.Color ? getContrast(item.Color) : "white";
                                var quantity = item.Quantity < 10 ? "&nbsp;&nbsp;" + item.Quantity + "%" : item.Quantity + "%";
                                var name = item.MaterialName ? item.MaterialName : "";
                                html += '<label class="rowClickable" style="text-overflow: ellipsis; overflow:hidden;"><span class="label-chart-text"><span class="label" style="background-color:' + color + '; color:' + frontColor + ';">' + quantity + '</span>';
                                html += "<span data-info= " + name + " > " + name + '</span></span></label><br/>';
                                count++;
                                if (count > 3) {
                                    html += '</div>';
                                    count=1;
                                }                               
                            }
                        });   
                        return html;
                    }),                
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable().withOption('width', '13%').renderWith(roleActionsHtml)
                ];

                $scope.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('ajax',
                    {
                        dataSrc: "data",
                        url: $scope.url,
                        type: "POST"
                    })
                    .withOption('processing', false)
                    .withOption('responsive', true)
                    .withOption('bAutoWidth', false)
                    .withOption('aaSorting', [])
                    .withOption('serverSide', true)
                    .withOption('lengthChange', false)
                    .withDisplayLength(5)
                    .withDOM('tr')
                    .withOption('rowCallback', rowCallback)
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);                     
                        if ($scope.viewMore != undefined && $scope.viewMore == false) {
                            $(angular.element(row)["0"].children[1]).addClass('move-right-15px');
                            $(angular.element(row)["0"].children[2]).addClass('move-right-20px');
                            $(angular.element(row)["0"].children[3]).addClass('move-right-25px');
                        }
                    })
                    .withOption('initComplete', function (settings, result) {
                    })
                    .withOption('drawCallback', function (settings, result) {
                        $scope.ShowActionGear = $scope.ShowActionGear &&
                                                settings.aoData[0] &&
                                                settings.aoData[0]._aData &&
                                                settings.aoData[0]._aData.recoverableMaterials &&
                                                settings.aoData[0]._aData.recoverableMaterials.length > 0
                        $scope.viewMore = (settings.aoData && settings.aoData.length >= 5 && !$scope.scroller);
                        $scope.$apply();
                    });

                //view more
                $scope.viewMoreClick = function ($event) {
                    $scope.viewMore = false;
                    $scope.scroller = true;

                    $scope.dtOptions.withScroller()
                                   .withOption('deferRender', true)
                                   .withOption('scrollY', 555)
                                    .withOption('scrollX', "100%");
                }

                $scope.delay = (function () {
                    var promise = null;
                    return {
                        calculateDelay: function (callback, ms) {
                            $timeout.cancel(promise);
                            promise = $timeout(callback, ms)
                        }
                    }
                })();

                //remove icon
                $scope.removeIcon = function (dtInstance) {

                    $scope.searchText = '';
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }

                $("#titleIcon").on("click", function () {
                    if ($(this).find(".fa-minus-square").length != 0) {
                        $(this).find(".fa-minus-square").removeClass("fa-minus-square").addClass("fa-plus-square");
                    }
                    else if ($(this).find(".fa-plus-square").length != 0) {
                        $(this).find(".fa-plus-square").removeClass("fa-plus-square").addClass("fa-minus-square");
                    }
                });

                function roleActionsHtml(data, type, full, meta) {
                    var html = $compile($templateCache.get('item-action-composition.html')                                   
                                    .replace(/itemName/g, data.ItemName)
                                    .replace(/itemShortName/g, data.ItemShortName)
                                    .replace(/itemID/g, data.ItemID))($scope);
                    return html[0].outerHTML;
                }

                //click to view
                function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:not(:last-child)', nRow).addClass('rowClickable');
                    $('td:not(:last-child)', nRow).unbind('click');
                    $('td:not(:last-child)', nRow).bind('click', function () {                       
                        $scope.$apply(function () {
                            $scope.openFormForView(aData);
                        });
                    });                 
                    var pie = $(nRow).find(".donut-chart");
                    var pieCharts = pieChart(aData.ItemMaterialList);
                    pie[0].appendChild(pieCharts);
                    return nRow;
                }
                // get Contrast color
                function getContrast(color) {
                    var colorRGB = color.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+)/i);
                    var rgb = [
                        parseInt(colorRGB[1]),
                        parseInt(colorRGB[2]),
                        parseInt(colorRGB[3])
                    ];
                    var o = Math.round(((parseInt(rgb[0]) * 299) +
                      (parseInt(rgb[1]) * 587) +
                      (parseInt(rgb[2]) * 114)) / 1000);
                    var frontcolor = (o > 125) ? 'black' : 'white';
                    return frontcolor;
                }

                $scope.openFormForView = function (aData) {              
                    $scope.result = [];    
                    $.each(aData.recoverableMaterials, function (name, value) {
                        var temp = {
                            itemID: $scope.itemId,
                            materialID: value.ItemID,
                            materialName: value.ItemName,
                            color: value.ItemValue,
                            quantity: 0
                        }
                        $scope.result.push(temp);
                    });
                    $.each(aData.ItemMaterialList, function (name, value) {
                        if (value.MaterialID) {
                            //$scope.result.find(c=>c.materialID == value.MaterialID).quantity = value.Quantity;
                            $scope.result.find( function ( c ){
                                return c.materialID == value.MaterialID
                            }).quantity = value.Quantity;
                        }
                    })                   
                   
                    var viewModal = $uibModal.open({
                        templateUrl: 'modal-material-composition.html',
                        controller: 'editCompositionController',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            itemName: function () {
                                return aData.ItemName;
                            },
                            itemID: function () {
                                return aData.ItemID;
                            },
                            itemShortName: function () {
                                return aData.ItemShortName;
                            },
                            isView: function () {
                                return true;
                            },
                            recoverableMaterials: function () {
                                return $scope.result;
                            }
                        }
                    });
                    viewModal.rendered.then(function (e) {                        
                        morrisDonut($scope.result, "composition-piechart");
                        setupCompositionSlider($scope.result, true);
                    });
                }

            }]
        }
    }]);

directives.directive('productCompositionActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "productService", function ($window, DTOptionsBuilder, DTColumnBuilder, $timeout, productService) {
    return {
        scope: {
            url: '@',
        },
        restrict: 'E',
        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));
                    }
                }
                scope.$digest();
            });
            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '300',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }
            }
        },
        templateUrl: 'product-composition-activity-panel.html',
        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {

            $scope.dtInstance = {};
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width', '15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return  date.date + ' ' + date.time;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];
            $scope.sortColumn=''
            $scope.direction = '';
            $scope.searchValue = '';
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.url,
                global: false,
                type: "POST",
                error: function (xhr, error, thrown) {    
                }
            }).withOption('processing', true)
                .withOption('responsive', true)
                .withOption('bAutoWidth', false)
                .withOption('order', [0, 'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    $scope.sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    $scope.direction = settings.aLastSort[0].dir;
                    $scope.searchValue = $('#searchActivity').val();
                    $scope.$apply();
                });

            //$scope.url = $scope.snpUrl; //keep original snpUrl
            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false
            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };
                $scope.searchValue = $('#searchActivity').val();
                if ($scope.searchValue) {                   
                    $scope.url = $scope.url + "&temp=" + $scope.searchValue;                
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 220)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc: "data",
                                    url: $scope.url,
                                    global: false, // this makes sure ajaxStart is not triggered
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //Auto reload datatable after new activity generated
            $rootScope.$on('TOTAL_ACTIVITY', function (e, data) {
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            });

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    //$scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.url,
                                   global: false,
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            $("#exportActivities").on("click", function (event) {
                event.stopPropagation();
                event.preventDefault();      
                var data={                                       
                    sortcolumn: $scope.sortColumn,
                    sortDirection:$scope.direction,
                    searchText: $scope.searchValue,
                }
                productService.ExcelActivity(data).then(function (result) {
                    var currentDate = new Date();
                    var min = currentDate.getMinutes();
                    var hh = currentDate.getHours();
                    var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                    var yyyy = currentDate.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = "Product-Composition-Activity-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                        navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                    }
                    else {
                        var anchor = '<a title="Export to CSV" href="javascript:void(0);" style="display: none;">';
                        var body = $(document.body).append(anchor);
                        $(anchor).attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                            target: '_blank',
                            download: filename + '.csv'
                        })[0].click();
                        $(anchor).remove();
                    }
                });
            });

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '300',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }         
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace

//for show action gear pop-up
directives.directive('tablerowpopover', ['$compile', function ($compile) {

    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var content = el[0].nextElementSibling.innerHTML;
            $(el).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });
        }
    }
}]);

directives.directive('htmlMessage', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            message: '='
        },
        link: function (scope, e, attrs) {
            var template = $compile(scope.message)(scope);
            e.replaceWith(template);
        }
    }
}]);
//Add Button handler
directives.directive('addButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'add-btn.html',
        scope: {
            panelName:'@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.isView = Global.ProductCompositions.Security.Authorize === Global.ProductCompositions.Security.Constants.ReadOnly;
        },
        controller: ['$scope', function ($scope) {
            $scope.openModal = function () {
                var addModal = $uibModal.open({
                    templateUrl:'modal-recoverable-material.html',
                    controller: 'addNewController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        panelName: function () {
                            return $scope.panelName;
                        }
                    }
                });
                //after modal rendered setup sliders.
                addModal.rendered.then(function () {
                    setupSlider(Global.ProductCompositions.InitColor,false);
                    $(".require-input").focusout(function (e) {                      
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).siblings(".required-validity").html(validMsg.isRequired);
                        } else if ($(this).data("name") == "material-name" && $(this).val().length>15) {
                            $(this).siblings(".required-validity").html(validMsg.invalidInput);
                        } else {
                            $(this).siblings(".required-validity").html(validMsg.noneMsg);
                        }
                    });
                });
            };        
        }]
    };
}]);

//Edit Button handler
directives.directive('editButton', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {          
            itemName: '@',
            itemId:'@',
            itemDescription: '@',
            itemColor:'@'
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                var editModal = $uibModal.open({
                    templateUrl: 'modal-recoverable-material.html',
                    controller:  'editController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        materialName: function () {
                            return $scope.itemName;
                        },
                        oldmaterialName: function () {
                            return $scope.itemName;
                        },
                        id: function () {
                            return $scope.itemId;
                        },
                        materialDescription: function () {
                            return $scope.itemDescription;
                        },
                        color: function () {
                            return $scope.itemColor;
                        },
                        isView: function () {
                            return false;
                        }
                    }
                });
                
                //initial recoverable material composition edit view
                editModal.rendered.then(function () {
                    setupSlider($scope.itemColor, false);
                    $(".require-input").focusout(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).siblings(".required-validity").html(validMsg.isRequired);
                        } else if ($(this).data("name") == "material-name" && $(this).val().length > 15) {
                            $(this).siblings(".required-validity").html(validMsg.invalidInput);
                        } else {
                            $(this).siblings(".required-validity").html(validMsg.noneMsg);
                        }
                    });
                });
             
            }
        }]
    };
}]);
directives.directive('editButtonComposition', ['$uibModal', '$http', function ($uibModal, $http) {
    return {
        restrict: 'E',
        templateUrl: 'edit-btn.html',
        scope: {         
            itemName: '@',
            itemId: '@',
            itemShortName: '@',            
        },
        link: function (scope, el, attrs, roleFormController) { },
        controller: ['$scope', function ($scope) {
            $scope.edit = function () {
                $scope.result = {};
                $.each($scope.$parent.dtInstance.DataTable.data(), function (index, item) {
                    if (item.ItemID == $scope.itemId) {
                        var copy = [];
                        $.each(item.recoverableMaterials, function (name, value) {
                            var temp = {
                                itemID:$scope.itemId,
                                materialID: value.ItemID,
                                materialName: value.ItemName,
                                color: value.ItemValue,
                                quantity:0
                            }
                            copy.push(temp);
                        });
                        $.each(item.ItemMaterialList, function (name, value) {
                            if (value.MaterialID) {
                                //copy.find(c=>c.materialID == value.MaterialID).quantity = value.Quantity;
                                copy.find(function ( c ) {
                                    return c.materialID == value.MaterialID
                                }).quantity = value.Quantity;
                            }
                        })
                        $scope.result = copy;
                        return false;
                    }
                });               
                var editModal = $uibModal.open({
                    templateUrl: 'modal-material-composition.html',
                    controller:  'editCompositionController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        itemName: function () {
                            return $scope.itemName;
                        },
                        itemID: function () {
                            return $scope.itemId;
                        },
                        itemShortName: function () {
                            return $scope.itemShortName;
                        },                       
                        isView: function () {
                            return false;
                        },
                        recoverableMaterials: function () {
                            return  $scope.result;
                        }
                    }
                });               
                editModal.rendered.then(function () {
                    morrisDonut($scope.result, "composition-piechart");  
                    setupCompositionSlider($scope.result, false);   
                });
            }
        }]
    };
}]);

directives.directive('validNumber', function () {
    return {
        require: '?ngModel',
        scope: {
            max: '@',
            min: '@',
            maxlength: '@',
            theprefex: '@',
            decimalSize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {

            ngModelCtrl.$parsers.push(function (val) {
              
                val = '' + val;               
                var clean = val.replace(/[^0-9]/g, '');
                var decimalCheck = clean.split('.');
                var scale = scope.decimalSize ? parseInt(scope.decimalSize) : 0;
                decimalCheck[0] = decimalCheck[0].slice(0, (scope.maxlength - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }                
                if (parseInt(clean) > parseInt(scope.max)) {
                    clean = "0";
                }              
                if (val !== clean && clean != "" || val==="0") {     
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
             
                return clean;
            });
            
            element.bind('keydown', function (e) {              
                if (!((e.key >= 0 && e.key <= 9) || [8, 37, 39, 46].indexOf(e.keyCode)>-1)) { 
                    e.preventDefault();
                }            
            }).bind('blur', function (event) {
                event.preventDefault();
                var temp = element.val()=="" ? "0": element.val();
                temp = +temp;
                ngModelCtrl.$setViewValue(""+temp);           
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});

//OTSTM2-1042 Estimated Weights==================Directive Section===============================
directives.directive('supplyEstimatedWeightListPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', 'productService', function (DTOptionsBuilder, DTColumnBuilder, productService) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'supply-estimated-weight-list-panel-template.html',
        scope: {
            weightUrl: '@',
            category: '@',
            header: '@',
            panelName: '@',
            isTop: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.dtInstance = {};
            $scope.chevronId = $scope.header.replace(/\s/g, '');
            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                if (tableId == $scope.dtInstance.id) {
                    $scope.dtInstance.DataTable.draw();
                }
            });
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "itemID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("ProductDescription", "PRODUCT DESCRIPTION").withOption('name', 'ProductDescription').withOption('width', '50%').notSortable().renderWith(function (data, type, full, meta) {
                        if (full !== null) {
                            return "<div style='text-align:center;'><img src='/images/shared/Material-Composition-Tire-icon.png' height='75px' width='75px' style='float:left;'/></div><div style='margin-left: 40px; padding-left:40px'><label style='font-size:16px; font-weight:bold; margin-top:15px;'>"
                                + full.ProductName + "</label><br/><label style='font-size:16px; margin-top:-8px;'>" + full.ProductDescription + "</label></div>";
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Min", "MIN(kg)").withOption('name', 'Min').notSortable().renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Max", "MAX(kg)").withOption('name', 'Max').notSortable().renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),                                
                    DTColumnBuilder.newColumn(null).withTitle('ACTIONS').notSortable()
                    .renderWith(actionsHtml),
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.weightUrl,
                type: "GET",
                error: function (xhr, error, thrown) {
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', true) //change bAutoWidth from false to true to make sure horizontal scrollbar appears correctly after clicking more
                .withOption('serverSide', true)              
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                    //clicking more, make sure alignment
                    if($scope.viewMore != undefined && $scope.viewMore == false) {
                        $(angular.element(row)["0"].children[2]).addClass('move-right-20px');
                        $(angular.element(row)["0"].children[3]).addClass('move-right-20px');
                        $(angular.element(row)["0"].children[4]).addClass('move-right-20px');
                    }
                })
                .withOption('initComplete', function (settings, result) {                   
                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                    $scope.categoryName = result.categoryName;
                    $scope.categoryId = result.category;
                    //table ID
                    $("#tableIDKeeping").attr("table-id", settings.sTableId);
                })
                .withOption('drawCallback', function (settings, result) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.$apply();
                });

            //open detail modal
            $scope.openDetailsModal = function (data, dtInstance_id, type) {
                var weightDetailModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: 'editSupplyEstimatedWeightCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        itemID: function () { return data.ID; },
                        category: function () { return $scope.categoryId; },
                        panelName: function () { return $scope.panelName; },
                        type: function () { return type; }
                    }
                });
                //Modal responsive (view mode)
                weightDetailModal.rendered.then(function () {                   
                    if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
                        $(".required-validity.product-name").css("left", "-5px");
                        $(".required-validity.product-description").css("left", "-5px");
                        $("#rangeMin").css("right", "185px");
                        $("#rangeMax").css("left", "190px");                       
                        if (data.ShortName.length == 3) {
                            $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
                        }
                        else {
                            $("#shortName").css("font-size", "45px").css("margin-left", "");
                        }
                    }
                    else if (matchMedia('(max-width: 767px)').matches) {
                        $("#rangeMin").css("right", "225px");
                        $("#rangeMax").css("left", "230px");
                        if (data.ShortName.length == 3) {
                            $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
                        }
                        else {
                            $("#shortName").css("font-size", "45px").css("margin-left", "");
                        }
                    }
                    else {
                        $(".required-validity.product-name").css("left", "");
                        $(".required-validity.product-description").css("left", "");
                        $("#rangeMin").css("right", "275px");
                        $("#rangeMax").css("left", "280px");
                        if (data.ShortName.length == 3) {
                            $("#shortName").css("margin-left", "").css("font-size", "50px");
                        }
                        else {
                            $("#shortName").css("font-size", "50px").css("margin-left", "20px");
                        }
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 500)
                               .withOption('scrollX', "100%");
            }

            //search
            $scope.search = function (dtInstance) {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $scope._pageX = 0;
            $scope._pageY = 0;
            $scope._cancelMouseClick = false;

            $('table').on('mousedown', 'tr', function (e) {
                $scope._pageX = e.pageX;
                $scope._pageY = e.pageY;
            });
            $('table').on('mouseup', 'tr', function (e) {
                $scope._pageX = e.pageX - $scope._pageX;
                $scope._pageY = e.pageY - $scope._pageY;
                if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                    $scope._cancelMouseClick = true;
                }
            });

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).addClass('rowClickable');
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    if ($scope._cancelMouseClick) {
                        $scope._cancelMouseClick = false;
                        return;
                    }
                    $scope.$apply(function () {
                        $scope.openDetailsModal(aData, $scope.dtInstance.id, 'View');//click on list row will open as [View] mode
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {               
                var html = "";
                var resource = Security.checkUserSecurity(data.Category);
                if (Security.isReadonly(resource)) {
                    return html;
                }
                var temp = $compile($templateCache.get('supply-estimated-weight-list-panel-action.html').replace(/itemNumber/g, data.ID).replace(/panelName/g, $scope.panelName).replace(/shortName/g, data.ShortName))($scope);
                html = temp[0].outerHTML;
                               
                return html;
            }
        }
    }
}]);
directives.directive('recoveryEstimatedWeightListPanelView', ['DTOptionsBuilder', 'DTColumnBuilder', 'productService', function (DTOptionsBuilder, DTColumnBuilder, productService) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'recovery-estimated-weight-list-panel-template.html',
        scope: {
            weightUrl: '@',
            category: '@',
            header: '@',
            panelName: '@',
            isTop: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $http, $compile, $filter, $templateCache, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.isEditSave = Security.isEditSave(resource);
            $scope.dtInstance = {};
            $scope.chevronId = $scope.header.replace(/\s/g, '');
            $rootScope.$on('PANEL_VIEW_UPDATED', function (e, tableId) {
                if (tableId == $scope.dtInstance.id) {
                    $scope.dtInstance.DataTable.draw();
                }
            });
            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("ID", "weightTransactionID").notSortable().withOption('name', 'ID').withClass('hidden'),
                    DTColumnBuilder.newColumn("EffectiveStartDate", "Effective Date").withOption('name', 'EffectiveStartDate').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("DateAdded", "Date Added").withOption('name', 'DateAdded').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("AddedBy", "Added By").withOption('name', 'AddedBy'),
                    DTColumnBuilder.newColumn("DateModified", "Date Modified").withOption('name', 'DateModified').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            return '<div>' + data.substring(0, data.indexOf("T")) + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("ModifiedBy", "Modified By").withOption('name', 'ModifiedBy'),
                    DTColumnBuilder.newColumn("NotesAllText", "Notes").notSortable().withOption('name', 'NotesAllText').renderWith(function (data, type, full, meta) {
                        if (data) {
                            var html = $compile($templateCache.get('messagePopover.html').replace('Message', data))($scope);
                            return html[0].outerHTML;
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                    .renderWith(actionsHtml),
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.weightUrl,
                type: "GET",
                error: function (xhr, error, thrown) {
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('serverSide', true)
                .withOption('aaSorting', [1, 'desc'])
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('rowCallback', rowCallback)
                .withOption('createdRow', function (row, data, dataIndex) {
                    $compile(angular.element(row).contents())($scope);
                })
                .withOption('initComplete', function (settings, result) {
                    $("#addNewWeightBtn" + result.category).prop("disabled", (result.isFutureRateExists || !$scope.isEditSave));
                    $("#addNewWeightBtn" + result.category).attr("table-id", settings.sTableId);
                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                    $scope.categoryName = result.categoryName;
                    $scope.categoryId = result.category;                   
                })
                .withOption('drawCallback', function (settings, result) {
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.$apply();
                });

            //open detail modal
            $scope.openDetailsModal = function (data, dtInstance_id, type) {
                var weightDetailModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: 'editWeightCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        weightTransactionID: function () { return data.ID; },
                        category: function () { return $scope.categoryId; },
                        panelName: function () { return $scope.panelName; },
                        type: function () { return type; }
                    }
                });
            }

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;
                $scope.dtOptions.withScroller()
                               .withOption('deferRender', true)
                               .withOption('scrollY', 250);
            }

            //search
            $scope.search = function (dtInstance) {
                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //remove icon
            $scope.removeIcon = function (dtInstance) {
                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                dtInstance.DataTable.search($scope.searchText).draw(false);
            }

            $scope._pageX = 0;
            $scope._pageY = 0;
            $scope._cancelMouseClick = false;

            $('table').on('mousedown', 'tr', function (e) {
                $scope._pageX = e.pageX;
                $scope._pageY = e.pageY;
            });
            $('table').on('mouseup', 'tr', function (e) {
                $scope._pageX = e.pageX - $scope._pageX;
                $scope._pageY = e.pageY - $scope._pageY;
                if ((Math.abs($scope._pageX) > 5) || (Math.abs($scope._pageY) > 5)) {
                    $scope._cancelMouseClick = true;
                }
            });

            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:not(:last-child)', nRow).addClass('rowClickable');
                $('td:not(:last-child)', nRow).unbind('click');
                $('td:not(:last-child)', nRow).bind('click', function () {
                    if ($scope._cancelMouseClick) {
                        $scope._cancelMouseClick = false;
                        return;
                    }
                    $scope.$apply(function () {
                        $scope.openDetailsModal(aData, $scope.dtInstance.id, 'View');//click on list row will open as [View] mode
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {
                var timestamp = Date.parse(data.EffectiveStartDate);
                var html = "";
                var resource = Security.checkUserSecurity(data.Category);
                if (Security.isReadonly(resource)) {
                    return html;
                }
                if (!isNaN(timestamp)) {
                    var now = new Date();
                    var startDate = new Date(timestamp);                    
                    //Comment out the followed code for QATEST
                    if (startDate > now) {
                        var temp = $compile($templateCache.get('recovery-estimated-weight-list-panel-action.html').replace(/weightTransactionNumber/g, data.ID).replace(/panelName/g, $scope.panelName))($scope);
                        html = temp[0].outerHTML;
                    }
                    //Uncomment out the followed code for QATEST 
                    //var temp = $compile($templateCache.get('recovery-estimated-weight-list-panel-action.html').replace(/weightTransactionNumber/g, data.ID).replace(/panelName/g, $scope.panelName)) ($scope);
                    //html = temp[0].outerHTML;
                }
                return html;
            }
        }
    }
}]);
directives.directive('addNewWeight', ['productService', function (productService) {
    return {
        restrict: 'E',
        templateUrl: 'weight-add-new-btn.html',
        scope: {
            category: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
        },
        controller: function ($scope, $uibModal, $rootScope) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.weightDetails = function () {
                var addNewWeightModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: 'addNewWeightCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                    }
                });
            }
        }
    }
}]);
directives.directive('editWeight', ['productService', function (productService) {
    return {
        restrict: 'E',
        templateUrl: 'weight-edit-btn.html',
        scope: {
            weighttransactionid: '@',
            category: '@',
            type: '@',
            panelName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = productService.anchorCss();

            if (scope.isReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.editDetails = function () {
                var postBatchModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: 'editWeightCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        weightTransactionID: function () {
                            return $scope.weighttransactionid;
                        },
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        type: function () {
                            return $scope.type;
                        }
                    }
                });
            }
        }
    }
}]);
directives.directive('editSupplyEstimatedWeight', ['productService', function (productService) {
    return {
        restrict: 'E',
        templateUrl: 'supply-estimated-weight-edit-btn.html',
        scope: {
            itemid: '@',
            category: '@',
            type: '@',
            panelName: '@',
            shortName: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = productService.anchorCss();

            if (scope.isReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);
            $scope.editDetails = function () {
                var postBatchModal = $uibModal.open({
                    templateUrl: getPanelUrl($scope.category),
                    controller: 'editSupplyEstimatedWeightCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        itemID: function () {
                            return $scope.itemid;
                        },
                        category: function () {
                            return $scope.category;
                        },
                        panelName: function () {
                            return $scope.panelName;
                        },
                        shortName: function () {
                            return $scope.shortName;
                        },
                        type: function () {
                            return $scope.type;
                        }
                    }
                });
                postBatchModal.rendered.then(function () {                   
                    $(".require-input.product-name").keyup(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).parent().parent().next().find(".required-validity.product-name").html(validMsg.isRequired);
                            $(this).addClass("has-error");
                        } else {
                            $(this).parent().parent().next().find(".required-validity.product-name").html(validMsg.noneMsg);
                            $(this).removeClass("has-error");
                        }
                    });
                    $(".require-input.product-description").keyup(function (e) {
                        if (!$(this).val() || $(this).val() == "") {
                            $(this).parent().parent().next().find(".required-validity.product-description").html(validMsg.isRequired);
                            $(this).addClass("has-error");
                        } else {
                            $(this).parent().parent().next().find(".required-validity.product-description").html(validMsg.noneMsg);
                            $(this).removeClass("has-error");
                        }
                    });

                    //Modal responsive (edit mode)
                    if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
                        $(".required-validity.product-name").css("left", "-5px");
                        $(".required-validity.product-description").css("left", "-5px");
                        $("#rangeMin").css("right", "185px");
                        $("#rangeMax").css("left", "190px");
                        if ($scope.shortName.length == 3) {
                            $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
                        }
                        else {
                            $("#shortName").css("font-size", "45px").css("margin-left", "");
                        }
                    }
                    else if (matchMedia('(max-width: 767px)').matches) {
                        $("#rangeMin").css("right", "225px");
                        $("#rangeMax").css("left", "230px");
                        if ($scope.shortName.length == 3) {
                            $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
                        }
                        else {
                            $("#shortName").css("font-size", "45px").css("margin-left", "");
                        }
                    }
                    else {
                        $(".required-validity.product-name").css("left", "");
                        $(".required-validity.product-description").css("left", "");
                        $("#rangeMin").css("right", "275px");
                        $("#rangeMax").css("left", "280px");                     
                        if ($scope.shortName.length == 3) {
                            $("#shortName").css("margin-left", "").css("font-size", "50px");
                        }
                        else {
                            $("#shortName").css("font-size", "50px").css("margin-left", "20px");
                        }
                    }
                });
            }
        }
    }
}]);
directives.directive('removeTransaction', ['productService', function (productService) {
    return {
        restrict: 'E',
        templateUrl: 'weight-remove-btn.html',
        scope: {
            weighttransactionid: '@',
            category: '@'
        },
        link: function (scope, el, attrs, formCtrl) {
            scope.anchorCss = productService.anchorCss();

            if (scope.isReadOnly) {
                scope.anchorCss.cursor = 'not-allowed'
            }
        },
        controller: function ($scope, $uibModal) {
            var resource = Security.checkUserSecurity($scope.category);
            $scope.isReadOnly = Security.isReadonly(resource);

            $scope.remove = function () {
                var removeTransactionModal = $uibModal.open({
                    templateUrl: 'weight-confirm-modal.html',
                    controller: 'removeTransCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        weightTransactionID: function () {
                            return $scope.weighttransactionid;
                        },
                        category: function () {
                            return $scope.category;
                        }
                    }
                });
            }
        }
    }
}]);
directives.directive('scrollPanelFixBtn', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('scroll', function () {
                element.find('.position-fixed-x').css('left', element.scrollLeft());
            });
        }
    };
});
directives.directive('messageHoverPopover', function ($compile, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;

            $(element).popover({
                content: function () {
                    return $compile(content)(scope);
                },
                placement: 'bottom',
                container: "body",
                template: '<div class="popoverMessage" role="tooltip"><div class="popover-content"></div></div>',
                html: true
            });

            $(element).bind('mouseenter', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover) {
                        $(element).popover('show');
                        scope.attachEvents(element);
                    }
                }, 200);
            });
            $(element).bind('mouseleave', function (e) {
                $timeout(function () {
                    if (!$rootScope.insidePopover)
                        $(element).popover('hide');
                }, 400);
            });
        },
        controller: function ($scope, $element) {
            $scope.attachEvents = function (element) {
                $('.popover').on('mouseenter', function () {
                    $rootScope.insidePopover = true;
                });
                $('.popover').on('mouseleave', function () {
                    $rootScope.insidePopover = false;
                    $(element).popover('hide');
                });
            }
        }
    };
});
directives.directive('noteMessageHoverPopover', function ($compile, $templateCache, $timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var content = element[0].nextElementSibling.innerHTML;
            var note = toHtml(content);

            function toHtml(data) {
                var internalNote = data;
                var res = internalNote.split('\n');
                var result = "";
                var arrayLength = res.length;
                for (var i = 0; i < arrayLength; i++) {
                    if (res[i]) {
                        result = result + '<p>' + res[i] + '</p>';
                    }
                }
                return result;
            }
            var placement = 'auto';
            if (scope.header === "Remittance Penalty") {
                placement = 'bottom';
            }
            $(element).webuiPopover({
                width: '500',
                height: '300',
                padding: true,
                multi: true,
                closeable: true,
                title: scope.header + ' Notes',
                type: 'html',
                trigger: 'hover',
                content: function () {
                    return $compile(note)(scope);
                },
                delay: {
                    show: 100, hide: 100
                },
                placement: placement,
            });
        },
        controller: function ($scope, $element) { }
    };
});
//OTSTM2-1042 valid estimated weight number
directives.directive('validWeightNumber', function () {
    return {
        require: '?ngModel',
        scope: {
            theprefex: '@',
            decimalsize: '@'
        },
        link: function (scope, element, attr, ngModelCtrl) {
            var precision = (angular.isDefined(attr.precision) && attr.precision != "") ? parseInt(attr.precision) : 12;
            var zero = "00000";
            var prefex = scope.theprefex == "%" || scope.theprefex == "non-dollar" ? '' : '$';
            var scale;


            ngModelCtrl.$parsers.push(function (val) {

                if (angular.isUndefined(val)) {
                    var val = '0';
                }


                var clean = val.replace(/[^0-9\.]/g, '');
                var decimalCheck = clean.split('.');
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                decimalCheck[0] = decimalCheck[0].slice(0, (precision - scale));
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, scale);
                    clean = decimalCheck[0] != '' ? decimalCheck[0] + '.' + decimalCheck[1] : '0.' + decimalCheck[1];
                } else {
                    clean = decimalCheck[0];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(prefex + clean);
                    ngModelCtrl.$render();
                }
                return clean != "" ? prefex + clean : clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            }).bind('blur', function () {
                scale = scope.decimalsize ? parseInt(scope.decimalsize) : 2;
                var elementValue = element.val() ? element.val().split('$').length > 1 ? element.val().split('$')[1] : element.val().split('$')[0] : '';
                if (elementValue == '' || elementValue == '-' || elementValue.indexOf('-') > -1) {
                    ngModelCtrl.$setViewValue(prefex + (scale > 0 ? "0." + zero.substr(0, scale) : "0"));
                } else {
                    var splitValue = elementValue.split('.');
                    if (splitValue.length > 1 && splitValue[1].length > scale) {
                        elementValue = splitValue[0] + '.' + splitValue[1].substr(0, scale);
                    }
                    $(".valid-number-error").hide()
                    var fixedValue = parseFloat(elementValue).toFixed(scale).toString();
                    if (scope.theprefex) {
                        if (scope.theprefex == "%" && parseFloat(elementValue) > 100) {
                            $(".valid-number-error").show();
                            fixedValue = "0";
                        }
                    } else {
                        fixedValue = prefex + fixedValue;
                    }
                    ngModelCtrl.$setViewValue(fixedValue);
                }
                ngModelCtrl.$render();
                scope.$apply();
            });
        }
    }
});

//OTSTM2-1042 Estimated Weights==================Directive Section end===========================

//controller section
var controllers = angular.module('productCompositions.controllers', []);
controllers.controller("productCompositionController", ['$rootScope', '$scope', '$http', '$uibModal', "productService", function ($rootScope, $scope, $http, $uibModal,productService) { }]);

controllers.controller('addNewController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http', 'panelName', 'productService', function ($scope,$rootScope, $uibModalInstance, $uibModal, $http, panelName, productService) {
    $scope.vm = {
        title: Global.ProductCompositions.PanelHeader.replace("AAA", "New ") + panelName,
        buttonTitle:"Save",
        ID:0,
        MaterialName: "",
        MaterialDescription: "",
        Color: "",
        isView:false
    }   
    $scope.submitContent = function () {
        //valid check   
        var isValid = validCheck($scope.vm);
        if (!isValid) {        
            return;
        };
        var uniqueName = productService.UniqueName($scope.vm.MaterialName);
        $scope.vm.Color = $("#result").css("color");
        uniqueName.success(function (response) {
            if (response && response.result) {
                $("input.require-input").siblings(".required-validity").html(validMsg.invalidInput);
                return;
            } else {
                //confirm 
                var confirmModal = $uibModal.open({
                    templateUrl: 'confirm-modal.html',          
                    size: 'lg',
                    backdrop: 'static',
                    controller: function ($scope, $uibModalInstance, $uibModal) {
                        $scope.confirmHeader = Global.ProductCompositions.ConfirmHeader;
                        $scope.confirmMessage = Global.ProductCompositions.ConfirmAddMaterialHtml;
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        }
                        $scope.confirm = function () {
                            $uibModalInstance.close();
                        };
                    }
                });

                confirmModal.result.then(function (confirm) { //confirm            
                    productService.SaveRM($scope.vm).then(function (response) {
                        $rootScope.$emit('PANEL_VIEW_UPDATED',null);
                        $uibModalInstance.close(true);
                    });
                    $uibModalInstance.dismiss('close');
                }, function (cancel) {
                    console.log('non-save');
                });
            }
        });
        uniqueName.error(function (data) {
            $uibModalInstance.close({ closeParentRoleModal: false });
            return;
        })
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('editController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'materialName','oldmaterialName', 'id', 'materialDescription', 'color','isView', 'productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, materialName,oldmaterialName, id, materialDescription, color,isView, productService) {
    $scope.vm = {
        title: isView ? Global.ProductCompositions.PanelHeader.replace("AAA", "Recoverable Material") :           
             Global.ProductCompositions.PanelHeader.replace("AAA", "Recoverable Material"),
        buttonTitle:"Save",
        ID:id,
        MaterialName: materialName,
        oldmaterialName:oldmaterialName,
        MaterialDescription: materialDescription,
        Color: color,
        isView:isView
    }    

    $scope.submitContent = function () {
        //valid check       

        var isValid = validCheck($scope.vm);
        if (!isValid) {
            return;
        };

        $scope.vm.Color = $("#result").css("color");

      
        if ($scope.vm.oldmaterialName.trim().toLowerCase() != $scope.vm.MaterialName.trim().toLowerCase())
        {
            var uniqueName = productService.UniqueName($scope.vm.MaterialName);
            uniqueName.success(function (response) {
                if (response && response.result) {
                    $("input.require-input").siblings(".required-validity").html(validMsg.invalidInput);
                    return;
                } else {
                    //confirm 
                    var confirmModal = $uibModal.open({
                        templateUrl: 'confirm-modal.html',
                        size: 'lg',
                        backdrop: 'static',
                        controller: function ($scope, $uibModalInstance, $uibModal) {
                            $scope.confirmHeader = Global.ProductCompositions.ConfirmHeader;
                            $scope.confirmMessage = Global.ProductCompositions.ConfirmAddMaterialHtml;
                            $scope.cancel = function () {
                                $uibModalInstance.dismiss('cancel');
                            }
                            $scope.confirm = function () {
                                $uibModalInstance.close();
                            };
                        }
                    });

                    confirmModal.result.then(function (confirm) { //confirm            
                        productService.SaveRM($scope.vm).then(function (response) {
                            $rootScope.$emit('PANEL_VIEW_UPDATED', null);
                            $uibModalInstance.close(true);
                        });
                        $uibModalInstance.dismiss('close');
                    }, function (cancel) {
                        console.log('non-save');
                    });
                }
            });         
        } else {
            //confirm 
            var confirmModal = $uibModal.open({
                templateUrl: 'confirm-modal.html',
                size: 'lg',
                backdrop: 'static',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmHeader = Global.ProductCompositions.ConfirmHeader;
                    $scope.confirmMessage = Global.ProductCompositions.ConfirmEditMaterialHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                }
            });
            confirmModal.result.then(function (confirm) { //confirm            
                productService.SaveRM($scope.vm).then(function (response) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED', null);
                    $uibModalInstance.close(true);
                });
                $uibModalInstance.dismiss('close');
            }, function (cancel) {
                console.log('non-save');
            });
        }
    }
    // close add modal 
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

controllers.controller('editCompositionController', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$http',
    'itemName', 'itemShortName','itemID', 'isView', 'recoverableMaterials','productService', function ($scope, $rootScope, $uibModalInstance, $uibModal, $http, 
    itemName, itemShortName, itemID, isView,recoverableMaterials, productService) {
        $scope.vm = {
            title: isView ? Global.ProductCompositions.PanelHeader.replace("AAA", "Product Composition") :
                Global.ProductCompositions.PanelHeader.replace("AAA", "Product Composition"),
            ItemID: itemID,
            ItemName: itemName,
            ItemShortName: itemShortName,          
            isView: isView,
            recoverableMaterials: recoverableMaterials
        }
        $scope.editSave = function () {
            //valid check       
            $scope.mc = $scope.vm;
            var tempM = [];
            var summary = 0;
            $.each($scope.vm.recoverableMaterials, function (index, item) {
                var temp = {                  
                    MaterialID: item.materialID,
                    MaterialName:item.materialName,
                    Quantity: item.quantity,
                    Color:item.color
                }
                summary += item.quantity;
                tempM.push(temp);
            });  
            
            if (summary != 100 && summary != 0) {
                $("#less100").show();
                $("div.col-md-7[style='overflow-x: auto;']").animate({ scrollLeft: '-2000' }, 1000);
                return;
            } else {
                $("#less100").hide();
            };
            $scope.mc.ItemMaterialList = tempM;
            //confirm 
            var confirmModal = $uibModal.open({
                templateUrl: 'confirm-modal.html',
                size: 'lg',
                backdrop: 'static',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmHeader = Global.ProductCompositions.ConfirmHeader;
                    $scope.confirmMessage = Global.ProductCompositions.ConfirmEditMaterialHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                }
            });
            confirmModal.result.then(function (confirm) { //confirm            
                productService.SaveMC($scope.mc).then(function (response) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    $uibModalInstance.close(true);
                });
                $uibModalInstance.dismiss('close');
            }, function (cancel) {
                console.log('non-save');
            });
        }
        // close add modal 
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

//OTSTM2-1042 Estimated Weights ===============controller section==========================
controllers.controller("estimatedWeightsController", ['$rootScope', '$scope', '$http', '$uibModal', "productService", function ($rootScope, $scope, $http, $uibModal, productService) { }]);
controllers.controller('weightFailCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'failResult', 'header', function ($scope, $uibModal, $uibModalInstance, failResult, header) {

    $scope.message = failResult;
    $scope.failHeader = header;

    $scope.confirm = function () {
        $uibModalInstance.close();
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//OTSTM2-1042 Add New Estimated Weights Controller
controllers.controller('addNewWeightCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'category', 'productService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, category, productService, $rootScope) {
    $scope.isValid = { note: true, noteIsRequire: true, effectiveStartDate: true };
    $scope.isView = false;
    $scope.isEdit = false;
    $scope.isAddNew = true;
    
    productService.getWeightTransactionByID(0, category).then(function (data) {//0:initial a new Rate
        $scope.weightVM = weightViewModel(data.data, category); 
        $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
        triggerDateTimePicker(category);       
    });

    $scope.tableId = $("#addNewWeightBtn" + category).attr("table-id");
    $scope.confirm = function () {
        $scope.disableBtn = true;
        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
        $uibModalInstance.close(response.data.result);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $uibModalInstance.close(true);
    };

    // Start of Add New Weight
    $scope.submitNewWeight = function (e) {
        $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.weightVM.Note != null;
        var timestamp = Date.parse(moment($scope.weightVM.EffectiveDate));

        var now = new Date();
        var nextMonth;
        
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        $scope.isValid.effectiveStartDate = true;

        //Comment out the followed code for QATEST
        if (isNaN(timestamp) || timestamp < nextMonth.getTime()) {  
            $scope.isValid.effectiveStartDate = false;
        } else {
            $scope.weightVM.EffectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.weightVM.EffectiveDate = new Date(timestamp).toISOString().split('Z')[0];
            $scope.weightVM.Notes = [];
        }
        
        //Uncomment out the followed code for QATEST
        //var lastMonth = new Date(now.getFullYear() - 1, now.getMonth() + 11, 1);
        //if (isNaN(timestamp) || timestamp < lastMonth.getTime()) {  ///QATEST nextMonth to thisMonth
        //    $scope.isValid.effectiveStartDate = false;
        //} else {
        //    $scope.weightVM.EffectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
        //    $scope.weightVM.EffectiveDate = new Date(timestamp).toISOString().split('Z')[0];
        //    $scope.weightVM.Notes =[];
        //}

        if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
            $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
            return;
        }
        var confirmAddNewModal = $uibModal.open({
            templateUrl: 'weight-confirm-modal.html',
            controller: function ($scope, $uibModalInstance, $uibModal) {
                if (category == Global.EstimatedWeights.WeightCategory.SupplyEstimatedWeights.weightCategoryID) {
                    $scope.confirmMessage = Global.EstimatedWeights.AddNewWeightConfirmHtml.replace("Weights", "Supply Estimated Weights");
                } else if (category == Global.EstimatedWeights.WeightCategory.RecoveryEstimatedWeights.weightCategoryID) {
                    $scope.confirmMessage = Global.EstimatedWeights.AddNewWeightConfirmHtml.replace("Weights", "Recovery Estimated Weights");
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.confirm = function () {
                    $uibModalInstance.close();
                };
            },
            size: 'lg',
            backdrop: 'static',
            resolve: {
                weightTransactionID: function () {
                    return $scope.weighttransactionid;
                },
                weightBatchEntyId: function () {
                    return $scope.gpiBatchentryId;
                },
                weightType: function () {
                    return $scope.category;
                }
            }
        });

        confirmAddNewModal.result.then(function (e) {
            $("#addNewWeightBtn" + category).prop("disabled", true);
            var data = weightPostModel($scope.weightVM);
            productService.AddNewWeight(data, category).then(function (response) {
                //console.log('response:', response);
                if (response.data.status) {
                    $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    $uibModalInstance.close(true);
                } else {
                    $("#addNewWeightBtn" + category).prop("disabled", false);
                    $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
                    var failSaveModal = $uibModal.open({
                        templateUrl: 'weight-fail-modal.html',
                        controller: 'weightFailCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        resolve: {
                            header: function () { return Global.EstimatedWeights.AddNewWeightFailHeader },
                            failResult: function () { return response && response.data ? response.data.statusMsg : Global.EstimatedWeights.EditWeightFailHtml; }
                        }
                    });
                };
            });
        }, function () {
            $("#addNewWeightBtn" + category).prop("disabled", false);
            $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
        });
    };
}]);

//OTSTM2-1042 Edit Estimated Weights Controller
controllers.controller('editWeightCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'weightTransactionID', 'category', 'panelName', 'type',
    'productService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, weightTransactionID, category, panelName, type, productService, $rootScope) {
        $scope.weightTransactionID = weightTransactionID;
        $scope.disableSaveBtn = false;
        $scope.disableNoteBtn = false;
        //if (type === 'View') {
        $scope.category = category;
        //}
        var resource = Security.checkUserSecurity($scope.category);
        if (Security.isReadonly(resource)) {//true: disable Add Notes
            $scope.disableNoteBtn = true;
            $scope.disableSaveBtn = true;
        }
        $scope.isValid = { note: true, noteIsRequire: true, effectiveStartDate: true };
        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;
        $scope.quantity = 5;
        $scope.tableId = $("#addNewWeightBtn" + category).attr("table-id");
        $scope.confirmMessage = Global.EstimatedWeights.EditWeightConfirmHtml.replace("WeightTransactionID", weightTransactionID).replace("Type", type).replace("PanelName", panelName);

        productService.getWeightTransactionByID(weightTransactionID, category).then(function (data) {
            $scope.weightVM = weightViewModel(data.data, category);
            if (typeof ($scope.weightVM) === 'object') {//session time out handling
                $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
                
                triggerDateTimePicker(category);
                $scope.allInternalNotes = $scope.weightVM.Notes;
                $scope.viewMore = ($scope.weightVM.Notes.length > 5 && !$scope.scroller);
                $('#weight-viewdetail-notes > div > div.form-group > textarea').prop("disabled", $scope.disableNoteBtn);
                $('#weight-viewdetail-notes > div > button').prop("disabled", $scope.disableNoteBtn);                
            }
            else {
                console.log('session time out?');
            }
        });

        $scope.addUrl = Global.EstimatedWeights.AddNewNoteUrl;
        $scope.loadUrl = Global.EstimatedWeights.LoadNotesListUrl;
        $scope.exportUrl = Global.EstimatedWeights.InternalNotesExportUrl;

        $scope.sortingOrder = false;

        $scope.noteSorting = function (e) {
            $scope.sortingOrder = !$scope.sortingOrder;
            $(e.target).removeClass('sorting');
            $(e.target).toggleClass('sorting_asc', $scope.sortingOrder);
            $(e.target).toggleClass('sorting_desc', !$scope.sortingOrder);
        };

        // Start of Save Weight
        $scope.editSaveWeight = function () {
            $scope.isValid.note = $scope.isValid.noteIsRequire && $scope.weightVM.Note != null;
            var timestamp = Date.parse(moment($scope.weightVM.EffectiveDate));
            
            var now = new Date();
            var nextMonth;
            
            if (now.getMonth() == 11) {
                nextMonth = new Date(now.getFullYear() + 1, 0, 1);
            } else {
                nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
            }
            $scope.isValid.effectiveStartDate = true;

            //Comment out the followed code for QATEST
            if (isNaN(timestamp) || timestamp < nextMonth.getTime()) { 
                $scope.isValid.effectiveStartDate = false;
            } else {
                $scope.weightVM.EffectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.weightVM.EffectiveDate = new Date(timestamp).toISOString().split('Z')[0];
                $scope.weightVM.Notes = [];
            }

            //Uncomment out the followed code for QATEST
            //var lastMonth = new Date(now.getFullYear() - 1, now.getMonth() + 11, 1);
            //if (isNaN(timestamp) || timestamp < lastMonth.getTime()) { ///QATEST nextMonth to thisMonth
            //    $scope.isValid.effectiveStartDate = false;
            //} else {
            //    $scope.weightVM.EffectiveStartDate = new Date(timestamp).toISOString().split('Z')[0];
            //    $scope.weightVM.EffectiveDate = new Date(timestamp).toISOString().split('Z')[0];
            //    $scope.weightVM.Notes =[];
            //}

            if (!$scope.isValid.note || !$scope.isValid.effectiveStartDate) {
                $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
                return;
            }
            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'weight-confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.EstimatedWeights.EditWeightConfirmHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    weightTransactionID: function () {
                        return $scope.weighttransactionid;
                    },
                    weightBatchEntyId: function () {
                        return $scope.gpiBatchentryId;
                    },
                    weightType: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {
                var data = weightPostModel($scope.weightVM);
                productService.EditWeight(data).then(function (response) {
                    if (response && response.data && response.data.status) {
                        $uibModalInstance.close();
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    } else {
                        $scope.disableSaveBtn = false;
                        $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'weight-fail-modal.html',
                            controller: 'weightFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.EstimatedWeights.EditWeightFailHeader },
                                failResult: function () {
                                    return response && response.data ? response.data.statusMsg : Global.EstimatedWeights.EditWeightFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {
                $scope.disableSaveBtn = false;
                $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
            });
        };

        $scope.cancel = function () {
            if (typeof ($scope.weightVM) === 'object') {//session time out handling

            }
            $scope.weightVM.EffectiveDate = $scope.weightVM.EffectiveStartDate.substring(0, 7);
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
        };
        //view more
        $scope.viewMoreNotesClick = function ($event) {
            $scope.quantity = $scope.allInternalNotes.length;
            $scope.viewMore = false;
            $scope.scroller = true;
            $('.table_scrollBody').css({ 'max-height': '160px', 'overflow': 'auto', 'overflow-y': 'scroll' });
        }
    }]);
//OTSTM2-1042 Remove Transaction Controller
controllers.controller('removeTransCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'weightTransactionID', 'category', 'productService', '$rootScope', function ($scope, $uibModal, $uibModalInstance, weightTransactionID, category, productService, $rootScope) {

    if (category == Global.EstimatedWeights.WeightCategory.SupplyEstimatedWeights.weightCategoryID) {
        $scope.confirmMessage = Global.EstimatedWeights.RemoveTransactionConfirmHtml.replace("Weights", "Supply Estimated Weights");
    } else if (category == Global.EstimatedWeights.WeightCategory.RecoveryEstimatedWeights.weightCategoryID) {
        $scope.confirmMessage = Global.EstimatedWeights.RemoveTransactionConfirmHtml.replace("Weights", "Recovery Estimated Weights");
    }
    $scope.confirm = function () {
        productService.removeTransaction(weightTransactionID, category).then(function (response) {
            if (response.data.result) {
                var tableId = $("#addNewWeightBtn" + category).attr("table-id");
                $rootScope.$emit('PANEL_VIEW_UPDATED', tableId);
                $uibModalInstance.close(response.data.result);
                $("#addNewWeightBtn" + category).prop("disabled", !response.data.result);
            } else {
                $uibModalInstance.dismiss('cancel');
                var failRemoveModal = $uibModal.open({
                    templateUrl: 'weight-fail-modal.html',
                    controller: 'weightFailCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        header: function () { return Global.EstimatedWeights.RemoveTransactionFailHeader },
                        failResult: function () {
                            return response && response.data ? response.data.statusMsg : Global.EstimatedWeights.RemoveTransactionFailHtml;
                        }
                    }
                });
            }
        });
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

//OTSTM2-1042 Edit Estimated Weights Controller
controllers.controller('editSupplyEstimatedWeightCtrl', ['$scope', '$http', '$uibModal', '$uibModalInstance', 'itemID', 'category', 'panelName', 'type',
    'productService', '$rootScope', function ($scope, $http, $uibModal, $uibModalInstance, itemID, category, panelName, type, productService, $rootScope) {
        $scope.itemID = itemID;
        $scope.disableSaveBtn = false;
        $scope.disableNoteBtn = false;
        //if (type === 'View') {
        $scope.category = category;
        //}
        var resource = Security.checkUserSecurity($scope.category);
        if (Security.isReadonly(resource)) {//true: disable Add Notes
            $scope.disableNoteBtn = true;
            $scope.disableSaveBtn = true;
        }
        //$scope.isValid = { note: true, noteIsRequire: true, effectiveStartDate: true };
        $scope.isEdit = type && type === 'Edit';
        $scope.isView = !$scope.isEdit;
        $scope.quantity = 5;
        $scope.tableId = $("#tableIDKeeping").attr("table-id");
        $scope.confirmMessage = Global.EstimatedWeights.EditWeightConfirmHtml.replace("ItemID", itemID).replace("Type", type).replace("PanelName", panelName);

        productService.getSupplyEstimatedWeightByID(itemID, category).then(function (data) {
            $scope.supplyEstimatedWeightVM = data.data;
            $scope.supplyEstimatedWeightVM.Category = category;
            //var array = $scope.supplyEstimatedWeightVM.ProductName.split(":");
            //$scope.supplyEstimatedWeightVM.ProductName = array[1];
            InitialSupplyEstimatedWeightSlider($scope.supplyEstimatedWeightVM.Min, $scope.supplyEstimatedWeightVM.Max, $scope.supplyEstimatedWeightVM.RangeMin, $scope.supplyEstimatedWeightVM.RangeMax);
        });
                     
        // Start of Save Weight
        $scope.editSaveSupplyEstimatedWeight = function () {
            var isValid = validRequiredCheck($scope.supplyEstimatedWeightVM);
            if (!isValid) {
                return;
            };
            $scope.disableSaveBtn = true;
            var confirmSaveModal = $uibModal.open({
                templateUrl: 'weight-confirm-modal.html',
                controller: function ($scope, $uibModalInstance, $uibModal) {
                    $scope.confirmMessage = Global.EstimatedWeights.EditWeightConfirmHtml;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.confirm = function () {
                        $uibModalInstance.close();
                    };
                },
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    itemID: function () {
                        return $scope.itemid;
                    },
                    weightBatchEntyId: function () {
                        return $scope.gpiBatchentryId;
                    },
                    weightType: function () {
                        return $scope.type;
                    }
                }
            });

            confirmSaveModal.result.then(function () {
                var data = $scope.supplyEstimatedWeightVM;
                //data.ProductName = data.ShortName + ':' + data.ProductName;
                //Make sure the Min and Max values can be adjusted by slider
                var supplyEstimatedWeightSlider = document.getElementById('supplyEstimatedWeightSlider');               
                var sliderArray = supplyEstimatedWeightSlider.noUiSlider.get();
                if (data.Min != sliderArray[0] || data.Max != sliderArray[1]) {
                    data.Min = sliderArray[0];
                    data.Max = sliderArray[1]
                }
                productService.EditSupplyEstimatedWeight(data).then(function (response) {
                    if (response && response.data && response.data.status) {
                        $uibModalInstance.close();
                        $rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
                    } else {
                        $scope.disableSaveBtn = false;                        
                        var failSaveModal = $uibModal.open({
                            templateUrl: 'weight-fail-modal.html',
                            controller: 'weightFailCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                header: function () { return Global.EstimatedWeights.EditWeightFailHeader },
                                failResult: function () {
                                    return response && response.data ? response.data.statusMsg : Global.EstimatedWeights.EditWeightFailHtml;
                                }
                            }
                        });
                    }
                });
            }, function () {
                $scope.disableSaveBtn = false;                
            });
        };

        $scope.cancel = function () {
            if (typeof ($scope.weightVM) === 'object') {//session time out handling

            }          
            $uibModalInstance.dismiss('cancel');
            //$rootScope.$emit('PANEL_VIEW_UPDATED', $scope.tableId);
        };
        
    }]);
function weightPostModel(data) {
    var result = {};
    $.each(data, function (name, value) {
        if (value instanceof Object && name != "notes") {
            var subObj = {};
            $.each(value, function (itemname, itemvalue) {
                subObj[itemname] = 0;
                subObj[itemname] = itemvalue && itemvalue.split('$').length > 1 ? parseFloat(itemvalue.split('$')[1]) : parseFloat(itemvalue.split('$')[0]);
            })
            result[name] = subObj;
        } else {
            result[name] = value;
        }
    });
    return result;
};
function weightViewModel(data, category) {
    var result = {};
    //var prefex = category == Global.Rate.RateCategory.RemittancePenaltyRates.rateCategoryID || category == Global.Rate.RateCategory.EstimatedWeightsRates.rateCategoryID ? '' : '$';
    if (typeof (data) === 'object') {//session time out handling
        $.each(data, function (name, value) {
            if (value instanceof Object && name != "notes") {
                var copy = {};
                $.each(value, function (itemname, itemvalue) {
                    if (typeof itemvalue !== 'object') {
                        if (typeof itemvalue === 'number') {
                            //copy[itemname] = prefex + itemvalue.toFixed(data.decimalsize).toString();
                            copy[itemname] = itemvalue.toFixed(data.decimalsize).toString();
                        } else if (typeof itemvalue === 'string') {
                            copy[itemname] = itemvalue;
                        }
                    }
                })
                result[name] = copy;
            } else {
                result[name] = value;
            }
        });
        return result;
    } else {
        console.log('session time out?');
        return false;//return Boolean instead of object[null]
    }
};
//OTSTM2-1042 Estimated Weights =============controller section end========================


//draw pie
function pieChart(data) {

    //init piechart
    var svgns = "http://www.w3.org/2000/svg";
    var chart = document.createElementNS(svgns, "svg:svg");
    var size = 80; //pie chart 2xradius.
    chart.setAttribute("width", size);
    chart.setAttribute("height", size);
    chart.setAttribute("viewBox", "0 0 " + size + " " + size);
    //chart.setAttribute("preserveAspectRatio","xMaxYMax meet"); //right align

    // Background circle
    var back = document.createElementNS(svgns, "circle");
    back.setAttributeNS(null, "cx", size / 2);
    back.setAttributeNS(null, "cy", size / 2);
    back.setAttributeNS(null, "r", size / 2);
    var backColor = "#ebebeb";   
    back.setAttributeNS(null, "fill", backColor);
    chart.appendChild(back);

    // draw pie path material
    var startPercentage = 0;
    var startAngle = 0;
    var color = "#EEEEEE"; //default gray 
    $.each(data, function (index, item) {
        if (startAngle < 0) startAngle = 0;
        startPercentage = startPercentage + item.Quantity;
        color = item.Color ? item.Color : "#EEEEEE";
        var maPath = piePath(startPercentage, size, startAngle, color);
        chart.appendChild(maPath.path);
        startAngle = maPath.endangle;
    });
  
    // foreground circle
    var front = document.createElementNS(svgns, "circle");
    front.setAttributeNS(null, "cx", (size / 2));
    front.setAttributeNS(null, "cy", (size / 2));
    front.setAttributeNS(null, "r", (size * 35/100)); //about 34% as big as back circle 
    front.setAttributeNS(null, "fill", "#fff");
    chart.appendChild(front);
    return chart;
}
// draw pie path
function piePath(percentage, size, startAngle, pathColor) {
    // primary wedge
    var svgns = "http://www.w3.org/2000/svg";
    var path = document.createElementNS(svgns, "path"); 
    var startangle = startAngle;
    var endangle = percentage *  (Math.PI * 2) / 100 - 0.001;
    var x1 = (size / 2) + (size / 2) * Math.sin(startangle);
    var y1 = (size / 2) - (size / 2) * Math.cos(startangle);
    var x2 = (size / 2) + (size / 2) * Math.sin(endangle);
    var y2 = (size / 2) - (size / 2) * Math.cos(endangle);
    var big = endangle - startangle > Math.PI ? 1 : 0;                    
    var d = "M " + (size / 2) + "," + (size / 2) +  // Start at circle center
        " L " + x1 + "," + y1 +     // Draw line to (x1,y1)
        " A " + (size / 2) + "," + (size / 2) +       // Draw an arc of radius r
        " 0 " + big + " 1 " +       // Arc details...
        x2 + "," + y2 +             // Arc goes to to (x2,y2)
        " Z";                       // Close path back to (cx,cy)
    path.setAttribute("d", d); // Set this path 
    path.setAttribute("fill", pathColor); 
    return { path: path, endangle: endangle, percentage:percentage };
}

//morris draw pie chart
function morrisDonut(items,element) {
    var data = [];
    var color = [];
    var init = 100;
    $.each(items, function (index, item) {
        if (item.quantity > 0) {
            init = init - item.quantity;
            data.push({ value: item.quantity, label: item.materialName, id:index});
            color.push(item.color);
        }
    });
    if (init > 0) {      
        data.push({ value: init, label: ""});
        color.push("#EEEEEE");
    }
    $("#composition-piechart").html("");
    var donut=Morris.Donut({
        element: element,
        data: data,
        colors: color,       
        formatter: function (x, data, element) {           
            if (data.label == "") {
                return parseInt(x) == 100 ? "0%" : parseInt(x) + "%";
            } else {
                return parseInt(x) + "%";
            }
        }
    }); 
    return donut;
}

// setup slider for color picker
function setupSlider(color,isView) {   
    var sliders = document.getElementsByClassName('sliders');
    var inputNumbers = $('.keypress');
    var rgb = color.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+)/i);
    var defaulRGB = [
        parseInt(rgb[1]),
        parseInt(rgb[2]),
        parseInt(rgb[3])
    ];
	for (var i = 0; i < sliders.length; i++) {
	    noUiSlider.create(sliders[i], {
	        start: defaulRGB[i],
	        connect: [true, false],
	        orientation: "vertical",
	        direction: 'rtl',
	        range: {
	            'min': 0,
	            'max': 255
	        },
	    });
	    sliders[i].noUiSlider.on('slide', setColor);
	    inputNumbers[i].value = defaulRGB[i];
	    if (isView) {
	        sliders[i].setAttribute('disabled', isView);
	    }
	}
    
	$('.keypress').on("change", function (e) {
	    var index=$(this).data("id");
	    sliders[index].noUiSlider.set($(this).val());
	    var color = 'rgb(' +
        sliders[0].noUiSlider.get().split('.')[0] + ',' +
        sliders[1].noUiSlider.get().split('.')[0] + ',' +
        sliders[2].noUiSlider.get().split('.')[0] + ')';
	    $("#result").css("background-color", color);
	    $("#result").css("color", color);
	});   

	function setColor(){
		var color = 'rgb(' +
			sliders[0].noUiSlider.get().split('.')[0] + ',' +
			sliders[1].noUiSlider.get().split('.')[0] + ',' +
			sliders[2].noUiSlider.get().split('.')[0] + ')';	
		$("#result").css("background-color",color);
		$("#result").css("color", color);       
		inputNumbers[0].value=sliders[0].noUiSlider.get().split('.')[0];
		inputNumbers[1].value=sliders[1].noUiSlider.get().split('.')[0];
		inputNumbers[2].value=sliders[2].noUiSlider.get().split('.')[0];
	}
    setColor();
}
// setup slider for composition
function setupCompositionSlider(data, isView) {

    var sliders = document.getElementsByClassName('composition-sliders');
    var inputNumbers = $('.keypress');
    var valids = $(".required-validity");
    var sv = {
        init : 100,
        start: 0,
        end:0
    }

    for (var i = 0; i < sliders.length; i++) {
        noUiSlider.create(sliders[i], {
            start: data[i].quantity,
            behaviour: "drag",
            connect: [true, false],
            orientation: "vertical",
            direction: 'rtl',
            step: 1,
            range: {
                'min': 0,
                'max': 100
            },
            pips: {
                mode: 'range',
                density: 20
            },
            id: i,
        });

        sv.init = sv.init - data[i].quantity;
        inputNumbers[i].value = data[i].quantity;
        $(sliders[i]).find(".noUi-connect")[0].style.backgroundColor = data[i].color;

        //events:   
        sliders[i].noUiSlider.on('start', function () {
            sv.start = parseInt(this.get());
        });
        sliders[i].noUiSlider.on('slide', function (value, handle) {
            var slidervalue = parseInt(value);
            var index=this.options.id;
            inputNumbers[index].value = slidervalue;
            if (slidervalue - sv.start <= sv.init) {
                $(valids[index]).hide();
            } else {
                $(valids[index]).show();
            }
        });
        sliders[i].noUiSlider.on('end', function () {
            sv.end = parseInt(this.get());
            var index=this.options.id;
            if (sv.end - sv.start <= sv.init) {
                data[index].quantity = sv.end;
                sv.init = sv.init - sv.end + sv.start;    
            } else {
                $(valids[index]).hide();
                sv.end = sv.init + sv.start;
                sv.init = 0;
                this.set(sv.end);
                inputNumbers[index].value = sv.end;
                data[index].quantity = sv.end;
            }
            var donut = morrisDonut(data, "composition-piechart");
            segmentSelect(donut, index);
            if (sv.init == 0) {
                $("#less100").hide();
            }
        });
        if (isView) {
            sliders[i].setAttribute('disabled', isView);
        }
    }
    $('.keypress').on("change", function (e) {
        var index = $(this).data("id");
        sv.start = parseInt(sliders[index].noUiSlider.get());
        sv.end = parseInt($(this).val());
        if (sv.end - sv.start <= sv.init) {
            $(valids[index]).hide();
            sliders[index].noUiSlider.set(sv.end);
            data[index].quantity = sv.end;
            sv.init = sv.init - sv.end + sv.start;
          
            var donut = morrisDonut(data, "composition-piechart");
            segmentSelect(donut, index);
            if (sv.init == 0) {
                $("#less100").hide();
            }
        } else {
            $(valids[index]).show();
            setTimeout(function () {
                $(valids[index]).hide();
                inputNumbers[index].value = sv.start;
            }, 1500);
        }
    });
}

function segmentSelect(donut,idx) {
    var index = 0;
    $.each(donut.data, function (i, item) {
        if (item.id == idx) {
            index = i;
            return false;
        }
    })
    donut.select(index);
}

function validCheck(vm) {
    var isValid = true;
    if (!vm.MaterialName || vm.MaterialName == "") {
        $("input.require-input").siblings(".required-validity").html(validMsg.isRequired);
        isValid = false;
    } else if (vm.MaterialName.length > 15) {
        $("input.require-input").siblings(".required-validity").html(validMsg.invalidInput);
        isValid = false;
    } 
    if (!vm.MaterialDescription || vm.MaterialDescription == "") {
        $("textarea.require-input").siblings(".required-validity").html(validMsg.isRequired);
        isValid = false;
    }
    return isValid;
}

function validRequiredCheck(vm) {
    var isValid = true;
    if (!vm.ProductName || vm.ProductName == "") {
        $("input.require-input.product-name").parent().parent().next().find(".required-validity.product-name").html(validMsg.isRequired);
        $("input.require-input.product-name").addClass("has-error");
        isValid = false;
    }
    if (!vm.ProductDescription || vm.ProductDescription == "") {
        $("input.require-input.product-description").parent().parent().next().find(".required-validity.product-description").html(validMsg.isRequired);
        $("input.require-input.product-description").addClass("has-error");
        isValid = false;
    }
    return isValid;
}

function showValidinfo(element, msg) {
    if (typeof element == 'string' && element == validMsg.clean) {
        $("div.valid-info").children("span").html(msg);
    } else {
        $(element).parents("div.form-inline").children("div.valid-info").children("span").html(msg);
    }
    if (msg != validMsg.noneMsg) {
        $('html, body').animate({
            scrollTop: $("#fixedClaimHeader").offset().top - 680
        }, 500);
    }
    return msg != validMsg.noneMsg;
}

function dateTimeConvert(data) {
    if (data == null) return '1/1/1950';
    var r = /\/Date\(([0-9]+)\)\//gi;
    var matches = data.match(r);
    if (matches == null) return '1/1/1950';
    var result = matches.toString().substring(6, 19);
    var epochMilliseconds = result.replace(
    /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
    '$1');
    var b = new Date(parseInt(epochMilliseconds));
    var c = new Date(b.toString());
    var curr_date = c.getDate();
    if (curr_date < 10) {
        curr_date = '0' + curr_date;
    }
    var curr_month = c.getMonth() + 1;
    if (curr_month < 10) {
        curr_month = '0' + curr_month;
    }
    var curr_year = c.getFullYear();

    var hours = c.getHours();
    var minutes = c.getMinutes();
    var second = c.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    second = second < 10 ? '0' + second : second;

    var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
    var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
    //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

    return {
        date: d,
        time: curr_time
    }
}

//OTSTM2-1042
function getPanelUrl(categoryID) {
    var url = ""
    if (categoryID == Global.EstimatedWeights.WeightCategory.SupplyEstimatedWeights.weightCategoryID) {
        url = "modal-supply-estimate-weights-detail-template.html";
    } else if (categoryID == Global.EstimatedWeights.WeightCategory.RecoveryEstimatedWeights.weightCategoryID) {
        url = "modal-recovery-estimate-weights-detail-template.html";
    }
    return url;
}

//OTSTM2-1042
function InitialSupplyEstimatedWeightSlider(Min, Max, RangeMin, RangeMax) {
    var supplyEstimatedWeightSlider = document.getElementById('supplyEstimatedWeightSlider');
    noUiSlider.create(supplyEstimatedWeightSlider, {
        start: [Min, Max],
        step: 1,
        connect: true,
        range: {
            'min': RangeMin,
            'max': RangeMax
        },
        format: {
            from: function (value) {
                return parseInt(value);
            },
            to: function (value) {
                return parseInt(value);
            }
        }
    });

    var inputNumberMin = document.getElementById('input-number-min');
    var inputNumberMax = document.getElementById('input-number-max');

    supplyEstimatedWeightSlider.noUiSlider.on('update', function (values, handle) {

        var value = values[handle];

        if (handle) {
            inputNumberMax.value = value;
        } else {
            inputNumberMin.value = value;
        }
    });

    inputNumberMin.addEventListener('change', function () {
        supplyEstimatedWeightSlider.noUiSlider.set([this.value, null]);
    });

    inputNumberMax.addEventListener('change', function () {
        supplyEstimatedWeightSlider.noUiSlider.set([null, this.value]);
    });
}
//OTSTM2-1042 
function triggerDateTimePicker(category) {
    var now = new Date();
    
    var nextMonth;
    if (now.getMonth() == 11) {
        nextMonth = new Date(now.getFullYear() + 1, 0, 1).toISOString().split('T')[0];
    } else {
        nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1).toISOString().split('T')[0];
    }
    var thisMonth = now.getFullYear() + ' ' + (now.getMonth() + 1);//zero based month counting.
    var quater = Math.round(now.getMonth() / 3 + 0.5);
    var nextQuarter = now.getFullYear() + ' ' + (quater * 3 + 1);


    //Comment out the followed code for QATEST
    $('#recoveryEffectiveStartDate').datetimepicker({
        minView: 3,
        showOn: 'focus',
        autoclose: true,
        format: "yyyy-mm",
        viewMode: "year",
        startView: 'year',
        minViewMode: "months",
        startDate: nextMonth,
        minDate: nextMonth
    });

    //Uncomment out the followed code for QATEST
    //var lastMonth = new Date(now.getFullYear() - 1, now.getMonth() + 11, 1);
    //$('#recoveryEffectiveStartDate').datetimepicker({
    //    minView: 3,
    //    showOn: 'focus',
    //    autoclose: true,
    //    format: "yyyy-mm",
    //    viewMode: "year",
    //    startView: 'year',
    //    minViewMode: "months",
    //    startDate: lastMonth,
    //    minDate: lastMonth
    //});

    $('#effectiveStartDate').on("focusout", function (e) {
        var inputDate = Date.parse(this.value);
        var now = new Date();
        var nextMonth;
        if (now.getMonth() == 11) {
            nextMonth = new Date(now.getFullYear() + 1, 0, 1);
        } else {
            nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        }
        if (isNaN(inputDate) || inputDate < nextMonth.getTime()) {
            this.value = nextMonth.toISOString().substr(0, 7);
        }
    });
}

//OTSTM2-1142 Modal responsive when resizing
$(function($) {
    $(window).resize(function () {
        if (matchMedia('(max-width: 991px)').matches && matchMedia('(min-width: 768px)').matches) {
            $(".required-validity.product-name").css("left", "-5px");
            $(".required-validity.product-description").css("left", "-5px");
            $("#rangeMin").css("right", "185px");
            $("#rangeMax").css("left", "190px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
            }
            else {
                $("#shortName").css("font-size", "45px").css("margin-left", "");
            }
        }
        else if (matchMedia('(max-width: 767px)').matches) {
            $("#rangeMin").css("right", "225px");
            $("#rangeMax").css("left", "230px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "-12px").css("font-size", "45px");
            }
            else {
                $("#shortName").css("font-size", "45px").css("margin-left", "");
            }
        }
        else {
            $(".required-validity.product-name").css("left", "");
            $(".required-validity.product-description").css("left", "");
            $("#rangeMin").css("right", "275px");
            $("#rangeMax").css("left", "280px");
            if ($("#shortName").text().length == 3) {
                $("#shortName").css("margin-left", "").css("font-size", "50px");
            }
            else {
                $("#shortName").css("font-size", "50px").css("margin-left", "20px");
            }
        }
    });  
})


