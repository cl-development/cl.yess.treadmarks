﻿$(function () {   
    $('.ckAddRole').click(function () {
        var isRoleSelected = true;
        $('input:checkbox[class=ckAddRole]').each(function () {
            if ($(this).is(":checked")) {
                isRoleSelected = false;
                return false;
            }
        });
        $('#btnadduser').prop('disabled', isRoleSelected);
    });

    $('.ckEditRole').click(function () {
        var isRoleSelected = true;
        $('input:checkbox[class=ckEditRole]').each(function () {
            if ($(this).is(":checked")) {
                isRoleSelected = false;
                return false;
            }
        });
        $('#btnEditUserNext').prop('disabled', isRoleSelected);
    });

    $('#modalAddUser').on('show.bs.modal', function () {
        clear();
    });

    $('#btnCancelUser').on("click", function () {
        clear();
    });

    function clear() {
        $('#firstname-input').val('');
        $('#lastname-input').val('');
        $('#emailaddress-input').val('');
        $('#status').text('');
        $('#validfirstname-input').text('');
        $('#validlastname-input').text('');
        $('#validemail-input').text('');
        $('input:checkbox[class=ckAddRole]').each(function () {
            $(this).prop('checked', false);
        });
    };

    var checkM = true;
    function CheckEmailExistance() {
        var email = $('#emailaddress-input').val();
        var req = { emailAddress: email }
        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.CheckEmailUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            traditional: true,
            success: function (data) {
                if (data.status !== '') {
                    $('#validemail-input').text(data.status);
                    checkM = false;
                    return false;
                }
                else {

                    checkM = true;
                    return true;
                }
            },
            error: function (xhr) {
            }
        });
    };
    $('#btnadduser').on('click', function (event) {
        var form = document.forms['frminvitation'];
        var trigger = $(event.relatedTarget);
        jQuery.ajaxSetup({ async: false });

        if (!$(form).valid()) {
            return false;
        }

        //if form is valid then check if email is unique
        var checkEmail = CheckEmailExistance();
        if (checkM == false) {
            return false;
        }
        if ($(form).valid()) {
            $('#modalAddUser').modal('hide');
            $('#modalAddUserSendInviteConfirm').modal('show');
            return true;
        };
        return false;
    });

    $('#btncanceluser').on("click", function () {
        clear();
    });

    $('#modalAddUserSendInviteConfirm').on('show.bs.modal', function (event) {
        var firstname = $("#firstname-input").val() + ' ' + $("#lastname-input").val();
        $(this).find('#dynamicname').text(firstname);
        $(this).find('#dynamicemail').text($("#emailaddress-input").val());

    });

    $('#btnadduserconfirm').on('click', function () {
        var firstname = $('#firstname-input').val();
        var lastname = $('#lastname-input').val();
        var email = $('#emailaddress-input').val();
        var isread = $('#ckRead').is(':checked');
        var iswrite = $('#ckWrite').is(':checked');
        var issubmit = $('#ckSubmit').is(':checked');
        var isparticipantadmin = $('#ckParticipantAdmin').is(':checked');

        var req = {
            firstName: firstname, lastName: lastname, emailAddress: email,
            isread: isread, iswrite: iswrite, issubmit: issubmit, isparticipantadmin: isparticipantadmin
        }
        var form = document.forms['frminvitation'];
        if (!$(form).valid()) {
            return;
        }
        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.CreateInvitationUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    return false;
                }
                else {
                    clear();
                    window.location.reload(true);
                }
            },
            error: function (xhr) {
                //  alert(xhr.status + ' ' + xhr.statusText);
                window.location.reload(true);
                clear();
            }
        });
    });

    var pageReadonly = function () {
        //OTSTM2-882
        //var isPageReadonly = Global.InvitationIndex.PageReadOnly.toLowerCase();
        var isPageReadonly = Global.InvitationIndex.StaffParticipantUserPageReadOnly.toLowerCase();
        return isPageReadonly == 'true';
    }

    //global variable
    var selectedUser;
    var pageSize = 7; //OTSTM2-1013, fix "More" existing issue
    //Define data table
    var table = $('#tbluserlist').DataTable({
        info: false,
        processing: false,
        serverSide: true,
        ajax: Global.InvitationIndex.GetListHandlerUrl,
        deferRender: true,
        dom: "rtiS",
        scrollY: 300,
        "sScrollX": "100%",
        "sScrollXInner": "110%",
        scrollCollapse: true,
        searching: true,
        ordering: true,
        order: [[4, "desc"]],
        scroller: {
            displayBuffer: 100,
            rowHeight: 70,
            serverWait: 100,
            loadingIndicator: false
        },

        columns: [
            {
                name: "firstName",
                width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + data.FirstName + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                name: "lastName",
                width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + data.LastName + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                className:"inviteEmail",
                name: "inviteEmail",
                width: "30%",
                data: null,
                render: function (data, type, full, meta) {
                    return data.InviteEmail;
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                name: "invitedDate", //OTSTM2-1013, for existing issue, consistent with back end name
                width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return "<span>" + dateTimeConvert(data.InviteSendDate) + "</span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                name: "displayStatus", //OTSTM2-1013, for existing issue, consistent with back end name
                className: "td-center",
                width: "13%",
                orderable: true,
                data: null,
                render: function (data, type, full, meta) {
                    if (data.DisplayStatus == "Inactive") {
                        return "<div class='panel-table-status color-tm-red-bg'>Inactive</div>";
                    }
                    if (data.DisplayStatus == "Active") {
                        return "<div class='panel-table-status color-tm-green-bg'>Active</div>";
                    }
                    if (data.DisplayStatus == "Invite Expired") {
                        return "<div class='panel-table-status color-tm-gray-light-bg'>Invite Expired</div>";
                    }
                    if (data.DisplayStatus == "Invite Sent") {
                        return "<div class='panel-table-status color-tm-blue-bg'>Invite Sent</div>";
                    }
                    //OTSTM2-1013
                    if (data.DisplayStatus == "Locked") {
                        return "<div class='panel-table-status color-tm-yellow-bg'>Locked</div>";
                    }
                    return "<span></span>";
                },
                createdCell: function (td, cellData, rowData, row, col) {
                    statusClickEvent(rowData, td)
                }
            },
            {
                className: "td-center",
                width: "12%",
                orderable: false,
                data: null,
                render: function (data, type, full, meta) {
                    if (!pageReadonly()) {
                        //fix for safari
                        var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex='" + data.ID + "'" : "";
                        var content = "<div class='btn-group dropdown-menu-actions'><a href='javascript:void(0)' tabindex='" + data.ID + "' data-trigger='focus' data-toggle='popover'" +
                            " id='actions" + meta.row + "' data-original-title='' title=''><i class='glyphicon glyphicon-cog'></i></a><div class='popover-menu'><ul>";
                        if ((data.DisplayStatus == "Invite Sent") || (data.DisplayStatus == "Invite Expired")) {
                            content += " <li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalAddUserResendInviteConfirm' data-rowId='" + meta.row + "'><i class='fa fa-envelope-o'></i> Re-send Invite</a></li>" +
                                " </ul> </div> </div>";
                        }
                        if (data.DisplayStatus == "Active") {
                            content += "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalEditUser' data-rowId='" + meta.row + "'><i class='fa fa-pencil-square-o'></i> Edit </a></li>" +
                                " <li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalInactivateUser' data-rowId='" +meta.row + "'><i class='fa fa-times'></i> Inactivate</a></li>" +
                                " </ul> </div> </div>";
                        }
                        if (data.DisplayStatus == "Inactive") {
                            content += "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalActivateUser' data-rowId='" + meta.row + "'><i class='fa fa-check'></i> Activate</a></li>" +
                              " </ul> </div> </div>";
                        }
                        //OTSTM2-1013
                        if (data.DisplayStatus == "Locked") {
                            content += "<li><a href='javascript:void(0)' data-toggle='modal' data-target='#modalUnlockUser' data-rowId='" + meta.row + "'><i class='fa fa-unlock-alt'></i> Unlock</a></li>" +
                              " </ul> </div> </div>";
                        }
                        return content;
                    }
                    else
                    {
                        return "";
                    }
                }
            },
            {
                data: null,
                className: "display-none",
                render: function (data, type, full, meta) {
                    return data.RowversionString;
                }
            }
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') : $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#found').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() > pageSize) ? $('#viewmore').css('visibility', 'visible') : $('#viewmore').css('visibility', 'hidden'); //OTSTM2-1013, existing issue, cannot be ">="
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#Search').val();
            var url = Global.InvitationIndex.ExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue);
            $('#export').attr('href', url);
        }
    });

    table.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                return $($(this)).siblings(".popover-menu").html();
            },
        });
    });
    function statusClickEvent(data, td) {
        if (data.DisplayStatus.toLowerCase() == "active" || data.DisplayStatus.toLowerCase() == "inactive" || data.DisplayStatus.toLowerCase() == "locked") {
            $(td).prop("style", "cursor:pointer");
            $(td).on("click", function (event) {
                fillUserData(data.UserEmail);
                $("#modalViewUser").modal("show");
            });
        }
    }
    function fillUserData(email) {
        $.ajax({
            type: 'GET',
            url: Global.InvitationIndex.LoadUserDetailsViewUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { emailAddress: email },
            traditional: true,
            success: function (result) {
                if (result.Status !== '') {  
                    $('#viewSpanEmail').html(result.EmailAddress);
                    $('#viewfirstname-input').val(result.FirstName);
                    $('#viewlastname-input').val(result.LastName);
                    $('#viewckread').prop('checked', result.IsRead);
                    $('#viewckwrite').prop('checked', result.IsWrite);
                    $('#viewcksubmit').prop('checked', result.IsSubmit);
                    $('#viewckparticipantadmin').prop('checked', result.IsParticipantAdmin);                  
                }
            },
            error: function (result) {
            }
        });
    }

    $('#viewmore').on('click', function () {
        $('.dataTables_scrollBody').css({ 'overflow': 'auto', 'overflow-y': 'scroll' });
        $('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $(this).hide();
    });

    //OTSTM2-1013, fix existing issue (no "Found x", no remove icon)
    $('#Search').on('keydown', function (e) {
        if (e.keyCode == 13) //End Home
        { e.preventDefault(); }
    });

    var myTimer;

    $('#Search').on('keyup', function (e) {
        //$('#export').attr('href', url);
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36) || (e.keyCode === 13)) //End Home
        { return; }
        var str = $(this).val();

        clearTimeout(myTimer);
        myTimer = setTimeout(searchIt, 500, str);
    });

    var searchIt = function (str) {
        if (str.length >= 1) {
            $("#Search").css('visibility', 'visible');
            $('#found').css('display', 'block');
            $('#found').siblings('.remove-icon').show();
            table.search(str).draw(false);
        }
        else {
            $('#found').siblings('.remove-icon').hide();
            $('#found').css('display', 'none');
            table.search(str).draw(false);
        }
    }

    $('#glyphicon-search-Rem').on('click', function () {
        var searchValue = $('#Search').val();
        table.search(searchValue).draw(false);
    });
    //on clear field click
    $('.remove-icon').click(function () {
        $(this).siblings("input").val("");
        $(this).hide();
        $('#found').css('display', 'none');
        table.search("").draw(false);
    });

    //existing issue, comment out for OTSTM2-1013
    //var hdSearchText = $('#hdSearchText').val();
    //if (hdSearchText != null && hdSearchText) {
    //    $('#search').val(hdSearchText);
    //    table.search(hdSearchText).draw(false);
    //}

    //$('#search').on('keyup', function () {
    //    table.search(this.value).draw(false);
    //});

    //$('#removeSearch').on('click', function () {
    //    table.search("").draw(false);
    //    $('#found').hide();
    //});

    //on resend show
    $('#modalAddUserResendInviteConfirm').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        CurrentUser = table.row(rowId).data();
        $(this).find('#spanFullName').html(CurrentUser.FirstName + ' ' + CurrentUser.LastName);
        $(this).find('#spanEmail').html(CurrentUser.InviteEmail);
    });

    //call resend web service
    $('#btnResendEmail').on('click', function () {
        var email = CurrentUser.InviteEmail;
        var req = { email: email }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.ResendInviteUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    $('#ajaxstatus').text(data.status);
                }
                else {
                    window.location.reload(true);
                }
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });

    //on activate show
    $('#modalActivateUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        CurrentUser = table.row(rowId).data();
        $(this).find('#spanFullName').html(CurrentUser.FirstName + ' ' + CurrentUser.LastName);
    });

    //call Activate Web Service
    $('#buttonActivateUser').on('click', function () {
        var userId = CurrentUser.UserId;
        var req = { userId: userId }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.ActiveUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                window.location.reload(true);
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });


    //on inactivate show		
    $('#modalInactivateUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        CurrentUser = table.row(rowId).data();

        $(this).find('#spanFullName').html(CurrentUser.FirstName + ' ' + CurrentUser.LastName);
    });

    //call inactivate web service
    $('#buttonInactivateUser').on('click', function () {
        var userId = CurrentUser.UserId;
        var req = { userId: userId}

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.InactiveUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                window.location.reload(true);
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });

    //OTSTM2-1013 unlock user
    $('#modalUnlockUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        CurrentUser = table.row(rowId).data();
        $(this).find('#unlockuser').text(CurrentUser.InviteEmail);
    });

    $('#btnunlockuser').on("click", function () {
        var userId = CurrentUser.UserId;
        var req = { userId: userId }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.UnlockParticipantUserUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                table.search($('#Search').val()).draw(false); 
            },
            error: function (xhr) {
                $('#ajaxstatus').text(xhr.status + ' ' + xhr.statusText);
            }
        });
    });

    //on edit show
    $('#modalEditUser').on('show.bs.modal', function (event) {
        var invoker = $(event.relatedTarget);
        var rowId = $(invoker).attr('data-rowId');
        CurrentUser = table.row(rowId).data();
        var email = CurrentUser.InviteEmail;
        $(this).find('#editSpanEmail').html(CurrentUser.InviteEmail);
        $(this).find('#editfirstname-input').val(CurrentUser.FirstName);
        $(this).find('#editlastname-input').val(CurrentUser.LastName);
        $.ajax({
            type: 'GET',
            url: Global.InvitationIndex.LoadUserDetailsUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { emailAddress: [email] },
            traditional: true,
            success: function (result) {
                if (result.Status !== '') {
                    $('#editckread').prop('checked', result.IsRead);
                    $('#editckwrite').prop('checked', result.IsWrite);
                    $('#editcksubmit').prop('checked', result.IsSubmit);
                    $('#editckparticipantadmin').prop('checked', result.IsParticipantAdmin);
                    CurrentUser = result;
                }
            },
            error: function (result) {
            }
        });
    });

    //populate confirm message
    $('#btnEditUserNext').on('click', function () {
        $('#edituserfrom').text(CurrentUser.FirstName + ' ' + CurrentUser.LastName);
        $('#edituserfrom1').text(CurrentUser.FirstName + ' ' + CurrentUser.LastName);
        $('#fedituserstatus').text('');
        $('#ledituserstatus').text('');
        var firstname = $('#editfirstname-input').val().trim();
        var lastname = $('#editlastname-input').val().trim();

        if (firstname == '' && lastname == '') {
            $('#fedituserstatus').text(Global.InvitationIndex.Resources.FirstNameRequired);
            $('#ledituserstatus').text(Global.InvitationIndex.Resources.LastNameRequired);
            return false;
        }
        if (firstname == '') {
            $('#fedituserstatus').text(Global.InvitationIndex.Resources.FirstNameRequired);
            return false;
        }
        if (lastname == '') {
            $('#ledituserstatus').text(Global.InvitationIndex.Resources.LastNameRequired);
            return false;
        }
        //fill to information
        $('#edituserto').text(firstname + ' ' + lastname);
        $('#edituserto1').text(firstname + ' ' + lastname);
        return ShowConfirmationMessage();
    });

    function ShowConfirmationMessage() {
        var firstname = $('#editfirstname-input').val();
        var lastname = $('#editlastname-input').val();

        var isread = $('#editckread').is(':checked');
        var iswrite = $('#editckwrite').is(':checked');
        var issubmit = $('#editcksubmit').is(':checked');
        var isparticipantadmin = $('#editckparticipantadmin').is(':checked');

        var isUserNameChanged = false;
        var isRoleChanged = false;
        if ((firstname != CurrentUser.FirstName) || (lastname != CurrentUser.LastName)) {
            isUserNameChanged = true;
        }
        if ((isread != CurrentUser.IsRead) ||
            (iswrite != CurrentUser.IsWrite) ||
            (issubmit != CurrentUser.IsSubmit) ||
            (isparticipantadmin != CurrentUser.IsParticipantAdmin)) {
            isRoleChanged = true;
        }

        if ((isUserNameChanged) && (!isRoleChanged)) {
            $('#editUserConfirmMsg1').show();
            $('#editUserConfirmMsg2').hide();
            $('#editUserConfirmMsg3').hide();
        }
        if ((!isUserNameChanged) && (isRoleChanged)) {
            $('#editUserConfirmMsg2').show();
            $('#editUserConfirmMsg1').hide();
            $('#editUserConfirmMsg3').hide();
        }
        if ((isUserNameChanged) && (isRoleChanged)) {
            $('#editUserConfirmMsg3').show();
            $('#editUserConfirmMsg1').hide();
            $('#editUserConfirmMsg2').hide();
        }
        if ((!isUserNameChanged) && (!isRoleChanged)) {
            return false;
        }
        return true;
    }

    //call edit web service
    $('#buttonEditUser').on('click', function () {
        var firstname = $('#editfirstname-input').val();
        var lastname = $('#editlastname-input').val();
        var email = CurrentUser.EmailAddress;
        var rowversionstring = CurrentUser.RowversionString;

        var isread = $('#editckread').is(':checked');
        var iswrite = $('#editckwrite').is(':checked');
        var issubmit = $('#editcksubmit').is(':checked');
        var isparticipantadmin = $('#editckparticipantadmin').is(':checked');

        var req = {
            firstName: firstname, lastName: lastname, emailAddress: email,
            isread: isread, iswrite: iswrite, issubmit: issubmit, isparticipantadmin: isparticipantadmin, rowversionstring: rowversionstring
        }

        $.ajax({
            type: 'POST',
            url: Global.InvitationIndex.EditUserUrl,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(req),
            success: function (data) {
                if (data.status !== '') {
                    localStorage.setItem("error", data.status);
                    window.location.reload(true);
                } else {
                    localStorage.removeItem('error');
                    window.location.reload(true);
                }
            },
            error: function (xhr) {
            }
        });
    });

    var dateTimeConvert = function (data) {
        if (data == null) return '1/1/1950';
        if (data.indexOf('-62135578800000') > -1) return '';

        var r = /\/Date\(([0-9]+)\)\//gi;
        var matches = data.match(r);
        if (matches == null) return data.substring(0, 10);
        var result = matches.toString().substring(6, 19);
        var epochMilliseconds = result.replace(
        /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
        '$1');
        var b = new Date(parseInt(epochMilliseconds));
        var c = new Date(b.toString());
        var curr_date = c.getDate();
        if (curr_date < 10) {
            curr_date = '0' + curr_date;
        }
        var curr_month = c.getMonth() + 1;
        if (curr_month < 10) {
            curr_month = '0' + curr_month;
        }
        var curr_year = c.getFullYear();
        //var curr_h = c.getHours();
        //var curr_m = c.getMinutes();
        //var curr_s = c.getSeconds();
        //var curr_offset = c.getTimezoneOffset() / 60;
        var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
        return d;
    }
})