﻿using MicrosoftSystem = System.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SystemIO = System.IO;
using MicrosoftWeb = System.Web;
using MicrosoftIO = System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CL.Framework.Logging;
using CL.TMS.Common;
using CL.TMS.DataContracts.ViewModel;
using CL.TMS.ServiceContracts.StewardServices;
using CL.TMS.UI.Common;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.Common.Enum;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Steward;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Common.FileUpload;
using CL.TMS.DataContracts.ViewModel.Hauler;
using Newtonsoft.Json;
using CL.TMS.Resources;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.Helper;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.DataContracts.ViewModel.Common.WorkFlow;
using CL.TMS.DataContracts.Adapter;
using System.Text;
using CL.TMS.DataContracts.ViewModel.Registrant;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security.Authorization;
using CL.TMS.Configuration;
using CL.TMS.ExceptionHandling;
using SelectPdf;
using System.Configuration;
using CL.TMS.ServiceContracts.CompanyBrandingServices;
using CL.TMS.UI.Common.UserInterface;
using CL.TMS.DataContracts.ViewModel.System;

namespace CL.TMS.Web.Areas.Steward.Controllers
{
    [AllowAnonymous]
    public class RegistrationController : BaseController
    {
        #region Fields
        private IStewardRegistrationService stewardRegistrationService;
        private IStewardRegistrationInvitationService applicationInvitationService; //IApplicationInvitationService        
        private IApplicationService applicationService; //IApplicationService applicationService;
        private IUserService userService;
        private IRegistrantService registrantService;
        private IFileUploadService fileUploadService;
        private ICompanyBrandingServices companyBrandingService;
        private IMessageService messageService;
        private string inviteUrl;
        private string uploadRepositoryPath;
        #endregion

        #region Constructors
        public RegistrationController(IStewardRegistrationInvitationService applicationInvitationService, IStewardRegistrationService StewardRegistrationService,
            IUserService userService, IApplicationService applicationService, IRegistrantService registrantService, IFileUploadService fileUploadService, ICompanyBrandingServices companyBrandingService,
            IMessageService messageService)
        {
            this.stewardRegistrationService = StewardRegistrationService;
            this.applicationInvitationService = applicationInvitationService;
            this.applicationService = applicationService;
            this.registrantService = registrantService;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.companyBrandingService = companyBrandingService;
            this.uploadRepositoryPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath"); //for QA/UAT/Prod
            //this.uploadRepositoryPath = "\\\\CL02268-PC\\Logo"; //for localhost
            this.messageService = messageService;
        }
        #endregion

        #region Action Methods

        [AllowAnonymous]
        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsStewardApplication)]
        [HttpGet]
        public ActionResult Index(Guid id)
        {
            if (SiteSettings.Instance.ApplicationInstance == "PRO")
                ViewBag.ngApp = "vendorRegistrationGlobalApp";
            StewardRegistrationModel model = stewardRegistrationService.GetByTokenId(id);
            ViewBag.Id = id;

            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }

            int appID = applicationInvitationService.GetByTokenID(id).ApplicationID.Value;
            ApplicationStatusEnum status = (ApplicationStatusEnum)Enum.Parse(typeof(ApplicationStatusEnum), applicationService.GetByApplicationID(appID).Status);

            if ((status == ApplicationStatusEnum.Open) || (status == ApplicationStatusEnum.BackToApplicant))
            {
                if ((model.ExpireDate != null) && (model.ExpireDate < DateTime.UtcNow))
                {
                    return RedirectToAction("InvitationExpired", "AppInvitation", new { area = "System" });
                }
            }

            if (status == ApplicationStatusEnum.Approved
                || status == ApplicationStatusEnum.Completed
                || status == ApplicationStatusEnum.BankInformationSubmitted
                || status == ApplicationStatusEnum.BankInformationApproved
                )
            {
                return RedirectToAction("CustomerIndex", "Registration", new { cId = model.VendorID });
            }
            else
            {
                #region read from form object
                #region prepare Model

                TempData["SubTypes"] = AppDefinitions.Instance.TypeDefinitions["StewardType"].ToList();

                if ((null == model.ContactInformationList) || (model.ContactInformationList.Count == 0))
                {
                    model.ContactInformationList = new List<ContactRegistrationModel>() { };
                    model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
                }
                if (null == model.ContactInformationList[0].ContactAddress)
                {
                    model.ContactInformationList[0].ContactAddress = new ContactAddressModel();
                }
                if (model.TireDetails == null)
                {
                    model.TireDetails = stewardRegistrationService.GetAllItemsList().TireDetails;
                }
                //Business Activity
                var temp = stewardRegistrationService.GetBusinessActivities((int)BusinessActivityTypeEnum.Steaward).ToList();
                var List = new List<SelectListItem>();
                foreach (var element in temp)
                {
                    List.Add(new SelectListItem
                    {
                        Value = element.Name,
                        Text = element.Name
                    });
                }
                model.StewardDetails.BusinessActivites = List;
                if (model.SupportingDocuments == null)
                {
                    model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Applications };
                }
                switch (status)
                {
                    case ApplicationStatusEnum.Approved:
                    case ApplicationStatusEnum.BankInformationSubmitted:
                    case ApplicationStatusEnum.BankInformationApproved:
                    case ApplicationStatusEnum.Completed:
                        model.SupportingDocuments.ID = model.VendorID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetCustomerAttachments(model.VendorID).Count();
                        model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Customer;
                        break;
                    default:
                        model.SupportingDocuments.ID = model.ID;
                        model.SupportingDocuments.CountOfUploadedDocuments = GetAttachments(model.ID).Count();
                        break;
                }

                model.TireDetails.Status = model.Status;
                model.BusinessLocation.Status = model.Status;
                foreach (var contact in model.ContactInformationList)
                {
                    contact.Status = model.Status;
                }

                model.StewardDetails.Status = model.Status;
                model.TermsAndConditions.Status = model.Status;
                model.TermsAndConditions.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("steward") > -1).Name;
                //OTSTM2-973 TermsAndConditions
                model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

                model.SupportingDocuments.ParticipantType = DataLoader.ParticipantTypes.FirstOrDefault(x => x.Name.ToLower().IndexOf("steward") > -1).Name;
                model.SupportingDocuments.Status = model.Status;

                model.RegistrantActiveStatus = model.RegistrantActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantActiveStatus;
                model.RegistrantInActiveStatus = model.RegistrantInActiveStatus == null ? new RegistrantStatusChange() : model.RegistrantInActiveStatus;

                #endregion

                var user = TMSContext.TMSPrincipal;

                if (user.Identity.IsAuthenticated)
                {
                    if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                    {
                        if (model.VendorID != 0 &&
                            (status == ApplicationStatusEnum.Approved
                            || status == ApplicationStatusEnum.Completed
                            || status == ApplicationStatusEnum.BankInformationSubmitted
                            || status == ApplicationStatusEnum.BankInformationApproved
                            ))
                        {
                            var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForCustomer(model.VendorID);
                            var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForCustomer(model.VendorID, false);
                            var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForCustomer(model.VendorID, true);

                            model.ActiveInactive.RegistrantActive = previousActiveHistory;
                            model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                            model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                            if (model.ActiveInactive.RegistrantInactive == null)
                            {
                                model.ActiveInactive.RegistrantInactive = new CustomerActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }
                            if (model.ActiveInactive.RegistrantActive == null)
                            {
                                model.ActiveInactive.RegistrantActive = new CustomerActiveHistory()
                                {
                                    ActiveStateChangeDate = DateTime.MinValue
                                };
                            }

                            model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                            model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                            model.ActiveInactive.Status = model.Status;
                        }
                        //OTSTM2-630, use the common code about Claim Activity             
                        ViewBag.ClaimsId = appID;
                        if (SecurityContextHelper.IsStaff())
                        {
                            ViewBag.HasActivityPanel = 1;
                        }
                        return View("StaffIndex", model);
                    }
                    return View("ParticipantIndex", model);
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["IsPdf"]))
                {
                    return View("StaffIndexForPDF", model);

                }
                if (null != applicationService)
                {
                    switch (
                        (ApplicationStatusEnum)
                            Enum.Parse(typeof(ApplicationStatusEnum),
                                applicationService.GetByApplicationID(appID).Status))
                    {
                        case ApplicationStatusEnum.Open:
                        case ApplicationStatusEnum.BackToApplicant:
                            break;
                        case ApplicationStatusEnum.Submitted:
                        case ApplicationStatusEnum.Approved:
                        case ApplicationStatusEnum.BankInformationSubmitted:
                        case ApplicationStatusEnum.BankInformationApproved:
                        case ApplicationStatusEnum.Completed:
                        case ApplicationStatusEnum.OnHold:
                        case ApplicationStatusEnum.Denied:
                        case ApplicationStatusEnum.Assigned:
                        default:
                            return RedirectToAction("ApplicationSubmittedPage", "Default", new { area = "" });
                    }
                }

                return View("ParticipantIndex", model);

                #endregion
            }
        }

        public ActionResult GeneratePDF(Guid id)
        {
            byte[] docBytes = null;
            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {
                if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                {
                    HtmlToPdf converter = new HtmlToPdf();
                    PdfDocument doc = converter.ConvertUrl(SiteSettings.Instance.GetSettingValue("Settings.DomainName") + "Steward/Registration/Index/" + id + "?IsPdf=true");
                    MemoryStream outStream = new MemoryStream();
                    doc.Save(outStream);
                    docBytes = outStream.ToArray();
                    doc.Save(Server.MapPath("~/SamplePdf.pdf"));
                    string filename = "application_" + DateTime.Now.ToFileTime() + ".pdf";
                    return File(docBytes, "application/pdf", filename);
                }
            }
            return RedirectToAction("NotFound");
        }

        [ClaimsAuthorizationAttribute(TreadMarksConstants.ApplicationsStewardApplication)]
        [HttpGet]
        public ActionResult CustomerIndex(int cId)
        {
            if (SiteSettings.Instance.ApplicationInstance == "PRO")
                ViewBag.ngApp = "vendorRegistrationGlobalApp";
            StewardRegistrationModel model = stewardRegistrationService.GetApprovedApplication(cId);
            model.VendorID = cId;
            ViewBag.SelectedMenu = "Applications";

            if (model == null)
            {
                ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
                TempData["ViewData"] = ViewData;
                return RedirectToAction("NotFound");
            }

            //OTSTM2-427: set up global search  
            var vendor = registrantService.GetVendorById(cId);
            if (vendor != null)
            {
                //Keep it in session
                if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 1)
                {
                    Session["SelectedVendor"] = vendor;
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }
            else
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            #region prepare Model

            TempData["SubTypes"] = AppDefinitions.Instance.TypeDefinitions["StewardType"].ToList();

            if ((null == model.ContactInformationList) || (model.ContactInformationList.Count == 0))
            {
                model.ContactInformationList = new List<ContactRegistrationModel>() { };
                model.ContactInformationList.Add(new ContactRegistrationModel() { ContactAddress = new ContactAddressModel() });
            }
            if (null == model.ContactInformationList[0].ContactAddress)
            {
                model.ContactInformationList[0].ContactAddress = new ContactAddressModel();
            }
            if (model.TireDetails == null)
            {
                model.TireDetails = stewardRegistrationService.GetAllItemsList().TireDetails;
            }
            //Business Activity
            var temp = stewardRegistrationService.GetBusinessActivities((int)BusinessActivityTypeEnum.Steaward).ToList();
            var List = new List<SelectListItem>();
            foreach (var element in temp)
            {
                List.Add(new SelectListItem
                {
                    Value = element.Name,
                    Text = element.Name
                });
            }
            model.StewardDetails.BusinessActivites = List;
            if (model.SupportingDocuments == null)
            {
                model.SupportingDocuments = new SupportingDocumentsRegistrationModel() { FileUploadSectionName = FileUploadSectionName.Customer };
            }
            model.SupportingDocuments.ID = cId;
            model.SupportingDocuments.CountOfUploadedDocuments = GetCustomerAttachments(cId).Count();
            model.SupportingDocuments.FileUploadSectionName = FileUploadSectionName.Customer;

            model.TireDetails.Status = model.Status;
            model.BusinessLocation.Status = model.Status;
            foreach (var contact in model.ContactInformationList)
            {
                contact.Status = model.Status;
            }

            model.StewardDetails.Status = model.Status;
            model.TermsAndConditions.Status = model.Status;
            model.TermsAndConditions.ParticipantType = "Steward";
            //OTSTM2-973 TermsAndConditions
            model.TermsAndConditions.TermAndConditionContent = companyBrandingService.LoadTermsAndConditionContent(model.TermsAndConditions.TermsAndConditionsID, model.TermsAndConditions.ParticipantType);

            model.SupportingDocuments.ParticipantType = "Steward";
            model.SupportingDocuments.Status = model.Status;

            #endregion

            var user = TMSContext.TMSPrincipal;

            if (user.Identity.IsAuthenticated)
            {
                if ((SecurityContextHelper.IsStaff()) || (SecurityContextHelper.CurrentVendor != null))//or is participant user
                {
                    var currentActiveHistory = registrantService.LoadCurrenActiveHistoryForCustomer(model.VendorID);
                    var previousInactiveHistory = registrantService.LoadPreviousActiveHistoryForCustomer(model.VendorID, false);
                    var previousActiveHistory = registrantService.LoadPreviousActiveHistoryForCustomer(model.VendorID, true);

                    model.ActiveInactive.RegistrantActive = previousActiveHistory;
                    model.ActiveInactive.RegistrantInactive = previousInactiveHistory;
                    model.ActiveInactive.CurrentActiveHistory = currentActiveHistory;

                    if (model.ActiveInactive.RegistrantInactive == null)
                    {
                        model.ActiveInactive.RegistrantInactive = new CustomerActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }
                    if (model.ActiveInactive.RegistrantActive == null)
                    {
                        model.ActiveInactive.RegistrantActive = new CustomerActiveHistory()
                        {
                            ActiveStateChangeDate = DateTime.MinValue
                        };
                    }

                    model.ActiveInactive.RegistrantActive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantActive.CreateDate);
                    model.ActiveInactive.RegistrantInactive.CreateDate = ConvertTimeFromUtc(model.ActiveInactive.RegistrantInactive.CreateDate);
                    model.ActiveInactive.Status = model.Status;

                    //OTSTM2-630, use the common code about Claim Activity             
                    ViewBag.ClaimsId = vendor.ApplicationID;
                    if (SecurityContextHelper.IsStaff())
                    {
                        ViewBag.HasActivityPanel = 1;
                    }

                    return View("StaffIndex", model);
                }
            }

            ModelState.AddModelError("", MessageResource.RegistrationController_ApplicationIdNotFound);
            TempData["ViewData"] = ViewData;
            return RedirectToAction("NotFound");
        }

        [AllowAnonymous]
        public ActionResult NotFound()
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            return View();

        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SaveModel(StewardRegistrationModel model)
        {
            try
            {
                var IsStewardAutoSaveDisabled = false;

                if (SecurityContextHelper.IsStaff())
                {
                    var user = TMSContext.TMSPrincipal;
                    var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Steward")).ToList();
                    IsStewardAutoSaveDisabled = ApplicationHelper.IsStewardAutoSaveDisabled(rolePermissionsClaims);
                }

                if (!IsStewardAutoSaveDisabled)
                {
                    PopulateTireDetails(model);

                    //OTSTM2-50 fix existing issue that check the following checkbox at the first time cannot be remembered after refreshing page
                    model.ContactInformationList.ToList().ForEach(c =>
                    {
                        if (c.ContactAddressSameAsBusinessAddress == false && c.ContactInformation.ContactAddressSameAsBusiness == true)
                        {
                            c.ContactAddressSameAsBusinessAddress = true;
                            c.ContactInformation.ContactAddressSameAsBusiness = false;
                        }
                    });

                    model.UserIPAddress = ("::1" == MicrosoftWeb.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftWeb.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow; //Use UTC time to avoid DateTime Serialze issue in JavaScriptSerializer().Serialize()                     
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI
                    int appID = model.ID;
                    string updatedBy = MicrosoftWeb.HttpContext.Current.User.Identity.Name;
                    stewardRegistrationService.UpdateUserExistsinApplication(model, appID, updatedBy);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Update Steward registration form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult StewardApplicationParticipantResults(StewardRegistrationModel model)
        {
            throw new NotImplementedException();

            //try
            //{
            //    if (model.TireDetails != null)
            //    {
            //        if (model.TireDetails.TireDetailsItemType != null)
            //            model.TireDetails.TireDetailsItemType = TireDetailsItemType(model).ToList();
            //        else
            //            model.TireDetails.TireDetailsItemType = new List<Item>();
            //    }
            //    else model.TireDetails = new TireDetails() { TireDetailsItemType = new List<Item>() };

            //    model.TireDetails.TireDetailsItemType = TireDetailsItemType(model).ToList();
            //    var json = JsonConvert.SerializeObject(model);
            //    int appID = model.ID;
            //    string updatedBy = HttpContext.User.Identity.Name;
            //    StewardRegistrationService.UpdateUserExistsinApplication(json, appID, updatedBy);

            //    return Json("Success", JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception ex)
            //{
            //    return Json(ex.Message, JsonRequestBehavior.AllowGet);
            //}
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SubmitApplication(int applicationId, Guid tokenId)
        {
            ApplicationInvitation appInvitation = applicationInvitationService.GetByTokenID(tokenId);
            if (appInvitation == null)
            {
                return new HttpStatusCodeResult(401);
            }

            this.stewardRegistrationService.SetApplicationStatus(applicationId, ApplicationStatusEnum.Submitted, null, null);

            string logContent = "<strong>Applicant submitted</strong> the application";
            this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Steward, logContent, applicationId);

            return new HttpStatusCodeResult(200);
        }

        public IList<Item> TireDetailsItemType(StewardRegistrationModel model)
        {

            //This method formats the TireDetailsItems list and creates an item model to populate
            IList<string> tiredetailsItemType = model.TireDetailsItemTypeString;
            var tireDetailsResult = new List<Item>();
            string name, shortname;
            int id;
            bool isChecked;

            foreach (string item in tiredetailsItemType)
            {
                string[] split = item.Split(':');
                name = split[0];
                shortname = split[1];
                Int32.TryParse(split[split.Length - 1], out id);

                if (name.Contains('_'))
                {
                    //chb_name tries to split by _ and get the final portion
                    string[] namesplit = name.Split('_');
                    name = namesplit[namesplit.Length - 1];
                }
                Boolean.TryParse(split[split.Length - 2], out isChecked);
                tireDetailsResult.Add(new Item() { Name = name, ShortName = shortname, ID = id });
            }
            return tireDetailsResult;
        }
        #endregion

        private IEnumerable<AttachmentModel> GetAttachments(int applicationId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (applicationId > 0)
            {
                attachments = this.fileUploadService.GetApplicationAttachments(applicationId);
            }
            return attachments;
        }

        private IEnumerable<AttachmentModel> GetCustomerAttachments(int customerId)
        {
            IEnumerable<AttachmentModel> attachments = null;
            if (customerId > 0)
            {
                attachments = this.fileUploadService.GetCustomerAttachments(customerId);
            }
            return attachments;
        }

        private void DeleteAttachment(int applicationId, string fileUniqueName)
        {
            var user = TMSContext.TMSPrincipal.Identity;

            string deletedBy = user.IsAuthenticated ? user.Name : "Online User";

            AttachmentModel attachment = this.fileUploadService.GetApplicationAttachment(applicationId, fileUniqueName);

            if (attachment.UniqueName.Equals(fileUniqueName.Trim()))
            {
                string fileUploadPath = SiteSettings.Instance.GetSettingValue("Settings.FileUploadRepositoryPath");
                string fileFullPath = Path.Combine(fileUploadPath, attachment.FilePath);

                FileUploadHandler.DeleteUploadedFile(fileFullPath);
                this.fileUploadService.RemoveApplicationAttachment(applicationId, fileUniqueName, deletedBy);
            }
            else
            {
                LogManager.LogExceptionWithMessage("The File ID does not match the specified File Name", new InvalidOperationException());
            }

        }

        private bool ComposeAndSendApplicationApprovalEmail(ApplicationEmailModel model)
        {

            Email.Email emailer = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                         SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                         SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                         SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                         SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                         SiteSettings.Instance.GetSettingValue("Company.Email"),
                         Convert.ToBoolean(SiteSettings.Instance.GetSettingValue("Email.useSSL")));
            MailAddress from = new MailAddress(SiteSettings.Instance.GetSettingValue("Company.Email"));
            MailAddress to = new MailAddress(model.Email);
            MailMessage message = new MailMessage(from, to) { };

            //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.StewardApplicationApprovedEmailTempl"));
            //string emailBody = MicrosoftIO.File.ReadAllText(htmlbody);

            string emailBody = EmailContentHelper.GetCompleteEmailByName("ApprovedApplicationSteward");
            string subject = EmailContentHelper.GetSubjectByName("ApprovedApplicationSteward");

            string inviteUrl = string.Format("{0}{1}{2}", SiteSettings.Instance.DomainName, "/System/Invitation/AcceptInvitation/", model.InvitationToken);
            DateTime expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));


            emailBody = emailBody
                .Replace(StewardApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                .Replace(StewardApplicationEmailTemplPlaceHolders.RegistrationNumber, model.RegistrationNumber)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessName, model.BusinessName)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessLegalName, model.BusinessLegalName)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessAddress1, model.Address1)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessCity, model.City)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessProvinceState, model.ProvinceState)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessPostalCode, model.PostalCode)
                .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessCountry, model.Country)
                .Replace(StewardApplicationEmailTemplPlaceHolders.AcceptInvitationUrl, inviteUrl)
                .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                .Replace(StewardApplicationEmailTemplPlaceHolders.InvitationExpirationDateTime, expireDate.ToString(TreadMarksConstants.DateFormat) + " at " + expireDate.ToString("hh:mm tt"))
                .Replace(StewardApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

            var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

            //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
            //rethinkTiresLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.CompanyLogo;

            //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

            //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
            //followUsOnTwitter.ContentId = StewardApplicationEmailTemplPlaceHolders.TwitterLogo;

            //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

            var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
            treadMarksLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.ApplicationLogo;

            alternateViewHTML.LinkedResources.Add(treadMarksLogo);

            if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
            {
                LinkedResource otsEmailLogo;
                try
                {
                    otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                }
                catch (Exception ex)
                {
                    LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                    otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                }
                otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                alternateViewHTML.LinkedResources.Add(otsEmailLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
            {
                var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                alternateViewHTML.LinkedResources.Add(facebookLogo);
            }

            if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
            {
                var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                alternateViewHTML.LinkedResources.Add(twitterLogo);
            }
            //string subject = MessageResource.StewardApplicationApprovedEmailSubject;

            SiteSettings.Instance.GetSettingValue("Invitation.WelcomeSubject");
            Task.Factory.StartNew(() =>
            {
                bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailStewardAndRemittance").ToBoolean();
                string bccEmailAddr = SiteSettings.Instance.GetSettingValue("Email.StewardApplicationApproveBCCEmailAddr");
                bool bSendAnotherBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

                //OTSTM2-132 BCC value update                
                emailer.SendEmailWithSecondBcc(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), model.Email, null, bSendBCCEmail ? bccEmailAddr : null,
                 subject, emailBody, null, alternateViewHTML, bSendAnotherBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null);
            });
            return true;
        }

        private void AddActivity(UserReference assignerUser, string ActivityArea, string logContent, int ObjectId)
        {
            var activity = new Activity();
            activity.Initiator = assignerUser != null ? assignerUser.UserName : string.Empty;
            activity.InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant";
            string tmp = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.';
            this.messageService.AddActivity(new Activity()
            {
                ObjectId = ObjectId,
                InitiatorName = assignerUser != null ? (!string.IsNullOrEmpty(assignerUser.FullName) ? assignerUser.FullName : "Applicant") : "Applicant",
                CreatedTime = DateTime.Now,
                ActivityType = TreadMarksConstants.VendorRegistrationActivity,// 6: application activity
                ActivityArea = ActivityArea,
                Message = logContent.Replace("&lt;", "<").Replace("&gt;", ">").Trim().TrimEnd('.') + '.',
                Initiator = assignerUser != null ? assignerUser.UserName : "Applicant",
                Assignee = assignerUser != null ? assignerUser.UserName : "Applicant@test.com",
                AssigneeName = assignerUser != null ? assignerUser.UserName : "Applicant",
            });
        }

        #region WorkFlow
        [HttpPost]
        public JsonResult ChangeStatus(string applicationId, string status, long userId = 0, string assgnedUserName = "")
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            int applicationID;
            int.TryParse(applicationId, out applicationID);

            if (applicationID == 0) { throw new ArgumentException("applicationId"); }

            ApplicationStatusEnum applicationStatus;
            Enum.TryParse<ApplicationStatusEnum>(status, true, out applicationStatus);

            var user = TMSContext.TMSPrincipal.Identity;
            string updatedBy = user.Name;

            if (string.Compare(status, ApplicationStatusEnum.Approved.ToString(), StringComparison.OrdinalIgnoreCase) != 0)
            {
                //if its backtoApplicant send an email
                if (string.Compare(applicationStatus.ToString(), ApplicationStatusEnum.BackToApplicant.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    var StewardApplicationModel = this.stewardRegistrationService.GetFormObject(applicationID);
                    string Email = this.applicationInvitationService.GetEmailByApplicationId(applicationID);

                    var backToApplicantHaulerEmailModel = new ApplicationEmailModel()
                    {
                        BusinessLegalName = StewardApplicationModel.BusinessLocation.BusinessName,
                        BusinessName = StewardApplicationModel.BusinessLocation.OperatingName,
                        Address1 = StewardApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                        City = StewardApplicationModel.BusinessLocation.BusinessAddress.City,
                        ProvinceState = StewardApplicationModel.BusinessLocation.BusinessAddress.Province,
                        PostalCode = StewardApplicationModel.BusinessLocation.BusinessAddress.Postal,
                        Country = StewardApplicationModel.BusinessLocation.BusinessAddress.Country,
                        Email = Email,
                    };

                    if (EmailContentHelper.IsEmailEnabled("NewApplicationBacktoApplicant"))
                        this.stewardRegistrationService.SendEmailForBackToApplicant(applicationID, backToApplicantHaulerEmailModel);
                }

                this.stewardRegistrationService.SetApplicationStatus(applicationID, applicationStatus, null, updatedBy, userId);
            }

            string logContent = string.Empty;
            switch (status.ToLower())
            {
                case "onhold":
                    logContent = "Application placed <strong>On-Hold</strong> by " + SecurityContextHelper.CurrentUser.FullName;
                    break;
                case "denied":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>rejected</strong>" + " this application";
                    break;
                case "backtoapplicant":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " sent this application <strong>back</strong> to the <strong>Applicant</strong>";
                    break;
                case "approved":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>approved</strong> this application ";
                    break;
                case "completed":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " send this application <strong>back</strong> to the <strong>Applicant</strong>";
                    break;
                case "offhold":
                    logContent = "Application placed <strong>Off-Hold</strong> by " + SecurityContextHelper.CurrentUser.FullName;
                    break;
                case "assigned":
                    logContent = "Application " + " <strong>assigned</strong> to " + "<strong>" + assgnedUserName + "</strong>";
                    break;
                case "unassigned":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>un-assigned</strong> the application from " + "<strong>" + assgnedUserName + "</strong>";
                    break;
                case "reassigned":
                    logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>assigned</strong> application to " + "<strong>" + assgnedUserName + "</strong>";
                    break;

                default:
                    if (userId == -1)//un-assign
                    {
                        logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>un-assigned</strong> the application from " + "<strong>" + assgnedUserName + "</strong>";
                    }
                    break;
            }
            if (applicationID > 0 && !string.IsNullOrEmpty(logContent))
            {
                this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Steward, logContent, applicationID);
            }

            return Json(new { status = MessageResource.ValidData }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUsersInAuditGroup()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            string[] groupName = { UsersInAuditGroupEnum.Audit.ToString(), UsersInAuditGroupEnum.CallCentre.ToString() };


            var textValuePair = this.userService.GetUsersByGroup(groupName)
                .Select(r => new TextValuePair() { Text = r.Value, Value = r.Key.ToString() })
                .OrderBy(r => r.Text);

            return Json(textValuePair, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DenyApplication(StewardApplicationDenyModel StewardApplicationDenyModel)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Steward")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsStewardRouting = SecurityContextHelper.StewardApplicationsWorkflow != null ? SecurityContextHelper.StewardApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsStewardRouting)
            {
                int applicationID;
                int.TryParse(StewardApplicationDenyModel.applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }
                string denyReasons = string.Join(",", StewardApplicationDenyModel.validationErrorMessages);
                //TODO: Check for call centre role
                var user = TMSContext.TMSPrincipal.Identity;
                string updatedBy = user.Name;
                this.stewardRegistrationService.SetApplicationStatus(applicationID, ApplicationStatusEnum.Denied, denyReasons, updatedBy);

                var StewardApplicationModel = this.stewardRegistrationService.GetFormObject(applicationID);
                string Email = this.applicationInvitationService.GetEmailByApplicationId(applicationID);
                var approvedHaulerApplicationModel = new ApplicationEmailModel()
                {
                    BusinessLegalName = StewardApplicationModel.BusinessLocation.BusinessName,
                    BusinessName = StewardApplicationModel.BusinessLocation.OperatingName,
                    Address1 = StewardApplicationModel.BusinessLocation.BusinessAddress.AddressLine1,
                    City = StewardApplicationModel.BusinessLocation.BusinessAddress.City,
                    ProvinceState = StewardApplicationModel.BusinessLocation.BusinessAddress.Province,
                    PostalCode = StewardApplicationModel.BusinessLocation.BusinessAddress.Postal,
                    Country = StewardApplicationModel.BusinessLocation.BusinessAddress.Country,
                    Email = Email,// StewardApplicationModel.ContactInformationList[0].ContactInformation.Email,
                };

                string message = "";
                if (EmailContentHelper.IsEmailEnabled("NewApplicationDenied"))
                {
                    message = ComposeAndSendDenyEMail(approvedHaulerApplicationModel, StewardApplicationDenyModel.validationErrorMessages);
                }
                else
                    message = "This email is disabled. Please contact us for assistance.";

                //applicationService.LogActivity((long)applicationID, null, message, HttpContext.User.Identity.Name);

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        private string ComposeAndSendDenyEMail(ApplicationEmailModel approvedApplicationModel, string[] validationErrorMessages)
        {
            string message = string.Empty;

            try
            {
                //string htmlbody = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Email.DenyHaulerApplicationEmailTemplLocation"));
                //string emailBody = SystemIO.File.ReadAllText(htmlbody);

                string emailBody = EmailContentHelper.GetCompleteEmailByName("NewApplicationDenied");
                string subject = EmailContentHelper.GetSubjectByName("NewApplicationDenied");

                emailBody = emailBody
                    .Replace(StewardApplicationEmailTemplPlaceHolders.ApplicationType, "Steward")
                    .Replace(StewardApplicationEmailTemplPlaceHolders.Date, DateTime.Now.ToString("MMMM dd, yyyy"))
                    .Replace(HaulerApplicationEmailTemplPlaceHolders.CompanyName, SiteSettings.Instance.GetSettingValue("Company.Name"))
                    .Replace(StewardApplicationEmailTemplPlaceHolders.BusinessLegalName, approvedApplicationModel.BusinessLegalName)
                    .Replace(StewardApplicationEmailTemplPlaceHolders.NewDatetimeNowYear, DateTime.Now.Year.ToString()); //OTSTM2-979

                if (validationErrorMessages != null && validationErrorMessages.Length > 0)
                {
                    var validationErrorMessagesHtmlBlock = new StringBuilder();
                    foreach (var errorMessage in validationErrorMessages)
                    {
                        string validationErrorMessageHtml = string.Format("<p style='color: #505050;font-family: Arial;font-size: 15px;line-height: 150%;text-align:left;'>{0}</p>", errorMessage);
                        validationErrorMessagesHtmlBlock.Append(validationErrorMessageHtml);
                    }

                    emailBody = emailBody.Replace(StewardApplicationEmailTemplPlaceHolders.ValidationErrorMessages, validationErrorMessagesHtmlBlock.ToString());
                }
                else
                {
                    emailBody = emailBody.Replace(StewardApplicationEmailTemplPlaceHolders.ValidationErrorMessages, string.Empty);
                }

                var alternateViewHTML = AlternateView.CreateAlternateViewFromString(emailBody, null, MediaTypeNames.Text.Html);

                //var rethinkTiresLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo1FileLocation")), MediaTypeNames.Image.Jpeg);
                //rethinkTiresLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.CompanyLogo;

                //alternateViewHTML.LinkedResources.Add(rethinkTiresLogo);

                //var followUsOnTwitter = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterImageLocation")), MediaTypeNames.Image.Jpeg);
                //followUsOnTwitter.ContentId = StewardApplicationEmailTemplPlaceHolders.TwitterLogo;

                //alternateViewHTML.LinkedResources.Add(followUsOnTwitter);

                var treadMarksLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Settings.ApplicationLogoFileLocation")), MediaTypeNames.Image.Jpeg);
                treadMarksLogo.ContentId = StewardApplicationEmailTemplPlaceHolders.ApplicationLogo;

                alternateViewHTML.LinkedResources.Add(treadMarksLogo);

                if (SiteSettings.Instance.GetSettingValue("Email.CBHomepageURL") == "1")
                {
                    LinkedResource otsEmailLogo;
                    try
                    {
                        otsEmailLogo = LogoHelper.GetCompanyLogo(AppSettings.Instance.AllSettings.FirstOrDefault(c => c.Key == "Company.LogoURL").Value, uploadRepositoryPath);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogExceptionWithMessage("Could not find uploaded logo file, default logo is applied", ex);
                        otsEmailLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.CompanyLogo2FileLocation")), MediaTypeNames.Image.Jpeg);
                    }
                    otsEmailLogo.ContentId = CollectorApplicationEmailTemplPlaceHolders.CompanyLogo;

                    alternateViewHTML.LinkedResources.Add(otsEmailLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBFacebookURL") == "1")
                {
                    var facebookLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.FacebookLogoLocation")), MediaTypeNames.Image.Jpeg);
                    facebookLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.FacebookLogo;
                    alternateViewHTML.LinkedResources.Add(facebookLogo);
                }

                if (SiteSettings.Instance.GetSettingValue("Email.CBTwitterURL") == "1")
                {
                    var twitterLogo = new LinkedResource(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SiteSettings.Instance.GetSettingValue("Site.TwitterLogoLocation")), MediaTypeNames.Image.Jpeg);
                    twitterLogo.ContentId = HaulerApplicationEmailTemplPlaceHolders.TwitterLogo;
                    alternateViewHTML.LinkedResources.Add(twitterLogo);
                }

                Email.Email email = new Email.Email(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Email.smtpPort")),
                    SiteSettings.Instance.GetSettingValue("Email.smtpServer"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpUserName"),
                    SiteSettings.Instance.GetSettingValue("Email.smtpPassword"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementBegin"),
                    SiteSettings.Instance.GetSettingValue("Email.dataElementEnd"),
                    SiteSettings.Instance.GetSettingValue("Email.defaultEmailTemplateDirectory"),
                    SiteSettings.Instance.GetSettingValue("Company.Email"),
                    Boolean.Parse(SiteSettings.Instance.GetSettingValue("Email.useSSL")));

                //string subject = MessageResource.StewardDeclinedSubject;
                bool bSendBCCEmail = SiteSettings.Instance.GetSettingValue("Email.CBCCEmailClaimAndApplication").ToBoolean();

                //OTSTM2-132 BCC value update
                email.SendEmail(SiteSettings.Instance.GetSettingValue("Email.CBDefaultFromEmailAddr") == "1" ? SiteSettings.Instance.GetSettingValue("Email.defaultFrom") : SiteSettings.Instance.GetSettingValue("Company.Email"), approvedApplicationModel.Email, null, bSendBCCEmail ? SiteSettings.Instance.GetSettingValue("Email.BccApplicationPath") : null, subject, emailBody, null, null, alternateViewHTML);

                message = string.Format("Steward application deny email was successfully sent to {0} at email {1}", approvedApplicationModel.BusinessLegalName, approvedApplicationModel.Email);
            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                message = ex.Message;
            }

            return message;
        }

        [HttpPost]
        public JsonResult ApproveApplication(string applicationId)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            var currUser = TMSContext.TMSPrincipal;
            var rolePermissionsClaims = currUser.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Steward")).ToList();
            var IsAllPanelEditSave = ApplicationHelper.IsAllPanelEditSave(rolePermissionsClaims);
            var IsStewardRouting = SecurityContextHelper.StewardApplicationsWorkflow != null ? SecurityContextHelper.StewardApplicationsWorkflow.Routing : false;

            if (IsAllPanelEditSave && IsStewardRouting)
            {
                int applicationID;
                int.TryParse(applicationId, out applicationID);

                if (applicationID == 0)
                {
                    throw new ArgumentOutOfRangeException("Application ID cannot be NULL or 0");
                }

                var applicationModel = this.stewardRegistrationService.GetFormObject(applicationID);

                var customerId = this.stewardRegistrationService.ApproveApplication(applicationModel, applicationID);

                //ET-79, comment out AddAppUser() because AppUsers are not required for Customer/Steward, comment out AddActivity() because there will be duplicated logs if not (ChangeStatus() method already had this activity log.)
                //if (customerId > 0)//approve successfully returned
                //{
                //    //registrantService.AddAppUser(1, customerId, HttpContext.User.Identity.Name); 
                //    string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>approved</strong> this application ";
                //    this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Steward, logContent, applicationID);
                //}
                ApplicationEmailModel approvedApplicationModel = this.stewardRegistrationService.GetApprovedApplicantInformation(applicationID);

                //Create User first to Get invitation token
                approvedApplicationModel.InvitationToken = CreateUserInvitation(approvedApplicationModel, customerId, applicationID);

                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationSteward"))
                {
                    bool result = ComposeAndSendApplicationApprovalEmail(approvedApplicationModel);
                }

                return Json(new { status = MessageResource.ValidData, isValid = true, customerID = customerId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //  Update save in DB 
        public ActionResult SaveModelDB(StewardRegistrationModel model)
        {
            try
            {
                var user = TMSContext.TMSPrincipal;
                var rolePermissionsClaims = user.Claims.Where(c => c.Type == TreadMarksConstants.RolePermissions && c.Value.StartsWith("Applications_Steward")).ToList();
                var IsStewardAutoSaveDisabled = ApplicationHelper.IsStewardAutoSaveDisabled(rolePermissionsClaims);

                if (!IsStewardAutoSaveDisabled)
                {
                    PopulateTireDetails(model);

                    if (model.VendorID != 0)
                    {
                        model.ContactInformationList.ToList().ForEach(c =>
                        {
                            c.isEmptyContact = false;
                            if (c.ContactInformation.ID == 0)
                            {
                                c.ContactAddressSameAsBusinessAddress = c.ContactInformation.ContactAddressSameAsBusiness;

                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (c.ContactAddressSameAsBusinessAddress)
                                    {
                                        model.ContactInformationList.Remove(c);
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            model.ContactInformationList.Remove(c);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(c.ContactInformation.FirstName) && string.IsNullOrEmpty(c.ContactInformation.LastName)
                                && string.IsNullOrEmpty(c.ContactInformation.PhoneNumber) && string.IsNullOrEmpty(c.ContactInformation.Position)
                                && string.IsNullOrEmpty(c.ContactInformation.Ext) && string.IsNullOrEmpty(c.ContactInformation.AlternatePhoneNumber)
                                && string.IsNullOrEmpty(c.ContactInformation.Email))
                                {
                                    if (!c.ContactAddressSameAsBusinessAddress)
                                    {
                                        if (string.IsNullOrEmpty(c.ContactAddress.AddressLine1) && string.IsNullOrEmpty(c.ContactAddress.AddressLine2)
                                            && string.IsNullOrEmpty(c.ContactAddress.City) && string.IsNullOrEmpty(c.ContactAddress.Postal)
                                            && string.IsNullOrEmpty(c.ContactAddress.Province) && string.IsNullOrEmpty(c.ContactAddress.Country))
                                        {
                                            c.isEmptyContact = true;
                                        }
                                    }
                                }
                            }
                        });
                    }

                    model.UserIPAddress = ("::1" == MicrosoftWeb.HttpContext.Current.Request.UserHostAddress) ? "localhost" : MicrosoftWeb.HttpContext.Current.Request.UserHostAddress;
                    if (model.TermsAndConditions != null)
                    {
                        model.TermsAndConditions.UserIP = model.UserIPAddress;
                        model.TermsAndConditions.SubmittedDate = DateTime.UtcNow; //Use UTC time to avoid DateTime Serialze issue in JavaScriptSerializer().Serialize()
                    }

                    model.ServerDateTimeOffset = DateTime.Now.Hour - DateTime.UtcNow.Hour; //For client side to convert to local time on UI

                    var customerId = this.stewardRegistrationService.UpdateApproveApplication(model, model.VendorID);

                    return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //For any potential internal expectations that would occur due to missing panel on any pages during "save" action  due to limited user access  please use following message 
                    return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ResendApproveCustomer(int customerId, int? applicationId)
        {
            if (customerId == 0)
            {
                throw new ArgumentOutOfRangeException("Customer ID cannot be NULL or 0");
            }
            ApplicationEmailModel approvedApplicationModel = this.registrantService.GetApprovedCustomerInformation(customerId);

            if (approvedApplicationModel != null)
            {
                var invitation = userService.FindInvitation(customerId, approvedApplicationModel.Email, false);
                var expireDate = DateTime.Now.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
                if (invitation != null)
                {
                    invitation.FirstName = approvedApplicationModel.PrimaryContactFirstName;
                    invitation.LastName = approvedApplicationModel.PrimaryContactLastName;
                    invitation.ExpireDate = expireDate;
                    userService.UpdateInvitation(invitation);
                    approvedApplicationModel.InvitationToken = invitation.InviteGUID;
                }
                else
                {
                    var userIp = Request.UserHostAddress;
                    var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));
                    var newinvitation = ModelAdapter.ToCustomerInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, string.Empty, customerId, 0);
                    userService.CreateParticipantDefaultUserInvitation(newinvitation);
                    approvedApplicationModel.InvitationToken = newinvitation.InviteGUID;
                }

                if (EmailContentHelper.IsEmailEnabled("ApprovedApplicationSteward"))
                {
                    bool result = ComposeAndSendApplicationApprovalEmail(approvedApplicationModel);
                    if (!result)
                        throw new Exception("Email failed to send");
                }

                string logContent = SecurityContextHelper.CurrentUser.FullName + " <strong>Re-Sent</strong> the welcome letter to the <strong>Participant</strong>.";
                if (invitation.ApplicationId.HasValue)
                {
                    this.AddActivity(SecurityContextHelper.CurrentUser, TreadMarksConstants.Steward, logContent, applicationId ?? 0); // invitation.ApplicationId ?? 0);
                }

                return Json(new { status = MessageResource.ValidData, isValid = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = MessageResource.InvalidData, isValid = false }, JsonRequestBehavior.AllowGet);
        }

        private Guid CreateUserInvitation(ApplicationEmailModel approvedApplicationModel, int customerId, int applicationId)
        {
            var userIp = Request.UserHostAddress;
            var expireDate = DateTime.UtcNow.AddDays(Convert.ToInt32(SiteSettings.Instance.GetSettingValue("Invitation.ExpirationDays")));
            var siteUrl = string.Format("{0}{1}", SiteSettings.Instance.GetSettingValue("Settings.DomainName"), SiteSettings.Instance.GetSettingValue("Invitation.URL"));
            var invitation = ModelAdapter.ToCustomerInvitation(approvedApplicationModel.PrimaryContactFirstName, approvedApplicationModel.PrimaryContactLastName, approvedApplicationModel.Email, userIp, expireDate, siteUrl, null, customerId, applicationId);
            userService.CreateParticipantDefaultUserInvitation(invitation);
            return invitation.InviteGUID;
        }

        [HttpPost]
        public JsonResult SaveItem(int vendorId, int ID, string PropName)
        {
            try
            {
                var userId = (int)SecurityContextHelper.CurrentUser.Id; //OTSTM2-21
                this.stewardRegistrationService.SetRemitanceStatus(vendorId, ID, PropName, userId);
                return Json("Success", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var rethrow = ExceptionHandlingHelper.HandlingClientException(ex);
                if (rethrow)
                {
                    throw;
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Private Helper Methods
        private void PopulateTireDetails(StewardRegistrationModel model)
        {
            #region TireDetailsItemType
            if (model.TireDetails != null)
            {
                if (model.TireDetails.TireDetailsItemType != null)
                {
                    model.TireDetails.TireDetailsItemType = ApplicationHelper.GetTireDetailsItemType(model).ToList();
                }
                else
                {
                    model.TireDetails.TireDetailsItemType = new List<TireDetailsItemTypeModel>();
                }
            }
            else
            {
                model.TireDetails = new StewardTireDetailsRegistrationModel()
                {
                    TireDetailsItemType = new List<TireDetailsItemTypeModel>()
                };
            }
            #endregion

            model.TireDetails.TireDetailsItemType = ApplicationHelper.GetTireDetailsItemType(model).ToList();
        }

        #endregion
    }
}