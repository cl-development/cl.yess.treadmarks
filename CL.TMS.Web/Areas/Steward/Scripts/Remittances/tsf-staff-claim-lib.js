﻿"use strict";

var tsfClaimLib = angular.module("tsfStaffClaimLib", ['ui.bootstrap', 'commonLib', 'datatables', 'datatables.scroller', 'tsfCommonLib']);

//directives section

tsfClaimLib.directive('tsfclaimActivityPanel', ['$window', 'DTOptionsBuilder', 'DTColumnBuilder', '$timeout', "tsfclaimService", function tsfclaimActivityPanel($window, DTOptionsBuilder, DTColumnBuilder, $timeout, tsfclaimService) {
    return {

        scope: {
            snpUrl: '@',
        },

        restrict: 'E',

        link: function (scope, elem) {
            //Activity Message responsive
            var isPopOver = false;
            angular.element($window).bind('resize', function () {
                var length = scope.dtInstance.DataTable.context[0].aoData.length;
                for (var i = 0; i < length; i++) {
                    var td = scope.dtInstance.DataTable.context[0].aoData[i].anCells[2];
                    if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                        isPopOver = true;
                        td.style.cursor = "pointer";
                        $(td).on("hover", popOverFunctionHover(td));
                    }
                    else {
                        isPopOver = false;
                        td.style.cursor = "default";
                        $(td).off("hover", popOverFunctionHover(td));
                    }
                }
                scope.$digest();
            });

            //popover when the activity message is too long
            function popOverFunctionHover(data) {
                if (isPopOver) {
                    $(data).webuiPopover({
                        width: '300',
                        height: '200',
                        padding: true,
                        multi: true,
                        closeable: true,
                        title: 'Activity',
                        type: 'html',
                        trigger: 'hover',
                        content: function () {
                            return data.innerHTML;
                        },
                        delay: { show: 100, hide: 100 },
                    });
                }
                else {
                    $(data).webuiPopover('destroy'); //destroy popover windows, otherwise, popover windows always exists once it generates
                }
            }
        },

        templateUrl: 'tsfclaimActivityPanel.html',

        controller: function ($scope, $http, $compile, $templateCache, $rootScope, $window) {

            $scope.dtInstance = {};

            $scope.dtColumns = [
                    DTColumnBuilder.newColumn("CreatedDate", "Date/Time").withOption('name', 'CreatedDate').withOption('width', '15%').withClass('sorting_l').renderWith(function (data, type, full, meta) {
                        if (data !== null) {
                            var date = dateTimeConvert(data);
                            return '<div>' + date.date + ' ' + date.time + '</div>';
                        }
                        else {
                            return "";
                        }
                    }),
                    DTColumnBuilder.newColumn("Initiator", "Initiator").withOption('name', 'Initiator').withOption('width', '15%').withClass('sorting_m'),
                    DTColumnBuilder.newColumn("Message", "Activity").withOption('name', 'Message').withOption('width', '70%').withClass('sorting_s')
            ];

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
                dataSrc: "data",
                url: $scope.snpUrl,
                global: false,
                type: "POST",
                error: function (xhr, error, thrown) {
                    console.log('xhr, error:', xhr, error);
                    window.location.reload(true);
                }
            }).withOption('processing', true)
                .withOption('responsive', true).withOption('bAutoWidth', false)
                .withOption('order', [0, 'desc'])
                .withOption('serverSide', true)
                .withOption('lengthChange', false)
                .withDisplayLength(5)
                .withDOM('tr')
                .withOption('initComplete', function (settings, result) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) { //the condition of text-overflow: ellipsis in css works
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));                          
                        }
                    }

                    $scope.viewMore = (result.data.length >= 5 && !$scope.scroller);
                    $scope.$apply();
                })
                .withOption('drawCallback', function (settings) {
                    for (var i = 0; i < settings.aoData.length; i++) {
                        var td = settings.aoData[i].anCells[2];
                        if (td.offsetWidth < td.scrollWidth) {
                            td.style.cursor = "pointer";
                            td.addEventListener("hover", popOverFunction(td));
                        }
                    }
                    $scope.found = 'Found ' + settings.fnRecordsDisplay();
                    $scope.viewMore = (settings.aoData.length >= 5 && !$scope.scroller); //"More" disappears if search result less than 5
                    var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                    var direction = settings.aLastSort[0].dir;
                    var searchValue = $('#searchActivity').val();
                    var claimId = Global.Remittance.ClaimId;
                    var url = "/Steward/TSFRemittances/GetAllActivitiesExport?searchText=CCC&sortColumn=AAA&sortDirection=BBB&claimId=DDD";
                    url = url.replace('AAA', sortColumn).replace('BBB', direction).replace('CCC', searchValue).replace('DDD', claimId);
                    $('#activityExport').attr('href', url);
                    $scope.$apply();
                });

            $scope.url = $scope.snpUrl; //keep original snpUrl
            $scope.isMoreClickedWithSearchValue = false; //Condition to check if "More" button is clicked while searchValue is not empty (only happens once), default is false

            //view more
            $scope.viewMoreClick = function ($event) {
                $scope.viewMore = false;
                $scope.scroller = true;

                //hidden div horizontal scroll after more click
                $scope.panelAfterMore = { 'overflow': 'hidden' };

                var searchValue = $('#searchActivity').val();
                if (searchValue) { //change original snpUrl by appending searchValue, because searchValue cannot be passed to backend by DataGridModel param if "More" button is clicked (maybe a bug of angular datatables)
                    var url = $scope.snpUrl;
                    url = url + "&temp=" + searchValue;
                    $scope.snpUrl = url;
                    $scope.isMoreClickedWithSearchValue = true; //"More" is clicked while searchValue is not empty, only happens once
                }
                $scope.dtOptions.withScroller()
                                .withOption('deferRender', true)
                                .withOption('scrollY', 190)
                                .withOption('responsive', true)
                                .withOption('ajax', { //Refresh datatable with the changed snpUrl. Option changed, then dtInstance changed with the updated options
                                    dataSrc: "data",
                                    url: $scope.snpUrl,
                                    global: false,
                                    type: "POST"
                                });
            }

            $scope.delay = (function () {
                var promise = null;

                return {
                    calculateDelay: function (callback, ms) {
                        $timeout.cancel(promise);
                        promise = $timeout(callback, ms)
                    }
                }
            })();

            //search, search operation can always pass searchValue to backend through "DataGridModel param", don't care snpUrl
            $scope.search = function () {

                if ($scope.searchText.length >= 1) {
                    $scope.foundStyle = { 'display': 'block' };
                    $scope.removeIconStyle = { 'display': 'block' };
                    $scope.searchStyle = { 'visibility': 'visible', 'width': '190px' };
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
                else {
                    $scope.foundStyle = { 'display': 'none' };
                    $scope.removeIconStyle = { 'display': 'none' };
                    $scope.searchStyle = '';
                    $scope.dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //Auto reload datatable after new activity generated
            $rootScope.$on('TOTAL_ACTIVITY', function (e, data) {
                $scope.dtInstance.reloadData();
                $scope.hasNew = true;
            });

            //remove icon
            $scope.removeIcon = function (dtInstance) {

                $scope.searchText = '';
                $scope.removeIconStyle = { 'display': 'none' };
                $scope.foundStyle = { 'display': 'none' };
                $scope.searchStyle = '';
                //dtInstance.DataTable.search($scope.searchText).draw(false);
                if ($scope.isMoreClickedWithSearchValue) { //If "More" is clicked while searchValue is not empty, change snpUrl back to original one and update options, then dtInstance changed with the updated options
                    $scope.snpUrl = $scope.url;
                    $scope.isMoreClickedWithSearchValue = false; //change the value back to false, all later clicking of "remove" icon will go to the "else" branch (with updated dtInstance)
                    $scope.dtOptions.withScroller()
                               .withOption('ajax', { //refresh datatable with the changed snpUrl (original one)
                                   dataSrc: "data",
                                   url: $scope.snpUrl,
                                   global: false, // this makes sure ajaxStart is not triggered
                                   type: "POST"
                               });
                }
                else {
                    dtInstance.DataTable.search($scope.searchText).draw(false);
                }
            }

            //popover when the activity message is too long
            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '300',
                    height: '200',
                    padding: true,
                    multi: true,
                    closeable: true,
                    title: 'Activity',
                    type: 'html',
                    trigger: 'hover',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: { show: 100, hide: 100 },
                });
            }

            function dateTimeConvert(data) {
                if (data == null) return '1/1/1950';
                var r = /\/Date\(([0-9]+)\)\//gi;
                var matches = data.match(r);
                if (matches == null) return '1/1/1950';
                var result = matches.toString().substring(6, 19);
                var epochMilliseconds = result.replace(
                /^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/,
                '$1');
                var b = new Date(parseInt(epochMilliseconds));
                var c = new Date(b.toString());
                var curr_date = c.getDate();
                if (curr_date < 10) {
                    curr_date = '0' + curr_date;
                }
                var curr_month = c.getMonth() + 1;
                if (curr_month < 10) {
                    curr_month = '0' + curr_month;
                }
                var curr_year = c.getFullYear();

                var hours = c.getHours();
                var minutes = c.getMinutes();
                var second = c.getSeconds();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                second = second < 10 ? '0' + second : second;

                var curr_time = hours + ':' + minutes + ':' + second + ' ' + ampm;
                var d = curr_year + '-' + curr_month.toString() + '-' + curr_date;
                //var d = curr_month.toString() + '/' + curr_date + '/' + curr_year;

                return {
                    date: d,
                    time: curr_time
                }
            }         
        } //controller closing brace
    } // literal object closing brace
}]); //directive function closing brace

tsfClaimLib.directive('decimalRequired', [function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {

            var REGEX = new RegExp('^-?[0-9]\\d*\\.?\\d{0,' + attr.decimalRequired + '}$');

            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (viewValue) {
                if (REGEX.test(viewValue) || !viewValue) {
                    ngModelCtrl.$setValidity('isDecimalRequired', true);
                    return viewValue;
                } else {
                    ngModelCtrl.$setValidity('isDecimalRequired', false);
                    return viewValue;
                }
            });
        }
    };
}]);

tsfClaimLib.directive('manageInternalNotes', ['$http', 'tsfclaimService', '$filter', function ($http, tsfclaimService, $filter) {
    return {
        restrict: 'E',
        templateUrl: 'tsf-claim-internal-notes.html',
        scope: {
            loadUrl: '@',
            addUrl: '@',
            exportUrl: '@',
            panelArrow: '@'
        },
        link: function (scope, elem, attr, ngModel) {                                

            scope.blur = function () {
                if (!scope.searchNote) {
                    scope.searchStyle = '';
                }
            }

            scope.removeIcon = function ($event) {
                scope.searchNote = '';
                var search = $event.currentTarget.parentElement.firstElementChild;
                $(search).focus();
            }

            scope.$watch('searchNote', function (n, o) {
                if (n) {
                    scope.searchStyle = 'visibility: visible;';
                }
            })

            scope.search = function () {
                //scope.internalNoteLength = scope.filtered.length;
                scope.internalNoteLength = $filter('filter')(scope.allInternalNotes, scope.searchNote).length;
            };

            scope.add = function () {
                if (scope.textAreaModel) {                   
                    //Add
                    $http({
                        url: scope.addUrl,
                        method: "POST",
                        data: JSON.stringify({
                            notes: scope.textAreaModel
                        })
                    }).success(function (result) {
                        //Load results after 
                        $http({
                            url: scope.loadUrl,
                            method: "GET"
                        }).success(function (result) {
                            scope.textAreaModel = '';
                            scope.allInternalNotes = result;
                            scope.$parent.$emit('NOTE_ADDED', scope.allInternalNotes.length);                           
                        })
                    })
                }
            };
        },
        controller: function ($scope, $rootScope) {          
            $http({
                url: $scope.loadUrl,
                method: "GET"
            }).then(function (result) {
                $scope.allInternalNotes = result.data;
                $scope.sortType = 'AddedOn';
                $scope.textAreaModel = '';
                $scope.sortReverse = true;
                $scope.searchNote = '';
                //$scope.searchStyle = 'visibility: hidden;';
                $scope.quantity = 4;
                $scope.showMore = true;
                $scope.export = Global.InternalNoteSettings.InternalExportUrl;
            });
            
            //Pop up note when it's too long
            $scope.popupHandler = function (ele_id, note) {
                var td = document.getElementById(ele_id);
                var noteWidth = getTextWidth(note, "14px Open Sans")
                if (td.offsetWidth < noteWidth + 15) { //the condition of text-overflow: ellipsis in css works, +15 to handle with/without vertical scrollbar
                    td.style.cursor = "pointer";
                    td.addEventListener("hover", popOverFunction(td));                  
                }
                td.innerHTML = note;
            }
            
            //get the width of string
            function getTextWidth(text, font) {
                // re-use canvas object for better performance
                var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
                var context = canvas.getContext("2d");
                context.font = font;
                var metrics = context.measureText(text);
                return metrics.width;
            }

            function popOverFunction(data) {
                $(data).webuiPopover({
                    width: '500',
                    height: '300',
                    padding: true,
                    multi: false,
                    closeable: true,
                    title: 'Internal Adjument Note',
                    type: 'html',
                    trigger: 'click',
                    content: function () {
                        return data.innerHTML;
                    },
                    delay: {
                        show: 100, hide: 100
                    },
                });
            }
        }
    };
}]);

tsfClaimLib.directive('internalNotesExport', ['$http', '$document', function ($http, $document) {

    return {
            restrict: 'E',
            templateUrl: 'anchorExport.html',
        scope: {
                    id: '@',
                    searchText: '@',
                    sortReverse: '@',
                    sortColumn: '@',
                    url: '@',
                    model: '=',
                    fileName: '@'
                },
            link: function (scope, elem, attr, ngModel) {
                scope.anchorCss = {
                    "background": "none",
                    "left": "140px",
                    "position": "absolute",
                    "top": "11px",
                    "width": "25px"
                };

                elem.bind('click', function (event) {
                    event.stopPropagation();
                    event.preventDefault();

                    var req = { id: scope.id, searchText: scope.searchText, sortReverse: scope.sortReverse, sortcolumn: scope.sortColumn, model: scope.model };
                    var submitVal = {
                        url: scope.url,
                        method: "POST",
                            data: JSON.stringify(req)
                    }
                    return $http(submitVal).then(function (result) {

                        var currentDate = new Date();
                        var min = currentDate.getMinutes();
                        var hh = currentDate.getHours();
                        var dd = currentDate.getDate();
                    var mm = currentDate.getMonth() + 1;
                        var yyyy = currentDate.getFullYear();

                        if (dd < 10) {
                        dd = '0' + dd
                        }
                        if (mm < 10) {
                            mm = '0' + mm
                        }
                    currentDate = yyyy + '-' + mm + '-' + dd;

                    var currentDateTime = currentDate + '-' + hh + '-' + min;
                    var filename = scope.fileName + "-" + currentDateTime;

                    if (window.getBrowserInfo().indexOf('IE ') > -1) {
                            navigator.msSaveOrOpenBlob(new Blob([result.data], { type: "text/plain;charset=utf-8;" }), filename + '.csv')
                        }
                        else {
                            var anchor = angular.element('<a/>');
                            anchor.css({
                                display: 'none'
                            });
                            var body = $document.find('body').eq(0).append(anchor);

                            anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(result.data),
                                    target: '_blank',
                            download: filename + '.csv'
                            })[0].click();

                            anchor.remove();
                        }
                    });
                });
            },
            controller: function ($scope) { 

            }
        };
    }]);

//controller section
//OTSTM2-553
tsfClaimLib.controller("remittanceActivityController", ["$scope", "tsfclaimService", function ($scope, tsfclaimService) {
    $scope.hasNew = false;
    var data = { claimId: Global.Remittance.ClaimId };
    tsfclaimService.totalActivity(data).then(function (dataResult) {
        $scope.totalActivity = dataResult;
    });

    $scope.increasingTotalActivity = function (claimId) {
        var data = { claimId: claimId };
        tsfclaimService.totalActivity(data).then(function (dataResult) {
            $scope.totalActivity = dataResult;
            $scope.$parent.$emit('TOTAL_ACTIVITY', dataResult);
        });        
        $scope.$apply();
    }
}]);

tsfClaimLib.controller("tsfStaffInternalAdjustmentController", ["$scope", "$filter", "$uibModal", "tsfclaimService", "$window", function ($scope, $filter, $uibModal, tsfclaimService, $window) {
    if (Global.Remittance.ClaimsStatus == "Approved") {
        $scope.DisableInternalAdjustmentsBtn = true;
    }
    //Page level check not required
    /*else if (Global.Remittance.AllowEdit != "True") {  
        $scope.DisableInternalAdjustmentsBtn = true;
    }*/
    else if (Global.Remittance.DisableInternalAdjustmentsBtn) {
        $scope.DisableInternalAdjustmentsBtn = true;
    }
    else {
        $scope.DisableInternalAdjustmentsBtn = false;
    }
    
   
    var initializeModalResult = function () {
        return {
            selectedItem: null,           
            paymentType: 'Overall',
            amount: 0,
            isAdd: true,
            adjustmentId: 0,            
            internalNote: ''
        };
    }

    $scope.InternalAdjustTypes = [      
       {
           name: 'Payment',
           id: '1'
       }
    ];

    var refreshInternalAjustmentAndPaymentPanel = function () {
        //refresh internal adjustment panel
        var table = $('#tblTSFInternalAdjusList').DataTable();
        table.ajax.reload();

        //refresh payment panel
        tsfclaimService.getTSFRemittanceModel().then(function (data) {
            $("#TSFRemittanceStaffPaymentPanel").html(data);
            if (!penaltiesPopover.content) {
                penaltiesPopover.content = $('#penalties-popover').data("popovertext");
            }
            $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({}, settings, penaltiesPopover));
          //  initialPage();
        });
    }

    //Add internal adjustment
    $scope.tsfInternalAdjustments = function () {
        var modalResult = initializeModalResult();
        var internalAdjustmentModalInstance = $uibModal.open({
            templateUrl: 'internalAdjustModal.html',
            controller: 'InternalAdjustmentCtrl',           
            size: 'lg',
            backdrop: 'static',
            resolve: {
                chooseTypes: function () {
                    return $scope.InternalAdjustTypes;
                },
                modalResult: function () {
                    return modalResult;
                },
                selectedItem: function () {
                    return $scope.InternalAdjustTypes[0];
                },
                chooseTypeIsDisable: function () {
                    return false;
                },
                isReadOnly: function () {
                    return false;
                },
                disableForReadOnlyStaff: function () { //OTSTM2-155                    
                    return $scope.DisableInternalAdjustmentsBtn;
                }
            }
        });

        internalAdjustmentModalInstance.result.then(function (modalResult) {
            var internalAdjustmentConfirmModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustConfirmModal.html',
                controller: 'InternalAdjustmentConfirmCtrl',
                //modal won't be closed if clicking outside of modal, followed 2 options
                backdrop: 'static', 
                keyboard: false,
                size: 'lg',
                resolve: {
                    modalResult: function () {
                        return modalResult;
                    }
                }
            });
            internalAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                tsfclaimService.submitInternalAdjustment(modalResult).then(function (data) {
                    if (data.status == "refresh") {                      
                        //$window.location.reload();
                        refreshInternalAjustmentAndPaymentPanel();
                    }
                    if (data) {//RedirectResult("~/System/Common/EmptyPage?navigationAction=loadTopMenu");
                        //document.write(data);
                    }
                });
            });
        });
    }

    //Edit internal adjustment
    $scope.editInternalAdjust = function (internalAdjustId, internalAdjustType) {
        tsfclaimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var editedItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var internalAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'InternalAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return editedItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return false;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            internalAdjustmentModalInstance.result.then(function (modalResult) {
                var internalAdjustmentConfirmModalInstance = $uibModal.open({
                    templateUrl: 'internalAdjustConfirmModal.html',
                    controller: 'InternalAdjustmentConfirmCtrl',
                    //modal won't be closed if clicking outside of modal, followed 2 options
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        modalResult: function () {
                            return modalResult;
                        }
                    }
                });
                internalAdjustmentConfirmModalInstance.result.then(function (modalResult) {
                    tsfclaimService.editInternalAdjustment(modalResult).then(function (data) {
                        if (data.status == "refresh") {
                            //$window.location.reload();
                            refreshInternalAjustmentAndPaymentPanel();
                        }
                    });
                });
            });

        });
    }   

    //View internal adjustment
    $scope.viewInternalAdjust = function (internalAdjustId, internalAdjustType) {
        tsfclaimService.getInternalAdjustment(internalAdjustType, internalAdjustId).then(function (data) {
            var viewItem = $filter('filter')($scope.InternalAdjustTypes, { id: data.selectedItem.id })[0];
            var internalAdjustmentModalInstance = $uibModal.open({
                templateUrl: 'internalAdjustModal.html',
                controller: 'InternalAdjustmentCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    chooseTypes: function () {
                        return $scope.InternalAdjustTypes;
                    },
                    modalResult: function () {
                        return data;
                    },
                    selectedItem: function () {
                        return viewItem;
                    },
                    chooseTypeIsDisable: function () {
                        return true;
                    },
                    isReadOnly: function () {
                        return true;
                    },
                    disableForReadOnlyStaff: function () { //OTSTM2-155
                        return $scope.DisableInternalAdjustmentsBtn;
                    }
                }
            });
            
            internalAdjustmentModalInstance.result.then(function (modalResult) {
                if (modalResult == "noteAdded") {
                    var table = $('#tblTSFInternalAdjusList').DataTable();
                    table.ajax.reload();
                }
            });

        });
    }

    //Remove internal adjustment
    $scope.removeInternalAdjust = function (data) {
        var internalAdjustmentId = data.InternalAdjustmentId;
        var internalAdjustmentType = data.TSFInternalAdjustmentType;
        tsfclaimService.removeInternalAdjustment(internalAdjustmentType, internalAdjustmentId).then(function (data) {
            if (data.status == "refresh") {               
                //$window.location.reload();
                refreshInternalAjustmentAndPaymentPanel();
            }
        });
    }

    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalNotesUrl;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalNotesUrl;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalNotes;

}]);

tsfClaimLib.controller('InternalAdjustmentCtrl', ['$scope', '$uibModalInstance', 'chooseTypes', 'modalResult', 'selectedItem', 'chooseTypeIsDisable', 'disableForReadOnlyStaff', 'isReadOnly', "$rootScope", function ($scope, $uibModalInstance, chooseTypes, modalResult, selectedItem, chooseTypeIsDisable, disableForReadOnlyStaff, isReadOnly, $rootScope) {
    $scope.items = chooseTypes;
    $scope.selectedItem = selectedItem;
    $scope.chooseTypeIsDisable = chooseTypeIsDisable;
    $scope.disableForReadOnlyStaff = disableForReadOnlyStaff; //OTSTM2-155
    $scope.modalResult = modalResult;
    $scope.isReadOnly = isReadOnly;
    $scope.validationMessage = "";

    $scope.noteAdded = false
    $rootScope.$on('NOTE_ADDED', function (e, data) {
        $scope.noteAdded = true;
    });

    if (!$scope.isReadOnly) {
        $scope.confirm = function (isValid) {

            //OTSTM2-270
            if (isValid === false || !isValid) return;

            $scope.modalResult.selectedItem = $scope.selectedItem;

            $uibModalInstance.close($scope.modalResult);
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
    else {
        $scope.cancel = function () {
            if ($scope.noteAdded) {
                $uibModalInstance.close('noteAdded');
            }
            else {
                $uibModalInstance.dismiss('cancel');
            }   
        };
    }


    $scope.loadUrl = Global.InternalNoteSettings.LoadInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
    $scope.addUrl = Global.InternalNoteSettings.AddInternalAdjustmentNotesUrl + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;
    $scope.exportUrl = Global.InternalNoteSettings.ExportToExcelInternalAdjustmentNotes + '?ClaimInternalPaymentAdjustId=' + $scope.modalResult.adjustmentId;

    $scope.returnCurrentChooseType = function (item) {

        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });
        return curr[0].isValid();
    }

    $scope.getErrorRule = function (item, rule) {
        var curr = validation.getAllValidationTypes().filter(function (obj) {
            return (obj.id === item.id)
        });

        if (curr[0].errorRules.hasOwnProperty(rule)) {
            return curr[0].errorRules[rule]($scope.internaladjustForm);
        }
    }

    $scope.allZeroesOrEmpty = function () {
        var args = [].slice.call(arguments);

        return validation.checkAllFieldsZero(args)
    }

    var validation = {
        init: function () {
            var self = this;

            this.validationTypes = [                  
                   {
                       name: 'Payment',
                       id: '1',
                       isValid: function () {
                           return self.isValid() && !self.checkAllFieldsZero([$scope.modalResult.amount])
                       },
                       
                       errorRules: {
                           _amount: function (form) {
                               return (form.amount.$error.isDecimalRequired && !form.amount.$pristine) || (form.amount.$error.number && !form.amount.$pristine) || (form.$submitted && self.checkAllFieldsZero([$scope.modalResult.amount]))
                           },
                           _internalNote: function (form) {
                               return (form.note.$invalid && !form.note.$pristine) || (form.note.$invalid && form.$submitted)
                           }
                       }
                   }
            ];

            $scope.$watch('selectedItem', function () {
                if ($scope.internaladjustForm) {
                    $scope.internaladjustForm.$setPristine();
                    $scope.internaladjustForm.$setUntouched();
                }
            });
        },
        isValid: function () {
            return ($scope.internaladjustForm.$valid);
        },
        
        getAllValidationTypes: function () {
            return this.validationTypes;
        },
        
        checkAllFieldsZero: function (arr) {
            if (arr instanceof Array) {
                return arr.every(function (val) {
                    return (val === 0 || val === null);
                });
            }
        },
    }

    validation.init();
    
}]);

tsfClaimLib.controller('InternalAdjustmentConfirmCtrl', ['$scope', '$uibModalInstance', 'modalResult', function ($scope, $uibModalInstance, modalResult) {
    $scope.confirm = function () {
        $uibModalInstance.close(modalResult);
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);

//service section
tsfClaimLib.factory('tsfclaimService', ["$http", function ($http) {

    var factory = {};

    //OTSTM2-553
    factory.totalActivity = function (data) {
        var req = { claimId: data.claimId };
        var submitVal = {
            url: "/TSFRemittances/TotalActivity",
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal);
    }

    factory.submitInternalAdjustment = function (modalResult) {
        var req = { modalResult: modalResult }
        var submitVal = {
            url: Global.Remittance.SubmitInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.removeInternalAdjustment = function (adjustmentType, internalAdjustmentId) {
        var req = {
            adjustmentType: adjustmentType,
            internalAdjustmentId: internalAdjustmentId
        }
        var submitVal = {
            url: Global.Remittance.RemoveInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.getInternalAdjustment = function (adjustmentType, internalAdjustmentId) {
        var req = {
            adjustmentType: adjustmentType,
            internalAdjustmentId: internalAdjustmentId
        }
        var submitVal = {
            url: Global.Remittance.GetInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
            }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.editInternalAdjustment = function (modalResult) {
        var req = { modalResult: modalResult }
        var submitVal = {
            url: Global.Remittance.EditInternalAdjustmentUrl,
            method: "POST",
            data: JSON.stringify(req)
        }
        return $http(submitVal).then(function (result) {
            return result.data;
        });
    }

    factory.getTSFRemittanceModel = function () {
        return $http.get(Global.Remittance.GetTSFRemittanceModelUrl).then(function (result) {
            return result.data;
        });
    }

    return factory;
}]);
