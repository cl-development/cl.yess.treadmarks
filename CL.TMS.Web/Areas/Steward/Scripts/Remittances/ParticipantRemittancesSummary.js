﻿$(function () {
    _PageSize = 12; //OTSTM2-984
    _txtSearchAppId = 'SearchApplications';//id of search textbox
    _txtSearchRegisId = 'SearchRegistrant';
    _exportAppId = 'exportApplicationToExcel'; //id of export
    _exportTransId = 'exportRegistrantToExcel';
    _modalId = 'myModal';

    //OTSTM2-984
    $('#tblApplicationlist tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchText' + idText + '"/>');
    });

    $(window).focus(function () {
        console.log('$(window).focus');
        //window.location.reload(true);
    });
    if (localStorage.getItem("error") === null) {
        $('#ajaxstatus').text('');
    } else {
        $('#ajaxstatus').text(localStorage.getItem("error"));
    }

    var tableApplication = $('#tblApplicationlist').DataTable({
        //paging: true,
        //pageLength: 10,
        info: false,
        processing: false,
        serverSide: true,
        ajax: "TSFRemittances/GetListOfApplicationsByCustomer",
        deferRender: true,
        dom: "rtiS",
        scrollY: 475,
        scrollX: true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        scrollCollapse: true,
        searching: true,
        ordering: true,
        order: [[0, "desc"]],
        //autoWidth: false,
        "columns": [
             {
                 sName: "Period",
                 //width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.Period, data.Activated);
                 }
             },
             {
                 sName: "Status",
                 //width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return ApplicationStatusRender(data.Status, data.Activated);
                 }
             },
             {
                 sName: "TotalReceivable", // TSF DUE($)/ Tire Counts ($) / Paymentable
                 //width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(addCommas(data.TotalReceivable.toFixed(2)), data.Activated);
                }
             },
             {
                 sName: "Due", //Total TSF DUE
                 //width: "10%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(addCommas(data.Due.toFixed(2)), data.Activated);
                 }
             },
             {
                sName: "Payments",
                //width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return GeneralColumnRender(addCommas(data.Payments.toFixed(2)), data.Activated);
                }
             },
             {
                sName: "BalanceDue",
                //width: "15%",
                data: null,
                render: function (data, type, full, meta) {

                    return GeneralColumnRender(addCommas(data.BalanceDue.toFixed(2)), data.Activated);
                }

             },
             {
                sName: "Submitted",
                //width: "15%",
                data: null,
                render: function (data, type, full, meta) {
                    return DatetimeRender(data.Submitted, data.Activated);
                }
             },
             {
                 sName: "SubmittedBy",
                 //width: "15%",
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.SubmittedBy, data.Activated);
                 }
             },
            {
                visible: Global.Remittance.ViewOption.showActionGear == 'True',
                //width: "10%",
                className: "td-center",
                data: null,
                orderable: false,
                render: function (data, type, full, meta) {
                    return ApplicationActionRender(data.Status, data.ID, data.TokenID, data.Activated);
                }
            },
            {
                visible: false,
                data: null,
                render: function (data, type, full, meta) {
                    return GeneralColumnRender(data.ID, data.Activated);
                }
            },
             {
                 visible: false,
                 data: null,
                 render: function (data, type, full, meta) {
                     return GeneralColumnRender(data.TokenID, data.Activated);
                 }
             },
        ],
        initComplete: function (settings, json) {
            $('.dataTables_scrollBody').css('overflow-y', 'hidden');

            //OTSTM2-984
            tableApplication.columns().eq(0).each(function (index) {
                var column = tableApplication.column(index);
                var that = column;
                $('input', column.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        },
        drawCallback: function (settings) {
            var direction = settings.aaSorting[0][1];
            $('.sort').html('<i class="fa fa-sort"></i>');
            (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') :
            $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

            $('#txtFoundApplications').html('Found ' + settings.fnRecordsDisplay());
            (settings.fnDisplayEnd() >= _PageSize) ? $('#ApplicationsViewMore').css('visibility', 'visible')
            : $('#ApplicationsViewMore').css('visibility', 'hidden');
            var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
            var searchValue = $('#SearchApplications').val();
            console.log('searchValue:' + searchValue);

            //OTSTM2-984
            if ($("#ApplicationsViewMore").is(':visible')) {
                $('.dataTables_scrollBody').css({'overflow-y': 'hidden'});
            }
            else {
                if (settings.fnDisplayEnd() >= _PageSize) {
                    $('.dataTables_scrollBody').css({'overflow-y': 'scroll'});
                }
                else {
                    $('.dataTables_scrollBody').css({'overflow-y': 'hidden' });
                }
            }

            if ($("#searchTextPeriod").val() || $("#searchTextStatus").val() || $("#searchTextSubtotal").val() || $("#searchTextTotalTSFDue").val() || $("#searchTextPayment").val() || $("#searchTextBalanceDue").val()
                || $("#searchTextSubmitted").val() || $("#searchTextSubmittedBy").val() || $("#SearchApplications").val()) {

                $("#txtFoundApplications").css('display', 'block');
            }
            else {
                $("#txtFoundApplications").css('display', 'none');
            }

            var url = Global.Remittance.ApplicationExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)
            .replace('-st-period', $("#searchTextPeriod").val()).replace('-st-status', $("#searchTextStatus").val()).replace('-st-due', $("#searchTextSubtotal").val())
            .replace('-st-total', $("#searchTextTotalTSFDue").val()).replace('-st-payment', $("#searchTextPayment").val()).replace('-st-balancedue', $("#searchTextBalanceDue").val())
            .replace('-st-submitted', $("#searchTextSubmitted").val()).replace('-st-submittedby', $("#searchTextSubmittedBy").val()); //OTSTM2-984

            $('#exportApplicationToExcel').attr('href', url);
            //OTSTM2-984 comment out
            //if (searchValue == '') {
            //    $('#txtFoundApplications').html(' ');
            //}
        },
        scroller: {
            displayBuffer: 20,
            rowHeight: 25,
            serverWait: 100,
            loadingIndicator: false,
        },
    });



    $('#tblApplicationlist tbody').on('click', 'td', function (e) {

    });

    $('#tblApplicationlist tbody').on('click', 'tr', function (e) {
        var data = tableApplication.row(this).data();
        var nodeName = e.target.nodeName;

        if ((!!data.ID)) {
            var url = "/Steward/TSFRemittances/ParticipantIndex/" + data.ID;
            window.open(url, '_self');
        } else {
            alert("empty ID");
        }
    });

    $('#tblApplicationlist tbody').on('dbclick', 'tr', function (e) {
        //alert('dbclick');
    });



    $(window).load(function () {
        $("#tblRegistrantlist").parents(".dataTables_scrollBody").css("overflow", "hidden");
        $("#tblApplicationlist").parents(".dataTables_scrollBody").css("overflow-x", "auto");
    });


    $("#ApplicationsViewMore").on("click", function () {
        $("#tblApplicationlist_wrapper").find(".dataTables_scrollBody").css("overflow", "auto").css("overflow-y","auto"); //OTSTM2-949 "overflow-y: auto" for IE11
        $("#tblApplicationlist_wrapper").find('.dataTables_scrollHead').removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $("#tblApplicationlist_wrapper").find('.dataTables_scrollFoot').removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-984
        $(this).hide();
    });

    $("#exportApplicationToExcel").on("click", function (e) {
        window.location.href = $(this).attr('href');
        e.stopPropagation();
    });

    $('#BtnAssignToMeOK').on("click", function () {
        //console.log('BtnAssignToMeOK click');
        if ($('#assgnToMeApplicationID').val() > 0)
            window.location.reload(true);
    });


    //$('.dropdown-menu-actions').on("click", function (e) {
    //    console.log('.dropdown-menu-actions');
    //    e.stopPropagation();
    //});

    function RegNumberColumnRender(iPadNumber, activated) {

        var span = ""
        if (activated) {
            span = "<span>" + iPadNumber + "</span>";
        }
        else {
            //span = "<span style=\"color:#b1b1b1\">" + iPadNumber + "</span>";
            span = "<span>" + iPadNumber + "</span>";
        }
        return span;
    }


    function ApplicationStatusRender(StatusName, activated) {

        var span = ""
        if (StatusName == null) return span;
        switch (StatusName.toLowerCase()) {
            case "approved":
                span = '<span>' + '<div class="panel-table-status color-tm-green-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "submitted":
                span = '<span>' + '<div class="panel-table-status color-tm-orange-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "open":
                span = '<span>' + '<div class="panel-table-status color-tm-blue-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "underreview":
            case "under review":
                span = '<span>' + '<div class="panel-table-status color-tm-yellow-bg">' + StatusName + '</div>' + '</span>';
                break;
            default:
                span = '<span>' + '<div class="panel-table-status color-tm-gray-bg">' + StatusName + '</div>' + '</span>';
                //default code block
        }
        return span;
    }

    function pad(num) {
        num = "0" + num;
        return num.slice(-2);
    }

    function DatetimeRender(value, activated) {
        if (value == null) return "<span></span>";
        var str, year, month, day, hour, minute, d, finalDate;
        str = value.replace(/\D/g, "");
        d = new Date(parseInt(str));

        year = d.getFullYear();
        month = pad(d.getMonth() + 1);
        day = pad(d.getDate());
        hour = pad(d.getHours());
        minutes = pad(d.getMinutes());
        finalDate = year + "-" + month + "-" + day;

        var span = ""
        if (activated) {
            span = "<span>" + finalDate + "</span>";
        }
        else {
            //span = "<span style=\"color:#b1b1b1\">" + iPadNumber + "</span>";
            span = "<span>" + finalDate + "</span>";
        }
        return span;
    }

    function GeneralColumnRender(Value, activated) {

        var span = "<span>"
        //if (!activated) {
        //    span = "<span style=\"color:#b1b1b1\">";
        //}

        if (Value == "") {
            //span = span + "Empty_Value</span>";
            span = "<span style=\"color:#b1b1b1\">";
        }
        else {
            span = span + Value + "</span>";
        }

        return span;
    }

    function ApplicationActionRender(StatusName, ID, TokenID, activated, isActivated) {
        var content = "";
        if (StatusName == null) return content;
        switch (StatusName.toLowerCase()) {
            case "submitted":
                content = "<div class=\"btn-group dropdown-menu-actions\">" +
                "<a href=\"#\" data-toggle=\"dropdown\"><i class=\"glyphicon glyphicon-cog\"></i></a>" +
                "<ul class=\"dropdown-menu\">" +
                "<li><a data-target=\"#modalAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-user\"></i>" +
                "Assign" +
                "</a></li></ul></div>";
                break;
            case "underreview":
            case "under review":
                content = "<div class=\"btn-group dropdown-menu-actions\"><a href=\"#\" data-toggle=\"dropdown\"><i class=\"glyphicon glyphicon-cog\"></i></a>" +
                "<ul class=\"dropdown-menu\"><li><a data-target=\"#modalReAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-user\"></i>Re-Assign</a></li>" +
                "<li><a data-target=\"#modalUnAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-minus-square-o\"></i>Un-Assign</a></li>" +
                "</ul></div>";
                break;
            default:
                break;
        }
        //console.log('content:' + content);
        return content;
    }

    function IPadGridActionColumn(ID, iPadNumber, assignedToName, BusinessName, userID, barcodeGroup, isAssigned, isActivated) {

        var content = "";
        //fix for safari
        var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex=\"" + ID + "\"" : "";
        if (isActivated) {
            content = "<div class=\"btn-group dropdown-menu-actions\">" +
                            "<a id=\"IPadActions" + ID + "\" data-toggle=\"popover\" tabindex=\"" + ID + "\" data-trigger=\"focus\"  href=\"#\">" +
                            "<i class=\"glyphicon glyphicon-cog\"></i>" +
                            "</a><div class=\"popover-menu\"><ul>";
            if (isAssigned) {
                content += "<li><a id=\"PrintDeviceLabel" + ID + "\" href=\"/default/DeviceLabelGen?deviceID=" + iPadNumber + "&businessName=" + assignedToName + "&regNumber=" + BusinessName + "&userID=" + userID + "&barcodeGroup=" + barcodeGroup + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Print Label</a></li>";
                content += "<li><a id=\"UnAssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalUnassigniPad\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\"  data-assignedto=\"" + BusinessName + "\"><i class=\"fa fa-minus-square-o\"></i> Unassign</a></li>";
            }
            else {
                content += "<li><a id=\"AssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalAssigniPad\"><i class=\"fa fa-plus-square-o\"></i> Assign</a></li>";
            }
            content += "<li><a id=\"DactivateAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalDeactivateiPad\"><i class=\"fa fa-remove\"></i> Deactivate</a></li>";
            content += "</ul></div></div>";
        }

        return content;
    }

    $('#SearchRegistrant').on('keyup', function (e) {
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36)) //End Home
        { return; }
        var str = $(this).val();
        if (str.length >= 1) {
            tableRegistrant.search(str).draw(false);
            $(this).css('visibility', 'visible');
            $('#txtFoundRegistrant').css('display', 'block');
            var url = Global.Remittance.RegistrantExportUrl.replace('----', $('#' + _txtSearchRegisId).val());
            $('#' + _exportTransId).attr('href', url);
            $('#txtFoundRegistrant').siblings('.remove-icon').show();
        }
        else {
            $('#txtFoundRegistrant').siblings('.remove-icon').hide();
            $('#' + _txtSearchRegisId).removeAttr('style');
            $('#txtFoundRegistrant').css('display', 'none');
            var url = Global.Remittance.RegistrantExportUrl.replace('----', $('#' + _txtSearchRegisId).val());
            $('#' + _exportTransId).attr('href', url);
            tableRegistrant.search(str).draw(false);
        }
    });

    $('#SearchApplications').on('keyup', function (e) {
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36)) //End Home
        { return; }

        console.log('e:' + e.key);
        var str = $(this).val();
        if (str.length >= 1) {
            tableApplication.search(str).draw(false);
            //$(this).css('visibility', 'visible');
            //$('#txtFoundApplications').css('display', 'block');
            //var url = Global.Remittance.ApplicationExportUrl.replace('----', $('#' + _txtSearchAppId).val());
            //$('#' + _exportAppId).attr('href', url);
            $('#txtFoundApplications').siblings('.remove-icon').show();
            //console.log('$(this).siblings(".remove-icon").show(); str:' + str + ' url:' + url);
        }
        else {
            $('#txtFoundApplications').siblings('.remove-icon').hide();
            //$('#' + _txtSearchAppId).removeAttr('style');
            //$('#txtFoundApplications').css('display', 'none');
            //var url = Global.Remittance.ApplicationExportUrl.replace('----', $('#' + _txtSearchAppId).val());
            //$('#' + _exportAppId).attr('href', url);
            //console.log('tableApplication.search(str).draw(false); str:' + str + ' url:' + url);
            tableApplication.search(str).draw(false);
        }
    });

    $('#glyphicon-search-App').on('click', function () {
        var searchValue = $('#SearchApplications').val();
        tableApplication.search(searchValue).draw();
    });

    $('#glyphicon-remove-App').on('click', function () {
        tableApplication.search("").draw();
    });

    $('#glyphicon-search-Reg').on('click', function () {
        var searchValue = $('#SearchRegistrant').val();
        tableRegistrant.search(searchValue).draw();
    });

    $('#glyphicon-remove-Reg').on('click', function () {
        tableRegistrant.search("").draw();
    });

    $.ajax({
        url: '/System/Common/GetUsersInAuditGroup',
        method: 'GET',
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, data) {
                $('.listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')//value:ID text:name text
            });
            $('.listOfStaff').prop('selectedIndex', 0);
        },
        failure: function (data) {

        },
    });

    $("#listOfStaffAssign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Assign").removeAttr("disabled");
            $("#Btn-Assign").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#Btn-Assign").attr("disabled", "disabled");
        }
    });

    $("#listOfStaffRe-Assign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Re-Assign").removeAttr("disabled");
            $("#Btn-Re-Assign").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#Btn-Re-Assign").attr("disabled", "disabled");
        }
    });

    $('.workflow').on('click', function () {
        console.log('#assignActionURL:' + $('#assignActionURL').text());
        var url = $('#assignActionURL').val();
        var Token = $('#selectedApplicationToken').val();
        var ID = $('#selectedApplicationID').val();
        var isAssigningToMe = false;

        var userID = 0;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }

        //assign to me
        if ((!url) && ((typeof $(this).attr('data-assgnmeurl') != 'undefined'))) {
            url = $(this).attr('data-assgnmeurl');
            ID = $('#assgnToMeApplicationID').val();
            isAssigningToMe = true;
        }

        //
        var status = $(this).attr('data-status');
        //if (status != "") {
        //    if (status.toLowerCase() == 'backtoapplicant') {
        //        //as per OTSTM-864 clear all fields that have their checkbox unchecked 

        //        $('input[type="checkbox"]:not(:checked)').each(function () {
        //            clearField(this);
        //        });

        //        $('#hdnExplicitSave').trigger('click');//saves data                
        //    }
        //    //let model save first then change status
        $('#SuccessAssignToMe').hide();
        $('#NoDataassignToMe').hide();
        $('#assignToMeTitle').html(' ');
        if (ID > 0) {
            setTimeout(function () {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: "JSON",
                    data: { applicationId: ID, status: status, userID: userID },
                    success: function (data) {
                        console.log('.workflow ajax success!!');
                        $('#assignToMeTitle').html('Assigned');
                        $('#SuccessAssignToMe').show();
                        (typeof $(this).attr('data-assgnmeurl') == 'undefined') //return from action gear, reload window.
                        {
                            //debugger;
                            if (!isAssigningToMe) window.location.reload(true);
                            //tableRegistrant.draw(true);
                        }
                    },
                    failure: function (data) {
                        console.log('.workflow ajax Failed!!');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if (xhr.status == 404) {
                            console.log('.workflow ajax Failed!!' + thrownError);
                            $('#assignToMeTitle').html('Not able to Assign application to Me');
                        }
                    },
                });
            }, 1000);
        } else {
            $('#assignToMeTitle').html('Warning');

            $('#NoDataassignToMe').show();

        };
        
    });

    $('#modalReAssign').on('shown.bs.modal', function () {
        var strUser = $("#listOfStaffRe-Assign option:selected").text() + ' ' + $("#listOfStaffRe-Assign option:selected").val();
        $("#Btn-Re-Assign").attr("data-assignuser", $("#listOfStaffRe-Assign option:selected").val());
        console.log($("#listOfStaffRe-Assign option:selected").text() + ' ' + $("#listOfStaffRe-Assign option:selected").val());
        //
    });

    $('#modalAssign').on('shown.bs.modal', function () {
        $("#Btn-Assign").attr("data-assignuser", $("#listOfStaffAssign option:selected").val());
        console.log($("#listOfStaffAssign option:selected").text() + ' ' + $("#listOfStaffAssign option:selected").val());
    });


    (function () {
        var settings = {
            trigger: 'hover',
            //title:'Pop Title',
            //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
            //width:300,
            multi: true,
            closeable: true,
            style: '',
            delay: { show: 300, hide: 800 },
            padding: true
        };


        // Popover: Participant Information
        var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
            popoverParticipantInfoSettings = {
                content: popoverParticipantInfoContent,
                width: 270,
            };
        var popLargeGenerator = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));//merge settings and popoverParticipantInfoSettings, without modify settings object; content set to $('.popoverParticipantInfo').webuiPopover()


    })();

})

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}