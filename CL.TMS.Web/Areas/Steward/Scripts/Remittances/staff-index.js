
//----Steward Remittance participant-index.js
var settings = {
    trigger: 'hover',
    //title:'Pop Title',
    //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
    //width:300,
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};

var popoverClassCategoryContent = $('#popoverClassCategory').html();
var popoverClassCategorySettings = {
    content: popoverClassCategoryContent,
    width: 300,
    height: 100,
};

var penaltiesPopover = {
    content: $('#penalties-popover').data("popovertext"),
    width: 300,
    height: 60,
};

// Popover: Participant Information
var popoverParticipantInfoContent = $('#popoverParticipantInfo').html();
var popoverParticipantInfoSettings = {
    content: popoverParticipantInfoContent,
    width: 270,
};

RemittancesDetails();

$(function () {
    //initialPage();
});

var initialPage = function () {
    var statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];
    TSFRemittance();
    TireCount();
    CheckRolePermissions();
    $('#tdedit').css('display', 'none');
    $('#tdAdd').css('display', 'block');
    $.fn.EnableDisableAddButton();
    var initialize = function () {
    }
    initialize();
    setExportUrl();
    formatDecimalValues();
    RunBusinessRules();

    checkRequiredFields();

    if (hdHasSuppDocsAccess == "True") {
        PanelGreenCheckSuppDoc();
    }

    //true when Custom or Edit
    if (Global.Remittance.AllowEdit === 'False') { //disable page edit
        $(".btn:not(.btn-navigation):not(.admin-btn)").addClass("disabled");
        //$(".export").prop("disabled", true).addClass("disabled").attr('href', "javascript:void(0);");
        //$(".uiaction-export").addClass("disabled");

        //$(".form-control").addClass("disabled").prop("disabled", true);
        $("#fileUploadInput").hide();
        //$("input.radioPaymentType").prop("disabled", true);
        //$("input.radioCurrencyType").prop("disabled", true);
    }

    //OTSTM2-21
    var PeriodDate = $("#PeriodDate").val();
    if (PeriodDate != "") {
        var dateArray = PeriodDate.split(' ');
        var dt = new Date();
        var currentYear = dt.getFullYear();
        if (dateArray[1] != currentYear) {
            $("#StewardSetting").removeAttr("title");
            $("#StewardSetting").find(".btn").addClass("disabled").prop("disabled", true);
        }
    }

    $('#CreditRatePeriodDate').val('');

    //OTSTM2-485 Added for payment panel refresh --begin
    if ($("#fmPayments table td + td").hasClass("disable-click")) {
        $("#ChequeReferenceNumber").css("background-color", "#eee");
        $("#PaymentAmount").css("background-color", "#eee");
    }
    $("#ChequeReferenceNumber").removeAttr("readonly");
    $("#PaymentAmount").removeAttr("readonly");

    $('#IsPenaltyOverride').change(function () {
        $('#Penalties').prop("disabled", true);
        $('#Penalties').trigger("change", ["IsPenaltyOverride_change"]);
    });

    $('#IsTaxApplicable').change(function () {
        if ($("#ClaimID").val() > 0) {
            $.ajax({
                url: '/Steward/TSFRemittances/UpdateHST',
                dataType: 'json',
                type: 'POST',
                data: { tsfClaimId: $("#ClaimID").val(), IsTaxApplicable: $('#IsTaxApplicable').prop('checked') },
                success: function (data) {
                    location.reload();
                },
                failure: function (data) {
                    console.log(data);
                },
                complete: function (data) {
                    $.ajax({
                        url: '/Steward/TSFRemittances/GetTSFRemittanceModel',
                        dataType: 'json',
                        type: 'GET',
                        data: { ClaimId: $("#ClaimID").val() },
                        complete: function (result) {
                            $("#TSFRemittanceStaffPaymentPanel").html(result.responseText);
                            if (!penaltiesPopover.content) {
                                penaltiesPopover.content = $('#penalties-popover').data("popovertext");
                            }
                            $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({}, settings, penaltiesPopover));
                        },
                    });
                },
            });
        }
    });
    if (!$('#IsTaxApplicable').prop('checked')) {
        $("#ApplicableTaxesHst").val("N/A");
    }

    IsDeleteVisible();
}

function SetDeleteVisibility(val) {
    if (val)
        $('#btnDelete').show();
    else
        $('#btnDelete').hide();
}

var hdHasSuppDocsAccess = $('#hdHasSuppDocsAccess').val();

function CheckRolePermissions() {

    var hdEditPermissionTSFRemittanceStatus = $('#hdEditPermissionTSFRemittanceStatus').val();
    var hdEditPermissionTSFRemittancePeriod = $('#hdEditPermissionTSFRemittancePeriod').val();
    var hdEditPermissionTSFRemittanceTireCounts = $('#hdEditPermissionTSFRemittanceTireCounts').val();
    var hdEditPermissionTSFRemittanceCredit = $('#hdEditPermissionTSFRemittanceCredit').val();
    var hdEditPermissionTSFRemittancePayments = $('#hdEditPermissionTSFRemittancePayments').val();
    //var hdEditPermissionTSFRemittanceSuppDocs = $('#hdEditPermissionTSFRemittanceSuppDocs').val();

    //OTSTM2-1110 comment out
    //if (hdEditPermissionTSFRemittanceStatus == "False") {
    //    $("#lblAudit").addClass("disable-click");
    //}

    if (hdEditPermissionTSFRemittancePeriod == "False") {
        //Period on Top
        $("#PeriodDate").attr("disabled", true);
        $("#Calender").addClass("disable-click")
    }


    if (hdEditPermissionTSFRemittanceTireCounts == "False") {
        //Tire counts
        $("#TSFRemittance_CategoryId").attr("disabled", true);
        $("#TSFRemittance_Quantity").prop("disabled", true);
        $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
        $("#Add").addClass("disable-click");
        $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);  //OTSTM2-457
    }
    if (hdEditPermissionTSFRemittanceCredit == "False") {
        //Credit panel
        $("#CreditRatePeriodDate").prop("disabled", true);
        $('#dpCreditRatePeriodDate').attr("style", "pointer-events: none; cursor: not-allowed;");
        $("#Credit_TSFRemittance_CategoryId").attr("disabled", true);
        $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
        $("#AddCredit").addClass("disable-click");
        $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
    }


    if (hdEditPermissionTSFRemittancePayments == "False") {
        //Payments
        $("#fmPayments table td + td").addClass("disable-click");
        $("input.radioPaymentType").prop("disabled", true);
        $("input.radioCurrencyType").prop("disabled", true);
    }

    //$("#btnSection").addClass("disable-click");    
}

function RunBusinessRules() {

    $('#negativeAdj').toggle(false);

    //1 . Old Rule if there is any -ve adj in tire counts panel, supp doc required
    if ($("#TSFRemittance_NegativeAdjustment").length != 0) {
        //$('#negativeAdj').toggle(false);
        $("#tblTireCount tr").each(function (e) {

            var negAdjValue = $(this).find("td").eq(2).find(".negAdj").html();

            //If june 2016 credit is not there but Period is there
            var dt1 = new Date($("#PeriodDate").val());
            var dt2 = new Date($('#tsfMonthYearSelectionDate').val());

            if (dt1.getUTCFullYear() == dt2.getUTCFullYear() && dt1.getUTCMonth() == dt2.getUTCMonth() && $("#SRemitsPerYears").val() == "False") {
                negAdjValue = $(this).find("td").eq(3).find(".negAdj").html();

            }

            if (negAdjValue != null && negAdjValue.indexOf("-") != -1) {
                $('#negativeAdj').toggle(true);
                return;
            }
        });
    }

    //absolute
    var credit = parseFloat($("#spnCredit").text().replace(',', '').replace('-', ''));

    //absolute
    //var tsfDue = parseFloat($("#spnTotal").text().replace(',', '').replace('-', ''));

    //OTSTM2-1315, the business rules should check against Tire counts now
    var totalRemittancePayable = $("#hdTotalRemittancePayable").val();
    var tsfDue = parseFloat(totalRemittancePayable.replace(',', '').replace('-', ''));


    //2. TSF Due > Credit, Credit > $199.99 (Steward is paying but credit is too high)  
    $("#tblCredit tr").each(function (e) {

        var negAdjValue = $(this).find("td").eq(2).find(".negAdj").html();
        if (negAdjValue != null && negAdjValue.indexOf("-") != -1 && tsfDue > credit && credit >= 200) {
            $('#negativeAdj').toggle(true);
            return;
        }
    });

    //3. Credit > TSF Due (OTS needs to pay back to Steward)
    if (credit > tsfDue) {
        $('#negativeAdj').toggle(true);
        return;
    }

    //4. TSF Due > 0, Credit > 0, TSF Due = Credit  (Steward is not paying anything)
    if (tsfDue > 0 && credit > 0 && credit == tsfDue) {
        $('#negativeAdj').toggle(true);
        return;
    }
}


//requireSupportDocV2() is for OTSTM2-478 base on below URL
//https://jira.centrilogic.com/wiki/display/CGI/New+TM+-+Stewards  "The Business rules were updated to consider TSF Due instead of Balance Due for the business rules"
//#16.
//a.If the TSF Due is greater than Credit and Credit is greater than 199.99$ supporting documents will be mandatory for both staff and participant to submit the remittance.
//  Internal notes will be created when participant submits the remittance with supporting documents attached.
//b.If the Credit is greater than TSF Due supporting documents will be mandatory for both staff and participant to submit the remittance.
//c.If the Credit = TSF Due but both are greater than 0  supporting documents will be mandatory for both staff and participant to submit /Approve the remittance.
//d.If the TSF Due is greater than Credit but the Credit is less than or equal to 199.99 $  supporting documents will not be mandatory for both staff and participant to submit the remittance.
//e.For Nill activity remittance where TSF Due= Credit =0  supporting documents will not be mandatory for both staff and participant to submit the remittance.

var requireSupportDocV2 = function () {
    //var tsfDue = parseFloat($("#spnTotal").text().replace(',', '').replace(" ", ""));
    //OTSTM2-1315, the business rules should check against Tire counts now
    var totalRemittancePayable = $("#hdTotalRemittancePayable").val();
    var tsfDue = parseFloat(totalRemittancePayable.replace(',', '').replace('-', ''));

    var Credit = parseFloat($('#spnCredit').text().replace(",", "").replace(" ", ""));
    Credit = Math.abs(Credit);//(Credit on UI shows negative number)
    var BalanceDue = parseFloat($('#spnBalDue').text().replace(",", "").replace(" ", ""));
    var tireCounts = $('#tblTireCount tr').length;

    if ((tsfDue > Credit) && (Credit >= 200)) {//a.
        return true;
    }
    if (Credit > tsfDue) {//b.
        return true;
    }
    if (Credit > 0 && tsfDue > 0 && Credit == tsfDue) {//c.
        return true;
    }
    return false;
}

var checkRequiredFields = function () {
    //var tsfDue = parseFloat($("#spnTotal.spn-tsfdue").text().replace(',', '').replace('-', ''));
    //OTSTM2-1315, the business rules should check against Tire counts now
    var totalRemittancePayable = $("#hdTotalRemittancePayable").val();
    var tsfDue = parseFloat(totalRemittancePayable.replace(',', '').replace('-', ''));

    var credit = parseFloat($("#spnCredit.spn-credit").text().replace(',', '').replace('-', '')); //OTSTM2-1039
    var paymentadj = parseFloat($("#spnTotal.spn-paymentadj").text().replace(',', '').replace('-', '')); //OTSTM2-1039

    if (hdHasSuppDocsAccess == "True") {
        var remittanceSupportDocIsReady = (($('#currUploadFiles').val() > 0) || !requireSupportDocV2());//support doc already uploaded || not required

        if (!remittanceSupportDocIsReady) {
            $('#flagSupportingDocuments').attr('src', statuses[0]);//statuses = ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];
        }
    }

    //OTSTM2-413
    var period = new Date($("#PeriodDate").val());
    var now = new Date();
    var isCurrentOrFutureReportPeriod = false;
    if (period.getUTCFullYear() > now.getUTCFullYear()) {
        isCurrentOrFutureReportPeriod = true;
    }
    else if (period.getUTCFullYear() == now.getUTCFullYear()) {
        if (period.getMonth() >= now.getMonth()) {
            isCurrentOrFutureReportPeriod = true;
        }
    }


    //this check means the fields are visible
    if ($("#dpPaymentDepositDate").length) {
        if ($("#PayReceiptDate").val() == '' || $("#PayDepositDate").val() == '' || $("#ChequeReferenceNumber").val() == '' || $("#PaymentAmount").val() == '' || ($('input[name="PaymentType"]:checked').val() == null && (tsfDue != 0 || credit != 0 || paymentadj != 0)) || ($('input[name="CurrencyType"]:checked').val() == null && tsfDue != 0) || !remittanceSupportDocIsReady || isCurrentOrFutureReportPeriod) {//OTSTM2-478 OTSTM2-413
            $("#btnApprove").prop("disabled", true);// Disable #btnApprove
        }
        else {
            $("#btnApprove").prop("disabled", false);//Enable #btnApprove
        }
    }
    else {
        if (isCurrentOrFutureReportPeriod) {
            $("#btnApprove").prop("disabled", true);// Disable #btnApprove
        }
        else {
            $("#btnApprove").prop("disabled", false);//Enable #btnApprove
        }
    }
}

var dynamicErrorMsg = function (params, element) {
    var labelName = 'Field';
    if ($(element).prev('label').html()) {
        labelName = stripHtml($(element).prev('label').html());
    }
    else if ($(element).parent('div.dropdown').prevAll('label')) {
        labelName = stripHtml($(element).parent('div.dropdown').prevAll('label').html());
    }
    return 'Invalid ' + labelName;
};

$("#CreditRatePeriodDate").on("change", function () {
    if ($("#CreditRatePeriodDate").attr('value') == $("#CreditRatePeriodDate").val()) {
        return; //month not changed.
    }
    $("#CreditRatePeriodDate").attr('value', $("#CreditRatePeriodDate").val());

    //if ($("#Credit_TSFRemittance_CategoryId").val() > 0) {
    var CreditRatePeriodDate = $("#CreditRatePeriodDate").val();

    //OTSTM2-457, enhance user experience, start--     
    $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
    $("#TSFRemittance_NegativeAdjustment").val('0');
    $.fn.showHideReasonDiv();
    $("#TSFRemittance_AdjustmentReasonType").val("");
    $("#td_CreditRate").text("0.00");
    $.fn.CreditAllCalculations();
    //--end--

    $.ajax({
        url: '/Steward/TSFRemittances/LoadCategoryByDate',
        method: 'GET',
        dataType: "JSON",
        data: { claimId: $('#ClaimID').val(), date: CreditRatePeriodDate },
        complete: function (result) {
            //$('#Credit_TSFRemittance_CategoryId').find('option').remove();
            //$('#Credit_TSFRemittance_CategoryId').append('Select Class/Category');
            //for (var i = 0; i < result.responseJSON.length; i++) {//rebuild category dropdown 
            //    var $select = $('#Credit_TSFRemittance_CategoryId');
            //    $select.append(
            //        $('<option/>', {
            //            value: result.responseJSON[i].Value,
            //            html: result.responseJSON[i].Text,
            //        })
            //     );
            //}
            //$("#Credit_TSFRemittance_CategoryId").change();

            //OTSTM2-457
            $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
            $("#Credit_TSFRemittance_CategoryId").prop("disabled", false);
            $('#Credit_TSFRemittance_CategoryId').empty();
            var $select = $('#Credit_TSFRemittance_CategoryId');
            $select.append('<option value="0">Select a Category</option>');

            if (result.responseJSON) {
                for (var i = 0; i < result.responseJSON.length; i++) {
                    $select.append(
                            $('<option/>', {
                                value: result.responseJSON[i].Value,
                                html: result.responseJSON[i].Text,
                            })
                    );
                }
            } else {
                if (result.responseText) {
                    //document.documentElement.innerHTML = result.responseText;
                    //document.write(result.responseText);
                    //document.close();
                }
            }
        }
    });

    //}
});


function formatDecimalValues() {
    var penalties = $("#Penalties").val();
    var ApplicableTaxesHst = $("#ApplicableTaxesHst").val();
    $('#IsPenaltyOverride').data('currentPenalty', penalties);

    var totalRemittancePayable = $("#TotalRemittancePayable").val();
    var spnBalDue = $("#spnBalDue").text();
    var paymentAmount = $("#PaymentAmount").val();
    var chequeReferenceNumber = $("#ChequeReferenceNumber").val();
    if (penalties != '') {
        $("#Penalties").val(addCommas(parseFloat(penalties).toFixed(2)));
    }

    if (ApplicableTaxesHst != '') {
        $("#ApplicableTaxesHst").val(addCommas(parseFloat(ApplicableTaxesHst).toFixed(2)));
    }

    if (totalRemittancePayable != '') {
        $("#TotalRemittancePayable").val(parseFloat(totalRemittancePayable).toFixed(2));
    }
    if (paymentAmount != '' && paymentAmount != "0") {
        $("#PaymentAmount").val(addCommas(parseFloat(paymentAmount).toFixed(2)));
    }
    else {
        $("#PaymentAmount").val("0.00");
    }
    if (spnBalDue != '') {
        //$("#spnBalDue").text(parseFloat(spnBalDue).toFixed(2));
    }

    $('#Penalties').prop("disabled", true);
    $('#ApplicableTaxesHst').prop("disabled", true);
}

function setExportUrl() {
    var url = Global.ExportUrl.TireCountsExportUrl;
    var urlPayments = Global.ExportUrl.PaymentsExportUrl;
    var urlStatus = Global.ExportUrl.StatusExportUrl;
    $('#tireCountsExport').attr('href', url);
    url = Global.ExportUrl.CreditExportUrl;
    $('#creditExport').attr('href', url);
    $('#exportPayments').attr('href', urlPayments);
    $('#exportStatus').attr('href', urlStatus);

}

function CloseEdit() {
    window.location.reload(true);
}

function TireCount() {
    var $select = $('#ddlTireList');

    //Add for 2X
    var dt1 = new Date($("#PeriodDate").val());
    var dt2 = new Date($('#tsfMonthYearSelectionDate').val());
    var dt3 = new Date($('#tsfNegAdjSwitchDate').val()) //add for 2x June
    if ($("#divStatus").text() != "Unknown") {
        $("#PeriodDate").attr("disabled", true);
        $("#Calender").addClass("disable-click")
        $("#btnSection").removeClass("disable-click")
    }
    else {
        $("#PeriodDate").attr("disabled", false);
        $("#Calender").removeClass("disable-click")
        $("#btnSection").addClass("disable-click")
    }

    if ($("#divStatus").text() == "Open" || $("#divStatus").text() == "Approved" || $("#divStatus").text() == "Unknown") {
        $("#TSFRemittance_CategoryId").prop("disabled", true);
        $("#Add").addClass("disable-click");
        $("#AddCredit").addClass("disable-click");
        $(".dropdown-menu-actions:not(.only-for-internal-adjustment)").addClass("disable-click"); //OTSTM2-485 added not()
        //$("#panelSupportingDocuments").addClass("disable-click");//enable upload for OTSTM2-173
        $("#fmPayments table td + td").addClass("disable-click");

    }
    else if (dt1 >= dt2 && $("#SRemitsPerYears").val() == "False") {//Add for 2X
        $("#TSFRemittance_CategoryId").prop("disabled", true);
        $("#TSFRemittance_Quantity").prop("disabled", true);
        if (dt1 < dt3) { //Add for 2X June
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);  //OTSTM2-457
        }
        else {
            $("#Credit_TSFRemittance_CategoryId").prop("disabled", true); //OTSTM2-457
            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
    }
    else {
        $("#TSFRemittance_CategoryId").removeProp("disabled");
        $("#TSFRemittance_Quantity").prop("disabled", true); //OTSTM2-377
        if (dt1 < dt3) { //OTSTM2-377
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
        else {
            $("#Credit_TSFRemittance_CategoryId").prop("disabled", true); //OTSTM2-457
            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true); //OTSTM2-457
        }
        $("#Add").removeClass("disable-click");
        $(".dropdown-menu-actions").removeClass("disable-click");
        $("#panelSupportingDocuments").removeClass("disable-click");
        $("#fmPayments table td + td").removeClass("disable-click");

    }

    if ($("#divStatus").text() == "Unknown") $("#divStatus").text("");
    if ($("#divStatus").text() == "Open") $("#submitRemittanceClaimBtn").hide();

    $select.append('<option value="0">Select a Category</option>');

    $('#item').change(function () {
        var itemID = $(this).val();
        $.ajax({
            url: '/Steward/TSFRemittances/GetRemittanceRateByItemId',
            method: 'POST',
            dataType: "JSON",
            data: { itemID: itemID },
            complete: function (data) {

            }
        });

    });
    if ($('#chkterms').is(":checked")) {

        $('#btnsubmit').prop("disabled", false);

    }
    else {
        $('#btnsubmit').prop("disabled", true);


    }

}

function HideEditForm(ShowID, HideID) {

    $(ShowID).show()
    $(HideID).hide()

}

function TSFRemittance() {

    $("#td_Net").text("0.00");
    $("#td_Rate").text("0.00");
    $("#td_Total").text("0.00");
    $("#td_CreditRate").text("0.00");
    $("#td_CreditTotal").text("0.00");


    if (Global.Remittance.AllowEdit === 'True') {
        $('#dpPeriodDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: 'MM yyyy',
            maxDate: "0",
            minDate: new Date(2009, 08, 1),
          //  altFormat: 'dd/mm/yyyy',
            altField: '.set',
            startDate: $("#hfPeriodStartDate").val(),
            endDate: new Date(),
        });

        $('#dpPaymentReceivedDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            startDate: "2009-09-01",
            format: 'yyyy-mm-dd',
            endDate: new Date() //OTSTM2-56
        });
        //$('#PayReceiptDate').addClass("handcursor"); //OTSTM2-485 comment out

        $('#dpPaymentDepositDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            startDate: "2009-09-01",
            format: 'yyyy-mm-dd',
            endDate: new Date() //OTSTM2-56
        });
        //$('#PayDepositDate').addClass("handcursor"); //OTSTM2-485 comment out

        var periodDate = new Date($("#PeriodDate").val());
        var y = periodDate.getFullYear();
        var m = periodDate.getMonth();
        var endDate = new Date(y, m + 1, 0);
        //var d = new Date($("#PeriodDate").val());
        //formatDate(d);
        //var mm = d.getMonth() ;
        //var dd = d.getDate();
        //var yy = d.getFullYear();

        //var endDate = yy + '-' + mm + '-' + dd;

        ////var end = "'+" + mm + "m'";

        ////alert(end);

        $('#dpCreditRatePeriodDate').datetimepicker({
            minView: 2,
            showOn: 'focus',
            autoclose: true,
            format: "mm-yyyy",
            viewMode: "months",
            startView: 'year',
            minViewMode: "months",
            startDate: "2009-09-01",
            //endDate: formatDate(d)
            endDate: endDate
        });
        //$('#dpCreditRatePeriodDate').datetimepicker('update', new Date());

        if (Global.Remittance.IsCreatedByStaff) {//OTSTM2-56
            $('#dpSubmittedDate').datetimepicker({
                minView: 2,
                showOn: 'focus',
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: "2009-09-01",
                endDate: new Date()
            });
            $('#SubmittedDate').addClass("handcursor");
        }
        else {
            $('#dpSubmittedDate').find('span').addClass("blocked");
        }
    }

    //Add for 2X
    $("#TSFRemittance_TSFRateDate").change(function () {

        //OTSTM2-457, enhance user experience, start--
        $("#TSFRemittance_Quantity").prop("disabled", true);
        $("#TSFRemittance_Quantity").val('0');
        if ($('#TireCountsTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) {
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);
            $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).val('0');
            $.fn.showHideReasonDiv();
            $("#TSFRemittance_AdjustmentReasonType").val("");
        }
        $("#td_Rate").text("0.00");
        $.fn.allCalculations();
        //--end--

        if ($("#TSFRemittance_TSFRateDate option:selected").val() != '') {
            var PeriodDate = $("#TSFRemittance_TSFRateDate option:selected").text();
            $.ajax({
                url: '/Steward/TSFRemittances/LoadTireListBasedOnTSFRateDate',
                dataType: 'json',
                type: 'GET',
                data: { PeriodDate: PeriodDate, ClaimID: $("#ClaimID").val() },
                success: function (result) {
                    $("#TSFRemittance_CategoryId").prop("disabled", false);
                    //$("#TSFRemittance_Quantity").prop("disabled", false); //OTSTM2-377 comment out
                    //$("#TSFRemittance_NegativeAdjustment").prop("disabled", false);//add for 2x June //OTSTM2-377 comment out
                    $('#TSFRemittance_CategoryId').empty();
                    var $select = $('#TSFRemittance_CategoryId');
                    $select.append('<option value="0">Select a Category</option>');

                    for (var i = 0; i < result.Tirelist.length; i++) {
                        $select.append(
                              $('<option/>', {
                                  value: result.Tirelist[i].ID,
                                  html: result.Tirelist[i].Name
                              })
                        );
                    }
                },
                complete: function (result) {
                    if (result.responseText) {
                        //document.write(result.responseText);
                    }
                },
            });
        }
        else {
            $("#TSFRemittance_CategoryId").prop("disabled", true);
            $("#TSFRemittance_Quantity").prop("disabled", true);
            $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);//add for 2x June
            $('#TSFRemittance_CategoryId').empty();
            var $select = $('#TSFRemittance_CategoryId');
            $select.append('<option value="0">Select a Category</option>');
            $("#TSFRemittance_Quantity").val('0');
            $("#td_Rate").text("0.00");
            $("#td_Total").text("0.00");
            $("#btnAdd").prop("disabled", true);
        }

    });

    $('#modalOpenRemittance').on('hidden.bs.modal', function () {
        console.log($("#PeriodDate").val());
        if (Global.Remittance.bResetDTpicker) {
            $("#PeriodDate").val('');
        }
    });

    $("#PeriodDate").change(function () {
        console.log($("#PeriodDate").val());
        if ($("#PeriodDate").val() == '') {
            return;
        }
        var pickupDateTime = $("#dpPeriodDate").data("datetimepicker").getDate().toISOString();
        $("#PeriodDate").attr("datedata", pickupDateTime);
        var PeriodDate =$(".set").val(); //$('#dpPeriodDate').data("datetimepicker").getDate().toISOString(); 
        $('#periodName').text(PeriodDate); 
        $('#modalOpenRemittance').modal('show');
    });

    //Bind Item Category Behalf of Selected period
    $("#btnYesOpenRemittance").click(function (event) {
        //OTSTM2-158 client side check, comment out
        //var activityApprovedDate = new Date(Global.Remittance.ActivityApprovedDate);
        //var activityYear = activityApprovedDate.getFullYear();
        //var activityMonth = activityApprovedDate.getMonth() + 1;
        //var selectedPeriodDate = $('#dpPeriodDate').data("datetimepicker").getDate();
        //var selectedPeriodYear = selectedPeriodDate.getFullYear();
        //var selectedPeriodMonth = selectedPeriodDate.getMonth() + 1;
        //if (Global.Remittance.ActiveStatus == "InActive") {
        //    if (selectedPeriodYear > activityYear) {
        //        $('#modalInactiveStewardStaff').modal('show');
        //    }
        //    else if (selectedPeriodYear == activityYear && selectedPeriodMonth > activityMonth) {
        //        $('#modalInactiveStewardStaff').modal('show');
        //    }
        //    else {
        //        createRemittance();
        //    }
        //}
        //else {
        //    createRemittance();
        //}

        //pass value to backend, do server side check
        //var selectedPeriodDate = $('#dpPeriodDate').data("datetimepicker").getDate().toISOString(); //toISOString() match the Datetime format in controller
        //var activityApprovedDate = new Date(Global.Remittance.ActivityApprovedDate).toISOString();
        //var isActive = Global.Remittance.ActiveStatus == "Active";  
        var dt1 = new Date($("#PeriodDate").attr("datedata"));
        var selectedPeriodDate = dt1.toISOString();
        console.log(selectedPeriodDate)
        $.ajax({
            url: '/Steward/TSFRemittances/RunCreateOrSubmitRules',
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            data: JSON.stringify({ selectedPeriodDate: selectedPeriodDate, customerID: Global.ParticipantClaimsSummary.CustomerId }),
            success: function (result) {
                if (result.canCreateOrSubmit) {
                    createRemittance();
                }
                else {
                    if (result.reason) {
                        $('#createTSFRemittanceMsgStaff').text(result.reason);
                    }
                    $('#modalInactiveStewardStaff').modal('show');
                }
            }
        });
    });

    var createRemittance = function () {
        Global.Remittance.bResetDTpicker = false;//
        var selectedPeriodDate=$('#dpPeriodDate').data("datetimepicker").getDate().toISOString();
        var form = $('fmRemittanceTireCount')
        var data = form.serializeArray();
        data.push({ name: 'SRemitsPerYears', value: $("#SRemitsPerYears").val() });
        data.push({ name: 'PeriodDate', value: selectedPeriodDate });
        //data.push({ name: 'Status', value: 'Submitted' });
        data.push({ name: 'Status', value: 'UnderReview' });//if TSFClaim is created by a staff user, set status == "Under Review", also assign to this user. -OTSTM2-182
        data.push({ name: 'CustomerID', value: $("#hdncustomerID").val() });
        data.push({ name: 'RegistrationNumber', value: $("#hdnregistrationNumber").val() });
        data.push({ name: 'IsCreatedByStaff', value: 'true' }); //OTSTM2-56

        var len = data.length;
        for (var i = 0; i < len; i++) {
            data[i].value = $.trim(data[i].value);
        }
        $.ajax({
            url: '/Steward/TSFRemittances/GetTireListOnPeriodBasis',
            dataType: 'json',
            type: 'POST',
            data: $.param(data),
            success: function (result) {
                if (result.ID == -1) {
                    //alert('You can select June and December Period only in semiannually for '+result.Year+'.');
                    //OTSTM2-21
                    var template = "You can select <strong>June</strong> and <strong>December</strong> only for <strong>" + result.Year + "</strong>.";
                    $('#modalCustomerSettingAlert').modal('show');
                    $('#modalCustomerSettingAlertBody').html(template);
                }
                else {
                    if (result.reason) {
                        console.log(result.reason);
                        $('#createTSFRemittanceMsgStaff').text(result.reason);
                        $('#modalInactiveStewardStaff').modal('show');
                    }
                    else {
                        window.location.href = "/Steward/TSFRemittances/StaffIndex/" + result.ID;
                    }
                }

            },
            failure: function (result) {
                console.log(result);
            }
        });
    }

    //Bind rate behalf of selected Item and Effective Period
    $("#TSFRemittance_CategoryId").change(function () {

        if ($("#TSFRemittance_CategoryId").val() != "0" && $("#TSFRemittance_CategoryId").val() != "" && $("#PeriodDate").val() !="") { //OTSTM2-457 jqury error fixed
            var PeriodDate = $("#PeriodDate").val();
            if ($("#TSFRemittance_TSFRateDate").length != 0) {
                PeriodDate = $("#TSFRemittance_TSFRateDate option:selected").text();
            }
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'GET',
                dataType: "JSON",
                data: { PeriodDate: PeriodDate, ItemID: $("#TSFRemittance_CategoryId").val() },
                complete: function (result) {
                    //for (var i = 0; i < result.responseJSON.length; i++) {
                    //    var Rate = parseFloat(result.responseJSON[i].ItemRate).toFixed(2);
                    //    $("#td_Rate").text(Rate);
                    //    $.fn.allCalculations();
                    //    $.fn.showHideReasonDiv();
                    //    $.fn.EnableDisableAddButton();
                    //}
                    var Rate = parseFloat(result.responseJSON.ItemRate).toFixed(2);
                    $("#td_Rate").text(Rate);
                    $.fn.allCalculations();
                    $.fn.showHideReasonDiv();
                    $.fn.EnableDisableAddButton();

                    $("#TSFRemittance_Quantity").prop("disabled", false); //OTSTM2-377
                    if (!Global.Remittance.bShowCreditPanel) { //OTSTM2-377
                        $("#TSFRemittance_NegativeAdjustment").prop("disabled", false);
                    }
                }
            });
        }
        else {
            //OTSTM2-457, enhance user experience, start--
            $("#TSFRemittance_Quantity").prop("disabled", true);
            $("#TSFRemittance_Quantity").val('0');
            if ($('#TireCountsTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) {
                $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", true);
                $("#TireCountsTable").find($("#TSFRemittance_NegativeAdjustment")).val('0');
                $("#TSFRemittance_AdjustmentReasonType").val("");
            }
            //--end
            $("#td_Rate").text("0.00");
            $.fn.allCalculations();
            $.fn.showHideReasonDiv();
            $.fn.EnableDisableAddButton();
        }

    });

    $("#Credit_TSFRemittance_CategoryId").change(function () {
        if ($("#Credit_TSFRemittance_CategoryId").val() != "0") {
            var CreditRatePeriodDate = $("#CreditRatePeriodDate").val();
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'GET',
                dataType: "JSON",
                data: { PeriodDate: CreditRatePeriodDate, ItemID: $("#Credit_TSFRemittance_CategoryId").val() },
                complete: function (result) {
                    //for (var i = 0; i < result.responseJSON.length; i++) {
                    //    var Rate = parseFloat(result.responseJSON[i].ItemRate).toFixed(2);
                    //    $("#td_CreditRate").text(Rate);
                    //    //$.fn.allCalculations(); 
                    //    $.fn.CreditAllCalculations(); //OTSTM2-457 should be for credit
                    //    $.fn.showHideReasonDiv();
                    //    //$.fn.EnableDisableAddButton(); 
                    //    $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit
                    //}
                    var Rate = parseFloat(result.responseJSON.ItemRate).toFixed(2);
                    $("#td_CreditRate").text(Rate);
                    //$.fn.allCalculations(); 
                    $.fn.CreditAllCalculations(); //OTSTM2-457 should be for credit
                    $.fn.showHideReasonDiv();
                    //$.fn.EnableDisableAddButton(); 
                    $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit

                    $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", false); //OTSTM2-457
                }
            });
        }
        else {
            //OTSTM2-457, enhance user experience, start--
            $("#TSFRemittance_NegativeAdjustment").prop("disabled", true);
            $("#TSFRemittance_NegativeAdjustment").val('0');
            $("#TSFRemittance_AdjustmentReasonType").val("");
            $.fn.CreditAllCalculations();
            //--end
            $("#td_CreditRate").text("0.00");
            //$.fn.allCalculations(); 
            $.fn.showHideReasonDiv(); //OTSTM2-457 should be for credit
            //$.fn.EnableDisableAddButton();
            $.fn.CreditEnableDisableAddButton(); //OTSTM2-457 should be for credit
        }

    });

    //calculation
    $(".Calculation").change(function () {
        $.fn.allCalculations();
        $.fn.showHideReasonDiv();
        $.fn.EnableDisableAddButton();
    });

    $(".CreditCalculation").change(function () {
        $.fn.CreditAllCalculations();
        $.fn.showHideReasonDiv();
        $.fn.CreditEnableDisableAddButton();
    });

    $("#TSFRemittance_AdjustmentReasonType").change(function () {
        $.fn.showHideDiv($(this).val(), ".reasonDesc", 4)
        if (!Global.Remittance.bShowCreditPanel) {//before effective date
            $.fn.EnableDisableAddButton();
        } else {
            $.fn.CreditEnableDisableAddButton();
        }
    });


    $('#TSFRemittance_OtherDesc').change(function () {
        $.fn.EnableDisableAddButton();
    });

    $(".drlOther").click(function () {
        $.fn.showHideDiv(1, ".reasonDesc", 4);

    });

    $.fn.showHideDiv = function (val1, DivId, val2) {

        if (val1 != "" && val2 != "") {

            if (val1 == val2) {
                $(DivId).show();

            }
            else {
                $(DivId).hide();
            }
        }
        else {

            if (val1 > 0) {



                $(DivId).show();

            }
            else {
                $(DivId).hide();
            }
        }
    }


    //Edit method
    $(".edit").click(function (event) {
        event.preventDefault();
        var detId = $(this).attr("id");
        var categoryDate = $(this).closest('tr').children('td.creditPeriodDate').text();
        if (categoryDate == "") {
            categoryDate = $("#PeriodDate").val();
        }
        $.ajax({
            url: '/Steward/TSFRemittances/GetTireListForEdit',
            method: 'GET',
            dataType: "JSON",
            data: { PeriodDate: categoryDate },
            complete: function (result) {
                $('#TSFRemittance_CategoryId').empty();
                var $select = $('#TSFRemittance_CategoryId');
                $select.append('<option value="0">Select a Category</option>');

                for (var i = 0; i < result.responseJSON.Tirelist.length; i++) {
                    $select.append(
                              $('<option/>', {
                                  value: result.responseJSON.Tirelist[i].ID,
                                  html: result.responseJSON.Tirelist[i].Name
                              })
                     );
                }

                $('#Credit_TSFRemittance_CategoryId').empty();
                var $selectCredit = $('#Credit_TSFRemittance_CategoryId');
                $selectCredit.append('<option value="0">Select a Category</option>');

                for (var i = 0; i < result.responseJSON.Tirelist.length; i++) {
                    $selectCredit.append(
                              $('<option/>', {
                                  value: result.responseJSON.Tirelist[i].ID,
                                  html: result.responseJSON.Tirelist[i].Name
                              })
                     );
                }

                $.ajax({
                    url: '/Steward/TSFRemittances/GetClaimDetail',
                    method: 'GET',
                    dataType: "JSON",
                    data: {
                        claimID: $("#ClaimID").val(), detId: detId
                    },
                    complete: function (result) {
                        $('.edit-remove-menu').css('display', 'block');
                        $('.edit-remove-menu').prop('disabled', true);
                        if (result.responseJSON.CreditNegativeAdj == 0) {//edit tire counts
                            //$("#td_Net").text("0.00");
                            $("#td_Rate").text("0.00");
                            $("#td_Total").text("0.00");
                            $("#ClaimDetailID").val(result.responseJSON.DetailID);
                            $("#TSFRemittance_TSFRateDate").val(result.responseJSON.TSFRateDateShortName);//add for 2x
                            $("#TSFRemittance_TSFRateDate").prop('disabled', true);//add for 2x
                            var cid = result.responseJSON.CategoryId.toString();
                            $("#TSFRemittance_CategoryId").val("" + cid + "");
                            $('#TSFRemittance_CategoryId').prop('disabled', true);
                            $("#TSFRemittance_Quantity").val(result.responseJSON.Quantity);
                            $("#TSFRemittance_Quantity").prop('disabled', false);
                            $("#TSFRemittance_NegativeAdjustment").val(result.responseJSON.NegativeAdjustment);
                            $("#TSFRemittance_NegativeAdjustment").prop('disabled', false);//add for 2x June
                            $("#td_Net").text(result.responseJSON.Net);//add for 2x June
                            var Rate = parseFloat(result.responseJSON.Rate).toFixed(2);
                            $("#td_Rate").text(Rate);
                            $("#td_Total").text(result.responseJSON.Total);
                            $('#TSFRemittance_Rate').val(Rate);
                            $('#TSFRemittance_Total').val(result.responseJSON.Total);
                            $('#TSFRemittance_Net').val(result.responseJSON.Net);
                            $('#tdedit').css('display', 'block');
                            $('#tdedit').prop('disabled', true);
                            $('#tdAdd').css('display', 'none');//otherdesc

                            //removed commenting for otstm2-53
                            $('#TSFRemittance_OtherDesc').val(result.responseJSON.OtherDesc);
                            BindAdjustmentReason(result.responseJSON.AdjustmentReasonType);
                        } else { //edit Credit
                            $("#td_CreditRate").text("0.00");
                            $("#td_CreditTotal").text("0.00");
                            $("#ClaimDetailID").val(result.responseJSON.DetailID);
                            var cid = result.responseJSON.CategoryId.toString();
                            $("#Credit_TSFRemittance_CategoryId").val("" + cid + "");
                            $('#Credit_TSFRemittance_CategoryId').prop('disabled', true);
                            $("#TSFRemittance_Quantity").val(result.responseJSON.Quantity);
                            $("#TSFRemittance_NegativeAdjustment").val(result.responseJSON.CreditNegativeAdj);
                            var Rate = parseFloat(result.responseJSON.Rate).toFixed(2);
                            $("#td_CreditRate").text(result.responseJSON.CreditTSFRate);
                            $("#td_CreditTotal").text(result.responseJSON.CreditTSFDue);
                            $('#TSFRemittance_Rate').val(Rate);
                            $('#TSFRemittance_Total').val(result.responseJSON.CreditTSFDue);
                            $('#tdeditCredit').css('display', 'block');
                            $('#tdeditCredit').prop('disabled', true);
                            $('#tdAddCredit').css('display', 'none');//otherdesc
                            $('#CreditRatePeriodDate').val(result.responseJSON.strNegativeAdjDate);
                            $('#CreditRatePeriodDate').prop('disabled', true);
                            $('#dpCreditRatePeriodDate').attr("style", "pointer-events: none; cursor: not-allowed;");
                            $('#TSFRemittance_OtherDesc').val(result.responseJSON.OtherDesc);
                            BindAdjustmentReason(result.responseJSON.AdjustmentReasonType);
                            $("#CreditTable").find($("#TSFRemittance_NegativeAdjustment")).prop("disabled", false); //OTSTM2-457
                        }
                    }
                });
            }
        });
    });

    $.fn.EnableDisableAddButton = function () {

        $("#btnAdd").prop("disabled", true);
        $("#btnupdate").prop("disabled", true);

        $("#tdAddCredit").prop("disabled", true);
        $("#btnAddCredit").prop("disabled", true);
        $("#btnupdateCredit").prop("disabled", true);

        if (!Global.Remittance.bShowCreditPanel) {
            //if ($('#TireCountTable').find($("#TSFRemittance_NegativeAdjustment")).length != 0) { //If there is NegativeAdjustment Column
            if ($('#TSFRemittance_AdjustmentReasonType').is(':visible')) {
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "")
                    return false;
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "4" && $("#TSFRemittance_OtherDesc").val().length <= 0)
                    return false;
            }
            else {
                if ($("#TSFRemittance_NegativeAdjustment").val() != "0") {
                    return false;
                }
            }
        }

        if (!Global.Remittance.bShowCreditPanel) {//before effective date
            if ($("#TSFRemittance_CategoryId").val() == "" || $("#TSFRemittance_CategoryId").val() == "0") {
                return false;
            }
            if (parseFloat($("#TSFRemittance_Quantity").val()) > 0 && $("#TSFRemittance_CategoryId").val() != "") {

                $("#btnAdd").prop("disabled", false);
                $("#btnupdate").prop("disabled", false);
            }
        } else {
            if ($('#TSFRemittance_AdjustmentReasonType').is(':visible')) {//
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "")
                    return false;
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "4" && $("#TSFRemittance_OtherDesc").val().length <= 0)
                    return false;
                $('#btnAddCredit').prop("disabled", false);
            }
            else {
            }
            if ($("#TSFRemittance_CategoryId").val() == "" || $("#TSFRemittance_CategoryId").val() == "0") {
                return false;
            }
            if (parseFloat($("#TSFRemittance_Quantity").val()) > 0 && $("#TSFRemittance_CategoryId").val() != "") {

                $("#btnAdd").prop("disabled", false);
                $("#btnupdate").prop("disabled", false);
            }
            if ($("#Credit_TSFRemittance_CategoryId").val() == "" || $("#Credit_TSFRemittance_CategoryId").val() == "0") {
                return false;
            }

            if (parseFloat($("#TSFRemittance_NegativeAdjustment").val()) > 0 && $("#Credit_TSFRemittance_CategoryId").val() != "") {
                $("#tdAddCredit").prop("disabled", false);
                $("#btnAddCredit").prop("disabled", false);
                $("#btnupdateCredit").prop("disabled", false);
            }
        }
    }

    $.fn.CreditEnableDisableAddButton = function () {
        $("#btnAddCredit").prop("disabled", true);
        $("#btnupdate").prop("disabled", true);
        if ($("#TSFRemittance_NegativeAdjustment").length != 0) { //If there is NegativeAdjustment Column
            if ($('#TSFRemittance_AdjustmentReasonType').is(':visible')) {
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "")
                    return false;
                if ($("#TSFRemittance_AdjustmentReasonType").val() == "4" && $("#TSFRemittance_OtherDesc").val().length <= 0)
                    return false;
            }
            else {
                if ($("#TSFRemittance_NegativeAdjustment").val() != "0") {
                    return false;
                }
            }
        }
        if ($("#Credit_TSFRemittance_CategoryId").val() == "" || $("#Credit_TSFRemittance_CategoryId").val() == "0") {
            return false;
        }

        if (parseFloat($("#TSFRemittance_NegativeAdjustment").val()) > 0 && $('#CreditRatePeriodDate').val() != '') {

            $("#btnAddCredit").prop("disabled", false);
            $("#btnupdateCredit").prop("disabled", false);
        }
    }

    $.fn.allCalculations = function () {
        var Quantity = parseInt($("#TSFRemittance_Quantity").val());

        if (isNaN(Quantity)) {
            Quantity = 0;
        }

        if ($("#TSFRemittance_NegativeAdjustment").val() == "") {
            $("#TSFRemittance_NegativeAdjustment").val("0");
        }
        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());

        if (isNaN(NegativeAdjustment)) {
            NegativeAdjustment = 0;
        }
        if (Quantity == 0) {
            $('#td_Total').text("0.00");
            $("#td_Net").text("0.00");
            $('#TSFRemittance_Total').val("0.00");
        }

        if (!Global.Remittance.bShowCreditPanel) {//before effective date
            if (Quantity <= NegativeAdjustment) {
                if (!(Quantity == 0 && NegativeAdjustment == 0)) {
                    $('#modalQtyGreaterWarning').modal('show');
                }
                $("#TSFRemittance_NegativeAdjustment").val("0");
                NegativeAdjustment = 0;
            }
        }

        var sub = (Quantity - NegativeAdjustment);
        if (!Global.Remittance.bShowCreditPanel) {
            $("#td_Net").text(sub);
            $('#TSFRemittance_Net').val(sub);
        }
        var Net = $("#td_Net").text();
        var Rate = parseFloat($("#td_Rate").text()).toFixed(2);
        var creditRate = parseFloat($("#td_CreditRate").text()).toFixed(2);

        var total = (Quantity * Rate).toFixed(2);
        if (!Global.Remittance.bShowCreditPanel) { //add for 2x June
            total = (sub * Rate).toFixed(2);
        }
        $("#td_Total").text(addCommas(total));
        //$("#td_CreditTotal").text(addCommas(total));

        //var creditTotal = (NegativeAdjustment * creditRate).toFixed(2);
        $('#TSFRemittance_Rate').val(Rate);
        $('#TSFRemittance_Total').val(total);
        $('#TSFRemittance_Quantity').val(Quantity);

        //$('#td_CreditTotal').val(creditTotal);
    }

    $.fn.CreditAllCalculations = function () {
        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());

        if (isNaN(NegativeAdjustment)) {
            NegativeAdjustment = 0;
        }
        if (NegativeAdjustment == 0) {
            $('#td_CreditTotal').text("0.00");
        }
        var creditRate = parseFloat($("#td_CreditRate").text()).toFixed(2);

        var creditTotal = (NegativeAdjustment * creditRate).toFixed(2);
        $("#td_CreditTotal").text(addCommas(creditTotal));
        $('#TSFRemittance_NegativeAdjustment').val(NegativeAdjustment);
    }

    $.fn.showHideReasonDiv = function () {
        $("#tdnegativeAdj").hide();
        $(".reasonDesc").hide();
        if (parseInt($("#TSFRemittance_NegativeAdjustment").val()) > 0) {

            $("#tdnegativeAdj").show();

            if ($("#TSFRemittance_AdjustmentReasonType").val() == "4")
                $(".reasonDesc").show();
        }
    }

    $('.Calculation, .CreditCalculation, .EditCalculation, .totCalculation').on('keydown', function (e) {

        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()

    });

    $("#TSFRemittance_Quantity").on("change", function () {
        $.fn.allCalculations();
        $.fn.showHideReasonDiv();
        $.fn.EnableDisableAddButton();
    });


    function BindAdjustmentReason(adjustmentval) {
        var NegativeAdjustment = parseInt($("#TSFRemittance_NegativeAdjustment").val());
        if (NegativeAdjustment > 0) {
            $('#tdnegativeAdj').removeAttr('style');
            $("#TSFRemittance_AdjustmentReasonType").val(adjustmentval);
            if (adjustmentval == 4) {
                $('.reasonDesc').show();
            }
            else {
                $('.reasonDesc').hide();
                $('.reasonDesc input').val('');
            }
        }
        else {
            $('#tdnegativeAdj').hide();;
        }
    }

    //Comment out by Frank
    //$("#btnsubmit").click(function (event) {
    //    changeStatus("Submitted");
    //});

    //Add by Frank
    $("#submitRemittanceClaimBtn").click(function () {
        if (this.innerHTML.trim() == "Submit $0 Remittance" && $('#hdCBRemittanceNilAutoApprove').val() == "1") {
            $("#btnsubmit").attr("DollarZeroFlag", "On"); ///comment out for May7 release
        }
    });

    $("#btnsubmit").click(function (event) {
        if ($(this).attr("DollarZeroFlag") == "On") {
            changeStatusAuto("Approved");
        }
        else {
            //changeStatus("Submitted");
            changeStatus("UnderReview");//if TSFClaim is created by a staff user, set status == "Under Review", also assign to this user. -OTSTM2-182
        }
    });

    //Not sure why the this function needs to be called (postback)
    //OTSTM2-56
    $("#SubmittedDate,#PayReceiptDate,#PayDepositDate,#ChequeReferenceNumber,#PaymentAmount").change(function (event) {
        changeStatus("");
    });

    $(".radioPaymentType").change(function () {
        //if (this.value != 'C') {
        //    $("#radioCurrencyTypeCAD").prop("checked", true);
        //    $("#radioCurrencyTypeCAD").trigger('change');
        //}
        $.ajax({
            url: '/Steward/TSFRemittances/UpdatePaymentType',
            dataType: 'json',
            data: { claimID: $("#ClaimID").val(), paymentType: this.value },
            type: 'POST',
            success: function (data) {
                if (data) {
                    //if (data.Errors.length == 0) {$('.btn-submit-remittance').removeAttr('disabled');}//requirement rollback
                } else {
                    console.log(data);
                }
            },
            failure: function (data) {
                console.log(data);
            }
        });
        checkRequiredFields();
        if (hdHasSuppDocsAccess == "True") {
            PanelGreenCheckSuppDoc();
        }
    });

    $(".radioCurrencyType").change(function () {
        $.ajax({
            url: '/Steward/TSFRemittances/UpdateCurrencyType',
            dataType: 'json',
            data: { claimID: $("#ClaimID").val(), currencyType: this.value },
            type: 'POST',
            success: function (data) {
                if (data) {
                    //if (data.Errors.length == 0) {$('.btn-submit-remittance').removeAttr('disabled');}//requirement rollback
                } else {
                    console.log(data);
                }
            },
            failure: function (data) {
                console.log(data);
            }
        });
        checkRequiredFields();
        if (hdHasSuppDocsAccess == "True") {
            PanelGreenCheckSuppDoc();
        }
    });

    $("#modalSubmitRemittanceTwo .close").click(function (event) {
        window.location.reload();
    });
    $("#btnOk").click(function (event) {
        window.location.reload();
    });

    $("#btnParticipantsubmit").click(function (event) {

        $.ajax({
            url: '/Steward/TSFRemittances/ChangeStatus',
            dataType: 'json',
            type: 'POST',
            data: { claimID: $("#ClaimID").val(), status: "Open" },
            success: function (data) {
                window.location.href = "/Steward/TSFRemittances"
            },
            failure: function (data) {
                console.log(data);
            },
            complete: function (result) {
                if (result.responseText) {//RedirectResult("~/System/Common/EmptyPage?navigationAction=loadTopMenu");
                    //document.write(result.responseText);
                }
            },
        });

    });

    $("#btnUnderReview").click(function (event) {
        checkRequiredFields();
        var payment = ($("#PaymentAmount").val() == "") ? "0.00" : $("#PaymentAmount").val();
        $.ajax({
            url: '/Steward/TSFRemittances/SetRemittanceUnderReview',
            dataType: 'json',
            type: 'POST',
            data: { claimID: $("#ClaimID").val(), status: 'UnderReview', submitted: $('#SubmittedDate').val(), received: $("#PayReceiptDate").val(), deposit: $("#PayDepositDate").val(), referenceNo: $("#ChequeReferenceNumber").val(), amount: payment, PaymentType: $('input[name="PaymentType"]:checked').val() },
            success: function (data) {

                //if (st != "" || $("#PayReceiptDate").val() != "")
                //OTSTM2-56
                if (!($("#dpPaymentReceivedDate").hasClass("has-error") || $("#dpSubmittedDate").hasClass("has-error") || $("#dpPaymentDepositDate").hasClass("has-error"))) {
                    window.location.reload(true);
                }
                if (data.BalanceDue != null) {
                    $('#spnBalDue').text(data.BalanceDue);
                }
            },
            failure: function (data) {
                console.log(data);
            },
            complete: function (result) {
                //if (result.responseText) {//RedirectResult("~/System/Common/EmptyPage?navigationAction=loadTopMenu");
                //    //document.write(result.responseText);
                //}
            },

        });
    });

    $("#btnApprovesubmit").off().click(function (event) {

        //Need to do this coz the model objects are not bound properly
        //Stupid code to handle the mess - first check whether this field is visible and then check if it is empty
        if ($("#dpPaymentDepositDate").length)
            if (!$("#PayDepositDate").val().trim()) {
                setTimeout(function () {
                    alert('Please enter Deposit date.');
                }, 1000);
                return;
            }

        if ($("#PayReceiptDate").val() != '' && new Date($("#PeriodDate").val()).getFullYear() > 2009 && parseFloat(removeCommas($('#Penalties').val())).toFixed(2) > 0) {
            $.ajax({
                url: '/Steward/TSFRemittances/IsFirstTimePenalty',
                method: 'GET',
                dataType: "JSON",
                data: { CustomerID: $("#hdncustomerID").val(), ClaimID: $("#ClaimID").val() },
                complete: function (result) {
                    //if (result.responseText) {//RedirectResult("~/System/Common/EmptyPage?navigationAction=loadTopMenu");
                    //    //document.write(result.responseText);
                    //}
                    if (result.responseJSON) {
                        $('#modalFirstTImePenalty').modal('show');
                    }
                    else {
                        $.ajax({
                            url: '/Steward/TSFRemittances/ChangeStatus',
                            dataType: 'json',
                            type: 'POST',
                            data: { claimID: $("#ClaimID").val(), status: "Approved" },
                            success: function (data) {
                                window.location.href = "/Steward/TSFRemittances"
                            },
                            failure: function (data) {
                                console.log(data);
                            }
                        });
                    }
                }
            });
        }
        else {
            $.ajax({
                url: '/Steward/TSFRemittances/ChangeStatus',
                dataType: 'json',
                type: 'POST',
                data: { claimID: $("#ClaimID").val(), status: "Approved" },
                success: function (data) {
                    window.location.href = "/Steward/TSFRemittances"
                },
                failure: function (data) {
                    console.log(data);
                }
            });
        }
    });

    /*Remote Click*/
    $(".delete").click(function (event) {
        $("#hdnID").val($(this).attr("id"));
        var ItemID = $(this).parent().attr("class");
        $("#lblcategory").html($('#' + ItemID).html());

        $(this).attr('data-target', '#modalTireCountsRemove');


    });

    $("#remove").click(function (event) {
        event.preventDefault();


        $.ajax({
            url: '/Steward/TSFRemittances/DelClaimDetail',
            method: 'POST',
            dataType: "JSON",
            data: { claimID: $("#ClaimID").val(), detailId: $("#hdnID").val() },
            complete: function (result) {
                window.location.reload(true);

            }
        });




    });

    //For Upadte--------------------------------------------------------------------
    //Bind rate behalf of selected Item and Effective Period
    $(".ddlEditcategory").change(function () {

        if ($(".ddlEditcategory").val() != "0") {
            var PeriodDate = $("#PeriodDate").val();
            $.ajax({
                url: '/Steward/TSFRemittances/RateByItemId',
                method: 'POST',
                dataType: "JSON",
                data: { PeriodDate: PeriodDate, ItemID: $(".ddlEditcategory").val() },
                complete: function (result) {
                    for (var i = 0; i < result.responseJSON.length; i++) {

                        $("#edit_Rate").text(result.responseJSON[i].ItemRate);
                    }
                }
            });
        }

    });

    //Check terms and condtions
    $("#chkterms").change(function () {

        if ($(this).is(":checked")) {

            $("#btnsubmit").prop("disabled", false);

        } else {
            $("#btnsubmit").prop("disabled", true);

        }
    });

    $("#Penalties").change(function (e) {
        var penaltyVal = $(this).val();
        if (e.handleObj.handler.arguments.length > 1) {
            if (e.handleObj.handler.arguments[1] == "IsPenaltyOverride_change") {
                penaltyVal = -0.001;
            }
        }
        if ($(this).val() != '') {

            $.ajax({
                url: '/Steward/TSFRemittances/UpdatePenaltiesManually',
                dataType: 'json',
                type: 'POST',
                global: false,
                data: { claimID: $("#ClaimID").val(), penaltiesManually: penaltyVal, IsPenaltyOverride: $('#IsPenaltyOverride').prop('checked') },
                success: function (data) {
                    location.reload();
                    //$('#spnTotal').text(addCommas(parseFloat(data.TotalTSFDue).toFixed(2)));
                    //$('#spnBalDue').text(addCommas(parseFloat(data.BalanceDue).toFixed(2)));                   
                },
                failure: function (data) {
                    console.log(data);
                },
                complete: function (data) {
                    $("#TSFRemittanceStaffPaymentPanel").html(data.responseText);
                    $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({ }, settings, penaltiesPopover));
                },
            });
        }
        //calculationOnTotal();
    });

    $('#negativeAdj input[name="SupportingDocuments.negativeAdj"]').click(function () {
        if ($(this).val() != '') {

            $.ajax({
                url: '/Steward/TSFRemittances/UpdateSupportingDocumentValue',
                dataType: 'json',
                type: 'POST',
                data: { claimID: $("#ClaimID").val(), docOptionValue: $(this).val() },
                success: function (data) {
                },
                failure: function (data) {
                    console.log(data);
                }
            });
        }
    });

    $("#btnupdate").click(function () {

        //var form = $('fmRemittanceTireCount')
        //var data = form.serializeArray();
        //// data.push({ name: 'TSFRemittance.id', value: "16" })
        //data.push({ name: 'TSFRemittance.ClaimID', value: $("#ClaimID").val() });
        //data.push({ name: 'TSFRemittance.DetailID', value: $("#ClaimDetailID").val() });
        //data.push({ name: 'TSFRemittance.CategoryId', value: $("TSFRemittance_CategoryId").val() });
        //data.push({ name: 'TSFRemittance.Quantity', value: $("#TSFRemittance_Quantity").val() });
        //data.push({ name: 'TSFRemittance.NegativeAdjustment', value: $("#TSFRemittance_NegativeAdjustment").val() });
        //data.push({ name: 'TSFRemittance.AdjustmentReasonType', value: $("#TSFRemittance_AdjustmentReasonType").val() });
        //data.push({ name: 'TSFRemittance.OtherDesc', value: $('#TSFRemittance_OtherDesc').val() });
        //data.push({ name: 'TSFRemittance.Net', value: $('#TSFRemittance_Net').val() });
        //data.push({ name: 'TSFRemittance.Rate', value: $('#TSFRemittance_Rate').val() });
        //data.push({ name: 'TSFRemittance.Total', value: $('#TSFRemittance_Total').val() });

        //var len = data.length;
        //for (var i = 0; i < len; i++) {
        //    data[i].value = $.trim(data[i].value);
        //}
        //$.ajax({
        //    url: '/Steward/TSFRemittances/AddRemittance',
        //    dataType: 'json',
        //    type: 'POST',
        //    data: $.param(data),
        //    success: function (data) {
        //        window.location.reload(true);
        //    },
        //    failure: function (data) {
        //        console.log(data);
        //    }
        //});


    });
}

//OTSTM2-56
function changeStatus(st) {
    checkRequiredFields();
    var payment = ($("#PaymentAmount").val() == "") ? "0.00" : $("#PaymentAmount").val();
    $.ajax({
        url: '/Steward/TSFRemittances/ApproveRemittance',
        dataType: 'json',
        type: 'POST',
        global: false, //OTSTM2-485 
        //OTSTM2-56
        data: { claimID: $("#ClaimID").val(), status: st, submitted: $('#SubmittedDate').val(), received: $("#PayReceiptDate").val(), deposit: $("#PayDepositDate").val(), referenceNo: $("#ChequeReferenceNumber").val(), amount: payment, PaymentType: $('input[name="PaymentType"]:checked').val() },
        success: function (data) {

            //if (st != "" || $("#PayReceiptDate").val() != "")
            //OTSTM2-56
            if (st != "" || !($("#dpPaymentReceivedDate").hasClass("has-error") || $("#dpSubmittedDate").hasClass("has-error") || $("#dpPaymentDepositDate").hasClass("has-error"))) {
                //window.location.reload(true);
                //OTSTM2-485 payment panel auto refresh
                $.ajax({
                    url: '/Steward/TSFRemittances/GetTSFRemittanceModel',
                    dataType: 'json',
                    type: 'GET',
                    data: { ClaimId: $("#ClaimID").val() },
                    complete: function (result) {
                        $("#TSFRemittanceStaffPaymentPanel").html(result.responseText);
                        if (!penaltiesPopover.content) {
                            penaltiesPopover.content = $('#penalties-popover').data("popovertext");
                        }
                        $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({}, settings, penaltiesPopover));
                    },
                });
            }
            if (data.BalanceDue != null) {
                $('#spnBalDue').text(data.BalanceDue);
            }
        },
        failure: function (data) {
            console.log(data);
        }
    });
}

$("#btnFirstTImePenalty").click(function (event) {
    //event.preventDefault();
    $.ajax({
        url: '/Steward/TSFRemittances/ChangeStatus',
        dataType: 'json',
        type: 'POST',
        data: { claimID: $("#ClaimID").val(), status: "Approved" },
        success: function (data) {
            window.location.href = "/Steward/TSFRemittances"
        },
        failure: function (data) {
            console.log(data);
        }
    });
});


function changeStatusAuto(st) {
    var payment = ($("#PaymentAmount").val() == "") ? "0.00" : $("#PaymentAmount").val();
    $.ajax({
        url: '/Steward/TSFRemittances/AutoApproveRemittance',
        dataType: 'json',
        type: 'POST',
        data: { claimID: $("#ClaimID").val(), status: st, amount: payment },
        success: function (data) {
            if (st != "")
                window.location.reload(true);
        },
        failure: function (data) {
            console.log(data);
        }
    });
}

/* End Common Functions */

/* SUPPORTING DOCUMENT VALIDATION */

var validatorSupportingDocInfo = $('#fmSupportingDoc').validate({

    ignore: '.no-validate, [readonly=readonly]',

    onkeyup: function (element) {
        this.element(element);
    },
    // onfocusout: function (element) {
    // this.element(element);
    // },
    invalidHandler: function (e, validator) {

    },
    errorPlacement: function (error, element) {

    },
    errorElement: 'p',
    errorClass: 'help-block',
    highlight: function (element) {

        var propertyName = $(element).attr('name');
        var index = panelLevelCheckErrorList.indexOf(propertyName);
        if (index > -1) {

            if ($(element).attr('name') == 'SupportingDocuments.negativeAdj') {
                $(element).closest('.form-group').removeClass('has-success').removeClass('has-required');
                $(element).closest('.form-group').addClass('has-error');
            }
        }
    },

    unhighlight: function (element) {

        var propertyName = $(element).attr('name');
        var index = panelLevelCheckErrorList.indexOf(propertyName);
        if (index > -1) {
            if ($(element).attr('name') == 'SupportingDocuments.negativeAdj') {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required');
                $(element).closest('.form-group').addClass('has-success');
            }
        }
    },

    success: function (error) {
        error.remove();
    },

    failure: function () {
        //failure
    },

    rules: {
        'SupportingDocuments.negativeAdj': {
            inInvalidList: true
        }
    },
    messages: {
        'SupportingDocuments.negativeAdj': "Invalid Required Documents"
    }
});


/* END SUPPORTING DOCUMENT VALIDATION */


/* GABE STYLES */

(function () {
    var popLargeLBN = $('.popoverClassCategory').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverClassCategorySettings));

    $('.popoverPenalties').webuiPopover('destroy').webuiPopover($.extend({}, settings, penaltiesPopover));

    var popLargeGenerator = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));//merge settings and popoverParticipantInfoSettings, without modify settings object; content set to $('.popoverParticipantInfo').webuiPopover()

})();

var isreadonly = $('#hfIsReadOnly').text();

RemittancesDetails(isreadonly);

function RemittancesDetails(status) {
    if (status == 'False') {
        $("input[type='text']").removeAttr("readonly");
        $('#PayReceiptDate').attr('readonly', true);
        $('#PayDepositDate').attr('readonly', true);
        $('#SubmittedDate').attr('readonly', true); //OTSTM2-56
        $(".CategoryId").removeAttr("readonly");
        $(".TSFRateDate").removeAttr("readonly");//Add for 2X
    }
}

$('#PaymentAmount').change(function () {
    calculationOnTotal();
});


function calculationOnTotal() {
    var subtot = removeCommas($('#tbSubtotal label').html());
    var penalities = parseFloat(removeCommas($('#Penalties').val())).toFixed(2);
    var ApplicableTaxesHst = 0;
    if ($("#ApplicableTaxesHst").val() != "N/A") {
        ApplicableTaxesHst = parseFloat(removeCommas($('#ApplicableTaxesHst').val())).toFixed(2);
    }
    var tot = (parseFloat(subtot) + parseFloat(penalities)).toFixed(2);
    var credit = $('#spnCredit').html();

    if (typeof credit != 'undefined') {
        credit = removeCommas($('#spnCredit').html()).replace("-", "");
    } else {
        credit = 0;
    }
    //alert(subtot+'/'+penalities+'/'+tot);
    $('#spnTotal').text("");
    $('#spnTotal').text(addCommas(tot));
    $('#spnBalDue').text("");
    var paymentamount = removeCommas($('#PaymentAmount').val());
    if (paymentamount == "")
        paymentamount = 0;
    var baldue = (parseFloat(tot).toFixed(2) - parseFloat(paymentamount).toFixed(2) - parseFloat(credit).toFixed(2)).toFixed(2);
    //alert('tot: ' + tot + ' paymentamount: ' + paymentamount + ' bal: ' + baldue);
    $('#spnBalDue').text(addCommas(baldue));
    $('#Penalties').val(addCommas(penalities));

    $('#ApplicableTaxesHst').val(addCommas(ApplicableTaxesHst));
    $('#PaymentAmount').val(addCommas(paymentamount));
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function removeCommas(strComma) {
    if (strComma == undefined) {
        return '';
    }
    var strArr = strComma.split(',');
    var strNew = '';
    for (var i = 0; i < strArr.length; i++) {
        strNew = strNew + strArr[i];
    }
    return strNew;
}
//fmPayments
$('#fmRemittanceTireCount').submit(function (ev) {
    $(this).append('<input type="hidden" name="TSFRemittance.ClaimID" id="TSFRemittance.ClaimID" value="' + $('#ClaimID').val() + '">');
    $(this).append('<input type="hidden" name="TSFRemittance.DetailID" id="TSFRemittance.DetailID" value="' + $('#ClaimDetailID').val() + '">');
    $(this).append('<input type="hidden" name="TSFRemittance.CategoryId" id="TSFRemittance.CategoryId" value="' + $('#TSFRemittance_CategoryId').val() + '">'); //OTSTM2-567
    $(this).append('<input type="hidden" name="TSFRemittance.TSFRateDate" id="TSFRemittance.TSFRateDate" value="' + $('#TSFRemittance_TSFRateDate').val() + '">'); //OTSTM2-567
    $('#btnAdd').prop("disabled", true);
    ev.preventDefault(); // to stop the form from submitting
    /* Validations go here */
    this.submit(); // If all the validations succeeded
});

$('#btnAddCredit').click(function () {
    var submitData = {
        ID: $("#ClaimID").val(),
        //CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
        TSFRemittance: {
            ClaimID: $('#ClaimID').val(),
            NegativeAdjDate: $('#CreditRatePeriodDate').val(),
            CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
            CreditNegativeAdj: $('#TSFRemittance_NegativeAdjustment').val(),
            CreditTSFRate: $('#td_CreditRate').text().replace(',', ''),
            CreditTSFDue: $('#td_CreditTotal').text().replace(',', ''),
            AdjustmentReasonType: $('#TSFRemittance_AdjustmentReasonType').val(),
            OtherDesc: $('#TSFRemittance_OtherDesc').val(),
        },
    }
    $(this).prop("disabled", true);
    $.ajax({
        url: '/Steward/TSFRemittances/AddRemittance',
        dataType: 'json',
        type: 'POST',
        data: submitData,
        success: function (data) {
            window.location.reload(true);
        },
        failure: function (data) {
            console.log(data);
        },
        complete: function (data) {
            window.location.reload(true);
        },
    });
});

$('#btnupdateCredit').click(function () {
    var submitData = {
        ID: $("#ClaimID").val(),
        //CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
        TSFRemittance: {
            DetailID: $('#ClaimDetailID').val(),
            ClaimID: $('#ClaimID').val(),
            NegativeAdjDate: $('#CreditRatePeriodDate').val(),
            CategoryId: $('#Credit_TSFRemittance_CategoryId').val(),
            CreditNegativeAdj: $('#TSFRemittance_NegativeAdjustment').val(),
            CreditTSFRate: $('#td_CreditRate').text().replace(',', ''),
            CreditTSFDue: $('#td_CreditTotal').text().replace(',', ''),
            AdjustmentReasonType: $('#TSFRemittance_AdjustmentReasonType').val(),
            OtherDesc: $('#TSFRemittance_OtherDesc').val(),
        },
    }

    $.ajax({
        url: '/Steward/TSFRemittances/AddRemittance',
        dataType: 'json',
        type: 'POST',
        data: submitData,
        success: function (data) {
            window.location.reload(true);
        },
        failure: function (data) {
            console.log(data);
        },
        complete: function (data) {
            window.location.reload(true);
        },
    });
});

//OTSTM2-21
$("#modalCustomerSettingAlertOkBtn").click(function () {
    $("#PeriodDate").val("");
});

$('#filesToUploadGrid').on('update', function (e, msg) {//called while add/remove support file
    if (msg.data === 'addfile') {
        if ($('#filesToUploadGrid')[0].rows.length > 0) {
            SetDeleteVisibility(false);
        }
    }
    if (msg.data === 'deletefile') {
        if ($('#filesToUploadGrid')[0].rows.length > 0) {
            SetDeleteVisibility(false);
        } else {
            IsDeleteVisible();            
        }
    }
});

function IsDeleteVisible() {
    if (Global.Remittance.ClaimId === 0) {
        SetDeleteVisibility(false);//id === 0 means during create new TFSRemittance, and before use date time picker to select a period, so don't show delete button, because TFSRemittance is not created yet
    } else {
        $.ajax({
            url: Global.Remittance.IsDeleteVisibleUrl,
            dataType: 'json',
            type: 'POST',
            data: { claimID: Global.Remittance.ClaimId },
            success: function (data) {
                if (data.status && data.result)
                    SetDeleteVisibility(true);
                else
                    SetDeleteVisibility(false);
            },
            failure: function (data) {
                SetDeleteVisibility(false);
                console.log(data);
            },
            complete: function (data) {
            },
        });
    }
}

$('#btn-delete-remittance').click(function () {
    //$(this).prop("disabled", true);
    $.ajax({
        url: Global.Remittance.DeleteRemittanceUrl,
        dataType: 'json',
        type: 'POST',
        data: { claimID: Global.Remittance.ClaimId, bTestData: false },
        success: function (data) {
            if (data.result) {
                window.location.href = "/Steward/TSFRemittances";
                //window.history.back();
            } else {
                alert('This remittance could not be deleted due to some existing data in the system.');                
            }
        },
        failure: function (data) {
            console.log(data);
        },
    });
});
