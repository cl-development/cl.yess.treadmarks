﻿$(function () {
    _PageSize = 12; //OTSTM2-929
    _txtSearchAppId = 'SearchApplications';//id of search textbox
    _txtSearchRegisId = 'SearchRegistrant';
    _exportAppId = 'exportApplicationToExcel'; //id of export
    _exportTransId = 'exportRegistrantToExcel';
    _modalId = 'myModal';

    //OTSTM2-929
    $('#tblRemittanceslist tfoot .search-filter').each(function () {
        var title = $(this).text();
        var idText = title.replace(/[\W_]/g, '');
        $(this).html('<input type="text" style="font-family:\'Open Sans\', sans-serif, FontAwesome" placeholder=" &#xf002; ' + title + '" id="searchText' + idText + '"/>');
    });

    if (localStorage.getItem("error") === null) {
        $('#ajaxstatus').text('');
    } else {
        $('#ajaxstatus').text(localStorage.getItem("error"));
    }
    try {
        var tblRemittances = $('#tblRemittanceslist').DataTable({
            info: false,
            processing: false,
            serverSide: true,
            ajax: "TSFRemittances/GetListOfApplications",
            deferRender: true,
            dom: "rtiS",
            scrollY: 475,
            scrollX: true,
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            scrollCollapse: true, //OTSTM2-929 "true" for dynamic height of datatables
            searching: true,
            ordering: true,
            order: [[4, "desc"]],
            "columns": [

                   {
                       sName: "RegNo",
                       //width: "5%",
                       data: null,
                       render: function (data, type, full, meta) {
                           return GeneralColumnRender(data.RegNo, data.Activated);
                       }
                   },
                     {
                         sName: "Company",
                         //width: "9%",
                         data: null,
                         render: function (data, type, full, meta) {
                             return GeneralColumnRender(data.Company, data.Activated);
                         }
                     },
                     { //OTSTM2-188
                         sName: "ReportingFrequency",
                         //width: "8%",
                         data: null,
                         render: function (data, type, full, meta) {
                             return GeneralColumnRender(data.ReportingFrequency, data.Activated);
                         }
                     },

                 {
                     sName: "Period",
                     //width: "6%",
                     data: null,
                     render: function (data, type, full, meta) {
                         return GeneralColumnRender(data.Period, data.Activated);
                     }
                 },
                    {
                        sName: "Submitted",
                        //width: "7%",
                        data: null,
                        render: function (data, type, full, meta) {
                            return DatetimeRender(data.Submitted, data.Activated);
                        }

                    },
                    {//OTSTM2-188
                        sName: "DepositDate",
                        //width: "8%",
                        data: null,
                        render: function (data, type, full, meta) {
                            return DatetimeRender(data.DepositDate, data.Activated);
                        }

                    },
                    {//OTSTM2-188
                        sName: "TotalReceivable",
                        //width: "11%",
                        data: null,
                        render: function (data, type, full, meta) {
                            return DecimalRender(data.TotalReceivable, data.Activated);
                        }

                    },
                    {//OTSTM2-188
                        sName: "ChequeAmount",
                        //width: "11%",
                        data: null,
                        render: function (data, type, full, meta) {
                            return DecimalRender(data.ChequeAmount, data.Activated);
                        }

                    },
                    {//OTSTM2-188
                        sName: "BalanceDue",
                        //width: "9%",
                        data: null,
                        render: function (data, type, full, meta) {
                            return DecimalRender(data.BalanceDue, data.Activated);
                        }

                    },
                 {
                     sName: "Status",
                     //width: "7%",
                     data: null,
                     render: function (data, type, full, meta) {
                         return ApplicationStatusRender(data.Status, data.Activated);
                     }
                 },
              {
                  sName: "AssignedTo",
                  //width: "8%",
                  data: null,
                  render: function (data, type, full, meta) {
                      return GeneralColumnRender(data.AssignedTo, data.Activated);
                  }
              },
                {
                    visible: 'True',
                    //width: "8%",
                    className: "td-center",
                    data: null,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if ((Global.Remittance.AllowEdit) && Global.Remittance.UserClaimsWorkflow.Steward.ActionGear)
                            return ApplicationActionRender(data.Status, data.ID, data.Activated, meta);
                        else
                            return '';
                    }
                },
                {
                    visible: false,
                    data: null,
                    render: function (data, type, full, meta) {
                        return GeneralColumnRender(data.ID, data.Activated);
                    }
                },
                 {
                     visible: false,
                     data: null,
                     render: function (data, type, full, meta) {
                         return GeneralColumnRender(data.TokenID, data.Activated);
                     }
                 },
            ],
            initComplete: function (settings, json) {
                $('.dataTables_scrollBody').css('overflow-y', 'hidden');

                //OTSTM2-929
                tblRemittances.columns().eq(0).each(function (index) {
                    var column = tblRemittances.column(index);
                    var that = column;
                    $('input', column.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            },
            drawCallback: function (settings) {
                var direction = settings.aaSorting[0][1];
                $('.sort').html('<i class="fa fa-sort"></i>');
                (direction == 'desc') ? $('.sorting_desc').find('.sort').html('<i class="fa fa-caret-down"></i>') :
                $('.sorting_asc').find('.sort').html('<i class="fa fa-caret-up"></i>');

                $('#txtFoundApplications').html('Found ' + settings.fnRecordsDisplay());
                (settings.fnDisplayEnd() >= _PageSize) ? $('#ApplicationsViewMore').css('visibility', 'visible') : $('#ApplicationsViewMore').css('visibility', 'hidden');

                //OTSTM2-929
                if ($("#ApplicationsViewMore").is(':visible')) {
                    $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                }
                else {
                    if (settings.fnDisplayEnd() >= _PageSize) {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'scroll' });
                    }
                    else {
                        $('.dataTables_scrollBody').css({ 'overflow-y': 'hidden' });
                    }
                }

                if ($("#searchTextReg").val() || $("#searchTextCompany").val() || $("#searchTextReportingFrequency").val() || $("#searchTextPeriod").val() || $("#searchTextSubmitted").val() || $("#searchTextDepositDate").val()
                    || $("#searchTextTotalTSFDue").val() || $("#searchTextPayment").val() || $("#searchTextBalanceDue").val() || $("#searchTextStatus").val() || $("#searchTextAssignedTo").val() || $("#SearchApplications").val()) {
                    $("#txtFoundApplications").css('display', 'block');
                }
                else {
                    $("#txtFoundApplications").css('display', 'none');
                }

                var sortColumn = settings.aoColumns[settings.aLastSort[0].col].sName;
                var searchValue = $('#SearchApplications').val();
                var url = Global.Remittance.ApplicationExportUrl.replace('AAA', sortColumn).replace('BBB', direction).replace('----', searchValue)
                .replace('-st-reg', $("#searchTextReg").val()).replace('-st-company', $("#searchTextCompany").val()).replace('-st-reportingfrequency', $("#searchTextReportingFrequency").val())
                .replace('-st-period', $("#searchTextPeriod").val()).replace('-st-submitted', $("#searchTextSubmitted").val()).replace('-st-depositdate', $("#searchTextDepositDate").val())
                .replace('-st-totalreceivable', $("#searchTextTotalTSFDue").val()).replace('-st-chequeamount', $("#searchTextPayment").val())
                .replace('-st-balancedue', $("#searchTextBalanceDue").val()).replace('-st-status', $("#searchTextStatus").val()).replace('-st-assignedto', $("#searchTextAssignedTo").val()); //OTSTM2-929
                $('#exportApplicationToExcel').attr('href', url);
            },
            scroller: {
                displayBuffer: 100,
                rowHeight: 70,
                serverWait: 100,
                loadingIndicator: false,
            },
        });
    }
    catch (e) {
        alert(e);
    }

    $('#tblRemittanceslist tbody').on('click', 'tr', function (e) {
        if (!(window.getSelection() && window.getSelection().toString().trim() !== '')) {
            var data = tblRemittances.row(this).data();
            var nodeName = e.target.nodeName;
            if ((nodeName == "I") || ((nodeName == "A"))) {
                $('.assignCompanyName').text(data.Company);
                $('.assignRegNo').text(data.RegNo);
                $(".assignPeriod").text(data.Period);
                $('.unAssignUserName').text(data.AssignedTo);
                $('#selectedName').val(data.Company);
                $('#selectedApplicationToken').val(data.TokenID);
                $('#selectedApplicationID').val(data.ID);
                $('#assignActionURL').val("/Steward/TSFRemittances/ChangeStatus");
                $('#assignVendorType').val(data.ParticipantType);
            }
            else if ((!!data.ID)) {
                window.open('/System/Claims/ClaimsTypeRouting' + '?claimId=' + data.ID + '&regNumber=' + data.RegNo, '_self');
            }
        }
    });
    $(window).load(function () {
        $("#tblRemittanceslist").parents(".dataTables_scrollBody").css("overflow-x", "auto");
    });
    $("#ApplicationsViewMore").on("click", function () {
        $("#tblRemittanceslist_wrapper").find(".dataTables_scrollBody").css("overflow", "auto").css("overflow-y","auto"); //OTSTM2-949 "overflow-y: auto" for IE11
        $("#tblRemittanceslist_wrapper").find(".dataTables_scrollHead").removeClass("dataTables_scrollHead").addClass("dataTables_scrollHead_after_vertical_scrollbar"); //OTSTM2-843
        $("#tblRemittanceslist_wrapper").find(".dataTables_scrollFoot").removeClass("dataTables_scrollFoot").addClass("dataTables_scrollFoot_after_vertical_scrollbar"); //OTSTM2-929
        $(this).hide();
    });

    $("#exportApplicationToExcel").on("click", function (e) {
        window.location.href = $(this).attr('href');
        e.stopPropagation();
    });

    $('#BtnAssignToMeOK, #modalAssingToMe .close').on("click", function () {
        if ($('#assgnToMeApplicationID').val() > 0) {
            window.location.reload(true);
        }
    });
    function RegNumberColumnRender(iPadNumber, activated) {
        var span = ""
        if (activated) {
            span = "<span>" + iPadNumber + "</span>";
        }
        else {
            //span = "<span style=\"color:#b1b1b1\">" + iPadNumber + "</span>";
            span = "<span>" + iPadNumber + "</span>";
        }
        return span;
    }
    function ApplicationStatusRender(StatusName, activated) {

        var span = ""
        if (StatusName == null) return span;
        switch (StatusName.toLowerCase()) {
            case "approved":
                span = '<span>' + '<div class="panel-table-status color-tm-green-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "submitted":
                span = '<span>' + '<div class="panel-table-status color-tm-orange-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "open":
                span = '<span>' + '<div class="panel-table-status color-tm-blue-bg">' + StatusName + '</div>' + '</span>';
                break;
            case "under review":
            case "underreview":
                span = '<span>' + '<div class="panel-table-status color-tm-yellow-bg">' + StatusName + '</div>' + '</span>';
                break;
            default:
                span = '<span>' + '<div class="panel-table-status color-tm-gray-bg">' + StatusName + '</div>' + '</span>';
                //default code block
        }
        return span;
    }
    function pad(num) {
        num = "0" + num;
        return num.slice(-2);
    }

    function DatetimeRender(value, activated) {
        if (value == null) return "<span></span>";
        var str, year, month, day, hour, minute, d, finalDate;
        str = value.replace(/\D/g, "");
        d = new Date(parseInt(str));

        year = d.getFullYear();
        month = pad(d.getMonth() + 1);
        day = pad(d.getDate());
        hour = pad(d.getHours());
        minutes = pad(d.getMinutes());
        finalDate = year + "-" + month + "-" + day;

        var span = ""
        if (activated) {
            span = "<span>" + finalDate + "</span>";
        }
        else {
            //span = "<span style=\"color:#b1b1b1\">" + iPadNumber + "</span>";
            span = "<span>" + finalDate + "</span>";
        }
        return span;
    }

    function GeneralColumnRender(Value, activated) {

        var span = "<span>"
        //if (!activated) {
        //    span = "<span style=\"color:#b1b1b1\">";
        //}

        if (Value == "") {
            //span = span + "Empty_Value</span>";
            span = "<span style=\"color:#b1b1b1\">";
        }
        else {
            span = span + Value + "</span>";
        }

        return span;
    }

    //OTSTM2-188
    function DecimalRender(Value, activated) {

        var span = "<span>"
        //if (!activated) {
        //    span = "<span style=\"color:#b1b1b1\">";
        //}

        if (Value == 0) {
            //span = span + "Empty_Value</span>";
            span = span + "0.00</span>";;
        }
        else {
            span = span + Value.toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + "</span>";
        }

        return span;
    }

    function ApplicationActionRender(StatusName, ID, isActivated, meta) {
        var content = "";

        if (StatusName == null) return content;
        switch (StatusName.toLowerCase()) {
            case "submitted":
                {
                    content = '<div class="btn-group dropdown-menu-actions">' +
                        '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                        '<i class="glyphicon glyphicon-cog"></i></a>' +
                        '<div class="popover-menu"><ul><li><a data-target=\"#modalAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-user\"></i> Assign</a>' +
                        '</li></ul></div></div>';
                }
                break;
            case "underreview":
            case "under review":
                {
                    content = '<div class="btn-group dropdown-menu-actions">' +
                        '<a href="javascript:void(0)" data-trigger="focus" tabindex="124" data-toggle="popover" id="actions' + meta.row + '" data-original-title="" title="">' +
                        '<i class="glyphicon glyphicon-cog"></i></a>' +
                        '<div class="popover-menu"><ul>' +
                        '<li><a data-target=\"#modalReAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-user\"></i>Re-Assign</a></li>' +
                        '<li><a data-target=\"#modalUnAssign\" data-toggle=\"modal\" href=\"#\"><i class=\"fa fa-minus-square-o\"></i>Un-Assign</a></li>' +
                        '</ul></div></div>';
                }
                break;
            default:
                break;
        }

        return content;
    }

    function IPadGridActionColumn(ID, iPadNumber, assignedToName, BusinessName, userID, barcodeGroup, isAssigned, isActivated) {

        var content = "";
        //fix for safari
        var popoverFix = navigator.userAgent.indexOf("Safari") > -1 ? "tabindex=\"" + ID + "\"" : "";
        if (isActivated) {
            content = "<div class=\"btn-group dropdown-menu-actions\">" +
                            "<a id=\"IPadActions" + ID + "\" data-toggle=\"popover\" tabindex=\"" + ID + "\" data-trigger=\"focus\"  href=\"#\">" +
                            "<i class=\"glyphicon glyphicon-cog\"></i>" +
                            "</a><div class=\"popover-menu\"><ul>";
            if (isAssigned) {
                content += "<li><a id=\"PrintDeviceLabel" + ID + "\" href=\"/default/DeviceLabelGen?deviceID=" + iPadNumber + "&businessName=" + assignedToName + "&regNumber=" + BusinessName + "&userID=" + userID + "&barcodeGroup=" + barcodeGroup + "\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Print Label</a></li>";
                content += "<li><a id=\"UnAssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalUnassigniPad\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\"  data-assignedto=\"" + BusinessName + "\"><i class=\"fa fa-minus-square-o\"></i> Unassign</a></li>";
            }
            else {
                content += "<li><a id=\"AssignAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalAssigniPad\"><i class=\"fa fa-plus-square-o\"></i> Assign</a></li>";
            }
            content += "<li><a id=\"DactivateAction" + ID + "\" href=\"#\" data-toggle=\"modal\" data-uid=\"" + ID + "\" data-deviceid=\"" + iPadNumber + "\" data-target=\"#modalDeactivateiPad\"><i class=\"fa fa-remove\"></i> Deactivate</a></li>";
            content += "</ul></div></div>";
        }

        return content;
    }

    TM = {};
    TM.settings = {
        currentpopmenuid: 0,
    };
    tblRemittances.on("draw.dt", function () {
        $("[id^='actions']").popover({
            placement: "bottom",
            container: "body",
            html: true,
            template: '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
            content: function () {
                //TM.settings.currentpopmenuid = this.id;
                return $($(this)).siblings(".popover-menu").html();
            }
        });
    });
    $('#SearchApplications').on('keydown', function (e) {
        if (e.keyCode == 13) //End Home
        { e.preventDefault(); }
    });

    var myTimer;

    $('#SearchApplications').on('keyup', function (e) {
        //$('#export').attr('href', url);
        if ((e.keyCode === 37) || (e.keyCode === 38) || (e.keyCode === 39) || (e.keyCode === 40)
            || (e.keyCode === 35) || (e.keyCode === 36) || (e.keyCode === 13)) //End Home
        { return; }
        var str = $(this).val();

        clearTimeout(myTimer);
        myTimer = setTimeout(searchIt, 500, str);
    });

    var searchIt = function (str) {
        if (str.length >= 1) {
            //$(this).css('visibility', 'visible');
            //$('#txtFoundApplications').css('display', 'block');
            //var url = Global.Remittance.ApplicationExportUrl.replace('----', $('#' + _txtSearchAppId).val());
            //$('#' + _exportAppId).attr('href', url);
            $('#txtFoundApplications').siblings('.remove-icon').show();
            tblRemittances.search(str).draw(false);
        }
        else {
            $('#txtFoundApplications').siblings('.remove-icon').hide();
            //$('#' + _txtSearchAppId).removeAttr('style');
            //$('#txtFoundApplications').css('display', 'none');
            //var url = Global.Remittance.ApplicationExportUrl.replace('----', $('#' + _txtSearchAppId).val());
            //$('#' + _exportAppId).attr('href', url);
            tblRemittances.search(str).draw(false);
        }
    }

    $('#glyphicon-search-App').on('click', function () {
        var searchValue = $('#SearchApplications').val();
        tblRemittances.search(searchValue).draw(false);
    });
    //on clear field click
    $('.remove-icon').click(function () {
        $(this).siblings("input").val("");
        $(this).hide();
        //$('#txtFoundApplications').css('display', 'none');
        tblRemittances.search("").draw(false);
    });
    $('#glyphicon-search-Reg').on('click', function () {
        var searchValue = $('#SearchRegistrant').val();
        tblRemittances.search(searchValue).draw();
    });

    $('#glyphicon-remove-Reg').on('click', function () {
        tblRemittances.search("").draw();
    });

    $.ajax({
        url: '/System/Common/GetUsersByClaimsWorkflow',
        method: 'GET',
        dataType: "JSON",
        data: { claimId: 0, accountName: 'Steward', claimType: 'Remittance' },
        success: function (data) {
            $.each(data, function (index, data) {
                $('.listOfStaff').append('<option value="' + data.Value + '">' + data.Text + '</option>')//value:ID text:name text
            });
            $('.listOfStaff').prop('selectedIndex', 0);
        },
        failure: function (data) {

        },
    });

    if (Global.Remittance.AllowEdit === 'True') {
        Global.Remittance.AllowEdit = true;
    } else {
        Global.Remittance.AllowEdit = false;//disable all action 
    }
    $("#listOfStaffAssign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Assign").removeAttr("disabled");
            $("#Btn-Assign").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#Btn-Assign").attr("disabled", "disabled");
        }
    });

    $("#listOfStaffRe-Assign").change(function () {
        var selectedStaffValue = $(this).val();
        var selectedStaffID = parseInt(selectedStaffValue);

        if (!isNaN(selectedStaffID) && selectedStaffID != 0) {
            $("#Btn-Re-Assign").removeAttr("disabled");
            $("#Btn-Re-Assign").attr("data-assignuser", selectedStaffID);
        }
        else {
            $("#Btn-Re-Assign").attr("disabled", "disabled");
        }
    });
    //============= Assign To Me ================================
    function loadFirstApplication() {
        return $.ajax({
            url: '/Steward/TSFRemittances/GetFirstSubmitremittanceService',
            method: 'GET',
            dataType: "JSON"
        });
    }

    $('#modalAssingToMe').on('show.bs.modal', function () {
        var status = $('#BtnAssignToMeOK').attr('data-status');

        loadFirstApplication().done(function (data) {
            if (data) {
                var assignToMeBtnResult = {
                    assignToMeUrl: "/Steward/TSFRemittances/ChangeStatus/",
                    assignToMeId: data.ID,
                    curentUserId: data.CurrentUser.Id,
                    period: data.Period,
                    regNo: data.RegNo,
                }

                $.ajax({
                    url: assignToMeBtnResult.assignToMeUrl,
                    method: 'POST',
                    dataType: "JSON",
                    data: {
                        claimID: assignToMeBtnResult.assignToMeId,
                        status: status,
                        assignuser: assignToMeBtnResult.curentUserId,
                        isAssigningToMe: true,
                    },
                    success: function (result) {
                        $('#assignRegNo4').text(assignToMeBtnResult.regNo);
                        $('#ReassignPeriod2').text(assignToMeBtnResult.period);
                        $('#assignToMeTitle').html('Assigned');
                        $('#SuccessAssignToMe').show();
                        $('#NoDataassignToMe').hide();
                        tblRemittances.search($('#' + _txtSearchAppId).val()).draw(false);
                    }
                });
            }
        }).fail(function () {
            $('#assignToMeTitle').html('Warning');
            $('#SuccessAssignToMe').hide();
            $('#NoDataassignToMe').show();
        })

    });

    $('#BtnAssignToMeOK').on("click", function () {
        tableApplication.search($('#SearchApplications').val()).draw(false);
    });
    //==================================================================

    $('.workflow').on('click', function () {
        var url = $('#assignActionURL').val();
        var Token = $('#selectedApplicationToken').val();
        var ID = $('#selectedApplicationID').val();
        var isAssigningToMe = false;

        var userID = 0;
        if ((typeof $(this).attr('data-assignuser') != 'undefined')) {
            userID = parseInt($(this).attr('data-assignuser'));
        }

        //assign to me
        if ((!url) && ((typeof $(this).attr('data-assgnmeurl') != 'undefined'))) {
            url = $(this).attr('data-assgnmeurl');
            ID = $('#assgnToMeApplicationID').val();
            isAssigningToMe = true;
        }

        //
        var status = $(this).attr('data-status');
        //if (status != "") {
        //    if (status.toLowerCase() == 'backtoapplicant') {
        //        //as per OTSTM-864 clear all fields that have their checkbox unchecked 

        //        $('input[type="checkbox"]:not(:checked)').each(function () {
        //            clearField(this);
        //        });

        //        $('#hdnExplicitSave').trigger('click');//saves data                
        //    }
        //    //let model save first then change status
        $('#SuccessAssignToMe').hide();
        $('#NoDataassignToMe').hide();
        $('#assignToMeTitle').html(' ');
        if (ID > 0) {
            setTimeout(function () {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: "JSON",
                    data: { claimID: ID, status: status, assignuser: userID, isAssigningToMe: isAssigningToMe },
                    success: function (data) {
                        $('#assignToMeTitle').html('Assigned');
                        $('#SuccessAssignToMe').show();
                        (typeof $(this).attr('data-assgnmeurl') == 'undefined') //return from action gear, reload window.
                        {
                            if (!isAssigningToMe) {
                                //window.location.reload(true);
                                tblRemittances.search($('#' + _txtSearchAppId).val()).draw(false);
                            }
                        }
                    },
                    failure: function (data) {
                        console.log('.workflow ajax Failed!!');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if (xhr.status == 404) {
                            console.log('.workflow ajax Failed!!' + thrownError);
                            $('#assignToMeTitle').html('Not able to Assign application to Me');
                        }
                    },
                });
            }, 1000);
        } else {
            $('#assignToMeTitle').html('Warning');

            $('#NoDataassignToMe').show();

        };
        //}
    });

    $('#modalReAssign').on('shown.bs.modal', function () {
        var strUser = $("#listOfStaffRe-Assign option:selected").text() + ' ' + $("#listOfStaffRe-Assign option:selected").val();
        $("#Btn-Re-Assign").attr("data-assignuser", $("#listOfStaffRe-Assign option:selected").val());
    });

    $('#modalAssign').on('shown.bs.modal', function () {
        $("#Btn-Assign").attr("data-assignuser", $("#listOfStaffAssign option:selected").val());
        console.log($("#listOfStaffAssign option:selected").text() + ' ' + $("#listOfStaffAssign option:selected").val());
    });

    (function () {
        var settings = {
            trigger: 'hover',
            //title:'Pop Title',
            //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
            //width:300,
            multi: true,
            closeable: true,
            style: '',
            delay: { show: 300, hide: 800 },
            padding: true
        };

        // Popover: Participant Information
        var popoverParticipantInfoContent = $('#popoverParticipantInfo').html(),
            popoverParticipantInfoSettings = {
                content: popoverParticipantInfoContent,
                width: 270,
            };
        var popLargeGenerator = $('.popoverParticipantInfo').webuiPopover('destroy').webuiPopover($.extend({}, settings, popoverParticipantInfoSettings));//merge settings and popoverParticipantInfoSettings, without modify settings object; content set to $('.popoverParticipantInfo').webuiPopover()
    })();
})
$(document).ready(function () {
    if ((!Global.Remittance.AllowEdit) || (Global.Remittance.UserClaimsWorkflow.Steward.WorkFlow != "TSF Clerk")) {
        $("#btnAssignRemittanceToMe").attr("disabled", "disabled");;
    }
});
