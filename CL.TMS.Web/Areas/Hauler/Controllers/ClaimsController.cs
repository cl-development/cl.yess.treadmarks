using CL.TMS.Common;
using CL.TMS.Common.Helper;
using CL.TMS.DataContracts.ViewModel.Common;
using CL.TMS.DataContracts.ViewModel.Hauler;
using CL.TMS.Framework.DTO;
using CL.TMS.Resources;
using CL.TMS.Security.Authorization;
using CL.TMS.ServiceContracts.HaulerServices;
using CL.TMS.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CL.TMS.DataContracts.ViewModel.Claims;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.UI.Common.UserInterface;
using System.IO;
using CL.TMS.Common.Enum;
using CL.TMS.ExceptionHandling.Exceptions;
using CL.TMS.ExceptionHandling;
using Newtonsoft.Json;
using CL.TMS.Web.Infrastructure;
using System.Resources;
using CL.TMS.UI.Common.Security;
using CL.TMS.Security;

namespace CL.TMS.Web.Areas.Hauler.Controllers
{
    public class ClaimsController : ClaimBaseController
    {
        #region Properties

        private IHaulerClaimService haulerClaimService;
        private IClaimService claimService;
        private IFileUploadService fileUploadService;

        #endregion

        #region Constructor

        public ClaimsController(IHaulerClaimService haulerClaimService, IClaimService claimService, IFileUploadService fileUploadService)
        {
            this.haulerClaimService = haulerClaimService;
            this.claimService = claimService;
            this.fileUploadService = fileUploadService;

        }
        #endregion

        // GET: /Hauler/Claims/
        public ActionResult Index()
        {
            if (SecurityContextHelper.CurrentVendor == null || SecurityContextHelper.CurrentVendor.VendorType != 3)
            {
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            }
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.BusinessName = SecurityContextHelper.CurrentVendor.BusinessName;
            ViewBag.SelectedMenu = "Claims";

            if (SecurityContextHelper.IsStaff())
            {
                return View("StaffClaimIndex");
            }
            else
            {
                return View("ParticipantClaimIndex");
            }
        }
        [ClaimsAuthorization(TreadMarksConstants.ClaimsHaulerClaimSummary)]
        public ActionResult ClaimSummary(int claimId, int? vId = null)
        {
            if (null == SecurityContextHelper.CurrentVendor && vId == null)//session time out            
                return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
            //return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
            else if (vId != null)
            {
                var vendor = claimService.GetVendorById(vId.Value);
                if (vendor != null)
                {
                    //Keep it in session
                    //Vendor Context Check
                    if (SecurityContextHelper.IsValidVendorContext(vendor.VendorId) && vendor.VendorType == 3)
                        Session["SelectedVendor"] = vendor;
                    else
                        return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
                else
                    return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            ViewBag.ngApp = "haulerClaimApp";
            ViewBag.ClaimsId = claimId;
            //OTSTM2-551 only claim summary page for staff has activity panel (can get real time activity)
            if (SecurityContextHelper.IsStaff())
            {
                ViewBag.HasActivityPanel = 1;
            }
            ViewBag.ClaimsStatus = claimService.GetClaimStatus(claimId);
            ViewBag.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;
            ViewBag.UserId = SecurityContextHelper.CurrentUser.Id;
            ViewBag.UserName = string.Format("{0} {1}", SecurityContextHelper.CurrentUser.FirstName, SecurityContextHelper.CurrentUser.LastName);

            ViewBag.AllowInternalNote = SecurityContextHelper.IsStaff();
            ViewBag.AllowTransactionHandling = false;
            ViewBag.AllowEdit = (ResourceActionAuthorizationHandler.CheckUserSecurity(TreadMarksConstants.ClaimsHaulerClaimSummary) == SecurityResultType.EditSave);

            var vendorReference = claimService.GetVendorReference(claimId);
            if (vendorReference == null)//claim doesn't exist
            {
                return RedirectToAction("EmptyPage", "Common", new { area = "System" });
            }

            if (SecurityContextHelper.CurrentVendor.VendorId != vendorReference.VendorId || vendorReference.VendorType != 3)
            {
                if (SecurityContextHelper.IsValidVendorContext(vendorReference.VendorId))
                {
                    return RedirectToAction("EmptyPage", "Common", new { area = "System", navigationAction = "loadTopMenu" });
                }
                else
                {
                    return RedirectToAction("UnauthorizedPage", "Common", new { area = "System" });
                }
            }

            if (SecurityContextHelper.IsStaff())
            {
                return View("StaffClaimSummary");
            }
            else
            {
                ViewBag.AllowEdit = SecurityContextHelper.HasEditable || SecurityContextHelper.HasSubmitPermission;
                return View("ParticipantClaimSummary");
            }
        }

        #region Claims Summary

        public ActionResult ClaimDetails(int claimId)
        {
            var vendorId = SecurityContextHelper.CurrentVendor.VendorId;
            var result = haulerClaimService.LoadClaimSummaryData(vendorId, claimId);
            if (SecurityContextHelper.IsStaff())
            {
                PopulateTransationUrlForStatusPanel(result.ClaimStatus, "Hauler", claimId);
            }
            result.ClaimCommonModel.ClaimPeriodName = result.ClaimCommonModel.ClaimPeriod.ShortName;
            result.ClaimCommonModel.RegistrationNumber = SecurityContextHelper.CurrentVendor.RegistrationNumber;

            if (null != result.ClaimStatus)
            {

                result.ClaimStatus.Submitted = ConvertTimeFromUtc(result.ClaimStatus.Submitted);
                result.ClaimStatus.Assigned = ConvertTimeFromUtc(result.ClaimStatus.Assigned);
                result.ClaimStatus.Started = ConvertTimeFromUtc(result.ClaimStatus.Started);
                result.ClaimStatus.ReviewDue = ConvertTimeFromUtc(result.ClaimStatus.ReviewDue);
                result.ClaimStatus.Approved = ConvertTimeFromUtc(result.ClaimStatus.Approved);
                result.ClaimStatus.Reviewed = ConvertTimeFromUtc(result.ClaimStatus.Reviewed);
                result.ClaimStatus.ClaimOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOnholdDate);
                result.ClaimStatus.ClaimOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.ClaimOffholdDate);
                result.ClaimStatus.AuditOnholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOnholdDate);
                result.ClaimStatus.AuditOffholdDate = ConvertTimeFromUtc(result.ClaimStatus.AuditOffholdDate);
            }

            return new NewtonSoftJsonResult(result, false);
        }

        [HttpGet]
        public ActionResult ClaimPayment(int claimId)
        {
            var result = haulerClaimService.LoadHaulerPayment(claimId);
            return new NewtonSoftJsonResult(result, false);
        }

        [HttpPost]
        public JsonResult SortYardCountSubmission(int claimId, ItemRow row)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = MessageResource.InvalidData }, JsonRequestBehavior.AllowGet);
            }

            this.haulerClaimService.UpdateYardCountSubmission(claimId, row);

            return Json(new { status = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubmitClaimCheck(HaulerSubmitClaimViewModel submitClaimModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }

                var updatedSubmitClaimModel = this.haulerClaimService.HaulerClaimSubmitBusinessRule(submitClaimModel);

                if (updatedSubmitClaimModel.Errors.Count > 0)
                {
                    //handle errors
                    var result = new { status = "Errors", Type = SubmitBusinessRuleType.Error.ToString(), Message = updatedSubmitClaimModel.Errors.First() };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                if (updatedSubmitClaimModel.Warnings.Count > 0)
                {
                    //handle warnings
                    return Json(new { status = "Warnings", Type = SubmitBusinessRuleType.Warning.ToString(), Warnings = updatedSubmitClaimModel.Warnings }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = "Valid Data", Type = SubmitBusinessRuleType.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult FinalSubmitClaim(int claimId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
                }
                this.claimService.SubmitClaim(claimId, false);

                return Json(new { status = "Valid Data" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Invalid data" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LoadClaimPeriodBreadCrumb()
        {
            var result = this.claimService.GetClaimPeriodForBreadCrumb(SecurityContextHelper.CurrentVendor.VendorId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }

}