// Popover Setup
var settings = {
    trigger: 'hover',
    //title:'Pop Title',
    //content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
    //width:300,
    multi: true,
    closeable: true,
    style: '',
    delay: { show: 300, hide: 800 },
    padding: true
};

$(document).ready(function () {
    var selectRelatedProcessorValue = $('#HRelatedProcessorSelectedValue').val();
    ApplicationCommon.populateProcessorDropDownList(selectRelatedProcessorValue, true);

    BusinessLocationModule.initialize();
    ContactInformation();

    TireDetails();
    HaulerDetails();
    SortyardDetails();
    SupportingDocuments();

    TermsAndConditions();
    initializeApplicationSubmitButton('/Hauler/Registration/SubmitApplication');

    var initialize = function () {
        titleCase();
        DisplayCVORDocumentOption();
        DisplayHSTDocumentOption();
        DisplayWSIBDocumentOption();
        DisplayProcessorLetterTireDetails();
        InitAutoSave();
    }

    initialize();
});

function HaulerDetails() {
    //applies to radiobutton for HaulerDetails_CVORNumber
    $('.cvor').on('click', function () {
        DisplayCVORDocumentOption();
    });

    //applies to radiobutton for HaulerDetails.HaulerDetailsVendor.HasMoreThanOneEmp
    $('.wsib').on('click', function () {
        DisplayWSIBDocumentOption();
    });

    $('#HaulerDetails_IsTaxExempt').on('change', function () {
        DisplayHSTDocumentOption();
    });

    $('#dpBusinessStartDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpInsuranceExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    $('#dpCVORExpiryDate').datetimepicker({
        minView: 2,
        showOn: 'focus',
        autoclose: true,
        format: Global.ParticipantIndex.Resources.DateTimeFormat//'yyyy-mm-dd'
    });

    //var disabledHSTTextBox = function () {
    //    if (!($('#HaulerDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
    //        if ($('#HaulerDetails_IsTaxExempt').is(':checked'))
    //        {
    //            $('#HaulerDetails_CommercialLiabHstNumber').val('').attr('disabled', 'disabled');
    //        }
    //        else {
    //            $('#HaulerDetails_CommercialLiabHstNumber').removeAttr('disabled');
    //        }
    //    }
    //}

    var disabledHSTTextBox = function () {
        if (!($('#HaulerDetails_IsTaxExempt').attr('disabled') == 'disabled')) {
            if ($('#HaulerDetails_IsTaxExempt').is(':checked')) {
                $('#HaulerDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
            }
            else {
                $('#HaulerDetails_CommercialLiabHstNumber').attr('readonly', false);
            }
        }
    }

    $('#HaulerDetails_IsTaxExempt').on('click', function () {
        disabledHSTTextBox();
    });

    $('.radio input.cvor').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlCVOR').toggle(show);
        if (!show) {
            $('#pnlCVOR').find('input').val('');
        }
    });

    $('.radio input.wsib').on('change', function () {
        var show = $(this).val() == 'True';
        $('#pnlWSIB').toggle(show);
        if (!show) {
            $('#pnlWSIB').find('input').val('');
        }
    });

    //disable checkboxes for CVORAndExpiryDate
    var disableCVORAndExpiryDateCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#HaulerDetails_cbCVORNumber').removeAttr('disabled');
            $('#HaulerDetails_cbCVORExpiryDate').removeAttr('disabled');
        }
        else {
            $('#HaulerDetails_cbCVORNumber').attr('disabled', 'disabled').removeAttr('checked');
            $('#HaulerDetails_cbCVORExpiryDate').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    //disable checkboxes for WSIBNumber
    var disableWSIBNumberCheckbox = function (element) {
        if ($(element).is(':checked')) {
            $('#HaulerDetails_cbWSIBNumber').removeAttr('disabled');
        }
        else {
            $('#HaulerDetails_cbWSIBNumber').attr('disabled', 'disabled').removeAttr('checked');
        }
    }

    $('#HaulerDetails_cbHIsGVWR').on('change', function () {
        disableCVORAndExpiryDateCheckbox(this);
    });

    $('#HaulerDetails_cbHasMoreThanOneEmp').on('change', function () {
        disableWSIBNumberCheckbox(this);
    });

    disabledHSTTextBox();
    $('#pnlCVOR').toggle($('.radio input.cvor:checked').val() == 'True');
    $('#pnlWSIB').toggle($('.radio input.wsib:checked').val() == 'True');
}

function TireDetails() {
    $('#cb_tiredetails').change(function () {
        if ($(this).is(":checked")) $('#cb_tiredetails').attr('checked', true);
        else $('#cb_tiredetails').attr('checked', false);
    });

    //if OTS Processor is null disable the dropdown list
    if (!$('input[type=radio][class=HHasRelatioshipWithProcessor]:checked').val() || $('input[type=radio][class=HHasRelatioshipWithProcessor]:checked').val() == 'False') {
        $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", true);
    }

    /*Needed for tiredetails validation */
    var OTSProcessorRadio;
    var OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();

    $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
        if (this.checked)
            OTSProcessorRadio = $(this).val();
    });

    $('input[type=radio][class=HHasRelatioshipWithProcessor]').change(function () {
        DisplayProcessorLetterTireDetails();

        OTSProcessorRadio = $(this).val();

        if (OTSProcessorRadio == "False") {
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').closest('.form-group').removeClass('has-error').removeClass('has-success');
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').addClass('has-required');
            $("#modalConfirmRelationshipWithProcessor").modal('show');
        }
        else {
            $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
            $('select[name="TireDetails.HRelatedProcessor"] option:selected').removeClass('has-success').removeClass('has-required');
        }
        OTSProcessorDropDown = $("select[name='TireDetails.HRelatedProcessor'] option:selected").index();
        $("#fmTireDetails").valid();
    });

    $('select[name="TireDetails.HRelatedProcessor"]').change(function () {
        $("#fmTireDetails").valid();
    });

    $('input[type=checkbox][name="TireDetails.TireItem"]').change(function () {
        $("#fmTireDetails").valid();
    });

    $("#modalConfirmRelationshipWithProcessorNo").on("click", function () {
        $("#modalConfirmRelationshipWithProcessor").modal('hide');
        $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", false);
        $('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
        OTSProcessorRadio = "True";
        $("#fmTireDetails").valid();
        $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
            if ($(this).val() == "True") {
                this.checked = true;
            }
            else this.checked = false;
        });
    });
    $("#modalConfirmRelationshipWithProcessorYes").on("click", function () {
        $("#modalConfirmRelationshipWithProcessor").modal('hide');
        $('select[name="TireDetails.HRelatedProcessor"]').attr("disabled", true);
        $('select[name="TireDetails.HRelatedProcessor"]')[0].selectedIndex = 0;
    });
    /*End tire details validation*/
}

function SortyardDetails() {
    var sortYardtempBase = $(".sortyardtemp").val();
    var sortYardtempIncremental;
    var sortYardTemplateCount = $('#sorYardCount').val();
    var maxStorageCap = 0;
    var currAddCount = 0;
    $('#SortYardCount').text("0");

    for (var i = 0; i < sortYardTemplateCount; i++) {
        var sortYardTemplate = $("#sortYardTemplate" + i).val();
        sortYardTemplate = "<div class='sortYardtemplate" + i + "'>" + sortYardTemplate + '</div><hr>';

        $(sortYardTemplate).appendTo('#fmSortYardDetails');

        //check if to hide certificate of approval
        var storage = $(".sortYardtemplate" + i + " input[data-maxstorage]");
        storage = parseFloat($(storage).attr('data-maxstorage'));

        if (!isNaN(storage)) {
            if (storage < 50) {
                //hide the div
                var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                $(certificate).closest('.col-md-4').hide();
            }
        }
        else {
            //hide the div
            var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
            $(certificate).closest('.col-md-4').hide();
        }

        if (i == 0)
            $('.sort-yard-address-disabled').attr('disabled', 'disabled');
    }

    $('#SortYardCount').text(sortYardTemplateCount);

    $('#btnAddSort').on('click', function () {
        addSortRow();
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
        titleCase();
    });

    var addSortRow = function () {
        currAddCount = sortYardTemplateCount++;
        $('#SortYardCount').text(sortYardTemplateCount);
        sortYardtempIncremental = "<div class='ctemp'><div class='sortYardtemplate" + currAddCount + "'>" + sortYardtempBase + '</div></div><hr>';
        $(sortYardtempIncremental).appendTo('#fmSortYardDetails');

        //set the datamaxstorage of the new added sortyard to 0 so its not nan
        $('.sortYardtemplate' + currAddCount + ' input[data-maxstorage]').attr('data-maxstorage', 0);
        $('.sortYardtemplate' + currAddCount + ' input[type="text"]').val('');

        for (var i = 0; i < sortYardTemplateCount; i++) {
            $(".sortYardtemplate" + i + " input[type='text']," + ".sortYardtemplate" + i + " input[type='checkbox']," + ".sortYardtemplate" + i + " input[type='hidden']," + ".sortYardtemplate" + i + " select").each(function () {
                if ((typeof $(this).attr('name') != 'undefined')) {
                    $(this).attr('name', $(this).attr('name').replace(/\d+/, i));
                }
                if ((typeof $(this).attr('id') != 'undefined')) {
                    $(this).attr('id', $(this).attr('id').replace(/\d+/, i));
                }
            });
            //check if to hide certificate of approval
            var storage = $(".sortYardtemplate" + i + " input[data-maxstorage]");
            storage = parseFloat($(storage).attr('data-maxstorage'));

            if (!isNaN(storage)) {
                if (storage < 50) {
                    //hide the div
                    var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                    $(certificate).closest('.col-md-4').hide();
                }
            }
            else {
                //hide the div
                var certificate = $(".sortYardtemplate" + i + " input[data-certificate]");
                $(certificate).closest('.col-md-4').hide();
            }

            if (i == currAddCount) {
                $('.sortYardtemplate' + i + ' input[type=text]').removeClass('sort-yard-address-disabled');
                $('.sortYardtemplate' + i + ' select').removeClass('sort-yard-address-disabled');
            }
        }
    }
    var totalStorage = function () {
        var inputs = $("input[data-maxstorage]");
        maxStorageCap = 0;
        inputs.each(function () {
            if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
                maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
            }
        });
        if (!isNaN(parseFloat(maxStorageCap))) {
            $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
        }
    }

    var bindSortYardSelect = function () {
        $(".sortYardtemplate" + currAddCount + " select").each(function () {
            if (currAddCount > 0)
                $(this).children().removeAttr('selected');
        });
    }

    var renameSortYardLabel = function () {
        var label = ['.sort-yard-address1-label', '.sort-yard-address2-label', '.sort-yard-capacity'];
        for (var j = 0; j < label.length; j++) {
            var num = 1;
            $(label[j]).each(function () {
                var newLabel = $(this).text().replace(/\d+/, num++);
                $(this).text(newLabel);
            });
        }
    }

    //copies code from business location to sort yard
    $('.sortyard-address').on('focusout', function () {
        var attr = $(this).attr('data-sortyard');
        if (typeof attr !== typeof undefined && attr) {
            if ($("input[name*='" + attr + "']").length > 0)
                $("input[name*='" + attr + "']").val($(this).val());
            if ($("select[name*='" + attr + "']").length > 0) {
                $("select[name*='" + attr + "']").val($(this).val());
            }
        }
    });

    var initSortYard = function () {
        totalStorage();
        renameSortYardLabel();
        bindSortYardSelect();
    }
    initSortYard();

    // Sort Yard Details: This Sort Yard Capacity
    var sortYardDetailsTSYCContent = $('#sortYardDetailsTSYC').html(),
        sortYardDetailsTSYCSettings = {
            content: sortYardDetailsTSYCContent,
            width: 300,
            height: 100,
        };

    var popLargeTSYC = $('.sortYardDetailsTSYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsTSYCSettings));
}

function SupportingDocuments() {
    $('input[name="supportingDocumentsOptionMBL"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCLI"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCVOR"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionPRL"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionHST"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionWSIB"]').on('change', function () {
    });
    $('input[name="supportingDocumentsOptionCHQ"]').on('change', function () {
    });
}


/* COMMON FUNCTIONS */
function DisplayCVORDocumentOption() {
    var show = ($('#HaulerDetails_HIsGvwr:checked').val() == 'True');
    $('#cVORDocument').toggle(show);
    PanelGreenCheckSuppDoc();
}

function DisplayWSIBDocumentOption() {
    var show = ($('#HaulerDetails_HasMoreThanOneEmp:checked').val() == 'True');
    $('#wSIBDocument').toggle(show);
    PanelGreenCheckSuppDoc();
}

function DisplayHSTDocumentOption() {
    var show = !$('#HaulerDetails_IsTaxExempt').is(':checked');
    $('#hSTDocument').toggle(show);
    PanelGreenCheckSuppDoc();
}

function DisplayProcessorLetterTireDetails() {
    var show = ($('.HHasRelatioshipWithProcessor:checked').val() == 'True');
    $('#processorRelationshipLetter').toggle(show);
    PanelGreenCheckSuppDoc();
}

var certificateOfApproval = function () {
    var allSortYards = 0;
    $("input[data-maxstorage]").each(function () {
        $(this).attr('data-maxstorage', $(this).val());
        var totalStorageVal = parseFloat($(this).attr('data-maxstorage'));
        if (!totalStorageVal) {
            totalStorageVal = 0;
        }
        if (totalStorageVal > 0) {
            allSortYards++;
        }
        var certApp = $(this).parents('.row').find('.COA');
        if (totalStorageVal < 50) {
            certApp.find('input:text').each(function () {
                $(this).val('');
            });
            certApp.hide();
        }
        else {
            certApp.show();
        }
    });
    $('#SortYardCount').text(allSortYards);
}

var totalStorage = function () {
    var maxStorageCap = 0;
    var inputs = $("input[data-maxstorage]");
    inputs.each(function () {
        if (!isNaN(parseFloat($(this).attr('data-maxstorage')))) {
            maxStorageCap += parseFloat($(this).attr('data-maxstorage'));
        }
    });
    if (!isNaN(parseFloat(maxStorageCap))) {
        $('#SortYardMaxCapacity').text(maxStorageCap.toFixed(4));
    }
}

/* END COMMON FUNCTIONS */

function InitAutoSave() {
    $('#AppHaulerFormID :input').on('change', function () {
        var id = this.id;
        var apphauler = $(this);
            //OTSTM2-630
        if (id == '') {
            id = this.name;
        }
        var actLogStr = 'panel:' + $(this.closest('form > div.panel.panel-default')).find('div.panel-heading > h4 > a').text().trim() + ' ' + id + ' ';
        switch (this.type) {
            case 'checkbox':
                actLogStr += ' checkbox:' + this.parentElement.textContent.trim().split('\n')[0] + ' ';
                actLogStr += this.checked ? 'checked' : 'unchecked';
                break;
            case 'radio':
                actLogStr += $(this.closest('div.radio')).siblings('label').text();
                actLogStr += ' radio btn set to :' + this.parentElement.textContent.trim().split('\n')[0] + ' ';
                //actLogStr += this.checked ? 'checked' : 'unchecked';
                break;
            case 'select-one':
                actLogStr += ' drop down list:';
                actLogStr += this.defaultValue + ' -> ' + this.options[this.selectedIndex].text;
                if (this.defaultValue === '') { //initial filling drop down, don't track this.
                    actLogStr = '';
                }
                break;
            default:
                actLogStr += this.defaultValue + ' -> ' + this.value;
                break;
        }
        Global.Transaction.ActivitiesLog = actLogStr;
        Global.Transaction.ActivitiesLog = '';//this line avoid tracking call, delete it when enable activities tracking
        this.defaultValue = this.value;
        if (this.type == 'select-one') {
            this.defaultValue = this.options[this.selectedIndex].text;
        }

        $.when(
        apphauler.focusout()).then(function () {
            //fix
            certificateOfApproval();
            processJson();
            totalStorage();
            contactAddressSameAs();

            //if (id != "HRelatedProcessor") {
            //    certificateOfApproval();
            //    processJson();
            //    totalStorage();
            //    contactAddressSameAs();
            //}
        });
    });

    $('#AppHaulerFormID').on('change', '.ctemp', function () {
        var id = this.id;
        var ctemp = $(this);
        $.when(
        ctemp.focusout()).then(function () {
            if (id != "HRelatedProcessor") {
                certificateOfApproval();
                processJson();
                totalStorage();
                contactAddressSameAs();
            }
        });
    });
}

function processJson() {
    handleBusinessAddressCheckboxForContact();

    var myform = $('#AppHaulerFormID');
    var disabled = myform.find(':input:disabled').removeAttr('disabled');
    var data = $('form').serializeArray();
    data = data.concat(
        $('#AppHaulerFormID input[type=checkbox]:not(:checked)').map(
        function () {
            return { "name": this.name, "value": this.value }
        }).get());
    data = data.concat(
    $('#AppHaulerFormID input[type=radio]:checked').map(
        function () {
            return { "name": this.name, "value": this.value }
        }).get());

    $('#item_checkbox input[type="checkbox"]').each(function () {
        if (!_isEmpty($(this).val()) && (typeof $(this).attr('data-myattri') != 'undefined')) {
            data.push({ name: 'TireDetailsItemTypeString[]', value: $(this).attr('data-myattri') + ':' + (this.checked ? 'true' : 'false') + ':' + $(this).attr('value') });
        }
    });

    if ($('#MailingAddressSameAsBusiness').is(':checked')) {
        data.push({ name: 'BusinessLocation.MailAddressSameAsBusinessAddress', value: true });
    }
    disabled.attr('disabled', 'disabled');

    if (Global.ParticipantIndex.Model.Status == 'BackToApplicant') {
        //insert current invalid forms back into model
        var tmpList = Global.ParticipantIndex.Model.InvalidFormFields;
        for (var i = 0; i < tmpList.length; i++) {
            data.push({ name: 'InvalidFormFields[]', value: tmpList[i] });
        }
    }

    if (Global.ParticipantIndex.Model.Status == 'Open' || Global.ParticipantIndex.Model.Status == 'None') {
        var len = data.length;
        for (var j = 0; j < len; j++) {
            data.push({ name: 'InvalidFormFields[]', value: data[j].name });
        }
    }
    //remove leading and trailing space
    var len = data.length;
    for (var i = 0; i < len; i++) {
        data[i].value = $.trim(data[i].value);
    }
    $.ajax({
        url: '/Hauler/Registration/SaveModel',
        dataType: 'json',
        type: 'POST',
        data: $.param(data),
        success: function (result) {
            if (result) {
                switch (result.toLowerCase()) {
                    case "success":
                        if (Global.Transaction.ActivitiesLog) {//if Global.Transaction.ActivitiesLog has content
                            $.ajax({
                                url: '/System/Common/AddCommonActivity',
                                dataType: 'json',
                                type: 'POST',
                                data: { Id: Global.Transaction.ClaimId, logContent: Global.Transaction.ActivitiesLog },
                                success: function (result) {
                                    console.log('logged:',Global.Transaction.ActivitiesLog);
                                    if (result) {
                                        switch (result.status) {
                                            case "success":
                                                //console.log('processJson() success, log activity trace here.');
                                                break;
                                            default:
                                                //window.location = 'http://' + window.location.hostname + '/Hauler/Registration/Message?msg=' + result;
                                                break;
                                        }
                                    }
                                },
                                error: function (result) {
                                }
                            });
                        }
                        break;
                    default:
                        window.location = 'http://' + window.location.hostname + '/Hauler/Registration/Message?msg=' + result;
                        break;
                }
            }
        },
        error: function (result) {
        }
    });
};

/* GABE STYLES */

(function () {
    // Sort Yard Details: All Sort Yards Capacity
    var sortYardDetailsASYCContent = $('#sortYardDetailsASYC').html(),
        sortYardDetailsASYCSettings = {
            content: sortYardDetailsASYCContent,
            width: 300,
            height: 100,
        };
    var popLargeASYC = $('.sortYardDetailsASYC').webuiPopover('destroy').webuiPopover($.extend({}, settings, sortYardDetailsASYCSettings));

    // Tire Details: Passenger & Light Truck Tires (PLT)
    var tiredetailsPopoverPLTContent = $('#tiredetailsPopoverPLT').html(),
        tiredetailsPopoverPLTSettings = {
            content: tiredetailsPopoverPLTContent,
            width: 500,
            height: 210,
        };
    var popLargePLT = $('.tiredetailsPopoverPLT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverPLTSettings));

    // Tire Details: Medium Truck Tires (MT)
    var tiredetailsPopoverMTContent = $('#tiredetailsPopoverMT').html(),
        tiredetailsPopoverMTSettings = {
            content: tiredetailsPopoverMTContent,
            width: 380,
            height: 160,
        };
    var popLargeMT = $('.tiredetailsPopoverMT').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMTSettings));

    // Tire Details: Agricultural Drive & Logger Skidder Tires (AGLS)
    var tiredetailsPopoverAGLSContent = $('#tiredetailsPopoverAGLS').html(),
        tiredetailsPopoverAGLSSettings = {
            content: tiredetailsPopoverAGLSContent,
            width: 560,
            height: 230,
        };
    var popLargeAGLS = $('.tiredetailsPopoverAGLS').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverAGLSSettings));

    // Tire Details: Industrial Tires (IND)
    var tiredetailsPopoverINDContent = $('#tiredetailsPopoverIND').html(),
        tiredetailsPopoverINDSettings = {
            content: tiredetailsPopoverINDContent,
            width: 360,
            height: 120,
        };
    var popLargeIND = $('.tiredetailsPopoverIND').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverINDSettings));

    // Tire Details: Small Off the Road Tire (SOTR)
    var tiredetailsPopoverSOTRContent = $('#tiredetailsPopoverSOTR').html(),
        tiredetailsPopoverSOTRSettings = {
            content: tiredetailsPopoverSOTRContent,
            width: 430,
            height: 120,
        };
    var popLargeSOTR = $('.tiredetailsPopoverSOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverSOTRSettings));

    // Tire Details: Medium Off the Road Tire (MOTR)
    var tiredetailsPopoverMOTRContent = $('#tiredetailsPopoverMOTR').html(),
        tiredetailsPopoverMOTRSettings = {
            content: tiredetailsPopoverMOTRContent,
            width: 430,
            height: 120,
        };
    var popLargeMOTR = $('.tiredetailsPopoverMOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverMOTRSettings));

    // Tire Details: Large Off the Road Tire (LOTR)
    var tiredetailsPopoverLOTRContent = $('#tiredetailsPopoverLOTR').html(),
        tiredetailsPopoverLOTRSettings = {
            content: tiredetailsPopoverLOTRContent,
            width: 420,
            height: 120,
        };
    var popLargeLOTR = $('.tiredetailsPopoverLOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverLOTRSettings));

    // Tire Details: Giant Off the Road Tire (GOTR)
    var tiredetailsPopoverGOTRContent = $('#tiredetailsPopoverGOTR').html(),
        tiredetailsPopoverGOTRSettings = {
            content: tiredetailsPopoverGOTRContent,
            width: 420,
            height: 100,
        };
    var popLargeGOTR = $('.tiredetailsPopoverGOTR').webuiPopover('destroy').webuiPopover($.extend({}, settings, tiredetailsPopoverGOTRSettings));

    // Supporitng Documents: Articles of Incorporation or Master Business Licence (MBL)
    var supportingDocumentsAIMBLContent = $('#supportingDocumentsAIMBL').html(),
        supportingDocumentsAIMBLSettings = {
            content: supportingDocumentsAIMBLContent,
            width: 500,
            height: 140,
        };
    var popLargeAIMBL = $('.supportingDocumentsAIMBL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsAIMBLSettings));

    // Supporitng Documents: Commercial Liability Insurance
    var supportingDocumentsCLIContent = $('#supportingDocumentsCLI').html(),
        supportingDocumentsCLISettings = {
            content: supportingDocumentsCLIContent,
            width: 500,
            height: 260,
        };
    var popLargeCLI = $('.supportingDocumentsCLI').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCLISettings));

    // Supporitng Documents: CVOR Abstract Level II
    var supportingDocumentsCVORContent = $('#supportingDocumentsCVOR').html(),
        supportingDocumentsCVORSettings = {
            content: supportingDocumentsCVORContent,
            width: 460,
            height: 170,
        };
    var popLargeCVOR = $('.supportingDocumentsCVOR').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsCVORSettings));

    // Supporitng Documents: Processor Relationship Letter
    var supportingDocumentsPRLContent = $('#supportingDocumentsPRL').html(),
        supportingDocumentsPRLSettings = {
            content: supportingDocumentsPRLContent,
            width: 460,
            height: 105,
        };
    var popLargePRL = $('.supportingDocumentsPRL').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsPRLSettings));

    // Supporitng Documents: supportingDocumentsHST
    var supportingDocumentsHSTContent = $('#supportingDocumentsHST').html(),
    supportingDocumentsHSTSettings = {
        content: supportingDocumentsHSTContent,
        width: 500,
        height: 160,
    };
    var popLargePRL = $('.supportingDocumentsHST').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsHSTSettings));

    // Supporitng Documents: supportingDocumentWSIB
    var supportingDocumentWSIBContent = $('#supportingDocumentWSIB').html(),
        supportingDocumentWSIBSettings = {
            content: supportingDocumentWSIBContent,
            width: 500,
            height: 86,
        };
    var popLargeVOCH = $('.supportingDocumentWSIB').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentWSIBSettings));

    // Supporitng Documents: Void Cheque
    var supportingDocumentsVOCHContent = $('#supportingDocumentsVOCH').html(),
        supportingDocumentsVOCHSettings = {
            content: supportingDocumentsVOCHContent,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.supportingDocumentsVOCH').webuiPopover('destroy').webuiPopover($.extend({}, settings, supportingDocumentsVOCHSettings));

    // Tire Details & Service Area: Zone 1
    var serviceAreaZone1Content = $('#serviceAreaZone1').html(),
        serviceAreaZone1Settings = {
            content: serviceAreaZone1Content,
            width: 674,
            height: 359,
        };
    var popLargeVOCH = $('.serviceAreaZone1').webuiPopover('destroy').webuiPopover($.extend({}, settings, serviceAreaZone1Settings));

    $('#AppHaulerFormID :input').each(function () {
        this.defaultValue = this.value;
        if (this.type == 'select-one') {
            this.defaultValue = this.options[this.selectedIndex].text;
        }
    })

})();

/* END GABE STYLES */