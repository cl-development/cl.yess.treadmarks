﻿$(function () {
    CommonValidation.initializeStandardFormValidations();

    var cbHaulerDetailsHaulerDetailsVendorIsTaxExempt = $('#HaulerDetails_CommercialLiabHSTNumber');

    /* START GLOBAL VALIDATION METHODS */
    $.validator.addMethod('dynamic-certificate-required', function (value, element) {
        var index = $(element).index('.dynamic-certificate-required');
        var maxStorage = $('input[name="SortYardDetails[' + index + '].MaxStorageCapacity"]').attr('data-maxstorage');
        var val = parseFloat(maxStorage);
        if (val && val >= 50) {
            if (value) return true;
        }
        return false;
    }, function (params, element) {		
        return dynamicErrorMsg(params, element)
    });
    /* END GLOBAL VALIDATION METHODS */

    /* START BUSINESS INFORMATION VALIDATION */

    var validatorBusinessInfo = $('#fmBusinessLocation').validate({
        ignore: '.no-validate, :hidden',
        //onkeyup:function(element){
        //    this.element(element);
        //},
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'BusinessLocation.BusinessName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.OperatingName': {
                required: true,
                //regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'BusinessLocation.BusinessAddress.AddressLine1': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.City': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.Province': {
                required: true,
            },
            'BusinessLocation.BusinessAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.BusinessAddress.Postal': {
                required: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$'
            },
            'BusinessLocation.BusinessAddress.Country': {
                required: true,
            },
            'BusinessLocation.Phone': {
                required: true,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$',
            },
            'BusinessLocation.Extension': {
                required: false
            },
            //'BusinessLocation.BusinessLocationAddress.Ext': {
            //    required: false,
            //    //inInvalidList: true
            //},
            'BusinessLocation.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$',
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.AddressLine1': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.City': {
                required: function (element) {
                    return !cbMailingAddressSameAsBusiness.is(':checked');
                }
            },
            'BusinessLocation.MailingAddress.Province': {
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.AddressLine2': {
                required: false,
                //inInvalidList: true
            },
            'BusinessLocation.MailingAddress.Postal': {
                //inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            },
            'BusinessLocation.MailingAddress.Country': {
                // inInvalidList: true,
                required: {
                    depends: function (element) {
                        return !cbMailingAddressSameAsBusiness.is(':checked');
                    }
                }
            }
        },
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failed
        },
        messages: {
            'BusinessLocation.BusinessName': 'Invalid Business Name',
            'BusinessLocation.OperatingName': 'Invalid Operating Name',
            'BusinessLocation.BusinessAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.BusinessAddress.City': 'Invalid City',
            'BusinessLocation.BusinessAddress.Province': 'Invalid Province/State',
            'BusinessLocation.BusinessAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.BusinessAddress.Postal': 'Invalid Postal Code',
            'BusinessLocation.BusinessAddress.Country': 'Invalid Country',
            'BusinessLocation.Phone': {
                regquired: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'BusinessLocation.Extension': 'Invalid Ext',
            'BusinessLocation.Email': 'Invalid Email',
            'BusinessLocation.MailingAddress.AddressLine1': 'Invalid Address Line 1',
            'BusinessLocation.MailingAddress.City': 'Invalid City',
            'BusinessLocation.MailingAddress.Province': 'Invalid Province/State',
            'BusinessLocation.MailingAddress.AddressLine2': 'Invalid Address Line 2',
            'BusinessLocation.MailingAddress.Postal': 'Invalid Postal/Zip',
            'BusinessLocation.MailingAddress.Country': 'Invalid Country',
        }
    });

    /* END BUSINESS INFORMATION VALIDATION */

    /* CONTACT INFORMATION VALIDATION */

    var validatorContactInfo = $('#fmContactInfo').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) { },
        errorPlacement: function (error, element) {
            if (doHighlight)
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            if (doHighlight) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');
                }
                $(element).closest('.form-group').addClass('has-error');
                if ($(element).next('span').hasClass('glyphicon-ok'))
                    $(element).next('span').removeClass('glyphicon-ok');
                $(element).next('span').addClass('glyphicon-remove');
            }
        },
        unhighlight: function (element) {
            if (doHighlight) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
        },
        success: function () {
            //complete
        },
        failure: function () {
            //failure
        },
        rules: {
            '': {
                required: true,
                regex: '^[A-Za-z0-9&-.,_ ]{1,}$'
            },
            'ContactInformation[0].ContactInformationContact.Name': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.Position': {
                required: true
            },
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Ext': {
                required: false
            },
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                required: false,
                regex: '^[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}$'
            },
            'ContactInformation[0].ContactInformationContact.Email': {
                required: true,
                regex: '^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$'
            },
            'ContactInformation[0].ContactInformationAddress.Address1': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.City': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Province': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Address2': {
                required: false
            },
            'ContactInformation[0].ContactInformationAddress.PostalCode': {
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
            'ContactInformation[0].ContactInformationAddress.Country': {
                required: function (element) {
                    return !cbContactAddressSameAsBusinessAddress.is(':checked');
                }
            },
        },
        messages: {
            'ContactInformation[0].ContactInformationContact.Name': 'Invalid Primary Contact Name',
            'ContactInformation[0].ContactInformationContact.Position': 'Invalid Position',
            'ContactInformation[0].ContactInformationContact.PhoneNumber': {
                required: 'Invalid Phone',
                regex: 'Expected format ###-###-####',
                inInvalidList: 'Invalid Phone',
                required: 'Invalid Phone'
            },
            'ContactInformation[0].ContactInformationContact.Ext': 'Invalid Ext.',
            'ContactInformation[0].ContactInformationContact.AlternatePhoneNumber': {
                regex: 'Expected format ###-###-####',
            },
            'ContactInformation[0].ContactInformationContact.Email': 'Invalid Email',
            'ContactInformation[0].ContactInformationAddress.Address1': 'Invalid Address Line 1',
            'ContactInformation[0].ContactInformationAddress.Address2': 'Invalid Address Line 2',
            'ContactInformation[0].ContactInformationAddress.City': 'Invalid City',
            'ContactInformation[0].ContactInformationAddress.PostalCode': 'Invalid Postal /Zip',
            'ContactInformation[0].ContactInformationAddress.Province': 'Invalid Province/State',
            'ContactInformation[0].ContactInformationAddress.Country': 'Invalid Country',
        }
    });

    /* END CONTACT INFORMATION VALIDATION */
    /* START SORT YARD DETAILS VALIDATION */
    var validatorSortYardInfo = $('#fmSortYardDetails').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.City': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Province': {
                inInvalidList: true,
                required: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.Address2': {
                inInvalidList: true,
            },
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': {
                inInvalidList: true,
                regex: '^[A-Za-z][0-9][A-Za-z][ ]?[0-9][A-Za-z][0-9]|[0-9]{5}(?:[- ][0-9]{4})?$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsAddress.Country': {
                inInvalidList: true,
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': {
                inInvalidList: true,
                regex: '^[0-9]{1,}$',
                required: true
            },
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': {
                inInvalidList: true,
                required: function (element) {
                    var txtMaxStorageCap = $('#SortYardDetails_0__SortYardDetailsVendorStorageSiteModel_MaxStorageCapacity');
                    var val = parseFloat(txtMaxStorageCap.val());
                    if (val) return val >= 50;
                    else return false;
                }
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok')) $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
            }
        },
        unhighlight: function (element) {
			var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
        },
        messages: {
            'SortYardDetails[0].SortYardDetailsAddress.Address1': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine1,
            'SortYardDetails[0].SortYardDetailsAddress.City': Global.ParticipantIndex.Resources.ValidationMsgInvalidCity,
            'SortYardDetails[0].SortYardDetailsAddress.Province': Global.ParticipantIndex.Resources.ValidationMsgInvalidProvinceState,
            'SortYardDetails[0].SortYardDetailsAddress.Address2': Global.ParticipantIndex.Resources.ValidationMsgInvalidAddressLine2,
            'SortYardDetails[0].SortYardDetailsAddress.PostalCode': Global.ParticipantIndex.Resources.ValidationMsgInvalidPostalCode,
            'SortYardDetails[0].SortYardDetailsAddress.Country': Global.ParticipantIndex.Resources.ValidationMsgInvalidCountry,
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.MaxStorageCapacity': Global.ParticipantIndex.Resources.ValidationMsgInvalidCapacity,
            'SortYardDetails[0].SortYardDetailsVendorStorageSiteModel.CertificateOfApprovalNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidCertificateOfApproval,
        }
    });
    /* END SORT YARD DETAILS VALIDATION */

    /* HAULER DETAILS */
    var validatorHaulerDetailsInfo = $('#fmHaulerDetails').validate({
        ignore: '.no-validate, :hidden, [readonly=readonly]',
        onfocusout: function (element) {
            this.element(element);
        },
        onclick: function (element) {
            this.element(element);
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'));
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                    $(element).closest('.form-group').removeClass('has-required');
                    $(element).closest('.form-group').removeClass('has-success');

                    $(element).closest('.form-group').addClass('has-error');
                    if ($(element).next('span').hasClass('glyphicon-ok'))
                        $(element).next('span').removeClass('glyphicon-ok');
                    $(element).next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('HaulerDetails.CommercialLiabInsurerExpDate') > -1) {
                    $(element).closest('#dpInsuranceExpiryDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpInsuranceExpiryDate').next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('HaulerDetails.BusinessStartDate') > -1) {
                    $(element).closest('#dpBusinessStartDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpBusinessStartDate').next('span').addClass('glyphicon-remove');
                }
                if (propertyName.indexOf('HaulerDetails.CvorExpiryDate') > -1) {
                    $(element).closest('#dpCVORExpiryDate').next('span').removeClass('glyphicon-ok');
                    $(element).closest('#dpCVORExpiryDate').next('span').addClass('glyphicon-remove');
                }
            }
            else {
                // $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                // $(element).closest('.form-group').addClass('has-required');
            }

            //if radio buttons have nothing checked dont highlight
            if ($(element).attr('type') == "radio") {
                var val = "input:radio[name='" + $(element).attr('name') + "']";

                if (!$(val).is(":checked")) {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                    $(element).closest('.form-group').addClass('has-required');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if ($(element).val() || index > -1) {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                $(element).next('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');

                if (propertyName.indexOf('HaulerDetails.CommercialLiabInsurerExpDate') > -1) {
                    $(element).closest('#dpInsuranceExpiryDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpInsuranceExpiryDate').next('span').addClass('glyphicon-ok');
                }
                if (propertyName.indexOf('HaulerDetails.BusinessStartDate') > -1) {
                    $(element).closest('#dpBusinessStartDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpBusinessStartDate').next('span').addClass('glyphicon-ok');
                }
                if (propertyName.indexOf('HaulerDetails.CvorExpiryDate') > -1) {
                    $(element).closest('#dpCVORExpiryDate').next('span').removeClass('glyphicon-remove');
                    $(element).closest('#dpCVORExpiryDate').next('span').addClass('glyphicon-ok');
                }
            }
            else {
                $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
                $(element).closest('.form-group').addClass('has-required');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        rules: {
            'HaulerDetails.BusinessStartDate': {
                required: true,
                lessThanOrEqualTodayDate: true,
                inInvalidList: true,
            },
            'HaulerDetails.BusinessNumber': {
                required: true,
                inInvalidList: true,
            },
            'HaulerDetails.CommercialLiabHstNumber': {
                inInvalidList: true,
                regex: '^[0-9]{9}$',
                required: function (element) {
                    //return !cbHaulerDetailsHaulerDetailsVendorIsTaxExempt.is(':checked');
                    return !$('#HaulerDetails_IsTaxExempt').is(':checked');
                }
            },
            'HaulerDetails.IsTaxExempt': {
                required: false
            },
            'HaulerDetails.HasMoreThanOneEmp': {
                inInvalidList: true,
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#HaulerDetails_WsibNumber'), true);
                    }
                    return true;
                }
            },
            'HaulerDetails.HIsGvwr': {
                inInvalidList: true,
                required: function (element) {
                    if ($(element).is(':checked')) {
                        resetHighlight($('#HaulerDetails_CvorNumber'), true);
                        resetHighlight($('#HaulerDetails_CvorExpiryDate'), true);
                    }
                    return true;
                }
            },
            'HaulerDetails.CommercialLiabInsurerName': {
                required: true,
                inInvalidList: true,
            },
            'HaulerDetails.CommercialLiabInsurerExpDate': {
                required: true,
                greaterThanTodayDate: true,
                inInvalidList: true,
            },
            'HaulerDetails.CvorNumber': {
                inInvalidList: true,
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR = $('#HaulerDetails_HIsGvwr:checked');
                    return rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'HaulerDetails.CvorExpiryDate': {
                greaterThanTodayDate: true,
                inInvalidList: true,
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR = $('#HaulerDetails_HIsGvwr:checked')
                    return rbtnHaulerDetailsHaulerDetailsVendorHIsGVWR.val() == 'True';
                }
            },
            'HaulerDetails.WsibNumber': {
                inInvalidList: true,
                regex: '^[0-9]{7}$',
                required: function (element) {
                    var rbtnHaulerDetailsHaulerDetailsVendorHasMoreThanOneEmp = $('#HaulerDetails_HasMoreThanOneEmp:checked');
                    return rbtnHaulerDetailsHaulerDetailsVendorHasMoreThanOneEmp.val() == 'True';
                }
            },
        },
        messages: {
            'HaulerDetails.BusinessStartDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidBusinessStartDate,
            'HaulerDetails.BusinessNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidOntarioBusinessNumber,
            'HaulerDetails.CommercialLiabHstNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidHSTRegistrationNumber,
            'HaulerDetails.CommercialLiabInsurerName': Global.ParticipantIndex.Resources.ValidationMsgInvalidComercialLiabilityInsurance,
            'HaulerDetails.CommercialLiabInsurerExpDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidExpiryDate,
            'HaulerDetails.CvorExpiryDate': '',//Global.ParticipantIndex.Resources.ValidationMsgInvalidExpiryDate,
            'HaulerDetails.CvorNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidCVORNumber,
            'HaulerDetails.WsibNumber': Global.ParticipantIndex.Resources.ValidationMsgInvalidWSIPNumber,
            'HaulerDetails.HIsGvwr': Global.ParticipantIndex.Resources.ValidationMsgHIsGVWR,
            'HaulerDetails.HasMoreThanOneEmp': Global.ParticipantIndex.Resources.ValidationMsgHasMoreThanOneEmp
        }
    });

    /* END HAULER DETAILS */

    /* AUTOSAVE FEATURE */

    /************************************************************************************************************
    ******************************* TIRE DETAILS Participant START  ********************************************
    ************************************************************************************************************/
    $.validator.addMethod("TireDetailsRequired", function (value, element) {
        var radioStr = $(element).val();
        if (typeof radioStr == 'undefined') {
            return false;
        }
        var radioBool = JSON.parse(radioStr.toLowerCase());
        if (radioBool && $("select[name='TireDetails.HRelatedProcessor'] option:selected").index() == "0") {
            return false;
        }
        //value is no; thus return true
        return !radioBool;
    }, function (params, element) {
        return dynamicErrorMsg(params, element);
    });

    $.validator.addMethod("HRelatedProcessorRequired", function (value, element) {
        if (typeof $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'undefined') {
            return false;
        }
        else if ($("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == 'True' && $('#HRelatedProcessor')[0].selectedIndex == 0)
        {
            return false;
        }

        return true;
    }, function (params, element) {
        return dynamicErrorMsg(params, element)
    });

    var validatorTireDetailsInfo = $('#fmTireDetails').validate({
        ignore: '.no-validate, :hidden',
        onfocusout: function (element) {
            this.element(element);
        },
        submitHandler: function (form) {
        },
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).closest('#item_checkbox'));
                }
                else if (element.attr("type") == "radio") {
                    return false;
                }
                else {
                    ($(element).next('span').length == 0) ? $(error).insertAfter($(element)) : $(error).insertAfter($(element).next('span'))
                }
            }
        },
        errorElement: 'p',
        errorClass: 'help-block',
        rules: {
            'TireDetails.TireItem': {
                inInvalidList: true,
                required: true
            },

            'TireDetails.HHasRelatioshipWithProcessor': {
                inInvalidList: true,
                required: true,
                TireDetailsRequired: true
            },

            'TireDetails.HRelatedProcessor': {
               inInvalidList: true,
                required: {
                    depends: function (element) {
                        return $("input[class^=HHasRelatioshipWithProcessor]:radio:checked").val() == "True";
                    }
                },
                HRelatedProcessorRequired: true
            }
        },
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (isActiveElement(propertyName) || $(element).val() || index > -1) {
                if (index > -1) {
                    if ($(element).attr('name') == 'TireDetails.TireItem') {
                        $(element).closest('.form-group').css('border', '1px solid #a94442');
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    }

                    if ($(element).attr('name') == 'TireDetails.HHasRelatioshipWithProcessor') {
                        if ($(element).closest('.form-group').hasClass('has-required') || $(element).closest('.form-group').hasClass('has-success')) {
                            $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                        }

                        if ($('#ProcessDiv').children('.form-group').hasClass('has-required') || $('#ProcessDiv').children('.form-group').hasClass('has-success')) {
                            $('#ProcessDiv').children('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                        }
                    }
                }
                else {
                    //only highlight if any is not empty
                    $('#item_checkbox :input').each(function () {
                        if (!$(this).is(':checked')) {
                            if ($(element).attr('data-tiredetails')) {
                                if ($(element).closest('.form-group').hasClass('has-success') || $(element).closest('.form-group').hasClass('has-error'))
                                    $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                            }
                        }
                    });

                    if ($(element).attr('type') == "radio") {
                        var val = "input:radio[name='" + $(element).attr('name') + "']";
                        if ($(element).closest('.form-group').hasClass('has-success') || $(element).closest('.form-group').hasClass('has-error'))
                            $(element).closest('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                    }

                    if ($('#ProcessDiv').children('.form-group').hasClass('has-success')) {
                        $('#ProcessDiv').children('.form-group').removeClass('has-required').removeClass('has-success').addClass('has-error');
                    }
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);

            if (index > -1) {
                if ($(element).attr('name') == 'TireDetails.TireItem') {
                    $(element).closest('.form-group').css('border', 'none');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }

                if ($(element).attr('type') == "radio") {
                    var val = "input:radio[name='" + $(element).attr('name') + "']";

                    if ($(element).closest('.form-group').hasClass('has-error'))
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                }
                if ($('#ProcessDiv').children('.form-group').hasClass('has-error'))
                    $('#ProcessDiv').children('.form-group').removeClass('has-error').addClass('has-success');
            }
            else {
                //unhighlight if any is not empty
                $('#item_checkbox :input').each(function () {
                    if (!$(this).is(':checked')) {
                        if ($(element).attr('data-tiredetails')) {
                            $(element).closest('.form-group').css('border', 'none');

                            if ($(element).closest('.form-group').hasClass('has-error') || $(element).closest('.form-group').hasClass('has-required')) {
                                $(element).closest('.form-group').removeClass('has-error').removeClass('has-required').addClass('has-success');
                            }
                        }
                    }
                });

                $("input:radio[class^=HHasRelatioshipWithProcessor]").each(function (i) {
                    if (this.checked) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    }
                });
                if ($('#ProcessDiv').children('.form-group').hasClass('has-error'))
                    $('#ProcessDiv').children('.form-group').removeClass('has-error').addClass('has-success');
            }
        },
        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failed
        },
        messages: {
            'TireDetails.TireItem': Global.ParticipantIndex.Resources.ValidationMsgInvalidTireType,
            'TireDetails.HRelatedProcessor': Global.ParticipantIndex.Resources.ValidationMsgInvalidProcessor,
            'TireDetails.HHasRelatioshipWithProcessor': Global.ParticipantIndex.Resources.ValidationMsgHasRelatioshipWithProcessor
        }
    });

    /*END TIRE DETAIL VALIDATION */

    /* SUPPORTING DOCUMENT VALIDATION */

    var validatorSupportingDocInfo = $('#fmSupportingDoc').validate({
        ignore: '.no-validate, [readonly=readonly]',
        /*
        onfocusout: function (element) {
            this.element(element);
        },
		*/
        invalidHandler: function (e, validator) {
        },
        errorPlacement: function (error, element) {
        },
        errorElement: 'p',
        errorClass: 'help-block',
        highlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if ($(element).attr('name') == 'SupportingDocuments.RequiredDocuments') {
                    $(element).closest('.form-group').removeClass('has-success').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-error');
                }
            }
        },
        unhighlight: function (element) {
            var propertyName = $(element).attr('name');
            var index = panelLevelCheckErrorList.indexOf(propertyName);
            if (index > -1) {
                if ($(element).attr('name') == 'SupportingDocuments.RequiredDocuments') {
                    $(element).closest('.form-group').removeClass('has-error').removeClass('has-required');
                    $(element).closest('.form-group').addClass('has-success');
                }
            }
        },

        success: function (error) {
            error.remove();
        },
        failure: function () {
            //failure
        },
        rules: {
            'SupportingDocuments.RequiredDocuments': {
                inInvalidList: true
            }
        },
        messages: {
            'SupportingDocuments.RequiredDocuments': "Invalid Required Documents"
        }
    });

    /* END SUPPORTING DOCUMENT VALIDATION */

    /* INITIALIZATION */
    var init = function () {
        if (Global.ParticipantIndex.Model.InvalidFormFields && statusStr.toLowerCase() == 'backtoapplicant') {
            highlightFields();
        }

        //BusinessLocationModule.initializePanelGreenCheck();
		PanelBusinessLocationGreenCheck();		
        PanelContactInfoGreenCheck();
        AddButtonHandlerForGreenCheck();
        PanelSortYardGreenCheck();
        PanelTireDetailsGreenCheck();
        PanelHaulerDetailsGreenCheck();
        PanelSupportingDocumentGreenCheck();
        PanelTermsAndConditionsGreenCheck();
        ApplicationCommon.SubmitButtonHandler();
        disableBackspaceOnCheckBox();
    }

    init();
});

var CV = CommonValidation || {};
ImageStatusArray = (CV == {}) ? CV.getStatusImages() :  ['/Images/comp-register-all-status-flag-grey.png', '/Images/comp-register-all-status-flag-green.png'];

function PanelBusinessLocationGreenCheck() {
    $('#panelBusinessLocation').removeClass('collapse');
    $('#panelBusinessLocation').addClass('collapse-in');
    var result = $("#fmBusinessLocation").valid();
    if (result) {

        $('#flagBusinessLocation').attr('src', ImageStatusArray[1]);
    }
    else {
        $('#flagBusinessLocation').attr('src', ImageStatusArray[0]);
    }
    $('#fmBusinessLocation').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);
            $.when(
            businessLoc.focusout()).then(function () {

                if ($(this).hasClass("isMailingAddressDifferent") && !$(this).is(":checked")) {
                    $('#flagBusinessLocation').attr('src', ImageStatusArray[0]);
                }
                else {
                    if ($("#fmBusinessLocation").valid()) {

                        $('#flagBusinessLocation').attr('src', ImageStatusArray[1]);
                    }
                    else {
                        $('#flagBusinessLocation').attr('src', ImageStatusArray[0]);
                    }
                }
                //SubmitButtonHandler();
				ApplicationCommon.SubmitButtonHandler();				
            });
        });
    })
}

function PanelSupportingDocumentGreenCheck() {
    $('#panelSupportingDocuments').removeClass('collapse');
    $('#panelSupportingDocuments').addClass('collapse-in');
}

function PanelSortYardGreenCheck() {
    $('#panelSortYardDetails').removeClass('collapse');
    $('#panelSortYardDetails').addClass('collapse-in');


	//TODO OTSTM-2179 workaround need to call valid() twice as
	//the first time is always returning false 
    var isValid = $("#fmSortYardDetails").valid();	
	isValid = $("#fmSortYardDetails").valid();
		
    if (isValid) {
        $('#flagSortYard').attr('src', ImageStatusArray[1]);
    }
    else {
        $('#flagSortYard').attr('src', ImageStatusArray[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmSortYardDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmSortYardDetails").valid()) {
                    $('#flagSortYard').attr('src', ImageStatusArray[1]);
                }
                else {
                    $('#flagSortYard').attr('src', ImageStatusArray[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

var resetHighlight = function (element, isRequired) {
    $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');
    $(element).next('span').removeClass('glyphicon-remove');
    $(element).nextAll('.help-block:first').remove();
    if (isRequired) {
        $(element).closest('.form-group').addClass('has-required');
        $(element).next('span').addClass('glyphicon-ok');
    }
}

function PanelTireDetailsGreenCheck() {

    $('#panelTireDetails').removeClass('collapse');
    $('#panelTireDetails').addClass('collapse-in');

    var isValid = $("#fmTireDetails").valid();

    if (isValid) {
        $('#flagTireDetails').attr('src', ImageStatusArray[1]);
    }
    else {
        $('#flagTireDetails').attr('src', ImageStatusArray[0]);
    }

    //any changes to the contact info form is handled here
    $('#fmTireDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                if ($("#fmTireDetails").valid()) {
                    $('#flagTireDetails').attr('src', ImageStatusArray[1]);
                }
                else {
                    $('#flagTireDetails').attr('src', ImageStatusArray[0]);
                }
                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function PanelHaulerDetailsGreenCheck() {
    $('#panelHaulerDetails').removeClass('collapse');
    $('#panelHaulerDetails').addClass('collapse-in');

    HaulerDetailEnableReadOnly(false);

    var isValid = $("#fmHaulerDetails").valid();
    $('#flagHaulerDetails').attr('src', isValid ? ImageStatusArray[1] : ImageStatusArray[0]);

    HaulerDetailEnableReadOnly(true);

    ///any changes to the hauler info form is handled here
    $('#fmHaulerDetails').find(':input').each(function () {
        $(this).on("change", function () {
            var businessLoc = $(this);

            $.when(
            businessLoc.focusout()).then(function () {
                HaulerDetailEnableReadOnly(false);

                if ($('#HaulerDetails_IsTaxExempt').is(':checked')) {
                    $('#flagHaulerDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#HaulerDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#HaulerDetails_CommercialLiabHstNumber').val('').attr('readonly', true);
                }
                else {
                    $('#flagHaulerDetailsHSTRegistrationNumber').removeClass('has-success').removeClass('has-error').addClass('has-required');
                    $('#HaulerDetails_CommercialLiabHstNumber').nextAll('.help-block:first').remove();
                    $('#HaulerDetails_CommercialLiabHstNumber').attr('readonly', false);
                }

                isValid = $("#fmHaulerDetails").valid();
                $('#flagHaulerDetails').attr('src', isValid ? ImageStatusArray[1] : ImageStatusArray[0]);
                HaulerDetailEnableReadOnly(true);
                $("#fmHaulerDetails").valid();

                //call this for any input change for green or grey flag update
                ApplicationCommon.SubmitButtonHandler();
            });
        });
    })
}

function HaulerDetailEnableReadOnly(flag) {
    //remove readonly methods in hauler details
    $('#HaulerDetails_BusinessStartDate').attr("readonly", flag);
    $('#HaulerDetails_CommercialLiabInsurerExpDate').attr("readonly", flag);
    $('#HaulerDetails_CvorExpiryDate').attr("readonly", flag);

    //Disable call Date Pickers if they are valid
    $('#fmHaulerDetails .date').each(function () {
        var isValid = $(this).children('input').attr('data-valid');

        if (isValid == 'True') {
            $(this).datetimepicker("remove");
        }
    });
}

function AddButtonHandlerForGreenCheck() {
    $('#btnAddContact').on('click', function () {
        PanelContactInfoGreenCheck();
        ApplicationCommon.SubmitButtonHandler();
    });

    $('#btnAddSort').on('click', function () {
        PanelSortYardGreenCheck();
        ApplicationCommon.SubmitButtonHandler();
    });
}

