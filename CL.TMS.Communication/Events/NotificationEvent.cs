﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Communication.Events
{
    public class NotificationEvent: PubSubEvent<NotificationPayload>
    {

    }

    public class NotificationPayload
    {
        public int NotificationId { get; set; }
        public string Receiver { get; set; }
    }
}
