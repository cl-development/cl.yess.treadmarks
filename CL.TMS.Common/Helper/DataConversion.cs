﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Helper
{
    public static class DataConversion
    {
        public static decimal ConvertLbToKg(double Lb)
        {
            return Convert.ToDecimal(Lb * 0.453600);
        }

        public static decimal ConvertKgToLb(double kg)
        {
            return Convert.ToDecimal(kg * 2.204586);
        }

        public static decimal ConvertTonToKg(double ton)
        {
            return Convert.ToDecimal(ton * 1000);
        }

        public static decimal ConvertKgToTon(double kg)
        {
            return Convert.ToDecimal(kg * 0.001);
        }

        public static decimal ConvertLbToTon(double lb)
        {
            return Convert.ToDecimal(lb * 0.0004536);
        }

        public static decimal ConvertTonToLb(double ton)
        {
            return Convert.ToDecimal(ton * 2204.586);
        }
    }
}
