﻿using CL.TMS.Common.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace CL.TMS.Common.Helper
{
    public static class FileUploadHandler
    {
        public static void SaveUploadFile(HttpPostedFileBase file, string uploadAbsolutePath, string fileName = "", bool validateFileType = false, string fileTypeRegex = "", int allowedFileSize = TreadMarksConstants.allowedFileSize, bool? createThumbnail = false)
        {
            if (file.ContentLength <= 0) { throw new ArgumentNullException("file"); }

            if (file.ContentLength > allowedFileSize)
            {
                throw new Exception(string.Format("The file '{0}' exceeded the allowed limit of 20 MB.", file.FileName)); //5120000
            }

            fileTypeRegex = string.IsNullOrEmpty(fileTypeRegex) ? @"^.*\.(bmp|png|tiff|jpg|jpeg|gif|doc|docx|xls|xlsx|pdf)$" : fileTypeRegex;

            if (validateFileType)
            {
                if (!Regex.IsMatch(Path.GetExtension(file.FileName).ToLower(), fileTypeRegex))
                {
                    throw new Exception(string.Format("Invalid file type. The file type '{0}' is not allowed.", Path.GetExtension(file.FileName).ToLower()));
                }
            }

            if (!Directory.Exists(uploadAbsolutePath))
            {
                Directory.CreateDirectory(uploadAbsolutePath);
            }

            string uniqueFileName = (string.IsNullOrEmpty(fileName) ? file.FileName : fileName);

            string fileFullPath = Path.Combine(uploadAbsolutePath, uniqueFileName);

            //SaveFile(file, fileFullPath);
            file.SaveAs(fileFullPath);
            if (createThumbnail ?? false)
            {
                using (Image img = Image.FromFile(fileFullPath))
                { 
                    Size thumbnailSize = GetThumbnailSize(img);
                    Image thumbnail = img.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);
                    // Save thumbnail
                    var thumbFullPathName = Path.Combine(uploadAbsolutePath, string.Format("{0}sm{1}", Path.GetFileNameWithoutExtension(uniqueFileName), Path.GetExtension(uniqueFileName)));
                    thumbnail.Save(thumbFullPathName);
                }
            }
        }

        public static FileInfo GetUploadedFileByFileFullPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(string.Format("File {0} does not exist.", path));
            }

            var fileInfo = new FileInfo(path);

            return fileInfo;
        }

        public static void DeleteUploadedFile(string path)
        {
            if (File.Exists(path))
            {
                var fileInfo = new FileInfo(path);
                fileInfo.Delete();
            }
            //throw new FileNotFoundException(string.Format("File {0} does not exist.", path));
        }

        public static void RemoveCurrentUploadDirectoryIfEmpty(string directoryPath)
        {
            if (Directory.Exists(directoryPath))
            {
                var di = new DirectoryInfo(directoryPath);
                FileInfo[] files = di.GetFiles("*.*");

                if (!files.Any()) Directory.Delete(directoryPath, true);
            }
        }

        public static void RemoveCurrentUploadDirectoryAndFiles(string directoryPath)
        {
            if (Directory.Exists(directoryPath)) Directory.Delete(directoryPath, true);
        }

        public static string ComposeUploadAbsolutePath(string fileUploadRepositoryPath, FileUploadSectionName section, string transactionId)
        {
            if (string.IsNullOrEmpty(fileUploadRepositoryPath)) { throw new ArgumentNullException("fileUploadRepositoryPath"); }
            if (string.IsNullOrEmpty(transactionId)) { throw new ArgumentNullException("transactionId"); }
            if (!Directory.Exists(fileUploadRepositoryPath)) { throw new DirectoryNotFoundException(string.Format("File upload repository '{0}' not found", fileUploadRepositoryPath)); }

            string relativeDirPath = Path.Combine(section.ToString(), transactionId);
            string absoluteDirPath = Path.Combine(fileUploadRepositoryPath, relativeDirPath);

            return absoluteDirPath;
        }

        public static string ComposeUploadFileRelativePath(FileUploadSectionName section, string transactionId, string fileName)
        {
            string relativeDirPath = Path.Combine(section.ToString(), transactionId, fileName);

            return relativeDirPath;
        }

        private static void SaveFile(HttpPostedFileBase file, string fileFullPath)
        {
            file.SaveAs(fileFullPath);
            //int fileLen = file.ContentLength;
            //byte[] fileData = new byte[fileLen];
            //file.InputStream.Read(fileData, 0, fileLen);
            //using (var fs = new FileStream(fileFullPath, FileMode.Create))
            //{
            //    fs.Write(fileData, 0, fileLen);
            //}
            //file.InputStream.Dispose();
        }

        public static string ComposeUploadAbsolutePath_Product(string fileUploadRepositoryPath, FileUploadSectionName section, string productId)
        {
            if (string.IsNullOrEmpty(fileUploadRepositoryPath)) { throw new ArgumentNullException("fileUploadRepositoryPath"); }
            if (string.IsNullOrEmpty(productId)) { throw new ArgumentNullException("productId"); }
            if (!Directory.Exists(fileUploadRepositoryPath)) { throw new DirectoryNotFoundException(string.Format("File upload repository '{0}' not found", fileUploadRepositoryPath)); }

            string relativeDirPath = Path.Combine(section.ToString(), productId);
            string absoluteDirPath = Path.Combine(fileUploadRepositoryPath, relativeDirPath);

            return absoluteDirPath;
        }

        public static string ComposeUploadFileRelativePath_Product(FileUploadSectionName section, string productId, string fileName)
        {
            string relativeDirPath = Path.Combine(section.ToString(), productId, fileName);

            return relativeDirPath;
        }

        private static Size GetThumbnailSize(Image original, int? maxPixels = null)
        {
            // Maximum size of any dimension.
            maxPixels = maxPixels ?? 100;
            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;
            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }
            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
        #region Company Logo
        public static string ComposeUploadAbsolutePath_Logo(string fileUploadRepositoryPath, FileUploadSectionName section)
        {
            if (string.IsNullOrEmpty(fileUploadRepositoryPath)) { throw new ArgumentNullException("fileUploadRepositoryPath"); }
            //if (string.IsNullOrEmpty(logoId)) { throw new ArgumentNullException("logoId"); }
            if (!Directory.Exists(fileUploadRepositoryPath)) { throw new DirectoryNotFoundException(string.Format("File upload repository '{0}' not found", fileUploadRepositoryPath)); }

            //string relativeDirPath = Path.Combine(section.ToString(), logoId);
            string relativeDirPath = section.ToString();
            string absoluteDirPath = Path.Combine(fileUploadRepositoryPath, relativeDirPath);

            return absoluteDirPath;
        }

        public static string ComposeUploadFileRelativePath_Logo(FileUploadSectionName section, string fileName)
        {
            string relativeDirPath = Path.Combine(section.ToString(), fileName);

            return relativeDirPath;
        }
        #endregion
        
    }
}
