﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Helper
{
    public static class CommonModelHelper
    {
        /// <summary>
        /// Convert registrant type int to registrant type name
        /// </summary>
        /// <param name="registrantTypeInt"></param>
        /// <returns></returns>
        public static string GetRegistrantTypeName(int registrantTypeInt)
        {
            switch (registrantTypeInt)
            {
                case 1:
                    return "Steward";
                case 2:
                    return "Collector";
                case 3:
                    return "Hauler";
                case 4:
                    return "Processor";
                case 5:
                    return "RPM";
                default:
                    return string.Empty;
            }
        }

    }
}
