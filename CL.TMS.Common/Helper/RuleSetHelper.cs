﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using CL.Framework.Common;
using CL.TMS.Common.Enum;

namespace CL.TMS.Common.Helper
{
    /// <summary>
    /// A helper class to get rule set
    /// </summary>
    public static class RuleSetHelper
    {
        /// <summary>
        /// Get rule set name
        /// </summary>
        /// <returns></returns>
        public static string[] GetRuleSetName()
        {
            var user = TMSContext.TMSPrincipal;
            if (user.Identity.IsAuthenticated)
            {
                var claim = user.FindFirst(TreadMarksConstants.StewardType);
                if (claim != null)
                {
                    var stewardType = claim.Value;
                    return new[] {TreadMarksConstants.DefaultRuleSet, stewardType};
                }
                return new[] { TreadMarksConstants.DefaultRuleSet };
            }
            //For unauthenticated user to return default rule set
            return new[] {TreadMarksConstants.DefaultRuleSet};
        }

        /// <summary>
        /// Get steward type
        /// </summary>
        /// <returns></returns>
        public static StewardType GetStewardType()
        {
            var stewardType = TreadMarksConstants.DefaultStewardType;
            var user = TMSContext.TMSPrincipal;       
            if (user.Identity.IsAuthenticated)
            {
                var claim = user.FindFirst(TreadMarksConstants.StewardType);
                if (claim != null)
                {
                    stewardType=claim.Value;
                }
            }
            return stewardType.ToEnum<StewardType>();
        }
    }
}
