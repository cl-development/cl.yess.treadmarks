﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common
{
    /// <summary>
    /// A place to store application constant
    /// </summary>
    public static class TreadMarksConstants
    {
        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        public const string DateFormat = "yyyy-MM-dd";
        public const string DateFormatJavascriptDateTimePicker = "yyyy-mm-dd";
        public const string DateMonthYearFormat = "MMM. yyyy";
        public const string DateMonthDayYearFormat = "MMM. dd, yyyy";

        public const string StewardType = "StewardType";
        public const string DefaultRuleSet = "Default";
        public const string DefaultStewardType = "OTS";
        public const int InviteExpirationDays = 7;
        public const string FirstTimePenalty = "First time penalty.";


        #region Zone String
        public const string N1 = "N1";
        public const string N2 = "N2";
        public const string N3 = "N3";
        public const string N4 = "N4";
        public const string SturgeonFalls = "Sturgeon Falls";
        public const string South = "South";
        public const string MooseCreek = "Moose Creek";
        public const string GTA = "GTA/Brantford";
        public const string WTC = "W/T/C";
        #endregion

        #region TransactionType String
        public const string TCR = "TCR";
        public const string DOT = "DOT";
        public const string STC = "STC";
        public const string UCR = "UCR";
        public const string DOR = "DOR";
        public const string HIT = "HIT";
        public const string PIT = "PIT";
        public const string PTR = "PTR";
        public const string RTR = "RTR";
        public const string SPS = "SPS";
        public const string SIR = "SIR";
        public const string SPSR = "SPSR";
        #endregion

        #region TransactionItem String
        public const string PLT = "PLT";
        public const string MT = "MT";
        public const string AGLS = "AGLS";
        public const string IND = "IND";
        public const string SOTR = "SOTR";
        public const string MOTR = "MOTR";
        public const string LOTR = "LOTR";
        public const string GOTR = "GOTR";
        #endregion

        #region TransactionPayment Eligibility String
        public const string TransactionPaymentEligi1 = "Non-registered collector";
        public const string TransactionPaymentEligi2 = "Charged a disposal fee";
        public const string TransactionPaymentEligi3 = "Pre-program tires";
        public const string TransactionPaymentEligi4 = "Other";
        #endregion

        #region Integer Values
        public const int Inbound = 1;
        public const int Outbound = 2;

        public const int Lb = 1;
        public const int Kg = 2;
        public const int Ton = 3;
        #endregion

        #region Vendor Type String //=ClaimType String
        public const string Steward = "Steward";        //1
        public const string Collector = "Collector";    //2
        public const string Hauler = "Hauler";          //3
        public const string Processor = "Processor";    //4
        public const string RPM = "RPM";                //5
        #endregion

        #region Item Type String
        public const string OnRoad = "OnRoad";
        public const string OffRoad = "OffRoad";
        #endregion

        #region Rate Payment Type String
        public const string NorthernPremium = "NorthernPremium";
        public const string DOTPremium = "DOTPremium";
        public const string ProcessorTIRates = "ProcessorTIRates";
        public const string ProcessorSPS = "ProcessorSPS";
        public const string ProcessorPITOutbound = "ProcessorPITOutbound";
        public const string RemittancePenalty = "RemittancePenalty";
        #endregion

        #region Material Type String
        public const string Trash = "Trash";
        public const string Steel = "Steel";
        public const string Fibre = "Fibre";
        public const string TireRims = "Tire Rims";
        public const string UsedTire = "Used Tire Sale";
        #endregion

        #region Disposition Reason String
        public const string ReuseResale = "Reuse/Resale";
        public const string Recycling = "Recycling";
        public const string Energy = "Energy";
        public const string Landfill = "Landfill";
        #endregion

        #region Banking Information
        public const int NumberToMask = 4;
        #endregion

        #region Security String

        public const string RolePermissions = "RolePermissions";
        public const string ClaimsWorkflow = "ClaimsWorkflow";
        public const string ApplicationsWorkflow = "ApplicationsWorkflow";

        public const string RoleRead = "Read";
        public const string RoleWrite = "Write";
        public const string RoleSubmit = "Submit";
        public const string RoleParticipantAdmin = "ParticipantAdmin";

        public const string StewardAccount = "Steward";
        public const string CollectorAccount = "Collector";
        public const string HaulerAccount = "Hauler";
        public const string ProcessorAccount = "Processor";
        public const string RPMAccount = "RPM";

        public const string NoneWorkflow = "None";
        public const string RepresentativeWorkflow = "Representative";
        public const string LeadWorkflow = "Lead";
        public const string SupervisorWorkflow = "Supervisor";
        public const string Approver1Workflow = "Approver 1";
        public const string Approver2Workflow = "Approver 2";
        public const string TSFClerkWorkflow = "TSF Clerk";

        public const string NoAccessPermission = "NoAccess";
        public const string ReadOnlyPermission = "ReadOnly";
        public const string EditSavePermission = "EditSave";
        public const string CustomPermission = "Custom";

        #endregion

        #region Supporting Document Types
        public const string SDPaperForm = "Paper Form";
        public const string SDScaleTicket = "Scale Ticket";
        public const string SDBillOfLading = "Bill of Lading";
        public const string SDInvoice = "Invoice";
        #endregion

        #region OTR Tire Type
        public const string OTR = "OTR";
        #endregion

        #region Attachment File Size
        public const int allowedFileSize = 20480000; //20MB
        #endregion

        #region Reporting settings
        public const string ReportEffectiveDateNewTireBegin = "04/01/2013";
        public const string ReportEffectiveDateOldTireBegin = "09/01/2009";
        public const string ReportEffectiveDateOldTireEnd = "2013-03-31";
        public const string ReportEffectiveStartDate = "2009-09-01";
        #endregion

        #region Retail Connection
        public const string UnderReviewStatus = "Under Review";
        public const string ActiveStatus = "Active";
        public const string InactiveStatus = "Inactive";
        public const string RejectedStatus = "Rejected";

        #endregion

        #region NotificationType
        public const int ClaimNotification = 1;
        #endregion

        #region  NotificationStatus
        public const string NotificationUnread = "Unread";
        public const string NotificationRead = "Read";
        public const string NotificationHide = "Hidden";
        #endregion

        #region ActivityType
        public const int ClaimActivity = 1;
        public const int TsfClaimActivity = 2;
        public const int RegistrationActivity = 3;
        public const int GPClaimActivity = 4;
        public const int AdminMenuActivity = 5;
        public const int VendorRegistrationActivity = 6;//for both vendor and customer
        public const int RegistrantActivity = 7;//for both vendor and customer, objectId will be RegNumber
        #endregion

        #region GP ActivityAreaType
        public const string GPParticipant = "GpParticipant";
        public const string GPHauler = "GpHauler";
        public const string GPProcessor = "GpProcessor";
        public const string GPCollector = "GpCollector";
        public const string GPRPM = "GpRPM";
        public const string GPSteward = "GpSteward";
        #endregion

        #region BusinessActivityType
        public const string Generator = "Generator";
        #endregion

        #region Admin ActivityAreaType
        public const string AdminActivityStaffUsers = "AdminStaffUsers";
        public const string AdminActivityRolesPermissions = "AdminRolesPermissions";
        public const string AdminActivityFeesIncentives = "AdminFeesIncentives";
        public const string AdminActivityRateGroupPickup = "AdminRateGroupPickup";
        public const string AdminActivityRateGroupDelivery = "AdminRateGroupDelivery";
        public const string AdminActivityWeights = "AdminWeights";
        public const string AdminActivityPickupGroups = "AdminPickupGroups";
        public const string AdminActivityDeliveryGroups = "AdminDeliveryGroups";
        public const string AdminActivityProductCompositions = "AdminProductCompositions";
        public const string AdminActivitySystemThresholdsAccounts = "AdminSystemThresholdsAccounts";
        public const string AdminActivitySystemThresholdsTransactions = "AdminSystemThresholdsTransactions";
        public const string AdminActivityApplicationSettings = "AdminApplicationSettings";
        public const string AdminActivityFinancialIntegration = "AdminFinancialIntegration";
        public const string AdminActivityEmailSettingsGeneral = "AdminEmailSettingsGeneral";
        public const string AdminActivityEmailSettingsContent = "AdminEmailSettingsContent";
        public const string AdminActivityCompanyBrandingInformation = "AdminCompanyBrandingInformation";
        public const string AdminActivityCompanyBrandingTermsConditions = "AdminCompanyBrandingTermsConditions";
        #endregion

        #region Screen Resource
        //format {Menu}_{Screen}_{Component} ={Name}
        public const string Claims = "Claims_Claims_Claims";
        public const string ClaimsClaimsAssignment = "Claims_Claims Assignment_Claims Assignment";//ResourceLevel 1
        public const string ClaimsClaimsAllClaims = "Claims_Claims All Claims_Claims All Claims";//ResourceLevel 1
        public const string ClaimsCollectorClaimSummary = "Claims_Collector Claim Summary_Collector Claim Summary";//ResourceLevel 1
        public const string ClaimsCollectorClaimSummaryStatus = "Claims_Collector Claim Summary_Status";
        public const string ClaimsCollectorClaimSummaryInbound = "Claims_Collector Claim Summary_Inbound";
        public const string ClaimsCollectorClaimSummaryOutbound = "Claims_Collector Claim Summary_Outbound";
        public const string ClaimsCollectorClaimSummaryAdjustments = "Claims_Collector Claim Summary_Adjustments";
        public const string ClaimsCollectorClaimSummaryReuseTires = "Claims_Collector Claim Summary_Reuse Tires";
        public const string ClaimsCollectorClaimSummaryTransactionAdjustments = "Claims_Collector Claim Summary_Transaction Adjustments";
        public const string ClaimsCollectorClaimSummaryPaymentsAndAdjustments = "Claims_Collector Claim Summary_Payments And Adjustments";
        public const string ClaimsCollectorClaimSummaryInternalAdjustments = "Claims_Collector Claim Summary_Internal Adjustments";
        public const string ClaimsCollectorClaimSummaryInternalNotes = "Claims_Collector Claim Summary_Internal Notes";
        public const string ClaimsCollectorClaimSummarySupportingDocuments = "Claims_Collector Claim Summary_Supporting Documents";
        public const string ClaimsCollectorClaimSummaryActivities = "Claims_Collector Claim Summary_Activities";
        public const string ClaimsHaulerClaimSummary = "Claims_Hauler Claim Summary_Hauler Claim Summary";//ResourceLevel 1
        public const string ClaimsHaulerClaimSummaryStatus = "Claims_Hauler Claim Summary_Status";
        public const string ClaimsHaulerClaimSummaryInbound = "Claims_Hauler Claim Summary_Inbound";
        public const string ClaimsHaulerClaimSummaryOutbound = "Claims_Hauler Claim Summary_Outbound";
        public const string ClaimsHaulerClaimSummaryTransactionAdjustments = "Claims_Hauler Claim Summary_Transaction Adjustments";
        public const string ClaimsHaulerClaimSummaryInventoryAndAdjustments = "Claims_Hauler Claim Summary_Inventory And Adjustments";
        public const string ClaimsHaulerClaimSummaryPaymentsAndAdjustments = "Claims_Hauler Claim Summary_Payments And Adjustments";
        public const string ClaimsHaulerClaimSummaryInternalAdjustments = "Claims_Hauler Claim Summary_Internal Adjustments";
        public const string ClaimsHaulerClaimSummaryInternalNotes = "Claims_Hauler Claim Summary_Internal Notes";
        public const string ClaimsHaulerClaimSummarySupportingDocuments = "Claims_Hauler Claim Summary_Supporting Documents";
        public const string ClaimsHaulerClaimSummaryActivities = "Claims_Hauler Claim Summary_Activities";
        public const string ClaimsProcessorClaimSummary = "Claims_Processor Claim Summary_Processor Claim Summary";//ResourceLevel 1
        public const string ClaimsProcessorClaimSummaryStatus = "Claims_Processor Claim Summary_Status";
        public const string ClaimsProcessorClaimSummaryInbound = "Claims_Processor Claim Summary_Inbound";
        public const string ClaimsProcessorClaimSummaryOutbound = "Claims_Processor Claim Summary_Outbound";
        public const string ClaimsProcessorClaimSummaryTransactionAdjustments = "Claims_Processor Claim Summary_Transaction Adjustments";
        public const string ClaimsProcessorClaimSummaryInventoryAndAdjustments = "Claims_Processor Claim Summary_Inventory And Adjustments";
        public const string ClaimsProcessorClaimSummaryPaymentsAndAdjustments = "Claims_Processor Claim Summary_Payments And Adjustments";
        public const string ClaimsProcessorClaimSummaryInternalAdjustments = "Claims_Processor Claim Summary_Internal Adjustments";
        public const string ClaimsProcessorClaimSummaryInternalNotes = "Claims_Processor Claim Summary_Internal Notes";
        public const string ClaimsProcessorClaimSummarySupportingDocuments = "Claims_Processor Claim Summary_Supporting Documents";
        public const string ClaimsProcessorClaimSummaryActivities = "Claims_Processor Claim Summary_Activities";
        public const string ClaimsRPMClaimSummary = "Claims_RPM Claim Summary_RPM Claim Summary";//ResourceLevel 1
        public const string ClaimsRPMClaimSummaryStatus = "Claims_RPM Claim Summary_Status";
        public const string ClaimsRPMClaimSummaryInbound = "Claims_RPM Claim Summary_Inbound";
        public const string ClaimsRPMClaimSummaryOutbound = "Claims_RPM Claim Summary_Outbound";
        public const string ClaimsRPMClaimSummaryTransactionAdjustments = "Claims_RPM Claim Summary_Transaction Adjustments";
        public const string ClaimsRPMClaimSummaryInventoryAndAdjustments = "Claims_RPM Claim Summary_Inventory And Adjustments";
        public const string ClaimsRPMClaimSummaryPaymentsAndAdjustments = "Claims_RPM Claim Summary_Payments And Adjustments";
        public const string ClaimsRPMClaimSummaryInternalAdjustments = "Claims_RPM Claim Summary_Internal Adjustments";
        public const string ClaimsRPMClaimSummaryInternalNotes = "Claims_RPM Claim Summary_Internal Notes";
        public const string ClaimsRPMClaimSummarySupportingDocuments = "Claims_RPM Claim Summary_Supporting Documents";
        public const string ClaimsRPMClaimSummaryActivities = "Claims_RPM Claim Summary_Activities";

        public const string Transactions = "Transactions_Transactions_Transactions";
        public const string TransactionsAllTransactions = "Transactions_All Transactions_All Transactions";//ResourceLevel 1

        public const string Remittance = "Remittance_Remittance_Remittance";
        public const string RemittanceRemittancesAssignment = "Remittance_Remittances Assignment_Remittances Assignment";//ResourceLevel 1
        public const string RemittanceTSFRemittances = "Remittance_TSF Remittances_TSF Remittances";//ResourceLevel 1

        public const string RemittanceTSFRemittance = "Remittance_TSF Remittance Summary_TSF Remittance";//ResourceLevel 1
        public const string RemittanceTSFRemittanceRemittancePeriod = "Remittance_TSF Remittance Summary_Remittance Period";
        public const string RemittanceTSFRemittanceStatus = "Remittance_TSF Remittance Summary_Status";
        public const string RemittanceTSFRemittanceTireCounts = "Remittance_TSF Remittance Summary_Tire Counts";
        public const string RemittanceTSFRemittanceCredit = "Remittance_TSF Remittance Summary_Credit";
        public const string RemittanceTSFRemittancePayments = "Remittance_TSF Remittance Summary_Payments";
        public const string RemittanceTSFRemittanceInternalAdjustments = "Remittance_TSF Remittance Summary_Internal Adjustments";
        public const string RemittanceTSFRemittanceInternalNotes = "Remittance_TSF Remittance Summary_Internal Notes";
        public const string RemittanceTSFRemittanceSupportingDocuments = "Remittance_TSF Remittance Summary_Supporting Documents";
        public const string RemittanceTSFRemittanceActivities = "Remittance_TSF Remittance Summary_Activities";

        public const string Applications = "Applications_Applications_Applications";
        public const string ApplicationsAllApplications = "Applications_All Applications_All Applications";//ResourceLevel 1
        public const string ApplicationsAllApplicationsAssignment = "Applications_All Applications_Assignment";
        public const string ApplicationsAllApplicationsRegistrants = "Applications_All Applications_Registrants";
        public const string ApplicationsAllApplicationsBankingActions = "Applications_All Applications_Banking Actions";
        public const string ApplicationsStewardApplication = "Applications_Steward Application_Steward Application"; //ResourceLevel 1
        public const string ApplicationsStewardApplicationActiveInactive = "Applications_Steward Application_Active/Inactive";
        public const string ApplicationsStewardApplicationRemitFrequency = "Applications_Steward Application_Remit Frequency";
        public const string ApplicationsStewardApplicationAuditToggle = "Applications_Steward Application_Audit Toggle";
        public const string ApplicationsStewardApplicationMOEToggle = "Applications_Steward Application_MOE Toggle";
        public const string ApplicationsStewardApplicationSubmissionInformation = "Applications_Steward Application_Submission Information";
        public const string ApplicationsStewardApplicationInternalNotes = "Applications_Steward Application_Internal Notes";
        public const string ApplicationsStewardApplicationBusinessLocation = "Applications_Steward Application_Business Location";
        public const string ApplicationsStewardApplicationContactInformation = "Applications_Steward Application_Contact Information";
        public const string ApplicationsStewardApplicationTireDetails = "Applications_Steward Application_Tire Details";
        public const string ApplicationsStewardApplicationStewardDetails = "Applications_Steward Application_Steward Details";
        public const string ApplicationsStewardApplicationSupportingDocuments = "Applications_Steward Application_Supporting Documents";
        public const string ApplicationsStewardApplicationTermsAndConditions = "Applications_Steward Application_Terms And Conditions";
        public const string ApplicationsStewardApplicationResendWelcomeLetter = "Applications_Steward Application_Re-send Welcome Letter";
        public const string ApplicationsStewardApplicationActivities = "Applications_Steward Application_Activities";
        public const string ApplicationsCollectorApplication = "Applications_Collector Application_Collector Application"; //ResourceLevel 1
        public const string ApplicationsCollectorApplicationActiveInactive = "Applications_Collector Application_Active/Inactive";
        public const string ApplicationsCollectorApplicationGeneratorToggle = "Applications_Collector Application_Generator Toggle";
        public const string ApplicationsCollectorApplicationSubCollectorToggle = "Applications_Collector Application_Sub-Collector Toggle";
        public const string ApplicationsCollectorApplicationSubmissionInformation = "Applications_Collector Application_Submission Information";
        public const string ApplicationsCollectorApplicationInternalNotes = "Applications_Collector Application_Internal Notes";
        public const string ApplicationsCollectorApplicationBusinessLocation = "Applications_Collector Application_Business Location";
        public const string ApplicationsCollectorApplicationContactInformation = "Applications_Collector Application_Contact Information";
        public const string ApplicationsCollectorApplicationTireDetails = "Applications_Collector Application_Tire Details";
        public const string ApplicationsCollectorApplicationCollectorDetails = "Applications_Collector Application_Collector Details";
        public const string ApplicationsCollectorApplicationBankingInformation = "Applications_Collector Application_Banking Information";
        public const string ApplicationsCollectorApplicationSupportingDocuments = "Applications_Collector Application_Supporting Documents";
        public const string ApplicationsCollectorApplicationTermsAndConditions = "Applications_Collector Application_Terms And Conditions";
        public const string ApplicationsCollectorApplicationResendWelcomeLetter = "Applications_Collector Application_Re-send Welcome Letter";
        public const string ApplicationsCollectorApplicationActivities = "Applications_Collector Application_Activities";
        public const string ApplicationsHaulerApplication = "Applications_Hauler Application_Hauler Application"; //ResourceLevel 1
        public const string ApplicationsHaulerApplicationActiveInactive = "Applications_Hauler Application_Active/Inactive";
        public const string ApplicationsHaulerApplicationSubmissionInformation = "Applications_Hauler Application_Submission Information";
        public const string ApplicationsHaulerApplicationInternalNotes = "Applications_Hauler Application_Internal Notes";
        public const string ApplicationsHaulerApplicationBusinessLocation = "Applications_Hauler Application_Business Location";
        public const string ApplicationsHaulerApplicationContactInformation = "Applications_Hauler Application_Contact Information";
        public const string ApplicationsHaulerApplicationSortYardDetails = "Applications_Hauler Application_Sort Yard Details";
        public const string ApplicationsHaulerApplicationTireDetails = "Applications_Hauler Application_Tire Details";
        public const string ApplicationsHaulerApplicationHaulerDetails = "Applications_Hauler Application_Hauler Details";
        public const string ApplicationsHaulerApplicationBankingInformation = "Applications_Hauler Application_Banking Information";
        public const string ApplicationsHaulerApplicationSupportingDocuments = "Applications_Hauler Application_Supporting Documents";
        public const string ApplicationsHaulerApplicationTermsAndConditions = "Applications_Hauler Application_Terms And Conditions";
        public const string ApplicationsHaulerApplicationResendWelcomeLetter = "Applications_Hauler Application_Re-send Welcome Letter";
        public const string ApplicationsHaulerApplicationActivities = "Applications_Hauler Application_Activities";
        public const string ApplicationsProcessorApplication = "Applications_Processor Application_Processor Application"; //ResourceLevel 1
        public const string ApplicationsProcessorApplicationActiveInactive = "Applications_Processor Application_Active/Inactive";
        public const string ApplicationsProcessorApplicationSubmissionInformation = "Applications_Processor Application_Submission Information";
        public const string ApplicationsProcessorApplicationInternalNotes = "Applications_Processor Application_Internal Notes";
        public const string ApplicationsProcessorApplicationBusinessLocation = "Applications_Processor Application_Business Location";
        public const string ApplicationsProcessorApplicationContactInformation = "Applications_Processor Application_Contact Information";
        public const string ApplicationsProcessorApplicationProcessingLocations = "Applications_Processor Application_Processing Locations";
        public const string ApplicationsProcessorApplicationTiresAndProcessingDetails = "Applications_Processor Application_Tires And Processing Details";
        public const string ApplicationsProcessorApplicationProcessorDetails = "Applications_Processor Application_Processor Details";
        public const string ApplicationsProcessorApplicationBankingInformation = "Applications_Processor Application_Banking Information";
        public const string ApplicationsProcessorApplicationSupportingDocuments = "Applications_Processor Application_Supporting Documents";
        public const string ApplicationsProcessorApplicationTermsAndConditions = "Applications_Processor Application_Terms And Conditions";
        public const string ApplicationsProcessorApplicationResendWelcomeLetter = "Applications_Processor Application_Re-send Welcome Letter";
        public const string ApplicationsProcessorApplicationActivities = "Applications_Processor Application_Activities";
        public const string ApplicationsRPMApplication = "Applications_RPM Application_RPM Application";                          //ResourceLevel 1
        public const string ApplicationsRPMApplicationActiveInactive = "Applications_RPM Application_Active/Inactive";
        public const string ApplicationsRPMApplicationSubmissionInformation = "Applications_RPM Application_Submission Information";
        public const string ApplicationsRPMApplicationInternalNotes = "Applications_RPM Application_Internal Notes";
        public const string ApplicationsRPMApplicationBusinessLocation = "Applications_RPM Application_Business Location";
        public const string ApplicationsRPMApplicationContactInformation = "Applications_RPM Application_Contact Information";
        public const string ApplicationsRPMApplicationManufacturingLocation = "Applications_RPM Application_Manufacturing Location";
        public const string ApplicationsRPMApplicationManufacturingDetails = "Applications_RPM Application_Manufacturing Details";
        public const string ApplicationsRPMApplicationRPMDetails = "Applications_RPM Application_RPM Details";
        public const string ApplicationsRPMApplicationBankingInformation = "Applications_RPM Application_Banking Information";
        public const string ApplicationsRPMApplicationSupportingDocuments = "Applications_RPM Application_Supporting Documents";
        public const string ApplicationsRPMApplicationTermsAndConditions = "Applications_RPM Application_Terms And Conditions";
        public const string ApplicationsRPMApplicationResendWelcomeLetter = "Applications_RPM Application_Re-send Welcome Letter";
        public const string ApplicationsRPMApplicationActivities = "Applications_RPM Application_Activities";

        public const string Users = "Users_Users_Users";
        public const string UsersRegistrantUsers = "Users_Registrant Users_Registrant Users";//ResourceLevel 1

        public const string iPadsQRCodes = "QR Codes_QR Codes_QR Codes";
        public const string iPadsQRCodesAlliPadsQRCodes = "QR Codes_All QR Codes_All QR Codes";//ResourceLevel 1

        public const string RetailConnection = "Retail Connection_Retail Connection_Retail Connection";
        public const string RetailConnectionAllRetailConnection = "Retail Connection_All Retail Connection_All Retail Connection";//ResourceLevel 1
        public const string RetailConnectionAllRetailConnectionAllRetailers = "Retail Connection_All Retail Connection_All Retailers";
        public const string RetailConnectionAllRetailConnectionAllProducts = "Retail Connection_All Retail Connection_All Products";
        public const string RetailConnectionAllRetailConnectionProductCategories = "Retail Connection_All Retail Connection_Product Categories";

        public const string Reports = "Reports_Reports_Reports";
        public const string ReportsAllReports = "Reports_All Reports_All Reports"; //ResourceLevel 1
        public const string ReportsAllReportsSteward = "Reports_All Reports_Steward";
        public const string ReportsAllReportsCollector = "Reports_All Reports_Collector";
        public const string ReportsAllReportsHauler = "Reports_All Reports_Hauler";
        public const string ReportsAllReportsProcessor = "Reports_All Reports_Processor";
        public const string ReportsAllReportsRPM = "Reports_All Reports_RPM";
        public const string ReportsAllReportsRegistrant = "Reports_All Reports_Registrant";

        public const string Dashboard = "Dashboard_Dashboard_Dashboard";
        public const string DashboardStaffDashboard = "Dashboard_Dashboard_StaffDashboard"; //resource level1
        public const string DashboardAnnouncement = "Dashboard_Dashboard_Announcements";
        public const string DashboardMetricTSFDollar = "Dashboard_Dashboard_Year to Date TSF Dollar";
        public const string DashboardMetricTSFUnits = "Dashboard_Dashboard_Year to Date TSF Units";
        public const string DashboardMetricTSFStatus = "Dashboard_Dashboard_TSF Status Overview";
        public const string DashboardMetricPaymentMethods = "Dashboard_Dashboard_Steward Payment Methods";
        public const string DashboardMetricTopTenStewards = "Dashboard_Dashboard_Top Ten Stewards";
        public const string DashboardSTC = "Dashboard_Dashboard_STC Events";
        public const string DashboardTCR = "Dashboard_Dashboard_Request for Service";

        public const string FinancialIntegration = "Financial Integration_Financial Integration_Financial Integration";
        public const string FinancialIntegrationGPFinancialIntegration = "Financial Integration_Financial Integration_Staff Financial Integration";//ResourceLevel 1
        public const string FinancialIntegrationGPFinancialIntegrationGPParticipantManager = "Financial Integration_Financial Integration_Participant Manager";
        public const string FinancialIntegrationGPFinancialIntegrationGPStewardManager = "Financial Integration_Financial Integration_Steward Manager";
        public const string FinancialIntegrationGPFinancialIntegrationGPCollectorManager = "Financial Integration_Financial Integration_Collector Manager";
        public const string FinancialIntegrationGPFinancialIntegrationGPHaulerManager = "Financial Integration_Financial Integration_Hauler Manager";
        public const string FinancialIntegrationGPFinancialIntegrationGPProcessorManager = "Financial Integration_Financial Integration_Processor Manager";
        public const string FinancialIntegrationGPFinancialIntegrationGPRPMManager = "Financial Integration_Financial Integration_RPM Manager";



        public const string AdminAccess = "Admin Access_Admin Access_Admin Access";

        public const string AdminUsers = "Admin/Users_Admin/Users_Admin/Users";
        public const string AdminUsersStaffUsers = "Admin/Users_Staff Users_Staff Users";//ResourceLevel 1
        public const string AdminUsersAddEditNewUser = "Admin/Users_Add/Edit New User_Add/Edit New User";//ResourceLevel 1
        public const string AdminUsersAddEditNewUserRoles = "Admin/Users_Add/Edit New User_Roles";
        public const string AdminUsersAddEditNewUserRoutingAndWorkflowCLAIMS = "Admin/Users_Add/Edit New User_Routing And Workflow - CLAIMS";
        public const string AdminUsersAddEditNewUserRoutingAndWorkflowREMITTANCES = "Admin/Users_Add/Edit New User_Routing And Workflow - REMITTANCES";
        public const string AdminUsersAddEditNewUserRoutingAndWorkflowAPPLICATIONS = "Admin/Users_Add/Edit New User_Routing And Workflow - APPLICATIONS";

        public const string AdminRolesAndPermissions = "Admin/Roles And Permissions_Admin/Roles And Permissions_Admin/Roles And Permissions";

        public const string AdminReports = "Admin/Reports_Admin/Reports_Admin/Reports";

        public const string AdminFeesAndIncentives = "Admin/Fees and Incentives_Admin/Fees and Incentives_Admin/Fees and Incentives";
        public const string AdminFeesAndIncentivesFeesAndIncentives = "Admin/Fees and Incentives_Fees and Incentives/Fees and Incentives";//ResourceLevel 1
        public const string AdminFeesAndIncentivesFeesAndIncentivesTireStewardshipFees = "Admin/Fees and Incentives_Fees and Incentives_Tire Stewardship Fees";
        public const string AdminFeesAndIncentivesFeesAndIncentivesStewardRemittancePenalty = "Admin/Fees and Incentives_Fees and Incentives_Remittance Penalty";
        public const string AdminFeesAndIncentivesFeesAndIncentivesCollectionAllowance = "Admin/Fees and Incentives_Fees and Incentives_Collection Allowances";
        public const string AdminFeesAndIncentivesFeesAndIncentivesTransportationIncentive = "Admin/Fees and Incentives_Fees and Incentives_Transportation Incentives";
        public const string AdminFeesAndIncentivesFeesAndIncentivesProcessingIncentive = "Admin/Fees and Incentives_Fees and Incentives_Processing Incentives";
        public const string AdminFeesAndIncentivesFeesAndIncentivesManufacturingIncentive = "Admin/Fees and Incentives_Fees and Incentives_Manufacturing Incentives";
        public const string AdminFeesAndIncentivesFeesAndIncentivesActivities = "Admin/Fees and Incentives_Fees and Incentives_Activities";
        //public const string AdminRatesAndWeightsRatesAndWeightsEstimatedWeights = "Admin/Rates And Weights_Rates And Weights_Estimated Weights";

        public const string AdminRateGroups = "Admin/Rate Groups_Admin/Rate Groups_Admin/Rate Groups";
        //public const string AdminRateGroupsPickupGroups = "Admin/Rate Groups_Pickup Groups_Pickup Groups";
        //public const string AdminRateGroupsDeliveryGroups = "Admin/Rate Groups_Delivery Groups_Delivery Groups";

        public const string AdminCompanyBranding = "Admin/Company Branding_Admin/Company Branding_Admin/Company Branding";
        public const string AdminCompanyBrandingCompanyInformation = "Admin/Company Branding_Company Information_Company Information";//ResourceLevel 1
        public const string AdminCompanyBrandingTermsAndConditions = "Admin/Company Branding_Terms & Conditions_Terms and Conditions";//ResourceLevel 1

        public const string AdminApplicationSettings = "Admin/Application Settings_Admin/Application Settings_Admin/Application Settings";

        public const string AdminFinancialIntegration = "Admin/Financial Integration_Admin/Financial Integration_Admin/Financial Integration";

        public const string AdminSystemThresholds = "Admin/System Thresholds_Admin/System Thresholds_Admin/System Thresholds";

        public const string AdminEmailSettings = "Admin/Email Settings_Admin/Email Settings_Admin/Email Settings";

        //28xxxxx
        public const string AdminAnnouncements = "Admin/Announcements_Admin/Announcements_Admin/Announcements";
        //29xxxxx
        public const string AdminProductCompositions = "Admin/Product Compositions_Admin/Product Compositions_Admin/Product Compositions";

        #endregion

        #region Rate category
        public const string TransportationIncentiveRates = "Transportation Incentives Rates";
        public const string RemittancePenaltyRates = "Remittance Penalty Rates";
        public const string CollectionAllowanceRates = "Collection Allowances Rates";
        public const string ProcessingIncentiveRates = "Processing Incentives Rates";
        public const string ManufacturingIncentiveRates = "Manufacturing Incentives Rates";
        public const string TireStewardshipFeeRates = "Tire Stewardship Fees Rates";
        public const string EstimatedWeightsRates = "Estimated Weights Rates";
        #endregion

        #region RateGroup Category
        public const string PickupGroup = "PickupGroup";        //rate group: 1 (inBound transactions)
        public const string DeliveryGroup = "DeliveryGroup";    //rate group: 0 (outBound transactions)
        public const string CollectorGroup = "CollectorGroup";  //vendor group: 2
        public const string ProcessorGroup = "ProcessorGroup";  //vendor group: 4
        #endregion

        #region Weight category
        public const string SupplyEstimatedWeights = "Supply Estimated Weights";
        public const string RecoveryEstimatedWeights = "Recovery Estimated Weights";
        #endregion

        #region Application Type //=ClaimType String
        public const string StewardApplication = "Steward Application";        //1
        public const string CollectorApplication = "Collector Application";    //2
        public const string HaulerApplication = "Hauler Application";          //3
        public const string ProcessorApplication = "Processor Application";    //4
        public const string RPMApplication = "RPM Application";                //5
        #endregion

        #region Admin Financial Integration category
        public const string AdminFISteward = "Stewards - Tire Stewardship Fees";       //1
        public const string AdminFICollector = "Collectors - Collection Allowances";   //2
        public const string AdminFIHauler = "Haulers - Transportation Premiums";       //3
        public const string AdminFIProcessor = "Processors - Processing Incentives";   //4
        public const string AdminFIRPM = "RPMs - Manufacturing Incentives";	           //5
        #endregion

        #region
        //public const string General = "General";
        //public const string Processor = "Processor";
        //public const string RPM = "RPM";
        //public const string Steward = "Steward";
        //public const string TransactionItem = "TransactionItem";
        //public const string ProcessorPITItem = "ProcessorPITItem";
        //public const string ProcessorSPSItem = "ProcessorSPSItem";
        //public const string RPMSPSItem = "RPMSPSItem";
        #endregion
    }
}
