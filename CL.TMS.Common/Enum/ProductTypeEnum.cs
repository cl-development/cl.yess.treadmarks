﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    //ProductTypeEnum for New TM
    //Need to hard code coz none of the field is unique, shortname is repeating + names, desc are not same in central and New TM
    public enum ProductTypeEnum
    {
        TDP5FT = 78,
        TDP5NT = 79,
        TD6NP = 80,
        TD6FP = 81,
        TDP7NT = 82,
        TDP7FT = 83,
        Transfer_Fiber_Rubber = 84,
        TDP1 = 73,
        TDP2 = 74,
        TDP3 = 75,
        TDP4FF = 76,
        TDP4FF_NoPI = 77,
    }
}

