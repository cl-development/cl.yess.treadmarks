﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum LegacyPhotoTypeEnum
    {
        [Description("Photo")]
        Photo = 1,
        [Description("Signature")]
        Signature = 2,
        [Description("Scale Ticket")]
        ScaleTicket = 3,
        [Description("Document")]
        Document = 4,        
    }
}
