﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum SubmitBusinessRuleType
    {
        [Description("Error")]
        Error = 1,
        [Description("Warning")]
        Warning,
        [Description("Success")]
        Success
    }
}
