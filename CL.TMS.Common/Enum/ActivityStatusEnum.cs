﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum ActivityStatusEnum
    {
        Active = 1,
        InActive = 2
    }

    public enum SRemitsPerYear
    {
        SRemitsPerYear12x = 1,
        SRemitsPerYear2x = 0


    }
    public enum SAudit
    {
        Off = 0,
        On = 1
    }
    public enum SMOE
    {
        Off = 0,
        On = 1
    }
}
