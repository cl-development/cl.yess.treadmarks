﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    /// <summary>
    /// Steward type
    /// </summary>
    public enum StewardType
    {
        OTS,
        OtherSteward
    }
}
