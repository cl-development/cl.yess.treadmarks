﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    /// <summary>
    /// Steward Sub type
    /// </summary>
    public enum StewardSubType
    {
        [Description("Original Equipment Manufacturer (OEM)")]
        OEM =1,
        [Description("Tire Manufacturer/Brand Owner")]
        Owner,
        [Description(" First Importer")]
        FirstImporter
    }
}
