﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum AddressTypeEnum
    {
        Business=1,
        Mail = 2,
        Contact=3,        
        Sortyard=4
    }
}
