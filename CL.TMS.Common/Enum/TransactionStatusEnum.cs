﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    //TransactionStatus for central repo
    public enum TransactionStatusEnum
    {
        [Description("ISSUES")]
        ISSUES = 1,

        [Description("INCOMPLETE")]
        INCOMPLETE = 2,

        [Description("COMPLETE")]
        COMPLETE = 3,

        [Description("DELETED")]
        DELETED = 4,
    }
}

