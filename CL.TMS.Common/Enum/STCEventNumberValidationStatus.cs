﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum STCEventNumberValidationStatus
    {
        Pass,
        NotExisting,
        IsUsed,
        NotInDateRange,
    }
}
