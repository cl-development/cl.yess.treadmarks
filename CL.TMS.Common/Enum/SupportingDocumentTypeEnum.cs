﻿using System;
using System.ComponentModel;

namespace CL.TMS.Common.Enum
{
    public enum SupportingDocumentTypeEnum
    {
        /// <summary>
        /// 1. Master Business Licence
        /// </summary>
        MBL = 1,
        /// <summary>
        /// 2. Commercial Liability Insurance
        /// </summary>
        CLI = 2,
        /// <summary>
        /// 3. Certificate of Approval for Processing
        /// </summary>
        CFP = 3,
        /// <summary>
        /// 4. HST Certificate
        /// </summary>
        HST = 4,
        /// <summary>
        /// 5. WSIB Certificate
        /// </summary>
        WSIB = 5,
        /// <summary>
        /// 6. Void Cheque
        /// </summary>
        CHQ = 6,
        /// <summary>
        /// 7. CVOR Abstract Level II
        /// </summary>
        CVOR = 7,
        /// <summary>
        /// 8. Processor Relationship Lette
        /// </summary>
        PRL = 8,
        /// <summary>
        /// 9. EASR/ECA (OTSTM2-486)
        /// </summary>
        EASR_ECA = 9
    }
}
