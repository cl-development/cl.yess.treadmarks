﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
    public enum RateCategoryEnum
    {
        [Description("Transportation Incentive Rates")]
        TransportationIncentiveRates = 1,
        [Description("Transportation Incentive Rates")]
        CollectorAllowanceRates = 2,
        [Description("Tire Stewardship Fee Rates")]
        TireStewardshipFeeRates = 3,
        [Description("Processing Incentives Rates")]
        ProcessingIncentivesRates = 4,
        [Description("Transportation Incentive Rates")]
        ManufacturingIncentivesRates = 5,
    }
}
