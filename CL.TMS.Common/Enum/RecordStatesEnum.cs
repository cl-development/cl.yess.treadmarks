﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Enum
{
   
    public enum RecordStates : byte
    {
        [Description("I")]
        Initial = 1,

        [Description("D")]
        Draft = 2,

        [Description("F")]
        Final = 3
    
    }
}
