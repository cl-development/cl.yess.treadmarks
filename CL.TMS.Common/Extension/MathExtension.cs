﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.TMS.Common.Extension
{
    public static class MathExtension
    {
        public static decimal Round(decimal d, int decimals, DateTime effectiveDate, DateTime entityDate)
        {
            if (entityDate.Date<effectiveDate.Date)
            {
                return Math.Round(d, decimals);
            }
            else
            {
                return Math.Round(d, decimals, MidpointRounding.AwayFromZero);
            }
        }
    }
}
