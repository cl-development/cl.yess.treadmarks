﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using CL.TMS.DataContracts.Communication;

namespace CL.TMS.DataSync.CommunicationData
{
    /// <summary>
    /// AppUserModel model class.
    /// Used for IN and OUT self-validated, data structure parameter by the web services.
    /// </summary>
    [DataContract]
    public class AppUserModel : IAppUserModel
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int VendorID { get; set; }
        [DataMember]
        public int VendorAppUserID { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string EMail { get; set; }
        [DataMember]
        public int AccessTypeID { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
        [DataMember]
        public Nullable<DateTime> ModifiedOn { get; set; }
        [DataMember]
        public string Metadata { get; set; }
        [DataMember]
        public Nullable<DateTime> LastAccessOn { get; set; }
        [DataMember]
        public Nullable<DateTime> SyncOn { get; set; }
        [DataMember]
        public bool Active { get; set; }
       
    }
}
