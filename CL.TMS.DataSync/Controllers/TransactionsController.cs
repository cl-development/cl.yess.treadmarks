﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CL.TMS.ServiceContracts.SystemServices;
using CL.TMS.DataContracts.DomainEntities;
using CL.TMS.DataSyncBLL;
using CL.TMS.DataContracts.Communication;
using CL.TMS.DataSync.ResponseMessages;
using CL.TMS.Common.Enum;
using CL.TMS.ServiceContracts.RegistrantServices;
using CL.TMS.DataSync.CommunicationData;
using CL.TMS.ExceptionHandling.Exceptions;

namespace CL.TMS.DataSync.Controllers
{
    public class TransactionsController : ApiController
    {
        private IAppService appService;
        private ITransactionService transactionService;

        public TransactionsController(IAppService appService, ITransactionService transactionService)
        {
            this.appService = appService;
            this.transactionService = transactionService;
        }

        public class TransactionModel
        {
            public Guid Id { set; get; }            
        }

        [HttpPost]
        //[Authorize]
        public IHttpActionResult Test(TestModel input)
        {
            return Json(string.Format("this is a test for {0}", input.Name));
        }
        public IHttpActionResult Get()
        {
            var domainName = appService.GetSettingValue("Settings.DomainName");
            return Json("Transaction Get Operation, and domain name is " + domainName);           
        }

        [HttpPost]
        //Commenting Token as remote host is not working due to this
        //[Authorize]
        [Route("api/v1/Transactions/SaveTransaction")]
        public IHttpActionResult SaveTransaction(TransactionModel transaction)
        {
            ResponseData resp = new ResponseData();
            try
            {
                transactionService.ImportMobileTransaction(transaction.Id);
                resp.Code = (int)WebServiceResponseStatus.OK;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message + " Stacktrace: " + ex.StackTrace + "Exception: " + ex.ToString();
                resp.Code = (int)WebServiceResponseStatus.ERROR;
            }
            return Json(resp);
        }
        
    }

    public class TestModel
    {
        public string Name { get; set; }
    }
}
