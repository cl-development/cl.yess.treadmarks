using Microsoft.Practices.Unity;
using System.Web.Http;
using CL.TMS.DependencyBuilder;
using Unity.WebApi;

namespace CL.TMS.DataSync
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            ApplicationDependencyBuilder.BuildDependenciesForCommunicator(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}