﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging.Implementation
{
    public class LogDbContext:DbContext
    {
        public DbSet<SystemLog> SystemLogs { get; set; }
        public DbSet<SqlAuditLog> SqlAuditLogs { get; set; }
        public LogDbContext():base("TMSDB")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
