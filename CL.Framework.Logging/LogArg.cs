﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    /// <summary>
    /// Log Argument
    /// </summary>
    public class LogArg
    {
        #region Properties
        public string Name { get; set; }
        public object Value { get; set; }
        #endregion 
    }
}
