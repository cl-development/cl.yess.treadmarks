﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    /// <summary>
    /// Log Category
    /// </summary>
    public enum LogCategory
    {
        Information,
        Warning,
        Error,
        Exception,
        Debug, 
        DbDebug
    }
}
