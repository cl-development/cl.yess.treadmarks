﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    /// <summary>
    /// Providing tracing capability to method that you want
    /// </summary>
    public class MethodTracer : IDisposable
    {
        #region Fields
        private bool disposed;
        private string className;
        private string methodName;
        private bool isTracingOn;
        private long startTicks;
        #endregion 

        #region Constructors
        public MethodTracer()
        {
            isTracingOn = false;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="methodArgs"></param>
        public MethodTracer(string className, string methodName, List<LogArg> methodArgs = null)
        {
            this.className = className;
            this.methodName = methodName;
            this.methodArgs = methodArgs ?? new List<LogArg>();
            this.startTicks = DateTime.Now.Ticks;
            isTracingOn = true;
        }

        #endregion 

        #region Properties
        private List<LogArg> methodArgs;
        public List<LogArg> MethodArgs
        {
            get
            {
                return methodArgs;
            }
        }
        #endregion 

        #region Public Methods
        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                CompleteDispose();
            }
        }
        #endregion 

        #region Private Methods
        private void CompleteDispose()
        {
            if (isTracingOn)
            {
                long elapsedTime = (DateTime.Now.Ticks - startTicks) / 10000;
                LogManager.LogMethodTracing(className, methodName, elapsedTime, methodArgs);
            }
        }
        #endregion 
    }
}
