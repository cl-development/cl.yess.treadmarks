﻿using CL.Framework.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CL.Framework.Logging
{
    /// <summary>
    /// A Log Facade
    /// </summary>
    public static class LogManager
    {
        #region Fields
        private static bool isTracingEnabled = false;
        #endregion 

        #region Constructors
        /// <summary>
        /// A static constractor to read tracing flag
        /// </summary>
        static LogManager()
        {
            //Check tracing flag
            var tracingEnabled = ConfigurationManager.AppSettings["TracingEnabled"];
            if (!string.IsNullOrWhiteSpace(tracingEnabled))
            {
                isTracingEnabled = Convert.ToBoolean(tracingEnabled);
            }
        }
        #endregion

        #region Properties
        public static bool IsTracingEnabled
        {
            get
            {
                return isTracingEnabled;
            }
        }
        #endregion

        private static string GetCurrentUserName()
        {
            if (HttpContext.Current != null)
            {
                var user = HttpContext.Current.User as ClaimsPrincipal;
                var claim = user.FindFirst(ClaimTypes.Name);
                if (claim != null)
                {
                    return claim.Value;
                }
                else
                {
                    return "System";
                }
            }
            else
            {
                return "System";
            }
        }

        private static void WriteDbAuditLog(SqlAuditLog sqlAuditLog)
        {
            Task.Factory.StartNew(() => LoggerFactory.GetLogger().WriteSqlAuditEntry(sqlAuditLog));
        }

        /// <summary>
        /// Using parallel programming to fire a task for perfoming log operation asynchronously
        /// </summary>
        /// <param name="logEntry"></param>
        private static void WriteLogEntry(SystemLog systemLog)
        {
            Task.Factory.StartNew(() => LoggerFactory.GetLogger().WriteLogEntry(systemLog));
        }

        #region Public Methods

        public static void LogDBAuditLog(string sqlStatement, List<LogArg> lstArg = null)
        {
            var sqlAuditLog = new SqlAuditLog();
            sqlAuditLog.SqlStatement = sqlStatement;
            var sqlParameters = new StringBuilder();
            if (lstArg != null)
            {
                lstArg.ForEach(m => sqlParameters.AppendLine(string.Format("Name:{0}-Value:{1}", m.Name, CommonFun.DumpObjectToJsonStringIgnoreException(m.Value))));
            }
            sqlAuditLog.SqlParameters = sqlParameters.ToString();
            sqlAuditLog.CreatedBy = GetCurrentUserName();
            sqlAuditLog.CreatedDate = DateTime.Now;
            WriteDbAuditLog(sqlAuditLog);
        }

        /// <summary>
        /// Add an information log
        /// </summary>
        /// <param name="message"></param>
        public static void LogInfo(string message)
        {
            var systemLog = new SystemLog();
            systemLog.Category = LogCategory.Information.ToString();
            systemLog.Message = message;
            systemLog.CreatedDate = DateTime.Now;
            WriteLogEntry(systemLog);
        }

        /// <summary>
        /// Add a warning log
        /// </summary>
        /// <param name="message"></param>
        public static void LogWarning(string message)
        {
            var systemLog = new SystemLog();
            systemLog.Category = LogCategory.Warning.ToString();
            systemLog.Message = message;
            systemLog.CreatedDate = DateTime.Now;
            WriteLogEntry(systemLog);
        }
        /// <summary>
        /// Add an erro log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="lstArg"></param>
        public static void LogError(string message, List<LogArg> lstArg = null)
        {
            var systemLog = new SystemLog();
            systemLog.Category = LogCategory.Error.ToString();
            systemLog.CreatedDate = DateTime.Now;
            var messages = new StringBuilder();
            messages.AppendLine(message);
            if (lstArg != null)
            {
                lstArg.ForEach(m => messages.AppendLine(string.Format("Name:{0}-Value:{1}", m.Name, CommonFun.DumpObjectToJsonStringIgnoreException(m.Value))));
            }
            systemLog.Message = messages.ToString();
            WriteLogEntry(systemLog);
        }

        /// <summary>
        /// Log an exception with a specific message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public static void LogExceptionWithMessage(string message, Exception ex)
        {
            var systemLog = new SystemLog();
            systemLog.Category = LogCategory.Exception.ToString();
            systemLog.CreatedDate = DateTime.Now;
            systemLog.Message = string.Format("{0}, Exception:{1}", message, CommonFun.DumpExceptionToJsonString(ex));
            WriteLogEntry(systemLog);
        }
        /// <summary>
        /// Log an exception 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="lstArg"></param>
        public static void LogException(Exception ex, List<LogArg> lstArg = null)
        {
            var systemLog = new SystemLog();
            systemLog.Category = LogCategory.Exception.ToString();
            systemLog.CreatedDate = DateTime.Now;
            var messages = new StringBuilder();
            messages.AppendLine(string.Format("Exception:{0}", CommonFun.DumpExceptionToJsonString(ex)));
            if (lstArg != null)
            {
                lstArg.ForEach(m => messages.AppendLine(string.Format("Name:{0}-Value:{1}", m.Name, CommonFun.DumpObjectToJsonStringIgnoreException(m.Value))));
            }
            systemLog.Message = messages.ToString();
            WriteLogEntry(systemLog);

        }

        /// <summary>
        /// Add debug log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="lstArg"></param>
        public static void LogDebug(string message, List<LogArg> lstArg = null)
        {
            if (isTracingEnabled)
            {
                var systemLog = new SystemLog();
                systemLog.Category = LogCategory.Debug.ToString();
                systemLog.CreatedDate = DateTime.Now;
                var messages = new StringBuilder();
                messages.AppendLine(message);
                if (lstArg != null)
                {
                    lstArg.ForEach(m => messages.AppendLine(string.Format("Name:{0}-Value:{1}", m.Name, CommonFun.DumpObjectToJsonStringIgnoreException(m.Value))));
                }
                systemLog.Message = messages.ToString();
                WriteLogEntry(systemLog);
            }
        }

        public static void LogDBDebug(string message, List<LogArg> lstArg = null)
        {
            if (isTracingEnabled)
            {
                var systemLog = new SystemLog();
                systemLog.Category = LogCategory.DbDebug.ToString();
                systemLog.CreatedDate = DateTime.Now;
                var messages = new StringBuilder();
                messages.AppendLine(message);
                if (lstArg != null)
                {
                    lstArg.ForEach(m => messages.AppendLine(string.Format("Name:{0}-Value:{1}", m.Name, CommonFun.DumpObjectToJsonStringIgnoreException(m.Value))));
                }
                systemLog.Message = messages.ToString();
                WriteLogEntry(systemLog);
            }
        }
        /// <summary>
        /// Add method level of tracing log
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="elapsedTime"></param>
        /// <param name="methodArgs"></param>
        public static void LogMethodTracing(string className, string methodName, long elapsedTime, List<LogArg> methodArgs)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Class {0}.{1} (elasped time: {2} ms)", className, methodName, elapsedTime);
            LogDebug(sb.ToString(), methodArgs);
        }

        /// <summary>
        /// Create a method tracer
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="methodArgs"></param>
        /// <returns></returns>
        public static MethodTracer CreateMethodTracer(string className, string methodName, List<LogArg> methodArgs = null)
        {
            if (IsTracingEnabled)
            {
                return new MethodTracer(className, methodName, methodArgs);
            }
            else
            {
                return new MethodTracer();
            }
        }

        #endregion 
    }
}
