﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CL.Framework.Logging
{
    public interface ILogger
    {
        void WriteLogEntry(SystemLog systemLog);
        void WriteSqlAuditEntry(SqlAuditLog sqlAuditLog);
    }
}
